<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="/css/test.css">
</head>
<body>
    <nav class="navbar clearfix">
        <div class="container">
            <ul class="nav">
                <li><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/simulations">Simulations</a></li>
                <li><a href="/admin/statistics">Statistic</a></li>
                <li><a href="/admin/simelation/lol">Life of Luxury</a></li>
                <li><a href="/admin/simelation/lol2">Life of Luxury 2</a></li>
                <li><a href="/admin/simelation/elGallo">Loteria</a></li>
                <li><a href="/admin/simelation/superkeno">Superball Keno</a></li>
                <li><a href="/admin/simelation/doublekeno">Double-Up Keno</a></li>
            </ul>
        </div>
    </nav>
<div class="main">
    <div class="container">
        <div class="row">
            @if($game === 'elGallo')
                <h1>Loteria</h1>
            @elseif($game === 'lifeOfLuxury')
                <h1>Life of Luxury</h1>
            @endif
            <div class="col-6">
                <div class="work_space">
                    <form
                        @if($game === 'elGallo')
                            action="/admin/simelation/elGallo"
                        @elseif($game === 'lifeOfLuxury')
                            action="/admin/simelation/lol"
                        @endif
                       method="get">
                        {{ csrf_field() }}
                        <div class="row_block">
                            <div class="left_side">
                                <label for="lines">lines:</label>
                            </div>
                            <div class="right_side">
                                <input type="text" id="lines" name="linesInGame" value="{{$linesInGame}}">
                            </div>
                        </div>
                        <div class="row_block">
                            <div class="left_side">
                                <label for="bet">bet per line:</label>
                            </div>
                            <div class="right_side">

                                    @if($game === 'elGallo')
                                        <input type="text" id="bet" name="betLine" value="{{$betLine}}">
                                    @elseif($game === 'lifeOfLuxury')
                                        <select name="betLine">
                                            <option value="0.01" selected>0.01</option>
                                            <option value="0.02">0.02</option>
                                            <option value="0.03">0.03</option>
                                            <option value="0.04">0.04</option>
                                            <option value="0.05">0.05</option>
                                            <option value="0.06">0.06</option>
                                            <option value="0.07">0.07</option>
                                            <option value="0.08">0.08</option>
                                            <option value="0.09">0.09</option>
                                            <option value="0.10">0.10</option>
                                        </select>
                                    @endif

                            </div>
                        </div>
                        <div class="row_block">
                            <div class="left_side">
                                <label for="iterations">iterations:</label>
                            </div>
                            <div class="right_side">
                                <input type="text" id="iterations" name="itr" value="{{$itr}}">
                            </div>
                        </div>
                        @if($game === 'elGallo')
                            <div class="row_block">
                                <div class="left_side">
                                    <label for="fast_mod">fast mod:</label>
                                </div>
                                <div class="right_side">
                                    <input type="checkbox" id="fast_mod" name="fast_mod" value="on" checked>
                                </div>
                            </div>
                        @endif

                        @if($game === 'elGallo')
                            <!-- <div class="btn-wrap">
                                <a href="/admin/simelation/elGallo/set-percentages">Set percentages</a>
                            </div> -->
                        @endif

                        <div class="btn-wrap">
                            <button class="btn">Begin</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-6">
                <div class="work_space">
                    Total bet = {{$allBet}} <br>
                    Total win = {{$allWinOnSlots}}<br><br> <!-- total winnings in the main game -->
                    @if($spinCount === 0)
                        Spins Count = {{$itr}} <br>
                    @else
                        Spins Count = {{$spinCount}} <br>
                    @endif
                    Win Spins Count = {{$numberOfWinningSpins}}<br>
                    Lose Spins Count = {{$numberOfLosingSpins}}<br>
                    Win Spins Amount = {{$allWinOnMainLocation}}<br>
                    @if($numberOfWinningSpins === 0)
                        Win % = 0<br><br>
                    @else
                        Win % = {{100 / $spinCount * $numberOfWinningSpins}} %<br><br>
                    @endif

                    Free Spins Count = {{$countFreeSpin}}<br>
                    Free Spins Amount = {{$freeSpinAllWin}}<br>
                    Win spin % = {{$freeSpinsWinSpinPercent}}<br><br>

                    @if($game === 'elGallo')
                        Jackpots Count = {{$numberOfJackpots}}<br>
                        MINI Jackpot Count = {{$jackpotCountsArray['MINI']}}<br>
                        MINOR Jackpot Count = {{$jackpotCountsArray['MINOR']}}<br>
                        MAJOR Jackpot Count = {{$jackpotCountsArray['MAJOR']}}<br>
                        MAXI Jackpot Count = {{$jackpotCountsArray['BIG_DADDY']}}<br><br>

                        MINI Jackpot Amount = {{$jackpotArray['MINI']}}<br>
                        MINOR Jackpot Amount = {{$jackpotArray['MINOR']}}<br>
                        MAJOR Jackpot Amount = {{$jackpotArray['MAJOR']}}<br>
                        MAXI Jackpot Amount = {{$jackpotArray['BIG_DADDY']}}<br><br>
                    @endif

                    PAYOUT = {{$probability1}} %<br>
                    PAYOUT by Spins = {{$probability2}} %<br>
                    PAYOUT by Free Spins = {{$freeSpinProbability}} %<br>
                    PAYOUT by Jackpots = {{$jackpotProbability}} %<br><br>

                    execution time = {{$time}} sec<br><br>

                    <hr>
                    <br>
                    Combination statistics:<br>
                    @if($game === 'elGallo')
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 0)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Diablo ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 1)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Nopal ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 2)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Gallo (start free game) ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 3)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Borracho ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 4)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Sirena ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 5)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Corazon ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 6)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Muerte ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 7)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Alacran ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 8)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Sol ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 9)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Mundo ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 10)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Chalupa ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @elseif($key === 11)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Calavera ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                                <br>
                            @endif
                        @endforeach
                    @elseif($game === 'lifeOfLuxury')
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 0)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Diamond ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 1)
                                @for($key2 = 5; $key2 > 1; $key2--)
                                    Plane ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 9)
                                @for($key2 = 5; $key2 > 1; $key2--)
                                    Yacht ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 4)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Car ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 5)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Ring ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 3)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Dollar ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 6)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Watch ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 7)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Gold ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 2)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Silver ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 8)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Bronze ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                        @foreach ($сombinationStats as $key => $сombinationStat)
                            @if($key === 10)
                                @for($key2 = 5; $key2 > 2; $key2--)
                                    Coin ({{$key2}}) {{$сombinationStat[$key2]}}<br>
                                @endfor
                            @endif
                        @endforeach
                        <br>
                    @endif
                    <hr>
                    <br>
                    @if($game === 'elGallo')
                        Diablo in the main game: {{$jokerInMainGameCount}}<br>
                        Diablo in the freespin game: {{$jokerInFreeGameCount}}<br>
                        <br>
                        Dropped gallo in one spin:<br>
                        @for($key2 = 5; $key2 > 0; $key2--)
                            Gallo ({{$key2}}) {{$galloCounterForSpins[$key2]}}<br>
                        @endfor
                        <br>
                        Minimum number of Diablo from the freespins game: {{$minDiabloValueInFreeSpinGame}}<br>
                        Maximum number of Diablo from the freespins game: {{$maxDiabloValueInFreeSpinGame}}<br>
                        <br>
                        Number of free games awarded:<br>
                        @foreach ($numberOfFreeGamesAwardedCounter as $key => $value)
                            count ({{$key}}) {{$value}}<br>
                        @endforeach
                        <br>
                        Free games multipliers:<br>
                        @foreach ($freeGameMultipliersCounter as $key => $value)
                            multiplier ({{$key}}) {{$value}}<br>
                        @endforeach
                        <br>
                        Number of free games awarded (percantage):<br>
                        @foreach ($percentsOfFreeGamesAwardedCounter as $key => $value)
                            count ({{$key}}) {{round($value, 2)}} %<br>
                        @endforeach
                        <br>
                        Free games multipliers (percantage):<br>
                        @foreach ($percentsFreeGameMultipliersCounter as $key => $value)
                            multiplier ({{$key}}) {{round($value, 2)}} %<br>
                        @endforeach
                        <br>
                    @elseif($game === 'lifeOfLuxury')
                        Diamonds in the main game: {{$diamsInMainGame}}<br>
                        Diamonds in the freespin game: {{$freeSpinDiams}}<br>
                        <br>
                        Dropped coins in one spin:<br>
                        @for($key2 = 5; $key2 > 0; $key2--)
                            Coin ({{$key2}}) {{$countCoinsArr[$key2]}}<br>
                        @endfor
                        <br>
                        Which led to the bonus game:<br>
                        5 coins: {{$countCoinsAndDiamsArr['5coins']}}<br>
                        4 coins + 1 diamonds: {{$countCoinsAndDiamsArr['4coinsAnd1Diams']}}<br>
                        3 coins + 2 diamonds: {{$countCoinsAndDiamsArr['3coinsAnd2Diams']}}<br>
                        2 coins + 3 diamonds: {{$countCoinsAndDiamsArr['2coinsAnd3Diams']}}<br>
                        <br>
                        4 coins: {{$countCoinsAndDiamsArr['4coins']}}<br>
                        3 coins + 1 diamonds: {{$countCoinsAndDiamsArr['3coinsAnd1Diams']}}<br>
                        2 coins + 2 diamonds: {{$countCoinsAndDiamsArr['2coinsAnd2Diams']}}<br>
                        1 coins + 3 diamonds: {{$countCoinsAndDiamsArr['1coinsAnd3Diams']}}<br>
                        <br>
                        3 coins: {{$countCoinsAndDiamsArr['3coins']}}<br>
                        2 coins + 1 diamonds: {{$countCoinsAndDiamsArr['2coinsAnd1Diams']}}<br>
                        1 coins + 2 diamonds: {{$countCoinsAndDiamsArr['1coinsAnd2Diams']}}<br>
                        <br>
                        Minimum number of diamonds from the freespins game {{$minDiamValueInFreeSpinGame}}<br>
                        Maximum number of diamonds from the freespins game {{$maxDiamValueInFreeSpinGame}}<br>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
 <script type="text/javascript">
            document.body.querySelector('.btn').addEventListener('click', function () {
                   this.innerHTML = 'in process...'
            })
        </script>
</body>
</html>
