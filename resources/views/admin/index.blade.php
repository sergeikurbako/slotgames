<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/test.css">
</head>
<body>
    <nav class="navbar clearfix">
        <div class="container">
            <ul class="nav">
                <li><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/simulations">Simulations</a></li>
                <li><a href="/admin/statistics">Statistic</a></li>
            </ul>
        </div>
    </nav>
</body>
</html>
