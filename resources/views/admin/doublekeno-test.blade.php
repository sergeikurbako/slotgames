<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/test.css">
</head>
<body>
    <nav class="navbar clearfix">
        <div class="container">
            <ul class="nav">
                <li><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/simulations">Simulations</a></li>
                <li><a href="/admin/statistics">Statistic</a></li>
                <li><a href="/admin/simelation/lol">Life of Luxury</a></li>
                <li><a href="/admin/simelation/lol2">Life of Luxury 2</a></li>
                <li><a href="/admin/simelation/elGallo">Loteria</a></li>
                <li><a href="/admin/simelation/superkeno">Superball Keno</a></li>
                <li><a href="/admin/simelation/doublekeno">Double-Up Keno</a></li>
            </ul>
        </div>
    </nav>
<div class="main">
    <div class="container">
        <div class="row">
            <h1>Double-Up Keno</h1>
            <div class="col-6">
                <div class="work_space">
                    <form action="/admin/simelation/doublekeno"
                       method="get">
                       <div class="row_block">
                            <div class="left_side">
                                <label for="lines">balls:</label>
                            </div>
                            <div class="right_side">
                                <input type="text" id="balls" name="balls" value="{{$balls}}">
                            </div>
                        </div>
                        <div class="row_block">
                            <div class="left_side">
                                <label for="bet">bet:</label>
                            </div>
                            <div class="right_side">
                                <input type="text" id="bet" name="bet" value="{{$bet}}">
                            </div>
                        </div>
                        <div class="row_block">
                            <div class="left_side">
                                <label for="iterations">iterations:</label>
                            </div>
                            <div class="right_side">
                                <input type="text" id="iterations" name="itr" value="{{$itr}}">
                            </div>
                        </div>
                             <div class="btn-wrap">
                                                    <button class="btn">Begin</button>
                                                </div>
                    </form>
                </div>
            </div>
            <div class="col-6">
                <div class="work_space">
                    Total bet = {{$allBet}} <br>
                    Total win = {{$allWinOnMainLocation + $jackpotWinnings}}<br><br>

                    Plays Count = {{$itr}} <br>
                    Win Plays Count = {{$numberOfWinningSpins}}<br>
                    Lose Plays Count = {{$numberOfLosingSpins}}<br>
                    Win Plays Amount = {{$allWinOnMainLocation}}<br>
                    Win % = {{100 / $itr * $numberOfWinningSpins}}<br><br>

                    2 Matches = {{$matches[2]}}<br>
                    3 Matches = {{$matches[3]}}<br>
                    4 Matches = {{$matches[4]}}<br>
                    5 Matches = {{$matches[5]}}<br>
                    6 Matches = {{$matches[6]}}<br>
                    7 Matches = {{$matches[7]}}<br>
                    8 Matches = {{$matches[8]}}<br>
                    9 Matches = {{$matches[9]}}<br>
                    10 Matches = {{$matches[10]}}<br><br>

                    Jackpots Count = {{$numberOfJackpots}}<br>
                    Jackpot Amount = {{$jackpotWinnings}}<br><br>

                    PAYOUT = {{$percentageOfMoneyReturned}} %<br>
                    PAYOUT by Plays = {{$percentageOfMoneyReturnedOnMainGame}} %<br>
                    PAYOUT by Jackpots = {{$jackpotProbability}} %<br><br>

                    execution time = {{$time}} sec
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
                document.body.querySelector('.btn').addEventListener('click', function () {
                       this.innerHTML = 'in process...'
                })
            </script>
</div>
</body>
</html>
