<?php

use App\Classes\CronLoop\CronTaskExecuter\ZeroUserBalanceExecuter;
use App\Classes\CronLoop\CronTask\CronTask;

class ZeroUserBalanceExecuterTest extends TestCase
{
    /**
     * Общий тест обработки задачи
     */
    public function testexecuteTask1()
    {
        $check = true;
        $_SESSION['test'] = true;

        $zeroUserBalanceExecuter = new ZeroUserBalanceExecuter();
        $closed = $zeroUserBalanceExecuter->executeTask(
            [
                'token' => '123jhjgs',
                'userId' => '1',
                'gameId' => '1'
            ]
        );

        if (!is_bool($closed)) {
            $check = false;
        }

        $this->assertTrue($check);
    }

}
