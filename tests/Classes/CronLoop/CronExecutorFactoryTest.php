<?php

use App\Classes\CronLoop\CronExecutorFactory\CronExecutorFactory;
use App\Classes\CronLoop\CronTask\CronTask;
use App\Classes\CronLoop\CronTaskExecuter\Base\CronTaskExecuterBase;

class CronExecutorFactoryTest extends TestCase
{
    public function testCreateCronTaskExecutor()
    {
        $cronExecutorFactory = new CronExecutorFactory();
        $task = new CronTask;
        $task->setData(['data' => 'data']);
        $task->setType('user_have_zero_balance');


        $cronTaskExecutor = $cronExecutorFactory->createCronTaskExecutor($task);

        $this->assertInstanceOf(CronTaskExecuterBase::class, $cronTaskExecutor);
    }
}
