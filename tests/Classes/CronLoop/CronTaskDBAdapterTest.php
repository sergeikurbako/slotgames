<?php

use App\Classes\CronLoop\CronTaskAdapter\CronTaskDBAdapter;
use App\Classes\CronLoop\CronTask\CronTask;
use App\Models\CronTask as CronTaskDB;

class CronTaskDBAdapterTest extends TestCase
{
    public function testAdapt()
    {
        $cronTaskDBAdapter = new CronTaskDBAdapter();
        $task = new CronTaskDB;
        $task->data = serialize(['data' => 'data']);
        $task->type = 'user_have_zero_balance';

        $cronTask = $cronTaskDBAdapter->adapt($task);

        $this->assertInstanceOf(CronTask::class, $cronTask);
    }
}
