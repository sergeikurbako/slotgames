<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
    * Пустой тест необходимый, чтобы новые классы с тестами не выдавали ошибки если в них нет тестов
    */
    public function testEmpty()
    {
        $check = false;
        if (1 === 1) {
            $check = true;
        }

        $this->assertTrue($check);
    }
}
