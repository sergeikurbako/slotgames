<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\Models\Session;
use App\Models\CronTask;

class GameControllerTest extends TestCase
{
    // public function testExitFromDemoGame1()
    // {
    //     $check = true;
    //     $_SESSION['test'] = true;
    //
    //     // entry into the game
    //     $sessionID = Curl::to("http://localhost/startGame?test=true&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQwZjhhZmFmZWEzODUyMmM4NmI2MTlmNzhmYjE1NzA4MmM1MTliZjIwN2QyMDYyYTBhMWE4M2RjMzQwZjc4M2JhMjgzMDRlMmI2Yzg1MjdmIn0.eyJhdWQiOiIzIiwianRpIjoiNDBmOGFmYWZlYTM4NTIyYzg2YjYxOWY3OGZiMTU3MDgyYzUxOWJmMjA3ZDIwNjJhMGExYTgzZGMzNDBmNzgzYmEyODMwNGUyYjZjODUyN2YiLCJpYXQiOjE1NDEwNTk0NTYsIm5iZiI6MTU0MTA1OTQ1NiwiZXhwIjoxNTcyNTk1NDU2LCJzdWIiOiI0Iiwic2NvcGVzIjpbImFwaSJdfQ.RuXIgoxvj8kSD1rzabJxH_gMDEGYEhe-yYTWoBQJVTfTChowweCZ_4GEqyVKS7wMoGp95hnF461n_Mv8_NO5StJCXI3eVyrToGH_BCBn8zjl4qp2mZjnmqIXOZDfX4WTB_hu_dtclbl8lDvcGB7Xsg6AGUpI1JQReNZxlEQbuGtWesdO3yadmc5ofl3g32QlE9LGv49yhHIZK_Gi5iXaOyMlwXPItaRtKEHtzapMhLS-uBW-lN_KpH0gq-tjMUDftCqee2rg7WUrcwvedcMmd4tA8uKf4KTw5Y_Jn7pFhL9SAySo_hx5IZ1gwioWvW7v7vpMWWdGHzP2GFGGW2Xd_ogbl5v8q-hmkJU_jT1JqsZkvmudmxGb4AaOttpR6Vi6lzR21dQr_Ph9DMLaDMXtXaakYHJka8AgnryWnkI8wuN3OFYZkkqvWeVHpjxQ_r-elevHZDYGI-iXsw0Xjd10XF3CYzNNCQYlpgw7ZHM5E-X2XaS0fNZrxv2ldaH1q7b9XLg2bL-zKpj_gS8Z3RwVRlZKQ4D1oCSBVxt_E1DzznssJm6ZTeQWY5imwX30PgeGOqv_Ewsgy_WWeadO4bXp4K_vZRp6TsgszfjfofkH4qPIy9wARb9LdHpah61cyTSTan1SlISdGHEBd3RB3E6Ikgy1OoIyuzDnU3GVaQetwJc&userId=4&nickname=t@t.com&gameId=1&demo=true&providerId=1&platformId=1")
    //         ->get();
    //
    //     $responseInit = Curl::to("http://localhost/init?sessionID=" . $sessionID)
    //         ->get();
    //
    //     $responseState = Curl::to("http://localhost/state?sessionID=" . $sessionID . '&game=elgallo')
    //         ->get();
    //
    //     $session = (new Session)->where('uuid', '=', $sessionID)->orderBy('created_at', 'desc')->get()->first();
    //
    //     $responseExit = Curl::to('http://localhost/exit?token=' . $session->token . '&userId=' . $session->userId . '&gameId=' . $session->gameId . '&collect=false&test=true' )
    //         ->get();
    //
    //     if ($responseExit !== 'exit_response_for_test_done') {
    //         $check = false;
    //     }
    //
    //     $session->delete();
    //
    //     $this->assertTrue($check);
    // }
    //
    // public function testActionCreateZeroBalanceCronTask()
    // {
    //     $check = true;
    //
    //     $response = Curl::to('http://localhost/create-zero-balance-cron-task?userId=1&gameId=1&token=cj823ync2&test=true')
    //         ->get();
    //
    //     if (json_decode($response)->status !== 'true') {
    //         $check = false;
    //     }
    //
    //     // проверка наличия cron task
    //     $cronTask = (new CronTask)->where('data', '=', serialize(['userId' => '1', 'gameId' => '1', 'token' => 'cj823ync2']))->get()->first();
    //     if ($cronTask == null) {
    //         $check = false;
    //     }
    //     $cronTask->delete();
    //
    //     $this->assertTrue($check);
    // }
    //
    // public function testActionSpinEG1()
    // {
    //     $check = true;
    //     $_SESSION['test'] = true;
    //
    //     // entry into the game
    //     $sessionID = Curl::to("http://localhost/startGame?test=true&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQwZjhhZmFmZWEzODUyMmM4NmI2MTlmNzhmYjE1NzA4MmM1MTliZjIwN2QyMDYyYTBhMWE4M2RjMzQwZjc4M2JhMjgzMDRlMmI2Yzg1MjdmIn0.eyJhdWQiOiIzIiwianRpIjoiNDBmOGFmYWZlYTM4NTIyYzg2YjYxOWY3OGZiMTU3MDgyYzUxOWJmMjA3ZDIwNjJhMGExYTgzZGMzNDBmNzgzYmEyODMwNGUyYjZjODUyN2YiLCJpYXQiOjE1NDEwNTk0NTYsIm5iZiI6MTU0MTA1OTQ1NiwiZXhwIjoxNTcyNTk1NDU2LCJzdWIiOiI0Iiwic2NvcGVzIjpbImFwaSJdfQ.RuXIgoxvj8kSD1rzabJxH_gMDEGYEhe-yYTWoBQJVTfTChowweCZ_4GEqyVKS7wMoGp95hnF461n_Mv8_NO5StJCXI3eVyrToGH_BCBn8zjl4qp2mZjnmqIXOZDfX4WTB_hu_dtclbl8lDvcGB7Xsg6AGUpI1JQReNZxlEQbuGtWesdO3yadmc5ofl3g32QlE9LGv49yhHIZK_Gi5iXaOyMlwXPItaRtKEHtzapMhLS-uBW-lN_KpH0gq-tjMUDftCqee2rg7WUrcwvedcMmd4tA8uKf4KTw5Y_Jn7pFhL9SAySo_hx5IZ1gwioWvW7v7vpMWWdGHzP2GFGGW2Xd_ogbl5v8q-hmkJU_jT1JqsZkvmudmxGb4AaOttpR6Vi6lzR21dQr_Ph9DMLaDMXtXaakYHJka8AgnryWnkI8wuN3OFYZkkqvWeVHpjxQ_r-elevHZDYGI-iXsw0Xjd10XF3CYzNNCQYlpgw7ZHM5E-X2XaS0fNZrxv2ldaH1q7b9XLg2bL-zKpj_gS8Z3RwVRlZKQ4D1oCSBVxt_E1DzznssJm6ZTeQWY5imwX30PgeGOqv_Ewsgy_WWeadO4bXp4K_vZRp6TsgszfjfofkH4qPIy9wARb9LdHpah61cyTSTan1SlISdGHEBd3RB3E6Ikgy1OoIyuzDnU3GVaQetwJc&userId=4&nickname=t@t.com&gameId=1&demo=true&providerId=1&platformId=1")
    //         ->get();
    //
    //     $responseInit = Curl::to("http://localhost/init?sessionID=" . $sessionID)
    //         ->get();
    //
    //     $responseState = Curl::to("http://localhost/state?sessionID=" . $sessionID . '&game=elgallo')
    //         ->get();
    //
    //     $session = (new Session)->where('uuid', '=', $sessionID)->orderBy('created_at', 'desc')->get()->first();
    //
    //     $responseExit = Curl::to('http://localhost/exit?token=' . $session->token . '&userId=' . $session->userId . '&gameId=' . $session->gameId . '&collect=false&test=true' )
    //         ->get();
    //
    //     if ($responseExit !== 'exit_response_for_test_done') {
    //         $check = false;
    //     }
    //
    //     $session->delete();
    //
    //     $this->assertTrue($check);
    // }

}
