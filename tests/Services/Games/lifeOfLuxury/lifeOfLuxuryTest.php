<?php

use App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService;
use App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService2;
use App\Models\BridgeApiRequest;

class lifeOfLuxuryTest extends TestCase
{
    public $game;

    public function __construct()
    {
        $this->game = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();
    }

    /*************************************************************************
     * Тесты основной логики
     ************************************************************************/

    /**
     * Проверка появления дробного баланса
     */
    public function testNotFloatBalance()
    {
        $this->prepareTest();
        $check = true;
        $checkFloat = false;
        $info = [1,5,2,2,6,0,3,5,6,8,6,7,5,7,6];

        // получение и проверка результатов с меняющейся ставкой на линию
        for ($i = 1; $i <= 10; $i++) {
            $betLine = $i / 100;

            // изменение кол-ва выбранных линий
            for ($i = 1; $i <= 15; $i++) {
                // получение результата хода
                $spinResult = $this->game->getSpinResultData($betLine, 15, 'lifeOfLuxury', $info);

                // проверка на присутствие в wl "бесконечной" дроби
                foreach ($spinResult['wl'] as $key => $value) {
                    $roundValue = round($value, 2);

                    if ($value > $roundValue) {
                        $checkFloat = true;
                    }
                }

                if ($spinResult['balance'] > round($spinResult['balance'], 2)) {
                    $checkFloat = true;
                }

                if ($spinResult['allWin'] > round($spinResult['allWin'], 2)) {
                    $checkFloat = true;
                }
            }
        }

        if ($checkFloat === true) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша
     */
    public function testAllWin1()
    {
        $this->prepareTest();
        $check = true;
        $info = [3,6,5,8,3,5,6,2,3,6,5,2,2,7,6];

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $info);

        if ($spinResult['allWin'] !== 1.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша
     */
    public function testAllWin2()
    {
        $this->prepareTest();
        $check = true;
        $info = [6,2,4,2,10,6,7,3,4,3,4,5,5,7,6];

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $info);

        if ($spinResult['allWin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша
     */
    public function testAllWin3()
    {
        $this->prepareTest();
        $check = true;
        $info = [8,9,7,6,2,9,8,9,2,2,3,8,2,6,5];

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $info);

        if ($spinResult['allWin'] != 3.5) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша
     */
    public function testAllWin4()
    {
        $this->prepareTest();
        $check = true;
        $info = [8,9,7,0,2,3,8,9,2,2,3,8,2,6,5];

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $info);

        if ($spinResult['allWin'] != 8) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша за несколько ходов
     */
    public function testAllWin5()
    {
        $this->prepareTest();
        $check = true;
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $infoArray = [
            [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1],
            [7,8,9,4,7,8,2,8,7,7,5,8,8,2,5],
            [2,6,7,5,7,8,4,8,2,2,4,3,2,7,8],
            [3,6,9,0,6,8,5,7,2,5,8,6,8,6,4],
            [8,3,6,4,7,8,6,8,0,1,2,7,4,8,6],
            [8,7,3,1,3,2,3,8,5,8,3,2,8,5,6],
            [7,2,6,7,3,8,0,2,6,6,7,8,6,8,3],
            [8,7,4,6,2,3,6,1,8,7,6,5,7,6,4],
            [3,7,4,8,5,6,9,7,1,8,4,7,8,2,7],
            [8,9,6,6,8,7,6,7,2,8,4,2,7,6,8],
            [2,8,7,6,7,2,6,2,8,6,8,3,8,6,3]
        ];

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[0]);
        if ($spinResult['allWin'] != 3) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[1]);
        if ($spinResult['allWin'] != 2) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[2]);
        if ($spinResult['allWin'] != 0) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[3]);
        if ($spinResult['allWin'] != 0) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[4]);
        if ($spinResult['allWin'] != 0) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[5]);
        if ($spinResult['allWin'] != 20) {
            $check = false;
        }

        $spinResult = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', $infoArray[5]);
        if ($spinResult['allWin'] != 20) {
            $check = false;
        }


        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к BridgeApi в фриспинах
     */
    public function testSendBridgeRequestInFreeSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        // выпадают фриспины
        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [10,2,2,10,7,8,10,7,8,4,2,8,2,11,1]);
        $bridgeApiRequests = (new BridgeApiRequest)->all()->toArray();

        if (count($bridgeApiRequests) !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка возможности выпадения на одном барабане алмаза и монеты в LifeOfLuxuryService
     */
    public function testCoinWithDiamondInRow()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $countCoinAndDiamondOnOneRow = 0;
        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury');
            $info = $result['info'];

            for ($k=0; $k < 5; $k++) {
                $diams = 0;
                $coins = 0;
                for ($j=0; $j < 3; $j++) {
                    if ($info[$k * 3 + $j] === 10) {
                        $coins += 1;
                    }

                    if ($info[$k * 3 + $j] === 0) {
                        $diams += 1;
                    }
                }

                // прерка наличия алмаза и монеты на одном барабане
            }

            if ($diams !== 0 && $coins !== 0) {
                $countCoinAndDiamondOnOneRow += 1;
            }
        }

        if ($countCoinAndDiamondOnOneRow !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка возможности выпадения на одном барабане алмаза и монеты в LifeOfLuxuryService2
     */
    public function testCoinWithDiamondInRowV2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $countCoinAndDiamondOnOneRow = 0;
        for ($i=0; $i < 10; $i++) {
            $result = (new LifeOfLuxuryService2)->getSpinResultData(1, 25, 'lifeOfLuxury');
            $info = $result['info'];

            for ($k=0; $k < 5; $k++) {
                $diams = 0;
                $coins = 0;
                for ($j=0; $j < 3; $j++) {
                    if ($info[$k * 3 + $j] === 10) {
                        $coins += 1;
                    }

                    if ($info[$k * 3 + $j] === 0) {
                        $diams += 1;
                    }
                }

                // прерка наличия алмаза и монеты на одном барабане
            }

            if ($diams !== 0 && $coins !== 0) {
                $countCoinAndDiamondOnOneRow += 1;
            }
        }

        if ($countCoinAndDiamondOnOneRow !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка выпадения несуществующих символов в новой версии генератора ячеек
     */
    public function testNotExistingSymbolsOnV2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $notExistingSymbols = 0;
        for ($i=0; $i < 10; $i++) {
            $result = (new LifeOfLuxuryService2)->getSpinResultData(1, 25, 'lifeOfLuxury');
            $info = $result['info'];

            for ($k=0; $k < 15; $k++) {
                if ($info[$k] > 10) {
                    $notExistingSymbols += 1;
                }
            }
        }

        if ($notExistingSymbols !== 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка проигрыша в основной игре
     */
    public function testLoseInMainGame()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        if ($result['allWin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка выигрыша в основной игре
     */
    public function testWinInMainGame()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [1,2,2,1,3,3,4,4,4,5,5,5,6,6,6]);

        if ($result['allWin'] === 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка выпадения фриспинов
     */
    public function testDropFreeSpinGame()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);

        if ($_SESSION['freeSpinData'] === false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка баланса при выпадении проигрыша в основной игре
     */
    public function testAllWin7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [2,2,2,3,3,3,4,10,4,5,5,5,6,6,6]);

        if ($result['allWin'] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка баланса при выпадении выигрыша в основной игре
     */
    public function testAllWin8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [1,2,2,1,3,3,4,1,4,5,5,5,6,6,6]);

        if ($result['allWin'] == 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка баланса при выпадении и прокручивании фриспинов в которых все ходы проигрышные
     */
    public function testAllWin9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);

        $allWin += $result['allWin'];

        for ($i=0; $i < 10; $i++) {
            $result2 = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
            $allWin += $result2['allWin'];
        }

        if ($allWin != 50) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка  баланса при выпадении и прокручивании фриспинов в которых все ходы выигрышные
     */
    public function testAllWin10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $result = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [10,2,2,10,3,3,4,10,4,5,5,5,6,6,6]);

        $allWin += $result['allWin'];

        for ($i=0; $i < 10; $i++) {
            $result2 = $this->game->getSpinResultData(1, 25, 'lifeOfLuxury', [1,2,2,1,3,3,1,4,4,5,5,5,6,6,6]);
            $allWin += $result2['allWin'];
        }

        if ($allWin <= 50) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    // /**
    //  * Проверка работы множетеля в фриспинах при выпадении выигрышна
    //  */
    // public function ()
    // {
    //
    // }
    //
    // /**
    //  * Проверка работы увеличения множетеля в фриспинах при выпадении алмазов
    //  */
    // public function ()
    // {
    //
    // }
    //


    /**
     * Тест пересборки барабана при выпадении в одном барабане алмаза и монеты
     */
    public function testExcludeDiamondAndCoinInOneDrum1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $cellValue = $this->game->excludeDiamondAndCoinInOneDrum(10, [1,0], 2);

        if ($cellValue === 10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест пересборки барабана при выпадении в одном барабане алмаза и монеты
     */
    public function testExcludeDiamondAndCoinInOneDrum2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $cellValue = $this->game->excludeDiamondAndCoinInOneDrum(0, [1,10], 2);

        if ($cellValue === 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 1 линии
     */
    public function testWinLinesInLol1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 1, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        // проверка wl
        if ($result['wl'][1] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][1] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][13] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 2 линии
     */
    public function testWinLinesInLol2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 2, 'lifeOfLuxury', [2,2,2,2,3,3,2,4,4,2,5,5,2,6,6]);

        // проверка wl
        if ($result['wl'][2] != 120) {
            $check = false;
        }


        // проверка winCellInfo
        if ($result['winCellInfo'][0] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][3] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][6] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][9] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][12] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 3 линии
     */
    public function testWinLinesInLol3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 3, 'lifeOfLuxury', [2,2,2,3,3,2,4,4,2,5,5,2,6,6,2]);

        // проверка wl
        if ($result['wl'][3] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][2] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][5] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][8] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][11] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][14] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 4 линии
     */
    public function testWinLinesInLol4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 4, 'lifeOfLuxury', [2,2,2,3,2,3,4,4,2,5,2,5,2,6,6]);

        // проверка wl
        if ($result['wl'][4] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][0] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][8] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][12] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 5 линии
     */
    public function testWinLinesInLol5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 5, 'lifeOfLuxury', [2,2,2,3,2,3,2,4,4,5,2,5,6,6,2]);

        // проверка wl
        if ($result['wl'][5] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][2] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][6] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][14] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 6 линии
     */
    public function testWinLinesInLol6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 6, 'lifeOfLuxury', [2,2,2,2,3,3,4,2,4,5,5,2,6,2,6]);

        // проверка wl
        if ($result['wl'][6] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][1] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][3] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][11] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][13] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 7 линии
     */
    public function testWinLinesInLol7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 7, 'lifeOfLuxury', [2,2,2,3,3,2,4,2,4,2,5,5,6,2,6]);

        // проверка wl
        if ($result['wl'][7] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][1] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][5] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][9] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][13] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 8 линии
     */
    public function testWinLinesInLol8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 8, 'lifeOfLuxury', [2,2,2,2,3,3,4,2,4,5,5,2,6,6,2]);

        // проверка wl
        if ($result['wl'][8] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][0] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][3] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][11] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][14] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 9 линии
     */
    public function testWinLinesInLol9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 9, 'lifeOfLuxury', [2,2,2,3,3,2,4,2,4,2,5,5,2,6,6]);

        // проверка wl
        if ($result['wl'][9] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][2] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][5] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][9] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][12] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 10 линии
     */
    public function testWinLinesInLol10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 10, 'lifeOfLuxury', [2,2,2,3,2,3,2,4,4,5,2,5,2,6,6]);

        // проверка wl
        if ($result['wl'][10] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][0] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][6] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][12] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 11 линии
     */
    public function testWinLinesInLol11()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 11, 'lifeOfLuxury', [2,2,2,3,2,3,4,4,2,5,2,5,6,6,2]);

        // проверка wl
        if ($result['wl'][11] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][2] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][8] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][14] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 12 линии
     */
    public function testWinLinesInLol12()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 12, 'lifeOfLuxury', [2,2,2,2,3,3,2,4,4,2,5,5,6,2,6]);

        // проверка wl
        if ($result['wl'][12] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][1] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][3] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][6] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][9] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][13] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 13 линии
     */
    public function testWinLinesInLol13()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 13, 'lifeOfLuxury', [2,2,2,3,3,2,4,4,2,5,5,2,6,2,6]);

        // проверка wl
        if ($result['wl'][13] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][1] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][5] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][8] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][11] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][13] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 14 линии
     */
    public function testWinLinesInLol14()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 14, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,2,6,6]);

        // проверка wl
        if ($result['wl'][14] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][0] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][12] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша по включенным линиях при влючении 15 линии
     */
    public function testWinLinesInLol15()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(1, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,6,2]);

        // проверка wl
        if ($result['wl'][15] != 120) {
            $check = false;
        }

        // проверка winCellInfo
        if ($result['winCellInfo'][2] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][4] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][7] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][10] !== 2) {
            $check = false;
        }
        if ($result['winCellInfo'][14] !== 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест пересборки барабана при выпадении в одном барабане одинаковых символов
     */
    public function testExcludeSameCharactersInOneDrum1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $cellValue = $this->game->excludeSameCharactersInOneDrum(0, [0,1], 2);

        if ($cellValue === 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест пересборки барабана при выпадении в одном барабане одинаковых символов
     */
    public function testExcludeSameCharactersInOneDrum2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $cellValue = $this->game->excludeSameCharactersInOneDrum(10, [2,10], 2);

        if ($cellValue === 10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест пересборки барабана при выпадении в одном барабане одинаковых символов
     */
    public function testExcludeSameCharactersInOneDrum3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $allWin = 0;
        $cellValue = $this->game->excludeSameCharactersInOneDrum(3, [2,3], 2);

        if ($cellValue === 3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(2, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 240) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 1.2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.02, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 2.4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.03, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 3.6) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.04, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 4.8) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.05, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 6.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.06, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 7.2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.07, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 8.4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.08, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 9.6) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.09, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 10.8) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet11()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 12.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet12()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.11, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 13.2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet13()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.12, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 14.4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet14()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.13, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 15.6) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet15()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.14, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 16.8) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест проверяющий, что выигрышь по линии умножается на ставку
     */
    public function testWinOnWlMulOnBet16()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.15, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 18) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий, что победы должны происходить на соседних барабанах,
     * начиная с самой левой катушки
     * Итог: выигрышь по линии 1 не происходит при отсутствии первого символа
     */
    public function testVictoriesOnNeighboringDrums1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.15, 15, 'lifeOfLuxury', [2,5,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий, что победы должны происходить на соседних барабанах,
     * начиная с самой левой катушки
     * Итог: выигрышь по линии 1 происходит при отсутствии первого символа
     */
    public function testVictoriesOnNeighboringDrums2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.15, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] == 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий, что победы должны происходить на соседних барабанах,
     * начиная с самой левой катушки
     * Итог: выигрышь по линии 1 происходит при отсутствии первого символа
     */
    public function testVictoriesOnLinesTogetherWithFreeSpins1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.15, 15, 'lifeOfLuxury', [10,2,2,10,2,3,10,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] != 18) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий, что победы должны происходить на соседних барабанах,
     * начиная с самой левой катушки
     * Итог: выигрышь по линии 1 происходит при отсутствии первого символа
     */
    public function testVictoriesOnLinesTogetherWithFreeSpins2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.15, 15, 'lifeOfLuxury', [10,3,2,10,2,3,10,2,4,5,2,5,6,2,6]);

        if ($result['wl'][1] > 0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест дополнительного удвоения выигрыша за выпадение фриспинов при наличии алмаза
     * Итог: дополнительное удвоение не должно происходить
     */
    public function testDoubleWinOnFreeSpinIfHaveDiamond1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,0,3,3,10,4,4,5,5,5,6,6,6]);

        if ($result['allWin'] != 0.3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест размера выигрыша по 2,3,4,5 символам "самолет"
     */
    public function testWinValueFromCombinations1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // два символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,4,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.1) {
            $check = false;
        }

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,1,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.5) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,1,4,5,1,5,6,6,6]);
        if ($result['wl'][1] != 5) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,1,4,5,1,5,6,1,6]);
        if ($result['wl'][1] != 50) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест размера выигрыша по 2,3,4,5 символам "яхта"
     */
    public function testWinValueFromCombinations2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // два символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,9,3,4,4,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.05) {
            $check = false;
        }

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,9,3,4,9,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.3) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,9,3,4,9,4,5,9,5,6,6,6]);
        if ($result['wl'][1] != 2) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,9,3,4,9,4,5,9,5,6,9,6]);
        if ($result['wl'][1] != 10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "машина"
     */
    public function testWinValueFromCombinations3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,4,2,3,4,3,4,4,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.2) {
            $check = false;
        }


        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,4,2,3,4,3,4,4,4,5,4,5,6,6,6]);
        if ($result['wl'][1] != 1) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,4,2,3,4,3,4,4,4,5,4,5,6,4,6]);
        if ($result['wl'][1] != 5) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "кольцо"
     */
    public function testWinValueFromCombinations4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,5,2,3,5,3,4,5,4,5,3,5,6,6,6]);
        if ($result['wl'][1] != 0.15) {
            $check = false;
        }


        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,5,2,3,5,3,4,5,4,5,5,5,6,6,6]);
        if ($result['wl'][1] != 0.75) {
            $check = false;
        }


        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,5,2,3,5,3,4,5,4,5,5,5,6,5,6]);
        if ($result['wl'][1] != 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "доллар"
     */
    public function testWinValueFromCombinations5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,2,3,3,3,4,3,4,5,4,5,6,6,6]);
        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,2,3,3,3,4,3,4,5,3,5,6,6,6]);
        if ($result['wl'][1] != 0.50) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,2,3,3,3,4,3,4,5,3,5,6,3,6]);
        if ($result['wl'][1] != 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "часы"
     */
    public function testWinValueFromCombinations6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,6,2,3,6,3,4,6,4,5,4,5,4,4,4]);
        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,6,2,3,6,3,4,6,4,5,6,5,4,4,4]);
        if ($result['wl'][1] != 0.30) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,6,2,3,6,3,4,6,4,5,6,5,4,6,4]);
        if ($result['wl'][1] != 1.5) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "золото"
     */
    public function testWinValueFromCombinations7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,7,2,3,7,3,4,7,4,5,4,5,4,4,4]);
        if ($result['wl'][1] != 0.05) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,7,2,3,7,3,4,7,4,5,7,5,4,4,4]);
        if ($result['wl'][1] != 0.30) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,7,2,3,7,3,4,7,4,5,7,5,4,7,4]);
        if ($result['wl'][1] != 1.5) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "серебро"
     */
    public function testWinValueFromCombinations8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,4,5,4,4,4]);
        if ($result['wl'][1] != 0.05) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,4,4,4]);
        if ($result['wl'][1] != 0.25) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,4,2,4]);
        if ($result['wl'][1] != 1.2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест размера выигрыша по 3,4,5 символам "бронза"
     */
    public function testWinValueFromCombinations9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,8,2,3,8,3,4,8,4,5,4,5,4,4,4]);
        if ($result['wl'][1] != 0.05) {
            $check = false;
        }

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,8,2,3,8,3,4,8,4,5,8,5,4,4,4]);
        if ($result['wl'][1] != 0.20) {
            $check = false;
        }

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,8,2,3,8,3,4,8,4,5,8,5,4,8,4]);
        if ($result['wl'][1] != 1) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша при вывадении 2,3,4,5 монет
     */
    public function testWinValueFromCombinations10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // два символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,10,2,3,10,3,4,8,4,5,4,5,4,4,4]);
        if ($result['allWin'] != 0) {
            $check = false;
        }

        // три символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,10,2,3,10,3,4,10,4,5,4,5,4,4,4]);
        if ($result['allWin'] != 0.3) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        // четыре символа в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,10,2,3,10,3,4,10,4,5,10,5,4,4,4]);
        if ($result['allWin'] != 2.25) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        // пять символов в комбинации
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,10,2,3,10,3,4,10,4,5,10,5,4,10,4]);
        if ($result['allWin'] != 15) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "самолет" при выпадении на линии одного самолета и одного алмаза
     */
    public function testReplacingTheDiamondSymbol1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,0,3,4,4,4,5,5,5,6,6,6]);

        if ($result['wl'][1] != 0.20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "яхта" при выпадении на линии одной яхты и одного алмаза
     */
    public function testReplacingTheDiamondSymbol2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,0,3,4,4,4,5,5,5,6,6,6]);

        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "машина" при выпадении на линии двух машин и одного алмаза
     */
    public function testReplacingTheDiamondSymbol3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,4,2,3,4,3,4,0,4,5,5,5,6,6,6]);

        if ($result['wl'][1] != 0.40) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "кольцо" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,5,2,3,5,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.30) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "доллар" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,2,3,3,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "часы" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,6,2,3,6,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "золото" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,7,2,3,7,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "серебро" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "бронза" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,8,2,3,8,3,4,0,4,1,1,1,9,9,9]);

        if ($result['wl'][1] != 0.10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест замены алмазом символа "монета" при выпадении на линии двух колец и одного алмаза
     */
    public function testReplacingTheDiamondSymbol10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,10,2,3,10,3,4,0,4,1,1,1,9,9,9]);

        if ($result['allWin'] != 0.30) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест однократного удвоения выигрыша за линию, при наличии в выигрышной комбинации 1,2,3 алмазов
     */
    public function testDoubleWinFromDiamonds1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,1,4,1,1,1,9,1,9]);
        if ($result['wl'][1] != 50) {
            $check = false;
        }

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,1,4,1,0,1,9,1,9]);
        if ($result['wl'][1] != 100) {
            $check = false;
        }

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,1,3,4,0,4,1,0,1,9,1,9]);
        if ($result['wl'][1] != 100) {
            $check = false;
        }

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,1,2,3,0,3,4,0,4,1,0,1,9,1,9]);
        if ($result['wl'][1] != 100) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 яхтами и алмазом
     */
    public function testDoubleWinFromDiamonds2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,9,2,3,9,3,4,9,4,1,9,1,9,0,9]);
        if ($result['wl'][1] != 20) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 машинами и алмазом
     */
    public function testDoubleWinFromDiamonds3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,4,2,3,4,3,4,4,4,1,4,1,9,0,9]);
        if ($result['wl'][1] != 10) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 кольцами и алмазом
     */
    public function testDoubleWinFromDiamonds4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,5,2,3,5,3,4,5,4,1,5,1,9,0,9]);
        if ($result['wl'][1] != 4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 долларами и алмазом
     */
    public function testDoubleWinFromDiamonds5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,2,3,3,3,4,3,4,1,3,1,9,0,9]);
        if ($result['wl'][1] != 4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 часами и алмазом
     */
    public function testDoubleWinFromDiamonds6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,6,2,3,6,3,4,6,4,1,6,1,9,0,9]);
        if ($result['wl'][1] != 3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 золотами и алмазом
     */
    public function testDoubleWinFromDiamonds7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,7,2,3,7,3,4,7,4,1,7,1,9,0,9]);
        if ($result['wl'][1] != 3) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 серебром и алмазом
     */
    public function testDoubleWinFromDiamonds8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,1,2,1,9,0,9]);
        if ($result['wl'][1] != 2.4) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест удвоения выигрыша за линию с 4 бронзами и алмазом
     */
    public function testDoubleWinFromDiamonds9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,8,2,3,8,3,4,8,4,1,8,1,9,0,9]);
        if ($result['wl'][1] != 2) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выпадения алмазов на барабанах 2,3 и 4 и их отсутствия на барабанах 1 и 5.
     */
    public function testFallSymbolsOnDrums1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // алмазы не выпадают на первом барабане
        $percentArr = $this->game->getPercentArray(0);
        if ($percentArr[0] !== [0,0]) {
            $check = false;
        }

        // алмазы не выпадают на 5 барабане
        $percentArr = $this->game->getPercentArray(4);
        if ($percentArr[0] !== [0,0]) {
            $check = false;
        }

        // алмазы выпадают на 2, 3, 4 барабанах
        $percentArr = $this->game->getPercentArray(1);
        if ($percentArr[0] === [0,0]) {
            $check = false;
        }
        $percentArr = $this->game->getPercentArray(2);
        if ($percentArr[0] === [0,0]) {
            $check = false;
        }
        $percentArr = $this->game->getPercentArray(3);
        if ($percentArr[0] === [0,0]) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выпадения не существующих символов в основной игре
     */
    public function testFallSymbolsOnDrums2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        // проверка выпадения несуществующих символов на 1 барабане
        $percentArr = $this->game->getPercentArray(0);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 2 барабане
        $percentArr = $this->game->getPercentArray(1);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 3 барабане
        $percentArr = $this->game->getPercentArray(2);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 4 барабане
        $percentArr = $this->game->getPercentArray(3);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 5 барабане
        $percentArr = $this->game->getPercentArray(4);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выпадения не существующих символов в фриспин игре
     */
    public function testFallSymbolsOnDrums3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $_SESSION['freeSpinData'] = [10, 2, 1];

        // проверка выпадения несуществующих символов на 1 барабане
        $percentArr = $this->game->getPercentArray(0);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0]) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] === 100000) {
                $prevValue = [0, 0];
            } else {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 2 барабане
        $percentArr = $this->game->getPercentArray(1);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0] && $value[0] !== 0) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] !== 0) {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 3 барабане
        $percentArr = $this->game->getPercentArray(2);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0] && $value[0] !== 0) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] !== 0) {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 4 барабане
        $percentArr = $this->game->getPercentArray(3);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0] && $value[0] !== 0) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] !== 0) {
                $prevValue = $value;
            }
        }

        // проверка выпадения несуществующих символов на 5 барабане
        $percentArr = $this->game->getPercentArray(4);
        $prevValue = [0, 0];
        foreach ($percentArr as $key => $value) {
            // сравнение диапозонов вероятностей у символов
            if ($prevValue[1] !== $value[0] && $value[0] !== 0) {
                $check = false;
                break;
            }

            // проверка на случай если вероятность выпадения для последнего символа = 0
            if ($value[1] !== 0) {
                $prevValue = $value;
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий отсутствие монет в фриспинах
     */
    public function testFallSymbolsOnDrums4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $_SESSION['freeSpinData'] = [10, 2, 1];

        // проверка выпадения монет на 1 барабане
        $percentArr = $this->game->getPercentArray(0);
        $prevValue = [0, 0];
        if ($percentArr[10] !== [0,0]) {
            $check = false;
        }

        // проверка выпадения монет на 2 барабане
        $percentArr = $this->game->getPercentArray(0);
        $prevValue = [0, 0];
        if ($percentArr[10] !== [0,0]) {
            $check = false;
        }

        // проверка выпадения монет на 3 барабане
        $percentArr = $this->game->getPercentArray(2);
        $prevValue = [0, 0];
        if ($percentArr[10] !== [0,0]) {
            $check = false;
        }

        // проверка выпадения монет на 4 барабане
        $percentArr = $this->game->getPercentArray(3);
        $prevValue = [0, 0];
        if ($percentArr[10] !== [0,0]) {
            $check = false;
        }

        // проверка выпадения монет на 5 барабане
        $percentArr = $this->game->getPercentArray(4);
        $prevValue = [0, 0];
        if ($percentArr[10] !== [0,0]) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест не выпадения фриспинов при отсутствии монет
     */
    public function testFallFreeSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,2,6]);

        if ($result['freeSpinData'] !== false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест не выпадения фриспинов при наличии 1 или 2 монет
     */
    public function testFallFreeSpin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,10,4,2,4,5,2,5,6,2,6]);
        if ($result['freeSpinData'] !== false) {
            $check = false;
        }

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,2,4,5,2,5,6,2,6]);
        if ($result['freeSpinData'] !== false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выпадения фриспинов при наличии 3,4,5 монет
     */
    public function testFallFreeSpin3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,10,4,5,2,5,6,2,6]);
        if ($result['freeSpinData'] === false) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,10,4,5,10,5,6,2,6]);
        if ($result['freeSpinData'] === false) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,10,4,5,10,5,10,2,6]);
        if ($result['freeSpinData'] === false) {
            $check = false;
        }
        $_SESSION['freeSpinData'] = false;

        $this->assertTrue($check);
    }

    /**
     * Тест выпадения фриспинов при наличии двух монет и одного алмаза
     */
    public function testFallFreeSpin4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);
        if ($result['freeSpinData'] === false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест проверяющий наличией 10 бесплатных вращений в фриспинах
     */
    public function testCountFreeSpinIteration1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,4,4,6,4,5,2,5,6,2,6]);
        }
        if ($_SESSION['balance'] != 99.85) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест изменения множителя в фриспинах при выигрыше на каждом ходу и отсутствии алмазов
     */
    public function testWorkFreeSpinMul()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,4,4,6,4,5,2,5,6,2,6]);

            if ($i === 8) {
                if ($_SESSION['freeSpinData']['mul'] != 2) {
                    $check = false;
                }
            }
        }

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [0,2,2,3,2,4,4,6,4,5,2,5,6,2,6]);

            if ($i === 8) {
                if ($_SESSION['freeSpinData']['mul'] != 11) {
                    $check = false;
                }
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест изменения множителя в фриспинах при выигрыше на каждом  ходу и наличии трех алмазов
     */
    public function testWorkWinFreeSpinWithMul1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,3,4,0,5,6,0,7,8,0,9,6,7,8,9]);

            if ($i === 8) {
                if ($result['freeSpinData']['mul'] !== 29) {
                    $check = false;
                }
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выигрыша за ход в фриспинах при выигрыше на каждом ходу при отсутствии алмазов
     */
    public function testWorkWinFreeSpin1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);

            if ($i === 8) {
                if ($result['allWin'] !== 0.3) {
                    $check = false;
                }
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест выигрыша за ход в фриспинах при выигрыше на каждом  ходу при наличии алмаза
     */
    public function testWorkWinFreeSpin2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,0,3,4,2,4,5,5,5,6,6,6]);

            if ($i === 8) {
                if ($result['allWin'] !== 0.15 * 10 * 2) {
                    $check = false;
                }
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест дополнительного удвоения выигрыша за линию в фриспинах при наличии в выигрышной линии 3 алмазов
     */
    public function testWorkWinFreeSpin3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = true;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [10,2,2,3,2,10,4,0,4,5,2,5,6,2,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,0,3,4,0,4,5,0,5,6,6,6]);

            if ($i === 8) {
                if ($result['allWin'] !== 1.5 * 26) {
                    $check = false;
                }
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест. Фриспины не должны начинаться при выпадении 3 алмазов
     */
    public function testFallFreeSpinGameForThreeDiamonds1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,0,3,4,0,4,5,0,5,6,6,6]);

        if ($result['freeSpinData'] !== false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi на снятие денег в основной игре
     */
    public function testWorkBridgeApi1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,0,3,4,0,4,5,0,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[0])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[0]->event_type !== "BetPlacing") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отпраки запроса к bridgeApi с результатами хода в основной игре
     * при проигрыше
     */
    public function testWorkBridgeApi2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[1])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[1]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отпраки запроса к bridgeApi с результатами хода в основной игре
     * при выигрыше
     */
    public function testWorkBridgeApi3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[1])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[1]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi с результатами хода на ходу,
     * на котором выпадают фриспины при проигрыше по линиям
     */
    public function testWorkBridgeApi4()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[1])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[1]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi на снятие денег в основной игре,
     * на котором выпадают фриспины при проигрыше по линиям
     */
    public function testWorkBridgeApi5()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[0])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[0]->event_type !== "BetPlacing") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi на снятие денег в основной игре,
     * на котором выпадают фриспины при выигрыше по линиям
     */
    public function testWorkBridgeApi6()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[0])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[0]->event_type !== "BetPlacing") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi с результатами хода на ходу,
     * на котором выпадают фриспины при выигрыше по линиям
     */
    public function testWorkBridgeApi7()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[1])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[1]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi
     * на снятие денег на первом ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi8()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);


        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[2])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[2]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi9()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);


        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[2])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[2]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi10()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);


        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[2])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[2]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода на пятом ходу
     * фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi11()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[6])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[6]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода на пятом ходу
     * фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi12()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[6])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[6]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi на снятие денег
     * на последнем ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi13()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[11])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[11]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на последнем ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi14()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[11])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[11]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на последнем ходу фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi15()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[11])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[11]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi на снятие денег
     * на первом ходу после фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi16()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[12])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[12]->event_type !== "BetPlacing") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу после фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi17()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[13])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[13]->event_type !== "Lose") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу после фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi18()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }
        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);

        $bridgeApiRequest = (new BridgeApiRequest)->all();

        if (!isset($bridgeApiRequest[13])) {
            $check = false;
            $this->assertTrue($check);
        }

        if ($bridgeApiRequest[13]->event_type !== "Win") {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест идентичности eventID при отправке запросов в основной игре
     */
    public function testWorkBridgeApi19()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);

        // for ($i=0; $i < 10; $i++) {
        //     $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        // }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        $eventID = '';
        foreach ($bridgeApiRequests as $key => $value) {
            if ($key === 0) { // получение event_id
                $eventID = $value->event_id;
            } elseif($eventID !== $value->event_id) { // проверка event_id у других записей
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест идентичности eventID при отправке запросов
     * при выпадении фриспин игры и ее прохождении
     */
    public function testWorkBridgeApi20()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;


        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        $eventID = '';
        foreach ($bridgeApiRequests as $key => $value) {
            if ($key === 0) { // получение event_id
                $eventID = $value->event_id;
            } elseif ($eventID !== $value->event_id) { // проверка event_id у других записей
                $check = false;
            }
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi с результатами хода
     * в основной игре при проигрыше
     */
    public function testWorkBridgeApi21()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,10,5,6,6,6]);

        // for ($i=0; $i < 10; $i++) {
        //     $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        // }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[0]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->direction !== "debit") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== "BetPlacing") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 0.15) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->extra_info !== 'a:1:{s:8:"selected";a:1:{i:0;i:15;}}') {
            $check = false;
        }

        if ($bridgeApiRequests[1]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== "Lose") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.0) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Dollar";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:3:"Car";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Coin";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:0;}') {
            $check = false;
        }

        $this->assertTrue($check);
    }


    /**
     * Тест данных отправляемых bridgeApi с результатами хода
     * в основной игре при выигрыше
     */
    public function testWorkBridgeApi22()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,2,5,6,6,6]);

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[0]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->direction !== "debit") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== "BetPlacing") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 0.15) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->extra_info !== 'a:1:{s:8:"selected";a:1:{i:0;i:15;}}') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=debit&eventType=BetPlacing&amount=0.15&extraInfo%5Bselected%5D%5B0%5D=15&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        if ($bridgeApiRequests[1]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== "Win") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.75) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Silver";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:6:"Silver";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:6:"Silver";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:0;}') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Win&amount=0.75&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Silver&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Silver&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Silver&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=0&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi с результатами хода на ходу,
     * на котором выпадают фриспины при проигрыше по линиям
     */
    public function testWorkBridgeApi23()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,10,3,4,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[0]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->direction !== "debit") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== "BetPlacing") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 0.15) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->extra_info !== 'a:1:{s:8:"selected";a:1:{i:0;i:15;}}') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=debit&eventType=BetPlacing&amount=0.15&extraInfo%5Bselected%5D%5B0%5D=15&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        if ($bridgeApiRequests[1]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== "Win") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.3) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:4:"Coin";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:4:"Coin";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Coin";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Win&amount=0.3&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Coin&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Coin&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Coin&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi с результатами хода на ходу,
     * на котором выпадают фриспины при выигрыше по линиям
     */
    public function testWorkBridgeApi24()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[0]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->direction !== "debit") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== "BetPlacing") {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 0.15) {
            $check = false;
        }
        if ($bridgeApiRequests[0]->extra_info !== 'a:1:{s:8:"selected";a:1:{i:0;i:15;}}') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=debit&eventType=BetPlacing&amount=0.15&extraInfo%5Bselected%5D%5B0%5D=15&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        if ($bridgeApiRequests[1]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== "Win") {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.3) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Silver";i:4;s:4:"Coin";i:5;s:6:"Dollar";i:6;s:6:"Silver";i:7;s:4:"Coin";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Coin";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Win&amount=0.3&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Silver&extraInfo%5Bresult%5D%5B4%5D=Coin&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Silver&extraInfo%5Bresult%5D%5B7%5D=Coin&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Coin&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi c результатами хода
     * на первом ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi25()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 1; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[2]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[2]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_type !== "Lose") {
            $check = false;
        }
        if ($bridgeApiRequests[2]->amount !== 0.0) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Dollar";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:3:"Car";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Ring";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Dollar&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Car&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Ring&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi c результатами хода
     * на первом ходу фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi26()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 1; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[2]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[2]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_type !== "Win") {
            $check = false;
        }
        if ($bridgeApiRequests[2]->amount !== 0.3) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Silver";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:6:"Silver";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Ring";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Win&amount=0.3&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Silver&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Silver&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Ring&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi c результатами хода
     * на последнем ходу фриспинов при выигрыше по линиям
     */
    public function testWorkBridgeApi27()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,2,3,4,2,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[11]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[11]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->event_type !== "Win") {
            $check = false;
        }
        if ($bridgeApiRequests[11]->amount !== 0.3) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Silver";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:6:"Silver";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Ring";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[11]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Win&amount=0.3&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Silver&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Silver&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Ring&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест данных отправляемых bridgeApi c результатами хода
     * на последнем ходу фриспинов при проигрыше по линиям
     */
    public function testWorkBridgeApi28()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,2,10,3,2,10,4,5,10,5,6,6,6]);

        for ($i=0; $i < 10; $i++) {
            $result = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6]);
        }

        $bridgeApiRequests = (new BridgeApiRequest)->all();

        if ($bridgeApiRequests[11]->token == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->user_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->event_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->direction !== "credit") {
            $check = false;
        }
        if ($bridgeApiRequests[11]->transaction_id == false) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->event_type !== "Lose") {
            $check = false;
        }
        if ($bridgeApiRequests[11]->amount !== 0.0) {
            $check = false;
        }
        if ($bridgeApiRequests[11]->extra_info !== 'a:3:{s:8:"selected";a:1:{i:0;d:0.15;}s:6:"result";a:15:{i:0;s:6:"Silver";i:1;s:6:"Silver";i:2;s:6:"Silver";i:3;s:6:"Dollar";i:4;s:6:"Dollar";i:5;s:6:"Dollar";i:6;s:3:"Car";i:7;s:3:"Car";i:8;s:3:"Car";i:9;s:4:"Ring";i:10;s:4:"Ring";i:11;s:4:"Ring";i:12;s:5:"Watch";i:13;s:5:"Watch";i:14;s:5:"Watch";}s:11:"featureGame";b:1;}') {
            $check = false;
        }
        if ($bridgeApiRequests[11]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=2&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=0.15&extraInfo%5Bresult%5D%5B0%5D=Silver&extraInfo%5Bresult%5D%5B1%5D=Silver&extraInfo%5Bresult%5D%5B2%5D=Silver&extraInfo%5Bresult%5D%5B3%5D=Dollar&extraInfo%5Bresult%5D%5B4%5D=Dollar&extraInfo%5Bresult%5D%5B5%5D=Dollar&extraInfo%5Bresult%5D%5B6%5D=Car&extraInfo%5Bresult%5D%5B7%5D=Car&extraInfo%5Bresult%5D%5B8%5D=Car&extraInfo%5Bresult%5D%5B9%5D=Ring&extraInfo%5Bresult%5D%5B10%5D=Ring&extraInfo%5Bresult%5D%5B11%5D=Ring&extraInfo%5Bresult%5D%5B12%5D=Watch&extraInfo%5Bresult%5D%5B13%5D=Watch&extraInfo%5Bresult%5D%5B14%5D=Watch&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест превышения лимита в обычном спине
     */
    public function testLimitWorkOnLol1()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', [1,1,1,0,0,0,0,0,0,0,0,0,1,1,1]);

        if ($result['allWin'] > 2500) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест превышения лимита в фриспинах
     */
    public function testLimitWorkOnLol2()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', [10,1,1,10,2,2,10,3,3,4,4,4,5,5,5]);
        $result2 = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]);

        if ($result2['allWin'] > 2500) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест превышения лимита в фриспинах на последнем ходу
     */
    public function testLimitWorkOnLol3()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;

        $result = $this->game->getSpinResultData(0.1, 15, 'lifeOfLuxury', [10,1,1,10,2,2,10,3,3,4,4,4,5,5,5]);
        for ($i=0; $i < 9; $i++) {
            $result2 = $this->game->getSpinResultData(0.01, 15, 'lifeOfLuxury', [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]);
        }

        if ($result2['allWin'] > 2500) {
            $check = false;
        }

        $this->assertTrue($check);
    }






    /**********************************************************************
     * ДОП ФУНКЦИИ
     *********************************************************************/
    private function prepareTest()
    {
        $this->resetSession();
        $this->clearBridgeApiRequests();
    }

    private function resetSession()
    {
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 2;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 100;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['testFreeSpinPrevAllWin'] = 0;
        $_SESSION['eventID'] = 0;

    }

    protected function clearBridgeApiRequests()
    {
        $bridgeApiRequests = (new BridgeApiRequest)->all();

        foreach ($bridgeApiRequests as $key => $value) {
            $value->delete();
        }
    }
}
