<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\GameServices\ElGalloSpinService;
use App\Models\Jackpot;
use App\Models\Stat;
use App\Models\UserJackpot;
use App\Models\Session;
use App\Models\BridgeApiRequest;
use App\Models\Log;
use Webpatser\Uuid\Uuid;

class ElGalloSpinServiceTest extends TestCase
{
    private $game;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->game = new \App\Services\GameServices\ElGalloSpinService();
    }

    /**
     * Проверка выпадения джекпота для функции
     */
    public function testJackpotFunction()
    {
        $check = true;
        $this->prepareTest();

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        $jackpot = $this->game->getJackpot();

        if ($jackpot === false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка выпадения джекпота как результата игры.
     * Линии и фриспины не выпадают
     */
    public function testJackpotLikeGameResult1()
    {
        $check = true;
        $this->prepareTest();

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        $result = $this->game->getSpinResultData(1,25,'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

        if ($result['jackpot'] === false) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка баланса при выпадения джекпота и выигрышной линии.
     */
    public function testJackpotLikeGameResult2()
    {
        $check = true;
        $this->prepareTest();

        $oldBalance = $_SESSION['balance'];

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,0,1,3,4,0,7,6,4,7,3]);

        $allWinOnLines = 0;
        foreach ($result['wl'] as $key => $value) {
            $allWinOnLines += $value;
        }

        if ($_SESSION['balance'] > ($oldBalance + $allWinOnLines - 25)) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Проверка баланса при выпадения джекпота, выигрышной линии и фриспинов.
     */
    public function testJackpotLikeGameResult3()
    {
        $check = true;
        $this->prepareTest();

        $oldBalance = $_SESSION['balance'];

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        // выпадение джекпота, выигрышной линии и фриспинов
        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [2,7,3,2,4,3,1,2,3,5,7,6,4,7,3]);

        if ($result['allWin'] != 140) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * тест выигрыша при выпадении джекпота
     * Итог: после выпадения джекпота баланс не должен менятся
     * (баланс должен менятся только после отправки доп запроса с фронта)
     */
    public function testWinJackpot1()
    {
        $check = true;
        $this->prepareTest();

        $oldBalance = $_SESSION['balance'];

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

        // после выпадения джекпота баланс не должен менятся
        if ($oldBalance != ($_SESSION['balance'] + 25)) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест создания записи в таблице user_jackpot о том, что пользователь получил джекпот
     */
    public function testWriteUserJackpotInDB()
    {
        $check = true;
        $this->prepareTest();

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 23;
        $jackpotDB->result_mini = 23;
        $jackpotDB->save();

        // пользователю на данном ходу выпадает джекпот
        $result = $this->game->getSpinResultData(1,25,'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

        // попытка получения записи из БД подтверждаующей это
        $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

        // после выпадения джекпота баланс не должен менятся
        if ($userJackpot === null) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тестирование увеличения баланса после доп запроса, который заменяет соотв. функция
     * Баланс должен увеличится
     */
    public function testGettingJackpotByUser()
    {
        $check = true;
        $this->prepareTest();

        $oldBalance = $_SESSION['balance'];

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 33;
        $jackpotDB->result_mini = 33;
        $jackpotDB->save();

        // пользователю на данном ходу выпадает джекпот
        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

        // доп запрос который делается для начисления джекпота к балансу
        $result2 = $this->game->getWinJackpot();

        // проверка наличия увелиения баланса
        if ($_SESSION['balance'] !== 108.0) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест общего выигрыша при выпадении фриспинов вместе с выигрышными линиями
     * Итог: выигрышь должен быть = 140
     */
    public function testAllWinOnWlAndStartFreeSpin()
    {
        $check = true;
        $this->prepareTest();

        $oldBalance = $_SESSION['balance'];

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [2,7,3,2,4,3,1,2,3,5,7,6,4,7,3]);

        if ($result['allWin'] != 140) {
            $check = false;
        }

        $this->assertTrue($check);
    }

    /**
     * Тест отправки запросов к апи при проигрыше
     */
    public function testSendRequestsForApiWhenLose()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['eventID'] = Uuid::generate()->string;

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

        $bridgeApiRequests = (new BridgeApiRequest)->where('event_id', '=', 'eventId-1111-1111-1111')->get();

        if ($bridgeApiRequests[0]->direction !== 'debit') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== 'BetPlacing') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 25.00) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== 'credit') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== 'Lose') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.00) {
            $check = false;
        }



        $this->assertTrue($check);
    }

    /**
     * Тест отправки запросов к апи при проигрыше
     */
    public function testSendRequestsForApiWhenWinOnLine()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['eventID'] = Uuid::generate()->string;

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

        $bridgeApiRequests = (new BridgeApiRequest)->where('event_id', '=', 'eventId-1111-1111-1111')->get();

        if ($bridgeApiRequests[0]->direction !== 'debit') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== 'BetPlacing') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 25.00) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== 'credit') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== 'Win') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 15.00) {
            $check = false;
        }



        $this->assertTrue($check);
    }

    /**
    * Тест отправки запросов к апи при выигрыше на джекпоте
    */
    public function testSendRequestsForApiWhenWinOnJackpot()
    {
        $check = true;
        $this->prepareTest();
        $_SESSION['test'] = true;
        $_SESSION['demo'] = false;
        $_SESSION['eventID'] = Uuid::generate()->string;

        // для выпадения джекпота изменяется запись в БД
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 33;
        $jackpotDB->result_mini = 33;
        $jackpotDB->save();

        $result = $this->game->getSpinResultData(1, 25, 'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);
        $result2 = $this->game->getWinJackpot();

        $bridgeApiRequests = (new BridgeApiRequest)->where('event_id', '=', 'eventId-1111-1111-1111')->get();

        if ($bridgeApiRequests[0]->direction !== 'debit') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->event_type !== 'BetPlacing') {
            $check = false;
        }
        if ($bridgeApiRequests[0]->amount !== 25.00) {
            $check = false;
        }
        if ($bridgeApiRequests[1]->direction !== 'credit') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->event_type !== 'Lose') {
            $check = false;
        }
        if ($bridgeApiRequests[1]->amount !== 0.00) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->direction !== 'credit') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_type !== 'Jackpot') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->amount !== 33.13) {
            $check = false;
        }

        if ($bridgeApiRequests[2]->direction !== 'credit') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->event_type !== 'Jackpot') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->amount !== 33.13) {
            $check = false;
        }
        if ($bridgeApiRequests[2]->extra_info !== 'a:1:{s:6:"result";a:1:{i:0;s:4:"MINI";}}') {
            $check = false;
        }
        if ($bridgeApiRequests[2]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Jackpot&amount=33.13&extraInfo%5Bresult%5D%5B0%5D=MINI&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
            $check = false;
        }

        $this->assertTrue($check);
    }

     /**
      * Тест отправки запросов к апи при выигрыше на джекпоте и линии
      */
     public function testSendRequestsForApiWhenWinOnJackpotAndLine()
     {
          $check = true;
          $this->prepareTest();
          $_SESSION['test'] = true;
          $_SESSION['demo'] = false;
          $_SESSION['eventID'] = Uuid::generate()->string;

          // для выпадения джекпота изменяется запись в БД
          $jackpotDB = (new Jackpot)->find(2);
          $jackpotDB->mini = 33;
          $jackpotDB->result_mini = 33;
          $jackpotDB->save();

          $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
          $result2 = $this->game->getWinJackpot();

          $bridgeApiRequests = (new BridgeApiRequest)->where('event_id', '=', 'eventId-1111-1111-1111')->get();

          if ($bridgeApiRequests[0]->direction !== 'debit') {
              $check = false;
          }
          if ($bridgeApiRequests[0]->event_type !== 'BetPlacing') {
              $check = false;
          }
          if ($bridgeApiRequests[0]->amount !== 25.00) {
              $check = false;
          }
          if ($bridgeApiRequests[1]->direction !== 'credit') {
              $check = false;
          }
          if ($bridgeApiRequests[1]->event_type !== 'Win') {
              $check = false;
          }
          if ($bridgeApiRequests[1]->amount !== 15.00) {
              $check = false;
          }
          if ($bridgeApiRequests[2]->direction !== 'credit') {
              $check = false;
          }
          if ($bridgeApiRequests[2]->event_type !== 'Jackpot') {
              $check = false;
          }
          if ($bridgeApiRequests[2]->amount !== 33.13) {
              $check = false;
          }



          $this->assertTrue($check);
      }

     /**
     * Тест баланса после проигрыша по линиям в demo режиме
     */
     public function testBalanceAfterLoseOnLineDemoMod()
     {
         $check = true;
         $this->prepareTest();

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         if ($result['allWin'] > 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест баланса после проигрыша по линиям в full режиме
     */
     public function testBalanceAfterLoseOnLineFullMod()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         if ($result['allWin'] > 0) {
             $check = false;
         }


         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total spin в full режиме.
     * Делается ход результатом которого является проигрышь.
     * Итог: спин в статистике увеличивается на 1
     */
     public function testStatisticsTotalSpin1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total spin в demo режиме
     * спин в статистике не увеличивается на 1
     */
     public function testStatisticsTotalSpin2()
     {
         $check = true;
         $this->prepareTest();

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count > 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Когда выпадает фриспин в full режиме,
     * то Total spin не увеличивается за все время фриспинов
     * кол-во спинов в статистике не увеличивается из-за фриспинов
     * Итог: кол-во спинов увеличивается на 1 (из-за хода на котором выпал фриспин)
     */
     public function testStatisticsTotalSpin3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $resultFS = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total spin в full режиме.
     * Делается ход результатом которого является выпадение джекпота.
     * Общее кол-во спинов в статистике увеличивается на 1.
     */
     public function testStatisticsTotalSpin4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 23;
         $jackpotDB->result_mini = 23;
         $jackpotDB->save();

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->iteration_count !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total bet в full режиме.
     * Делается ход результатом которого является выигрышь по линии.
     * Итог: Сумма ставок увеличивается после хода.
     */
     public function testStatisticsTotalBet1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet !== 25.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total bet в full режиме.
     * Находу выпадает фриспин и они полуностью прокручиваются.
     * Итог: Сумма ставок увеличивается после фриспинов только на ставку, которая была на ходу, на котором выпал фриспин
     */
     public function testStatisticsTotalBet2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         }
         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet !== 25.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total bet в demo режиме.
     * Делается ход результатом которого является проигрышь.
     * Итог: Сумма ставок не увеличивается после хода
     */
     public function testStatisticsTotalBet3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;

         // проигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet !== 0.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total bet в demo режиме.
     * Делается ход результатом которого является выигрышь по линии и выпадение фриспинов
     * Итог: Сумма ставок не увеличивается после фриспинов
     */
     public function testStatisticsTotalBet4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_bet !== 0.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Работа тестов не вносит вклад в рост jackpot (ни в demo, ни в dev)
     */
     public function testWorkNotIncreaseJackpot()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $miniOld = (new Jackpot)->where('id', '=', 1)->get()->first()->mini;

         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $miniNew = (new Jackpot)->where('id', '=', 1)->get()->first()->mini;

         if ($miniNew !== $miniOld) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является проигрышь.
     * Итог: Общий выигрышь не растет
     */
     public function testStatisticsTotalWimAfterSpin1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win !== 0.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии.
     * Итог: Общий выигрышь растет
     */
     public function testStatisticsTotalWimAfterSpin2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win !== 15.00) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии и выпадение фриспинов
     * Итог: Общий выигрышь правильно растет с учетом выигрыша на фриспинах и линии
     */
     public function testStatisticsTotalWimAfterSpin3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != (530.0 + $result['freeSpinData']['count'] * 15 * $result['freeSpinData']['mul'])) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии и выпадение джекпота
     * Итог: Общий выигрышь правильно увеличивается с учетом выигрыша на джекпоте и линии
     */
     public function testStatisticsTotalWimAfterSpin4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 23;
         $jackpotDB->result_mini = 23;
         $jackpotDB->save();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         $jackpot = $this->game->getWinJackpot();

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->sum_win != 38.13) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в demo режиме.
     * Делается ход результатом которого является выигрыш по линии
     * Итог: Общий выигрышь не растет в full записи таблицы не растет
     */
     public function testStatisticsTotalWimAfterSpin5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statOld->sum_win !== $statNew->sum_win) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в demo режиме.
     * Делается ход результатом которого является выигрыш по линии и выпадение фриспинов
     * Итог: Общий выигрышь не растет после окончания фриспинов
     */
     public function testStatisticsTotalWimAfterSpin6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statOld->sum_win !== $statNew->sum_win) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии. И пользователь играет в игру на удвоение и проигрывает
     * Итог: Общий выигрышь не растет
     */
     public function testStatisticsTotalWimAfterSpin7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $_SESSION['doubleLose'] = $_SESSION['allWin'];
         $_SESSION['allWin'] = 0;
         $this->game->writeStatisticsAfterDouble();

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->sum_win !== $statOld->sum_win) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии. И пользователь играет в игру на удвоение и выигрывает
     * Итог: Общий выигрышь растет с учетом игры на удвоение
     */
     public function testStatisticsTotalWimAfterSpin8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $_SESSION['doubleLose'] = $_SESSION['allWin'];
         $_SESSION['allWin'] = $_SESSION['doubleLose'] * 2;
         $this->game->writeStatisticsAfterDouble();

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->sum_win != 30) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в full режиме.
     * Делается ход результатом которого является выигрыш по линии. И пользователь играет в игру на удвоение и проигрывает на втором ходу
     * Итог: Общий выигрышь не растет
     */
     public function testStatisticsTotalWimAfterSpin9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $_SESSION['doubleLose'] = $_SESSION['allWin'];
         $_SESSION['allWin'] = $_SESSION['doubleLose'] * 2;
         $this->game->writeStatisticsAfterDouble();

         $_SESSION['doubleLose'] = $_SESSION['allWin'];
         $_SESSION['allWin'] = 0;
         $this->game->writeStatisticsAfterDouble();

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->sum_win !== $statOld->sum_win * 2) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Total win в demo режиме.
     * Делается ход результатом которого является выигрыш по линии. И пользователь играет в игру на удвоение и проигрывает
     * Итог: Общий выигрышь не растет
     */
     public function testStatisticsTotalWimAfterSpin10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $_SESSION['doubleLose'] = $_SESSION['allWin'];
         $_SESSION['allWin'] = 0;
         $this->game->writeStatisticsAfterDouble();

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statNew->sum_win !== $statOld->sum_win) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of winning spins в full режиме.
     * Делается ход результатом которого является проигрышь
     * Итог: кол-выигрышных спинов не увеличивается
     */
     public function testStatisticsNumberOfWinningSpins1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->number_of_winning_spins !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of winning spins в full режиме.
     * Делается ход результатом которого является выигрышь по линии
     * Итог: кол-выигрышных спинов увеличивается
     */
     public function testStatisticsNumberOfWinningSpins2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->number_of_winning_spins !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of winning spins в demo режиме.
     * Делается ход результатом которого является выигрышь по линии
     * Итог: кол-выигрышных спинов не увеличивается для full записи в таблице
     */
     public function testStatisticsNumberOfWinningSpins3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statNew->number_of_winning_spins !== $statOld->number_of_winning_spins) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of winning spins в full режиме.
     * Делается ход результатом которого является выигрышь по линии и выпадение фриспина
     * Итог: кол-выигрышных спинов увеличивается только на 1
     */
     public function testStatisticsNumberOfWinningSpins4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_winning_spins !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of losing spins в full режиме.
     * Делается ход результатом которого является выигрышь по линии.
     * Итог: кол-во lose ходов остается = 0
     */
     public function testStatisticsNumberOfLosingSpins()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->number_of_losing_spins !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of jackpots в full режиме.
     * На ходу не выпадает джекпот
     * Итог: кол-во джекпотов не изменяется
     */
     public function testStatisticsNumberOfJackpots1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($statNew->number_of_jackpots !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of jackpots в full режиме.
     * На ходу выпадает джекпот
     * Итог: кол-во джекпотов изменяется
     */
     public function testStatisticsNumberOfJackpots2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 33;
         $jackpotDB->result_mini = 33;
         $jackpotDB->save();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $this->game->getWinJackpot();

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_jackpots !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of jackpots в demo режиме.
     * На ходу выпадает джекпот
     * Итог: кол-во джекпотов не изменяется для full аписи в БД
     */
     public function testStatisticsNumberOfJackpots3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 33;
         $jackpotDB->result_mini = 33;
         $jackpotDB->save();

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         $this->game->getWinJackpot();

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statNew->number_of_jackpots != $statOld->number_of_jackpots) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of free spins в full режиме.
     * На ходу не выпадает фриспина
     * Итог: кол-во фриспинов не изменяется
     */
     public function testStatisticsNumberOfFreeSpins1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')
            ->where('demo', '=', 'yes')->get()->first();

         if ($statNew->number_of_bonus_game !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of free spins в full режиме.
     * На ходу выпадает фриспина и они прокручивается.
     * Итог: кол-во фриспинов изменяется на 1.
     */
     public function testStatisticsNumberOfFreeSpins2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->number_of_bonus_game !== 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. Number of free spins в demo режиме.
     * На ходу выпадает фриспина и они прокручивается.
     * Итог: кол-во фриспинов не изменяется для full записи в БД.
     */
     public function testStatisticsNumberOfFreeSpins3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statNew->number_of_bonus_game !== $statOld->number_of_bonus_game) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on main game в full режиме.
     * На ходу не выпадает выигрышь
     * Итог: Статистика выигрышей в основной игре не изменяется
     */
     public function testStatisticsMoneyReturnedOnMainGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game !== 0.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on main game в full режиме.
     * На ходу выпадает выигрышь по линии
     * Итог: Статистика выигрышей в основной игре изменяется
     */
     public function testStatisticsMoneyReturnedOnMainGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game !== 15.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on main game в demo режиме.
     * На ходу выпадает выигрышь по линии
     * Итог: Статистика выигрышей в основной игре не изменяется
     */
     public function testStatisticsMoneyReturnedOnMainGame3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $statOld = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $statNew = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'no')->get()->first();

         if ($statOld->money_returned_on_main_game !== $statNew->money_returned_on_main_game) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on main game в full режиме.
     * На ходу выпадает джекпот
     * Итог: Статистика выигрышей в основной игре не изменяется
     */
     public function testStatisticsMoneyReturnedOnMainGame4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 23;
         $jackpotDB->result_mini = 23;
         $jackpotDB->save();

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game !== 15.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on main game в full режиме.
     * На ходу выпадает фриспин.
     * Итог: Статистика выигрышей по основной игре не изменяется
     */
     public function testStatisticsMoneyReturnedOnMainGame5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_main_game != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on freespin в full режиме.
     * На ходу не выпадает freespin.
     * Итог: статистика денег полученных на фриспинах не изменяется
     */
     public function testStatisticsMoneyReturnedOnFreespin1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on freespin в full режиме.
     * На ходу выпадает freespin.
     * Итог: статистика денег полученных на фриспинах изменяется после прокрутки всех бесплатных ходов
     */
     public function testStatisticsMoneyReturnedOnFreespin2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [7,9,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         }

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->money_returned_on_bonus_game != (530 + $result['freeSpinData']['count'] * 15 * $result['freeSpinData']['mul'])) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on jackpots в full режиме.
     * На ходу не выпадает джекпот
     * Итог: статистика выигрышей полученных от джекотов не изменяется
     */
     public function testStatisticsMoneyReturnedOnJackpots1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->total_jackpot_winning !== 0.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест статистики. money returned on jackpots в full режиме.
     * На ходу выпадает джекпот
     * Итог: статистика выигрышей полученных от джекотов изменяется
     */
     public function testStatisticsMoneyReturnedOnJackpots2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 23;
         $jackpotDB->result_mini = 23;
         $jackpotDB->save();

         // при совершении хода джекпот должен увеличится
         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,3,7,4,3,1,4,3,5,7,6,4,7,3]);
         $this->game->getWinJackpot();

         $stat = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();

         if ($stat->total_jackpot_winning !== 23.13) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     public function testSendBridgeRequestInFreeSpin1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;


         // выпадают фриспины
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
         $bridgeApiRequests = (new BridgeApiRequest)->all()->toArray();


         if (count($bridgeApiRequests) !== 2) {
             $check = false;
         }

         $bridgeApiRequests = (new BridgeApiRequest)->all();

         foreach ($bridgeApiRequests as $key => $value) {
             $value->delete();
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка отправляемых данных через bridgeApi. У запросов c eventType = Correction
      * в extraInfo должно быть featureGame = true
      */
     public function testCorrectionFeatureTrue1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         }
         $data = $this->game->getChoiceResultData('repeat');

         $bridgeApiRequest = BridgeApiRequest::orderBy('id', 'desc')->first();

         if ($bridgeApiRequest->event_type !== 'Correction') {
             $check = false;
         }

         if (unserialize($bridgeApiRequest->extra_info)['featureGame'] !== true) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Проверка отправляемых данных через bridgeApi. У запросов c eventType = Correction
      * в extraInfo должно быть featureGame = true
      */
     public function testCorrectionFeatureTrue2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(1, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);

         for ($i = 0; $i < $result['freeSpinData']['count']; $i++) {
             $result2 = $this->game->getSpinResultData(1, 25, 'elGallo', [1,1,1,6,6,6,3,3,3,4,4,4,5,5,5]);
         }
         $data = $this->game->getChoiceResultData('random');

         $bridgeApiRequest = BridgeApiRequest::orderBy('id', 'desc')->first();

         if ($bridgeApiRequest->event_type !== 'Correction') {
             $check = false;
         }

         if (unserialize($bridgeApiRequest->extra_info)['featureGame'] !== true) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест данных отправляемых через bridgeApi.
      * В extraInfo должно быть JackpotAmount = big_daddy
      */
     public function testDataForBridgeApiJackpotAmount()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->sendBetPlacing();

         $bridgeApiRequest = BridgeApiRequest::orderBy('id', 'desc')->first();

         if ($bridgeApiRequest->event_type !== 'BetPlacing') {
             $check = false;
         }

         if (isset(unserialize($bridgeApiRequest->extra_info)['jackpotAmount']) !== true) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест правильности подсчет итогового выигрыша в фриспинах
     */
     public function testAllWinInFreeSpinForSomeValues1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $arrayData = [
             [8,6,10,8,6,5,8,11,3,3,11,7,3,8,9], //
             [7,3,11,7,11,4,4,7,3,11,8,9,7,6,3], //
             [11,8,3,8,6,3,5,7,6,6,3,7,3,1,11], //
             [8,6,9,6,8,3,3,9,6,7,6,3,6,3,7], //
             [11,5,1,3,7,10,9,6,11,8,11,3,11,8,5], //
             [4,10,11,0,3,11,8,3,6,6,7,5,7,1,6], //
             [6,7,4,8,3,11,3,7,9,9,5,7,8,7,5], //
             [7,8,6,9,6,7,9,7,4,6,8,9,4,5,3], //
             [3,7,6,8,10,7,1,8,9,4,5,6,3,9,11], //
             [0,7,10,7,10,6,7,6,3,9,7,1,8,7,1], //
             [8,7,11,9,8,3,3,8,11,11,3,7,9,7,11], //
             [6,10,11,3,8,4,3,5,8,1,6,7,1,3,7], //
             [7,3,8,1,11,9,11,3,4,8,11,9,8,1,7], //
             [3,6,8,5,1,7,9,3,6,7,9,8,9,5,6], //
             [7,8,9,7,8,9,3,6,1,8,5,7,8,1,3], //
             [7,6,11,3,1,6,6,8,3,7,11,8,7,6,3], //
             [9,3,8,10,3,9,7,3,6,1,3,11,7,6,1], //
             [7,8,10,8,7,6,1,7,11,1,6,9,6,3,1], //
             [6,3,9,9,5,6,11,10,4,6,3,11,11,5,6], //
             [3,7,11,11,3,7,8,11,7,1,3,11,8,1,11], //
             [7,4,11,3,4,9,3,11,8,3,1,5,3,1,5], //
             [3,7,8,8,11,3,8,6,4,8,11,1,1,11,5], //
             [5,6,0,3,6,8,6,9,8,3,6,9,1,9,8], //
             [8,3,1,11,3,8,4,6,8,8,9,3,7,1,3], //
             [7,6,3,6,5,7,6,3,5,7,8,9,8,6,1], //
             [11,3,5,5,0,6,4,7,8,9,8,7,7,11,5], //
             [8,7,4,6,11,4,7,6,5,11,1,6,11,8,9], //
             [6,3,8,0,4,8,8,3,6,8,1,7,3,1,7], //
             [11,7,5,11,6,9,4,5,6,8,6,5,7,9,8], //
             [11,6,7,7,5,6,6,7,11,3,8,11,9,7,1] //
         ];

         $allWin = 0;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
         $allWin += $result['allWin'];

         $_SESSION['freeSpinMul'] = 2;
         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 30;

         for ($i = 0; $i < 30; $i++) {
             $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', $arrayData[$i]);
             $allWin += $result['allWin'];
         }

         $allWin *= 10;

         if ((int) $allWin != 132) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест правильности подсчет итогового выигрыша в фриспинах
     * с большим кол-вом джокер-символов
     */
     public function testAllWinInFreeSpinForSomeValues2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $arrayData = [
             [6,3,0,5,3,0,3,0,6,6,0,3,0,5,3], // 13.1
             [0,4,1,3,0,6,0,7,1,6,3,0,0,6,3], // 8.9
             [0,6,3,0,3,5,5,0,7,5,0,6,0,3,1], // 38.1
             [0,3,6,0,7,3,3,1,4,0,3,5,0,6,5], // 7.6
             [0,5,6,3,0,4,0,6,3,0,6,5,3,5,0], // 16.7
             [0,3,5,6,4,0,4,0,6,6,0,5,5,0,3], // 21.4
             [3,0,6,6,0,1,0,4,6,0,5,6,3,0,5], // 25.2
             [3,0,1,6,0,7,3,0,4,0,5,1,1,0,3], // 21.8
             [5,0,1,0,1,3,0,3,1,0,5,3,0,6,3], // 40.6
             [1,0,6,6,0,4,4,0,5,0,3,7,0,3,6], // 8.8
             [7,5,0,7,4,0,0,7,1,0,3,6,0,5,6], // 11.1
             [3,0,6,6,4,3,0,6,3,1,5,3,5,0,3], // 6.4
             [6,0,3,6,0,4,0,3,6,0,5,6,3,1,0], // 9.1
             [0,6,1,0,3,6,3,4,6,0,5,3,0,1,3], // 24.6
             [3,0,7,6,0,3,0,3,7,1,3,0,0,5,6], // 11.3
         ];

         $allWin = 0;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [1,2,2,1,7,8,1,7,8,4,2,8,2,11,1]);
         $allWin += $result['allWin'];

         $_SESSION['freeSpinMul'] = 2;
         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 15;

         for ($i = 0; $i < 15; $i++) {
             $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', $arrayData[$i]);
             $allWin += $result['allWin'];
         }

         $allWin *= 10;

         if ((int) $allWin != 2700) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по первой линии в getWinLinesDataV2
     */
     public function testWinOnLine1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 1, 'elGallo', [5,5,5,3,5,3,1,5,1,6,6,6,7,7,7]);

         if ($result['winCellInfo'] != [false,5,false,false,5,false,false,5,false,false,false,false,false,false,false]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по первой линии в getWinLinesDataV2
     * если первый выигрышный символ джокер
     */
     public function testWinOnLine2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 1, 'elGallo', [9,0,9,3,5,3,1,5,1,6,6,6,7,7,7]);

         if ($result['winCellInfo'] != [false,0,false,false,5,false,false,5,false,false,false,false,false,false,false]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по первой линии в getWinLinesDataV2
     * если второй и третий выигрышные символы джокеры
     */
     public function testWinOnLine3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 1, 'elGallo', [9,3,9,3,0,3,1,0,1,6,6,6,7,7,7]);

         if ($result['winCellInfo'] != [false,3,false,false,0,false,false,0,false,false,false,false,false,false,false]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 4 линии
     */
     public function testWinOnLine4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 4, 'elGallo', [3,3,3,4,3,4,5,5,3,6,6,6,7,7,7]);

         if ($result['winCellInfo'] != [3,false,false,false,3,false,false,false,3,false,false,false,false,false,false]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 5 линии
     */
     public function testWinOnLine5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 5, 'elGallo', [3,3,3,4,3,4,3,5,5,6,6,6,7,7,7]);

         if ($result['winCellInfo'] != [false,false,3,false,3,false,3,false,false,false,false,false,false,false,false]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 6 линии
     */
     public function testWinOnLine6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 6, 'elGallo', [3,3,3,4,4,3,5,5,3,6,6,3,7,3,7]);

         if ($result['wl'][6] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 7 линии
     */
     public function testWinOnLine7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 7, 'elGallo', [3,3,3,3,4,4,3,5,5,3,6,6,7,3,7]);

         if ($result['wl'][7] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 8 линии
     */
     public function testWinOnLine8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 8, 'elGallo', [3,3,3,4,3,4,5,5,3,6,3,6,7,3,7]);

         if ($result['wl'][8] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 9 линии
     */
     public function testWinOnLine9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 9, 'elGallo', [3,3,3,4,3,4,3,5,5,6,3,6,7,3,7]);

         if ($result['wl'][9] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 10 линии
     */
     public function testWinOnLine10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 10, 'elGallo', [3,3,3,3,4,4,5,3,5,3,6,6,3,7,7]);

         if ($result['wl'][10] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 11 линии
     */
     public function testWinOnLine11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 11, 'elGallo', [3,3,3,4,4,3,5,3,5,6,6,3,7,7,3]);

         if ($result['wl'][11] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 12 линии
     */
     public function testWinOnLine12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 12, 'elGallo', [3,3,3,4,3,4,5,3,5,6,3,6,3,7,7]);

         if ($result['wl'][12] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 13 линии
     */
     public function testWinOnLine13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 13, 'elGallo', [3,3,3,4,3,4,5,3,5,6,3,6,7,7,3]);

         if ($result['wl'][13] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 14 линии
     */
     public function testWinOnLine14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 14, 'elGallo', [3,3,3,4,3,4,3,5,5,6,3,6,3,7,7]);

         if ($result['wl'][14] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 15 линии
     */
     public function testWinOnLine15()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 15, 'elGallo', [3,3,3,4,3,4,5,5,3,6,3,6,7,7,3]);

         if ($result['wl'][15] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 16 линии
     */
     public function testWinOnLine16()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 16, 'elGallo', [3,3,3,3,4,4,5,3,5,3,6,6,7,3,7]);

         if ($result['wl'][16] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 17 линии
     */
     public function testWinOnLine17()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 17, 'elGallo', [3,3,3,4,4,3,5,3,5,6,6,3,7,3,7]);

         if ($result['wl'][17] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 18 линии
     */
     public function testWinOnLine18()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 18, 'elGallo', [3,3,3,3,4,4,5,5,3,3,6,6,7,7,3]);

         if ($result['wl'][18] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 19 линии
     */
     public function testWinOnLine19()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 19, 'elGallo', [3,3,3,4,4,3,3,5,5,6,6,3,3,7,7]);

         if ($result['wl'][19] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 20 линии
     */
     public function testWinOnLine20()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 20, 'elGallo', [3,3,3,3,4,4,5,5,3,3,6,6,7,3,7]);

         if ($result['wl'][20] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 21 линии
     */
     public function testWinOnLine21()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 21, 'elGallo', [3,3,3,4,4,3,3,5,5,6,6,3,7,3,7]);

         if ($result['wl'][21] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 22 линии
     */
     public function testWinOnLine22()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 22, 'elGallo', [3,3,3,3,4,4,5,3,5,6,6,3,7,7,3]);

         if ($result['wl'][22] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 23 линии
     */
     public function testWinOnLine23()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 23, 'elGallo', [3,3,3,4,4,3,5,3,5,3,6,6,3,7,7]);

         if ($result['wl'][23] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 24 линии
     */
     public function testWinOnLine24()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 24, 'elGallo', [3,3,3,4,3,4,5,3,5,6,3,6,7,7,3]);

         if ($result['wl'][24] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша по 25 линии
     */
     public function testWinOnLine25()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,3,3,4,3,4,5,3,5,6,3,6,3,7,7]);

         if ($result['wl'][25] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест не возможности выпадения одинаковых символов на одном барабане
     * для второй версии алгоритма генерации поля
     */
     public function testFallSameSymbolsOnOneDrum1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $result = $this->game->excludeSameCharactersInOneDrum(1, [1,2], 2);

         if ($result === 1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения не существующих символов
     */
     public function testFallNotExistingSymbols1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // выигрышь
         $percentArr = $this->game->getPercentArr(1);

         $prevRang = [0,0];
         foreach ($percentArr as $key => $value) {
             if ($prevRang[1] !== $value[0]) {
                 $check = false;
             }
             $prevRang = $value;
         }

         if ($value[1] < 100000000) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест правильности подсчета баланса при ставке 0.01$,
     * выбранных трех линиях и последовательнысти выигрышей по разным комбинациям
     */
     public function testAccurateFloatBalance1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,3,3,3,4,4,3,5,5,6,6,6,7,7,7]);
         $result = $this->game->getSpinResultData(0.01, 3, 'elGallo', [3,3,3,3,4,4,3,5,5,6,6,6,7,7,7]);
         $result = $this->game->getSpinResultData(0.01, 7, 'elGallo', [3,3,3,3,4,4,3,5,5,6,6,6,7,7,7]);
         $result = $this->game->getSpinResultData(0.01, 1, 'elGallo', [3,3,3,4,4,4,5,5,5,6,6,6,7,7,7]);

         if ($result['balance'] !== 100.39) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест проверяющий, что выигрышь по линии умножается на ставку
     */
     public function testWinOnLineMulOnBetLine1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         // выигрышь
         $result = $this->game->getSpinResultData(0.02, 25, 'elGallo', [3,3,3,3,4,4,3,5,5,6,6,6,7,7,7]);

         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест проверяющий, что победы должны происходить на соседних барабанах,
     * начиная с самой левой катушки
     */
     public function testWinLineFromLeftDrum1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         // выигрышь
         $result = $this->game->getSpinResultData(0.02, 25, 'elGallo', [8,8,8,3,4,4,3,5,5,3,6,6,3,7,7]);

         if ($result['allWin'] !== 0.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выиграша по линиям при выпадении фриспинов
     */
     public function testWinLineWithDropFreeSpinGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         // выигрышь
         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,2,8,3,2,4,3,2,5,6,6,6,7,7,7]);

         if ($result['wl'][2] !== 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Diablo"
     */
     public function testWinOnSymbols0()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,3,3,0,4,4,0,5,5,6,6,6,7,7,7]);
         if ($result['wl'][2] !== 1.0) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,3,3,0,4,4,0,5,5,0,6,6,7,7,7]);
         if ($result['wl'][2] !== 10.0) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,3,3,0,4,4,0,5,5,0,6,6,0,7,7]);
         if ($result['wl'][2] !== 100.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Borracho"
     */
     public function testWinOnSymbols3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,2,2,3,4,4,3,5,5,6,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.15) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,2,2,3,4,4,3,5,5,3,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.2) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,2,2,3,4,4,3,5,5,3,6,6,3,7,7]);
         if ($result['wl'][2] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Sirena"
     */
     public function testWinOnSymbols4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [4,2,2,4,4,4,4,5,5,6,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [4,2,2,4,4,4,4,5,5,4,6,6,7,7,7]);
         if ($result['wl'][2] !== 2.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [4,2,2,4,4,4,4,5,5,4,6,6,4,7,7]);
         if ($result['wl'][2] !== 10.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Corazon"
     */
     public function testWinOnSymbols5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [5,2,2,5,4,4,5,5,5,6,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.4) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [5,2,2,5,4,4,5,5,5,5,6,6,7,7,7]);
         if ($result['wl'][2] !== 1.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [5,2,2,5,4,4,5,5,5,5,6,6,5,7,7]);
         if ($result['wl'][2] !== 7.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Muerte"
     */
     public function testWinOnSymbols6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [6,6,6,6,4,4,6,5,5,2,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.25) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [6,2,2,6,4,4,6,5,5,6,6,6,7,7,7]);
         if ($result['wl'][2] !== 0.4) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [6,2,2,6,4,4,6,5,5,6,6,6,6,7,7]);
         if ($result['wl'][2] !== 1.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Alacran"
     */
     public function testWinOnSymbols7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [7,6,6,7,4,4,7,5,5,2,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.2) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [7,2,2,7,4,4,7,5,5,7,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [7,2,2,7,4,4,7,5,5,7,6,6,7,3,3]);
         if ($result['wl'][2] !== 1.25) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Sol"
     */
     public function testWinOnSymbols8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [8,6,6,8,4,4,8,5,5,2,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.1) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [8,2,2,8,4,4,8,5,5,8,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.2) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [8,2,2,8,4,4,8,5,5,8,6,6,8,3,3]);
         if ($result['wl'][2] !== 0.75) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Mundo"
     */
     public function testWinOnSymbols9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [9,6,6,9,4,4,9,5,5,2,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [9,2,2,9,4,4,9,5,5,9,6,6,3,3,3]);
         if ($result['wl'][2] !== 1.0) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [9,2,2,9,4,4,9,5,5,9,6,6,9,3,3]);
         if ($result['wl'][2] !== 5.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Mundo"
     */
     public function testWinOnSymbols10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [10,6,6,10,4,4,10,5,5,2,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [10,2,2,10,4,4,10,5,5,10,6,6,3,3,3]);
         if ($result['wl'][2] !== 2.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [10,2,2,10,4,4,10,5,5,10,6,6,10,3,3]);
         if ($result['wl'][2] !== 15.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша по 3,4,5 символам "Mundo"
     */
     public function testWinOnSymbols11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [11,6,6,11,4,4,11,5,5,2,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.25) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [11,2,2,11,4,4,11,5,5,11,6,6,3,3,3]);
         if ($result['wl'][2] !== 0.5) {
             $check = false;
         }

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [11,2,2,11,4,4,11,5,5,11,6,6,11,3,3]);
         if ($result['wl'][2] !== 1.75) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша при выпадения 2 петухов
     */
     public function testAllWinOnGallo1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [2,6,6,2,4,4,5,5,5,6,6,6,3,3,3]);

         if ($result['allWin'] !== 0.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша при выпадения 3 петухов
     */
     public function testAllWinOnGallo2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [2,6,6,2,4,4,2,5,5,6,6,6,3,3,3]);

         if ($result['allWin'] !== 1.25) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша при выпадения 4 петухов
     */
     public function testAllWinOnGallo3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [2,6,6,2,4,4,2,5,5,2,6,6,3,3,3]);

         if ($result['allWin'] !== 5.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест размера выигрыша при выпадения 5 петухов
     */
     public function testAllWinOnGallo4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [2,6,6,2,4,4,2,5,5,2,6,6,2,3,3]);

         if ($result['allWin'] !== 100.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают diablo а не Borracho
     */
     public function testWinWithDiablo1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,3,3,3,4,4,4]);

         if ($result['wl'][2] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают Corazon а не diablo
     */
     public function testWinWithDiablo2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,5,3,3,4,4,4]);

         if ($result['wl'][2] !== 1.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают diablo а не 5 Sol
     */
     public function testWinWithDiablo3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,8,3,3,8,4,4]);

         if ($result['wl'][2] !== 1.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают 5 Alacran а не diablo
     */
     public function testWinWithDiablo4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,7,3,3,7,4,4]);

         if ($result['wl'][2] !== 1.25) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают 4 diablo а не 5 Sol
     */
     public function testWinWithDiablo5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,0,3,3,8,4,4]);

         if ($result['wl'][2] !== 10.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * тест выигрыга 3 diablo стоящих с 0-го барабана на линии при наличии,
     * возможности выигрыша на линии более дешевыми символами
     * Итог: выигрывают 5 Chalupa а не 4 diablo
     */
     public function testWinWithDiablo6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,0,4,4,0,5,5,0,3,3,10,4,4]);

         if ($result['wl'][2] !== 15.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Nopal" при выпадении на линии diablo
     */
     public function testWinWithDiablo7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [0,6,6,1,4,4,1,5,5,3,3,3,4,4,4]);

         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Nopal" при выпадении на линии diablo
     */
     public function testWinWithDiablo8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [1,6,6,1,4,4,0,5,5,3,3,3,4,4,4]);

         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Borracho" при выпадении на линии diablo
     */
     public function testWinWithDiablo9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [3,6,6,3,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.15) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Sirena" при выпадении на линии diablo
     */
     public function testWinWithDiablo10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [4,6,6,4,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Corazon" при выпадении на линии diablo
     */
     public function testWinWithDiablo11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [5,6,6,5,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.4) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Muerte" при выпадении на линии diablo
     */
     public function testWinWithDiablo12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [6,6,6,6,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.25) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Alacran" при выпадении на линии diablo
     */
     public function testWinWithDiablo13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [7,6,6,7,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.2) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Sol" при выпадении на линии diablo
     */
     public function testWinWithDiablo14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [8,6,6,8,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.1) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Mundo" при выпадении на линии diablo
     */
     public function testWinWithDiablo15()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [9,6,6,9,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.3) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Chalupa" при выпадении на линии одного diablo
     */
     public function testWinWithDiablo16()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [10,6,6,10,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест замены diablo символа "Calavera" при выпадении на линии одного diablo
     */
     public function testWinWithDiablo17()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.01, 25, 'elGallo', [11,6,6,11,4,4,0,5,5,1,1,1,4,4,4]);

         if ($result['wl'][2] !== 0.25) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест работоспособности игры на удвоение
     * Итог: функция возвращает ответ для фронта
     */
     public function testWorkDoubleGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $selectedCard = 'red';
         $result = $this->game->getDoubleResultData($selectedCard);

         if ($result === null) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест работоспособности игры на удвоение
     * Итог: возвращает карту диллера (dcard)
     */
     public function testWorkDoubleGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $selectedCard = 'red';
         $result = $this->game->getDoubleResultData($selectedCard);

         if ($result['dcard'] === false) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест работоспособности игры на удвоение
     * Итог: функция не вмешивается в результат выигрыша и возвращает true
     */
     public function testCheckExcessWinnings1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $selectedCard = 'red';
         $result = $this->game->checkExcessWinnings($selectedCard, true);

         if ($result !== true) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест работоспособности игры на удвоение
     * Итог: функция вмешивается в результат выигрыша и возвращает false
     */
     public function testCheckExcessWinnings2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 30000;

         $selectedCard = 'red';
         $result = $this->game->checkExcessWinnings($selectedCard, true);

         if ($result !== false) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $win = true;
         $selectedCard = 'red';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 2.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $win = false;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $win = true;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 4.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 3;

         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 3;

         $win = true;
         $selectedCard = 'red';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 2.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 3;

         $win = false;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 3;

         $win = true;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 4.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 5;

         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 5;

         $win = true;
         $selectedCard = 'red';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 2.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 5;

         $win = false;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 5;

         $win = true;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 4.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 6;

         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе цвета
     */
     public function testDoubleGame14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 6;

         $win = true;
         $selectedCard = 'red';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления выигрыша при проигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame15()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 6;

         $win = false;
         $selectedCard = 'p';
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест удвоения выигрыша при выигрыше на первом ходу
     * при выборе масти
     */
     public function testDoubleGame16()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 6;

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест возможности максимум 5-ти выигрышей подрят
     */
     public function testMax5StepForDoubleGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;
         $_SESSION['cardGameIteration'] = 0;

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $win = true;
         $selectedCard = 'p';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша когда выбрано черное и выпадае пика
     */
     public function testResultInDoubleGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $_SESSION['allWin'] = 1;

         $win = true;
         $selectedCard = 'black';
         $win = $this->game->getResultDoubleGame($selectedCard, 90);
         $_SESSION['dcard'] = 'p';
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 2.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест увеличения баланса если после выигрыша в игре на удвоение
     * совершается ход в основной игре
     */
     public function testResultInDoubleGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $_SESSION['allWin'] = 1;

         $win = true;
         $selectedCard = 'black';
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         if ($_SESSION['balance'] !== 77.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест НЕ увеличения баланса если после проигрыша в игре на удвоение
     * совершается ход в основной игре
     */
     public function testResultInDoubleGame3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $_SESSION['allWin'] = 1;

         $win = false;
         $selectedCard = 'black';
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         if ($_SESSION['balance'] !== 75.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест увеличени размера джекпота (mini, minor, major, big_daddy)
     * при совершении хода на минимальную ставку и одной выбранной линии
     */
     public function testJackpotWork1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->mini = 10;
         $jackpotDBOld->result_mini = 10;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.01, 1, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);

         if ($jackpotDB->mini !== 10.00005) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест увеличени размера джекпотов (mini, minor, major, big_daddy)
     * при совершении хода на максимальную ставку и 25-ти выбранных линиях
     */
     public function testJackpotWork2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->mini = 10;
         $jackpotDBOld->result_mini = 23;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.25, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);

         if ($jackpotDB->mini !== 10.03125) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест не выпадения джекпота при ставке < 0.5$
     */
     public function testJackpotWork3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->mini = 23;
         $jackpotDBOld->result_mini = 23;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.01, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

         if ($userJackpot !== null) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения джекпота при ставке > 0.5$
     */
     public function testJackpotWork4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->mini = 23;
         $jackpotDBOld->result_mini = 23;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

         if ($userJackpot === null) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения джекпота minor
     */
     public function testJackpotWork5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->minor = 50;
         $jackpotDBOld->result_minor = 50;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

         if ($userJackpot->jackpot_type !== 'minor') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения джекпота major
     */
     public function testJackpotWork6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->major = 250;
         $jackpotDBOld->result_major = 250;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

         if ($userJackpot->jackpot_type !== 'major') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения джекпота big_daddy
     */
     public function testJackpotWork7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->big_daddy = 250;
         $jackpotDBOld->result_big_daddy = 250;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();

         if ($userJackpot->jackpot_type !== 'big_daddy') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест получения пользователем джекпота при его выпадении в основной игре
     */
     public function testJackpotWork8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->big_daddy = 250;
         $jackpotDBOld->result_big_daddy = 250;
         $jackpotDBOld->save();


         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // получение джекпота
         $this->game->getWinJackpot();

         // контрольный ход
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();
         if ($userJackpot !== null) {
             $check = false;
         }
         if ($_SESSION['balance'] !== 345.01) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест получения пользователем джекпота при его выпадении после фриспинов
     */
     public function testJackpotWork9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->big_daddy = 250;
         $jackpotDBOld->result_big_daddy = 250;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,5,5,5,4,7,3]);

         $_SESSION['freeSpinData']['count'] = 10;
         for ($i=0; $i < 11; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);
         }

         // получение джекпота
         $this->game->getWinJackpot();

         $userJackpot = (new UserJackpot)->where('user_id', '=', $_SESSION['userId'])->get()->first();
         if ($userJackpot !== null) {
             $check = false;
         }
         if ($_SESSION['balance'] !== 345.01) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления накомленного джекпота mini после его выпадения
     */
     public function testJackpotWork10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->mini = 23;
         $jackpotDBOld->result_mini = 23;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // получение джекпота
         $this->game->getWinJackpot();

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->mini !== 10.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления накомленного джекпота minor после его выпадения
     */
     public function testJackpotWork11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->minor = 60;
         $jackpotDBOld->result_minor = 60;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // получение джекпота
         $this->game->getWinJackpot();

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->minor !== 50.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления накомленного джекпота major после его выпадения
     */
     public function testJackpotWork12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->major = 260;
         $jackpotDBOld->result_major = 260;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // получение джекпота
         $this->game->getWinJackpot();

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->major !== 250.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест обнуления накомленного джекпота big_daddy после его выпадения
     */
     public function testJackpotWork13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDBOld = (new Jackpot)->find(2);
         $jackpotDBOld->big_daddy = 560;
         $jackpotDBOld->result_big_daddy = 560;
         $jackpotDBOld->save();

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         // получение джекпота
         $this->game->getWinJackpot();

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->big_daddy !== 500.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест минимального и максимально возможного значения джекпота mini
     */
     public function testJackpotWork14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->min_mini !== 10.0) {
             $check = false;
         }
         if ($jackpotDBOld->max_mini !== 40.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест минимального и максимально возможного значения джекпота minor
     */
     public function testJackpotWork15()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->min_minor !== 50.0) {
             $check = false;
         }
         if ($jackpotDBOld->max_minor !== 200.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест минимального и максимально возможного значения джекпота major
     */
     public function testJackpotWork16()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->min_major !== 250.0) {
             $check = false;
         }
         if ($jackpotDBOld->max_major !== 500.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест минимального и максимально возможного значения джекпота big_daddy
     */
     public function testJackpotWork17()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;
         $_SESSION['simulation'] = true;

         $jackpotDBOld = (new Jackpot)->find(2);

         if ($jackpotDBOld->min_big_daddy !== 500.0) {
             $check = false;
         }
         if ($jackpotDBOld->max_big_daddy !== 1000.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест не возможности выпадения бонусной игры при ставке < 0.25$
     */
     public function testWorkFreeSpinGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['bet'] = 0.01;

         $valuesForCells = $this->game->excludeGalloIfNotEnoughBet([2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]);

         $countBonusSymbol = 0;
         for ($i=0; $i < 15; $i++) {
             if ($valuesForCells[$i] === 2) {
                 $countBonusSymbol += 1;
             }
         }

         if ($countBonusSymbol > 2) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест не выпадения фриспинов при отсутствии петухов
     */
     public function testWorkFreeSpinGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,4,7,3]);

         if ($_SESSION['freeSpinData'] !== false) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест не выпадения фриспинов при наличии 1 или 2 петухов
     */
     public function testWorkFreeSpinGame3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(1, 25, 'elGallo', [2,7,7,2,4,4,1,1,1,5,5,5,4,7,3]);

         if ($_SESSION['freeSpinData'] !== false) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения фриспинов при наличии 3 петухов
     */
     public function testWorkFreeSpinGame4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,5,5,5,4,7,3]);

         if ($_SESSION['freeSpinData'] === false) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения фриспинов при наличии 4 петухов
     */
     public function testWorkFreeSpinGame5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,4,7,3]);

         if ($_SESSION['freeSpinData'] === false) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выпадения фриспинов при наличии 5 петухов
     */
     public function testWorkFreeSpinGame6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.25, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         if ($_SESSION['freeSpinData'] === false) {
              $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест проверяющий соответсвие кол-ва выпавших бесплатных вращений
     * и кол-во бесплатно прокрученных спинов
     */
     public function testWorkFreeSpinGame7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $countFreeSpins = $_SESSION['freeSpinData']['count'];

         for ($i=0; $i < $countFreeSpins; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,5,4,4,1,1,1,5,5,5,3,7,3]);

             if ($_SESSION['balance'] !== 97.5) {
                 $check = false;
             }
         }

         $this->assertTrue($check);
     }

     /**
     * Тест возможных кол-в кручений фриспинов: 10, 15, 20, 25, 30
     */
     public function testWorkFreeSpinGame8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $countFreeSpins = $_SESSION['freeSpinData']['count'];

         for ($i=0; $i < $countFreeSpins; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,5,4,4,1,1,1,5,5,5,3,7,3]);

             if ($_SESSION['balance'] !== 97.5) {
                 $check = false;
             }
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша в фриспинах при множителе > 1
     */
     public function testWorkFreeSpinGame9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);

             if ($result['allWin'] !== 18.0) {
                 $check = false;
             }
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрыша в фриспинах при отсутвии выигрышей на всех ходах
     */
     public function testWorkFreeSpinGame10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [3,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);

             if ($result['allWin'] !== 0.0) {
                 $check = false;
             }
         }

         $this->assertTrue($check);
     }

     /**
     * Тест выигрша в фриспинах при выигрыше на всех ходах
     */
     public function testWorkFreeSpinGame11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);

             if ($result['allWin'] === 0.0) {
                 $check = false;
             }
         }

         $this->assertTrue($check);
     }

     /**
     * Тест получения выигрыша в после окончания фриспинов
     * при выборе "keep your prize"
     */
     public function testWorkFreeSpinGame12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);
         }

         $this->game->getChoiceResultData('get');

         if ($result['allWin'] === 1277.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест бесплатных кручений при выборе после окончания фрипинов "повтора"
     */
     public function testWorkFreeSpinGame13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);
         }

         $this->game->getChoiceResultData('repeat');

         if ($_SESSION['freeSpinData']['allWin'] !== 1000.0) {
             $check = false;
         }
         if ($_SESSION['freeSpinData']['repeat'] !== true) {
             $check = false;
         }

         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,1,5,5,1,7,3]);
         }

         if ($_SESSION['balance'] !== 97.5) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест взымания платы за ход после окончания фриспинов
     */
     public function testWorkFreeSpinGame14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,2,5,5,2,7,3]);

         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,7,3]);
         }

         $this->game->getChoiceResultData('get');

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,1,4,4,1,1,1,1,5,5,1,7,3]);

         if ($_SESSION['balance'] !== 1275.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }


     /**
     * Тест отпраки запроса к bridgeApi с результатами хода
     * в основной игре при проигрыше
     */
     public function testBridgeApi2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[1]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:6:"Sirena";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:0;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Sirena&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=0&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отпраки запроса к bridgeApi с результатами хода в основной игре при выигрыше
     */
     public function testBridgeApi3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[1]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->amount !== 9.0) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Nopal";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:0;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=9&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Nopal&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=0&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi на снятие денег в основной игре,
     * на котором выпадают фриспины при проигрыше по линиям
     */
     public function testBridgeApi4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,4,4,2,1,1,5,5,5,3,3,3]);
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[1]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->amount !== 12.5) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Gallo";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Gallo";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Gallo";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=12.5&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Gallo&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Gallo&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Gallo&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi с результатами хода на ходу,
     * на котором выпадают фриспины при выигрыше по линиям
     */
     public function testBridgeApi6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[1]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->amount !== 18.5) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Gallo";i:1;s:5:"Nopal";i:2;s:7:"Alacran";i:3;s:5:"Gallo";i:4;s:5:"Nopal";i:5;s:6:"Sirena";i:6;s:5:"Gallo";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=18.5&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Gallo&extraInfo%5Bresult%5D%5B1%5D=Nopal&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Gallo&extraInfo%5Bresult%5D%5B4%5D=Nopal&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Gallo&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi на снятие денег
     * на первом ходу фриспинов при проигрыше по линиям
     */
     public function testBridgeApi7()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);
             break;
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[2]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[2]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[2]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:6:"Sirena";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Sirena&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу фриспинов при выигрыше по линиям
     */
     public function testBridgeApi8()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
             break;
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[2]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[2]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->amount !== 9.0 * $_SESSION['freeSpinData']['mul']) {
             $check = false;
         }
         if ($bridgeApiRequest[2]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Nopal";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[2]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=9&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Nopal&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на пятом ходу фриспинов выигрыше по линиям
     */
     public function testBridgeApi9()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);

             if ($i === 4) {
                 break;
             }
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[6]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[6]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->amount !== 9.0 * $_SESSION['freeSpinData']['mul']) {
             $check = false;
         }
         if ($bridgeApiRequest[6]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Nopal";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=9&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Nopal&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на пятом ходу фриспинов при проигрыше по линиям
     */
     public function testBridgeApi10()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);

             if ($i === 4) {
                 break;
             }
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[6]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[6]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[6]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[6]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на последнем ходу фриспинов выигрыше по линиям
     */
     public function testBridgeApi11()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[11]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[11]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->amount !== 9.0 * $_SESSION['freeSpinData']['mul']) {
             $check = false;
         }
         if ($bridgeApiRequest[11]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Nopal";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=9&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Nopal&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на последнем ходу фриспинов при проигрыше по линиям
     */
     public function testBridgeApi12()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[11]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[11]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[11]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:1;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[11]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=1&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi c результатами хода
     * на первом ходу после фриспинов при выирыше по линиям
     */
     public function testBridgeApi13()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'] + 1;
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[12]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_type !== 'Win') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->amount !== 9.0) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:5:"Nopal";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:0;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Win&amount=9&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Nopal&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=0&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi на снятие денег
     * на первом ходу после фриспинов при проигрыше по линиям
     */
     public function testBridgeApi14()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'] + 1;
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[12]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:2.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:5:"Nopal";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:0;s:13:"jackpotAmount";d:500.03;}') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=2.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Nopal&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=0&extraInfo%5BjackpotAmount%5D=500.03&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест данных отправляемых в запросе при выпадении джекпота в основной игре
     * при проигрыше по линиям
     */
     public function testBridgeApi15()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // для выпадения джекпота изменяется запись в БД
         $jackpotDB = (new Jackpot)->find(2);
         $jackpotDB->mini = 23;
         $jackpotDB->result_mini = 23;
         $jackpotDB->save();

         $this->game->getSpinResultData(0.3, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[1]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->event_type !== 'Lose') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->amount !== 0.0) {
             $check = false;
         }
         if ($bridgeApiRequest[1]->extra_info !== 'a:4:{s:8:"selected";a:1:{i:0;d:7.5;}s:6:"result";a:15:{i:0;s:7:"Alacran";i:1;s:7:"Alacran";i:2;s:7:"Alacran";i:3;s:6:"Sirena";i:4;s:6:"Sirena";i:5;s:6:"Sirena";i:6;s:5:"Nopal";i:7;s:5:"Nopal";i:8;s:5:"Nopal";i:9;s:7:"Corazon";i:10;s:7:"Corazon";i:11;s:7:"Corazon";i:12;s:8:"Borracho";i:13;s:8:"Borracho";i:14;s:8:"Borracho";}s:11:"featureGame";b:0;s:13:"jackpotAmount";d:500.08;}') {
             $check = false;
         }
         if ($bridgeApiRequest[1]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Lose&amount=0&extraInfo%5Bselected%5D%5B0%5D=7.5&extraInfo%5Bresult%5D%5B0%5D=Alacran&extraInfo%5Bresult%5D%5B1%5D=Alacran&extraInfo%5Bresult%5D%5B2%5D=Alacran&extraInfo%5Bresult%5D%5B3%5D=Sirena&extraInfo%5Bresult%5D%5B4%5D=Sirena&extraInfo%5Bresult%5D%5B5%5D=Sirena&extraInfo%5Bresult%5D%5B6%5D=Nopal&extraInfo%5Bresult%5D%5B7%5D=Nopal&extraInfo%5Bresult%5D%5B8%5D=Nopal&extraInfo%5Bresult%5D%5B9%5D=Corazon&extraInfo%5Bresult%5D%5B10%5D=Corazon&extraInfo%5Bresult%5D%5B11%5D=Corazon&extraInfo%5Bresult%5D%5B12%5D=Borracho&extraInfo%5Bresult%5D%5B13%5D=Borracho&extraInfo%5Bresult%5D%5B14%5D=Borracho&extraInfo%5BfeatureGame%5D=0&extraInfo%5BjackpotAmount%5D=500.08&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }


     /**
     * Тест отправки запроса к bridgeApi
     * о сделанной ставке пользователем в игре на удвоение при проигрыше
     */
     public function testBridgeApi17()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['allWin'] = 1;

         $_SESSION['doubleLose'] = round($_SESSION['allWin'], 2);
         if ($_SESSION['checkStartDouble'] === false) {
             $_SESSION['savedStartAllWinForDouble'] = round($_SESSION['allWin'], 2);
         }
         // вычисление результата хода
         $win = false;
         $selectedCard = 'red';
         // получение рандомного ответа в соответствии с рандомно полученным результатом
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         // проверка превышения максимального выигрыша
         $win = $this->game->checkExcessWinnings($selectedCard, $win);
         // изменение выигрыша
         $this->game->changeAllWinAfterDouble($selectedCard, $win);
         $responseData = [
             'win' => $win,
             'allWin' => $_SESSION['allWin'],
             'dcard' => $dcard,
             'count' => $_SESSION['cardGameIteration'],
             'selected_for_api' => [$this->game->getCardSuit($selectedCard)],
             'result_for_api' => ['Spades'],
         ];
         // отправка запроса к bridgeApi
         $this->game->sendDoubleMoveFunds($responseData);

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[0]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[0]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->direction !== 'debit') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->event_type !== 'Raise') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->amount !== 1.0) {
             $check = false;
         }
         if ($bridgeApiRequest[0]->extra_info !== 'a:2:{s:8:"selected";a:1:{i:0;s:3:"Red";}s:6:"result";a:1:{i:0;s:6:"Spades";}}') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=debit&eventType=Raise&amount=1&extraInfo%5Bselected%5D%5B0%5D=Red&extraInfo%5Bresult%5D%5B0%5D=Spades&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi
     * о сделанной ставке пользователем в игре на удвоение при выигрыше
     */
     public function testBridgeApi18()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;
         $_SESSION['allWin'] = 1;

         $_SESSION['doubleLose'] = round($_SESSION['allWin'], 2);
         if ($_SESSION['checkStartDouble'] === false) {
             $_SESSION['savedStartAllWinForDouble'] = round($_SESSION['allWin'], 2);
         }
         // вычисление результата хода
         $win = true;
         $selectedCard = 'black';
         // получение рандомного ответа в соответствии с рандомно полученным результатом
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         // проверка превышения максимального выигрыша
         $win = $this->game->checkExcessWinnings($selectedCard, $win);
         // изменение выигрыша
         $this->game->changeAllWinAfterDouble($selectedCard, $win);
         $responseData = [
             'win' => $win,
             'allWin' => $_SESSION['allWin'],
             'dcard' => $dcard,
             'count' => $_SESSION['cardGameIteration'],
             'selected_for_api' => [$this->game->getCardSuit($selectedCard)],
             'result_for_api' => ['Spades'],
         ];
         // отправка запроса к bridgeApi
         $this->game->sendDoubleMoveFunds($responseData);

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[0]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[0]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->direction !== 'credit') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->event_type !== 'Raise') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->amount !== 1.0) {
             $check = false;
         }
         if ($bridgeApiRequest[0]->extra_info !== 'a:2:{s:8:"selected";a:1:{i:0;s:5:"Black";}s:6:"result";a:1:{i:0;s:6:"Spades";}}') {
             $check = false;
         }
         if ($bridgeApiRequest[0]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Raise&amount=1&extraInfo%5Bselected%5D%5B0%5D=Black&extraInfo%5Bresult%5D%5B0%5D=Spades&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi о выборе "забрать приз" после окончания фриспинов
     */
     public function testBridgeApi19()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $this->game->getChoiceResultData('get');

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if (isset($bridgeApiRequest[12]) !== false) {
             $check = false;
         }

         // if ($bridgeApiRequest[12]->token !== 'token-1111-1111-1111') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->user_id !== 1) {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->event_id !== 'eventId-1111-1111-1111') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->direction !== 'credit') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->transaction_id !== 'transactionId-1111-1111-1111') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->event_type !== 'Raise') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->amount !== 1.0) {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->extra_info !== 'a:2:{s:8:"selected";a:1:{i:0;s:5:"Black";}s:6:"result";a:1:{i:0;s:6:"Spades";}}') {
         //     $check = false;
         // }
         // if ($bridgeApiRequest[12]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=credit&eventType=Raise&amount=1&extraInfo%5Bselected%5D%5B0%5D=Black&extraInfo%5Bresult%5D%5B0%5D=Spades&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
         //     $check = false;
         // }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi о выборе "рандомный приз"
     * после окончания фриспинов
     */
     public function testBridgeApi20()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $this->game->getChoiceResultData('random');

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[12]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->direction !== $bridgeApiRequest[12]->direction) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_type !== 'Correction') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->amount !== $bridgeApiRequest[12]->amount) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->extra_info !== 'a:1:{s:11:"featureGame";b:1;}') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=' . $bridgeApiRequest[12]->direction . '&eventType=Correction&amount=' . $bridgeApiRequest[12]->amount . '&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест отправки запроса к bridgeApi
     * о выборе "повторить фриспины" после окончания фриспинов
     */
     public function testBridgeApi21()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }
         $this->game->getChoiceResultData('repeat');

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[12]->token !== 'token-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->user_id !== 1) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_id !== 'eventId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->direction !== 'debit') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->transaction_id !== 'transactionId-1111-1111-1111') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->event_type !== 'Correction') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->amount !== 90.0) {
             $check = false;
         }
         if ($bridgeApiRequest[12]->extra_info !== 'a:1:{s:11:"featureGame";b:1;}') {
             $check = false;
         }
         if ($bridgeApiRequest[12]->requestURL !== 'https://play777games.com/moveFunds?token=token-1111-1111-1111&userId=1&gameId=1&direction=debit&eventType=Correction&amount=90&extraInfo%5BfeatureGame%5D=1&transactionId=transactionId-1111-1111-1111&eventId=eventId-1111-1111-1111') {
             $check = false;
         }

         $this->assertTrue($check);
     }


     /**
     * Тест идентичности eventID при отправке запросов в основной игре
     */
     public function testBridgeApi22()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
             $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();
             if ($bridgeApiRequest[$i + 1]->event_id !== $bridgeApiRequest[$i]->event_id) {
                 $check = false;
             }
         }


         $this->assertTrue($check);
     }

     /**
     * Тест идентичности eventID при отправке запросов
     * в основной игре
     */
     public function testBridgeApi23()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $bridgeApiRequest = (new BridgeApiRequest)->where('token', '=', 'token-1111-1111-1111')->get();

         if ($bridgeApiRequest[0]->event_id !== $bridgeApiRequest[1]->event_id) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест выигрыша на 2-х петухах в фриспинах с mul = 2
      */
     public function testDropGalloInFreeSpinGame1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;
         $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         if ($result2['allWin'] !== 10.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест выигрыша на 2-х петухах в на последнем ходу фриспинов с mul = 2
      */
     public function testDropGalloInFreeSpinGame2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;

         for ($i=0; $i < 10; $i++) {
             $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         }

         if ($result2['allWin'] !== 10.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест выигрыша на 3-х петухах в фриспинах с mul = 2
      */
     public function testDropGalloInFreeSpinGame3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;
         $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         if ($result2['allWin'] !== 25.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест выигрыша на 4-х петухах в фриспинах с mul = 2
      */
     public function testDropGalloInFreeSpinGame4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;


         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;

         $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,2,5,5,3,3,3]);

         if ($result2['allWin'] !== 100.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест выигрыша на 5-х петухах в фриспинах с mul = 2
      */
     public function testDropGalloInFreeSpinGame5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;


         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;

         $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,2,5,5,2,3,3]);

         if ($result2['allWin'] !== 2000.0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест Не начинания поновой фриспинх игры при выпадении в фриспинах 3,4,5 петухов
      */
     public function testDropGalloInFreeSpinGame6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $_SESSION['freeSpinData']['mul'] = 2;
         $_SESSION['freeSpinData']['count'] = 10;
         $result2 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,2,5,5,2,3,3]);
         $result3 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,2,5,5,3,3,3]);
         $result4 = $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,7,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         if ($result2['rope'] !== false) {
             $check = false;
         }
         if ($result3['rope'] !== false) {
             $check = false;
         }
         if ($result4['rope'] !== false) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест баланса после фриспинов после выбора get
     */
     public function testDoubleAfterFreeSpins1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('get');

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] !== ($allWin + 100 - 5)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест баланса после фриспинов после выбора random
     */
     public function testDoubleAfterFreeSpins2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('random');

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] != ($allWin + 100 - 5)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест ход в игре на удвоение с проигрышем после фриспинов после выбора get
     */
     public function testDoubleAfterFreeSpins3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('get');

         // проигрышь в игре на удвоение
         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] != (100 - 5)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест ход в игре на удвоение с выигрышем после фриспинов после выбора get
     */
     public function testDoubleAfterFreeSpins4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('get');

         // проигрышь в игре на удвоение
         $selectedCard = 'red';
         $win = true;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] != (100 - 5 + $allWin * 2)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест ход в игре на удвоение с проигрышем после фриспинов после выбора random
     */
     public function testDoubleAfterFreeSpins5()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('random');

         // проигрышь в игре на удвоение
         $selectedCard = 'red';
         $win = false;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] != (100 - 5)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест ход в игре на удвоение с выигрышем после фриспинов после выбора random
     */
     public function testDoubleAfterFreeSpins6()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = true;

         $this->game->getSpinResultData(0.1, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);

         $_SESSION['freeSpinData']['mul'] = 1;
         $_SESSION['freeSpinData']['count'] = 10;
         $count = $_SESSION['freeSpinData']['count'];
         for ($i=0; $i < $count; $i++) {
             $this->game->getSpinResultData(0.1, 25, 'elGallo', [1,7,7,1,4,4,1,1,1,5,5,5,3,3,3]);
         }

         // получение денег от фриспинов
         $allWin = $this->game->getChoiceResultData('random');

         // проигрышь в игре на удвоение
         $selectedCard = 'red';
         $win = true;
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         $result = $this->game->changeAllWinAfterDouble($selectedCard, $win);

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.1, 25, 'elGallo', [7,7,7,4,4,4,1,1,1,5,5,5,3,3,3]);

         if ($_SESSION['balance'] != (100 - 5 + $allWin * 2)) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест проверяющий правильность подсчета баланса в ходе full игры
     */
     // public function testCorrectBridgeApiAllWin1()
     // {
     //     if ($_SERVER['DB_PASSWORD'] !== 'root') {
     //         $check = true;
     //
     //         $allLogs = (new Log)->all();
     //
     //         $logs = [];
     //         foreach ($allLogs as $key => $log) {
     //             $data = json_decode($log->data);
     //
     //             if ($data->userId === 4) {
     //                 $logs[] = $data;
     //             }
     //         }
     //
     //         $curBalance = 0;
     //         $prevBalance = 0;
     //         $curAllWin = 0;
     //         $prevAllWin = 0;
     //         $dropFeatureGame = false;
     //         $startFeatureGameBalance = 0;
     //         $startFeatureGameAllWin = 0;
     //         $allWinOnFeatureGame = 0;
     //         foreach ($logs as $key => $log) {
     //             if ($key !== 0) { // проверка
     //                 $needBalance = (string)(($prevBalance + $prevAllWin - 0.5) * 100);
     //                 $curBalance = (string)($log->balance * 100);
     //
     //                 // обычный ход в основной игре
     //                 if ($log->action === 'spin' && $log->featureGame === false) {
     //                     if ($curBalance !== $needBalance) {
     //                         $check = false;
     //                         dd('curBalance * 100 = ' . $curBalance . '; needBalance * 100 = ' . $needBalance . '; action = ' . $log->action . '; foreachKey = ' . $key);
     //                     }
     //
     //                     $prevBalance = $log->balance;
     //                     $prevAllWin = $log->allWin;
     //                 }
     //
     //                 // игра на удвоение
     //                 if ($log->action === 'double') {
     //                     $prevAllWin = $log->allWin;
     //                 }
     //
     //                 // начало фриспин игры
     //                 if ($log->action === 'spin' && $log->featureGame === true && $dropFeatureGame === false) {
     //                     $startFeatureGameBalance = $log->balance;
     //                     $startFeatureGameAllWin = $log->allWin;
     //                     $allWinOnFeatureGame = $log->allWin;
     //                     $dropFeatureGame = true;
     //                 } elseif ($log->action === 'spin' && $log->featureGame === true && $dropFeatureGame === true) {
     //                     // бесплатные кручения в featureGame
     //                     $allWinOnFeatureGame += $log->allWin;
     //                 }
     //
     //                 if ($log->action === 'jackpot') {
     //                     $prevBalance += $prevAllWin;
     //                     $prevAllWin = $log->allWin;
     //                 }
     //
     //                 // выбор после фриспинов
     //                 if ($log->action === 'choice') {
     //                     $dropFeatureGame = false;
     //
     //                     if ($log->actionType === 'get') {
     //                         $prevAllWin = $allWinOnFeatureGame;
     //                         $prevBalance = $startFeatureGameBalance;
     //                         $dropFeatureGame = false;
     //                     }
     //
     //                     if ($log->actionType === 'random') {
     //                         $prevAllWin = $log->randWin;
     //                         $prevBalance = $startFeatureGameBalance;
     //                         $dropFeatureGame = false;
     //                     }
     //
     //                     if ($log->actionType === 'repeat') {
     //                         $dropFeatureGame = true;
     //                         $allWinOnFeatureGame = $startFeatureGameAllWin;
     //                     }
     //                 }
     //
     //             } else {
     //                 $prevBalance = $log->balance;
     //                 $prevAllWin = $log->allWin;
     //             }
     //         }
     //
     //         dd('curBalance * 100 = ' . $curBalance . '; needBalance * 100 = ' . $needBalance);
     //
     //         if ($startBalance != (100 - 5 + $allWin * 2)) {
     //             $check = false;
     //         }
     //
     //         $this->assertTrue($check);
     //     }
     // }

     /**
     * Тест работы лимита для обычных спинов
     */
     public function testAllWinOverLimit1()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // делается дополнительный ход для начисления баланса
         $result = $this->game->getSpinResultData(0.25, 25, 'elGallo', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);

         if ($result['allWin'] > 2500 || $result['info'] === [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
     * Тест работы лимита для фриспинов спинов
     */
     public function testAllWinOverLimit2()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         // делается дополнительный ход для начисления баланса
         $this->game->getSpinResultData(0.25, 25, 'elGallo', [2,1,7,2,1,4,2,1,1,5,5,5,3,3,3]);
         $result = $this->game->getSpinResultData(0.25, 25, 'elGallo', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);

         if ($result['allWin'] > 2500 || $result['info'] === [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест работы лимита для удвоения при выборе цвета
      */
     public function testAllWinOverLimit3()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [10,10,10,10,10,10,10,10,10,10,10,10,10,1,1]);

         // выигрышь в игре на удвоение
         $win = true;
         $selectedCard = 'red';
         // получение рандомного ответа в соответствии с рандомно полученным результатом
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         // проверка превышения максимального выигрыша
         $win = $this->game->checkExcessWinnings($selectedCard, $win);
         // изменение выигрыша
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }

     /**
      * Тест работы лимита для удвоения при выборе масти
      */
     public function testAllWinOverLimit4()
     {
         $check = true;
         $this->prepareTest();
         $_SESSION['test'] = true;
         $_SESSION['demo'] = false;

         $result = $this->game->getSpinResultData(0.1, 25, 'elGallo', [10,10,10,10,10,10,10,10,10,10,10,10,1,1,1]);

         // выигрышь в игре на удвоение
         $win = true;
         $selectedCard = 'b';
         // получение рандомного ответа в соответствии с рандомно полученным результатом
         $dcard = $this->game->getRandomCardValue($selectedCard, $win);
         // проверка превышения максимального выигрыша
         $win = $this->game->checkExcessWinnings($selectedCard, $win);
         // изменение выигрыша
         $this->game->changeAllWinAfterDouble($selectedCard, $win);

         if ($_SESSION['allWin'] !== 0) {
             $check = false;
         }

         $this->assertTrue($check);
     }


    /**********************************************************************
     * ДОП ФУНКЦИИ
     *********************************************************************/







    /**
     * Подготовительные рабаты для теста
     */
    private function prepareTest()
    {
        $this->resetSession();
        $this->resetSessionTableInDB();
        $this->resetJackpot();
        $this->resetUserJackpot();
        $this->clearTableBridgeApiRequests();
        $this->clearTableStats();
    }

    private function clearTableStats()
    {
        $stats = (new Stat)->where('name', '=', 'elgallo')->where('demo', '=', 'yes')->get()->first();
        $stats->iteration_count = 0;
        $stats->sum_bet = 0.00;
        $stats->sum_win = 0.00;
        $stats->number_of_winning_spins = 0.00;
        $stats->number_of_losing_spins = 0.00;
        $stats->number_of_jackpots = 0.00;
        $stats->number_of_bonus_game = 0.00;
        $stats->money_returned_on_main_game = 0.00;
        $stats->money_returned_on_bonus_game = 0.00;
        $stats->money_returned_on_jackpots = 0.00;
        $stats->total_jackpot_winning = 0.00;
        $stats->save();
    }

    /**
    * Создание и подготовка переменных в сессии, которые нужны для работы игры
    */
    private function resetSession()
    {
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 1;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 100;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['testFreeSpinPrevAllWin'] = 0;
        $_SESSION['eventID'] = 0;
        $_SESSION['checkStartDouble'] = false;
        $_SESSION['cardGameIteration'] = 0;
    }

    /**
    * Приведение джекпота к моменту когда он должен выпасть на следующем ходу.
    * Изменяются значения только demo джекпота
    */
    private function resetJackpot()
    {
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 10;
        $jackpotDB->result_mini = 39;
        $jackpotDB->minor = 50;
        $jackpotDB->result_minor = 199;
        $jackpotDB->major = 250;
        $jackpotDB->result_major = 499;
        $jackpotDB->big_daddy = 500;
        $jackpotDB->result_big_daddy = 999;
        $jackpotDB->save();
    }

    /**
     * Удаление всех записей из таблицы user_jackpots (не начисленные джекпоты у пользователей)
     */
    private function resetUserJackpot()
    {
        $userJackpots = (new UserJackpot)->all();

        foreach ($userJackpots as $key => $userJackpot) {
            $userJackpot->delete();
        }
    }

    /**
     * Очистка табюлицы bridge_api_requests от созданных в ходе теста записей
     * Записи создаются при $_SESSION['test'] === true
     */
    protected function clearTableBridgeApiRequests()
    {
        $bridgeApiRequests = (new BridgeApiRequest)->all();
        foreach ($bridgeApiRequests as $key => $bridgeApiRequest) {
            $bridgeApiRequest->delete();
        }
    }

    protected function resetSessionTableInDB()
    {
        // удаление всех записей у который uuid === '111122223333'
        $sessions = (new Session)->where('uuid', '=', '111122223333')->get();
        foreach ($sessions as $key => $session) {
            $session->delete();
        }

        // создание сессии для теста
        $session = new Session;
        $session->uuid = '111122223333';
        $session->token = 'token';
        $session->userId = 1;
        $session->gameId = 1;
        $session->nickname = 'nick1';
        $session->demo = 'true';
        $session->platformId = 1;
        $session->status = 1;
        $session->freeSpinData = 'false';
        $session->allWin = 0;
        $session->reconnect = 'false';
        $session->check0FreeSpin = '';
        $session->balance = 100;
        $session->freeSpinMul = 0;
        $session->freeSpinResultAllWin = 'false';
        $session->doubleLose = 'false';
        $session->checkStartDouble = 'false';
        $session->savedStartAllWinForDouble = 'false';
        $session->cardGameIteration = '0';
        $session->dcard = 'false';
        $session->eventID = 'false';
        $session->preAllWinOnRope = 'false';
        $session->linesInGame = '1';
        $session->save();
    }
}
