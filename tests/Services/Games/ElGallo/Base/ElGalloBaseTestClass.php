<?php

namespace Tests\ElGallo\Base;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\GameServices\ElGalloSpinService;
use App\Models\Jackpot;
use App\Models\UserJackpot;
use App\Models\BridgeApiRequest;
use Webpatser\Uuid\Uuid;

class ElGalloBaseTestClass extends TestCase
{
    public $game;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->game = new \App\Services\GameServices\ElGalloSpinService();
    }

    public function testEmpty()
    {
        $check = false;
        if (1 === 1) {
            $check = true;
        }

        $this->assertTrue($check);
    }

    /**********************************************************************
     * ДОП ФУНКЦИИ
     *********************************************************************/

    /**
     * Подготовительные рабаты для теста
     */
    private function prepareTest()
    {
        $this->resetSession();
        $this->resetJackpot();
        $this->resetUserJackpot();
        $this->clearTableBridgeApiRequests();
    }

    /**
    * Создание и подготовка переменных в сессии, которые нужны для работы игры
    */
    private function resetSession()
    {
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 1;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 100;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['testFreeSpinPrevAllWin'] = 0;
        $_SESSION['eventID'] = 0;
    }

    /**
    * Приведение джекпота к моменту когда он должен выпасть на следующем ходу.
    * Изменяются значения только demo джекпота
    */
    private function resetJackpot()
    {
        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 10;
        $jackpotDB->result_mini = 39;
        $jackpotDB->minor = 50;
        $jackpotDB->result_minor = 199;
        $jackpotDB->major = 250;
        $jackpotDB->result_major = 499;
        $jackpotDB->big_daddy = 500;
        $jackpotDB->result_big_daddy = 999;
        $jackpotDB->save();
    }

    /**
     * Удаление всех записей из таблицы user_jackpots (не начисленные джекпоты у пользователей)
     */
    private function resetUserJackpot()
    {
        $userJackpots = (new UserJackpot)->all();

        foreach ($userJackpots as $key => $userJackpot) {
            $userJackpot->delete();
        }
    }

    /**
     * Очистка табюлицы bridge_api_requests от созданных в ходе теста записей
     * Записи создаются при $_SESSION['test'] === true
     */
    protected function clearTableBridgeApiRequests()
    {
        $bridgeApiRequests = (new BridgeApiRequest)->all();
        foreach ($bridgeApiRequests as $key => $bridgeApiRequest) {
            $bridgeApiRequest->delete();
        }
    }

}
