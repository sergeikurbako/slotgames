<?php

use App\Services\_BridgeApiService;

class BridgeApiServiceTest extends TestCase
{
    /**
     * Тест обычного спина
     */
    // public function testUpdateMoveFundsDates1()
    // {
    //     // задаем тестовые параметры
    //     $params = [
    //         'token' => 'token',
    //         'userId' => 'userId',
    //         'gameId' => 'gameId',
    //         'eventId' => 'eventId',
    //         'direction' => '',
    //         'transactionId' => 'transactionId',
    //         'eventType' => '',
    //         'amount' => 25,
    //         'extraInfo' => ['selected' => [25],
    //                         'result' => ["Sirena","Borracho","Sol","Muerte","Sol","Borracho","Chalupa","Borracho","Alacran","Calavera","Mundo","Alacran","Alacran","Corazon","Diablo"],
    //                         'featureGame' => null
    //         ],
    //         'allWin' => 100,
    //         'jackpot' => false
    //     ];
    //
    //     $bridgeApiService = new _BridgeApiService();
    //     $params = $bridgeApiService->_updateMoveFundsDates($params, 'end_of_spin');
    //
    //     $check = true; // в случае ошибки станет false
    //
    //     // проверка direction
    //     if ($params['direction'] !== 'credit') {
    //         $check = false;
    //     }
    //
    //     // проверка eventType
    //     if ($params['eventType'] !== 'Win') {
    //         $check = false;
    //     }
    //
    //     $this->assertTrue($check);
    // }
    //
    // /**
    //  * Тест спина с джекпотом
    //  */
    //
    //  public function testUpdateMoveFundsDates2()
    //  {
    //      // задаем тестовые параметры
    //      $params = [
    //          'token' => 'token',
    //          'userId' => 'userId',
    //          'gameId' => 'gameId',
    //          'eventId' => 'eventId',
    //          'direction' => '',
    //          'transactionId' => 'transactionId',
    //          'eventType' => '',
    //          'amount' => 25,
    //          'extraInfo' => ['selected' => [25],
    //                          'result' => ["Sirena","Borracho","Sol","Muerte","Sol","Borracho","Chalupa","Borracho","Alacran","Calavera","Mundo","Alacran","Alacran","Corazon","Diablo"],
    //                          'featureGame' => null
    //          ],
    //          'allWin' => 100,
    //          'jackpot' => '{"MINI":2250,"MINOR":6250,"MAJOR":26250,"BIG DADDY":51250,"result":"MINI","nextData":{"MINI":1000,"MINOR":6250,"MAJOR":26250,"BIG DADDY":51250}}'
    //      ];
    //
    //      $bridgeApiService = new _BridgeApiService();
    //      $params = $bridgeApiService->_updateMoveFundsDates($params, 'end_of_spin');
    //
    //      $check = true; // в случае ошибки станет false
    //
    //      // проверка direction
    //      if ($params['direction'] !== 'credit') {
    //          $check = false;
    //      }
    //
    //      // проверка eventType
    //      if ($params['eventType'] !== 'Jackpot') {
    //          $check = false;
    //      }
    //
    //      // проверка jackpot
    //      if ($params['jackpot'] === false) {
    //          $check = false;
    //      }
    //
    //      $this->assertTrue($check);
    //  }
    //
    //  /**
    //   * Тест спина с выпадением freeSpin
    //   */
    //  public function testUpdateMoveFundsDates3()
    //  {
    //      // задаем тестовые параметры
    //      $params = [
    //           'token' => 'token',
    //           'userId' => 'userId',
    //           'gameId' => 'gameId',
    //           'eventId' => 'eventId',
    //           'direction' => '',
    //           'transactionId' => 'transactionId',
    //           'eventType' => '',
    //           'amount' => 25,
    //           'extraInfo' => ['selected' => [25],
    //                           'result' => ["Sirena","Borracho","Sol","Muerte","Sol","Borracho","Chalupa","Borracho","Alacran","Calavera","Mundo","Alacran","Alacran","Corazon","Diablo"],
    //                           'featureGame' => true
    //           ],
    //           'allWin' => 100,
    //           'jackpot' => '',
    //           'freeSpin' => '{"count":0,"mul":2,"allWin":720,"repeat":false}'
    //       ];
    //
    //       $bridgeApiService = new _BridgeApiService();
    //       $params = $bridgeApiService->_updateMoveFundsDates($params, 'end_of_spin');
    //
    //       $check = true; // в случае ошибки станет false
    //
    //       // проверка direction
    //       if ($params['direction'] !== 'credit') {
    //           $check = false;
    //       }
    //
    //       // проверка eventType
    //       if ($params['eventType'] !== 'Win') {
    //           $check = false;
    //       }
    //
    //       // проверка freeSpin
    //       if ($params['extraInfo']['featureGame'] !== true) {
    //           $check = false;
    //       }
    //
    //       $this->assertTrue($check);
    //   }

      // /**
      //  * Тест double с проигрышем; пользователь выбрал red
      //  */
      // public function testUpdateMoveFundsDates4()
      // {
      //     $_SESSION['allWin'] = 10;
      //     $selectedCard = 'p';
      //     $_SESSION['checkStartDouble'] = false;
      //     $_SESSION['cardGameIteration'] = 0;
      //     $_SESSION['savedStartAllWinForDouble'] = 10;
      //
      //     for ($i=0; $i < 100000; $i++) {
      //         $elGalloSpinService = new \App\Services\GameServices\ElGalloSpinService();
      //         $doubleResultData = $elGalloSpinService->getDoubleResultData($selectedCard);
      //
      //         $data = (new \App\Services\_BridgeApiService())->_updateMoveFundsDates(array(
      //                 'token' => $_SESSION['token'],
      //                 'userId' => $_SESSION['userId'],
      //                 'gameId' => $_SESSION['gameId'],
      //                 'direction' => '',
      //                 'eventType' => 'Raise',
      //                 'eventID' => 1,
      //                 'amount' => $_SESSION['allWin'],
      //                 'extraInfo' => ['selected' => $doubleResultData['selected_for_api'],
      //                 'result' => $doubleResultData['result_for_api'],
      //             ],
      //             'allWin' => $doubleResultData['allWin'],
      //             'allLose' => $_SESSION['doubleLose']
      //         ), 'end_of_double');
      //
      //         if ($data["allWin"] > 0) {
      //         //    if ($data['extraInfo']['selected'] !== $data['extraInfo']['result']) {
      //                 dd($data);
      //         //    }
      //         }
      //     }
      //
      //
      //     $check = true; // в случае ошибки станет false
      //     // проверка direction
      //     if ($data['direction'] === 'credit') {
      //         $check = false;
      //     }
      //
      //     $this->assertTrue($check);
      // }
}
