<?php
namespace App\Classes\Socket;

use ZMQContext;
use Ratchet\Wamp\WampServerInterface;
use Ratchet\ConnectionInterface;

class Pusher implements WampServerInterface {
    /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTopics = array();

    public function onSubscribe(ConnectionInterface $conn, $topic) {
        echo "New connection ({$conn->resourceId})\n";
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public static function sentDataToServer(array $data) {
        // This is our new stuff
        $context = new ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'pusher');
        $socket->connect("tcp://localhost:5555");

        $socket->send(json_encode($data));
    }

    public function broadcast($entry)
    {
        $entryData = json_decode($entry, true);

        $subscribedTopics = $this->subscribedTopics;

        // отправка данных всем, кто подписан на topic_id
        if (isset($subscribedTopics[$entryData['topic_id']])) {
            $topic = $subscribedTopics[$entryData['topic_id']];
            $topic->broadcast($entryData);
        }

    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
    }
    public function onOpen(ConnectionInterface $conn) {
    }
    public function onClose(ConnectionInterface $conn) {
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
    }
}
