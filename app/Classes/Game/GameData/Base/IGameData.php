<?php

namespace App\Classes\GameData\Base;

/**
 *  Интерфейс, который должен реализовывать класс работающий с объектами данных
 */
interface IGameData
{
    public function get();
    public function set();
    public function update();
}
