<?php

namespace App\Classes\Game\GameData;

use App\Classes\GameData\Base\IGameData;
use Illuminate\Http\Request;


/**
 * Основной класс для работы со всеми данными
 */
class GameData implements IGameData
{
    protected $sessionData;
    protected $balanceData;
    protected $logicData;
    protected $stateData;
    protected $errorData;

    function __construct()
    {
        $this->sessionData = new SessionData;
        $this->balanceData = new BalanceData;
        $this->logicData = new LogicData;
        $this->stateData = new StateData;
        $this->errorData = new ErrorData;
    }

    /**
     * Получение объекта SessionData данных
     *
     * @return object
     */
    public function getSessionData() {

    }

    /**
     * Загрузка данных в объект
     *
     * @return object
     */
    public function set(Request $request) {

    }

    /**
     * Изменение объектов содержащих данных
     *
     * @return object
     */
    public function update() {

    }
}
