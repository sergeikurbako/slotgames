<?php

namespace App\Classes\CronLoop;

use App\Classes\CronLoop\CronExecutorFactory\CronExecutorFactory;

/**
 * Клас делает периодическую проверку наличия задач через БД и запускает их выполнение
 * Запуск приложения делается через крон
 */
class CronLoopDB
{
    protected $cronLoopFactory;

    public function __construct(CronLoopFactoryBase $cronLoopFactory)
    {
        $this->cronLoopFactory = $cronLoopFactory;
    }
    /**
     * Запустить выполнение задач для cron
     */
    public function cronLoop()
    {
        // получение задач для крон
        $cronTasks = CronTask::all();

        // отправка задач на выполнение
        $cronTaskDBAdapter = new CronTaskDBAdapter();
        $cronExecutorFactory = new CronExecutorFactory();
        foreach ($cronTasks as $taskDB) {
            // преобразование задачи к типу CronTaskBase
            $сronTask = $cronTaskDBAdapter->adapt($taskDB);

            // создание исполнителя задачи
            $cronTaskExecutor = $cronExecutorFactory->createCronTaskExecutor($сronTask);

            // выполнение задачи
            $finished = $cronTaskExecutor->executeTask();

            // в случае выполнения задачи, она удаляется из БД
            if ($finished === true) {
                $taskDB->delete();
            }
        }
    }
}
