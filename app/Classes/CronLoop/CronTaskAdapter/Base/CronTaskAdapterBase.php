<?php

namespace App\Classes\CronLoop\CronTaskAdapter\Base;

use App\Classes\CronLoop\CronTask\Base\CronTaskBase;

/**
 * Интерфейс, который должны реализовывать классы, которые преобразуют данные
 * для в объкты типа CronTaskBase
 */
interface CronTaskAdapterBase
{
    public function adapt($task) : CronTaskBase;
}
