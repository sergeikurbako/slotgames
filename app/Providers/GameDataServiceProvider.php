<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\GameData;

class GameDataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(GameData::class, function () {
            return new GameData();
        });
    }
}
