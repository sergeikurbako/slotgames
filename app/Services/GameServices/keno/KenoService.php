<?php
namespace App\Services\GameServices\keno;

use Illuminate\Support\Facades\Config;
use App\Services\BridgeApiService;
use App\Models\Event;
use App\Models\Session;
use App\Models\Stat;
use App\Exceptions\V1\BetPlacingAbortException;
use App\Exceptions\V1\FirstMoveFundsException;
use App\Exceptions\V1\MoveFundsException;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;

/**
 * keno
 */
class KenoService
{
    public function getSpinResultData($test = false, $testData = [])
    {
        if ($test !== true) {
            session_start();
        }

        if ($test === true) {
            $_GET['balls_selected'] = $testData['balls_selected'];
            $_GET['betValue'] = $testData['betValue'];
            $_GET['sessionName'] = $testData['sessionName'];
            $_SESSION['demo'] = true;
        }

        $uuid = $_GET['sessionName'];

        if ($test === true) {
            $_SESSION['sessionName'] = 'sessionName';
        } else {
            $session = (new Session())->where('uuid', '=', $uuid)->get()->first();
            if (is_object($session->uuid)) {
                $_SESSION['sessionName'] = $session->uuid->string;
            } else {
                $_SESSION['sessionName'] = $session->uuid;
            }
            $_SESSION['sessionName'] = $session->uuid;
            $_SESSION['balance'] = unserialize($session->balance);
            $_SESSION['token'] = $session->token;
            $_SESSION['userId'] = $session->userId;
            $_SESSION['gameId'] = $session->gameId;
            $_SESSION['nickname'] = $session->nickname;
            $_SESSION['demo'] = $session->demo;
            $_SESSION['platformId'] = $session->platformId;
            $_SESSION['freeSpinData'] = unserialize($session->freeSpinData);
            $_SESSION['allWin'] = unserialize($session->allWin);
            $_SESSION['reconnect'] = unserialize($session->reconnect);
            $_SESSION['freeSpinData'] = unserialize($session->freeSpinData);
            $_SESSION['check0FreeSpin'] = unserialize($session->check0FreeSpin);
            $_SESSION['freeSpinMul'] = unserialize($session->freeSpinMul);
            $_SESSION['freeSpinResultAllWin'] = unserialize($session->freeSpinResultAllWin);
            $_SESSION['doubleLose'] = unserialize($session->doubleLose);
            $_SESSION['checkStartDouble'] = unserialize($session->checkStartDouble);
            $_SESSION['savedStartAllWinForDouble'] = unserialize($session->savedStartAllWinForDouble);
            $_SESSION['cardGameIteration'] = unserialize($session->cardGameIteration);
            $_SESSION['dcard'] = unserialize($session->dcard);
            $_SESSION['eventID'] = unserialize($session->eventID);
            $_SESSION['preAllWinOnRope'] = unserialize($session->preAllWinOnRope);
        }

        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        // predvaritelnye statusi
        $mesage = "succes";
        $lastbal = 'No';

        // проверка входящих данных
        if (!isset($_GET['balls_selected']) or !isset($_GET['betValue'])) {
            $data = json_encode(array(
              "mesage" => "error with get data"
            ));
            echo $data;
            exit();
        }

        // получаем шары которые выбрал игрок
        $user_numbers = array_map('intval', explode(',', $_GET["balls_selected"]));
        $sum_bet = floatval($_GET["betValue"]);
        $picked = count($user_numbers);

        // Тираж
        $server_numbers = $this->getNumbers(20);

        $status = "lose";

        // Проверка на совпадение
        $result = array_intersect($server_numbers, $user_numbers);

        // Генерация ответа
        $response = [
          "server_numbers" => $server_numbers,
          "user_numbers" => $user_numbers,
          "result" => $result
        ];

        $count = count($response['result']);
        //$count = 9;

        $win_sum = 0;

        //>----------------------- Jack Pot --------------------------
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) { // не выполняется во время теста
            $jackPot = Cache::get('kenoJackPot');
            if (!$jackPot) {
                Cache::forever('kenoJackPot', 1500);
            }
            if ($jackPot < 5000) {
                Cache::forever('kenoJackPot', Cache::get('kenoJackPot') + $sum_bet * 0.002);
                if (Cache::get('kenoJackPot') > 5000) {
                    Cache::forever('kenoJackPot', 5000);
                }
            }
        } else {
            $jackPot = Cache::get('kenoJackPotTest');
            if (!$jackPot) {
                Cache::forever('kenoJackPotTest', 1500);
            }
            if ($jackPot < 5000) {
                Cache::forever('kenoJackPotTest', Cache::get('kenoJackPotTest') + $sum_bet * 0.002);
                if (Cache::get('kenoJackPotTest') > 5000) {
                    Cache::forever('kenoJackPotTest', 5000);
                }
            }
        }

        $checkJackpot = false;
        // You always can get keno jack pot. Just use: money_format('%.2n', cache('kenoJackPot'))
        //<

        switch ($picked) {
            case 2:
                if ($count == 2) {
                    $win_sum = $sum_bet * 11;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 3:
                if ($count == 3) {
                    $win_sum = $sum_bet * 26;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 4:
                if ($count == 4) {
                    $win_sum = $sum_bet * 80;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 5;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 5:
                if ($count == 5) {
                    $win_sum = $sum_bet * 75;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 11;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 6:
                if ($count == 6) {
                    $win_sum = $sum_bet * 125;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 30;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 7;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 7:
                if ($count == 7) {
                    $win_sum = $sum_bet * 200;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 75;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 12;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 4;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 8:
                if ($count == 8) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev') {
                        $checkJackpot = true;
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);

                        //$win_sum = $jackPot;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoJackPot', 1500);
                        } else {
                            Cache::forever('kenoJackPotTest', 1500);
                        }

                    } else {
                        $win_sum = $sum_bet * 200;
                    }
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 120;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 17;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 6;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 9:
                if ($count == 9) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev') {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoJackPot', 1500);
                        } else {
                            Cache::forever('kenoJackPotTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 500;
                    }
                } elseif ($count == 8) {
                    $win_sum = $sum_bet * 200;
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 55;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 14;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 5;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 10:
                if ($count == 10) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev') {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoJackPot', 1500);
                        } else {
                            Cache::forever('kenoJackPotTest', 1500);
                        }
                    } else {
                        $win_sum += 2500;
                    }
                } elseif ($count == 9) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev') {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoJackPot', 1500);
                        } else {
                            Cache::forever('kenoJackPotTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 500;
                    }
                } elseif ($count == 8) {
                    $win_sum = $sum_bet * 100;
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 24;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 8;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 4;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;
        }

        if (in_array($server_numbers[19], $user_numbers)) {
            $win_sum *= 4;
            $lastbal = "yes";
        }


        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            $_SESSION['eventID'] = Uuid::generate()->string;

            $apiService = new BridgeApiService();

            // отправка запроса moveFunds для снятия средств
            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = (int) $_GET['platformId'];
            }
            $responseMoveFunds = (new BridgeApiService())->moveFunds(array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => 'debit',
                'platformId' => $platformId,
                'eventType' => 'BetPlacing',
                'amount' => floatval($_GET["betValue"]),
                'extraInfo' => [
                    'selected' => array_map('intval', explode(',', $_GET["balls_selected"])),
                    'result' => $server_numbers,
                    'jackpotAmount' => Cache::get('kenoJackPot')
                ],
                'eventID' => $_SESSION['eventID']
            ));

            // проверка сессии
            if ($responseMoveFunds === false) {
                throw new FirstMoveFundsException();
            }
            if (!isset(json_decode($responseMoveFunds)->status)) {
                throw new FirstMoveFundsException();
            }
            if (json_decode($responseMoveFunds)->status === false) {
                throw new FirstMoveFundsException();
            }

            // получение баланса
            $getBalanceData = json_decode($responseMoveFunds);

            if ($getBalanceData === false) {
                throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
            }
            if (!isset($getBalanceData->status)) {
                throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
            }
            if ($getBalanceData->status === false) {
                throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
            }

            $balance = (float) $getBalanceData->balance;
        } else {
            if (!isset($_SESSION['balance'])) {
                $_SESSION['balance'] = 100;
            }

            // прибавляем к балансу предыдущий выигрышь
            $balance = $_SESSION['balance'] - floatval($_GET["betValue"]);
            $_SESSION['balance'] = $_SESSION['balance'] - floatval($_GET["betValue"]) + $win_sum;
        }

        $balance = $balance + $win_sum; // плюсуем выйгрешь

        //echo "<br>count - ".$count."<br>";
        //echo "lastball - ".$lastbal."<br>";
        //echo "balance - ".$balance."<br>";
        //}

        ///баланс пока храним в файле
        // $my_file = 'balance.txt';
        // $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
        // $data = $balance;
        // fwrite($handle, $data);
        ////


        if ($win_sum > 0) {
            $status = "win";
        }

        $_SESSION['demo'] ? $jackpotValue = Cache::get('kenoJackPotTest') : $jackpotValue = Cache::get('kenoJackPot');

        $data = json_encode(array(
          "balance" => $balance,
          "result" => $win_sum,
          "status" => $status,
          "balls" => $server_numbers,
          "balls_selected" => $user_numbers,
          "got_balls" => $count,
          "win_balls" => $result,
          "lastbal" => $lastbal,
          "picked" => $picked,
          "mesage" => $mesage,
          "jackpot" => $checkJackpot,
          "jackpotValue" => $jackpotValue
        ));

        if ($win_sum >= 2500 && $checkJackpot === false) {
            return $this->getSpinResultData();
        }

        // отправка moveFunds с результатом
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            if ($win_sum > 0) {
                $eventType = 'Win';
            } else {
                $eventType = 'Lose';
            }

            if ($checkJackpot) {
                $eventType = 'Jackpot';
            }

            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = (int) $_GET['platformId'];
            }
            $dadas = array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => '',
                'platformId' => $platformId,
                'eventType' => $eventType,
                'amount' => $win_sum,
                'extraInfo' => ['selected' => $server_numbers,
                                'result' => $server_numbers,
                                'jackpotAmount' => Cache::get('kenoJackPot')
                ],
                'allWin' => $win_sum,
                'eventID' => $_SESSION['eventID']
            );

            $_SESSION['allWin'] = $win_sum;

            $responseMoveFunds = (new BridgeApiService())->moveFunds($dadas, 'end_of_spin');

            // отправка статистики в бд
            $stat = (new Stat)->where('name', '=', 'superkeno')->get()->first();

            if ($stat !== null) {
                $stat->iteration_count += 1;
                $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                $stat->sum_win += floatval($win_sum);
                $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                $stat->data = '';
                $stat->save();
            } else {
                $stat = new Stat;
                $stat->name = 'superkeno';
                $stat->iteration_count += 1;
                $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                $stat->sum_win += floatval($win_sum);
                $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                $stat->data = '';
                $stat->save();
            }

            if (!isset(json_decode($responseMoveFunds)->status)) {
                throw new MoveFundsException(serialize($dadas));
            }
            if (json_decode($responseMoveFunds)->status === false) {
                throw new MoveFundsException(serialize($dadas));
            }
        }

        if ($test !== true) {
            if (isset($spinResultData['freeSpinData'])) {
                $session->freeSpinData = serialize($spinResultData['freeSpinData']);
            }

            if (isset($spinResultData['allWin'])) {
                $session->allWin = serialize($spinResultData['allWin']);
            }

            if (isset($_SESSION['balance'])) {
                $session->balance = serialize($_SESSION['balance']);
            }

            if (isset($_SESSION['eventID'])) {
                $session->eventID = serialize($_SESSION['eventID']);
            } else {
                $session->eventID = serialize(false);
            }

            if (isset($_SESSION['preAllWinOnRope'])) {
                $session->preAllWinOnRope = serialize($_SESSION['preAllWinOnRope']);
            } else {
                $session->preAllWinOnRope = serialize(false);
            }

            if (isset($_SESSION['doubleLose'])) {
                $session->doubleLose = serialize($_SESSION['doubleLose']);
            } else {
                $session->doubleLose = serialize(false);
            }

            if (isset($_SESSION['checkStartDouble'])) {
                $session->checkStartDouble = serialize($_SESSION['checkStartDouble']);
            } else {
                $session->checkStartDouble = serialize(false);
            }

            if (isset($_SESSION['savedStartAllWinForDouble'])) {
                $session->savedStartAllWinForDouble = serialize($_SESSION['savedStartAllWinForDouble']);
            } else {
                $session->savedStartAllWinForDouble = serialize(false);
            }

            if (isset($_SESSION['cardGameIteration'])) {
                $session->cardGameIteration = serialize($_SESSION['cardGameIteration']);
            } else {
                $session->cardGameIteration = serialize(false);
            }

            if (isset($_SESSION['dcard'])) {
                $session->dcard = serialize($_SESSION['dcard']);
            } else {
                $session->dcard = serialize(false);
            }

            if (isset($_SESSION['freeSpinResultAllWin'])) {
                $session->freeSpinMul = serialize($_SESSION['freeSpinResultAllWin']);
            } else {
                $session->freeSpinMul = serialize(false);
            }

            if (isset($_SESSION['freeSpinMul'])) {
                $session->freeSpinMul = serialize($_SESSION['freeSpinMul']);
            } else {
                $session->freeSpinMul = serialize(false);
            }

            if (isset($spinResultData['reconnect'])) {
                $session->reconnect = serialize($spinResultData['reconnect']);
            } else {
                $session->reconnect = serialize(false);
            }

            if (isset($spinResultData['check0FreeSpin'])) {
                $session->check0FreeSpin = serialize($spinResultData['check0FreeSpin']);
            } else {
                $session->check0FreeSpin = serialize(false);
            }

            $session->save();
        }

        return $data;
    }

    // генератор тиража
    function getNumbers($count = 20)
    {
        $numbers = range(1, 80);
        shuffle($numbers);
        return array_slice($numbers, 0, $count);
    }

}
