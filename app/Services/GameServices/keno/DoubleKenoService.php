<?php
namespace App\Services\GameServices\keno;

use Illuminate\Support\Facades\Config;
use App\Services\BridgeApiService;
use App\Models\Event;
use App\Models\Session;
use App\Models\Stat;
use App\Exceptions\V1\BetPlacingAbortException;
use App\Exceptions\V1\FirstMoveFundsException;
use App\Exceptions\V1\MoveFundsException;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Cache;

class DoubleKenoService
{
    public function getSpinResultData($test = false, $testData = [])
    {
        if ($test !== true) {
            session_start();
        }

        if ($test === true) {
            $_GET['balls_selected'] = $testData['balls_selected'];
            $_GET['betValue'] = $testData['betValue'];
            $_GET['sessionName'] = $testData['sessionName'];

            if (isset($testData['ballsDouble'])) {
                $_GET['ballsDouble'] = $testData['ballsDouble'];
            }
            if (isset($testData['key'])) {
                $_GET['key'] = $testData['key'];
            }
            if (isset($testData['doubleSpin'])) {
                $_GET['doubleSpin'] = $testData['doubleSpin'];
            }
            $_SESSION['demo'] = true;
        }

        $uuid = $_GET['sessionName'];
        $session = (new Session())->where('uuid', '=', $uuid)->get()->first();

        if ($test !== true) {
            if ($session !== null) {
                if (is_object($session->uuid)) {
                    $_SESSION['sessionName'] = $session->uuid->string;
                } else {
                    $_SESSION['sessionName'] = $session->uuid;
                }

                $_SESSION['balance'] = unserialize($session->balance);
                $_SESSION['token'] = $session->token;
                $_SESSION['userId'] = $session->userId;
                $_SESSION['gameId'] = $session->gameId;
                $_SESSION['nickname'] = $session->nickname;
                $_SESSION['demo'] = $session->demo;
                $_SESSION['platformId'] = $session->platformId;
                $_SESSION['allWin'] = unserialize($session->allWin);
                $_SESSION['sessionName'] = $session->uuid;
                $_SESSION['savedStartAllWinForDouble'] = unserialize($session->savedStartAllWinForDouble);
                $_SESSION['eventID'] = unserialize($session->eventID);
            }
        }

        if ($_SESSION['demo'] === 'false' || $_SESSION['demo'] === 0 || $_SESSION['demo'] === '0') {
            $_SESSION['demo'] = false;
        }

        $ballsDouble = false;

        // predvaritelnye statusi
        $mesage = "succes";
        $lastbal = 'No';

        // проверка входящих данных
        if (!isset($_GET['balls_selected']) or !isset($_GET['betValue'])) {
            $data = json_encode(["mesage" => "error with get data"]);
            echo $data;
            exit();
        }

        //>duble balls proverka

        if (isset($_GET['ballsDouble'])) {
            $kenoDouble = true;
            $ballsDouble = $_GET['ballsDouble'];
        } else {
            $kenoDouble = false;
        }

        // !!!!!!!!!!!!!!!  duble balls proverka  //  end


        // получаем шары которые выбрал игрок
        $user_numbers = array_map('intval', explode(',', $_GET["balls_selected"]));
        $sum_bet = floatval($_GET["betValue"]);
        $picked = count($user_numbers);

        // Тираж
        // $server_numbers = getNumbers(20, $kenoDouble, $ballsDouble);


        /// !!!!!!!!!!!!  duble keno


        if ($kenoDouble == true and $ballsDouble == 0) {
            $server_numbers = $this->getNumbers($kenoDouble, $ballsDouble, 10);
        } else {
            $server_numbers = $this->getNumbers($kenoDouble, $ballsDouble, 20);
        }


        // !!!!!!!!!!!!!  duble keno // end



        //$server_numbers = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);



        // проверка если выпал последий шар если da to  меняю местами с первым шаром

        // for ($x = 0; $x <= 999; $x++) {
        //     if (in_array($server_numbers[19], $user_numbers)) {
        //         $server_numbers = getNumbers(20, $kenoDouble, $ballsDouble);
        //     } else {
        //         break;
        //     }
        // }



        // for ($x = 0; $x <= 9999; $x++) {
        //     if ($server_numbers[19] == end($user_numbers)) {$server_numbers = getNumbers(20);}
        //     	else {break;}
        // }





        //for ($x = 0; $x <= 1000000; $x++) {

        //$balance = $balance - $sum_bet;

        $status = "lose";

        // Выбор пользователя
        //$user_numbers = getNumbers();

        $apiService = new BridgeApiService();

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            if (!isset($_GET["doubleSpin"])) {
                // отправка запроса moveFunds для снятия средств
                $_SESSION['eventID'] = Uuid::generate()->string;

                //$_SESSION['ballsDouble'] = $ballsDouble;
                //$balance = $_SESSION['balance'] - floatval($_GET["betValue"]);

                $platformId = 1;
                if (isset($_GET['platformId'])) {
                    $platformId = (int) $_GET['platformId'];
                }
                $responseMoveFunds = (new BridgeApiService())->moveFunds(array(
                    'token' => $_SESSION['token'],
                    'userId' => $_SESSION['userId'],
                    'gameId' => $_SESSION['gameId'],
                    'direction' => 'debit',
                    'platformId' => $platformId,
                    'eventType' => 'BetPlacing',
                    'amount' => floatval($_GET["betValue"]),
                    'extraInfo' => [
                        'selected' => array_map('intval', explode(',', $_GET["balls_selected"])),
                        'result' => $server_numbers,
                        'jackpotAmount' => Cache::get('kenoDoubleJP')
                    ],
                    'eventID' => $_SESSION['eventID']
                ));

                // получение баланса от BridgeApi
                if ($responseMoveFunds === false) {
                    throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
                }
                if (!isset(json_decode($responseMoveFunds)->status)) {
                    throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
                }
                if (json_decode($responseMoveFunds)->status === false) {
                    throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
                }

                $balance = (float) json_decode($responseMoveFunds)->balance;
            } else {
                $balance = $_SESSION['balance'];
            }
        } else {
            if (!isset($_SESSION['balance'])) {
                $_SESSION['balance'] = 100;
            }


            if (isset($_GET['doubleSpin'])) {
                $balance = $_SESSION['balance'];
            } else {
                $balance = $_SESSION['balance'] - floatval($_GET["betValue"]);
            }
        }

        $_SESSION['balance'] = $balance;
        // Проверка на совпадение
        $result = array_intersect($server_numbers, $user_numbers);

        // Генерация ответа
        $response = [
            "server_numbers" => $server_numbers,
            "user_numbers" => $user_numbers,
            "result" => $result
        ];

        $count = count($response['result']);

        $win_sum = 0;

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false && isset($_GET["doubleSpin"])) {
            $jackPot = Cache::get('kenoDoubleJP');
            if (!$jackPot) {
                Cache::forever('kenoDoubleJP', 1500);
            }
            if ($jackPot < 5000) {
                Cache::forever('kenoDoubleJP', Cache::get('kenoDoubleJP') + $sum_bet * 0.002);
                if (Cache::get('kenoDoubleJP') > 5000) {
                    Cache::forever('kenoDoubleJP', 5000);
                }
            }
        } else {
            $jackPot = Cache::get('kenoDoubleJPTest');
            if (!$jackPot) {
                Cache::forever('kenoDoubleJPTest', 1500);
            }
            if ($jackPot < 5000) {
                Cache::forever('kenoDoubleJPTest', Cache::get('kenoDoubleJPTest') + $sum_bet * 0.002);
                if (Cache::get('kenoDoubleJPTest') > 5000) {
                    Cache::forever('kenoDoubleJPTest', 5000);
                }
            }
        }

        $checkJackpot = false;

        switch ($picked) {
            case 2:
                if ($count == 2) {
                    $win_sum = $sum_bet * 8;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 3:
                if ($count == 3) {
                    $win_sum = $sum_bet * 16;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 4:
                if ($count == 4) {
                    $win_sum = $sum_bet * 36;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 4;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 5:
                if ($count == 5) {
                    $win_sum = $sum_bet * 80;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 16;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 6:
                if ($count == 6) {
                    $win_sum = $sum_bet * 150;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 20;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 7;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 7:
                if ($count == 7) {
                    $win_sum = $sum_bet * 200;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 25;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 8;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 3;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 8:
                if ($count == 8) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev' && isset($_GET["doubleSpin"])) {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoDoubleJP', 1500);
                        } else {
                            Cache::forever('kenoDoubleJPTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 500;
                    }
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 80;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 22;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 8;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 2;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 9:
                if ($count == 9) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev' && isset($_GET["doubleSpin"])) {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoDoubleJP', 1500);
                        } else {
                            Cache::forever('kenoDoubleJPTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 1000;
                    }
                } elseif ($count == 8) {
                    $win_sum = $sum_bet * 200;
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 50;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 10;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 5;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;

            case 10:
                if ($count == 10) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev' && isset($_GET["doubleSpin"])) {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoDoubleJP', 1500);
                        } else {
                            Cache::forever('kenoDoubleJPTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 1000;
                    }
                } elseif ($count == 9) {
                    if ($sum_bet >= 1 && Config::get('app.mod') !== 'dev' && isset($_GET["doubleSpin"])) {
                        // $win_sum = money_format('%.2n', $jackPot);
                        $win_sum = round($jackPot, 2);
                        //$win_sum = $jackPot;
                        $checkJackpot = true;

                        if ($_SESSION['demo'] === false) {
                            Cache::forever('kenoDoubleJP', 1500);
                        } else {
                            Cache::forever('kenoDoubleJPTest', 1500);
                        }
                    } else {
                        $win_sum = $sum_bet * 500;
                    }
                } elseif ($count == 8) {
                    $win_sum = $sum_bet * 50;
                } elseif ($count == 7) {
                    $win_sum = $sum_bet * 11;
                } elseif ($count == 6) {
                    $win_sum = $sum_bet * 6;
                } elseif ($count == 5) {
                    $win_sum = $sum_bet * 3;
                } elseif ($count == 4) {
                    $win_sum = $sum_bet * 1;
                } elseif ($count == 3) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 2) {
                    $win_sum = $sum_bet * 0;
                } elseif ($count == 1) {
                    $win_sum = $sum_bet * 0;
                }
                break;
        }

        if ($ballsDouble and in_array($server_numbers[19], $user_numbers)) {
            $win_sum *= 4;
            $lastbal = "yes";
        }


        // $lastball_probility = rand(1, 100);
        //
        //
        //
        // if ($count >= 1) {
        //
        //     // шансы на выпад послегнего шара
        //     if ($lastball_probility < 28) { // !!!!!!!!!!!!!!!!CAUTION!!!!!!!!!!!!!!!!!!!!!!! V
        //
        //
        //         $win_sum *= 4;
        //         $lastbal = "yes";
        //
        //
        //         if ($server_numbers[19] != end($user_numbers)) {
        //
        //
        //
        //
        //         // подгоняем последний шар
        //             $first_key = key($result);
        //             $key_for_last_ball = array_search($result[$first_key], $server_numbers);
        //
        //
        //             $temp = $server_numbers[19];
        //             $server_numbers[19] = $server_numbers[$key_for_last_ball];
        //             $server_numbers[$key_for_last_ball]  = $temp;
        //         }
        //     }
        // }



        if (Config::get('app.mod') === 'dev' && $_SESSION['demo'] !== false) {
            $balance = $balance + $win_sum; // плюсуем выйгрешь
        }

        //echo "<br>count - ".$count."<br>";
        //echo "lastball - ".$lastbal."<br>";
        //echo "balance - ".$balance."<br>";







        //}







        $data = $balance;
        ////


        if ($win_sum > 0) {
            $status = "win";
        }



        $salt = "3d19e27a-82ce-461a-8b75-4f96b0674f55";
        $server_numbers_for_hash = [];
        for ($i=0; $i < 20 ; $i++) {
            if ($i < 10) {
                $server_numbers_for_hash[] = $server_numbers[$i];
            }
        }
        $password = hash('sha256', $salt.implode(",", $server_numbers_for_hash));


        //// kenoduble
        if ($kenoDouble == true and $ballsDouble == 0) {
            //$balance = false;
            //$win_sum = false;
            //$status = false;
            //$count = false;
            //$result = false;
            //$lastbal = false;
            //$picked = false;
        } elseif ($kenoDouble == true and $ballsDouble != 0) {
            $keyRequest = $_GET['key'];

            $keyLocal = hash('sha256', $salt.$ballsDouble);

            if ($keyRequest != $keyLocal) {
                $balance = false;
                $win_sum = false;
                $status = false;
                $count =false;
                $result=false;
                $lastbal=false;
                $picked=false;
                $server_numbers = false;
                $user_numbers =false;
                $result=false;
                $mesage = "Error request not validate";
                $password = false;
            }
        }

        if ($win_sum >= 2500 && $checkJackpot === false) {
            return $this->getSpinResultData($test, $testData);
        }

        if ($_SESSION['demo'] === 'false') {
            $_SESSION['demo'] = false;
        }

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {

            if (isset($_GET["doubleSpin"])) {
                if ($win_sum > 0) {
                    $eventType = 'Win';
                } else {
                    $eventType = 'Lose';
                }

                if ($eventType === 'Win') {
                    $amount = $win_sum;
                } else {
                    $amount = floatval($_GET["betValue"]);
                }

                if ($checkJackpot) {
                    $eventType = 'Jackpot';
                }

                if ($_GET["doubleSpin"] === 'true') { // double запрос

                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = (int) $_GET['platformId'];
                    }
                    $responseMoveFunds = (new BridgeApiService())->moveFunds(array(
                            'token' => $_SESSION['token'],
                            'userId' => $_SESSION['userId'],
                            'gameId' => $_SESSION['gameId'],
                            'direction' => 'debit',
                            'platformId' => $platformId,
                            'eventType' => 'Raise',
                            'eventID' => $_SESSION['eventID'],
                            'amount' => floatval($_GET["betValue"]) / 2,
                            'extraInfo' => [
                                'selected' => $ballsDouble,
                                'result' => $result,
                                'jackpotAmount' => Cache::get('kenoDoubleJP')
                            ],
                            'allWin' => $_SESSION['allWin']
                    ), 'end_of_double_in_keno');

                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = $_GET['platformId'];
                    }
                    $dadas = array(
                        'token' => $_SESSION['token'],
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'direction' => '',
                        'platformId' => $platformId,
                        'eventType' => $eventType,
                        'amount' => $amount,
                        'extraInfo' => ['selected' => $ballsDouble,
                                        'result' => $server_numbers,
                                        'jackpotAmount' => Cache::get('kenoDoubleJP')
                        ],
                        'allWin' => $win_sum,
                        'eventID' => $_SESSION['eventID']
                    );
                    $responseMoveFunds = (new BridgeApiService())->moveFunds($dadas, 'end_of_spin');

                    if ($responseMoveFunds === false) {
                        throw new FirstMoveFundsException();
                    }
                    if (!isset(json_decode($responseMoveFunds)->status)) {
                        throw new FirstMoveFundsException();
                    }
                    if (json_decode($responseMoveFunds)->status === false) {
                        throw new FirstMoveFundsException();
                    }

                    $balance = (float) json_decode($responseMoveFunds)->balance;
                    //$balance = $_SESSION['balance'] + $win_sum - floatval($_GET["betValue"]) / 2;
                    $_SESSION['balance'] = $balance;

                    // отправка статистики в бд
                    $stat = (new Stat)->where('name', '=', 'doublekeno')->get()->first();

                    if ($stat !== null) {
                        $stat->iteration_count += 1;
                        $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                        $stat->sum_win += floatval($win_sum);
                        $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                        $stat->data = 'empty';
                        $stat->save();
                    } else {
                        $stat = new Stat;
                        $stat->name = 'doublekeno';
                        $stat->data = 'empty';
                        $stat->iteration_count += 1;
                        $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                        $stat->sum_win += floatval($win_sum);
                        $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                        $stat->save();
                    }
                }

                if ($_GET["doubleSpin"] === 'false') { // обычный запрос
                    // отправка moveFunds с результатом

                    // if ($checkJackpot) {
                    //     $win_sum = $win_sum;
                    // } else {
                    //     $win_sum = $win_sum / 2;
                    // }
                    // $amount = $amount / 2;

                    if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {

                        $platformId = 1;
                        if (isset($_GET['platformId'])) {
                            $platformId = $_GET['platformId'];
                        }
                        $dadas = array(
                            'token' => $_SESSION['token'],
                            'userId' => $_SESSION['userId'],
                            'gameId' => $_SESSION['gameId'],
                            'direction' => '',
                            'platformId' => $platformId,
                            'eventType' => $eventType,
                            'amount' => $amount,
                            'extraInfo' => ['selected' => $ballsDouble,
                                            'result' => $server_numbers,
                                            'jackpotAmount' => Cache::get('kenoDoubleJP')
                            ],
                            'allWin' => $win_sum,
                            'eventID' => $_SESSION['eventID']
                        );

                        //$balance = $_SESSION['balance'] + $win_sum;

                        $responseMoveFunds = (new BridgeApiService())->moveFunds($dadas, 'end_of_spin');
                        $balance = (float) json_decode($responseMoveFunds)->balance;
                        $_SESSION['balance'] = $balance;

                        // отправка статистики в бд
                        $stat = (new Stat)->where('name', '=', 'doublekeno')->get()->first();

                        if ($stat !== null) {
                            $stat->iteration_count += 1;
                            $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                            $stat->sum_win += floatval($win_sum);
                            $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                            $stat->data = 'empty';
                            $stat->save();
                        } else {
                            $stat = new Stat;
                            $stat->name = 'doublekeno';
                            $stat->iteration_count += 1;
                            $stat->sum_bet += floatval(floatval($_GET["betValue"]));
                            $stat->sum_win += floatval($win_sum);
                            $stat->data = 'empty';
                            $win_sum > 0 ? $stat->number_of_winning_spins += 1 : $stat->number_of_losing_spins += 1;
                            $stat->save();
                        }
                    }
                }

                if ($responseMoveFunds === false) {
                    throw new FirstMoveFundsException();
                }
                if (!isset(json_decode($responseMoveFunds)->status)) {
                    throw new FirstMoveFundsException();
                }
                if (json_decode($responseMoveFunds)->status === false) {
                    throw new FirstMoveFundsException();
                }

                // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)


                if ($apiService === false) {
                    throw new FirstMoveFundsException();
                }
                $checkApiSession = json_decode($apiService->sessionCheck($_SESSION['token'], $_SESSION['userId']));
                if (!isset($checkApiSession->status)) {
                    throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
                }
                if ($checkApiSession->status === false) {
                    throw new BetPlacingAbortException(floatval($_GET["betValue"]), array_map('intval', explode(',', $_GET["balls_selected"])));
                }
            }
        } else {
            if (!isset($_SESSION['balance'])) {
                $_SESSION['balance'] = 100;
            }

            // работа с балансом
            if (isset($_GET["doubleSpin"])) {
                if ($_GET["doubleSpin"] === 'true') { // double запрос
                    $_SESSION['balance'] = $_SESSION['balance'] - floatval($_GET["betValue"]) / 2;
                }

                $balance = $_SESSION['balance'] + floatval($win_sum);
            }

            $_SESSION['balance'] = $balance;
        }

        $_SESSION['balance'] = $balance;
        $_SESSION['savedStartAllWinForDouble'] = $win_sum;

        /// keno duble

        if ($test !== true) {
            if (isset($_SESSION['allWin'])) {
                $session->allWin = serialize($_SESSION['allWin']);
            }

            if (isset($_SESSION['balance'])) {
                $session->balance = serialize($_SESSION['balance']);
            }

            if (isset($_SESSION['eventID'])) {
                $session->eventID = serialize($_SESSION['eventID']);
            } else {
                $session->eventID = serialize(false);
            }

            if (isset($_SESSION['savedStartAllWinForDouble'])) {
                $session->savedStartAllWinForDouble = serialize($_SESSION['savedStartAllWinForDouble']);
            } else {
                $session->savedStartAllWinForDouble = serialize(false);
            }

            $session->save();
        }


        $_SESSION['demo'] ? $jackpotAmount = Cache::get('kenoDoubleJPTest') : $jackpotAmount = Cache::get('kenoDoubleJP');

        return array(
            "balance" => $balance,
            "result" => $win_sum,
            "status" => $status,
            'win_sum' => $win_sum,
            "balls" => $server_numbers,
            "balls_selected" => $user_numbers,
            "got_balls" => $count,
            "win_balls" => $result,
            "lastbal" => $lastbal,
            "picked" => $picked,
            "mesage" => $mesage,
            "key" => $password,
            'ballsDouble' => $ballsDouble,
            'jackpot' => $checkJackpot,
            'jackpotValue' => $jackpotAmount
        );
    }

    // generator
    function getNumbers($kenoDouble, $ballsDouble, $count = 20)
    {
        $numbers = range(1, 80);
        /// !!!!!!!!!!!!  duble keno
        if ($kenoDouble == true and $ballsDouble != 0) {
            $ballsDouble = array_map('intval', explode(',', $_GET["ballsDouble"]));
            foreach ($ballsDouble as $oldNumber) {
                unset($numbers[$oldNumber - 1]);
            }
        }
        // !!!!!!!!!!!!!  duble keno // end
        shuffle($numbers);

        if ($kenoDouble == true and $ballsDouble != 0) {
            $numbers = $ballsDouble+$numbers;
        }

        return array_slice($numbers, 0, $count);
    }
}
