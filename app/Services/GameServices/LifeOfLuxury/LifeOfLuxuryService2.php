<?php

namespace App\Services\GameServices\LifeOfLuxury;

use App\Models\SavedDataBundle;
use App\Models\Stat;
use App\Services\GameServices\AbstractSpinService;
use App\Services\BridgeApiService;
use App\Exceptions\V1\BalanceException;
use Illuminate\Support\Facades\Config;
use App\Exceptions\V1\UnauthenticatedException;
use App\Exceptions\V1\BetPlacingAbortException;
use App\Exceptions\V1\MoveFundsException;
use App\Models\Event;
use Webpatser\Uuid\Uuid;

class LifeOfLuxuryService2 extends AbstractSpinService
{
    private $jokerValue = 0;
    private $bonusValue = 10;
    private $maxNOVGFA = 1000; // максимальное колличество сгенерированных для массива значений
    private $wl = [];
    private $balance;
    private $allWin;
    private $rope = false;
    private $bet;
    private $checkWinJoker = false;
    private $info;
    private $winCellInfo;

    public function getSpinResultData($betLine, $linesInGame, $gameName, $data = [])
    {
        $_SESSION['bet'] = round($linesInGame * $betLine, 2);
        $_SESSION['linesInGame'] = $linesInGame;

        $this->sendBetPlacing();

        // получение баланса
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            if (isset($_SESSION['test'])) {
                if ($_SESSION['test'] === false) {
                    $_SESSION['balance'] = (float) json_decode((new BridgeApiService())->getBalance($_SESSION['token'], $_SESSION['userId'], $_SESSION['gameId']))->balance - $_SESSION['bet'];
                }
            } else {
                $_SESSION['balance'] = (float) json_decode((new BridgeApiService())->getBalance($_SESSION['token'], $_SESSION['userId'], $_SESSION['gameId']))->balance - $_SESSION['bet'];
            }
        }

        if (!isset($_SESSION)) {
            session_start();
        }

        if ($_SESSION['freeSpinResultAllWin'] != false) {
            $_SESSION['freeSpinResultAllWin'] = false;
        }

        $this->balance = round($_SESSION['balance'], 2);
        $this->allWin = round($_SESSION['allWin'], 2);

        //$gameRules = $this->getGameRules($gameName);
        $gameRules = '{"lines":[[1,4,7,10,13],[0,3,6,9,12],[2,5,8,11,14],[0,4,8,10,12],[2,4,6,10,14],[1,3,7,11,13],[1,5,7,9,13],[0,3,7,11,14],[2,5,7,9,12],[0,4,6,10,12],[2,4,8,10,14],[1,3,6,9,13],[1,5,8,11,13],[0,4,7,10,12],[2,4,7,10,14]],"winRules":[[10,3,2,0],[10,4,15,0],[10,5,100,0],[8,3,5,0],[8,4,20,0],[8,5,100,0],[3,3,10,0],[3,4,50,0],[3,5,200,0],[6,3,10,0],[6,4,30,0],[6,5,150,0],[7,3,5,0],[7,4,30,0],[7,5,150,0],[2,3,5,0],[2,4,25,0],[2,5,120,0],[1,2,10,0],[1,3,50,0],[1,4,500,0],[1,5,5000,0],[9,2,5,0],[9,3,30,0],[9,4,200,0],[9,5,1000,0],[4,3,20,0],[4,4,100,0],[4,5,500,0],[5,3,15,0],[5,4,75,0],[5,5,200,0]],"numberOfCellValues":11}';

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === 'false') {

        } else {
            // прибавляем к балансу предыдущий выигрышь
            $_SESSION['balance'] = round(($_SESSION['balance'] + $_SESSION['allWin']), 2);
        }

        $_SESSION['allWin'] = 0; // обнуляем общий выигрышь
        $this->allWin = 0;

        if ($data === []) {
            //$info = $this->generatingValuesForCells($gameRules);
            $info = $this->generatingValuesForCellsV2();
            //$info = [1,1,1,2,2,2,3,3,3,4,4,4,5,5,5]; // false
            //$info = [1,2,6,1,4,4,3,3,3,7,3,4,5,7,9]; // win
            //$info = [7,1,5,0,5,4,4,5,1,5,4,8,1,2,7];
            //$info = [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1]; // freespin
            //$info = [1,6,7,4,5,7,7,4,0,7,5,6,1,5,4];
            //$info = [1,5,2,2,6,0,3,5,6,8,6,7,5,7,6]; // дробный выишрышь при ставке на линию 0.03$
            //$info = [8,7,3,1,3,2,3,8,5,8,3,2,8,5,6];
        } else {
            $info = $data;
        }

        // для возможности управления результатом с фронта
        if (Config::get('app.mod') === 'dev') {
            if (isset($_GET['code'])) {
                if ($_GET['code'] === 'lose') {
                    $info = [1,1,1,2,2,2,3,3,3,4,4,4,5,5,5];
                }
                if ($_GET['code'] === 'win') {
                    $info = [1,2,6,2,4,4,0,1,3,0,3,4,5,7,9];
                }
                if ($_GET['code'] === 'freespin') {
                    if (isset($spinResultData['freeSpinData'])) {
                        if ($spinResultData['freeSpinData'] == false) {
                            $info = [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1];
                        }
                    } else {
                        $info = [5,2,10,10,1,3,3,5,2,0,9,6,9,3,1];
                    }
                }
            }
        }

        $this->info = $info;

        $winBonusSymbolsData = $this->getWinBonusSymbolsData($gameRules, $info, $this->bonusValue); // [count, winValue]

        $startFreeSpin = false;
        $_SESSION['startFreeSpin'] = false;
        if ($this->checkBonusGame($info, $this->bonusValue) === 'bonus') {
            if (!$_SESSION['freeSpinData']) {
                $startFreeSpin = true;
                $_SESSION['startFreeSpin'] = true;
            }
        }

        $winLinesData = $this->getWinLinesData($gameRules, $info, $linesInGame);
        $winCellInfo = $this->getWinCellInfo($winLinesData, $gameRules, $info, $winBonusSymbolsData);
        $this->winCellInfo = $winCellInfo;
        $this->wl = $this->getWl($gameRules, $winLinesData, $betLine);

        if ($startFreeSpin) {
            $this->firstStartFreeSpin($betLine, $linesInGame, $winBonusSymbolsData, $gameRules, $winLinesData);
        } else {
            if ($_SESSION['freeSpinData'] != false || $_SESSION['check0FreeSpin'] != false) {
                $this->freeSpinIteration($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame, $info);
            } else {
                $this->allWin += round($this->getAllWinWithBonus($gameRules, $winLinesData, $betLine), 2);

                if ($this->allWin >= 2500 && $_SESSION['freeSpinData'] === false) {
                    return $this->getSpinResultData($betLine, $linesInGame, 'lifeOfLuxury');
                }

                if (Config::get('app.mod') === 'dev' || $_SESSION['demo'] !== false) {
                    $this->balance = round(($_SESSION['balance'] - $betLine * $linesInGame), 2);
                }

                $_SESSION['allWin'] = round(($_SESSION['allWin'] + $this->allWin), 2);
            }
        }

        $_SESSION['balance'] = round($this->balance, 2);

        // info для Api
        $infoForApi = $this->makeInfoForApi($info);

        $spinResultData = [
            'info' => $info,
            'allWin' => round($this->allWin, 2),
            'betLine' => $betLine,
            'linesInGame' => $linesInGame,
            'winCellInfo' => $winCellInfo,
            'wl' => $this->wl,
            'status' => true,
            'balance' => round($this->balance, 2),
            'rope' => $this->rope,
            'winBonusSymbolsData' => $winBonusSymbolsData,
            'freeSpinData' => $_SESSION['freeSpinData'],
            'check0FreeSpin' => false, // параметр, который нужен для отправки запроса на сервер на последнем фриспине
            'info_for_api' => $infoForApi,
            'winLinesData' => $winLinesData
        ];

        if (
            !isset($spinResultData['allWin']) ||
            !isset($spinResultData['info_for_api'])
        ) {
            throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
        }

        $freeSpinForBridge = false;
        if ($spinResultData['freeSpinData'] != false) {
            $freeSpinForBridge = true;
        } elseif($_SESSION['check0FreeSpin'] !== false) {
            $freeSpinForBridge = true;
        }

        // отправка moveFunds с учетом данных результата хода
        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            if ($spinResultData['allWin'] > 0) {
                $eventType = 'Win';
            } else {
                $eventType = 'Lose';
            }

            $platformId = 1;
            if (isset($_GET['platformId'])) {
                $platformId = $_GET['platformId'];
            }
            $responseMoveFunds = (new BridgeApiService())->moveFunds(array(
                'token' => $_SESSION['token'],
                'userId' => $_SESSION['userId'],
                'gameId' => $_SESSION['gameId'],
                'direction' => '',
                'platformId' => $platformId,
                'eventType' => $eventType,
                'amount' => round($linesInGame * $betLine, 2),
                'extraInfo' => ['selected' => [$betLine * $linesInGame],
                                'result' => $spinResultData['info_for_api'],
                                'featureGame' => $freeSpinForBridge
                ],
                'allWin' => $spinResultData['allWin'],
                'eventID' => $_SESSION['eventID']
            ), 'end_of_spin');

            if (!isset(json_decode($responseMoveFunds)->status)) {
                throw new MoveFundsException(serialize($responseMoveFunds));
            }
            if (json_decode($responseMoveFunds)->status === false) {
                throw new MoveFundsException(serialize($responseMoveFunds));
            }

            // отправка статистики в бд
            if ($_SESSION['test'] === true) {
                $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'yes')->get()->first();
            } else {
                $stat = (new Stat)->where('name', '=', 'lifeOfLuxury')->where('demo', '=', 'no')->get()->first();
            }

            $freeSpinAllWin = 0;
            $allWinOnMainLocation = 0;
            if ($stat !== null) {
                // подсчет суммы всех ставок
                if ($spinResultData['freeSpinData'] == false && $spinResultData['check0FreeSpin'] == false) {
                    $stat->iteration_count += 1;
                    $stat->sum_bet += $betLine * $linesInGame;
                }

                // выигрышь в основной игре и подсчет выигрышных/проигрышных спинов
                if ($spinResultData['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false && $spinResultData['allWin'] > 0) {
                    $allWinOnMainLocation += $spinResultData['allWin'];
                    $stat->money_returned_on_main_game += $spinResultData['allWin'];
                    $stat->number_of_winning_spins += 1;
                } elseif ($spinResultData['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false && $spinResultData['allWin'] == 0) {
                    $stat->number_of_losing_spins += 1;
                }

                if ($_SESSION['startFreeSpin'] === true) {
                    $stat->number_of_winning_spins += 1;
                }

                // подсчет выигрышных/проигрышных спинов в фриспинах
                if ($spinResultData['freeSpinData'] !== false || $spinResultData['check0FreeSpin'] !== false) {
                    if ($spinResultData['allWin'] > 0) {
                        //$stat->number_of_winning_spins += 1;
                    } else {
                        //$stat->number_of_losing_spins += 1;
                    }
                }

                // прибавляется выигрышь от фриспинов и +1 к кол-ву фриспинов
                if ($_SESSION['freeSpinData'] != false || $_SESSION['check0FreeSpin'] != false) {
                    $freeSpinAllWin += $this->allWin;
                    $stat->money_returned_on_bonus_game += $this->allWin;

                    if ($_SESSION['startFreeSpin'] === false) {
                        $stat->number_of_bonus_game += 1;
                    }
                }

                $stat->sum_win += $freeSpinAllWin + $allWinOnMainLocation;
                $stat->data = '';
                $stat->save();
            }

        }

        if ($_SESSION['check0FreeSpin'] === true) {
            $_SESSION['check0FreeSpin'] = false;
        }

        return $spinResultData;
    }

    public function getWinCellInfo($winLinesData, $gameRules, $info, $winBonusSymbolsData)
    {
        // получаем позиции выйгравших ячеек и их значения
        $winCellPositions = [];
        $winCellInfo = [];
        foreach ($winLinesData as $item) { //получаем информацию по отдельной выигрышной линии
            if ($item !== 0) {
                $savedKey = -1;
                foreach (json_decode($gameRules)->lines[$item[0]] as $key => $cellPosition) {
                    // проверяем есть ли в данной линии символ-joker
                    if ($info[$cellPosition] == $this->jokerValue) {
                        if ($savedKey === ($key - 1)) {
                            $this->checkWinJoker = true;
                            $winCellPositions[] = [$cellPosition, $this->jokerValue]; // записываем позицию ячейки и ее значение
                            $savedKey += 1;
                        }
                    }

                    if ($info[$cellPosition] == $item[1]) {
                        if ($savedKey === ($key - 1)) {
                            $winCellPositions[] = [$cellPosition, $item[1]]; // записываем позицию ячейки и ее значение
                            $savedKey += 1;
                        }
                    }
                }
            }
        }


        // получение позиций и значений выигрышных ячеек
        for ($i = 0; $i < 15; $i++) {
            $value = false;

            foreach ($winCellPositions as $winCellPosition) {
                if ($winCellPosition[0] == $i) {
                    $value = $winCellPosition[1];
                    break;
                }
            }

            $winCellInfo[] = $value;
        }

        // добавление символов и джокеров вызывающих фриспин
        if ($winBonusSymbolsData[0] != 0) {
            foreach ($info as $key => $item) {
                if ($item === $this->bonusValue) {
                    $winCellInfo[$key] = $this->bonusValue;
                }

                if ($item === $this->jokerValue) {
                    $winCellInfo[$key] = $this->jokerValue;
                }
            }
        }

        return $winCellInfo;
    }

    /**
     * Получение выигрышных линий
     */
    protected function getWl($gameRules, $winLinesData, $betLine)
    {
        $winOfLines = [];
        for ($i = 1; $i <= 15; $i++) {
            foreach ($winLinesData as $winLineData) {
                if (($winLineData[0] + 1) != $i) {
                    $winOfLines[$i] = 0;
                } else {
                    foreach (json_decode($gameRules)->winRules as $rule) {
                        if ($rule[0] == $winLineData[1]) {
                            // проверка есть ли в выигрышной линии джокер
                            foreach (json_decode($gameRules)->lines[$winLineData[0]] as $cellNumber) {
                                if ($this->winCellInfo[$cellNumber] === $this->jokerValue) {
                                    $this->checkWinJoker = true;
                                    break;
                                } else {
                                    $this->checkWinJoker = false;
                                }
                            }

                            if ($rule[1] == $winLineData[2]) {
                                if ($_SESSION['freeSpinData'] != false || $_SESSION['freeSpinResultAllWin'] != false) {
                                    if ($this->checkWinJoker) {
                                        $winOfLines[$i] = $betLine * $rule[2] * $_SESSION['freeSpinData']['mul'] * 2;
                                    } else {
                                        $winOfLines[$i] = $betLine * $rule[2] * $_SESSION['freeSpinData']['mul'];
                                    }
                                } else {
                                    if ($this->checkWinJoker) {
                                        $winOfLines[$i] = $betLine * $rule[2] * 2;
                                    } else {
                                        $winOfLines[$i] = $betLine * $rule[2];
                                    }
                                }


                            }
                        }
                    }
                    break;
                }
            }
        }

        $_SESSION['freeSpinResultAllWin'] = false;

        foreach ($winOfLines as $key => $value) {
            $winOfLines[$key] = round($value, 2);
        }

        return $winOfLines;
    }


    /**
     * Получение выигрыша с учетов бонусной игры и выигрыша не по линиям
     *
     * @param $gameRules
     * @param $winLinesData
     * @param $betLine
     * @param $winBonusSymbolsData
     */
    public function getAllWinWithBonus($gameRules, $winLinesData, $betLine)
    {
        $this->allWin = round($this->getAllWin($gameRules, $winLinesData, $betLine), 2);
    }

    /**
     * Запуск игры freeSpin
     *
     * @param $betLine
     * @param $linesInGame
     * @param $winBonusSymbolsData
     */
    public function firstStartFreeSpin($betLine, $linesInGame, $winBonusSymbolsData, $gameRules, $winLinesData)
    {
        $this->rope = $this->getRope(); // ['count' => ... , 'mul' => ..., 'allWin' => ...]
        $this->getAllWinWithBonus($gameRules, $winLinesData, $betLine);

        $_SESSION['freeSpinMul'] = false;
        $_SESSION['startFreeSpin'] = true;

//        if ($this->checkWinJoker) {
//            $this->rope['allWin'] = $winBonusSymbolsData[1] * 2 * $betLine * $linesInGame + $this->allWin;
//        } else {
//            $this->rope['allWin'] = $winBonusSymbolsData[1] * $betLine * $linesInGame + $this->allWin;
//        }

        $this->rope['allWin'] = round(($winBonusSymbolsData[1] * $betLine * $linesInGame + $this->allWin), 2);

        $_SESSION['linesInGame'] = $linesInGame;
        $_SESSION['freeSpinData'] = $this->rope;
        $_SESSION['freeSpinMul'] = $_SESSION['freeSpinData']['mul'];

        $this->allWin = round($this->rope['allWin'], 2);

        if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
            $this->balance = round($_SESSION['balance'], 2);
        } else {
            $this->balance = round(($_SESSION['balance'] - $betLine * $linesInGame), 2); // выигрыш прибавляется на следующем ходу
        }
    }

    /**
     * Итерация в игре freeSpin
     *
     * @param $gameRules
     * @param $winLinesData
     * @param $betLine
     */
    public function freeSpinIteration($gameRules, $winLinesData, $betLine, $winBonusSymbolsData, $linesInGame, $info)
    {
        $_SESSION['startFreeSpin'] = false;
        $freeSpinData = $_SESSION['freeSpinData'];

        $oldMul = $freeSpinData['mul'];
        $_SESSION['freeSpinMul'] = $freeSpinData['mul'];
        $newMul = $freeSpinData['mul'];

        $newCount = $this->correctionFreeSpinCounter();

        if (($freeSpinData['count'] - 1) === 0) {
            $this->allWin = round($this->getAllWin($gameRules, $winLinesData, $betLine) * $oldMul, 2);

            $_SESSION['check0FreeSpin'] = true;

            //$_SESSION['balance'] = $_SESSION['balance'] + $freeSpinData['allWin'];
            $this->balance = round($_SESSION['balance'], 2);

            $_SESSION['freeSpinMul'] = $newMul;

            $_SESSION['allWin'] = round($this->allWin, 2);
            $_SESSION['freeSpinData'] = false;
            $_SESSION['linesInGame'] = false;

            return false;
        } elseif ($freeSpinData['count'] < 0) {
            $_SESSION['freeSpinData'] = false;
            $_SESSION['check0FreeSpin'] = false;
            $_SESSION['linesInGame'] = false;

            return false;
        } else {
            // не первая и не последняя итерация в фриспинах
            $_SESSION['freeSpinResultAllWin'] = round($this->allWin, 2); // сохранение информации о выигрые в freespin

            foreach ($info as $item) {
                if ($item === $this->jokerValue && $newMul < 29) {
                    $newMul += 1;
                    $_SESSION['freeSpinData']['mul'] += 1; // в ответе отправляется множитель которое будет использовано на следующем ходу
                }
            }

            $this->allWin = round($this->getAllWin($gameRules, $winLinesData, $betLine) * $oldMul, 2);
            $this->balance = round($_SESSION['balance'], 2);
        }

        $_SESSION['freeSpinData']['count'] = $newCount;

    }

    /**
     * Получение выигрыша от бонусного символа
     *
     * @param $gameRules
     * @param array $info
     * @param int $bonusValue
     * @return array
     */
    public function getWinBonusSymbolsData($gameRules, array $info, int $bonusValue)
    {
        $bonusSymbolCount = 0;
        foreach ($info as $item) {
            if ($item === $bonusValue || $item === $this->jokerValue) {
                $bonusSymbolCount += 1;
            }
        }

        // в случае если не достаточно бонусных символов, то выходим
        if ($bonusSymbolCount <= 2) {
            return [0, 0];
        }

        $winValue = 0;
        foreach (json_decode($gameRules)->winRules as $item) {
            if ($item[0] === $bonusValue) {
                if ($bonusSymbolCount <= 5) {
                    if ($item[1] === $bonusSymbolCount) {
                        $winValue = $item[2];
                        break;
                    }
                } else {
                    // если бонусных символов больше чем пять, то отбираем наибольший возможный выигрыш
                    if ($winValue < $item[2]) {
                        $winValue = $item[2];
                    }
                }
            }
        }

        $winBonusSymbolsData = [$bonusSymbolCount, $winValue];

        return $winBonusSymbolsData;
    }

    public function getCountCellValuesOnLinePlusJokers(array $countCellValuesOnLine, int $jokerCount)
    {
        foreach ($countCellValuesOnLine as $key => $item) {
            if ($key !== $this->jokerValue && $key !== $this->bonusValue) {
                $countCellValuesOnLine[$key] += $jokerCount;
            }
        }

        return $countCellValuesOnLine;
    }

    /**
     * Получение данных о выигрышных линиях
     *
     * @return $winLinesData [[номер линии, значение, кол-во значений], ...]
     */
    public function getWinLinesData($gameRules, $info, $linesInGame)
    {
        $winLinesData = [];

        if ($_SESSION['freeSpinData']) {
            $linesInGame = $_SESSION['linesInGame'];
        }

        // прогоняем массив содержащий значения позиций ячеек для каждой из линий
        foreach (json_decode($gameRules)->lines as $lineNumber => $cellNumbers) {

            $winLineData = [];

            $countCellValuesOnLine = $this->getCountCellValuesOnLine($cellNumbers, $info);

            //получаем кол-во джокеров на линии
            $jokerCount = $this->getCountJokersOnLine($countCellValuesOnLine, $this->jokerValue);

            $countCellValuesOnLinePlusJokers = $this->getCountCellValuesOnLinePlusJokers($countCellValuesOnLine, $jokerCount);

            if ($lineNumber < $linesInGame) {

                // определяем является ли линия выигрышной
                // $winLineData = [номер линии, значение, кол-во значений]

                $winLineDataArray = [];

                foreach ($countCellValuesOnLinePlusJokers as $value => $valueCounter) {
                    if ($value === 1 || $value === 9) {
                        $needCount = 2;
                    } else {
                        $needCount = 3;
                    }

                    if ($valueCounter >= $needCount) {
                        $checkStep = false;
                        if ($info[$cellNumbers[0]] === $value || $info[$cellNumbers[0]] === $this->jokerValue) {
                            if ($info[$cellNumbers[0]] !== 10) {
                                $checkStep = true;
                                for ($i = 0; $i < ($valueCounter - 1); $i++) {

                                    if ($value !== $info[$cellNumbers[$i + 1]] && $info[$cellNumbers[$i + 1]] !== $this->jokerValue) {
                                        if ($value === 1 || $value === 9) {
                                            if ($i > 0) {
                                                $winLineData[0] = $lineNumber;
                                                $winLineData[1] = $value;
                                                $winLineData[2] = $i + 1;

                                                $winLineDataArray[] = $winLineData;

                                                break(2);
                                            }
                                        } else {
                                            if ($i > 1) {
                                                $winLineData[0] = $lineNumber;
                                                $winLineData[1] = $value;
                                                $winLineData[2] = $i + 1;

                                                $winLineDataArray[] = $winLineData;

                                                break(2);
                                            }
                                        }

                                        $checkStep = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if ($checkStep === true) {
                            $winLineData[0] = $lineNumber;
                            $winLineData[1] = $value;
                            $winLineData[2] = $valueCounter;

                            $winLineDataArray[] = $winLineData;
                        }
                    }
                }

                $winLinesData[] = $this->getMaxWinLineData($winLineDataArray); // получаем наиболее стоящий выигрышь
            }
        }

        $winLinesData = $this->trimEmptyWinLineData($winLinesData); // отсекаем пустые наборы данных

        return $winLinesData;
    }

    /**
     * Получение значений для бонусной игры
     *
     * @return array|string
     */
    private function getRope()
    {
        $freeSpinData = ['count' => 10, 'mul' => 2, 'allWin' => 0];

        return $freeSpinData;
    }

    /**
     * Генерация значений для ячеек
     *
     * @return array $valueForCells
     */
    public function generatingValuesForCells($gameRules)
    {
        $allValuesArray = [];
        for ($i = 0; $i <= $this->maxNOVGFA; $i++) {
            // if ($_SESSION['bet'] < 0.5) {
            //     if ($i < 261) {
            //         $allValuesArray[] = 8;
            //     } elseif ($i > 260 && $i < 363) {
            //         $allValuesArray[] = 2;
            //     } elseif ($i > 362 && $i < 466) {
            //         $allValuesArray[] = 7;
            //     } elseif ($i > 465 && $i < 541) {
            //         $allValuesArray[] = 6;
            //     } elseif ($i > 540 && $i < 671) {
            //         $allValuesArray[] = 5;
            //     } elseif ($i > 670 && $i < 831) {
            //         $allValuesArray[] = 3;
            //     } elseif ($i > 830 && $i < 881) {
            //         $allValuesArray[] = 5;
            //     } elseif ($i > 880 && $i < 956) {
            //         $allValuesArray[] = 4;
            //     } elseif ($i > 955 && $i < 986) {
            //         $allValuesArray[] = 9;
            //     } elseif ($i > 985 && $i < 992) {
            //         $allValuesArray[] = 1;
            //     } elseif ($i > 991 && $i < 995) {
            //         $allValuesArray[] = 10; // монеты для фриспинов
            //     } elseif ($i > 994 && $i < $this->maxNOVGFA) {
            //         $allValuesArray[] = 0; // джокер
            //     }
            //
            // } else {
            //
            // }

            // if ($_SESSION['freeSpinData'] === false) { // подогнанные проценты 15 11 18
            //   if ($i < 208) {
            //       $allValuesArray[] = 8;
            //   } elseif ($i > 207 && $i < 408) {
            //       $allValuesArray[] = 2;
            //   } elseif ($i > 407 && $i < 518) {
            //       $allValuesArray[] = 7;
            //   } elseif ($i > 517 && $i < 618) {
            //       $allValuesArray[] = 6;
            //   } elseif ($i > 617 && $i < 718) {
            //       $allValuesArray[] = 5;
            //   } elseif ($i > 717 && $i < 818) {
            //       $allValuesArray[] = 3;
            //   } elseif ($i > 817 && $i < 888) {
            //       $allValuesArray[] = 5;
            //   } elseif ($i > 887 && $i < 938) {
            //       $allValuesArray[] = 4;
            //   } elseif ($i > 937 && $i < 968) {
            //       $allValuesArray[] = 9;
            //   } elseif ($i > 967 && $i < 979) {
            //       $allValuesArray[] = 1;
            //   } elseif ($i > 978 && $i < 994) {
            //       $allValuesArray[] = 10; // монеты для фриспинов
            //   } elseif ($i > 993 && $i < $this->maxNOVGFA) {
            //       $allValuesArray[] = 0; // джокер
            //   }
            // } else {
            //     if ($i < 306) {
            //         $allValuesArray[] = 8;
            //     } elseif ($i > 305 && $i < 506) {
            //         $allValuesArray[] = 2;
            //     } elseif ($i > 505 && $i < 606) {
            //         $allValuesArray[] = 7;
            //     } elseif ($i > 605 && $i < 706) {
            //         $allValuesArray[] = 6;
            //     } elseif ($i > 705 && $i < 776) {
            //         $allValuesArray[] = 5;
            //     } elseif ($i > 775 && $i < 836) {
            //         $allValuesArray[] = 3;
            //     } elseif ($i > 835 && $i < 876) {
            //         $allValuesArray[] = 5;
            //     } elseif ($i > 875 && $i < 958) {
            //         $allValuesArray[] = 4;
            //     } elseif ($i > 957 && $i < 983) {
            //         $allValuesArray[] = 9;
            //     } elseif ($i > 982 && $i < 992) {
            //         $allValuesArray[] = 1;
            //     } elseif ($i > 991 && $i < $this->maxNOVGFA) {
            //         $allValuesArray[] = 0; // джокер
            //     }
            // }

        //     if ($_SESSION['bet'] < 0.5) {
        //         if ($_SESSION['freeSpinData'] === false) {
        //           if ($i < 160) { // 16
        //               $allValuesArray[] = 8;
        //           } elseif ($i > 159 && $i < 310) { // 15
        //               $allValuesArray[] = 2;
        //           } elseif ($i > 309 && $i < 450) { // 14
        //               $allValuesArray[] = 7;
        //           } elseif ($i > 449 && $i < 560) { // 11
        //               $allValuesArray[] = 6;
        //           } elseif ($i > 559 && $i < 660) { // 10
        //               $allValuesArray[] = 3;
        //           } elseif ($i > 659 && $i < 750) { // 9
        //               $allValuesArray[] = 5;
        //           } elseif ($i > 749 && $i < 820) { // 7
        //               $allValuesArray[] = 4;
        //           } elseif ($i > 819 && $i < 885) { // 6,5
        //               $allValuesArray[] = 9;
        //           } elseif ($i > 884 && $i < 940) { // 5,5
        //               $allValuesArray[] = 1;
        //           } elseif ($i > 939 && $i < 955) { // 1,5
        //               $allValuesArray[] = 10; // монеты для фриспинов
        //           } elseif ($i > 954 && $i < $this->maxNOVGFA) { // 4,5
        //               $allValuesArray[] = 0; // джокер
        //           }
        //         }
        //     } else {
        //

            if ($_SESSION['freeSpinData'] === false) {
                if ($i < 120) { // 12
                    $allValuesArray[] = 8;
                } elseif ($i > 119 && $i < 230) { // 11
                    $allValuesArray[] = 2;
                } elseif ($i > 229 && $i < 340) { // 11
                    $allValuesArray[] = 7;
                } elseif ($i > 339 && $i < 450) { // 11
                    $allValuesArray[] = 6;
                } elseif ($i > 449 && $i < 560) { // 11
                    $allValuesArray[] = 3;
                } elseif ($i > 559 && $i < 660) { // 10
                    $allValuesArray[] = 5;
                } elseif ($i > 659 && $i < 760) { // 10
                    $allValuesArray[] = 4;
                } elseif ($i > 759 && $i < 860) { // 10
                    $allValuesArray[] = 9;
                } elseif ($i > 859 && $i < 955) { // 9,5
                    $allValuesArray[] = 1;
                } elseif ($i > 954 && $i < 975) { // 2
                    $allValuesArray[] = 10; // монеты для фриспинов
                } elseif ($i > 974 && $i < $this->maxNOVGFA) { // 2,5
                    $allValuesArray[] = 0; // джокер
                }
            } else {
                if ($i < 200) { // 20
                    $allValuesArray[] = 8;
                } elseif ($i > 199 && $i < 360) { // 16
                    $allValuesArray[] = 2;
                } elseif ($i > 359 && $i < 500) { // 14
                    $allValuesArray[] = 7;
                } elseif ($i > 499 && $i < 620) { // 12
                    $allValuesArray[] = 6;
                } elseif ($i > 619 && $i < 720) { // 10
                    $allValuesArray[] = 3;
                } elseif ($i > 719 && $i < 810) { // 9
                    $allValuesArray[] = 5;
                } elseif ($i > 809 && $i < 890) { // 7
                    $allValuesArray[] = 4;
                } elseif ($i > 889 && $i < 950) { // 6
                    $allValuesArray[] = 9;
                } elseif ($i > 949 && $i < 990) { // 4
                    $allValuesArray[] = 1;
                } elseif ($i > 989 && $i < $this->maxNOVGFA) { // 1
                    $allValuesArray[] = 0; // джокер
                }
            }
        }

        //     }
        // }

        $valueForCells = [];

        for ($i = 0; $i < 15; $i++) {
            if ($i < 3 || $i > 11) { // исключаем из первого и последнего барабана алмазы
                $valueForCells[] = $allValuesArray[rand(0, 990)];
            } else {
                $valueForCells[] = $allValuesArray[rand(0, $this->maxNOVGFA - 1)];
            }
        }

        $check = true;
        while ($check) {
            $newValueForCells = $this->updateValueForCells($valueForCells, $allValuesArray);
            if ($newValueForCells === $valueForCells) {
                $check = false;
            }

            $valueForCells = $newValueForCells;
        }

        if ($_SESSION['freeSpinData'] != false) {
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === $this->bonusValue) {
                    $valueForCells[$key] = 10;
                }
            }
        }

        return $valueForCells;
    }

    public function generatingValuesForCellsV2()
    {
        $cellsRows = [[],[],[],[],[]];
        for ($i=0; $i < 5; $i++) { // уровень отдельного барабана

            $cellRow = $cellsRows[$i];

            for ($k=0; $k < 3; $k++) { // уроверь отдельной ячейки в барабане
                $cellRow[$k] = $this->getCellValue($cellRow, $i);

            }

            $cellsRows[$i] = $cellRow;
        }

        $valuesForCells = [];
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 3; $k++) {
                $valuesForCells[] = $cellsRows[$i][$k];
            }
        }

        return $valuesForCells;
    }

    public function getCellValue($cellRow, $i)
    {
        // задание вероятностей и исключение алмазов
        if ($_SESSION['freeSpinData'] === false) {
            if ($i === 0 || $i === 4) { // убираются алмазы из крайних барабанов
                $percentArr = [8 => [0, 15], 2 => [15, 27], 7 => [27, 40], 6 => [40, 52], 3 => [52, 63], 5 => [63, 73], 4 => [73, 83], 9 => [83, 91], 1 => [91, 97.5], 10 => [97.5, 100],  0 => [0, 0]];
            } else {
                $percentArr = [8 => [0, 15], 2 => [15, 27], 7 => [27, 39], 6 => [39, 50], 3 => [50, 61], 5 => [61, 71], 4 => [71, 81], 9 => [81, 89], 1 => [89, 95.5], 10 => [95.5, 97.5],  0 => [97.5, 100]];
            }
        } else { // убираются монеты
            // убираются алмазы
            if ($i === 0) {
                $percentArr = [8 => [0, 30], 2 => [30, 50], 7 => [50, 65], 6 => [65, 80], 3 => [80, 88], 5 => [88, 96], 4 => [96, 98], 9 => [98, 99], 1 => [99, 100], 10 => [0, 0],  0 => [0, 0]];
            }
            if ($i === 1) {
                $percentArr = [8 => [0, 30], 2 => [30, 50], 7 => [50, 65], 6 => [65, 80], 3 => [80, 88], 5 => [88, 96], 4 => [96, 98], 9 => [98, 99], 1 => [99, 100], 10 => [0, 0],  0 => [0, 0]];
            }
            if ($i === 2) {
                $percentArr = [8 => [0, 30], 2 => [30, 50], 7 => [50, 65], 6 => [65, 80], 3 => [80, 88], 5 => [88, 96], 4 => [96, 98], 9 => [98, 99], 1 => [99, 95], 10 => [0, 0],  0 => [95, 100]];
            }
            if ($i === 4) {
                $percentArr = [8 => [0, 1], 2 => [1, 3], 7 => [3, 5], 6 => [5, 10], 3 => [10, 20], 5 => [20, 35], 4 => [35, 50], 9 => [50, 80], 1 => [80, 95], 10 => [0, 0],  0 => [95, 100]];
            }

            if ($i === 3) {
                $percentArr = [8 => [0, 1], 2 => [1, 3], 7 => [3, 5], 6 => [5, 10], 3 => [10, 20], 5 => [20, 35], 4 => [35, 50], 9 => [50, 80], 1 => [80, 95], 10 => [0, 0],  0 => [95, 100]];
            }
        }

        // if ($_SESSION['freeSpinData'] === false) { // 29 11 18
        //     if ($i === 0 || $i === 4) { // убираются алмазы из крайних барабанов
        //         $percentArr = [8 => [0, 15], 2 => [15, 27], 7 => [27, 40], 6 => [40, 52], 3 => [52, 63], 5 => [63, 73], 4 => [73, 83], 9 => [83, 91], 1 => [91, 97.5], 10 => [97.5, 100],  0 => [0, 0]];
        //     } else {
        //         $percentArr = [8 => [0, 15], 2 => [15, 27], 7 => [27, 39], 6 => [39, 50], 3 => [50, 61], 5 => [61, 71], 4 => [71, 81], 9 => [81, 89], 1 => [89, 95.5], 10 => [95.5, 97.5],  0 => [97.5, 100]];
        //     }
        // } else { // убираются монеты
        //     if ($i === 0 || $i === 4) { // убираются алмазы из крайних барабанов
        //         $percentArr = [8 => [0, 19], 2 => [19, 37], 7 => [37, 51], 6 => [51, 63], 3 => [63, 73], 5 => [73, 82], 4 => [82, 90], 9 => [90, 96], 1 => [96, 100], 10 => [0, 0],  0 => [0, 0]];
        //     } else {
        //         $percentArr = [8 => [0, 17.5], 2 => [17.5, 33.5], 7 => [33.5, 47.5], 6 => [47.5, 59.5], 3 => [59.5, 69.5], 5 => [69.5, 78.5], 4 => [78.5, 86.5], 9 => [86.5, 95.5], 1 => [95.5, 96.5], 10 => [0, 0],  0 => [96.5, 100]];
        //     }
        // }

        // if ($_SESSION['freeSpinData'] === false) { // версия 27 11 18
        //     if ($i === 0 || $i === 4) { // убираются алмазы из крайних барабанов
        //         $percentArr = [8 => [0, 14.5], 2 => [14.5, 25.5], 7 => [25.5, 36.5], 6 => [36.5, 47.5], 3 => [47.5, 58.5], 5 => [58.5, 68.5], 4 => [68.5, 78.5], 9 => [78.5, 88.5], 1 => [88.5, 97.5], 10 => [97.5, 100],  0 => [0, 0]];
        //     } else {
        //         $percentArr = [8 => [0, 12], 2 => [12, 23], 7 => [23, 34], 6 => [34, 45], 3 => [45, 56], 5 => [56, 66], 4 => [66, 76], 9 => [76, 86], 1 => [86, 95.5], 10 => [95.5, 97.5],  0 => [97.5, 100]];
        //     }
        // } else { // убираются монеты
        //     if ($i === 0 || $i === 4) { // убираются алмазы из крайних барабанов
        //         $percentArr = [8 => [0, 21], 2 => [21, 37], 7 => [37, 51], 6 => [51, 63], 3 => [63, 73], 5 => [73, 82], 4 => [82, 90], 9 => [90, 96], 1 => [96, 100], 10 => [0, 0],  0 => [0, 0]];
        //     } else {
        //         $percentArr = [8 => [0, 20], 2 => [20, 36], 7 => [36, 50], 6 => [50, 62], 3 => [62, 72], 5 => [72, 81], 4 => [81, 89], 9 => [89, 95], 1 => [95, 99], 10 => [0, 0],  0 => [99, 100]];
        //     }
        // }

        $cellValue = '123123';
        $rand = rand(0, 99);
        foreach ($percentArr as $key => $percentRang) { // поиск выпавшего значения ячейки
            if ($rand >= $percentRang[0] && $rand < $percentRang[1]) {
                $cellValue = $key;
                break;
            }
        }

        // исключение возможности выпадения на одном барабане монеты и алмаза
        if ($cellValue === 10 && in_array(2, $cellRow)) {
            return $this->getCellValue($cellRow, $i);
        }

        if (!in_array($cellValue, $cellRow) && $cellValue !== null) { // проверка есть ли уже в ряде такая ячейка
            return $cellValue;
        } else {
            return $this->getCellValue($cellRow, $i);
        }
    }

    /**
     * Проверка наличия более 5 одинаковых символов
     *
     * @param $valueCounts
     * @return bool|array Возвращает false, либо значение символа, которого больше 5 и его кол-во
     */
    public function checkHave5Symbols($valueCounts)
    {
        foreach ($valueCounts as $value => $count) {
            if ($count > 5) {
                return [$value, $count];
            }
        }

        return false;
    }

    /**
     * Делаем так, чтобы в столбе не было три повторяющиеся символа. Убираем из сгенерированного набора лишние символы, чтобы их было < 5.
     *
     * @param $valueForCells
     * @param $symbol
     * @return array
     */
    public function updateValueForCells($valueForCells, $allValuesArray)
    {
        // убираем лишние зоторые манеты если ставка < 50 центов
        $coinCount = 0;
        if ($_SESSION['bet'] < 0.50) {
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 10 || $valueForCell === 0) {
                    $coinCount += 1;
                }

                foreach ($valueForCells as $key2 => $cell) {
                    if ($coinCount > 2) {
                        $valueForCells[$key2] = rand(1, 9);
                    }
                }
            }
        }

        for ($i = 0; $i < 13; $i += 3) {
            if ($valueForCells[$i] === $valueForCells[$i + 1] || $valueForCells[$i] === $valueForCells[$i + 2]) {
                $valueForCells = [];

                for ($i = 0; $i < 15; $i++) {
                    $valueForCells[] = $allValuesArray[rand(0, 990)];
                }
            } elseif ($valueForCells[$i + 1] === $valueForCells[$i + 2]) {
                $valueForCells = [];

                for ($i = 0; $i < 15; $i++) {
                    $valueForCells[] = $allValuesArray[rand(0, 990)];
                }
            }
        }


        $symbol = $this->checkHave5Symbols($this->getValueCouner($valueForCells));

        for ($i = 0; $i < $symbol[1] - 5; $i++) {
            foreach ($valueForCells as $key => $cell) {
                if ($cell === $symbol[0]) {
                    if ($cell < 5) {
                        $valueForCells[$key] = $cell + 2;
                    } else {
                        $valueForCells[$key] = $cell - 2;
                    }

                    break;
                }
            }
        }

        //проверка наличина несуществующих символов
        foreach ($valueForCells as $key => $cell) {
            if ($cell === -1 || $cell === 12) {
                $valueForCells[$key] = 5;
            }
        }

        // убираем алмазы из крайних барабанов
        foreach ($valueForCells as $key => $cell) {
            if ($key < 3 || $key > 11) {
                if ($cell === 0) {
                    $valueForCells[$key] = rand(1, 9);
                }
            }
        }

        // убираем золотые монеты из фриспинов
        if ($_SESSION['freeSpinData'] != false || $_SESSION['freeSpinResultAllWin'] != false) {
            foreach ($valueForCells as $key => $cell) {
                if ($cell === 10) {
                    $valueForCells[$key] = rand(1, 9);
                }
            }
        }

        // убираем лишние золотые монеты если ставка < 50 центов
        $coinCount = 0;
        if ($_SESSION['bet'] < 0.50) {
            foreach ($valueForCells as $key => $valueForCell) {
                if ($valueForCell === 10 || $valueForCell === 0) {
                    $coinCount += 1;
                }

                foreach ($valueForCells as $key2 => $cell) {
                    if ($coinCount > 2) {
                        $valueForCells[$key2] = rand(1, 9);
                    }
                }
            }
        }

        // убираем алмазы из крайних барабанов еще раз
        foreach ($valueForCells as $key => $cell) {
            if ($key < 3 || $key > 11) {
                if ($cell === 0) {
                    $valueForCells[$key] = rand(1, 9);
                }
            }
        }

        // убираем рядом стоящие алмазы и монеты
        foreach ($valueForCells as $key => $cell) {
            if ($cell === 0) {
                if (isset($valueForCells[$key - 1])) {
                    if ($valueForCells[$key - 1] === 10) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }

                if (isset($valueForCells[$key + 1])) {
                    if ($valueForCells[$key + 1] === 10) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }
            }

            if ($cell === 10) {
                if (isset($valueForCells[$key - 1])) {
                    if ($valueForCells[$key - 1] === 0) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }

                if (isset($valueForCells[$key + 1])) {
                    if ($valueForCells[$key + 1] === 0) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }
            }

            if ($cell === 10) {
                if (isset($valueForCells[$key - 1])) {
                    if ($valueForCells[$key - 1] === 10) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }

                if (isset($valueForCells[$key + 1])) {
                    if ($valueForCells[$key + 1] === 10) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }
            }

            if ($cell === 0) {
                if (isset($valueForCells[$key - 1])) {
                    if ($valueForCells[$key - 1] === 0) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }

                if (isset($valueForCells[$key + 1])) {
                    if ($valueForCells[$key + 1] === 0) {
                        $valueForCells[$key] = rand(1, 9);
                    }
                }
            }
        }

        return $valueForCells;
    }

    /**
     * Получаем кол-во каждого элемента в массиве
     *
     * @param $valueForCells
     * @return array
     */
    public function getValueCouner($valueForCells)
    {
        $valueCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($valueForCells as $cell) {
            switch ($cell) {
                case 0:
                    $valueCounts[0] += 1;
                    break;
                case 1:
                    $valueCounts[1] += 1;
                    break;
                case 2:
                    $valueCounts[2] += 1;
                    break;
                case 3:
                    $valueCounts[3] += 1;
                    break;
                case 4:
                    $valueCounts[4] += 1;
                    break;
                case 5:
                    $valueCounts[5] += 1;
                    break;
                case 6:
                    $valueCounts[6] += 1;
                    break;
                case 7:
                    $valueCounts[7] += 1;
                    break;
                case 8:
                    $valueCounts[8] += 1;
                    break;
                case 9:
                    $valueCounts[9] += 1;
                    break;
                case 10:
                    $valueCounts[10] += 1;
                    break;
            }
        }

        return $valueCounts;
    }

    // проверяем на наличие особых значений запускающих бонусную игру. Если есть, то возвращаем 'freeSpin'
    public function checkBonusGame(array $cellValues, int $bonusValue)
    {
        $counter = 0;
        $jokerCount = 0;
        $coinCount = 0;
        foreach ($cellValues as $value) {
            if ($value == $bonusValue) {
                $counter += 1;
                $coinCount += 1;
            }

            if ($value == $this->jokerValue) {
                $counter += 1;
                $jokerCount += 1;
            }
        }

        if ($coinCount === 0) {
            return false;
        } elseif($counter > 2) {
            return 'bonus';
        }
    }

    // получаем общее кол-во значений на линии
    public function getCountCellValuesOnLine(array $cellNumbers, array $cellValues)
    {
        $countCellValuesOnLine = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        foreach ($cellNumbers as $cellNumber) {
            switch ($cellValues[$cellNumber]) {
                case 0:
                    $countCellValuesOnLine[0] += 1;
                    break;
                case 1:
                    $countCellValuesOnLine[1] += 1;
                    break;
                case 2:
                    $countCellValuesOnLine[2] += 1;
                    break;
                case 3:
                    $countCellValuesOnLine[3] += 1;
                    break;
                case 4:
                    $countCellValuesOnLine[4] += 1;
                    break;
                case 5:
                    $countCellValuesOnLine[5] += 1;
                    break;
                case 6:
                    $countCellValuesOnLine[6] += 1;
                    break;
                case 7:
                    $countCellValuesOnLine[7] += 1;
                    break;
                case 8:
                    $countCellValuesOnLine[8] += 1;
                    break;
                case 9:
                    $countCellValuesOnLine[9] += 1;
                    break;
                case 10:
                    $countCellValuesOnLine[10] += 1;
                    break;
            }
        }

        return $countCellValuesOnLine;
    }


    public function getCountJokersOnLine(array $countCellValuesOnLine, int $jokerValue)
    {
        $count = $countCellValuesOnLine[$jokerValue];

        return $count;
    }

    // получаем наиболее стоящий выигрышь
    public function getMaxWinLineData(array $winLineDataArray)
    {
        $maxWinLineData = [0, 0, 0];
        foreach ($winLineDataArray as $item) {
            if ($item[2] > $maxWinLineData[2]) {
                $maxWinLineData = $item;
            }
        }

        return $maxWinLineData;
    }

    // отсекаем пустые наборы данных
    public function trimEmptyWinLineData(array $winLinesData)
    {
        $newWinLineData = [];
        foreach ($winLinesData as $item) {
            if ($item[2] !== 0) {
                $newWinLineData[] = $item;
            }
        }


        return $newWinLineData;
    }

    /**
     * Получение общей суммы выигрыша
     *
     * @return int $allWin
     */
    public function getAllWin($gameRules, $winLinesData, $betLine)
    {
        $allWin = 0;

        foreach (json_decode($gameRules)->winRules as $key => $rule) {
            foreach ($winLinesData as $winLineData) {
                if ($rule[0] == $winLineData[1]) {
                    if ($rule[1] == $winLineData[2]) {
                        $allWinItr = $betLine * $rule[2];

                        // проверка на наличие в линии joker
                        foreach (json_decode($gameRules)->lines as $key => $lineNumbres) {
                            if ($key === $winLineData[0]) {
                                foreach ($lineNumbres as $lineNumbre) {
                                    if ($this->info[$lineNumbre] === $this->jokerValue && $this->winCellInfo[$lineNumbre] === $this->jokerValue) {
                                        $allWinItr *= 2;
                                        break(2);
                                    }
                                }
                            }
                        }

                        $allWin += $allWinItr;
                    }
                }
            }
        }

        return round($allWin, 2);
    }

    public function correctionFreeSpinCounter()
    {
        // проверить наличие записи о фриспинах для данного пользователя и данной игры
        // if (Config::get('app.mod') === 'dev' && $_SESSION['demo'] !== 'false') {
        //     if ($_SESSION['freeSpinData'] !== false) {
        //         $_SESSION['freeSpinData']['count'] -= 1;
        //         $newCount = $_SESSION['freeSpinData']['count'];
        //     } else {
        //         $newCount = 0;
        //     }
        // } else {
        //     $freeSpinDB = (new SavedDataBundle())
        //         ->where('user_id', '=', $_SESSION['userId'])
        //         ->where('game_id', '=', $_SESSION['gameId'])
        //         ->where('session_name', '=', $_SESSION['sessionName'])
        //         ->get()
        //         ->last();
        //
        //     if ($freeSpinDB) {
        //         if (json_decode($freeSpinDB->data)->freeSpinData->count <= 1) {
        //             $freeSpinDB->delete();
        //
        //             $this->setNewFreeSpinDB();
        //             $newCount = $_SESSION['freeSpinData']['count'] - 1;
        //         } else {
        //             $data = json_decode($freeSpinDB->data);
        //             $countDB = $data->freeSpinData->count;
        //             $newCount = $countDB - 1; // отнимаем 1 так как не учитывается предыдущая итерация
        //
        //             // исправление значения в сессии
        //             $freeSpinData = $_SESSION['freeSpinData'];
        //             if (($freeSpinData['count'] - 1) !== $newCount) {
        //                 $freeSpinData['count'] = $newCount;
        //                 $_SESSION['freeSpinData'] = $freeSpinData;
        //             }
        //
        //             $data->freeSpinData->count = $newCount;
        //
        //             $freeSpinDB->data = json_encode($data);
        //             $freeSpinDB->save();
        //         }
        //     } else {
        //         $this->setNewFreeSpinDB();
        //         $newCount = $_SESSION['freeSpinData']['count'] - 1;
        //     }
        // }

        $_SESSION['freeSpinData']['count'] -= 1;
        $newCount = $_SESSION['freeSpinData']['count'];

        return $newCount;
    }

    public function setNewFreeSpinDB()
    {
        $freeSpinData = $_SESSION['freeSpinData'];

        // if (isset($freeSpinData['count'])) {
        //     $freeSpinData['count'] = $freeSpinData['count'] - 1;
        //     $freeSpinDB = new SavedDataBundle();
        //     $freeSpinDB->user_id = 1;
        //     $freeSpinDB->game_id = 1;
        //     $freeSpinDB->session_name = $_SESSION['sessionName'];
        //     $freeSpinDB->data = json_encode(['freeSpinData' => $freeSpinData]);
        //     $freeSpinDB->save();
        // }
    }

    public function makeInfoForApi($info) {
        $newResult = [];
        foreach ($info as $key => $value) {
            switch ($value) {
                case 0:
                    $newResult[] = 'Diamond';
                    break;
                case 1:
                    $newResult[] = 'Plane';
                    break;
                case 2:
                    $newResult[] = 'Silver';
                    break;
                case 3:
                    $newResult[] = 'Dollar';
                    break;
                case 4:
                    $newResult[] = 'Car';
                    break;
                case 5:
                    $newResult[] = 'Ring';
                    break;
                case 6:
                    $newResult[] = 'Watch';
                    break;
                case 7:
                    $newResult[] = 'Gold';
                    break;
                case 8:
                    $newResult[] = 'Bronze';
                    break;
                case 9:
                    $newResult[] = 'Yacht';
                    break;
                case 10:
                    $newResult[] = 'Coin';
                    break;

                default:
                    $newResult[] = '';
                    break;
            }
        }

        return $newResult;
    }

    public function sendBetPlacing()
    {
        try {
            if (Config::get('app.mod') !== 'dev' && $_SESSION['demo'] === false) {
                if ($_SESSION['freeSpinData'] === false) {
                    if ($_SESSION['eventID'] !== false) {
                        if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false) {
                            $_SESSION['eventID'] = Uuid::generate()->string;
                            $eventID = $_SESSION['eventID'];
                        } else {
                            $eventID = $_SESSION['eventID'];
                        }
                    } else {
                        $_SESSION['eventID'] = Uuid::generate()->string;
                        $eventID = $event->uuid->string;
                    }
                }

                if ($_SESSION['freeSpinData']['mul'] === null) {
                    $_SESSION['freeSpinData'] = false;
                }

                // отправка события о снятии средств
                if ($_SESSION['freeSpinData'] == false && $_SESSION['check0FreeSpin'] == false && $_SESSION['demo'] == false) {
                    //$data = Jackpot::firstOrFail()->toArray();

                    $platformId = 1;
                    if (isset($_GET['platformId'])) {
                        $platformId = $_GET['platformId'];
                    }
                    $requestData = array(
                        'token' => $_SESSION['token'],
                        'userId' => $_SESSION['userId'],
                        'gameId' => $_SESSION['gameId'],
                        'direction' => 'debit',
                        'platformId' => $platformId,
                        'eventType' => 'BetPlacing',
                        'amount' => $_SESSION['bet'],
                        'extraInfo' => [
                            'selected' => [$_SESSION['linesInGame']],
                            //'jackpotAmount' => $data['big_daddy']
                        ],
                        'eventID' => $eventID
                    );

                    // получение ответа от slot.pantera
                    $responseMoveFunds = (new BridgeApiService())->moveFunds($requestData);

                    // получение баланса
                    $_SESSION['balance'] = (float) json_decode($responseMoveFunds)->balance;

                    // проверка активности сессии на сервер с мост-апи (http://slot.pantera.co.ua)
                    $checkApiSession = json_decode($responseMoveFunds);
                    if (!isset($checkApiSession->status)) {
                        throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
                    }
                    if ($checkApiSession->status === false) {
                        throw new BetPlacingAbortException($linesInGame * $betLine, $linesInGame);
                    }
                }
            }
        } catch (BetPlacingAbortException $e) {
            //$e->sendBetPlacingAbort();
        } catch (BalanceException $e) {
            //return '{"status":"false"}';
        } catch (EmptySessionDataException $e) {
            if (Config::get('app.mod') === 'dev') {
                $e->setDevSessionData();
            } else {
                return response()->json('{"status":"false"}');
            }
        } catch (UnauthenticatedException $e) {
            //return '{"status":"false"}';
        }
    }
}
