<?php
namespace App\Http\Controllers\Api;

use App\Services\TestServices\TestServiceType1;
use App\Models\ProbabilityTest;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\GameServices\ElGalloSpinService;
use App\Services\GameServices\ElGallo\ElGalloSpinService2;
use App\Services\GameServices\ElGallo\ElGalloSpinService3;
use App\Models\Jackpot;
use App\Models\UserJackpot;
use App\Models\Stat;
use Ixudra\Curl\Facades\Curl;
use App\Models\Log;
use App\Models\V2Game;
use App\Models\Game;
use App\Models\SavedDataBundle;
use App\Models\Event;
use App\Models\Transaction;
use App\Models\Session;
use App\Models\MoveFundsExceptionModel;
use App\Models\BridgeApiRequest;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Вывод кол-ва записей которые есть в таблицах БД
     */
    public function showAllTables()
    {
        $count = SavedDataBundle::all()->count();
        echo 'SavedDataBundles count = ' . $count. "<br>";
        $count = Event::all()->count();
        echo 'Events count = ' . $count. "<br>";
        $count = Transaction::all()->count();
        echo 'Transactions count = ' . $count. "<br>";
        $count = Session::all()->count();
        echo 'Sessions count = ' . $count. "<br>";
        $count = MoveFundsExceptionModel::all()->count();
        echo 'MoveFundsExceptions count = ' . $count. "<br>";
        $count = UserJackpot::all()->count();
        echo 'UserJackpots count = ' . $count. "<br>";
        $count = BridgeApiRequest::all()->count();
        echo 'BridgeApiRequests count = ' . $count. "<br>";
        $count = Log::all()->count();
        echo 'Logs count = ' . $count. "<br>";
    }

    /**
     * Очистка БД от старых записей
     */
    public function clearOldData()
    {
        $startDate = Carbon::today()->subDays(7);

        $savedDataBundles = SavedDataBundle::where('created_at', '<', $startDate)->delete();
        $events = Event::where('created_at', '<', $startDate)->delete();
        $transactions = Transaction::where('created_at', '<', $startDate)->delete();
        $sessions = Session::where('created_at', '<', $startDate)->delete();
        $moveFundsExceptionModels = MoveFundsExceptionModel::where('created_at', '<', $startDate)->delete();
        $bridgeApiRequests = BridgeApiRequest::where('created_at', '<', $startDate)->delete();
        $logs = Log::where('created_at', '<', $startDate)->delete();
    }

    /**
     * Вывод всех логов которые есть в БД
     *
     * @return void
     */
    public function showLogs(Request $request)
    {
        $userId = $request->input('user_id');

        $logs = (new Log)->all();

        if (!isset($logs[0])) {
            dd('no logs');
        }

        foreach ($logs as $key => $log) {
            $data = json_decode($log->data);
            if ($userId !== null) {
                if ($data->userId === (int)$userId) {
                    echo $key . ' ; ' . $log->data . '<br>';
                }
            } else {
                echo $key . ' ; ' . $log->data . '<br>';
            }
        }
    }

    /**
     * Вывод всех логов которые есть в БД
     *
     * @return void
     */
    public function removeLogs()
    {
        $logs = (new Log)->all();

        if (!isset($logs[0])) {
            dd('no logs');
        }

        foreach ($logs as $key => $log) {
            $log->delete();
        }
    }

    /**
     * Вывод данных которые есть в БД в таблице jackpots
     *
     * @return void
     */
    public function showJackpots()
    {
        $jackpots = (new Jackpot)->all();

        if (!isset($jackpots[0])) {
            dd('no jackpots');
        }

        foreach ($jackpots as $key => $jackpot) {
            foreach ($jackpot['attributes'] as $key2 => $value2) {
                if (!is_int($key2)) {
                    echo $key2 .' = '. $value2 . '<br>';
                }
            }

            echo '<br>';
        }

        dd((new UserJackpot)->all()->toArray());
    }

    /**
     * Удаление данных о джекпотах, которые ожидают получения пользователями
     *
     * @return void
     */
    public function removeUserJackpots()
    {
        $userJackpots = (new UserJackpot)->all();
        foreach ($userJackpots as $key => $value) {
            $value->delete();
        }

        return 'done';
    }

    /**
     * Перезапись данных которые есть в БД в таблице jackpots
     *
     * @return void
     */
    public function setJackpots()
    {
        $jackpots = (new Jackpot)->all();

        if (!isset($jackpots[0])) {
            dd('no jackpots');
        }

        $jackpotDB = (new Jackpot)->find(1);
        $jackpotDB->mini = 13.19;
        $jackpotDB->result_mini = 13.19;
        $jackpotDB->minor = 63.03;
        $jackpotDB->result_minor = 63.03;
        $jackpotDB->major = 250.19;
        $jackpotDB->result_major = 250.19;
        $jackpotDB->big_daddy = 530.19;
        $jackpotDB->result_big_daddy = 530.19;
        $jackpotDB->save();

        $jackpotDB = (new Jackpot)->find(2);
        $jackpotDB->mini = 13.19;
        $jackpotDB->result_mini = 13.19;
        $jackpotDB->minor = 63.03;
        $jackpotDB->result_minor = 63.03;
        $jackpotDB->major = 250.19;
        $jackpotDB->result_major = 250.19;
        $jackpotDB->big_daddy = 530.19;
        $jackpotDB->result_big_daddy = 530.19;
        $jackpotDB->save();

        echo 'done';
    }

    public function getData()
    {
        $data = [];

        $probabilities = ProbabilityTest::all();

        foreach ($probabilities as $key => $probability) {
            if ($key === 0) {
                $data[] = [
                    'iterations' => $probability['iterations'],
                    'total_bet' => $probability['total_bet'],
                    'total_balance' => $probability['total_balance'],
                    'total_win' => $probability['total_win'],
                    'total_winnings_in_the_main_game' => $probability['total_winnings_in_the_main_game'],
                    'total_winnings_in_jackpot' => $probability['total_winnings_in_jackpot'],
                    'total_winnings_in_freespin' => $probability['total_winnings_in_freespin'],
                    'percentage_of_money_returned' => $probability['percentage_of_money_returned'],
                    'percentage_of_money_returned_om_main_game' => $probability['percentage_of_money_returned_om_main_game'],
                    'percentage_of_money_returned_on_jackpots' => $probability['percentage_of_money_returned_on_jackpots'],
                    'percentage_of_money_returned_on_freespin' => $probability['percentage_of_money_returned_on_freespin'],
                    'execution_time' => $probability['execution_time']
                ];
            } else {

            }
        }
    }

    public function testSlots(Request $request, $gameName)
    {
        $itr = 10000;
        $betLine = 1;
        $linesInGame = 25;

        $allWinOnSlots = 0;
        $allBet = 0;
        $probability = 0;
        $time = 0;

        if ($_POST) {
            $start = microtime(true);
            $betLine = $request->input('betLine');
            $linesInGame = $request->input('linesInGame');
            $itr = $request->input('itr');
            //$itr = $itr/10;

            $elGallo = new ElGalloSpinService();

            $allWinOnSlots = 0;
            for ($i = 0; $i < $itr; $i++) {
                $resultData = $elGallo->getLiteTestSpinData($betLine, $linesInGame, $gameName);
                $allWinOnSlots += $resultData['allWin'];
            }

            $allBet = $itr * $betLine * $linesInGame;
            $probability = 100 / $allBet * $allWinOnSlots;

            $time = microtime(true) - $start;

        }

        return view('tests.test', [
            'itr' => $itr,
            'betLine' => $betLine,
            'linesInGame' => $linesInGame,
            'allWinOnSlots' => $allWinOnSlots,
            'allBet' => $allBet,
            'probability' => $probability,
            'time' => $time
        ]);
    }


    public function testJackpots()
    {
        $linesInGame = 25;
        $betLine = 1;
        $itr = 10000;

        $mini = 1000;
        $minor = 5000;
        $major = 25000;
        $bigDaddy = 50000;

        $allWin = 0;
        $percentage = 0;

        $jackpots = Jackpot::find(1)->toArray();

        if ($_POST) {
            $jackpots = Jackpot::find(1);
            $jackpots->mini = $_POST['mini'] * 100;
            $jackpots->minor = $_POST['minor'] * 100;
            $jackpots->major = $_POST['major'] * 100;
            $jackpots->big_daddy = $_POST['big_daddy'] * 100;
            $jackpots->save();

            $mini = $_POST['mini'] * 100;
            $minor = $_POST['minor'] * 100;
            $major = $_POST['major'] * 100;
            $bigDaddy = $_POST['big_daddy'] * 100;
        }

        if ($_POST) {
            $jackpots = Jackpot::find(1);
            $jackpots->result_mini = $_POST['result_mini'] * 100;
            $jackpots->result_minor = $_POST['result_minor'] * 100;
            $jackpots->result_major = $_POST['result_major'] * 100;
            $jackpots->result_big_daddy = $_POST['result_big_daddy'] * 100;
            $jackpots->save();
        }


        $mini = floor($mini);
        $minor = floor($minor);
        $major = floor($major);
        $bigDaddy = floor($bigDaddy);

        return view('tests.jackpot-test', [
            'jackpots' => $jackpots,
            'mini' => $mini,
            'minor' => $minor,
            'major' => $major,
            'big_daddy' => $bigDaddy,
            'linesInGame' => $linesInGame,
            'betLine' => $betLine,
            'itr' => $itr,
            'allWin' => $allWin,
            'percentage' => $percentage
        ]);
    }

    public function jackpotProbabilityTest(Request $request)
    {
        $gameName = 'elGallo';
        $linesInGame = $request->input('linesInGame');
        $betLine = $request->input('betLine');
        $itr = $request->input('itr');
        $mini = 1000;
        $minor = 5000;
        $major = 25000;
        $bigDaddy = 50000;
        $jackpots = Jackpot::find(1)->toArray();

        $elGallo = new ElGalloSpinService3();
        $allWin = 0;
        for ($i = 0; $i < $itr; $i++) {
            $resultData = $elGallo->getSpinData($betLine, $linesInGame, $gameName, 3);

            if ($resultData['jackpot'] != false) {
                $winType = $resultData['jackpot']['result'];
                $allWin += $resultData['jackpot'][$winType];
            }
        }

        $percentage = 100 / ($itr * $betLine * $linesInGame) * $allWin;

//        echo 'all win: ' . $allWin . '<br>';
//        echo 'percentage of money returned: ' . $percentage . '<br><br><br><br>';

        return view('tests.jackpot-test', [
            'jackpots' => $jackpots,
            'mini' => $mini,
            'minor' => $minor,
            'major' => $major,
            'big_daddy' => $bigDaddy,
            'linesInGame' => $linesInGame,
            'betLine' => $betLine,
            'itr' => $itr,
            'percentage' => $percentage,
            'allWin' => $allWin
        ]);
    }

    // основной тест для elGallo
    public function commonTest(Request $request)
    {
        set_time_limit(999999999);
        session_start();

        $itr = 100;
        $balance = 10000000000000000;
        $betLine = 0.01;
        $linesInGame = 25;
        $gameName = 'elGallo';
        $allBet = 0;
        $resultData['balance'] = 0;
        $allWinOnSlots = 0;
        $allWinOnMainLocation = 0;
        $freeSpinAllWin = 0;
        $freeSpinProbability = 0;
        $jackpotProbability = 0;
        $jackpotWinnings = 0;
        $probability1 = 0;
        $probability2 = 0;
        $countFreeSpin = 0;
        $time = 0;
        $numberOfWinningSpins = 0;
        $numberOfJackpots = 0;
        $numberOfLosingSpins = 0;
        $game = 'elGallo';
        $countFloatBalances = 0;
        $galloCount = 0;
        $jackpotArray = ['MINI' => 0, 'MINOR' => 0, 'MAJOR' => 0, 'BIG_DADDY' => 0];
        $spinCount = 0;
        $diamsInMainGame = 0;
        $freeSpinDiams = 0;
        $freeSpinsWinSpinPercent = 0;
        $freeSpinWinSpinCount = 0;
        $freeSpinLoseSpinCount = 0;
        $jokerInMainGameCount = 0;
        $jokerInFreeGameCount = 0;
        $galloCounterForSpins = [0,0,0,0,0,0];
        $minDiabloValueInFreeSpinGame = 9999;
        $maxDiabloValueInFreeSpinGame = 0;
        // ['символ' => ['кол-во символов' => 'кол-во выбадений', 'кол-во символов', ...], ...]
        $сombinationStats = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];
        $jackpotCountsArray = ['MINI' => 0, 'MINOR' => 0, 'MAJOR' => 0, 'BIG_DADDY' => 0];
        $numberOfFreeGamesAwardedCounter = [30 => 0, 25 => 0, 20 => 0, 15 => 0, 10 => 0];
        $freeGameMultipliersCounter = [5 => 0, 4 => 0, 3 => 0, 2 => 0, 1 => 0];
        $percentsOfFreeGamesAwardedCounter = [30 => 0, 25 => 0, 20 => 0, 15 => 0, 10 => 0];
        $percentsFreeGameMultipliersCounter = [5 => 0, 4 => 0, 3 => 0, 2 => 0, 1 => 0];

        $_SESSION['test'] = true;
        $_SESSION['simulation'] = true;
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 2;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 10000000000000000;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['allWin'] = 0;
        $_SESSION['minDiabloValueInFreeSpinGame'] = 9999;
        $_SESSION['maxDiabloValueInFreeSpinGame'] = 0;
        $_SESSION['diabloCountInCurrentFreeGame'] = 0;

        if ($_GET) {
            $start = microtime(true);
            $betLine = $request->input('betLine');
            $linesInGame = $request->input('linesInGame');
            $itr = $request->input('itr');

            $_SESSION['balance'] = $balance;
            $_SESSION['sessionName'] = rand(0, 100000000000);

            if (isset($_GET['fast_mod'])) {
                $elGallo = new ElGalloSpinService2();
            } else {
                $elGallo = new ElGalloSpinService();
            }

            $countFreeSpin = 0;
            $resultData = [];
            $freeSpinItrCounter = 0;
            $freeSpinAllWin = 0;
            $jackpotWinnings = 0;
            $allWinOnMainLocation = 0;
            $rope = ['count' => 0, 'mul' => 0, 'hash' => 0];
            $allBet = 0;
            $spinCount = 0;
            while ($spinCount < $itr) {
                $resultData = $elGallo->getSpinResultData($betLine, $linesInGame, $gameName);
                $simulationResult = $this->getResultSimulationEG($resultData, $elGallo);

                // прибавляем выигрышные комбинации
                foreach ($simulationResult['сombinationStats'] as $key => $valueArray) {
                    foreach ($valueArray as $key2 => $value2) {
                        $сombinationStats[$key][$key2] += $value2;
                    }
                }

                // получение кол-ва петухов выпавших за спин
                foreach ($simulationResult['galloCounterForSpins'] as $key => $value) {
                    $galloCounterForSpins[$key] += $value;
                }

                // прибавляем джекпоты
                foreach ($simulationResult['jackpotArray'] as $key => $value) {
                    $jackpotArray[$key] += $value;
                }

                // прибавление выпавшего кол-ва бесплатных кручений в фрисинах
                foreach ($simulationResult['numberOfFreeGamesAwardedCounter'] as $key => $value) {
                    $numberOfFreeGamesAwardedCounter[$key] += $value;
                }

                // прибавление кол-ва выпавших множителей в фриспинах
                foreach ($simulationResult['freeGameMultipliersCounter'] as $key => $value) {
                    $freeGameMultipliersCounter[$key] += $value;
                }


                foreach ($simulationResult['jackpotCountsArray'] as $key => $value) {
                    if ($key === 'MINI') {
                        $jackpotCountsArray['MINI'] += $value;
                    }
                    if ($key === 'MINOR') {
                        $jackpotCountsArray['MINOR'] += $value;
                    }
                    if ($key === 'MAJOR') {
                        $jackpotCountsArray['MAJOR'] += $value;
                    }
                    if ($key === 'BIG_DADDY') {
                        $jackpotCountsArray['BIG_DADDY'] += $value;
                    }
                }

                $countFreeSpin += $simulationResult['countFreeSpin'];
                $freeSpinItrCounter += $simulationResult['freeSpinItrCounter'];
                $freeSpinAllWin += $simulationResult['freeSpinAllWin'];
                $jackpotWinnings += $simulationResult['jackpotWinnings'];
                $allWinOnMainLocation += $simulationResult['allWinOnMainLocation'];
                $galloCount += $simulationResult['galloCount'];
                $allBet += $simulationResult['allBet'];
                $numberOfWinningSpins += $simulationResult['numberOfWinningSpins'];
                $numberOfLosingSpins += $simulationResult['numberOfLosingSpins'];
                $spinCount += $simulationResult['spinCount'];
                $numberOfJackpots += $simulationResult['numberOfJackpots'];
                $freeSpinWinSpinCount += $simulationResult['freeSpinWinSpinCount'];
                $freeSpinLoseSpinCount += $simulationResult['freeSpinLoseSpinCount'];
                $jokerInMainGameCount += $simulationResult['jokerInMainGameCount'];
                $jokerInFreeGameCount += $simulationResult['jokerInFreeGameCount'];

            }

            $minDiabloValueInFreeSpinGame = $_SESSION['minDiabloValueInFreeSpinGame'];
            $maxDiabloValueInFreeSpinGame = $_SESSION['maxDiabloValueInFreeSpinGame'];

            $allWinOnSlots = $allWinOnMainLocation + $freeSpinAllWin + $jackpotWinnings;
            if (($allWinOnMainLocation + $freeSpinAllWin + $jackpotWinnings) !== 0) {
                $probability1 = 100 / $allBet * ($allWinOnMainLocation + $freeSpinAllWin + $jackpotWinnings); // общий выигрышь в игре
            }
            if ($allWinOnMainLocation !== 0) {
                $probability2 = 100 / $allBet * $allWinOnMainLocation; // общий выигрышь толjackpotWinningsько по слотам
            }
            if ($freeSpinAllWin !== 0) {
                $freeSpinProbability = 100 / $allBet * $freeSpinAllWin; // попытка подсчитать
            }
            if ($jackpotWinnings !== 0) {
                $jackpotProbability = 100 / $allBet * $jackpotWinnings;
            }

            if ($freeSpinWinSpinCount !== 0) {
                $freeSpinsWinSpinPercent = 100 / ($freeSpinLoseSpinCount + $freeSpinWinSpinCount) * $freeSpinWinSpinCount;
            }

            // подсчет процетов выпадения значений кол-ва бесплатных спинов
            $percentsOfFreeGamesAwardedCounter = [30 => 0, 25 => 0, 20 => 0, 15 => 0, 10 => 0];
            $allCountFreeStepsOnFreeGame = array_sum($numberOfFreeGamesAwardedCounter);
            if ($allCountFreeStepsOnFreeGame !== 0) {
                foreach ($numberOfFreeGamesAwardedCounter as $key => $value) {
                    $percentsOfFreeGamesAwardedCounter[$key] = 100 / $allCountFreeStepsOnFreeGame * $value;
                }
            }

            // подсчет процетов выпадения значений множителей фриспинов
            $percentsFreeGameMultipliersCounter = [5 => 0, 4 => 0, 3 => 0, 2 => 0, 1 => 0];
            $allCountFreeGameMultipliers = array_sum($freeGameMultipliersCounter);
            if ($allCountFreeGameMultipliers !== 0) {
                foreach ($freeGameMultipliersCounter as $key => $value) {
                    $percentsFreeGameMultipliersCounter[$key] = 100 / $allCountFreeGameMultipliers * $value;
                }
            }

            $time = microtime(true) - $start;
        }

        if ($minDiabloValueInFreeSpinGame === 9999) {
            $minDiabloValueInFreeSpinGame = 0;
        }

        return view('admin.common-test', [
            'itr' => $itr,
            'betLine' => $betLine,
            'linesInGame' => $linesInGame,
            'allBet' => $allBet,
            'balance' => $resultData['balance'],
            'allWinOnSlots' => $allWinOnSlots,
            'allWinOnMainLocation' => $allWinOnMainLocation,
            'freeSpinAllWin' => $freeSpinAllWin,
            'freeSpinProbability' => $freeSpinProbability,
            'jackpotProbability' => $jackpotProbability,
            'jackpotWinnings' => $jackpotWinnings,
            'probability1' => $probability1,
            'probability2' => $probability2,
            'countFreeSpin' => $countFreeSpin,
            'time' => $time,
            'numberOfWinningSpins' => $numberOfWinningSpins,
            'numberOfLosingSpins' => $numberOfLosingSpins,
            'numberOfJackpots' => $numberOfJackpots,
            'game' => $game,
            'jackpotArray' => $jackpotArray,
            'сombinationStats' => $сombinationStats,
            'spinCount' => $spinCount,
            'maxDiamValueInFreeSpinGame' => 0,
            'minDiamValueInFreeSpinGame' => 0,
            'freeSpinsWinSpinPercent' => $freeSpinsWinSpinPercent,
            'jokerInMainGameCount' => $jokerInMainGameCount,
            'jokerInFreeGameCount' => $jokerInFreeGameCount,
            'galloCounterForSpins' => $galloCounterForSpins,
            'minDiabloValueInFreeSpinGame' => $minDiabloValueInFreeSpinGame,
            'maxDiabloValueInFreeSpinGame' => $maxDiabloValueInFreeSpinGame,
            'jackpotCountsArray' => $jackpotCountsArray,
            'numberOfFreeGamesAwardedCounter' => $numberOfFreeGamesAwardedCounter,
            'freeGameMultipliersCounter' => $freeGameMultipliersCounter,
            'percentsOfFreeGamesAwardedCounter' => $percentsOfFreeGamesAwardedCounter,
            'percentsFreeGameMultipliersCounter' => $percentsFreeGameMultipliersCounter
        ]);
    }

    public function getResultSimulationEG($resultData, $elGallo) : array
    {
        $simulationResult = [];
        $simulationResult['сombinationStats'] = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];
        $simulationResult['allWinOnMainLocation'] = 0;
        $simulationResult['numberOfLosingSpins'] = 0;
        $simulationResult['numberOfWinningSpins'] = 0;
        $simulationResult['allBet'] = 0;
        $simulationResult['galloCount'] = 0;
        $simulationResult['galloCounterForSpins'] = [0,0,0,0,0,0];
        $simulationResult['countFreeSpin'] = 0;
        $simulationResult['freeSpinItrCounter'] = 0;
        $simulationResult['freeSpinAllWin'] = 0;
        $simulationResult['jackpotWinnings'] = 0;
        $simulationResult['spinCount'] = 0;
        $simulationResult['jackpotArray'] = ['MINI' => 0, 'MINOR' => 0, 'MAJOR' => 0, 'BIG_DADDY' => 0];
        $simulationResult['numberOfJackpots'] = 0;
        $simulationResult['freeSpinsWinSpinPercent'] = 0;
        $simulationResult['freeSpinWinSpinCount'] = 0;
        $simulationResult['freeSpinLoseSpinCount'] = 0;
        $simulationResult['jokerInMainGameCount'] = 0;
        $simulationResult['jokerInFreeGameCount'] = 0;
        $simulationResult['jackpotCountsArray'] = ['MINI' => 0, 'MINOR' => 0, 'MAJOR' => 0, 'BIG_DADDY' => 0];
        $simulationResult['numberOfFreeGamesAwardedCounter'] = [10 => 0, 15 => 0, 20 => 0, 25 => 0, 30 => 0];
        $simulationResult['freeGameMultipliersCounter'] = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0];

        // получение статистики по выигрышным комбинациям
        if ($resultData['winBonusSymbolsData'][1] !== 0 && $_SESSION['checkFreeSpin'] === 'freeSpin') {
            $simulationResult['сombinationStats'][2][$resultData['winBonusSymbolsData'][0]] += 1;
            $simulationResult['countFreeSpin'] += 1;

            // подсчет кол-ва выпавших бесплатных спинов
            $simulationResult['numberOfFreeGamesAwardedCounter'][$resultData['freeSpinData']['count']] += 1;

            // подсчет кол-ва выпавших множетелей для фриспинов
            $simulationResult['freeGameMultipliersCounter'][$resultData['freeSpinData']['mul']] += 1;
        }

        if ($resultData['winBonusSymbolsData'][1] !== 0 && $_SESSION['checkFreeSpin'] === false) {
            $simulationResult['сombinationStats'][2][2] += 1;
        }

        if ($resultData['winLinesData'] !== [] && $resultData['winLinesData'] !== 'freeSpin') {
            foreach ($resultData['winLinesData'] as $key => $winLineData) {
                // $winLineData[1] - выигрышный символ
                // $winLineData[2] - кол-во символов в выигрышной комбинации
                $simulationResult['сombinationStats'][$winLineData[1]][$winLineData[2]] += 1;
            }
        }

        if ($_SESSION['checkFreeSpin'] === 'freeSpin') {
            $simulationResult['spinCount'] = 1;
        }
        if ($_SESSION['freeSpinData'] === false) {
            $simulationResult['spinCount'] = 1;
        }

        // проверка наличия дробной части
        $deltaBalance = $resultData['balance'] - floor($resultData['balance']);
        if ($deltaBalance > 0) {
            $simulationResult['countFloatBalances'] += 1;
        }

        if (!$resultData || $resultData === []) {
            $simulationResult['i'] += 1;
        }

        // подсчет кол-ва выигрышных спинов
        if ($_SESSION['freeSpinData'] == false || $_SESSION['checkFreeSpin'] === 'freeSpin') {
            if ($resultData['allWin'] > 0) {
                $simulationResult['numberOfWinningSpins'] += 1;
            } else {
                $simulationResult['numberOfLosingSpins'] += 1;
            }
        } else {
            if ($resultData['allWin'] > 0) {
                $simulationResult['freeSpinWinSpinCount'] += 1;
            } else {
                $simulationResult['freeSpinLoseSpinCount'] += 1;
            }
        }

        if ($_SESSION['freeSpinData'] == false) {
            $simulationResult['allWinOnMainLocation'] += $resultData['allWin'];
        }

        if ($_SESSION['freeSpinData']) {
            if ($_SESSION['freeSpinData']['count'] === 0) {
                $simulationResult['freeSpinAllWin'] += $_SESSION['freeSpinData']['allWin'];
            }
        }

        if ($resultData['jackpot'] != false) {
            // подсчет общего выигрыша на джекпотах
            $winType = $resultData['jackpot']['result'];
            $simulationResult['jackpotWinnings'] += $resultData['jackpot'][$winType];
            $simulationResult['jackpotArray'][$winType] += $resultData['jackpot'][$winType];
            $simulationResult['numberOfJackpots'] += 1;

            // подсчет кол-ва джекпотов
            $simulationResult['jackpotCountsArray'][$winType] += 1;

            $elGallo->getWinJackpot();
        }

        foreach ($resultData['info'] as $key => $value) {
            if ($value === 2) {
                $simulationResult['galloCount'] += 1;
            }

            if ($_SESSION['freeSpinData'] === false || $_SESSION['checkFreeSpin'] === 'freeSpin') {
                if ($value === 0) {
                    $simulationResult['jokerInMainGameCount'] += 1;
                }
            } else {
                if ($value === 0) {
                    $_SESSION['diabloCountInCurrentFreeGame'] += 1;
                    $simulationResult['jokerInFreeGameCount'] += 1;
                }
            }
        }

        $simulationResult['galloCounterForSpins'][$simulationResult['galloCount']] += 1;

        if ($_SESSION['checkFreeSpin'] === 'freeSpin') {
            $simulationResult['allBet'] += $_SESSION['betLine'] * $_SESSION['linesInGame'];
        }
        if ($_SESSION['freeSpinData'] === false) {
            $simulationResult['allBet'] += $_SESSION['betLine'] * $_SESSION['linesInGame'];
        }

        // подсчет мин и макс кол-ва дьяволов в фриспинах
        if ($_SESSION['freeSpinData']) {
            if ($_SESSION['freeSpinData']['count'] === 0) {
                if ($_SESSION['diabloCountInCurrentFreeGame'] > $_SESSION['maxDiabloValueInFreeSpinGame']) {
                    $_SESSION['maxDiabloValueInFreeSpinGame'] = $_SESSION['diabloCountInCurrentFreeGame'];
                }

                if ($_SESSION['diabloCountInCurrentFreeGame'] < $_SESSION['minDiabloValueInFreeSpinGame']) {
                    $_SESSION['minDiabloValueInFreeSpinGame'] = $_SESSION['diabloCountInCurrentFreeGame'];
                }

                $_SESSION['diabloCountInCurrentFreeGame'] = 0;
            }
        }

        return $simulationResult;
    }

    public function testLOL(Request $request)
    {
        session_start();
        $_SESSION['sessionName'] = 1;
        $_SESSION['token'] = 1;
        $_SESSION['userId'] = 1;
        $_SESSION['gameId'] = 2;
        $_SESSION['nickname'] = 1;
        $_SESSION['demo'] = true;
        $_SESSION['platformId'] = 1;
        $_SESSION['freeSpinData'] = false;
        $_SESSION['gameData'] = false;
        $_SESSION['balance'] = 10000000000000000;
        $_SESSION['allWin'] = 0;
        $_SESSION['reconnect'] = false;
        $_SESSION['freeSpinResultAllWin'] = false;
        $_SESSION['check0FreeSpin'] = false;
        $_SESSION['testFreeSpinPrevAllWin'] = 0;
        $_SESSION['next0FSpin'] = false;
        $_SESSION['startFreeSpin'] = false;

        $start = microtime(true);
        $gameName = 'lifeOfLuxury';
        $itr = 0;
        $game = 'lifeOfLuxury';
        $time = 0;

        $simulationResult = [];
        $simulationResult['linesInGame'] = 15;
        $simulationResult['betLine'] = 0.01;
        $simulationResult['itrX'] = 1000;
        $simulationResult['allWinOnMainLocation'] = 0;
        $simulationResult['freeSpinAllWin'] = 0;
        $simulationResult['allWin'] = 0;
        $simulationResult['countFreeSpin'] = 0;
        $simulationResult['numberOfWinningSpins'] = 0;
        $simulationResult['allBet'] = 0;
        $simulationResult['numberOfLosingSpins'] = 0;
        $simulationResult['allWinOnSlots'] = 0;
        $simulationResult['percentageOfMoneyReturned'] = 0; // общий выигрышь в игре
        $simulationResult['percentageOfMoneyReturnedOnMainGame'] = 0; // общий выигрышь только по слотам
        $simulationResult['freeSpinProbability'] = 0; // попытка подсчитать
        $simulationResult['balance'] = 0;
        $simulationResult['countCoinsArr'] = [0,0,0,0,0,0];
        $simulationResult['countCoinsAndDiamsArr'] = [
            '5coins' => 0,
            '4coinsAnd1Diams' => 0,
            '3coinsAnd2Diams' => 0,
            '2coinsAnd3Diams' => 0,
            '4coins' => 0,
            '3coinsAnd1Diams' => 0,
            '2coinsAnd2Diams' => 0,
            '1coinsAnd3Diams' => 0,
            '3coins' => 0,
            '2coinsAnd1Diams' => 0,
            '1coinsAnd2Diams' => 0,
        ];
        $simulationResult['diamsInMainGame'] = 0;
        $simulationResult['freeSpinDiams'] = 0;
        $simulationResult['spinCount'] = 0;
        // ['символ' => ['кол-во символов' => 'кол-во выбадений', 'кол-во символов', ...], ...]
        $simulationResult['сombinationStats'] = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];
        $simulationResult['maxDiamValueInFreeSpinGame'] = 0;
        $simulationResult['minDiamValueInFreeSpinGame'] = 27;
        $simulationResult['diamsInCurrentFreeSpinGame'] = 0;
        $simulationResult['checkLastFreeSpin'] = false;
        $simulationResult['freeSpinsWinSpinPercent'] = 0;
        $simulationResult['freeSpinWinSpinCount'] = 0;
        $simulationResult['freeSpinLoseSpinCount'] = 0;

        if ($_GET) {
            $simulationResult['betLine'] = $request->input('betLine');
            $simulationResult['linesInGame'] = $request->input('linesInGame');
            $itr = $request->input('itr');
            $simulationResult['itrX'] = $request->input('itr');

            $gameService = new \App\Services\GameServices\LifeOfLuxury\LifeOfLuxuryService();

            for ($i = 0; $i < $simulationResult['itrX']; $i++) {
                $result = $gameService->getSpinResultData($simulationResult['betLine'], $simulationResult['linesInGame'], $gameName);

                $simulationResult = $this->getResultSimulationLOL($result, $simulationResult);
            }

            $simulationResult['allWinOnSlots'] = $simulationResult['freeSpinAllWin'] + $simulationResult['allWinOnMainLocation'];
            $simulationResult['percentageOfMoneyReturned'] = 100 / $simulationResult['allBet'] * ($simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin']); // общий выигрышь в игре
            $simulationResult['percentageOfMoneyReturnedOnMainGame'] = 100 / $simulationResult['allBet'] * $simulationResult['allWinOnMainLocation']; // общий выигрышь только по слотам
            $simulationResult['freeSpinProbability'] = 100 / $simulationResult['allBet'] * $simulationResult['freeSpinAllWin']; // попытка подсчитать
            $time = microtime(true) - $start;
            $freeSpinProbability['balance'] = $result['balance'];
        }

        $simulationResult['itrX'] -= $simulationResult['countFreeSpin'];

        // убираем что было мин 27 алмазов в фриспинах, если фриспинов не было
        if ($simulationResult['countFreeSpin'] === 0) {
            $simulationResult['minDiamValueInFreeSpinGame'] = 0;
        }

        return view('admin.common-test', [
            'itr' => $itr,
            'betLine' => $simulationResult['betLine'],
            'linesInGame' => $simulationResult['linesInGame'],
            'allBet' => $simulationResult['allBet'],
            'balance' => $simulationResult['balance'],
            'allWinOnSlots' => $simulationResult['allWinOnSlots'],
            'allWinOnMainLocation' => $simulationResult['allWinOnMainLocation'],
            'freeSpinAllWin' => $simulationResult['freeSpinAllWin'],
            'freeSpinProbability' => $simulationResult['freeSpinProbability'],
            'jackpotProbability' => 0,
            'jackpotWinnings' => 0,
            'probability1' => $simulationResult['percentageOfMoneyReturned'],
            'probability2' => $simulationResult['percentageOfMoneyReturnedOnMainGame'],
            'countFreeSpin' => $simulationResult['countFreeSpin'],
            'time' => $time,
            'numberOfWinningSpins' => $simulationResult['numberOfWinningSpins'],
            'numberOfLosingSpins' => $simulationResult['numberOfLosingSpins'],
            'numberOfJackpots' => 0,
            'game' => $game,
            'сombinationStats' => $simulationResult['сombinationStats'],
            'spinCount' => $simulationResult['spinCount'],
            'diamsInMainGame' => $simulationResult['diamsInMainGame'],
            'freeSpinDiams' => $simulationResult['freeSpinDiams'],
            'countCoinsArr' => $simulationResult['countCoinsArr'],
            'countCoinsAndDiamsArr' => $simulationResult['countCoinsAndDiamsArr'],
            'maxDiamValueInFreeSpinGame' => $simulationResult['maxDiamValueInFreeSpinGame'],
            'minDiamValueInFreeSpinGame' => $simulationResult['minDiamValueInFreeSpinGame'],
            'freeSpinsWinSpinPercent' => $simulationResult['freeSpinsWinSpinPercent']
        ]);
    }

    public function getResultSimulationLOL($result, $simulationResult) : array
    {
        // подсчет кол-ва ходов
        if ($_SESSION['freeSpinData'] === false && $simulationResult['checkLastFreeSpin'] === false) {
            $simulationResult['spinCount'] += 1;
        }
        if ($_SESSION['startFreeSpin'] === true) {
            $simulationResult['spinCount'] += 1;
        }

        // подсчет выигрышных комбинаций
        if ($_SESSION['startFreeSpin'] === true) {
            if ($result['winBonusSymbolsData'][0] > 5) {
                $result['winBonusSymbolsData'][0] = 5;
            }
            $simulationResult['сombinationStats'][10][$result['winBonusSymbolsData'][0]] += 1;
            $simulationResult['countFreeSpin'] += 1;
        }

        if ($result['winLinesData'] !== [] && $_SESSION['freeSpinData'] === false && $simulationResult['checkLastFreeSpin'] === false) {
            foreach ($result['winLinesData'] as $key => $winLineData) {
                // $winLineData[1] - выигрышный символ
                // $winLineData[2] - кол-во символов в выигрышной комбинации
                $simulationResult['сombinationStats'][$winLineData[1]][$winLineData[2]] += 1;
            }
        }

        $countCoins = 0;
        $countDiams = 0;
        $currentCointCount = 0;
        foreach ($result['info'] as $key => $value) {
            if ($value === 10) {
                $currentCointCount += 1;
            }

            if ($value === 0) {
                if ($result['freeSpinData'] === false &&
                    $result['check0FreeSpin'] === false &&
                    $simulationResult['checkLastFreeSpin'] === false)
                {
                    $simulationResult['diamsInMainGame'] += 1;
                } else {
                    if ($_SESSION['startFreeSpin'] !== true || $simulationResult['checkLastFreeSpin'] === true) {
                        $simulationResult['freeSpinDiams'] += 1;
                    } else {
                        $simulationResult['diamsInMainGame'] += 1;
                    }
                }

                // подсчет общего кол-ва алмазов за текущие фриспины
                if ($_SESSION['freeSpinData'] !== false && $_SESSION['startFreeSpin'] !== true) {
                    $simulationResult['diamsInCurrentFreeSpinGame'] += 1;
                }
                if ($simulationResult['checkLastFreeSpin'] === true) {
                    $simulationResult['diamsInCurrentFreeSpinGame'] += 1;
                }
            }

            // счетчики кол-ва алмазо и манет, с которых стартуют фриспины
            if ($_SESSION['startFreeSpin'] === true) {
                if ($value === 0) {
                    $countDiams += 1;
                }
                if ($value === 10) {
                    $countCoins += 1;
                }
            }

        }
        $simulationResult['countCoinsArr'][$currentCointCount] += 1;
        // Статистика получения фри спинов, точнее что привело к фри спину (1 монета + 2 бриллианта; 2 монеты + 1 бриллиант и т.д.)
        if ($countCoins === 5) {
            $simulationResult['countCoinsAndDiamsArr']['5coins'] += 1;
        } elseif ($countCoins === 4 && $countDiams === 1) {
            $simulationResult['countCoinsAndDiamsArr']['4coinsAnd1Diams'] += 1;
        } elseif ($countCoins === 3 && $countDiams === 2) {
            $simulationResult['countCoinsAndDiamsArr']['3coinsAnd2Diams'] += 1;
        } elseif ($countCoins === 2 && $countDiams === 3) {
            $simulationResult['countCoinsAndDiamsArr']['2coinsAnd3Diams'] += 1;
        } elseif ($countCoins === 4) {
            $simulationResult['countCoinsAndDiamsArr']['4coins'] += 1;
        } elseif ($countCoins === 3 && $countDiams === 1) {
            $simulationResult['countCoinsAndDiamsArr']['3coinsAnd1Diams'] += 1;
        } elseif ($countCoins === 2 && $countDiams === 2) {
            $simulationResult['countCoinsAndDiamsArr']['2coinsAnd2Diams'] += 1;
        } elseif ($countCoins === 1 && $countDiams === 3) {
            $simulationResult['countCoinsAndDiamsArr']['1coinsAnd3Diams'] += 1;
        } elseif ($countCoins === 3) {
            $simulationResult['countCoinsAndDiamsArr']['3coins'] += 1;
        } elseif ($countCoins === 2 && $countDiams === 1) {
            $simulationResult['countCoinsAndDiamsArr']['2coinsAnd1Diams'] += 1;
        } elseif ($countCoins === 1 && $countDiams === 2) {
            $simulationResult['countCoinsAndDiamsArr']['1coinsAnd2Diams'] += 1;
        }

        // подсчет суммы всех ставок
        if ($result['freeSpinData'] == false && $simulationResult['checkLastFreeSpin'] == false) {
            $simulationResult['allBet'] += $simulationResult['betLine'] * $simulationResult['linesInGame'];
            //dd($simulationResult['allBet']);
        } elseif ($_SESSION['startFreeSpin'] === true) {
            $simulationResult['allBet'] += $simulationResult['betLine'] * $simulationResult['linesInGame'];
        }

        // выигрышь в основной игре и подсчет выигрышных/проигрышных спинов
        if ($result['freeSpinData'] === false && $simulationResult['checkLastFreeSpin'] === false && $result['allWin'] > 0) {
            $simulationResult['allWinOnMainLocation'] += $result['allWin'];
            $simulationResult['numberOfWinningSpins'] += 1;
        } elseif ($result['freeSpinData'] == false && $simulationResult['checkLastFreeSpin'] == false && $result['allWin'] == 0) {
            $simulationResult['numberOfLosingSpins'] += 1;
        }
        if ($_SESSION['startFreeSpin'] === true) {
            $simulationResult['numberOfWinningSpins'] += 1;
        }

        // подсчет выигрышных/проигрышных спинов в фриспинах
        if ($result['freeSpinData'] !== false || $result['check0FreeSpin'] !== false) {
            $simulationResult['itrX'] += 1;

            $_SESSION['testFreeSpinPrevAllWin'] = $result['freeSpinData']['allWin'];
        }

        if ($_SESSION['next0FSpin'] === true) {
            $_SESSION['next0FSpin'] = false;
            $simulationResult['freeSpinAllWin'] += $result['allWin'];
        }
        if ($_SESSION['freeSpinData'] !== false) {
            $simulationResult['freeSpinAllWin'] += $result['allWin'];

            if ($_SESSION['freeSpinData']['count'] === 1) {
                $_SESSION['next0FSpin'] = true;
            }
        }


        // подсчет максимального и минимального кол-ва алмазов за фриспины
        if($simulationResult['checkLastFreeSpin'] === true) {
            if ($simulationResult['diamsInCurrentFreeSpinGame'] > $simulationResult['maxDiamValueInFreeSpinGame']) {
                $simulationResult['maxDiamValueInFreeSpinGame'] = $simulationResult['diamsInCurrentFreeSpinGame'];
            }
            if ($simulationResult['minDiamValueInFreeSpinGame'] > $simulationResult['diamsInCurrentFreeSpinGame']) {
                $simulationResult['minDiamValueInFreeSpinGame'] = $simulationResult['diamsInCurrentFreeSpinGame'];
            }
            $simulationResult['diamsInCurrentFreeSpinGame'] = 0;
        }

        // подсчет кол-ва выигрышных и проигрышных спинов в фриспинах
        if ($_SESSION['startFreeSpin'] === false) {
            if ($_SESSION['freeSpinData'] != false || $simulationResult['checkLastFreeSpin'] != false) {
                if ($result['allWin'] > 0) {
                    $simulationResult['freeSpinWinSpinCount'] += 1;
                } else {
                    $simulationResult['freeSpinLoseSpinCount'] += 1;
                }

                if ($simulationResult['freeSpinWinSpinCount'] === 0) {
                    $simulationResult['freeSpinsWinSpinPercent'] = 0;
                } else {
                    $simulationResult['freeSpinsWinSpinPercent'] = 100 / ($simulationResult['freeSpinWinSpinCount'] + $simulationResult['freeSpinLoseSpinCount']) * $simulationResult['freeSpinWinSpinCount'];
                }
            }
        }

        // следующий спин будет последним
        if ($_SESSION['freeSpinData']['count'] === 1) {
            $simulationResult['checkLastFreeSpin'] = true;
        } elseif ($simulationResult['checkLastFreeSpin'] === true) {
            $simulationResult['checkLastFreeSpin'] = false;
        }

        $simulationResult['allWinOnSlots'] += $simulationResult['allWinOnMainLocation'] + $simulationResult['freeSpinAllWin'];

        // $countCointFromCoinsAndDiamsArr = 0;
        // $countCointFromCoinsAndDiamsArr += $simulationResult['countCoinsAndDiamsArr']['3coins'];
        // $countCointFromCoinsAndDiamsArr += $simulationResult['countCoinsAndDiamsArr']['2coinsAnd1Diams'];
        // $countCointFromCoinsAndDiamsArr += $simulationResult['countCoinsAndDiamsArr']['1coinsAnd2Diams'];
        // if ($countCointFromCoinsAndDiamsArr !== $simulationResult['сombinationStats'][10][3]) {
        //     dd($result);
        //     dd($countCointFromCoinsAndDiamsArr .' '. $simulationResult['сombinationStats'][10][3]);
        // }

        return $simulationResult;
    }

    public function superKenoTest(Request $request)
    {
        $itr = 100;
        $bet = 1;
        $balls = 2;
        $game = 'superkeno';
        $allBet = 0;
        $allWinOnMainLocation = 0;
        $jackpotWinnings = 0;
        $numberOfWinningSpins = 0;
        $numberOfLosingSpins = 0;
        $numberOfJackpots = 0;
        $percentageOfMoneyReturned = 0;
        $percentageOfMoneyReturnedOnMainGame = 0;
        $jackpotProbability = 0;
        $time = 0;
        $balance = 100000;
        $matches = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 'withLastBall' => 0];

        if ($_GET) {
            $balls = $request->input('balls');
        }

        $arrayBalls = [];
        for ($i=0; $i < $balls; $i++) {
            $arrayBalls[] = $i + 1;
        }
        $arrayStr = implode(',', $arrayBalls);

        if (isset($_GET['bet'])) {
            $start = microtime(true);
            $bet = $request->input('bet');
            $balls = $request->input('balls');
            $itr = $request->input('itr');

            $kenoService = new \App\Services\GameServices\keno\KenoService();
            for ($i=0; $i < $itr; $i++) {
                $result = json_decode($kenoService->getSpinResultData(true, [
                    'balls_selected' => $arrayStr,
                    'betValue' => $bet,
                    'sessionName' => 'sessionName'
                ]));

                if (isset($result->got_balls)) {
                    if ($result->got_balls > 0) {
                        $matches[$result->got_balls] += 1;
                    }
                }

                $allBet += $bet;

                $result->status === 'lose' ? $numberOfLosingSpins += 1 : $numberOfWinningSpins += 1;

                if ($result->jackpot != false) {
                    $numberOfJackpots += 1;
                    $jackpotWinnings += $result->result;
                } else {
                    $allWinOnMainLocation += $result->result;
                }
            }

            $percentageOfMoneyReturned = ($allWinOnMainLocation + $jackpotWinnings) / $allBet * 100;
            $percentageOfMoneyReturnedOnMainGame = $allWinOnMainLocation / $allBet * 100;
            $jackpotProbability = $jackpotWinnings / $allBet * 100;

            $time = microtime(true) - $start;
        }

        return view('admin.keno-test', [
            'bet' => $bet,
            'balls' => $balls,
            'game' => $game,
            'allBet' => $allBet,
            'allWinOnMainLocation' => $allWinOnMainLocation,
            'jackpotWinnings' => $jackpotWinnings,
            'itr' => $itr,
            'numberOfWinningSpins' => $numberOfWinningSpins,
            'numberOfLosingSpins' => $numberOfLosingSpins,
            'numberOfJackpots' => $numberOfJackpots,
            'percentageOfMoneyReturned' => $percentageOfMoneyReturned,
            'percentageOfMoneyReturnedOnMainGame'=> $percentageOfMoneyReturnedOnMainGame,
            'jackpotProbability' => $jackpotProbability,
            'time' => $time,
            'matches' => $matches
        ]);
    }

    public function doubleKenoTest(Request $request)
    {
        $itr = 100000;
        $bet = 1;
        $balls = 2;
        $game = 'doublekeno';
        $allBet = 0;
        $allWinOnMainLocation = 0;
        $jackpotWinnings = 0;
        $numberOfWinningSpins = 0;
        $numberOfLosingSpins = 0;
        $numberOfJackpots = 0;
        $percentageOfMoneyReturned = 0;
        $percentageOfMoneyReturnedOnMainGame = 0;
        $jackpotProbability = 0;
        $time = 0;
        $matches = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0];

        $arrayBalls = [];
        for ($i=0; $i < $balls; $i++) {
            $arrayBalls[] = $i + 1;
        }
        $ballsStr = implode(',', $arrayBalls);

        if (isset($_GET['bet'])) {
            $start = microtime(true);
            $bet = $request->input('bet');
            $balls = $request->input('balls');
            $itr = $request->input('itr');

            $arrayBalls = [];
            for ($i=0; $i < $balls; $i++) {
                $arrayBalls[] = $i + 1;
            }
            $ballsStr = implode(',', $arrayBalls);

            $kenoService = new \App\Services\GameServices\keno\DoubleKenoService();
            for ($i=0; $i < $itr; $i++) {
                $result = $kenoService->getSpinResultData(true, [
                    'balls_selected' => $ballsStr,
                    'betValue' => $bet,
                    'sessionName' => 'sessionName',
                    'ballsDouble' => '0'
                ]);

                $countRe = count(array_intersect($result['balls'], $arrayBalls));

                if ($countRe >= 2) {
                    $result2 = $kenoService->getSpinResultData(true, [
                        'balls_selected' => $ballsStr,
                        'betValue' => $bet * 2,
                        'sessionName' => 'sessionName',
                        'ballsDouble' => implode(',', $result['balls']),
                        'key' => $result['key'],
                        'doubleSpin' => 'true'
                    ]);
                    $allBet += $bet * 2;
                } else {
                    $result2 = $kenoService->getSpinResultData(true, [
                        'balls_selected' => $ballsStr,
                        'betValue' => $bet,
                        'sessionName' => 'sessionName',
                        'ballsDouble' => implode(',', $result['balls']),
                        'key' => $result['key'],
                        'doubleSpin' => 'false'
                    ]);
                    $allBet += $bet;
                }

                if ($result2['result'] > 0) {
                    $matches[count($result2['win_balls'])] += 1;
                }


                if ($result2['jackpot'] === true) {
                    $numberOfJackpots += 1;
                    $jackpotWinnings += $result2['result'];
                } else {
                    $allWinOnMainLocation += $result2['result'];
                }

                $result2['status'] === 'lose' ? $numberOfLosingSpins += 1 : $numberOfWinningSpins += 1;
            }

            $percentageOfMoneyReturned = ($allWinOnMainLocation + $jackpotWinnings) / $allBet  * 100;
            $percentageOfMoneyReturnedOnMainGame = ($allWinOnMainLocation) / $allBet  * 100;
            $jackpotProbability = $jackpotWinnings / $allBet * 100;

            $time = microtime(true) - $start;
        }

        return view('admin.doublekeno-test', [
            'bet' => $bet,
            'balls' => $balls,
            'game' => $game,
            'allBet' => $allBet,
            'allWinOnMainLocation' => $allWinOnMainLocation,
            'jackpotWinnings' => $jackpotWinnings,
            'itr' => $itr,
            'numberOfWinningSpins' => $numberOfWinningSpins,
            'numberOfLosingSpins' => $numberOfLosingSpins,
            'numberOfJackpots' => $numberOfJackpots,
            'percentageOfMoneyReturned' => $percentageOfMoneyReturned,
            'percentageOfMoneyReturnedOnMainGame'=> $percentageOfMoneyReturnedOnMainGame,
            'jackpotProbability' => $jackpotProbability,
            'time' => $time,
            'matches' => $matches
        ]);
    }

    public function showIndex()
    {
        return view('admin.index');
    }

    public function showSimelations()
    {
        return view('admin.simelations');
    }

    public function showStatistics()
    {
        $statEG = (new Stat)->where('name','=', 'elgallo')->get()->first();
        $statLOL = (new Stat)->where('name','=', 'lifeOfLuxury')->get()->first();
        $statKeno = (new Stat)->where('name','=', 'superkeno')->get()->first();
        $statDKeno = (new Stat)->where('name','=', 'doublekeno')->get()->first();

        return view('admin.statistics', [
            'statEG' => $statEG,
            'statLOL' => $statLOL,
            'statKeno' => $statKeno,
            'statDKeno' => $statDKeno
        ]);
    }

    /**
     * Запись в БД процентов вероятности для символов в EG
     */
    public function setPercentagesEG(Request $request)
    {
        session_start();
        session_destroy();

        // получение данных из запроса
        $data = ['percentages' => [
            'percentagesMainGame' => [
                [
                    $request->input('cell0_b0'),
                    $request->input('cell1_b0'),
                    $request->input('cell2_b0'),
                    $request->input('cell3_b0'),
                    $request->input('cell4_b0'),
                    $request->input('cell5_b0'),
                    $request->input('cell6_b0'),
                    $request->input('cell7_b0'),
                    $request->input('cell8_b0'),
                    $request->input('cell9_b0'),
                    $request->input('cell10_b0'),
                    $request->input('cell11_b0')
                ],
                [
                    $request->input('cell0_b1'),
                    $request->input('cell1_b1'),
                    $request->input('cell2_b1'),
                    $request->input('cell3_b1'),
                    $request->input('cell4_b1'),
                    $request->input('cell5_b1'),
                    $request->input('cell6_b1'),
                    $request->input('cell7_b1'),
                    $request->input('cell8_b1'),
                    $request->input('cell9_b1'),
                    $request->input('cell10_b1'),
                    $request->input('cell11_b1')
                ],
                [
                    $request->input('cell0_b2'),
                    $request->input('cell1_b2'),
                    $request->input('cell2_b2'),
                    $request->input('cell3_b2'),
                    $request->input('cell4_b2'),
                    $request->input('cell5_b2'),
                    $request->input('cell6_b2'),
                    $request->input('cell7_b2'),
                    $request->input('cell8_b2'),
                    $request->input('cell9_b2'),
                    $request->input('cell10_b2'),
                    $request->input('cell11_b2')
                ],
                [
                    $request->input('cell0_b3'),
                    $request->input('cell1_b3'),
                    $request->input('cell2_b3'),
                    $request->input('cell3_b3'),
                    $request->input('cell4_b3'),
                    $request->input('cell5_b3'),
                    $request->input('cell6_b3'),
                    $request->input('cell7_b3'),
                    $request->input('cell8_b3'),
                    $request->input('cell9_b3'),
                    $request->input('cell10_b3'),
                    $request->input('cell11_b3')
                ],
                [
                    $request->input('cell0_b4'),
                    $request->input('cell1_b4'),
                    $request->input('cell2_b4'),
                    $request->input('cell3_b4'),
                    $request->input('cell4_b4'),
                    $request->input('cell5_b4'),
                    $request->input('cell6_b4'),
                    $request->input('cell7_b4'),
                    $request->input('cell8_b4'),
                    $request->input('cell9_b4'),
                    $request->input('cell10_b4'),
                    $request->input('cell11_b4')
                ]
            ],
            'percentagesFreeGame' => [
                [
                    $request->input('cellfg0_b0'),
                    $request->input('cellfg1_b0'),
                    $request->input('cellfg2_b0'),
                    $request->input('cellfg3_b0'),
                    $request->input('cellfg4_b0'),
                    $request->input('cellfg5_b0'),
                    $request->input('cellfg6_b0'),
                    $request->input('cellfg7_b0'),
                    $request->input('cellfg8_b0'),
                    $request->input('cellfg9_b0'),
                    $request->input('cellfg10_b0'),
                    $request->input('cellfg11_b0')
                ],
                [
                    $request->input('cellfg0_b1'),
                    $request->input('cellfg1_b1'),
                    $request->input('cellfg2_b1'),
                    $request->input('cellfg3_b1'),
                    $request->input('cellfg4_b1'),
                    $request->input('cellfg5_b1'),
                    $request->input('cellfg6_b1'),
                    $request->input('cellfg7_b1'),
                    $request->input('cellfg8_b1'),
                    $request->input('cellfg9_b1'),
                    $request->input('cellfg10_b1'),
                    $request->input('cellfg11_b1')
                ],
                [
                    $request->input('cellfg0_b2'),
                    $request->input('cellfg1_b2'),
                    $request->input('cellfg2_b2'),
                    $request->input('cellfg3_b2'),
                    $request->input('cellfg4_b2'),
                    $request->input('cellfg5_b2'),
                    $request->input('cellfg6_b2'),
                    $request->input('cellfg7_b2'),
                    $request->input('cellfg8_b2'),
                    $request->input('cellfg9_b2'),
                    $request->input('cellfg10_b2'),
                    $request->input('cellfg11_b2')
                ],
                [
                    $request->input('cellfg0_b3'),
                    $request->input('cellfg1_b3'),
                    $request->input('cellfg2_b3'),
                    $request->input('cellfg3_b3'),
                    $request->input('cellfg4_b3'),
                    $request->input('cellfg5_b3'),
                    $request->input('cellfg6_b3'),
                    $request->input('cellfg7_b3'),
                    $request->input('cellfg8_b3'),
                    $request->input('cellfg9_b3'),
                    $request->input('cellfg10_b3'),
                    $request->input('cellfg11_b3')
                ],
                [
                    $request->input('cellfg0_b4'),
                    $request->input('cellfg1_b4'),
                    $request->input('cellfg2_b4'),
                    $request->input('cellfg3_b4'),
                    $request->input('cellfg4_b4'),
                    $request->input('cellfg5_b4'),
                    $request->input('cellfg6_b4'),
                    $request->input('cellfg7_b4'),
                    $request->input('cellfg8_b4'),
                    $request->input('cellfg9_b4'),
                    $request->input('cellfg10_b4'),
                    $request->input('cellfg11_b4')
                ]

            ],
        ]];
        $data['percentages']['type'] = 'numbers';

        // генерация json
        $percentagesMainGameJson = '{"columnArrPercentsMain":[';
        for ($l=0; $l < 5; $l++) {
            $percentagesMainGameJson .= '[';
            for ($k=0; $k < 12; $k++) {
                if ($k === 11) {
                    $percentagesMainGameJson .= $data['percentages']['percentagesMainGame'][$l][$k];
                } else {
                    $percentagesMainGameJson .= $data['percentages']['percentagesMainGame'][$l][$k] . ',';
                }
            }

            if ($l === 4) {
                $percentagesMainGameJson .= ']';
            } else {
                $percentagesMainGameJson .= '],';
            }
        }
        $percentagesMainGameJson .= ']}';

        $percentagesFreeGameJson = '{"columnArrPercentsFree":[';
        for ($l=0; $l < 5; $l++) {
            $percentagesFreeGameJson .= '[';
            for ($k=0; $k < 12; $k++) {
                if ($k === 11) {
                    $percentagesFreeGameJson .= $data['percentages']['percentagesFreeGame'][$l][$k];
                } else {
                    $percentagesFreeGameJson .= $data['percentages']['percentagesFreeGame'][$l][$k] . ',';
                }
            }
            if ($l === 4) {
                $percentagesFreeGameJson .= ']';
            } else {
                $percentagesFreeGameJson .= '],';
            }
        }
        $percentagesFreeGameJson .= ']}';

        $data['percentages']['jsonMainGame'] = $percentagesMainGameJson;
        $data['percentages']['jsonFreeGame'] = $percentagesFreeGameJson;

        $data = serialize($data);

        // запись данных в БД
        $game = (new V2Game)->where('name', '=', 'elgallo')->get()->first();
        if ($game !== null) {
            $game->data = $data;
        } else {
            $game = new V2Game;
            $game->name = 'elgallo';
            $game->data = $data;
        }
        $game->save();

        return back();
    }

    public function showSetPercentagesEG()
    {
        // получение процентов для EG из БД
        $data = (new V2Game)->where('name', '=', 'elgallo')->get()->first();
        if ($data !== null) {
            $data = unserialize($data->data);
            $percentagesMainGame50 = $data['percentages']['percentagesMainGame50'];
            $percentagesMainGame25 = $data['percentages']['percentagesMainGame25'];
            $percentagesMainGame1 = $data['percentages']['percentagesMainGame1'];
            $percentagesFreeGame50 = $data['percentages']['percentagesFreeGame50'];
            $percentagesFreeGame25 = $data['percentages']['percentagesFreeGame25'];
            $percentagesFreeGame1 = $data['percentages']['percentagesFreeGame1'];
            $jsonMainGame50 = $data['percentages']['jsonMainGame50'];
            $jsonMainGame25 = $data['percentages']['jsonMainGame25'];
            $jsonMainGame1 = $data['percentages']['jsonMainGame1'];
            $jsonFreeGame50 = $data['percentages']['jsonFreeGame50'];
            $jsonFreeGame25 = $data['percentages']['jsonFreeGame25'];
            $jsonFreeGame1 = $data['percentages']['jsonFreeGame1'];
            $type = $data['percentages']['type'];
        } else {
            $percentagesMainGame50 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $percentagesMainGame25 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $percentagesMainGame1 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $percentagesFreeGame50 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $percentagesFreeGame25 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $percentagesFreeGame1 = [
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10],
                [10,10,10,10,10,10,10,10,10,10,10,10]
            ];
            $jsonMainGame50 = '';
            $jsonMainGame25 = '';
            $jsonMainGame1 = '';
            $jsonFreeGame50 = '';
            $jsonFreeGame25 = '';
            $jsonFreeGame1 = '';
            $type = 'numbers';

            $data = new V2Game;
            $data->name = 'elgallo';
            $data->data = serialize([
                'percentages' => [
                    'percentagesMainGame50' => $percentagesMainGame50,
                    'percentagesMainGame25' => $percentagesMainGame25,
                    'percentagesMainGame1' => $percentagesMainGame1,
                    'percentagesFreeGame50' => $percentagesFreeGame50,
                    'percentagesFreeGame25' => $percentagesFreeGame25,
                    'percentagesFreeGame1' => $percentagesFreeGame1,
                    'jsonMainGame50' => $jsonMainGame50,
                    'jsonMainGame25' => $jsonMainGame25,
                    'jsonMainGame1' => $jsonMainGame1,
                    'jsonFreeGame50' => $jsonFreeGame50,
                    'jsonFreeGame25' => $jsonFreeGame25,
                    'jsonFreeGame1' => $jsonFreeGame1,
                    'type' => $type
                ],
            ]);
            $data->save();
        }

        return view('admin.set-percentages-eg', [
            'percentagesMainGame50' => $percentagesMainGame50,
            'percentagesMainGame25' => $percentagesMainGame25,
            'percentagesMainGame1' => $percentagesMainGame1,
            'percentagesFreeGame50' => $percentagesFreeGame50,
            'percentagesFreeGame25' => $percentagesFreeGame25,
            'percentagesFreeGame1' => $percentagesFreeGame1,
            'jsonMainGame50' => $jsonMainGame50,
            'jsonMainGame25' => $jsonMainGame25,
            'jsonMainGame1' => $jsonMainGame1,
            'jsonFreeGame50' => $jsonFreeGame50,
            'jsonFreeGame25' => $jsonFreeGame25,
            'jsonFreeGame1' => $jsonFreeGame1,
            'type' => $type
        ]);
    }

    public function importPercentagesEG(Request $request)
    {
        session_start();
        session_destroy();

        $game = (new V2Game)->where('name','=', 'elgallo')->get()->first();
        $data = unserialize($game->data);

        $arrMainGame50 = json_decode($request->input('jsonMainGame50'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesMainGame50'][$i][$k] = $arrMainGame50->columnArrPercentsMain[$i][$k];
            }
        }
        $arrMainGame25 = json_decode($request->input('jsonMainGame25'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesMainGame25'][$i][$k] = $arrMainGame25->columnArrPercentsMain[$i][$k];
            }
        }
        $arrMainGame1 = json_decode($request->input('jsonMainGame1'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesMainGame1'][$i][$k] = $arrMainGame1->columnArrPercentsMain[$i][$k];
            }
        }

        $arrFreeGame50 = json_decode($request->input('jsonFreeGame50'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesFreeGame50'][$i][$k] = $arrFreeGame50->columnArrPercentsFree[$i][$k];
            }
        }
        $arrFreeGame25 = json_decode($request->input('jsonFreeGame25'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesFreeGame25'][$i][$k] = $arrFreeGame25->columnArrPercentsFree[$i][$k];
            }
        }
        $arrFreeGame1 = json_decode($request->input('jsonFreeGame1'));
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 12; $k++) {
                $data['percentages']['percentagesFreeGame1'][$i][$k] = $arrFreeGame1->columnArrPercentsFree[$i][$k];
            }
        }

        $data['percentages']['jsonMainGame50'] = $request->input('jsonMainGame50');
        $data['percentages']['jsonFreeGame50'] = $request->input('jsonFreeGame50');
        $data['percentages']['jsonMainGame25'] = $request->input('jsonMainGame25');
        $data['percentages']['jsonFreeGame25'] = $request->input('jsonFreeGame25');
        $data['percentages']['jsonMainGame1'] = $request->input('jsonMainGame1');
        $data['percentages']['jsonFreeGame1'] = $request->input('jsonFreeGame1');
        $data['percentages']['type'] = 'json';

        $game->data = serialize($data);
        $game->save();

        return back();
    }
}
