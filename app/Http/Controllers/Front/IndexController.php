<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Game;
use URL;
use Jenssegers\Agent\Agent;
use App\Services\GameServices\keno\KenoService;
use App\Services\GameServices\keno\DoubleKenoService;
use React\ZMQ\Context as ReactContext;
use App\Services\GameServices\ElGallo\ElGalloSpinServiceVFast;

class IndexController extends Controller
{
    function getIndex()
    {
        // $start = microtime(true);
        //
        // $gameService = new ElGalloSpinServiceVFast;
        // $result = $gameService->getResultData();
        //
        // dd($result);

        return view('ezsl');
    }

    public function generatingValuesForCellsV2()
    {
        $cellsRows = [[],[],[],[],[]];
        for ($i=0; $i < 5; $i++) { // уровень отдельного барабана

            $cellRow = $cellsRows[$i];

            for ($k=0; $k < 3; $k++) { // уроверь отдельной ячейки в барабане
                $cellRow[$k] = $this->getCellValue($cellRow);
            }

            $cellsRows[$i] = $cellRow;
        }

        $valuesForCells = [];
        for ($i=0; $i < 5; $i++) {
            for ($k=0; $k < 3; $k++) {
                $valuesForCells[] = $cellsRows[$i][$k];
            }
        }

        if ($_SESSION['bet'] < 0.25) {
            $checkValuesForCells = true;
            $countBonusSymbol = 0;
            for ($i=0; $i < 15; $i++) {
                if ($valuesForCells[$i] === 2) {
                    $countBonusSymbol += 1;
                }
            }

            if ($countBonusSymbol > 2) {
                return $this->generatingValuesForCellsV2();
            }
        }

        return $valuesForCells;
    }

    public function getCellValue($cellRow)
    {
        $percentArr = [[0, 10], [10,20], [20,30], [30,40], [40,50], [50,60], [60,70], [70,80], [80,90], [90,100],  [100,110],  [110,121]];

        $cellValue = '123123';
        $rand = rand(0,120);
        foreach ($percentArr as $key => $percentRang) { // поиск выпавшего значения ячейки
            if ($rand >= $percentRang[0] && $rand < $percentRang[1]) {
                $cellValue = $key;
                break;
            }
        }

        if (!in_array($cellValue, $cellRow) && $cellValue !== null) { // проверка есть ли уже в ряде такая ячейка
            return $cellValue;
        } else {
            return $this->getCellValue($cellRow);
        }
    }

    public function getHome()
    {
        $games = Game::all();
        $preURL = url()->current();

        return view('index',['games' => $games, 'preURL' => $preURL]);
    }

    function getGame(Agent $agent, $gameName)
    {
        $game = Game::where('gameName','=',$gameName);

        if ($agent->isMobile() || $agent->isTablet()) {
            return view('mobile-game',['game' => $game, 'gameName' => $gameName]);
        }
        return view('game',['game' => $game, 'gameName' => $gameName]);
    }

    public function show404()
    {
        return view('404');
    }

    public function getSpinKeno(KenoService $kenoService)
    {
        return $kenoService->getSpinResultData();
    }

    public function getDoubleKeno(DoubleKenoService $kenoService)
    {
        return $kenoService->getSpinResultData();
    }

    /**
     * Демонстрация для Ильи
     */
    public function firstMoveFundsException()
    {
        exit(json_encode(["status" => "false", "message" => "FirstMoveFundsException"]));
    }

    public function betPlacingAbortException()
    {
        exit(json_encode(["status" => "false", "message" => "BetPlacingAbortException", "betPlacingAbortExceptionID" => 1]));
    }

    public function moveFundsException()
    {
        exit(json_encode(["status" => "false", "message" => "moveFundsException", "moveFundsExceptionID" => 1]));
    }

    public function balanceException()
    {
        exit(json_encode(["status" => "false", "message" => "LowBalanceException"]));
    }

    public function test123()
    {
        exit(json_encode(["status" => "false", "message" => "UnauthenticatedException"]));
    }

    public function testCookies()
    {
        $_COOKIE['testcookie'] = 'sdfsdfsdfsd';
        return redirect('http://partycamera.org/iframe');
    }
}
