<?php
namespace App\Exceptions\V1;

use Exception;
use App\Services\ApiService;

class LowBalanceException extends Exception
{
    public function render()
    {
        return ["status" => "false", "message" => "LowBalanceException"];
    }
}
