<?php
namespace App\Exceptions;

use Exception;
use App\Services\ApiService;
use App\Models\Log;

class UnauthenticatedException extends Exception
{
    public function __construct()
    {
        return ["status" => "false", "message" => "UnauthenticatedException"];
    }
}
