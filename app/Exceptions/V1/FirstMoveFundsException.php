<?php
namespace App\Exceptions\V1;

use Exception;
use App\Models\Log;
use App\Services\ApiService;

class FirstMoveFundsException extends Exception
{
    public function __construct($data)
    {
        $log = new Log;
        $log->type = 'error';
        $log->data = serialize($data);
        $log->save();

        return ["status" => "false", "message" => "FirstMoveFundsException"];
    }
}
