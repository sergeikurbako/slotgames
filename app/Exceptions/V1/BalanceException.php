<?php
namespace App\Exceptions\V1;

use Exception;
use App\Services\ApiService;

class BalanceException extends Exception
{
    public function __construct()
    {
        return ["status" => "false", "message" => "LowBalanceException"];
    }
}
