<?php
namespace App\Exceptions\V1;

use Exception;
use App\Services\ApiService;
use App\Models\MoveFundsExceptionModel;

class MoveFundsException extends Exception
{
    public function __construct($data)
    {
        // сохранение данных о неудачной попытке в БД
        $moveFundsExceptionModel = new MoveFundsExceptionModel();
        $moveFundsExceptionModel->data = $data;
        $moveFundsExceptionModel->count = 1;
        $moveFundsExceptionModel->save();

        return ["status" => "false", "message" => "moveFundsException", "moveFundsExceptionID" => $moveFundsExceptionModel->id];
    }
}
