<?php
namespace App\Exceptions\V1;

use Exception;
use App\Services\BridgeApiService;
use App\Models\BetPlacingAbortExceptionModel;

class BetPlacingAbortException extends Exception
{
    public function __construct($amount, $selected)
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        // сохранение данных о неудачной попытке в БД
        $betPlacingAbortExceptionModel = new BetPlacingAbortExceptionModel();
        $betPlacingAbortExceptionModel->token = $_SESSION['userId'];
        $betPlacingAbortExceptionModel->userId = $_SESSION['userId'];
        $betPlacingAbortExceptionModel->gameId = $_SESSION['gameId'];
        $betPlacingAbortExceptionModel->eventID = $_SESSION['eventID'];
        $betPlacingAbortExceptionModel->amount = $amount;
        $betPlacingAbortExceptionModel->selected = $selected;
        $betPlacingAbortExceptionModel->save();

        return ["status" => "false", "message" => "BetPlacingAbortException", "betPlacingAbortExceptionID" => $betPlacingAbortExceptionModel->id];
    }

    public function sendBetPlacingAbort($betLine, $linesInGame)
    {

    }
}
