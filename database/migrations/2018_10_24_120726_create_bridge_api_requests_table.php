<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBridgeApiRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bridge_api_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->integer('user_id');
            $table->string('event_id');
            $table->string('direction');
            $table->string('transaction_id');
            $table->string('event_type');
            $table->float('amount', 10, 2);
            $table->text('extra_info'); // serialize
            $table->text('requestURL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bridge_api_requests');
    }
}
