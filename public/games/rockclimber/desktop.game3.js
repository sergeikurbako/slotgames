var stepTotalWinR = 0;

function showWin(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y, win, { font: '18px \"Times New Roman\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

    // var timeInterval = 450;
    // var textCounter = setInterval(function () {
    //     text.position.y -= 3;
    // }, 10);

    // setTimeout(function() {
    //     clearInterval(textCounter);
    // }, timeInterval);
    number_win = game.add.audio('number_win');
    number_win.play();
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');unlockDisplay();", 2500);
    }, timeInterval);
}






function game3() {
    (function () {

        var game3 = {   
           rope_arr : [],
           stone_arr : [],
           hook_arr : [],
           blue_screen_arr : [],
           hookfinal_arr : [],
           bottom_bg_arr : [],
           void_rope : [],
           eyes_arr : [],
           selectedRope : null,
           climb : false,
           floor: 1
       };

       game3.preload = function () {};

       game3.create = function () {
           var mobileX = 0;
           var mobileY = 0;
           var ropeStep = 0;
           checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            //звуки

            winSound = game.add.audio('winGame3');
            hit_game3 = game.add.audio('hit_game3');
            game3_1Sound = game.add.audio('game3_1');
            game3_2Sound = game.add.audio('game3_2');
            game3_loseSound = game.add.audio('game3_lose');
            game3_win_2Sound = game.add.audio('game3_win_2');
            rope_1Sound = game.add.audio('rope_2');
            rope_2Sound = game.add.audio('rope_1');
            rope_3Sound = game.add.audio('rope_2');
            rope_4Sound = game.add.audio('rope_2');
            rope_5Sound = game.add.audio('rope_2');
            rope_6Sound = game.add.audio('rope_2');
            rope_7Sound = game.add.audio('rope_2');
            win_1Sound = game.add.audio('game3.win_1');
            win_2Sound = game.add.audio('game3.win_2');
            winSound = game.add.audio('winGame3');
            //изображения
            background = game.add.sprite(94,22, 'topScoreGame4');


            game3.mountain_1= game.add.sprite(94+mobileX,-3978-1152-544+mobileY, 'game3.mountain_1');
            for (var j = 1; j <= 5; ++j) {
             game3.bottom_bg_arr[j]= game.add.sprite(94+mobileX,374+mobileY-(j-1)*1152, 'game3.bottom_1');
         };   

         for (var j = 1; j <= 5; ++j) {
            for (var i = 1; i <= 5; ++i) {
                ji = j*10 + i;
                game3.rope_arr[ji] = game.add.sprite(94+mobileX+(i-1)*128,-522+mobileY-(j-1)*1152, 'game3.rope'+i);
                game3.rope_arr[ji].inputEnabled = true;
                if(j>1){
                    game3.stone_arr[ji] = game.add.sprite(94+mobileX+(i-1)*128,-522+mobileY-(j-1)*1152, 'game3.stone'+i);
                    game3.stone_arr[ji].visible = false;
                }
                if (j<=4){
                    game3.hook_arr[ji] = game.add.sprite(126+mobileX+(i-1)*128,-730+mobileY-(j-1)*1152, 'game3.hook'+i);
                }
                game3.void_rope[ji] = game.add.sprite(94+mobileX+(i-1)*128,-522+mobileY-(j-1)*1152, 'game3.void_rope_1'); 
                game3.void_rope[ji].visible = false;
                game3.eyes_arr[ji] = game.add.sprite(142+mobileX+(i-1)*128,-473+mobileY-(j-1)*1152, 'game3.eyes');
                game3.eyes_arr[ji].animations.add('game3.eyes', [0,1,2,3,4], 6, true).play();
                game3.eyes_arr[ji].visible = false;
            }
            game3.hookfinal_arr[j] = game.add.sprite(126+mobileX+(j-1)*128,-3978-1152-160+mobileY, 'game3.hookfinal_'+j);
        };

        game3.hit_1 = game.add.sprite(110+mobileX,-506+mobileY, 'game3.hit');
        game3.hit_1Animation = game3.hit_1.animations.add('game3.hit', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 15, false);
        game3.hit_1.visible = false;   
        game3.hit_2 = game.add.sprite(110+mobileX,-506+mobileY, 'game3.hit');
        game3.hit_2Animation = game3.hit_2.animations.add('game3.hit', [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33], 15, false);
        game3.hit_2.visible = false;

        game3.fall_1_men = game.add.sprite(110+mobileX,-506+762+mobileY, 'game3.fall_men');
        game3.fall_1_menAnimation = game3.fall_1_men.animations.add('game3.fall_men', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, false);
        game3.fall_1_men.visible = false;  
        game3.fall_2_men = game.add.sprite(110+mobileX,-506+762+mobileY, 'game3.fall_men');
        game3.fall_2_menAnimation = game3.fall_2_men.animations.add('game3.fall_men', [14,15,16,17], 10, true);
        game3.fall_2_men.visible = false;  
        game3.fall_3_men = game.add.sprite(110+mobileX,-506+762+mobileY, 'game3.fall_men');
        game3.fall_3_menAnimation = game3.fall_3_men.animations.add('game3.fall_men', [18,19,20,21,22,23,24,25,26,27], 10, false);
        game3.fall_3_men.visible = false;

        game3.men = game.add.sprite(94+mobileX,312+mobileY, 'game3.men');
        game3.men_climb = game.add.sprite(112+mobileX, 312+mobileY, 'game3.men_climb');
        game3.men_climbAnimation = game3.men_climb.animations.add('men_climb', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true);
        game3.men_climb.visible = false;    
        game3.top_men_1 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_1Animation = game3.top_men_1.animations.add('top_men_1', [0,1,2], 10, false);
        game3.top_men_1.visible = false;  
        game3.top_men_2 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_2Animation = game3.top_men_2.animations.add('top_men_2', [3,4,5,6], 10, false);
        game3.top_men_2.visible = false;  
        game3.top_men_3 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_3Animation = game3.top_men_3.animations.add('top_men_3', [7,8,9,10,11], 10, false);
        game3.top_men_3.visible = false;  
        game3.top_men_4 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_4Animation = game3.top_men_4.animations.add('top_men_4', [12,13,14], 10, false);
        game3.top_men_4.visible = false;  
        game3.top_men_5 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_5Animation = game3.top_men_5.animations.add('top_men_5', [15,16,17,18], 10, false);
        game3.top_men_5.visible = false;  
        game3.top_men_6 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
        game3.top_men_6Animation = game3.top_men_6.animations.add('top_men_6', [19,20,21,22,23,24,25,26,27,28], 10, false);
        game3.top_men_6.visible = false;    
        game3.top_men_7 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_2');
        game3.top_men_7Animation = game3.top_men_7.animations.add('top_men_7', [0,1,2,3,4,5,6,7], 10, false);
        game3.top_men_7.visible = false;  

        game3.mountain_2= game.add.sprite(94+mobileX,56+mobileY, 'game3.mountain_2');
        game3.mountain_2.visible = false;
        game3.sun= game.add.sprite(481+mobileX,154+mobileY, 'game3.sun');
        game3.sun.visible = false;
        game3.flag_1= game.add.sprite(334+mobileX,89+mobileY, 'game3.flag_1');
        game3.flag_1Animation = game3.flag_1.animations.add('flag_1', [0,1,2,3,4,5], 10, false);
        game3.flag_1.visible = false;   
        game3.flag_2_1= game.add.sprite(334+mobileX,121+mobileY, 'game3.flag_2_1');
        game3.flag_2_1Animation = game3.flag_2_1.animations.add('flag_2_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
        game3.flag_2_1.visible = false;   
        game3.flag_2_2= game.add.sprite(334+mobileX,121+mobileY, 'game3.flag_2_2');
        game3.flag_2_2Animation = game3.flag_2_2.animations.add('flag_2_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
        game3.flag_2_2.visible = false; 
        game3.flag_2_1Animation.onComplete.add(function(){
            game3.flag_2_2Animation.play();
            game3.flag_2_1.visible = false;
            game3.flag_2_2.visible = true;
        });
        game3.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'game3.game3_top');
        game3.blue_screen_arr[1] = game.add.sprite(94+mobileX,54+mobileY, 'game3.blue_screen_1');
        game3.blue_screen_arr[1].visible = false;
        for (var i = 2; i <= 5; ++i){
            game3.blue_screen_arr[i] = game.add.sprite(94+121+(i-2)*128+mobileX,54+mobileY, 'game3.blue_screen_2');
            game3.blue_screen_arr[i].visible = false;
        }


        win_1Sound.onStop.add(function(){
            game3.mountain_2.visible = true; 
            game3.sun.visible = true; 
            game3.flag_1Animation.play();
            game3.flag_1.visible = true; 
            win_2Sound.play();
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });
            var textt = game.add.text(501+50+40+mobileX,174+20+5+mobileY, ropeValues[ropeStep], { font: '28px \"TeX Gyre Schola Bold\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});
            updateBalanceGame3(game, scorePosions, balanceR);
        });
        rope_1Sound.onStop.add(function(){
            if (game3.climb)
                rope_2Sound.play();
        });
        rope_2Sound.onStop.add(function(){
            if (game3.climb)
                rope_3Sound.play();
        });   
        rope_3Sound.onStop.add(function(){
            if (game3.climb)
                rope_4Sound.play();
        });  
        rope_4Sound.onStop.add(function(){
            if (game3.climb)
                rope_5Sound.play();
        });   
        rope_5Sound.onStop.add(function(){
            if (game3.climb)
                rope_6Sound.play();
        });  
        rope_6Sound.onStop.add(function(){
            if (game3.climb)
                rope_7Sound.play();
        });
        rope_7Sound.onStop.add(function(){
            if (game3.climb)
                rope_1Sound.play();
        });

        function takeBananaRope(x,y,lineNumber){
            lockDisplay();
            game3.climb = true;
            game3.men.position.x = x+mobileX;
            if(ropeValues[ropeStep] == 0){
                ji = game3.floor*10 + lineNumber;
                game3.void_rope[ji].visible = true;
                game3.eyes_arr[ji].visible = true;
                game3.hook_arr[ji].visible = false;
                movingWall(game3.hit_1);                    
                movingWall(game3.hit_2);  
            }
            game3.floor +=1;
            movingWall(game3.mountain_1);   
            game3.men.visible = false;
            game3.men_climb.position.x = game3.men.position.x+18;
            game3.men_climbAnimation.play();
            game3.men_climb.visible = true;
            rope_1Sound.play();
            for (var j = game3.floor; j <= 5; ++j) {
                ji = j*10 + lineNumber;
                game3.rope_arr[ji].visible = false;
                if (j>1)
                    game3.stone_arr[ji].visible = true;
                if (j<=4)
                    game3.hook_arr[ji].visible = false;
                game3.hookfinal_arr[lineNumber].visible = false;
            }
            for (var j = 1; j <= 5; ++j) {       
                movingWall(game3.hookfinal_arr[j])         
                movingWall(game3.bottom_bg_arr[j]);
                for (var i = 1; i <= 5; ++i) {
                    ji = j*10 + i;
                    movingWall(game3.rope_arr[ji]);
                    movingWall(game3.void_rope[ji]);
                    movingWall(game3.eyes_arr[ji]);
                    if (j>1)
                        movingWall(game3.stone_arr[ji]);
                    if (j<=4)
                        movingWall(game3.hook_arr[ji]);
                }
            }
        }
        function movingWall(item) {
            if(ropeValues[ropeStep] == 0){
                game.add.tween(item).to({y:item.position.y+762}, 3000, Phaser.Easing.LINEAR, true).onComplete.add(function(){    
                    game3.climb = false;
                    rope_1Sound.stop();
                    rope_2Sound.stop();
                    rope_3Sound.stop();
                    rope_4Sound.stop();
                    rope_5Sound.stop();
                    rope_6Sound.stop();
                    rope_7Sound.stop();
                    game3.men_climbAnimation.stop();
                    game3.hit_1.position.x = game3.men_climb.position.x-2;
                    game3.hit_2.position.x = game3.men_climb.position.x-2;
                    game3.hit_1.visible = true;    
                    setTimeout(function() {
                        hit_game3.play();                        
                    }, 500);
                    game3.hit_1Animation.play();
                    game3.hit_1Animation.onComplete.add(function(){
                        game3.men_climb.visible = false;
                        game3.hit_1.visible = false;
                        game3.hit_2.visible = true;
                        game3.hit_2Animation.play();
                        game3.fall_1_men.visible = true;
                        game3.fall_1_menAnimation.play();
                        game3.fall_1_men.position.x = game3.men_climb.position.x-18;
                        game3.fall_2_men.position.x = game3.men_climb.position.x-18;
                        game3.fall_3_men.position.x = game3.men_climb.position.x-18;
                        game3.fall_1_men.position.y = game3.men_climb.position.y;
                        game3.fall_2_men.position.y = game3.men_climb.position.y;
                        game3.fall_3_men.position.y = game3.men_climb.position.y;
                        game3.fall_1_menAnimation.onComplete.add(function(){
                            game3.hit_2.visible = false;
                            game3.fall_1_men.visible = false;
                            game3.fall_2_men.visible = true;
                            game3.fall_2_menAnimation.play();
                        });
                        game.add.tween(item).to({y:item.position.y-760}, 1500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                            game3.fall_2_men.visible = false;
                            game3.fall_2_menAnimation.stop();    
                            game3.fall_3_men.visible = true;
                            game3.fall_3_menAnimation.play();
                            updateBalanceGame3(game, scorePosions, balanceR);
                        });
                    });
                });
            } else if (game3.floor <=5){
                game.add.tween(item).to({y:item.position.y+1024}, 4000, Phaser.Easing.LINEAR, true).onComplete.add(function(){                
                    game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
                    game3.climb = false;
                    rope_1Sound.stop();
                    rope_2Sound.stop();
                    rope_3Sound.stop();
                    rope_4Sound.stop();
                    rope_5Sound.stop();
                    rope_6Sound.stop();
                    rope_7Sound.stop();
                    game3.men_climb.visible = false;
                    game3.men_climbAnimation.stop();
                    game3.top_men_1.position.x = game3.men_climb.position.x-18;
                    game3.top_men_1.position.y = game3.men_climb.position.y;
                    game3.top_men_1.visible = true;
                    game3.top_men_1Animation.play();
                    game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        game3.top_men_1.visible = false;
                        game3.top_men_2.position.x = game3.top_men_1.position.x;
                        game3.top_men_2.position.y = game3.top_men_1.position.y;
                        game3.top_men_2.visible = true;
                        game3.top_men_2Animation.play();
                    });
                });
            } else {
                game.add.tween(item).to({y:item.position.y+978}, 3900, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                  game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
                  game3.climb = false;
                  rope_1Sound.stop();
                  rope_2Sound.stop();
                  rope_3Sound.stop();
                  rope_4Sound.stop();
                  rope_5Sound.stop();
                  rope_6Sound.stop();
                  rope_7Sound.stop();
                  game3.men_climb.visible = false;
                  game3.men_climbAnimation.stop();
                  game3.top_men_1.position.x = game3.men_climb.position.x-18;
                  game3.top_men_1.position.y = game3.men_climb.position.y;
                  game3.top_men_1.visible = true;
                  game3.top_men_1Animation.play();
                  game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game3.top_men_1.visible = false;
                    game3.top_men_2.position.x = game3.top_men_1.position.x;
                    game3.top_men_2.position.y = game3.top_men_1.position.y;
                    game3.top_men_2.visible = true;
                    game3.top_men_2Animation.play();
                });
              });
            }
        }
        game3.top_men_2Animation.onComplete.add(function(){
            game3.top_men_2.visible = false;            
            game3.top_men_3.position.x = game3.top_men_2.position.x;
            game3.top_men_3.position.y = game3.top_men_2.position.y;
            game3.top_men_3.visible = true;
            game3.top_men_3Animation.play();
            game.add.tween(game3.top_men_3).to({y:game3.top_men_3.y-48}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game3.top_men_3.visible = false;                
                game3.top_men_4.position.x = game3.top_men_3.position.x;
                game3.top_men_4.position.y = game3.top_men_3.position.y;
                game3.top_men_4.visible = true;
                game3.top_men_4Animation.play();
                game.add.tween(game3.top_men_4).to({y:game3.top_men_4.y-8}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game3.top_men_4.visible = false;
                    game3.top_men_5.position.x = game3.top_men_4.position.x;
                    game3.top_men_5.position.y = game3.top_men_4.position.y;
                    game3.top_men_5.visible = true;
                    game3.top_men_5Animation.play();
                    game.add.tween(game3.top_men_5).to({y:game3.top_men_5.y-24}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        game3.top_men_5.visible = false;
                        game3.top_men_6.position.x = game3.top_men_5.position.x;
                        game3.top_men_6.position.y = game3.top_men_5.position.y;
                        game3.top_men_6.visible = true;
                        game3.top_men_6Animation.play();
                    });
                });
            });
        });
        game3.top_men_6Animation.onComplete.add(function(){
            game3.top_men_6.visible = false;
            game3.top_men_7.position.x = game3.top_men_6.position.x;
            game3.top_men_7.visible = true;
            game3.top_men_7Animation.play();
        });
        game3.top_men_7Animation.onComplete.add(function(){
            game3.top_men_7.visible = false;
            game3.men.visible = true;
            game3.blue_screen_arr[game3.selectedRope].visible = true;
            if(ropeValues[ropeStep] != 0) { 
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
            }
            setTimeout(showWin,1, 150+128*(game3.selectedRope-1),52 , ropeValues[ropeStep], stepTotalWinR);
            unlockDisplay();
            ropeStep += 1;
            if (game3.floor > 5){
                win_1Sound.play();
            } else {
                game3.freeze = false;
                game3.men.position.y = game3.top_men_7.position.y;
            }
        });
        game3.flag_1Animation.onComplete.add(function(){
            game3.flag_1.visible = false; 
            game3.flag_2_1Animation.play();
            game3.flag_2_1.visible = true;

        });

            //счет
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            
            // background = game.add.sprite(94,54, 'game.backgroundGame3');
            background = game.add.sprite(0,0, 'game.background');
            //кнопки

            var selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            var payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            var betone = game.add.sprite(490,510, 'betone_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            var betmax = game.add.sprite(535,510, 'betmax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            var startButton = game.add.sprite(597, 510, 'startButton_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = true;
            buttonLine1.input.useHandCursor = true;

            var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = true;
            buttonLine5.input.useHandCursor = true;

            var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = true;
            buttonLine9.input.useHandCursor = true;

            buttonLine1.events.onInputOver.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1_p');
                }
            });
            buttonLine1.events.onInputOut.add(function(){
                if(buttonLine1.inputEnabled == true) {
                    buttonLine1.loadTexture('buttonLine1');
                }
            });

            buttonLine3.events.onInputOver.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3_p');
                }
            });
            buttonLine3.events.onInputOut.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3');
                }
            });

            buttonLine5.events.onInputOver.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5_p');
                }
            });
            buttonLine5.events.onInputOut.add(function(){
                if(buttonLine5.inputEnabled == true) {
                    buttonLine5.loadTexture('buttonLine5');
                }
            });

            buttonLine7.events.onInputOver.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7_p');
                }
            });
            buttonLine7.events.onInputOut.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7');
                }
            });

            buttonLine9.events.onInputOver.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9_p');
                }
            });
            buttonLine9.events.onInputOut.add(function(){
                if(buttonLine9.inputEnabled == true) {
                    buttonLine9.loadTexture('buttonLine9');
                }
            });

            buttonLine1.events.onInputDown.add(function(){
                takeBananaRope(94,312,1);
                game3.selectedRope = 1;
                buttonLine1.loadTexture('buttonLine1_d');
                buttonLine1.inputEnabled = false;
                buttonLine1.input.useHandCursor = false;

                // if(ropeValues[ropeStep] != 0) {
                //     stepTotalWinR += betline*lines*ropeValues[ropeStep];
                // }
                // setTimeout(showWin,2500, 130,41 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine3.events.onInputDown.add(function(){
                takeBananaRope(222,312,2);
                game3.selectedRope = 2;
                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                // if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                //     stepTotalWinR += betline*lines*ropeValues[ropeStep];
                // }

                // setTimeout(showWin,2500, 130+128,41 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine5.events.onInputDown.add(function(){
                takeBananaRope(350,312,3);
                game3.selectedRope = 3;
                buttonLine5.loadTexture('buttonLine5_d');
                buttonLine5.inputEnabled = false;
                buttonLine5.input.useHandCursor = false;

                // if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                //     stepTotalWinR += betline*lines*ropeValues[ropeStep];
                // }

                // setTimeout(showWin,2500, 130+128+128,41 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine7.events.onInputDown.add(function(){
                takeBananaRope(478,312,4);
                game3.selectedRope = 4;
                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                // if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                //     stepTotalWinR += betline*lines*ropeValues[ropeStep];
                // }

                // setTimeout(showWin,2500, 130+128+128+128,41 , ropeValues[ropeStep], stepTotalWinR);
            });
            buttonLine9.events.onInputDown.add(function(){
                takeBananaRope(606,312,5);
                game3.selectedRope = 5;
                buttonLine9.loadTexture('buttonLine9_d');
                buttonLine9.inputEnabled = false;
                buttonLine9.input.useHandCursor = false;

                // if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                //     stepTotalWinR += betline*lines*ropeValues[ropeStep];
                // }

                // setTimeout(showWin,2500, 130+128+128+128+128,41 , ropeValues[ropeStep], stepTotalWinR);
            });
            
            //анимации и логика



            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = false; //проверка доступа ко второй бонусной игре

            full_and_sound();


        };

        game3.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game3', game3);

})();

}