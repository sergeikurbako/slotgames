function randomNumber(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var isMobile = false;
var mobileX = null;
var mobileY = null;
if (navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
    ) {
    isMobile = true;
mobileX = -94;
mobileY = -54;
}
var width = 829.7;
var height = 598.95;

if(isMobile) {
    width = 640;
    height = 480;
}
var game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');
var flickerInterval = '';
var fullStatus = false;
var soundStatus = true;
function createLevelButtons() {
    var lvl1 = game.add.sprite(0,0, 'x');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(30, 0, 'x');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');        
        game2.ropePositionX = 257;
    }, this);

    var lvl3 = game.add.sprite(60, 0, 'x');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');
        game3.freeze = false;
        game3.floor = 1;
    }, this);
}
function full_and_sound(){
    if (!fullStatus)
       full = game.add.sprite(740+mobileX,30+mobileY, 'non_full');
   else
    full = game.add.sprite(740+mobileX,30+mobileY, 'full');
full.inputEnabled = true;
full.input.useHandCursor = true;
full.events.onInputUp.add(function(){
    if (fullStatus == false){
       full.loadTexture('full');
       fullStatus = true;
       if(document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
    } else if(document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
    } else if(document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen();
    }
} else {
   full.loadTexture('non_full');
   fullStatus = false;
   if(document.cancelFullScreen) {
    document.cancelFullScreen();
} else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
} else if(document.webkitCancelFullScreen) {
    document.webkitCancelFullScreen();
}
}
});
if (soundStatus)
   sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_on');
else
   sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_off');
sound.inputEnabled = true;
sound.input.useHandCursor = true;
sound.events.onInputUp.add(function(){
    if (soundStatus == true){
       sound.loadTexture('sound_off');
       soundStatus =false;
       game.sound.mute = true;
   } else {
       sound.loadTexture('sound_on');
       soundStatus = true;
       game.sound.mute = false;
   }
});
}
function selectLine(n) {
    game1.currentLine = n;

    for (var i = 1; i <= 9; ++i) {
        game1.lines[i].sprite.visible = false;
        game1.lines[i].number.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        game1.lines[i].sprite.visible = true;
        game1.lines[i].number.visible = true;
        game1.lines[i].sprite.loadTexture('line_' + i);
        if (i === 5)
            game1.lines[5].sprite.position.y = 144+mobileY;
        if (i === 7)
            game1.lines[7].sprite.position.y = 318+mobileY;

    }
};
function preselectLine(n) {
    for (var i = 1; i <= 9; ++i) {
        game1.lines[i].sprite.visible = false;
        game1.lines[i].number.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        game1.lines[i].sprite.loadTexture('linefull_' + i);        
        game1.lines[i].sprite.visible = true;
        game1.lines[i].number.visible = true;
        if (i === 5)
            game1.lines[5].sprite.position.y = 131+mobileY;
        if (i === 7)
            game1.lines[7].sprite.position.y = 263+mobileY;

    }
    // game1.lines[5].sprite.rotation = 1;
}
function disableLinesBtn(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            game1.lines[i].button.loadTexture('btnline_d' + i);
        }
    }
};
function unDisableLines(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            game1.lines[i].button.loadTexture('btnline' + i);            
        }
    }    
};
function winLine(n) {
    game1.lines[n].sprite.loadTexture('linefull_' + n);
    game1.lines[n].sprite.visible = true;
    winNumber(n);
};
function winNumber(i){
 flickerInterval = setInterval(              
    function(){
        if (game1.takeWin){                        
            game1.lines[i].number.visible = false;
        } 
        unflicker(i);
    }, 1000);
};
function unflicker(i){
    setTimeout(function() {
        game1.lines[i].number.visible = true;
    }, 500);
}
function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}
//локация 1
var game1 = {
    bars: [],
    currentLine : 9,
    spinStatus : false,
    barsCurrentSpins : [0, 0, 0, 0, 0],
    barsTotalSpins : [15, 27, 39, 51, 63 ],
    countPlayBars : 0,
    takeWin : false,
    pages : [],
    settingsMode : false,
    currentPage : null,
    lines : {
        1: {
            coord: 243,
            sprite: null,
            btncoord : 250,
            button : null,
            number : null,
            numbercoord : 230
        },
        2: {
            coord: 110,
            sprite: null,
            number : null,
            numbercoord :86
        },
        3: {
            coord: 383,
            sprite: null,
            btncoord : 295,
            button : null,
            number : null,
            numbercoord : 374
        },
        4: {
            coord: 158,
            sprite: null,
            number : null,
            numbercoord : 150
        },
        5: {
            coord: 144,
            sprite: null,
            btncoord : 340,
            button : null,
            number : null,
            numbercoord : 310
        },
        6: {
            coord: 132,
            sprite: null,
            number : null,
            numbercoord :118
        },
        7: {
            coord: 318,
            sprite: null,
            btncoord : 385,
            button : null,
            number : null,
            numbercoord : 342
        },
        8: {
            coord: 270,
            sprite: null,
            number : null,
            numbercoord : 262
        },
        9: {
            coord: 159,
            sprite: null,
            btncoord : 430,
            button : null,
            number : null,
            numbercoord : 198
        },

    },
    create:function(){
        for (var i = 0; i < 5; ++i) {
           game1.bars[i] = game.add.tileSprite(142+mobileX+i*112, 87+mobileY, 96, 322, 'game1.bar');
           game1.bars[i].tilePosition.y =  randomNumber(0,8)*112 ; 
       }
       game1.rope_bar_win_1 = game.add.sprite(142+mobileX, 87+mobileY, 'rope_bar_win');
       game1.rope_bar_win_1Animation = game1.rope_bar_win_1.animations.add('rope_bar_win', [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9], 8, false);
       game1.rope_bar_win_1.visible = false;
       game1.rope_bar_win_2 = game.add.sprite(254+mobileX, 87+mobileY, 'rope_bar_win');
       game1.rope_bar_win_2Animation = game1.rope_bar_win_2.animations.add('rope_bar_win', [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9], 8, false);
       game1.rope_bar_win_2.visible = false;
       game1.rope_bar_win_3 = game.add.sprite(366+mobileX, 87+mobileY, 'rope_bar_win');
       game1.rope_bar_win_3Animation = game1.rope_bar_win_3.animations.add('rope_bar_win', [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9], 8, false);
       game1.rope_bar_win_3.visible = false;
       game1.bg = game.add.sprite(94+mobileX,22+mobileY, 'game1.bg');
       game1.fire = game.add.sprite(318+mobileX, 406+mobileY, 'fire');
       game1.fireAnimation = game1.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true);
       game1.fire.visible = true;             
       game1.tent = game.add.sprite(94+mobileX, 422+mobileY, 'tent');
       game1.tentAnimation = game1.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true);
       game1.tent.visible = true;       
       game1.cup = game.add.sprite(414+mobileX, 390+mobileY, 'cup');
       game1.cupAnimation = game1.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
       game1.cup.visible = false;  
       game1.glasses = game.add.sprite(414+mobileX, 406+mobileY, 'glasses');
       game1.glassesAnimation = game1.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false);
       game1.glasses.visible = true;  
       game1.head_left_right = game.add.sprite(414+mobileX, 390+mobileY, 'head_left_right');
       game1.cap = game.add.sprite(446+mobileX, 391+mobileY, 'cap');
       game1.cap.visible = false; 
       game1.head_left_rightAnimation = game1.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
       game1.head_left_right.visible = false;  
       game1.win = game.add.sprite(414+mobileX, 406+mobileY, 'win');
       game1.winAnimation = game1.win.animations.add('win', [0,1,2,3,4,5,1,0], 6, false);
       game1.win.visible = false;  
       game1.glassesAnimation.onComplete.add(function(){
         game1.glasses.visible = false;
         game1.cup.visible = true;
         game1.cupAnimation.play();
         game1.cap.visible = false;
     });
       game1.head_left_rightAnimation.onComplete.add(function(){
         game1.head_left_right.visible = false;
         game1.glasses.visible = true;
         game1.glassesAnimation.play();
         game1.cap.visible = true;
     });
       game1.cupAnimation.onComplete.add(function(){
         game1.cup.visible = false;
         game1.head_left_right.visible = true;
         game1.head_left_rightAnimation.play();
         game1.cap.visible = false;
     });
       game1.winAnimation.onComplete.add(function(){
        game1.win.visible = false;
        game1.cup.visible = true;
        game1.cupAnimation.play();
        game1.cap.visible = false;
    });
       for (var i = 1; i <= 9; ++i) {
        game1.lines[i].sprite = game.add.sprite(134+mobileX, game1.lines[i].coord+mobileY, 'line_' + i);
        game1.lines[i].sprite.visible = false;
        game1.lines[i].number = game.add.sprite(94+mobileX, game1.lines[i].numbercoord+mobileY, 'win_' + i);
        game1.lines[i].number.visible = false;
    }
    for (var i = 1; i <= 5; ++i) {
        game1.page = game.add.sprite(94+mobileX, 22+mobileY, 'game1.page_' + i);
        game1.page.visible = false;
        game1.pages[i] = game1.page;
    }
    prev_page = game.add.sprite(94+mobileX, 454+mobileY, 'prev_page');
    prev_page.visible = false;
    prev_page.inputEnabled = true;
    prev_page.input.useHandCursor = true;
    prev_page.events.onInputUp.add(function(){
        if (game1.settingsMode)  {
            pageSound.play();
            if (game1.currentPage == 1)
                game1.currentPage = 5;
            else{
                game1.pages[game1.currentPage].visible = false;
                game1.currentPage -=1;
            }
        }
        game1.pages[game1.currentPage].visible = true;
    });
    exit_btn = game.add.sprite(303+mobileX, 454+mobileY, 'exit_btn');
    exit_btn.visible = false;
    exit_btn.inputEnabled = true;
    exit_btn.input.useHandCursor = true;
    exit_btn.events.onInputUp.add(function(){
        pageSound.play();
        for (var i = 1; i <= 5; ++i) {
            game1.pages[i].visible = false;
        }            
        prev_page.visible = false;
        exit_btn.visible = false;
        next_page.visible = false;
        border_btns.visible = false;
        game1.settingsMode = false;
        if (!isMobile){
            unDisableLines()
            game1.automatic_start.loadTexture('automatic_start');
            game1.bet_max.loadTexture('bet_max');
            game1.bet_one.loadTexture('bet_one');
            game1.paytable.loadTexture('paytable');
            game1.select_game.loadTexture('select_game');
        } else {
            doubleb.visible = true;
            dollar.visible = true;
            home.visible = true;
            button.visible = true;
            bet1.visible = true;
            gear.visible = true;
        }
    });
    next_page = game.add.sprite(527+mobileX, 454+mobileY, 'next_page');
    next_page.visible = false;
    next_page.inputEnabled = true;
    next_page.input.useHandCursor = true;
    next_page.events.onInputUp.add(function(){
        if (game1.settingsMode)  {
            pageSound.play();
            if (game1.currentPage == 5){
                game1.pages[game1.currentPage].visible = false;
                game1.currentPage = 1;
            } else if (game1.currentPage == 1){
                game1.currentPage +=1;
            } else {                    
                game1.pages[game1.currentPage].visible = false;
                game1.currentPage +=1;
            }
        }
        game1.pages[game1.currentPage].visible = true;
    });
    border_btns = game.add.sprite(299+mobileX, 454+mobileY, 'border_btns');
    border_btns.visible = false;
    if(!isMobile){
        game.add.sprite(0,0, 'main_window');
        full_and_sound();
    }

    if(!isMobile){
        for (var i = 1; i <= 9; ++i) {
            if (i % 2 != 0) {
                game1.lines[i].sound = game.add.audio('line' + i);
                game1.lines[i].button = game.add.sprite(game1.lines[i].btncoord+mobileX, 510+mobileY, 'btnline' + i);
                game1.lines[i].button.scale.setTo(0.7, 0.7);
                game1.lines[i].button.inputEnabled = true;
                game1.lines[i].button.input.useHandCursor = true;
                (function (n) {
                    game1.lines[n].button.events.onInputOver.add(function(){
                        if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
                            return;
                        game1.lines[n].button.loadTexture('btnline_p' + n);
                    });
                    game1.lines[n].button.events.onInputOut.add(function(){
                        if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
                            return;
                        game1.lines[n].button.loadTexture('btnline' + n);
                    });
                    game1.lines[n].button.events.onInputUp.add(function () {
                        if(game1.spinStatus || game1.takeWin || game1.settingsMode)
                            return;
                        hideLines();
                        selectLine(n);
                        game1.lines[n].sound.play();
                    }, this);
                })(i);
            }
        }            
    }
    if (isMobile){
        button = game.add.sprite(544, 188, 'spin');
        button.inputEnabled = true;
        button.input.useHandCursor = true;

        bet1 = game.add.sprite(548, 274, 'bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputDown.add(function(){
            bet1.loadTexture('bet1_p');
        });
        bet1.events.onInputUp.add(function(){
            document.getElementById('betMode').style.display = 'block';
            widthVisibleZone = $('.betWrapper .visibleZone').height();
            console.log(widthVisibleZone);
            $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
            $('canvas').css('display', 'none');
            bet1.loadTexture('bet1');
        });
        home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });
        dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
        });

        gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            game1.pages[1].visible = true;
            prev_page.visible = true;
            exit_btn.visible = true;
            next_page.visible = true;
            border_btns.visible = true;
            game1.settingsMode = true;
            game1.currentPage = 1;
            doubleb.visible = false;
            dollar.visible = false;
            home.visible = false;
            button.visible = false;
            bet1.visible = false;
            gear.visible = false;
        });

        doubleb = game.add.sprite(546, 133, 'double');
        doubleb.inputEnabled = true;
        doubleb.input.useHandCursor = true;
        doubleb.events.onInputDown.add(function(){
            game.state.start('game2');
            game2.ropePositionX = 257;
        });
        function buttonClicked() {
            if (game1.spinStatus) {
                return;
            }
            button.loadTexture('spin_p');
        }
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(btnStartUp, this);
    }
    if(!isMobile){
        game1.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
        game1.startButton.scale.setTo(0.7, 0.7);
        game1.startButton.inputEnabled = true;
        game1.startButton.input.useHandCursor = true;
        game1.startButton.events.onInputUp.add(btnStartUp, this);
        game1.startButton.events.onInputOver.add(function(){
            if(game1.spinStatus)
                return;
            game1.startButton.loadTexture('start_p');
        });
        game1.startButton.events.onInputOut.add(function(){
            if(game1.spinStatus)
                return;
            game1.startButton.loadTexture('start');
        });

        game1.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');        
        game1.automatic_start.scale.setTo(0.7, 0.7);
        game1.automatic_start.inputEnabled = true;
        game1.automatic_start.input.useHandCursor = true;
        game1.automatic_start.events.onInputOver.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.automatic_start.loadTexture('automatic_start_p');
        });
        game1.automatic_start.events.onInputOut.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.automatic_start.loadTexture('automatic_start');
        });
        game1.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
        game1.bet_max.scale.setTo(0.7, 0.7);
        game1.bet_max.inputEnabled = true;
        game1.bet_max.input.useHandCursor = true;
        game1.bet_max.events.onInputOver.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.bet_max.loadTexture('bet_max_p');
        });
        game1.bet_max.events.onInputOut.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.bet_max.loadTexture('bet_max');
        });
        game1.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
        game1.bet_one.scale.setTo(0.7, 0.7);
        game1.bet_one.inputEnabled = true;
        game1.bet_one.input.useHandCursor = true;
        game1.bet_one.events.onInputOver.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.bet_one.loadTexture('bet_one_p');
        });
        game1.bet_one.events.onInputOut.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.bet_one.loadTexture('bet_one');
        });
        game1.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
        game1.paytable.scale.setTo(0.7, 0.7);
        game1.paytable.inputEnabled = true;
        game1.paytable.input.useHandCursor = true;
        game1.paytable.events.onInputOver.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.paytable.loadTexture('paytable_p');
        });
        game1.paytable.events.onInputOut.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.paytable.loadTexture('paytable');
        });
        game1.paytable.events.onInputUp.add(function(){
            game1.pages[1].visible = true;
            prev_page.visible = true;
            exit_btn.visible = true;
            next_page.visible = true;
            border_btns.visible = true;
            game1.settingsMode = true;
            game1.currentPage = 1;
            game1.automatic_start.loadTexture('automatic_start_d');
            game1.bet_max.loadTexture('bet_max_d');
            game1.bet_one.loadTexture('bet_one_d');
            game1.paytable.loadTexture('paytable_d');
            game1.select_game.loadTexture('select_game_d');
            game1.lines[3].button.loadTexture('btnline_d' + 3);
            game1.lines[5].button.loadTexture('btnline_d' + 5);
            game1.lines[7].button.loadTexture('btnline_d' + 7);
        }, this);
        game1.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
        game1.select_game.scale.setTo(0.7, 0.7);
        game1.select_game.inputEnabled = true;
        game1.select_game.input.useHandCursor = true;
        game1.select_game.events.onInputOver.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.select_game.loadTexture('select_game_p');
        });
        game1.select_game.events.onInputOut.add(function(){
            if(game1.spinStatus || game1.settingsMode)
                return;
            game1.select_game.loadTexture('select_game');
        });
    }
    stopSound = game.add.audio('stop');
    rotateSound = game.add.audio('rotate');
    tadaSound = game.add.audio('tada');
    pageSound = game.add.audio('page');
    rowWinSound = game.add.audio('rowWin');
    takeWinSound = game.add.audio('takeWin');
    takeWinSound.addMarker('take', 0, 0.6);
    rotateSound.loop = true;

    game1.fireAnimation.play();
    game1.tentAnimation.play();
    game1.glassesAnimation.play();
    game1.cap.visible = true; 
    if(!isMobile){
        game1.lines[1].button.events.onInputUp.add(function () {
            if (game1.settingsMode)  {
                pageSound.play();
                if (game1.currentPage == 1)
                    game1.currentPage = 5;
                else{
                    game1.pages[game1.currentPage].visible = false;
                    game1.currentPage -=1;
                }
                game1.pages[game1.currentPage].visible = true;
            }
        });      
        game1.lines[9].button.events.onInputUp.add(function () {
         if (game1.settingsMode)  {
            pageSound.play();
            if (game1.currentPage == 5){
                game1.pages[game1.currentPage].visible = false;
                game1.currentPage = 1;
            } else if (game1.currentPage == 1){
                game1.currentPage +=1;
            } else {                    
                game1.pages[game1.currentPage].visible = false;
                game1.currentPage +=1;
            }
            game1.pages[game1.currentPage].visible = true;
        }
    });
        game1.lines[1].button.events.onInputOver.add(function(){
            if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                game1.lines[1].button.loadTexture('btnline_p' + 1);
        });
        game1.lines[1].button.events.onInputOut.add(function(){
            if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                game1.lines[1].button.loadTexture('btnline' + 1);
        });
        game1.lines[9].button.events.onInputOver.add(function(){
            if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                game1.lines[9].button.loadTexture('btnline_p' + 9);
        });
        game1.lines[9].button.events.onInputOut.add(function(){
            if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                game1.lines[9].button.loadTexture('btnline' + 9);
        });
    }
    function btnStartUp(){
        if(game1.spinStatus)
            return;
        if (game1.settingsMode){
            pageSound.play();
            for (var i = 1; i <= 5; ++i) {
                game1.pages[i].visible = false;
            }            
            prev_page.visible = false;
            exit_btn.visible = false;
            next_page.visible = false;
            border_btns.visible = false;
            game1.settingsMode = false;
            unDisableLines()
            game1.automatic_start.loadTexture('automatic_start');
            game1.bet_max.loadTexture('bet_max');
            game1.bet_one.loadTexture('bet_one');
            game1.paytable.loadTexture('paytable');
            game1.select_game.loadTexture('select_game');
            console.log(game1.settingsMode);
        } else{
            if (game1.takeWin){
                game1.takeWin = false;
                unDisableLines();
                selectLine(game1.currentLine);
                clearInterval(flickerInterval);
                takeWinSound.play('take');
            } else{
                if (!isMobile){
                    game1.startButton.loadTexture('start_d');
                    game1.automatic_start.loadTexture('automatic_start_d');
                    game1.bet_max.loadTexture('bet_max_d');
                    game1.bet_one.loadTexture('bet_one_d');
                    game1.paytable.loadTexture('paytable_d');
                    game1.select_game.loadTexture('select_game_d');
                    disableLinesBtn();
                } else   
                button.loadTexture('spin');
                game1.countPlayBars = 5;
                game1.barsCurrentSpins = [0, 0, 0, 0, 0];
                game1.spinStatus = true;
                hideLines();
                rotateSound.play();
            }
        }
    };
    createLevelButtons();
    if (isMobile)
        game1.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
    function hideLines() {
        for (var i = 1; i <= 9; ++i) {
         game1.lines[i].sprite.visible = false;
     }

 }   

 game1.rope_bar_win_1Animation.onComplete.add(function(){
    game.state.start('game3');
});
 preselectLine(game1.currentLine);     
},
update:function(){
    if (game1.spinStatus){
        for (var i in game1.bars) {
            game1.barsCurrentSpins[i]++;
            if (game1.barsCurrentSpins[i] < game1.barsTotalSpins[i]) {
                game1.bars[i].tilePosition.y += 112;
            } else if (game1.barsCurrentSpins[i] == game1.barsTotalSpins[i]) {
                game1.countPlayBars--;
                stopSound.play();
            }
        }
        if (game1.countPlayBars === 0){
            game1.spinStatus = false;
            rotateSound.stop();
            if (!isMobile){   
                game1.startButton.loadTexture('start');
                game1.automatic_start.loadTexture('automatic_start');
                game1.bet_max.loadTexture('bet_max');
                game1.bet_one.loadTexture('bet_one');
                game1.paytable.loadTexture('paytable');
                game1.select_game.loadTexture('select_game');
            }
            if(game1.currentLine == 3)   {
                game1.takeWin = true;
                winLine(3);
                tadaSound.play();
                game1.cup.visible = false;
                game1.glasses.visible = false;
                game1.head_left_right.visible = false;
                game1.win.visible = true;
                game1.cap.visible = true;
                game1.winAnimation.play();
            } else if (game1.currentLine == 5){
                rowWinSound.play();
                game1.rope_bar_win_1Animation.play();
                game1.rope_bar_win_1.visible = true;
                game1.rope_bar_win_2Animation.play();
                game1.rope_bar_win_2.visible = true;
                game1.rope_bar_win_3Animation.play();
                game1.rope_bar_win_3.visible = true;
            } else {
                if(!isMobile){  
                    unDisableLines();
                }
                selectLine(game1.currentLine);   
            }
        }
    }
}
};
game.state.add('game1', game1); 

var game2 = {
    cards : [],
    ropePositionX : 257,
    cardValues : {1:5, 2:7, 3:12, 4:13, 5:15},
    // cardValues : {1:'5b', 2:'7c', 3:'12p', 4:'13t', 5:'15'},
    cardIndexes : null,
    create : function() {
        // game2.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
        game2.bg = game.add.sprite(94+mobileX,22+mobileY, 'game1.bg');
        if(!isMobile){
            game.add.sprite(0,0, 'main_window');
        }
        pickCardSound = game.add.audio('pickCard');
        cardWin = game.add.audio('cardWin');
        for(var i=1; i<=4; ++i) {
            game2.cards[i] = game.add.sprite(game2.ropePositionX-3+mobileX, 151+mobileY, 'shirt_cards');
            game2.ropePositionX += 112;
            if (isMobile){
                game2.cards[i].inputEnabled = true;
                game2.cards[i].input.useHandCursor = true;
            }
        }
        var buttonsPositionsX = {1:{pos:295,i:3}, 2:{pos:340,i:5}, 3:{pos:385,i:7}, 4:{pos:430,i:9}};
        var buttons = {};
        // var cardIndexes = {1 : '2b', 2: '2c', 3: '2p', 4: '2t'};        
        game2.cardIndexes = shuffle(Object.keys(game2.cardValues));
        game2.dealer_card = game.add.sprite(125+mobileX, 151+mobileY, 'card_'+game2.cardValues[game2.cardIndexes[0]]);
        game2.bg2 = game.add.sprite(94+mobileX,22+mobileY, 'game2.bg');
        game2.fire = game.add.sprite(318+mobileX, 406+mobileY, 'fire');
        game2.fireAnimation = game2.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true);
        game2.fire.visible = true;             
        game2.tent = game.add.sprite(94+mobileX, 422+mobileY, 'tent');
        game2.tentAnimation = game2.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true);
        game2.tent.visible = true;       
        game2.cup = game.add.sprite(414+mobileX, 390+mobileY, 'cup');
        game2.cupAnimation = game2.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
        game2.cup.visible = false;  
        game2.glasses = game.add.sprite(414+mobileX, 406+mobileY, 'glasses');
        game2.glassesAnimation = game2.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false);
        game2.glasses.visible = true;  
        game2.head_left_right = game.add.sprite(414+mobileX, 390+mobileY, 'head_left_right');
        game2.head_left_rightAnimation = game2.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
        game2.head_left_right.visible = false;  
        game2.winMen = game.add.sprite(414+mobileX, 406+mobileY, 'win');
        game2.winMenAnimation = game2.winMen.animations.add('winMen', [0,1,2,3,4,5,1,0], 6, false);
        game2.winMen.visible = false; 
        game2.cap = game.add.sprite(446+mobileX, 391+mobileY, 'cap');
        game2.cap.visible = false; 
        game2.glassesAnimation.onComplete.add(function(){
         game2.glasses.visible = false;
         game2.cup.visible = true;
         game2.cupAnimation.play();
         game2.cap.visible = false;
     });
        game2.head_left_rightAnimation.onComplete.add(function(){
         game2.head_left_right.visible = false;
         game2.glasses.visible = true;
         game2.glassesAnimation.play();
         game2.cap.visible = true;
     });
        game2.cupAnimation.onComplete.add(function(){
         game2.cup.visible = false;
         game2.head_left_right.visible = true;
         game2.head_left_rightAnimation.play();
         game2.cap.visible = false;
     });
        game2.winMenAnimation.onComplete.add(function(){
            game2.winMen.visible = false;
            game2.cup.visible = true;
            game2.cupAnimation.play();
            game2.cap.visible = false;
        });
        if(!isMobile)
            game.add.sprite(250+mobileX, 510+mobileY, 'btnline_d1').scale.setTo(0.7, 0.7);;
        for(var i in buttonsPositionsX) {
            if(!isMobile){
                var button = game.add.sprite(buttonsPositionsX[i].pos+mobileX, 510+mobileY, 'btnline' + buttonsPositionsX[i].i);
                button.scale.setTo(0.7, 0.7);
                button.inputEnabled = true;
                button.input.useHandCursor = true;
                (function(n, btn, pic){
                    btn.events.onInputUp.add(function () {
                        var card = game2.cards[n];
                        card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
                        game.add.sprite(card.position.x+13+mobileX, 315+mobileY, 'pick_card');
                        game2.lose = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.loseAnimation = game2.lose.animations.add('game2.lose', [0,1,3,3,3,3,3,3,3,3,3,3], 5, false);
                        game2.lose.visible = false;
                        game2.win = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.winAnimation = game2.win.animations.add('game2.win_lose', [0,1,2,2,2,2,2,2,2,2,2,2], 5, false);
                        game2.win.visible = false;                   
                        game2.forward = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.forwardAnimation = game2.forward.animations.add('game2.win_lose', [0,1,4,4,4,4,4,4,4,4,4,4], 5, false);
                        game2.forward.visible = false;  
                        game2.winAnimation.onComplete.add(function(){
                            game.state.start('game2');        
                            game2.ropePositionX = 257;
                        });           
                        game2.forwardAnimation.onComplete.add(function(){
                            game.state.start('game2');        
                            game2.ropePositionX = 257;
                        });         
                        game2.loseAnimation.onComplete.add(function(){
                            game.state.start('game1'); 
                        });   
                        pickCardSound.play();
                        var dealerValue = game2.cardValues[game2.cardIndexes[0]];
                        var userValue = game2.cardValues[game2.cardIndexes[n]];
                        console.log('dealer ' + dealerValue);
                        console.log('user ' + userValue);
                        if(dealerValue < userValue) {                            
                            game2.win.visible = true;
                            game2.winAnimation.play();
                            game2.winMenAnimation.play()
                            game2.winMen.visible = true;
                            game2.cup.visible = false; 
                            game2.glasses.visible = false; 
                            game2.head_left_right.visible = false;                             
                            game2.cap.visible = true;
                            cardWin.play();
                        } else if(dealerValue == userValue) {
                            game2.forward.visible = true;
                            game2.forwardAnimation.play();
                        } else {
                            game2.lose.visible = true;
                            game2.loseAnimation.play();
                        }

                        for(var c in game2.cards) {
                            game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
                        }
                        for(var b in buttons) {
                            buttons[b].loadTexture('btnline_d' + b);
                            buttons[b].inputEnabled = false;
                        }
                        

                    }, this);
                })(i, button, buttonsPositionsX[i].i);
            } else {
                (function(n, btn, pic){
                    btn.events.onInputUp.add(function () {
                        var card = game2.cards[n];
                        card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
                        game.add.sprite(card.position.x+13, 315+mobileY, 'pick_card');
                        game2.lose = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.loseAnimation = game2.lose.animations.add('game2.lose', [0,1,3,3,3,3,3], 5, false);
                        game2.lose.visible = false;
                        game2.win = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.winAnimation = game2.win.animations.add('game2.win_lose', [0,1,2,2,2,2,2], 5, false);
                        game2.win.visible = false;                   
                        game2.forward = game.add.sprite(381+mobileX, 342+mobileY, 'game2.win_lose');
                        game2.forwardAnimation = game2.forward.animations.add('game2.win_lose', [0,1,4,4,4,4,4], 5, false);
                        game2.forward.visible = false; 
                        game2.winAnimation.onComplete.add(function(){
                            game.state.start('game2');        
                            game2.ropePositionX = 257;
                        });           
                        game2.forwardAnimation.onComplete.add(function(){
                            game.state.start('game2');        
                            game2.ropePositionX = 257;
                        });         
                        game2.loseAnimation.onComplete.add(function(){
                            game.state.start('game1'); 
                        });   
                        pickCardSound.play();
                        var dealerValue = game2.cardValues[game2.cardIndexes[0]];
                        var userValue = game2.cardValues[game2.cardIndexes[n]];
                        console.log('dealer ' + dealerValue);
                        console.log('user ' + userValue);
                        if(dealerValue < userValue) {                            
                            game2.win.visible = true;
                            game2.winAnimation.play();
                            game2.cap.visible = true;
                            game2.winMenAnimation.play()
                            game2.winMen.visible = true;
                            game2.cup.visible = false; 
                            game2.glasses.visible = false; 
                            game2.head_left_right.visible = false;                            
                            cardWin.play();
                        } else if(dealerValue == userValue) {
                            game2.forward.visible = true;
                            game2.forwardAnimation.play();
                        } else {
                            game2.lose.visible = true;
                            game2.loseAnimation.play();
                        }

                        for(var c in game2.cards) {
                            game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
                        }
                        for(var b in game2.cards) {
                            game2.cards[b].inputEnabled = false;
                        }                        

                    }, this);
                })(i, game2.cards[i], buttonsPositionsX[i].i)
            }
            

            if (!isMobile){
                buttons[buttonsPositionsX[i].i] = button;
            } 
        }     
        if(!isMobile){
            game2.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
            game2.startButton.scale.setTo(0.7, 0.7);
            game2.startButton.inputEnabled = true;
            game2.startButton.input.useHandCursor = true;
            game2.startButton.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.startButton.loadTexture('start_p');
            });
            game2.startButton.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.startButton.loadTexture('start');
            });
            game2.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
            game2.automatic_start.scale.setTo(0.7, 0.7);
            game2.automatic_start.inputEnabled = true;
            game2.automatic_start.input.useHandCursor = true;
            game2.automatic_start.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.automatic_start.loadTexture('automatic_start_p');
            });
            game2.automatic_start.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.automatic_start.loadTexture('automatic_start');
            });
            game2.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
            game2.bet_max.scale.setTo(0.7, 0.7);
            game2.bet_max.inputEnabled = true;
            game2.bet_max.input.useHandCursor = true;
            game2.bet_max.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_max.loadTexture('bet_max_p');
            });
            game2.bet_max.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_max.loadTexture('bet_max');
            });
            game2.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
            game2.bet_one.scale.setTo(0.7, 0.7);
            game2.bet_one.inputEnabled = true;
            game2.bet_one.input.useHandCursor = true;
            game2.bet_one.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_one.loadTexture('bet_one_p');
            });
            game2.bet_one.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_one.loadTexture('bet_one');
            });
            game2.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
            game2.paytable.scale.setTo(0.7, 0.7);
            game2.paytable.inputEnabled = true;
            game2.paytable.input.useHandCursor = true;
            game2.paytable.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.paytable.loadTexture('paytable_p');
            });
            game2.paytable.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.paytable.loadTexture('paytable');
            });
            game2.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
            game2.select_game.scale.setTo(0.7, 0.7);
            game2.select_game.inputEnabled = true;
            game2.select_game.input.useHandCursor = true;
            game2.select_game.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.select_game.loadTexture('select_game_p');
            });
            game2.select_game.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.select_game.loadTexture('select_game');
            });
        }
        game2.fireAnimation.play();
        game2.tentAnimation.play();
        game2.glassesAnimation.play();        
        game2.cap.visible = true;
        createLevelButtons();
        if (isMobile)
            game2.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');

        if(!isMobile)
            full_and_sound();
    }
};
game.state.add('game2', game2);
var game3 = {
    freeze : false,
    climb : false,
    buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    ropesafeCount : 5,
    menPositionsX : {1:94, 3:222, 5:350, 7:478, 9:606},
    selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
    closedDoor : [1,3,5,7,9],
    selectedSafe : null,
    safe_hit_arr : [],
    rope_arr : [],
    stone_arr : [],
    hook_arr : [],
    blue_screen_arr : [],
    hookfinal_arr : [],
    bottom_bg_arr : [],
    safe_door_arr : [],
    safe_door_arr_anim : [],
    tween1: null,
    tween2: null,
    tween3: null,
    tween4: null,
    bg_main : null,
    floor: 1,
    selectedRope : null,

    create : function() {

     game3.mountain_1= game.add.sprite(94+mobileX,-3978-1152-544+mobileY, 'game3.mountain_1');
     for (var j = 1; j <= 5; ++j) {
         game3.bottom_bg_arr[j]= game.add.sprite(94+mobileX,374+mobileY-(j-1)*1152, 'game3.bottom_1');
     };   

     for (var j = 1; j <= 5; ++j) {
        for (var i = 1; i <= 5; ++i) {
            ji = j*10 + i;
            game3.rope_arr[ji] = game.add.sprite(94+mobileX+(i-1)*128,-522+mobileY-(j-1)*1152, 'game3.rope'+i);
            game3.rope_arr[ji].inputEnabled = true;
            if(j>1){
                game3.stone_arr[ji] = game.add.sprite(94+mobileX+(i-1)*128,-522+mobileY-(j-1)*1152, 'game3.stone'+i);
                game3.stone_arr[ji].visible = false;
            }
            if (j<=4)
                game3.hook_arr[ji] = game.add.sprite(126+mobileX+(i-1)*128,-730+mobileY-(j-1)*1152, 'game3.hook'+i);
        }
        game3.hookfinal_arr[j] = game.add.sprite(126+mobileX+(j-1)*128,-3978-1152-160+mobileY, 'game3.hookfinal_'+j);
    };
    game3.void_rope_1 = game.add.sprite(350+mobileX,-522+mobileY, 'game3.void_rope_1'); 
    game3.eyes = game.add.sprite(398+mobileX,-473+mobileY, 'game3.eyes');
    game3.eyesAnimation = game3.eyes.animations.add('game3.eyes', [0,1,2,3,4], 6, true);
    game3.eyesAnimation.play();
    game3.hit_1 = game.add.sprite(366+mobileX,-506+mobileY, 'game3.hit');
    game3.hit_1Animation = game3.hit_1.animations.add('game3.hit', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 15, false);
    game3.hit_1.visible = false;   
    game3.hit_2 = game.add.sprite(366+mobileX,-506+mobileY, 'game3.hit');
    game3.hit_2Animation = game3.hit_2.animations.add('game3.hit', [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33], 15, false);
    game3.hit_2.visible = false;

    game3.fall_1_men = game.add.sprite(366+mobileX,-506+mobileY, 'game3.fall_men');
    game3.fall_1_menAnimation = game3.fall_1_men.animations.add('game3.fall_men', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, false);
    game3.fall_1_men.visible = false;  
    game3.fall_2_men = game.add.sprite(366+mobileX,-506+mobileY, 'game3.fall_men');
    game3.fall_2_menAnimation = game3.fall_2_men.animations.add('game3.fall_men', [14,15,16,17], 10, true);
    game3.fall_2_men.visible = false;  
    game3.fall_3_men = game.add.sprite(366+mobileX,-506+mobileY, 'game3.fall_men');
    game3.fall_3_menAnimation = game3.fall_3_men.animations.add('game3.fall_men', [18,19,20,21,22,23,24,25,26,27], 10, false);
    game3.fall_3_men.visible = false;

    game3.hook_arr[13].visible = false;
    // game3.mountain_1
    // game3.hookfinal_' + i
    game3.men = game.add.sprite(94+mobileX,312+mobileY, 'game3.men');
    game3.men_climb = game.add.sprite(112+mobileX, 312+mobileY, 'game3.men_climb');
    game3.men_climbAnimation = game3.men_climb.animations.add('men_climb', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true);
    game3.men_climb.visible = false;    
    game3.top_men_1 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_1Animation = game3.top_men_1.animations.add('top_men_1', [0,1,2], 10, false);
    game3.top_men_1.visible = false;  
    game3.top_men_2 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_2Animation = game3.top_men_2.animations.add('top_men_2', [3,4,5,6], 10, false);
    game3.top_men_2.visible = false;  
    game3.top_men_3 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_3Animation = game3.top_men_3.animations.add('top_men_3', [7,8,9,10,11], 10, false);
    game3.top_men_3.visible = false;  
    game3.top_men_4 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_4Animation = game3.top_men_4.animations.add('top_men_4', [12,13,14], 10, false);
    game3.top_men_4.visible = false;  
    game3.top_men_5 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_5Animation = game3.top_men_5.animations.add('top_men_5', [15,16,17,18], 10, false);
    game3.top_men_5.visible = false;  
    game3.top_men_6 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_1');
    game3.top_men_6Animation = game3.top_men_6.animations.add('top_men_6', [19,20,21,22,23,24,25,26,27,28], 10, false);
    game3.top_men_6.visible = false;    
    game3.top_men_7 = game.add.sprite(94+mobileX, 312+mobileY, 'game3.top_men_2');
    game3.top_men_7Animation = game3.top_men_7.animations.add('top_men_7', [0,1,2,3,4,5,6,7], 10, false);
    game3.top_men_7.visible = false;  

    game3.mountain_2= game.add.sprite(94+mobileX,56+mobileY, 'game3.mountain_2');
    game3.mountain_2.visible = false;
    game3.sun= game.add.sprite(481+mobileX,154+mobileY, 'game3.sun');
    game3.sun.visible = false;
    game3.flag_1= game.add.sprite(334+mobileX,89+mobileY, 'game3.flag_1');
    game3.flag_1Animation = game3.flag_1.animations.add('flag_1', [0,1,2,3,4,5], 10, false);
    game3.flag_1.visible = false; 
    // game3.flag_2= game.add.sprite(334+mobileX,121+mobileY, 'game3.flag_2');
    // game3.flag_2Animation = game3.flag_2.animations.add('flag_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, true);
    // game3.flag_2.visible = false;   
    game3.flag_2_1= game.add.sprite(334+mobileX,121+mobileY, 'game3.flag_2_1');
    game3.flag_2_1Animation = game3.flag_2_1.animations.add('flag_2_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
    game3.flag_2_1.visible = false;   
    game3.flag_2_2= game.add.sprite(334+mobileX,121+mobileY, 'game3.flag_2_2');
    game3.flag_2_2Animation = game3.flag_2_2.animations.add('flag_2_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
    game3.flag_2_2.visible = false; 
    game3.flag_2_1Animation.onComplete.add(function(){
        game3.flag_2_2Animation.play();
        game3.flag_2_1.visible = false;
        game3.flag_2_2.visible = true;
    });
    game3.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'game3.game3_top');
    game3.blue_screen_arr[1] = game.add.sprite(94+mobileX,54+mobileY, 'game3.blue_screen_1');
    game3.blue_screen_arr[1].visible = false;
    for (var i = 2; i <= 5; ++i){
        game3.blue_screen_arr[i] = game.add.sprite(94+121+(i-2)*128+mobileX,54+mobileY, 'game3.blue_screen_2');
        game3.blue_screen_arr[i].visible = false;
    }

    if(!isMobile){
        game.add.sprite(0,0, 'main_window');
    }
    rope_1Sound = game.add.audio('rope_2');
    rope_2Sound = game.add.audio('rope_1');
    rope_3Sound = game.add.audio('rope_2');
    rope_4Sound = game.add.audio('rope_2');
    rope_5Sound = game.add.audio('rope_2');
    rope_6Sound = game.add.audio('rope_2');
    rope_7Sound = game.add.audio('rope_2');
    win_1Sound = game.add.audio('game3.win_1');
    win_2Sound = game.add.audio('game3.win_2');
    winSound = game.add.audio('winGame3');
    dynamiteSound = game.add.audio('dynamite');
    boomSound = game.add.audio('boom');
    win_1Sound.onStop.add(function(){
        game3.mountain_2.visible = true; 
        game3.sun.visible = true; 
        game3.flag_1Animation.play();
        game3.flag_1.visible = true; 
        win_2Sound.play();
    });
    rope_1Sound.onStop.add(function(){
        if (game3.climb)
            rope_2Sound.play();
    });
    rope_2Sound.onStop.add(function(){
        if (game3.climb)
            rope_3Sound.play();
    });   
    rope_3Sound.onStop.add(function(){
        if (game3.climb)
            rope_4Sound.play();
    });  
    rope_4Sound.onStop.add(function(){
        if (game3.climb)
            rope_5Sound.play();
    });   
    rope_5Sound.onStop.add(function(){
        if (game3.climb)
            rope_6Sound.play();
    });  
    rope_6Sound.onStop.add(function(){
        if (game3.climb)
            rope_7Sound.play();
    });
    rope_7Sound.onStop.add(function(){
        if (game3.climb)
            rope_1Sound.play();
    });

    for(var i in game3.menPositionsX) {
        if (!isMobile){  
            var button = game.add.sprite(game3.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
            button.scale.setTo(0.7, 0.7);
            button.inputEnabled = true;
            button.input.useHandCursor = true;     
            (function(n, btn){
                btn.events.onInputUp.add(function () {
                    if(game3.freeze) {
                        return;
                    }
                    game3.freeze = true;                    
                    game3.climb = true;
                    btn.loadTexture('btnline_d' + n);
                    btn.inputEnabled = false;
                    game3.men.position.x = game3.menPositionsX[n]+mobileX;
                    game3.selectedRope = game3.selectedRopeNormal[n];
                    console.log(game3.selectedRope);
                    game3.floor +=1;
                    movingWall(game3.mountain_1);
                    movingWall(game3.void_rope_1);                    
                    movingWall(game3.eyes);                    
                    movingWall(game3.hit_1);                    
                    movingWall(game3.hit_2);  
                    game3.men.visible = false;
                    game3.men_climb.position.x = game3.men.position.x+18;
                    game3.men_climbAnimation.play();
                    game3.men_climb.visible = true;
                    rope_1Sound.play();
                    for (var j = game3.floor; j <= 5; ++j) {
                        ji = j*10 + game3.selectedRope;
                        game3.rope_arr[ji].visible = false;
                        if (j>1)
                            game3.stone_arr[ji].visible = true;
                        if (j<=4)
                            game3.hook_arr[ji].visible = false;
                        game3.hookfinal_arr[game3.selectedRope].visible = false;
                    }
                    for (var j = 1; j <= 5; ++j) {       
                        movingWall(game3.hookfinal_arr[j])         
                        movingWall(game3.bottom_bg_arr[j]);
                        for (var i = 1; i <= 5; ++i) {
                            ji = j*10 + i;
                            movingWall(game3.rope_arr[ji]);
                            if (j>1)
                                movingWall(game3.stone_arr[ji]);
                            if (j<=4)
                                movingWall(game3.hook_arr[ji]);
                        }
                    }
                }, this);
            })(i, button);
        } else {
            for (var l = 1; l <= 5; ++l) {                
                il = game3.selectedRopeNormal[i]*10 + l;
                (function(n, btn){
                    btn.events.onInputUp.add(function () {
                        if(game3.freeze) {
                            return;
                        };
                        game3.freeze = true;
                        game3.climb = true;
                        btn.inputEnabled = false;
                        game3.men.position.x = game3.menPositionsX[n]+mobileX;
                        game3.selectedRope = game3.selectedRopeNormal[n];
                        game3.floor +=1;
                        movingWall(game3.mountain_1);
                        movingWall(game3.void_rope_1);                    
                        movingWall(game3.eyes);                    
                        movingWall(game3.hit_1);                    
                        movingWall(game3.hit_2); 
                        game3.men.visible = false;
                        game3.men_climb.position.x = game3.men.position.x+18;
                        game3.men_climbAnimation.play();
                        game3.men_climb.visible = true;
                        rope_1Sound.play();
                        for (var j = game3.floor; j <= 5; ++j) {
                            ji = j*10 + game3.selectedRope;
                            game3.rope_arr[ji].visible = false;
                            game3.stone_arr[ji].visible = true;
                            if (j<=4)
                                game3.hook_arr[ji].visible = false;
                            game3.hookfinal_arr[game3.selectedRope].visible = false;
                        };
                        for (var j = 1; j <= 5; ++j) {       
                            movingWall(game3.hookfinal_arr[j])         
                            movingWall(game3.bottom_bg_arr[j]);
                            for (var i = 1; i <= 5; ++i) {
                                ji = j*10 + i;
                                movingWall(game3.rope_arr[ji]);
                                if (j>1)
                                    movingWall(game3.stone_arr[ji]);
                                if (j<=4)
                                    movingWall(game3.hook_arr[ji]);
                            };
                        };
                    }, this);
                })((l-1)*2+1, game3.rope_arr[il]);
            }              
        }              
    }
    function movingWall(item) {
        if(game3.floor === 2 && game3.selectedRope === 3){
            game.add.tween(item).to({y:item.position.y+762}, 3000, Phaser.Easing.LINEAR, true).onComplete.add(function(){    
                game3.climb = false;
                rope_1Sound.stop();
                rope_2Sound.stop();
                rope_3Sound.stop();
                rope_4Sound.stop();
                rope_5Sound.stop();
                rope_6Sound.stop();
                rope_7Sound.stop();
                // game3.men_climb.visible = false;
                game3.men_climbAnimation.stop();
                game3.eyes.visible = false;
                game3.hit_1.visible = true;
                game3.hit_1Animation.play();
                game3.hit_1Animation.onComplete.add(function(){
                    game3.men_climb.visible = false;
                    game3.hit_1.visible = false;
                    game3.hit_2.visible = true;
                    game3.hit_2Animation.play();
                    game3.fall_1_men.visible = true;
                    game3.fall_1_menAnimation.play();
                    game3.fall_1_men.position.x = game3.men_climb.position.x-18;
                    game3.fall_2_men.position.x = game3.men_climb.position.x-18;
                    game3.fall_3_men.position.x = game3.men_climb.position.x-18;
                    game3.fall_1_men.position.y = game3.men_climb.position.y;
                    game3.fall_2_men.position.y = game3.men_climb.position.y;
                    game3.fall_3_men.position.y = game3.men_climb.position.y;
                    game3.fall_1_menAnimation.onComplete.add(function(){
                        game3.fall_1_men.visible = false;
                        game3.fall_2_men.visible = true;
                        game3.fall_2_menAnimation.play();
                    });
                    game.add.tween(item).to({y:item.position.y-740}, 1500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        game3.fall_2_men.visible = false;
                        game3.fall_2_menAnimation.stop();    
                        game3.fall_3_men.visible = true;
                        game3.fall_3_menAnimation.play();
                    });
                });
                // game3.top_men_1.position.x = game3.men_climb.position.x-18;
                // game3.top_men_1.position.y = game3.men_climb.position.y;
                // game3.top_men_1.visible = true;
                // game3.top_men_1Animation.play();
                // game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                //     game3.top_men_1.visible = false;
                //     game3.top_men_2.position.x = game3.top_men_1.position.x;
                //     game3.top_men_2.position.y = game3.top_men_1.position.y;
                //     game3.top_men_2.visible = true;
                //     game3.top_men_2Animation.play();
            });
        } else if (game3.floor <=5){
            game.add.tween(item).to({y:item.position.y+1024}, 4000, Phaser.Easing.LINEAR, true).onComplete.add(function(){                
                game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
                game3.climb = false;
                rope_1Sound.stop();
                rope_2Sound.stop();
                rope_3Sound.stop();
                rope_4Sound.stop();
                rope_5Sound.stop();
                rope_6Sound.stop();
                rope_7Sound.stop();
                game3.men_climb.visible = false;
                game3.men_climbAnimation.stop();
                game3.top_men_1.position.x = game3.men_climb.position.x-18;
                game3.top_men_1.position.y = game3.men_climb.position.y;
                game3.top_men_1.visible = true;
                game3.top_men_1Animation.play();
                game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game3.top_men_1.visible = false;
                    game3.top_men_2.position.x = game3.top_men_1.position.x;
                    game3.top_men_2.position.y = game3.top_men_1.position.y;
                    game3.top_men_2.visible = true;
                    game3.top_men_2Animation.play();
                });
            });
        } else {
            game.add.tween(item).to({y:item.position.y+978}, 3900, Phaser.Easing.LINEAR, true).onComplete.add(function(){
              game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
              game3.climb = false;
              rope_1Sound.stop();
              rope_2Sound.stop();
              rope_3Sound.stop();
              rope_4Sound.stop();
              rope_5Sound.stop();
              rope_6Sound.stop();
              rope_7Sound.stop();
              game3.men_climb.visible = false;
              game3.men_climbAnimation.stop();
              game3.top_men_1.position.x = game3.men_climb.position.x-18;
              game3.top_men_1.position.y = game3.men_climb.position.y;
              game3.top_men_1.visible = true;
              game3.top_men_1Animation.play();
              game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game3.top_men_1.visible = false;
                game3.top_men_2.position.x = game3.top_men_1.position.x;
                game3.top_men_2.position.y = game3.top_men_1.position.y;
                game3.top_men_2.visible = true;
                game3.top_men_2Animation.play();
            });
          });
        }
    }
    game3.top_men_2Animation.onComplete.add(function(){
        game3.top_men_2.visible = false;            
        game3.top_men_3.position.x = game3.top_men_2.position.x;
        game3.top_men_3.position.y = game3.top_men_2.position.y;
        game3.top_men_3.visible = true;
        game3.top_men_3Animation.play();
        game.add.tween(game3.top_men_3).to({y:game3.top_men_3.y-48}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
            game3.top_men_3.visible = false;                
            game3.top_men_4.position.x = game3.top_men_3.position.x;
            game3.top_men_4.position.y = game3.top_men_3.position.y;
            game3.top_men_4.visible = true;
            game3.top_men_4Animation.play();
            game.add.tween(game3.top_men_4).to({y:game3.top_men_4.y-8}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                game3.top_men_4.visible = false;
                game3.top_men_5.position.x = game3.top_men_4.position.x;
                game3.top_men_5.position.y = game3.top_men_4.position.y;
                game3.top_men_5.visible = true;
                game3.top_men_5Animation.play();
                game.add.tween(game3.top_men_5).to({y:game3.top_men_5.y-24}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game3.top_men_5.visible = false;
                    game3.top_men_6.position.x = game3.top_men_5.position.x;
                    game3.top_men_6.position.y = game3.top_men_5.position.y;
                    game3.top_men_6.visible = true;
                    game3.top_men_6Animation.play();
                });
            });
        });
    });
    game3.top_men_6Animation.onComplete.add(function(){
        game3.top_men_6.visible = false;
        game3.top_men_7.position.x = game3.top_men_6.position.x;
        game3.top_men_7.visible = true;
        game3.top_men_7Animation.play();
    });
    game3.top_men_7Animation.onComplete.add(function(){
        game3.top_men_7.visible = false;
        game3.men.visible = true;
        game3.blue_screen_arr[game3.selectedRope].visible = true;
        if (game3.floor > 5){
            win_1Sound.play();
        } else {
            game3.freeze = false;
            game3.men.position.y = game3.top_men_7.position.y;
        }
    });
    game3.flag_1Animation.onComplete.add(function(){
        game3.flag_1.visible = false; 
        game3.flag_2_1Animation.play();
        game3.flag_2_1.visible = true;
    });
    if(!isMobile){
        game3.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
        game3.startButton.scale.setTo(0.7, 0.7);
        game3.startButton.inputEnabled = true;
        game3.startButton.input.useHandCursor = true;
        game3.startButton.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.startButton.loadTexture('start_p');
        });
        game3.startButton.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.startButton.loadTexture('start');
        });
        game3.startButton.events.onInputUp.add(function(){   
        });
        game3.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
        game3.automatic_start.scale.setTo(0.7, 0.7);
        game3.automatic_start.inputEnabled = true;
        game3.automatic_start.input.useHandCursor = true;
        game3.automatic_start.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.automatic_start.loadTexture('automatic_start_p');
        });
        game3.automatic_start.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.automatic_start.loadTexture('automatic_start');
        });
        game3.automatic_start.events.onInputUp.add(function(){ 
        });
        game3.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
        game3.bet_max.scale.setTo(0.7, 0.7);
        game3.bet_max.inputEnabled = true;
        game3.bet_max.input.useHandCursor = true;
        game3.bet_max.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.bet_max.loadTexture('bet_max_p');
        });
        game3.bet_max.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.bet_max.loadTexture('bet_max');
        });
        game3.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
        game3.bet_one.scale.setTo(0.7, 0.7);
        game3.bet_one.inputEnabled = true;
        game3.bet_one.input.useHandCursor = true;
        game3.bet_one.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.bet_one.loadTexture('bet_one_p');
        });
        game3.bet_one.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.bet_one.loadTexture('bet_one');
        });
        game3.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
        game3.paytable.scale.setTo(0.7, 0.7);
        game3.paytable.inputEnabled = true;
        game3.paytable.input.useHandCursor = true;
        game3.paytable.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.paytable.loadTexture('paytable_p');
        });
        game3.paytable.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.paytable.loadTexture('paytable');
        });
        game3.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
        game3.select_game.scale.setTo(0.7, 0.7);
        game3.select_game.inputEnabled = true;
        game3.select_game.input.useHandCursor = true;
        game3.select_game.events.onInputOver.add(function(){
            if(game3.spinStatus)
                return;
            game3.select_game.loadTexture('select_game_p');
        });
        game3.select_game.events.onInputOut.add(function(){
            if(game3.spinStatus)
                return;
            game3.select_game.loadTexture('select_game');
        });
    }
    if (isMobile)
        game3.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');

    createLevelButtons();
    if(!isMobile)
        full_and_sound();
}

};
game.state.add('game3', game3);

var gamePreload = {

    preload:function(){
        if (isMobile){
            game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
                if(progress % 8 == 0) {
                    document.getElementById('preloaderBar').style.width = progress + '%';                    
                }
            });
        }
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        // game.load.image('main_window', 'img/main_window.png');
        if(!isMobile){
            game.load.image('main_window', 'img/shape1206.png');
        }
        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');  
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('cap', 'img/cap.png');
        game.load.image('x', 'img/x.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('game1.page_' + i, 'img/page_' + i + '.png');            
        }

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');
        game.load.image('border_btns', 'img/border_btns.png');

        // game.load.image('game1.bar', 'img/shape_full.png');
        game.load.image('game1.bar', 'img/shape_fullv3.png');
        if(!isMobile){
            game.load.image('start', 'img/btns/btn_start.png');
            game.load.image('start_p', 'img/btns/btn_start_p.png');
            game.load.image('start_d', 'img/btns/btn_start_d.png');
            game.load.image('automatic_start', 'img/btns/btn_automatic_start.png');
            game.load.image('automatic_start_p', 'img/btns/btn_automatic_start_p.png');
            game.load.image('automatic_start_d', 'img/btns/btn_automatic_start_d.png');
            game.load.image('bet_max', 'img/btns/btn_bet_max.png');
            game.load.image('bet_max_p', 'img/btns/btn_bet_max_p.png');
            game.load.image('bet_max_d', 'img/btns/btn_bet_max_d.png');
            game.load.image('bet_one', 'img/btns/btn_bet_one.png');
            game.load.image('bet_one_p', 'img/btns/btn_bet_one_p.png');
            game.load.image('bet_one_d', 'img/btns/btn_bet_one_d.png');
            game.load.image('paytable', 'img/btns/btn_paytable.png');
            game.load.image('paytable_p', 'img/btns/btn_paytable_p.png');
            game.load.image('paytable_d', 'img/btns/btn_paytable_d.png');
            game.load.image('select_game', 'img/btns/btn_select_game.png');
            game.load.image('select_gamev2', 'img/btns/btn_select_gamev2.png');
            game.load.image('select_game_p', 'img/btns/btn_select_game_p.png');
            game.load.image('select_game_d', 'img/btns/btn_select_game_d.png');
            game.load.image('non_full', 'img/non_full.png');
            game.load.image('full', 'img/full.png');
            game.load.image('sound_on', 'img/sound_on.png');
            game.load.image('sound_off', 'img/sound_off.png');
        }
        if(isMobile) {
         game.load.image('spin', 'img/spin.png');
         game.load.image('spin_p', 'img/spin_p.png');
         game.load.image('spin_d', 'img/spin_d.png');
         game.load.image('bet1', 'img/bet1.png');
         game.load.image('bet1_p', 'img/bet1_p.png');
         game.load.image('home', 'img/home.png');
         game.load.image('home_p', 'img/home_p.png');
         game.load.image('dollar', 'img/dollar.png');
         game.load.image('gear', 'img/gear.png');
         game.load.image('double', 'img/double.png');
     }
     game.load.image('shirt_cards', 'img/shirt_cards.png');
     game.load.image('dealer', 'img/dealer.png');
     game.load.image('pick_card', 'img/pick_game2.png');
     game.load.image('card_15', 'img/cards/joker.png');
     game.load.image('card_2b', 'img/cards/2b.png');
     game.load.image('card_5', 'img/cards/5b.png');
     game.load.image('card_2c', 'img/cards/2c.png');
     game.load.image('card_7', 'img/cards/7c.png');
     game.load.image('card_2p', 'img/cards/2p.png');
     game.load.image('card_12', 'img/cards/12p.png');
     game.load.image('card_2t', 'img/cards/2t.png');
     game.load.image('card_13', 'img/cards/13t.png');

     game.load.image('game3.bottom_1', 'img/game3_bottom_1.png');
     game.load.image('game3.men', 'img/game3_men.png');
     game.load.image('game3.mountain_1', 'img/mountain_1.png');
     game.load.image('game3.mountain_2', 'img/mountain_2.png');
     game.load.image('game3.sun', 'img/sun.png');
     game.load.image('game3.blue_screen_1', 'img/blue_screen_1.png');
     game.load.image('game3.blue_screen_2', 'img/blue_screen_2.png');

     game.load.image('game3.void_rope_1', 'img/void_rope_1.png');     
     for (var i = 1; i <= 5; ++i) { 
        game.load.image('game3.rope' + i, 'img/rope' + i + '.png');       
        game.load.image('game3.stone' + i, 'img/stone' + i + '.png');       
        game.load.image('game3.hook' + i, 'img/hook' + i + '.png');       
        game.load.image('game3.hookfinal_' + i, 'img/hookfinal_' + i + '.png');       
    }



    game.load.image('game3.game3_top', 'img/game3_top.png');   
    game.load.audio('stop', 'sounds/stop.wav');
    game.load.audio('rotate', 'sounds/rotate.wav');
    game.load.audio('tada', 'sounds/tada.wav');
    game.load.audio('takeWin', 'sounds/takeWin.mp3');
    game.load.audio('pickCard', 'sounds/pickCard.mp3');
    game.load.audio('cardWin', 'sounds/cardWin.mp3');
    game.load.audio('hit', 'sounds/hit.mp3');
    game.load.audio('winGame3', 'sounds/winGame3.mp3');
    game.load.audio('dynamite', 'sounds/dynamite.mp3');
    game.load.audio('boom', 'sounds/boom.mp3');
    game.load.audio('rowWin', 'sounds/rowWin.mp3');
    game.load.audio('codeDoor', 'sounds/codeDoor.mp3');
    game.load.audio('page', 'sounds/page.mp3');
    game.load.audio('rope_1', 'sounds/rope_1.mp3');
    game.load.audio('rope_2', 'sounds/rope_2.mp3');
    game.load.audio('game3.win_2', 'sounds/game3_win_1.mp3');
    game.load.audio('game3.win_1', 'sounds/game3_win_2.mp3');
    for (var i = 1; i <= 9; ++i) {
        game.load.image('line_' + i, 'img/lines/select/line' + i + '.png');
        game.load.image('linefull_' + i, 'img/lines/win/linefull' + i + '.png');
        game.load.image('win_' + i, 'img/win_' + i + '.png');

        if (i % 2 != 0) {
            game.load.audio('line' + i, 'sounds/line' + i + '.wav');
            if(!isMobile){
                game.load.image('btnline' + i, 'img/btns/btn' + i + '.png');
                game.load.image('btnline_p' + i, 'img/btns/btn' + i + '_p.png');
                game.load.image('btnline_d' + i, 'img/btns/btn' + i + '_d.png');
            }
        }
    }

    game.load.spritesheet('fire', 'img/fire.png', 64, 96, 40);
    game.load.spritesheet('tent', 'img/tent_anim.png', 144, 80, 14);
    game.load.spritesheet('rope_bar_win', 'img/rope_bar_anim.png', 96, 112, 10);
    game.load.spritesheet('cup', 'img/cup_anim.png', 112, 112, 5);
    game.load.spritesheet('glasses', 'img/glasses_anim.png', 112, 96, 7);
    game.load.spritesheet('head_left_right', 'img/head_left_right_anim.png', 112, 112, 7);
    game.load.spritesheet('win', 'img/win.png', 112, 96, 6);
    game.load.spritesheet('game2.win_lose', 'img/game2_win_lose.png', 112, 96, 5);
    game.load.spritesheet('game3.men_climb', 'img/game3_men_climb.png', 80, 128, 40);
    game.load.spritesheet('game3.top_men_1', 'img/game3_top_men_1.png', 112, 112, 29);
    game.load.spritesheet('game3.top_men_2', 'img/game3_top_men_2.png', 112, 112, 8);
    game.load.spritesheet('game3.flag_1', 'img/game3_flag_1.png', 144, 192, 6);
    // game.load.spritesheet('game3.flag_2', 'img/game3_flag_2.png', 144, 160, 57);
    game.load.spritesheet('game3.flag_2_1', 'img/game3_flag_2_1.png', 144, 160, 28);
    game.load.spritesheet('game3.flag_2_2', 'img/game3_flag_2_2.png', 144, 160, 28);
    game.load.spritesheet('game3.eyes', 'img/game3_eyes.png', 32, 16, 5);
    game.load.spritesheet('game3.hit', 'img/game3_hit.png', 80, 80, 34);
    game.load.spritesheet('game3.fall_men', 'img/game3_fall_men.png', 112, 128, 28);

    game.load.audio('rotate', 'sounds/rotate.wav');

},
create:function(){
        game.state.start('game1'); //переключение на 1 игру
        if(isMobile)
            document.getElementById('preloader').style.display = 'none';
    }

};
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру

game.state.start('gamePreload'); //начало игры с локации загрузчика

$(document).ready(function(){
    var HeightNow = document.documentElement.clientHeight; 
    console.log(HeightNow);
    function checkWidth() {

        var realHeight = document.documentElement.clientHeight; 
            if (HeightNow > realHeight){    // Если текущий размер  меньше размера в полном экране, то выставляем заглушку
                console.log('выход из полноэкранного режима');
                full.loadTexture('non_full');
                fullStatus = false;
            }
            HeightNow = realHeight; 
        };
        $( window ).resize(function() {
            var realHeight = document.documentElement.clientHeight; 
        });
        window.addEventListener('resize', checkWidth); 
        checkWidth();
    });