window.game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example');

isMobile = true;

var mobileX = 94;
var mobileY = 22;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
var gamename = 'rockclimberrus'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0]+0, linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0]+0, linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0]+0, linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0]+0, linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0]+0, linePosition[4][1]+13, lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0]+0, linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0]+0, linePosition[6][1]+56, lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0]+0, linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0]+0, linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
var linesArray = [];
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 11:
                line1.visible = false;
                break;
                case 12:
                line2.visible = false;
                break;
                case 13:
                line3.visible = false;
                break;
                case 14:
                line4.visible = false;
                break;
                case 15:
                line5.visible = false;
                break;
                case 16:
                line6.visible = false;
                break;
                case 17:
                line7.visible = false;
                break;
                case 18:
                line8.visible = false;
                break;
                case 19:
                line9.visible = false;
                break;
            }
        });
    }
}

function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = true;
                break;
                case 2:
                linefull2.visible = true;
                break;
                case 3:
                linefull3.visible = true;
                break;
                case 4:
                linefull4.visible = true;
                break;
                case 5:
                linefull5.visible = true;
                break;
                case 6:
                linefull6.visible = true;
                break;
                case 7:
                linefull7.visible = true;
                break;
                case 8:
                linefull8.visible = true;
                break;
                case 9:
                linefull9.visible = true;
                break;
                case 11:
                line1.visible = true;
                break;
                case 12:
                line2.visible = true;
                break;
                case 13:
                line3.visible = true;
                break;
                case 14:
                line4.visible = true;
                break;
                case 15:
                line5.visible = true;
                break;
                case 16:
                line6.visible = true;
                break;
                case 17:
                line7.visible = true;
                break;
                case 18:
                line8.visible = true;
                break;
                case 19:
                line9.visible = true;
                break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = false;
                break;
                case 2:
                number2.visible = false;
                break;
                case 3:
                number3.visible = false;
                break;
                case 4:
                number4.visible = false;
                break;
                case 5:
                number5.visible = false;
                break;
                case 6:
                number6.visible = false;
                break;
                case 7:
                number7.visible = false;
                break;
                case 8:
                number8.visible = false;
                break;
                case 9:
                number9.visible = false;
                break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = true;
                break;
                case 2:
                number2.visible = true;
                break;
                case 3:
                number3.visible = true;
                break;
                case 4:
                number4.visible = true;
                break;
                case 5:
                number5.visible = true;
                break;
                case 6:
                number6.visible = true;
                break;
                case 7:
                number7.visible = true;
                break;
                case 8:
                number8.visible = true;
                break;
                case 9:
                number9.visible = true;
                break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки и логика для игры с последовательным выбором
var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
function addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm){
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[0](ropeValues, ropeStep, checkHelm);

        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }


        ropesAnim[1](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine5.events.onInputDown.add(function(){

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[2](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[3](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine9.events.onInputDown.add(function(){

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[4](ropeValues, ropeStep);
        setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
}

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
function addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1) {
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });
    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        selectionsAmin[0](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }

    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        selectionsAmin[1](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }
    });
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
var buttonsArray = [];
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');

    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        if(typeof finalValues != "undefined") {
            totalWinR = finalValues[15];
            linesR = finalValues[16];
            betlineR = finalValues[17];

            checkSpinResult(totalWinR);

            slotRotation(game, finalValues);
        }




    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var bonusWin = game.add.audio('bonusWin');
        bonusWin.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 5500);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                game.cup.visible = false;
                game.glasses.visible = false;
                game.head_left_right.visible = false;
                game.win.visible = true;
                game.cap.visible = true;
                game.winAnimation.play();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 562-mobileX, 429-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 562, 429);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);
    //alert('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            // game2:
            // data = 'result=ok&state=0&info=|3|5|7|5|2|1|1|6|0|5|1|4|6|1|3|3|1|1&wl=|3|0|0|0|0&balance=5511&dcard=19&swCnt=|3|0|0|0|0|0|0|0|0&swCntId=|1|0|0|0|0|0|0|0|0&rope=|&extralife=0&jackpots=2374.58|4674.58|6674.58';
            // game3:
            // data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|55|0|0|0|0|0|extralife=45&jackpots=1822.16|4122.16|6122.16';
            // alert(data);
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
       font: '24px "TeX Gyre Schola Bold"',
       fill: '#fff567',
       stroke: '#000000',
       strokeThickness: 3,
   });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=init',
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            dataString = data;

            initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            startDataArray = dataString.split('&');

            if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }

                    /*game1();
                     game2();
                     game3();
                     game4();*/
                 });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Arial Black"',
            fill: '#ffff00',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Arial Black"',
            fill: '#ffff00',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('topScoreGame1'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                        fill: '#fff567',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();
    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            game.cup.visible = false;
            game.glasses.visible = false;
            game.head_left_right.visible = false;
            game.win.visible = true;
            game.cap.visible = true;
            game.winAnimation.play();
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            game.cup.visible = false;
            game.glasses.visible = false;
            game.head_left_right.visible = false;
            game.win.visible = true;
            game.cap.visible = true;
            game.winAnimation.play();
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y, fontsize) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: fontsize+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: fontsize+'px "TeX Gyre Schola Bold"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
        font: scorePosions[3][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
    if(!isMobile) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    } else {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout("card1.loadTexture('card_'+dcard); openCard.play(); enableInputCards(); unlockDisplay();", 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {
    openCard.play();
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_'+valuesOfAllCards[i]);
    });
}

function hideAllCards(cardArray) {
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}





//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var loseTryInGame3 = false;
function showWinGame3(x, y, win, stepTotalWinR) {
    var text = game.add.text(x,y, win, { font: '18px \"Times New Roman\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});

    // var timeInterval = 450;
    // var textCounter = setInterval(function () {
    //     text.position.y -= 3;
    // }, 10);

    // setTimeout(function() {
    //     clearInterval(textCounter);
    // }, timeInterval);
    number_win = game.add.audio('number_win');
    number_win.play();
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
        fill: '#fff567',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');unlockDisplay();", 2500);
    }, timeInterval);
}

//функции для игры с выбором из двух вариантов

function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
            fill: '#fff567',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
    }, timeInterval);
}





//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                fill: '#fff567',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
        betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });
    gear.events.onInputUp.add(function(){
        // hideButtons([[payTable, 'payTable'], [betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
        hideButtons([]);
        pagePaytables[1].visible = true;
        prev_page.visible = true;
        exit_btn.visible = true;
        next_page.visible = true;
        border_btns.visible = true;
        settingsMode = true;
        currentPage = 1;
        betline1Score.visible = false;
        betline2Score.visible = false;
        betScore.visible = false;
        linesScore.visible = false;
        balanceScore.visible = false;
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}


var pagePaytables =[];
var settingsMode = false;
var currentPage = null;

function addPaytable(pageCount, pageCoord, btnCoord){
    pageSound = game.add.audio('page');
    for (var i = 1; i <= pageCount; ++i) {
        pagePaytable = game.add.sprite(pageCoord[i-1][0], pageCoord[i-1][1], 'pagePaytable_' + i);
        pagePaytable.visible = false;
        pagePaytables[i] = pagePaytable;
    }

    prev_page = game.add.sprite(btnCoord[0][0], btnCoord[0][1], 'prev_page');
    prev_page.visible = false;
    prev_page.inputEnabled = true;
    prev_page.input.useHandCursor = true;
    prev_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == 1)
                currentPage = pageCount;
            else{
                pagePaytables[currentPage].visible = false;
                currentPage -=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
    exit_btn = game.add.sprite(btnCoord[1][0], btnCoord[1][1], 'exit_btn');
    exit_btn.visible = false;
    exit_btn.inputEnabled = true;
    exit_btn.input.useHandCursor = true;
    exit_btn.events.onInputUp.add(function(){
        pageSound.play();
        for (var i = 1; i <= pageCount; ++i) {
            pagePaytables[i].visible = false;
        }            
        prev_page.visible = false;
        exit_btn.visible = false;
        next_page.visible = false;
        border_btns.visible = false;
        settingsMode = false;
        betline1Score.visible = true;
        betline2Score.visible = true;
        betScore.visible = true;
        linesScore.visible = true;
        balanceScore.visible = true;
        showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
        // showButtons([[betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [payTable, 'payTable'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
    });
    next_page = game.add.sprite(btnCoord[2][0], btnCoord[2][1], 'next_page');
    next_page.visible = false;
    next_page.inputEnabled = true;
    next_page.input.useHandCursor = true;
    next_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == pageCount){
                pagePaytables[currentPage].visible = false;
                currentPage = 1;
            } else if (currentPage == 1){
                currentPage +=1;
            } else {                    
                pagePaytables[currentPage].visible = false;
                currentPage +=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
    border_btns = game.add.sprite(300-mobileX, 454-mobileY, 'border_btns');
    border_btns.visible = false;
};

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
var buttonsGame3Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок

function addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm) {
    //логика для событий нажатия на изображения
    buttonsGame3Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){

            button.inputEnabled = false;

            if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
            }

            ropesAnim[i](ropeValues,ropeStep, checkHelm);

            //setTimeout(showWin, 500, 195+440-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);
            setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[i][0],winRopeNumberPosition[i][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

            ropeStep += 1;
        });
    });
}

//функции для игры с выбором из двух вариантов
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
var buttonsGame4Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок

//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}


//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

requestInit();

(function () {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки

        var playSound = game.add.audio('play');

        slotPosition = [[142-mobileX, 87-mobileY], [142-mobileX, 199-mobileY], [142-mobileX, 311-mobileY], [254-mobileX, 87-mobileY], [254-mobileX, 199-mobileY], [254-mobileX, 311-mobileY], [365-mobileX, 87-mobileY], [365-mobileX, 199-mobileY], [365-mobileX, 311-mobileY], [477-mobileX, 87-mobileY], [477-mobileX, 199-mobileY], [477-mobileX, 311-mobileY], [589-mobileX, 87-mobileY], [589-mobileX, 199-mobileY], [589-mobileX, 311-mobileY]];
        addSlots(game, slotPosition);
        // изображения

        game.add.sprite(94-mobileX,22-mobileY, 'game.background1');
        topBarImage = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame1');

        addTableTitle(game, 'play1To',540-mobileX,422-mobileY);


        var linePosition = [[134-mobileX,243-mobileY], [134-mobileX,110-mobileY], [134-mobileX,383-mobileY], [134-mobileX,158-mobileY], [134-mobileX,132-mobileY], [134-mobileX,132-mobileY], [134-mobileX,261-mobileY], [134-mobileX,270-mobileY], [134-mobileX,159-mobileY]];
        var numberPosition = [[94-mobileX,230-mobileY], [94-mobileX,86-mobileY], [94-mobileX,374-mobileY], [94-mobileX,150-mobileY], [94-mobileX,310-mobileY], [94-mobileX,118-mobileY], [94-mobileX,342-mobileY], [94-mobileX,262-mobileY], [94-mobileX,198-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        showLines(linesArray);
        showNumbers(numberArray);

        // кнопки
        addButtonsGame1Mobile(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [99-mobileX, 59-mobileY, 16], [707-mobileX, 59-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        game.fire = game.add.sprite(318-mobileX, 406-mobileY, 'fire');
        game.fireAnimation = game.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true).play();         
        game.tent = game.add.sprite(94-mobileX, 422-mobileY, 'tent');
        game.tentAnimation = game.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true).play();   
        game.cup = game.add.sprite(414-mobileX, 390-mobileY, 'cup');
        game.cupAnimation = game.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
        game.cup.visible = false;  
        game.glasses = game.add.sprite(414-mobileX, 406-mobileY, 'glasses');
        game.glassesAnimation = game.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false).play();
        game.head_left_right = game.add.sprite(414-mobileX, 390-mobileY, 'head_left_right');
        game.cap = game.add.sprite(446-mobileX, 391-mobileY, 'cap');
        game.head_left_rightAnimation = game.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
        game.head_left_right.visible = false;
        game.win = game.add.sprite(414-mobileX, 406-mobileY, 'win');
        game.winAnimation = game.win.animations.add('win', [0,1,2,3,4,5,1,0], 6, false);
        game.win.visible = false;  
        game.glassesAnimation.onComplete.add(function(){
            game.glasses.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        game.head_left_rightAnimation.onComplete.add(function(){
            game.head_left_right.visible = false;
            game.glasses.visible = true;
            game.glassesAnimation.play();
            game.cap.visible = true;
        });
        game.cupAnimation.onComplete.add(function(){
            game.cup.visible = false;
            game.head_left_right.visible = true;
            game.head_left_rightAnimation.play();
            game.cap.visible = false;
        });
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        var pageCount = 5;
        // кнопки
        var pageCoord = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];   
        var btnCoord = [[95 - mobileX, 454 - mobileY], [304 - mobileX, 454 - mobileY], [527 - mobileX, 454 - mobileY]];   
        addPaytable(pageCount, pageCoord, btnCoord);
    };



    game1.update = function () {

    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================


(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        background1 = game.add.sprite(94-mobileX,22-mobileY, 'game.background1');
        var cardPosition = [[124-mobileX,151-mobileY], [254-mobileX,151-mobileY], [366-mobileX,151-mobileY], [478-mobileX,151-mobileY], [590-mobileX,151-mobileY]];
        addCards(game, cardPosition);
        //изображения
        // topBarImage2 = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame3');
        backgroundGame3 = game.add.sprite(94-mobileX,22-mobileY, 'game.backgroundGame2');

        // addTableTitle(game, 'winTitleGame2', 334-mobileX,54-mobileY);
        hideTableTitle();



        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [463-mobileX, 95-mobileY, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты

        openDCard(dcard);

        //анимация

        game.fire = game.add.sprite(318-mobileX, 406-mobileY, 'fire');
        game.fireAnimation = game.fire.animations.add('fire', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true).play();         
        game.tent = game.add.sprite(94-mobileX, 422-mobileY, 'tent');
        game.tentAnimation = game.tent.animations.add('tent', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, true).play();   
        game.cup = game.add.sprite(414-mobileX, 390-mobileY, 'cup');
        game.cupAnimation = game.cup.animations.add('cup', [0,1,2,3,4,0], 6, false);
        game.cup.visible = false;  
        game.glasses = game.add.sprite(414-mobileX, 406-mobileY, 'glasses');
        game.glassesAnimation = game.glasses.animations.add('glasses', [0,1,2,3,4,5,6,2,1,0], 6, false).play();
        game.head_left_right = game.add.sprite(414-mobileX, 390-mobileY, 'head_left_right');
        game.cap = game.add.sprite(446-mobileX, 391-mobileY, 'cap');
        game.head_left_rightAnimation = game.head_left_right.animations.add('head_left_right', [0,1,2,0,3,4,5,6], 6, false);
        game.head_left_right.visible = false;
        game.win = game.add.sprite(414-mobileX, 406-mobileY, 'win');
        game.winAnimation = game.win.animations.add('win', [0,1,2,3,4,5,1,0], 6, false);
        game.win.visible = false;  
        game.glassesAnimation.onComplete.add(function(){
            game.glasses.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        game.head_left_rightAnimation.onComplete.add(function(){
            game.head_left_right.visible = false;
            game.glasses.visible = true;
            game.glassesAnimation.play();
            game.cap.visible = true;
        });
        game.cupAnimation.onComplete.add(function(){
            game.cup.visible = false;
            game.head_left_right.visible = true;
            game.head_left_rightAnimation.play();
            game.cap.visible = false;
        });
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.cup.visible = true;
            game.cupAnimation.play();
            game.cap.visible = false;
        });
        fontsize = 16;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 565-mobileX, 431-mobileY, fontsize);

        // кнопки
        addButtonsGame2Mobile(game);
    };

    game2.update = function () {};

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================


(function () {

    var game3 = {   
       rope_arr : [],
       stone_arr : [],
       hook_arr : [],
       blue_screen_arr : [],
       hookfinal_arr : [],
       bottom_bg_arr : [],
       void_rope : [],
       eyes_arr : [],
       selectedRope : null,
       climb : false,
       floor: 1
   };

   game3.preload = function () {};

   game3.create = function () {
       var ropeStep = 0;
       checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            //звуки

            winSound = game.add.audio('winGame3');
            hit_game3 = game.add.audio('hit_game3');
            game3_1Sound = game.add.audio('game3_1');
            game3_2Sound = game.add.audio('game3_2');
            game3_loseSound = game.add.audio('game3_lose');
            game3_win_2Sound = game.add.audio('game3_win_2');
            rope_1Sound = game.add.audio('rope_2');
            rope_2Sound = game.add.audio('rope_1');
            rope_3Sound = game.add.audio('rope_2');
            rope_4Sound = game.add.audio('rope_2');
            rope_5Sound = game.add.audio('rope_2');
            rope_6Sound = game.add.audio('rope_2');
            rope_7Sound = game.add.audio('rope_2');
            win_1Sound = game.add.audio('game3.win_1');
            win_2Sound = game.add.audio('game3.win_2');
            winSound = game.add.audio('winGame3');
            //изображения
            rope1 = game.add.sprite(94-mobileX+(1-1)*128, -522-mobileY, 'game3.rope1');
            rope1.inputEnabled = true;
            rope1.input.useHandCursor = true;

            rope3 = game.add.sprite(94-mobileX+(2-1)*128, -522-mobileY, 'game3.rope2');
            rope3.inputEnabled = true;
            rope3.input.useHandCursor = true;

            rope5 = game.add.sprite(94-mobileX+(3-1)*128, -522-mobileY, 'game3.rope3');
            rope5.inputEnabled = true;
            rope5.input.useHandCursor = true;

            rope7 = game.add.sprite(94-mobileX+(4-1)*128, -522-mobileY, 'game3.rope4');
            rope7.inputEnabled = true;
            rope7.input.useHandCursor = true;

            rope9 = game.add.sprite(94-mobileX+(5-1)*128, -522-mobileY, 'game3.rope5');
            rope9.inputEnabled = true;
            rope9.input.useHandCursor = true;

            rope1.events.onInputDown.add(function(){
                takeBananaRope(94-mobileX,312-mobileY,1);
                game3.selectedRope = 1;
                rope1.inputEnabled = false;
                rope1.input.useHandCursor = false;
            });
            rope3.events.onInputDown.add(function(){
                takeBananaRope(222-mobileX,312-mobileY,2);
                game3.selectedRope = 2;
                rope3.inputEnabled = false;
                rope3.input.useHandCursor = false;
            });
            rope5.events.onInputDown.add(function(){
                takeBananaRope(350-mobileX,312-mobileY,3);
                game3.selectedRope = 3;
                rope5.inputEnabled = false;
                rope5.input.useHandCursor = false;
            });
            rope7.events.onInputDown.add(function(){
                takeBananaRope(478-mobileX,312-mobileY,4);
                game3.selectedRope = 4;
                rope7.inputEnabled = false;
                rope7.input.useHandCursor = false;
            });
            rope9.events.onInputDown.add(function(){
                takeBananaRope(606-mobileX,312-mobileY,5);
                game3.selectedRope = 5;
                rope9.inputEnabled = false;
                rope9.input.useHandCursor = false;
            });
            background = game.add.sprite(94-mobileX,22-mobileY, 'topScoreGame4');

            game3.mountain_1= game.add.sprite(94-mobileX,-3978-1152-544-mobileY, 'game3.mountain_1');
            for (var j = 1; j <= 5; ++j) {
                game3.bottom_bg_arr[j]= game.add.sprite(94-mobileX,374-mobileY-(j-1)*1152, 'game3.bottom_1');
            };   

            for (var j = 1; j <= 5; ++j) {
                for (var i = 1; i <= 5; ++i) {
                    ji = j*10 + i;
                    game3.rope_arr[ji] = game.add.sprite(94-mobileX+(i-1)*128,-522-mobileY-(j-1)*1152, 'game3.rope'+i);
                    if(j>1){
                        game3.stone_arr[ji] = game.add.sprite(94-mobileX+(i-1)*128,-522-mobileY-(j-1)*1152, 'game3.stone'+i);
                        game3.stone_arr[ji].visible = false;
                    }
                    if (j<=4){
                        game3.hook_arr[ji] = game.add.sprite(126-mobileX+(i-1)*128,-730-mobileY-(j-1)*1152, 'game3.hook'+i);
                    }
                    game3.void_rope[ji] = game.add.sprite(94-mobileX+(i-1)*128,-522-mobileY-(j-1)*1152, 'game3.void_rope_1'); 
                    game3.void_rope[ji].visible = false;
                    game3.eyes_arr[ji] = game.add.sprite(142-mobileX+(i-1)*128,-473-mobileY-(j-1)*1152, 'game3.eyes');
                    game3.eyes_arr[ji].animations.add('game3.eyes', [0,1,2,3,4], 6, true).play();
                    game3.eyes_arr[ji].visible = false;
                }
                game3.hookfinal_arr[j] = game.add.sprite(126-mobileX+(j-1)*128,-3978-1152-160-mobileY, 'game3.hookfinal_'+j);
            };

            game3.hit_1 = game.add.sprite(110-mobileX,-506-mobileY, 'game3.hit');
            game3.hit_1Animation = game3.hit_1.animations.add('game3.hit', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 15, false);
            game3.hit_1.visible = false;   
            game3.hit_2 = game.add.sprite(110-mobileX,-506-mobileY, 'game3.hit');
            game3.hit_2Animation = game3.hit_2.animations.add('game3.hit', [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33], 15, false);
            game3.hit_2.visible = false;

            game3.fall_1_men = game.add.sprite(110-mobileX,-506+762-mobileY, 'game3.fall_men');
            game3.fall_1_menAnimation = game3.fall_1_men.animations.add('game3.fall_men', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 10, false);
            game3.fall_1_men.visible = false;  
            game3.fall_2_men = game.add.sprite(110-mobileX,-506+762-mobileY, 'game3.fall_men');
            game3.fall_2_menAnimation = game3.fall_2_men.animations.add('game3.fall_men', [14,15,16,17], 10, true);
            game3.fall_2_men.visible = false;  
            game3.fall_3_men = game.add.sprite(110-mobileX,-506+762-mobileY, 'game3.fall_men');
            game3.fall_3_menAnimation = game3.fall_3_men.animations.add('game3.fall_men', [18,19,20,21,22,23,24,25,26,27], 10, false);
            game3.fall_3_men.visible = false;

            game3.men = game.add.sprite(94-mobileX,312-mobileY, 'game3.men');
            game3.men_climb = game.add.sprite(112-mobileX, 312-mobileY, 'game3.men_climb');
            game3.men_climbAnimation = game3.men_climb.animations.add('men_climb', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39], 10, true);
            game3.men_climb.visible = false;    
            game3.top_men_1 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_1Animation = game3.top_men_1.animations.add('top_men_1', [0,1,2], 10, false);
            game3.top_men_1.visible = false;  
            game3.top_men_2 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_2Animation = game3.top_men_2.animations.add('top_men_2', [3,4,5,6], 10, false);
            game3.top_men_2.visible = false;  
            game3.top_men_3 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_3Animation = game3.top_men_3.animations.add('top_men_3', [7,8,9,10,11], 10, false);
            game3.top_men_3.visible = false;  
            game3.top_men_4 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_4Animation = game3.top_men_4.animations.add('top_men_4', [12,13,14], 10, false);
            game3.top_men_4.visible = false;  
            game3.top_men_5 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_5Animation = game3.top_men_5.animations.add('top_men_5', [15,16,17,18], 10, false);
            game3.top_men_5.visible = false;  
            game3.top_men_6 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_1');
            game3.top_men_6Animation = game3.top_men_6.animations.add('top_men_6', [19,20,21,22,23,24,25,26,27,28], 10, false);
            game3.top_men_6.visible = false;    
            game3.top_men_7 = game.add.sprite(94-mobileX, 312-mobileY, 'game3.top_men_2');
            game3.top_men_7Animation = game3.top_men_7.animations.add('top_men_7', [0,1,2,3,4,5,6,7], 10, false);
            game3.top_men_7.visible = false;  

            game3.mountain_2= game.add.sprite(94-mobileX,56-mobileY, 'game3.mountain_2');
            game3.mountain_2.visible = false;
            game3.sun= game.add.sprite(481-mobileX,154-mobileY, 'game3.sun');
            game3.sun.visible = false;
            game3.flag_1= game.add.sprite(334-mobileX,89-mobileY, 'game3.flag_1');
            game3.flag_1Animation = game3.flag_1.animations.add('flag_1', [0,1,2,3,4,5], 10, false);
            game3.flag_1.visible = false;   
            game3.flag_2_1= game.add.sprite(334-mobileX,121-mobileY, 'game3.flag_2_1');
            game3.flag_2_1Animation = game3.flag_2_1.animations.add('flag_2_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
            game3.flag_2_1.visible = false;   
            game3.flag_2_2= game.add.sprite(334-mobileX,121-mobileY, 'game3.flag_2_2');
            game3.flag_2_2Animation = game3.flag_2_2.animations.add('flag_2_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27], 10, false);
            game3.flag_2_2.visible = false; 
            game3.flag_2_1Animation.onComplete.add(function(){
                game3.flag_2_2Animation.play();
                game3.flag_2_1.visible = false;
                game3.flag_2_2.visible = true;
            });
            game3.bg_top = game.add.sprite(94-mobileX,22-mobileY, 'game3.game3_top');
            game3.blue_screen_arr[1] = game.add.sprite(94-mobileX,54-mobileY, 'game3.blue_screen_1');
            game3.blue_screen_arr[1].visible = false;
            for (var i = 2; i <= 5; ++i){
                game3.blue_screen_arr[i] = game.add.sprite(94+121+(i-2)*128-mobileX,54-mobileY, 'game3.blue_screen_2');
                game3.blue_screen_arr[i].visible = false;
            }


            win_1Sound.onStop.add(function(){
                game3.mountain_2.visible = true; 
                game3.sun.visible = true; 
                game3.flag_1Animation.play();
                game3.flag_1.visible = true; 
                win_2Sound.play();
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "TeX Gyre Schola Bold"',
                    fill: '#fff567',
                    stroke: '#000000',
                    strokeThickness: 3,
                });
                var textt = game.add.text(501+50+40-mobileX,174+20+5-mobileY, ropeValues[ropeStep], { font: '28px \"TeX Gyre Schola Bold\"', fill: '#f8f221', stroke: '#000000', strokeThickness: 3});
                updateBalanceGame3(game, scorePosions, balanceR);
            });
            rope_1Sound.onStop.add(function(){
                if (game3.climb)
                    rope_2Sound.play();
            });
            rope_2Sound.onStop.add(function(){
                if (game3.climb)
                    rope_3Sound.play();
            });   
            rope_3Sound.onStop.add(function(){
                if (game3.climb)
                    rope_4Sound.play();
            });  
            rope_4Sound.onStop.add(function(){
                if (game3.climb)
                    rope_5Sound.play();
            });   
            rope_5Sound.onStop.add(function(){
                if (game3.climb)
                    rope_6Sound.play();
            });  
            rope_6Sound.onStop.add(function(){
                if (game3.climb)
                    rope_7Sound.play();
            });
            rope_7Sound.onStop.add(function(){
                if (game3.climb)
                    rope_1Sound.play();
            });

            function takeBananaRope(x,y,lineNumber){
                lockDisplay();
                game3.climb = true;
                game3.men.position.x = x;
                if(ropeValues[ropeStep] == 0){
                    ji = game3.floor*10 + lineNumber;
                    game3.void_rope[ji].visible = true;
                    game3.eyes_arr[ji].visible = true;
                    game3.hook_arr[ji].visible = false;
                    movingWall(game3.hit_1);                    
                    movingWall(game3.hit_2);  
                }
                game3.floor +=1;
                movingWall(game3.mountain_1);   
                game3.men.visible = false;
                game3.men_climb.position.x = game3.men.position.x+18;
                game3.men_climbAnimation.play();
                game3.men_climb.visible = true;
                rope_1Sound.play();
                for (var j = game3.floor; j <= 5; ++j) {
                    ji = j*10 + lineNumber;
                    game3.rope_arr[ji].visible = false;
                    if (j>1)
                        game3.stone_arr[ji].visible = true;
                    if (j<=4)
                        game3.hook_arr[ji].visible = false;
                    game3.hookfinal_arr[lineNumber].visible = false;
                }
                for (var j = 1; j <= 5; ++j) {       
                    movingWall(game3.hookfinal_arr[j])         
                    movingWall(game3.bottom_bg_arr[j]);
                    for (var i = 1; i <= 5; ++i) {
                        ji = j*10 + i;
                        movingWall(game3.rope_arr[ji]);
                        movingWall(game3.void_rope[ji]);
                        movingWall(game3.eyes_arr[ji]);
                        if (j>1)
                            movingWall(game3.stone_arr[ji]);
                        if (j<=4)
                            movingWall(game3.hook_arr[ji]);
                    }
                }
            }
            function movingWall(item) {
                if(ropeValues[ropeStep] == 0){
                    game.add.tween(item).to({y:item.position.y+762}, 3000, Phaser.Easing.LINEAR, true).onComplete.add(function(){    
                        game3.climb = false;
                        rope_1Sound.stop();
                        rope_2Sound.stop();
                        rope_3Sound.stop();
                        rope_4Sound.stop();
                        rope_5Sound.stop();
                        rope_6Sound.stop();
                        rope_7Sound.stop();
                        game3.men_climbAnimation.stop();
                        game3.hit_1.position.x = game3.men_climb.position.x-2;
                        game3.hit_2.position.x = game3.men_climb.position.x-2;
                        game3.hit_1.visible = true;    
                        setTimeout(function() {
                            hit_game3.play();                        
                        }, 500);
                        game3.hit_1Animation.play();
                        game3.hit_1Animation.onComplete.add(function(){
                            game3.men_climb.visible = false;
                            game3.hit_1.visible = false;
                            game3.hit_2.visible = true;
                            game3.hit_2Animation.play();
                            game3.fall_1_men.visible = true;
                            game3.fall_1_menAnimation.play();
                            game3.fall_1_men.position.x = game3.men_climb.position.x-18;
                            game3.fall_2_men.position.x = game3.men_climb.position.x-18;
                            game3.fall_3_men.position.x = game3.men_climb.position.x-18;
                            game3.fall_1_men.position.y = game3.men_climb.position.y;
                            game3.fall_2_men.position.y = game3.men_climb.position.y;
                            game3.fall_3_men.position.y = game3.men_climb.position.y;
                            game3.fall_1_menAnimation.onComplete.add(function(){
                                game3.hit_2.visible = false;
                                game3.fall_1_men.visible = false;
                                game3.fall_2_men.visible = true;
                                game3.fall_2_menAnimation.play();
                            });
                            game.add.tween(item).to({y:item.position.y-760}, 1500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                                game3.fall_2_men.visible = false;
                                game3.fall_2_menAnimation.stop();    
                                game3.fall_3_men.visible = true;
                                game3.fall_3_menAnimation.play();
                                updateBalanceGame3(game, scorePosions, balanceR);
                            });
                        });
                    });
                } else if (game3.floor <=5){
                    game.add.tween(item).to({y:item.position.y+1024}, 4000, Phaser.Easing.LINEAR, true).onComplete.add(function(){                
                        game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
                        game3.climb = false;
                        rope_1Sound.stop();
                        rope_2Sound.stop();
                        rope_3Sound.stop();
                        rope_4Sound.stop();
                        rope_5Sound.stop();
                        rope_6Sound.stop();
                        rope_7Sound.stop();
                        game3.men_climb.visible = false;
                        game3.men_climbAnimation.stop();
                        game3.top_men_1.position.x = game3.men_climb.position.x-18;
                        game3.top_men_1.position.y = game3.men_climb.position.y;
                        game3.top_men_1.visible = true;
                        game3.top_men_1Animation.play();
                        game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                            game3.top_men_1.visible = false;
                            game3.top_men_2.position.x = game3.top_men_1.position.x;
                            game3.top_men_2.position.y = game3.top_men_1.position.y;
                            game3.top_men_2.visible = true;
                            game3.top_men_2Animation.play();
                        });
                    });
                } else {
                    game.add.tween(item).to({y:item.position.y+978}, 3900, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                      game.add.tween(item).to({y:item.position.y+128}, 500, Phaser.Easing.LINEAR, true);
                      game3.climb = false;
                      rope_1Sound.stop();
                      rope_2Sound.stop();
                      rope_3Sound.stop();
                      rope_4Sound.stop();
                      rope_5Sound.stop();
                      rope_6Sound.stop();
                      rope_7Sound.stop();
                      game3.men_climb.visible = false;
                      game3.men_climbAnimation.stop();
                      game3.top_men_1.position.x = game3.men_climb.position.x-18;
                      game3.top_men_1.position.y = game3.men_climb.position.y;
                      game3.top_men_1.visible = true;
                      game3.top_men_1Animation.play();
                      game.add.tween(game3.top_men_1).to({y:game3.top_men_1.y+80}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        game3.top_men_1.visible = false;
                        game3.top_men_2.position.x = game3.top_men_1.position.x;
                        game3.top_men_2.position.y = game3.top_men_1.position.y;
                        game3.top_men_2.visible = true;
                        game3.top_men_2Animation.play();
                    });
                  });
                }
            }
            game3.top_men_2Animation.onComplete.add(function(){
                game3.top_men_2.visible = false;            
                game3.top_men_3.position.x = game3.top_men_2.position.x;
                game3.top_men_3.position.y = game3.top_men_2.position.y;
                game3.top_men_3.visible = true;
                game3.top_men_3Animation.play();
                game.add.tween(game3.top_men_3).to({y:game3.top_men_3.y-48}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                    game3.top_men_3.visible = false;                
                    game3.top_men_4.position.x = game3.top_men_3.position.x;
                    game3.top_men_4.position.y = game3.top_men_3.position.y;
                    game3.top_men_4.visible = true;
                    game3.top_men_4Animation.play();
                    game.add.tween(game3.top_men_4).to({y:game3.top_men_4.y-8}, 300, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                        game3.top_men_4.visible = false;
                        game3.top_men_5.position.x = game3.top_men_4.position.x;
                        game3.top_men_5.position.y = game3.top_men_4.position.y;
                        game3.top_men_5.visible = true;
                        game3.top_men_5Animation.play();
                        game.add.tween(game3.top_men_5).to({y:game3.top_men_5.y-24}, 400, Phaser.Easing.LINEAR, true).onComplete.add(function(){
                            game3.top_men_5.visible = false;
                            game3.top_men_6.position.x = game3.top_men_5.position.x;
                            game3.top_men_6.position.y = game3.top_men_5.position.y;
                            game3.top_men_6.visible = true;
                            game3.top_men_6Animation.play();
                        });
                    });
                });
            });
            game3.top_men_6Animation.onComplete.add(function(){
                game3.top_men_6.visible = false;
                game3.top_men_7.position.x = game3.top_men_6.position.x;
                game3.top_men_7.visible = true;
                game3.top_men_7Animation.play();
            });
            game3.top_men_7Animation.onComplete.add(function(){
                game3.top_men_7.visible = false;
                game3.men.visible = true;
                game3.blue_screen_arr[game3.selectedRope].visible = true;
                if(ropeValues[ropeStep] != 0) { 
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }
                setTimeout(showWinGame3,1, 150-mobileX+128*(game3.selectedRope-1),52-mobileY , ropeValues[ropeStep], stepTotalWinR);
                unlockDisplay();
                ropeStep += 1;
                if (game3.floor > 5){
                    win_1Sound.play();
                } else {
                    game3.freeze = false;
                    game3.men.position.y = game3.top_men_7.position.y;
                }
            });
            game3.flag_1Animation.onComplete.add(function(){
                game3.flag_1.visible = false; 
                game3.flag_2_1Animation.play();
                game3.flag_2_1.visible = true;

            });

            //счет
            scorePosions = [[260 - mobileX, 26 - mobileY, 20], [485- mobileX, 26 - mobileY, 20], [640- mobileX, 26 - mobileY, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);
            
            //анимации и логика



            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = false; //проверка доступа ко второй бонусной игре
        };

        game3.update = function () {
        };

        game.state.add('game3', game3);

    })();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('game.background', 'img/shape1206.svg');
        game.load.image('game.background1', 'img/main_bg.png');

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/win_1.png');
        game.load.image('game.number2', 'img/win_2.png');
        game.load.image('game.number3', 'img/win_3.png');
        game.load.image('game.number4', 'img/win_4.png');
        game.load.image('game.number5', 'img/win_5.png');
        game.load.image('game.number6', 'img/win_6.png');
        game.load.image('game.number7', 'img/win_7.png');
        game.load.image('game.number8', 'img/win_8.png');
        game.load.image('game.number9', 'img/win_9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');

        game.load.image('line1', 'img/lines/select/line1.png');
        game.load.image('line2', 'img/lines/select/line2.png');
        game.load.image('line3', 'img/lines/select/line3.png');
        game.load.image('line4', 'img/lines/select/line4.png');
        game.load.image('line5', 'img/lines/select/line5.png');
        game.load.image('line6', 'img/lines/select/line6.png');
        game.load.image('line7', 'img/lines/select/line7.png');
        game.load.image('line8', 'img/lines/select/line8.png');
        game.load.image('line9', 'img/lines/select/line9.png');

        game.load.image('linefull1', 'img/lines/win/linefull1.png');
        game.load.image('linefull2', 'img/lines/win/linefull2.png');
        game.load.image('linefull3', 'img/lines/win/linefull3.png');
        game.load.image('linefull4', 'img/lines/win/linefull4.png');
        game.load.image('linefull5', 'img/lines/win/linefull5.png');
        game.load.image('linefull6', 'img/lines/win/linefull6.png');
        game.load.image('linefull7', 'img/lines/win/linefull7.png');
        game.load.image('linefull8', 'img/lines/win/linefull8.png');
        game.load.image('linefull9', 'img/lines/win/linefull9.png');

        game.load.audio('line1Sound', 'sounds/line1.wav');
        game.load.audio('line3Sound', 'sounds/line3.wav');
        game.load.audio('line5Sound', 'sounds/line5.wav');
        game.load.audio('line7Sound', 'sounds/line7.wav');
        game.load.audio('line9Sound', 'sounds/line9.wav');

        game.load.audio('sound', 'sounds/spin.mp3');
        game.load.audio('rotateSound', 'sounds/rotate.wav');
        game.load.audio('stopSound', 'sounds/stop.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('play', 'sounds/play.mp3');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('page', 'sounds/page.mp3');

        game.load.audio('soundWinLine8', 'sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sounds/winLines/sound19.mp3');

        game.load.audio('bonusWin', 'sounds/bonusWin.mp3');


        //карты
        game.load.image('card_bg', 'img/shirt_cards.png');

        game.load.image('card_39', 'img/cards/2b.png');
        game.load.image('card_40', 'img/cards/3b.png');
        game.load.image('card_41', 'img/cards/4b.png');
        game.load.image('card_42', 'img/cards/5b.png');
        game.load.image('card_43', 'img/cards/6b.png');
        game.load.image('card_44', 'img/cards/7b.png');
        game.load.image('card_45', 'img/cards/8b.png');
        game.load.image('card_46', 'img/cards/9b.png');
        game.load.image('card_47', 'img/cards/10b.png');
        game.load.image('card_48', 'img/cards/11b.png');
        game.load.image('card_49', 'img/cards/12b.png');
        game.load.image('card_50', 'img/cards/13b.png');
        game.load.image('card_51', 'img/cards/14b.png');

        game.load.image('card_26', 'img/cards/2c.png');
        game.load.image('card_27', 'img/cards/3c.png');
        game.load.image('card_28', 'img/cards/4c.png');
        game.load.image('card_29', 'img/cards/5c.png');
        game.load.image('card_30', 'img/cards/6c.png');
        game.load.image('card_31', 'img/cards/7c.png');
        game.load.image('card_32', 'img/cards/8c.png');
        game.load.image('card_33', 'img/cards/9c.png');
        game.load.image('card_34', 'img/cards/10c.png');
        game.load.image('card_35', 'img/cards/11c.png');
        game.load.image('card_36', 'img/cards/12c.png');
        game.load.image('card_37', 'img/cards/13c.png');
        game.load.image('card_38', 'img/cards/14c.png');

        game.load.image('card_0', 'img/cards/2t.png');
        game.load.image('card_1', 'img/cards/3t.png');
        game.load.image('card_2', 'img/cards/4t.png');
        game.load.image('card_3', 'img/cards/5t.png');
        game.load.image('card_4', 'img/cards/6t.png');
        game.load.image('card_5', 'img/cards/7t.png');
        game.load.image('card_6', 'img/cards/8t.png');
        game.load.image('card_7', 'img/cards/9t.png');
        game.load.image('card_8', 'img/cards/10t.png');
        game.load.image('card_9', 'img/cards/11t.png');
        game.load.image('card_10', 'img/cards/12t.png');
        game.load.image('card_11', 'img/cards/13t.png');
        game.load.image('card_12', 'img/cards/14t.png');

        game.load.image('card_13', 'img/cards/2p.png');
        game.load.image('card_14', 'img/cards/3p.png');
        game.load.image('card_15', 'img/cards/4p.png');
        game.load.image('card_16', 'img/cards/5p.png');
        game.load.image('card_17', 'img/cards/6p.png');
        game.load.image('card_18', 'img/cards/7p.png');
        game.load.image('card_19', 'img/cards/8p.png');
        game.load.image('card_20', 'img/cards/9p.png');
        game.load.image('card_21', 'img/cards/10p.png');
        game.load.image('card_22', 'img/cards/11p.png');
        game.load.image('card_23', 'img/cards/12p.png');
        game.load.image('card_24', 'img/cards/13p.png');
        game.load.image('card_25', 'img/cards/14p.png');
        game.load.image('card_52', 'img/cards/joker.png');
        game.load.image('pick', 'img/pick_game2.png');

        game.load.image('game.backgroundGame2', 'img/bg_game2.png');
        game.load.audio('openCard', 'sounds/sound31.mp3');
        game.load.audio('winCard', 'sounds/sound30.mp3');

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');
        game.load.image('border_btns', 'img/border_btns.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('pagePaytable_' + i, 'img/page_' + i + '.png');            
        }

        game.load.image('game3.bottom_1', 'img/game3_bottom_1.png');
        game.load.image('game3.men', 'img/game3_men.png');
        game.load.image('game3.mountain_1', 'img/mountain_1.png');
        game.load.image('game3.mountain_2', 'img/mountain_2.png');
        game.load.image('game3.sun', 'img/sun.png');
        game.load.image('game3.blue_screen_1', 'img/blue_screen_1.png');
        game.load.image('game3.blue_screen_2', 'img/blue_screen_2.png');

        game.load.image('game3.void_rope_1', 'img/void_rope_1.png');     
        for (var i = 1; i <= 5; ++i) { 
            game.load.image('game3.rope' + i, 'img/rope' + i + '.png');       
            game.load.image('game3.stone' + i, 'img/stone' + i + '.png');       
            game.load.image('game3.hook' + i, 'img/hook' + i + '.png');       
            game.load.image('game3.hookfinal_' + i, 'img/hookfinal_' + i + '.png');       
        }

        game.load.spritesheet('fire', 'img/fire.png', 64, 96, 40);
        game.load.spritesheet('tent', 'img/tent_anim.png', 144, 80, 14);
        game.load.spritesheet('cup', 'img/cup_anim.png', 112, 112, 5);
        game.load.spritesheet('glasses', 'img/glasses_anim.png', 112, 96, 7);
        game.load.spritesheet('head_left_right', 'img/head_left_right_anim.png', 112, 112, 7);
        game.load.spritesheet('win', 'img/win.png', 112, 96, 6);
        game.load.image('cap', 'img/cap.png');
        game.load.spritesheet('game2.win_lose', 'img/game2_win_lose.png', 112, 96, 5);
        game.load.spritesheet('game3.men_climb', 'img/game3_men_climb.png', 80, 128, 40);
        game.load.spritesheet('game3.top_men_1', 'img/game3_top_men_1.png', 112, 112, 29);
        game.load.spritesheet('game3.top_men_2', 'img/game3_top_men_2.png', 112, 112, 8);
        game.load.spritesheet('game3.flag_1', 'img/game3_flag_1.png', 144, 192, 6);
    // game.load.spritesheet('game3.flag_2', 'img/game3_flag_2.png', 144, 160, 57);
    game.load.spritesheet('game3.flag_2_1', 'img/game3_flag_2_1.png', 144, 160, 28);
    game.load.spritesheet('game3.flag_2_2', 'img/game3_flag_2_2.png', 144, 160, 28);
    game.load.spritesheet('game3.eyes', 'img/game3_eyes.png', 32, 16, 5);
    game.load.spritesheet('game3.hit', 'img/game3_hit.png', 80, 80, 34);
    game.load.spritesheet('game3.fall_men', 'img/game3_fall_men.png', 112, 128, 28);
    game.load.image('game3.game3_top', 'img/game3_top.png');  
    game.load.audio('rope_1', 'sounds/rope_1.mp3');
    game.load.audio('rope_2', 'sounds/rope_2.mp3');
    game.load.audio('game3.win_2', 'sounds/game3_win_1.mp3');
    game.load.audio('hit_game3', 'sounds/hit_game3.mp3');
    game.load.audio('game3.win_1', 'sounds/game3_win_2.mp3');
    game.load.audio('number_win', 'sounds/number_win.mp3');

    game.load.image('cell0', 'img/0.png');
    game.load.image('cell1', 'img/1.png');
    game.load.image('cell2', 'img/2.png');
    game.load.image('cell3', 'img/3.png');
    game.load.image('cell4', 'img/4.png');
    game.load.image('cell5', 'img/5.png');
    game.load.image('cell6', 'img/6.png');
    game.load.image('cell7', 'img/7.png');
    game.load.image('cell8', 'img/8.png');

    game.load.spritesheet('cellAnim', 'img/cellAnim.png', 96, 112);

    game.load.image('bonusGame', 'img/image536.png');
    game.load.image('wildSymbol', 'img/image537.png');
    game.load.image('play1To', 'img/image546.png');
    game.load.image('takeOrRisk1', 'img/image474.png');
    game.load.image('takeOrRisk2', 'img/image475.png');
    game.load.image('take', 'img/image554.png');

    game.load.image('topScoreGame1', 'img/main_bg_1.png');
    game.load.image('topScoreGame2', 'img/main_bg_2.png');
    game.load.image('topScoreGame3', 'img/main_bg_3.png');
    game.load.image('topScoreGame4', 'img/main_bg_4.png');

        // game.load.image('loseTitleGame2', 'img/lose.png');
        // game.load.image('winTitleGame2', 'img/win.png');
        // game.load.image('forwadTitleGame2', 'img/forward.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/bonus.png', 96, 112);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

