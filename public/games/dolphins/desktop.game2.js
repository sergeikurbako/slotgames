function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        // звуки
        sound22 = game.add.audio('sound22');
        sound22.loop = true;
        sound22.play();


        //изображения
        game.add.sprite(0,0, 'border');
        game.add.sprite(94,86, 'backgroundGame2');


        // кнопки
        addButtonsGame2TypeBOR(game);

        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[690, 475, 22], [690, 440, 22], [200, 473, 22], [690, 420, 22], [690, 420, 22]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        var gamblePosition = [[250,125],[580,125]];
        totalWinR = 25;
        addGamble(totalWinR, gamblePosition);

        addTitles();
        var text = 'CHOOSE RED OR BLACK OR TAKE WIN';
        changeTableTitleTypeBOR(text);


        buttonMPosition = [[165-11,260-17], [553-27,260-17]];
        addMButtons(buttonMPosition);

        //карты и масти
        cardPositionTypeBOR = [[382-19,234-20], [[415-20,179-16], [470-22,179-16], [526-25,179-16], [581-27,179-16], [638-31,179-16], [693-33,179-16]]];
        addCardsTypeBOR(cardPositionTypeBOR);

        addMs();

        //анимации
        addCardAnim(game);

        full_and_sound();
    };

    game2.update = function () {

    };

    game.state.add('game2', game2);


}