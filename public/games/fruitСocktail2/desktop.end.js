(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background', needUrlPath + '/img/shape1206.svg');
        game.load.image('game.background1', needUrlPath + '/img/main_bg.png');

        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');

        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/win_1.png');
        game.load.image('game.number2', needUrlPath + '/img/win_2.png');
        game.load.image('game.number3', needUrlPath + '/img/win_3.png');
        game.load.image('game.number4', needUrlPath + '/img/win_4.png');
        game.load.image('game.number5', needUrlPath + '/img/win_5.png');
        game.load.image('game.number6', needUrlPath + '/img/win_6.png');
        game.load.image('game.number7', needUrlPath + '/img/win_7.png');
        game.load.image('game.number8', needUrlPath + '/img/win_8.png');
        game.load.image('game.number9', needUrlPath + '/img/win_9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotate', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stop', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');

        game.load.image('line1', needUrlPath + '/img/lines/select/line1.png');
        game.load.image('line2', needUrlPath + '/img/lines/select/line2.png');
        game.load.image('line3', needUrlPath + '/img/lines/select/line3.png');
        game.load.image('line4', needUrlPath + '/img/lines/select/line4.png');
        game.load.image('line5', needUrlPath + '/img/lines/select/line5.png');
        game.load.image('line6', needUrlPath + '/img/lines/select/line6.png');
        game.load.image('line7', needUrlPath + '/img/lines/select/line7.png');
        game.load.image('line8', needUrlPath + '/img/lines/select/line8.png');
        game.load.image('line9', needUrlPath + '/img/lines/select/line9.png');

        game.load.image('linefull1', needUrlPath + '/img/lines/win/linefull1.png');
        game.load.image('linefull2', needUrlPath + '/img/lines/win/linefull2.png');
        game.load.image('linefull3', needUrlPath + '/img/lines/win/linefull3.png');
        game.load.image('linefull4', needUrlPath + '/img/lines/win/linefull4.png');
        game.load.image('linefull5', needUrlPath + '/img/lines/win/linefull5.png');
        game.load.image('linefull6', needUrlPath + '/img/lines/win/linefull6.png');
        game.load.image('linefull7', needUrlPath + '/img/lines/win/linefull7.png');
        game.load.image('linefull8', needUrlPath + '/img/lines/win/linefull8.png');
        game.load.image('linefull9', needUrlPath + '/img/lines/win/linefull9.png');

        game.load.audio('line1Sound', needUrlPath + '/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sounds/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sounds/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sounds/stop.wav');
        game.load.audio('tada', needUrlPath + '/sounds/tada.wav');
        game.load.audio('play', needUrlPath + '/sounds/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');
        game.load.audio('page', needUrlPath + '/sounds/page.mp3');
        // game.load.audio('game.winCards', needUrlPath + '/sounds/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sounds/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sounds/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sounds/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sounds/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sounds/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sounds/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sounds/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sounds/winLines/sound19.mp3');
        // game.load.spritesheet('game.monkey', needUrlPath + '/img/monkey.png', 96, 96);
        game.load.audio('gnomeWin', needUrlPath + '/sounds/rowWin.mp3');


        game.load.image('game.backgroundGame2', needUrlPath + '/img/bg_game2.png');
        game.load.audio('openCard', needUrlPath + '/sounds/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sounds/sound30.mp3');

        game.load.image('prev_page', needUrlPath + '/img/prev_page.png');
        game.load.image('exit_btn', needUrlPath + '/img/exit_btn.png');
        game.load.image('next_page', needUrlPath + '/img/next_page.png');
        for (var i = 1; i <= 6; ++i) {
            game.load.image('pagePaytable_' + i, needUrlPath + '/img/page_' + i + '.png');            
        }
        for (var i = 1; i <= 9; ++i) {
            game.load.image('unwin_' + i, needUrlPath + '/img/unwin_' + i + '.png');
        }
        //карты
        game.load.image('card_bg', needUrlPath + '/img/shirt_cards.png');

        game.load.image('card_39', needUrlPath + '/img/cards/2b.png');
        game.load.image('card_40', needUrlPath + '/img/cards/3b.png');
        game.load.image('card_41', needUrlPath + '/img/cards/4b.png');
        game.load.image('card_42', needUrlPath + '/img/cards/5b.png');
        game.load.image('card_43', needUrlPath + '/img/cards/6b.png');
        game.load.image('card_44', needUrlPath + '/img/cards/7b.png');
        game.load.image('card_45', needUrlPath + '/img/cards/8b.png');
        game.load.image('card_46', needUrlPath + '/img/cards/9b.png');
        game.load.image('card_47', needUrlPath + '/img/cards/10b.png');
        game.load.image('card_48', needUrlPath + '/img/cards/11b.png');
        game.load.image('card_49', needUrlPath + '/img/cards/12b.png');
        game.load.image('card_50', needUrlPath + '/img/cards/13b.png');
        game.load.image('card_51', needUrlPath + '/img/cards/14b.png');

        game.load.image('card_26', needUrlPath + '/img/cards/2c.png');
        game.load.image('card_27', needUrlPath + '/img/cards/3c.png');
        game.load.image('card_28', needUrlPath + '/img/cards/4c.png');
        game.load.image('card_29', needUrlPath + '/img/cards/5c.png');
        game.load.image('card_30', needUrlPath + '/img/cards/6c.png');
        game.load.image('card_31', needUrlPath + '/img/cards/7c.png');
        game.load.image('card_32', needUrlPath + '/img/cards/8c.png');
        game.load.image('card_33', needUrlPath + '/img/cards/9c.png');
        game.load.image('card_34', needUrlPath + '/img/cards/10c.png');
        game.load.image('card_35', needUrlPath + '/img/cards/11c.png');
        game.load.image('card_36', needUrlPath + '/img/cards/12c.png');
        game.load.image('card_37', needUrlPath + '/img/cards/13c.png');
        game.load.image('card_38', needUrlPath + '/img/cards/14c.png');

        game.load.image('card_0', needUrlPath + '/img/cards/2t.png');
        game.load.image('card_1', needUrlPath + '/img/cards/3t.png');
        game.load.image('card_2', needUrlPath + '/img/cards/4t.png');
        game.load.image('card_3', needUrlPath + '/img/cards/5t.png');
        game.load.image('card_4', needUrlPath + '/img/cards/6t.png');
        game.load.image('card_5', needUrlPath + '/img/cards/7t.png');
        game.load.image('card_6', needUrlPath + '/img/cards/8t.png');
        game.load.image('card_7', needUrlPath + '/img/cards/9t.png');
        game.load.image('card_8', needUrlPath + '/img/cards/10t.png');
        game.load.image('card_9', needUrlPath + '/img/cards/11t.png');
        game.load.image('card_10', needUrlPath + '/img/cards/12t.png');
        game.load.image('card_11', needUrlPath + '/img/cards/13t.png');
        game.load.image('card_12', needUrlPath + '/img/cards/14t.png');

        game.load.image('card_13', needUrlPath + '/img/cards/2p.png');
        game.load.image('card_14', needUrlPath + '/img/cards/3p.png');
        game.load.image('card_15', needUrlPath + '/img/cards/4p.png');
        game.load.image('card_16', needUrlPath + '/img/cards/5p.png');
        game.load.image('card_17', needUrlPath + '/img/cards/6p.png');
        game.load.image('card_18', needUrlPath + '/img/cards/7p.png');
        game.load.image('card_19', needUrlPath + '/img/cards/8p.png');
        game.load.image('card_20', needUrlPath + '/img/cards/9p.png');
        game.load.image('card_21', needUrlPath + '/img/cards/10p.png');
        game.load.image('card_22', needUrlPath + '/img/cards/11p.png');
        game.load.image('card_23', needUrlPath + '/img/cards/12p.png');
        game.load.image('card_24', needUrlPath + '/img/cards/13p.png');
        game.load.image('card_25', needUrlPath + '/img/cards/14p.png');
        game.load.image('card_52', needUrlPath + '/img/cards/joker.png');

        game.load.image('pick', needUrlPath + '/img/pick.png');

        game.load.spritesheet('game2.win', needUrlPath + '/img/game2_win.png', 96, 48, 8);
        game.load.image('game2.forward', needUrlPath + '/img/forward.png');
        game.load.image('game2.lose', needUrlPath + '/img/lose.png');    


        game.load.audio('winGame3', needUrlPath + '/sounds/winGame3.mp3');
        game.load.audio('game3_1', needUrlPath + '/sounds/game3_1.mp3');
        game.load.audio('game3_2', needUrlPath + '/sounds/game3_2.mp3');
        game.load.audio('game3_lose', needUrlPath + '/sounds/game3_lose.mp3');
        game.load.audio('game3_win_2', needUrlPath + '/sounds/game3_win_2.mp3');

        game.load.image('cell0', needUrlPath + '/img/0.png');
        game.load.image('cell1', needUrlPath + '/img/1.png');
        game.load.image('cell2', needUrlPath + '/img/2.png');
        game.load.image('cell3', needUrlPath + '/img/3.png');
        game.load.image('cell4', needUrlPath + '/img/4.png');
        game.load.image('cell5', needUrlPath + '/img/5.png');
        game.load.image('cell6', needUrlPath + '/img/6.png');
        game.load.image('cell7', needUrlPath + '/img/7.png');
        game.load.image('cell8', needUrlPath + '/img/8.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.png', 96, 96);

        game.load.image('bonusGame', needUrlPath + '/img/image536.png');
        game.load.image('wildSymbol', needUrlPath + '/img/image537.png');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.png');
        game.load.image('takeOrRisk2', needUrlPath + '/img/image475.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('topScoreGame1', needUrlPath + '/img/main_bg_1.png');
        game.load.image('topScoreGame2', needUrlPath + '/img/main_bg_2.png');
        game.load.image('topScoreGame3', needUrlPath + '/img/main_bg_3.png');
        game.load.image('topScoreGame4', needUrlPath + '/img/main_bg_4.png');

        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');
        game.load.image('dealer', needUrlPath + '/img/dealer.png');

        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/bonus.png', 96, 96);

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

