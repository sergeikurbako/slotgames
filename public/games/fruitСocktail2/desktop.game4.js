function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 50;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, 50);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        unlockDisplay();
        game.state.start('game1');
    }, timeInterval);
}

function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {
            //Добавление фона
            background = game.add.sprite(94,22, 'topScoreGame4');
            backgroundGame4 = game.add.sprite(95,55, 'game.backgroundGame4');
            background = game.add.sprite(0,0, 'game.background');

            // winPoint1 = game.add.audio('game.winPoint1');
            // winPoint2 = game.add.audio('game.winPoint2');
            losePoint = game.add.audio('game.losePoint');
            game4_winSound = game.add.audio('game4_win');
            game4_winSound.addMarker('win', 0.2, 5); 
            game4_loseSound = game.add.audio('game4_lose');  

            game.gnome4_1 = game.add.sprite(366, 327, 'game.gnome4_1');
            game.gnome4_1Animation = game.gnome4_1.animations.add('game.gnome4_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
            game.gnome4_1Animation.play();     
            game.gnome4_2 = game.add.sprite(366, 327, 'game.gnome4_2');
            game.gnome4_2Animation = game.gnome4_2.animations.add('game.gnome4_2', [0,1,2,3,4,5,6,7,8,9], 8, false);            
            game.gnome4_2.visible = false;
            game.gnome4_1Animation.onComplete.add(function(){
                game.gnome4_2Animation.play();
                game.gnome4_1.visible = false;
                game.gnome4_2.visible = true;
            });
            game.gnome4_2Animation.onComplete.add(function(){
                game.gnome4_1Animation.play();
                game.gnome4_1.visible = true;
                game.gnome4_2.visible = false;
            });     

            //счет
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);

            //кнопок

            var position = 1;

            var selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            var payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            var betone = game.add.sprite(490,510, 'betone_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            var betmax = game.add.sprite(535,510, 'betmax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            var startButton = game.add.sprite(597, 510, 'startButton_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = false;

            var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = false;

            var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = false;

            buttonLine3.events.onInputOver.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3_p');
                }
            });
            buttonLine3.events.onInputOut.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3');
                }
            });

            buttonLine7.events.onInputOver.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7_p');
                }
            });
            buttonLine7.events.onInputOut.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7');
                }
            });

            buttonLine3.events.onInputDown.add(function(){

                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                if(ropeValues[5] != 0){
                    game4_winSound.play('win');
                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        game.chest_win_left.visible = true; 
                        game.chest_win_left.position.x = 271;
                        game.chest_win_leftAnimation.play().onComplete.add(function(){   
                            game.gnome4_win.visible = true; 
                            game.gnome4_win.position.x = 159; 
                            game.gnome4_key.visible = false;
                            game.chest_open2.visible = true;
                            game.gnome4_winAnimation.play().onComplete.add(function(){  
                                game.gnome4_win.visible = false; 
                                game.gnome4_win2.visible = true;
                                game.gnome4_win2.position.x = 159;
                                game.gold.visible = true;
                                game.gold.position.x = 288;
                                setTimeout(function() {
                                    game.gnome4_win3.visible = true; 
                                    game.gnome4_win3.position.x = 159; 
                                    game.gnome4_win2.visible = false;
                                    game.gnome4_win3Animation.play().onComplete.add(function(){  
                                        updateBalanceGame4(game, scorePosions, balanceR);
                                    });         
                                }, 100);    
                            });         
                        });  

                    });

                } else {

                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        losePoint.play();
                        game.chest_lose_left.visible = true; 
                        game.chest_lose_leftAnimation.play().onComplete.add(function(){   
                            game.gnome4_key.visible = false;
                            game.chest_open2.visible = true;
                            game.gnome4_lose.visible = true; 
                            game.gnome4_loseAnimation.play().onComplete.add(function(){  
                                game.gnome4_lose.visible = false; 
                                game.gnome4_lose2.visible = true; 
                                game.gnome4_lose2Animation.play().onComplete.add(function(){  
                                    updateBalanceGame4(game, scorePosions, balanceR);
                                });         
                            });         
                        });  

                    });

                }

            });
            buttonLine7.events.onInputDown.add(function(){

                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                if(ropeValues[5] != 0){
                    game4_winSound.play('win');
                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_key.position.x = 415;  
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        game.chest_win_right.visible = true; 
                        game.chest_win_rightAnimation.play().onComplete.add(function(){   
                            game.gnome4_win.visible = true; 
                            game.gnome4_key.visible = false;
                            game.chest_open3.visible = true;
                            game.chest_open3.position.x = 528;
                            game.gnome4_winAnimation.play().onComplete.add(function(){  
                                game.gnome4_win.visible = false; 
                                game.gnome4_win2.visible = true;
                                game.gold.visible = true;
                                setTimeout(function() {
                                    game.gnome4_win3.visible = true; 
                                    game.gnome4_win2.visible = false;
                                    game.gnome4_win3Animation.play().onComplete.add(function(){  
                                        updateBalanceGame4(game, scorePosions, balanceR);
                                    });         
                                }, 100);    
                            });         
                        });  

                    });

                } else {

                    lockDisplay();
                    gnome4Hide();
                    game.gnome4_key.visible = true; 
                    game.gnome4_key.position.x = 415;  
                    game.gnome4_keyAnimation.play().onComplete.add(function(){
                        losePoint.play();
                        game.chest_lose_right.visible = true; 
                        game.chest_lose_rightAnimation.play().onComplete.add(function(){   
                            game.gnome4_key.visible = false;
                            game.chest_open3.visible = true;
                            game.chest_open3.position.x = 528;
                            game.gnome4_lose.visible = true; 
                            game.gnome4_lose.position.x = 415;
                            game.gnome4_loseAnimation.play().onComplete.add(function(){  
                                game.gnome4_lose.visible = false; 
                                game.gnome4_lose2.visible = true; 
                                game.gnome4_lose2.position.x = 399;
                                game.gnome4_lose2Animation.play().onComplete.add(function(){  
                                    updateBalanceGame4(game, scorePosions, balanceR);
                                });         
                            });         
                        });  

                    });

                }

            });

            game.chest_win_left = game.add.sprite(271, 199, 'game.chest_win_left');
            game.chest_win_leftAnimation = game.chest_win_left.animations.add('game.chest_win_left', [0,1,2,3,4,5], 8, false);
            game.chest_win_left.visible = false;

            game.chest_win_right = game.add.sprite(528, 199, 'game.chest_win_right');
            game.chest_win_rightAnimation = game.chest_win_right.animations.add('game.chest_win_right', [0,1,2,3,4,5], 8, false);
            game.chest_win_right.visible = false;

            game.chest_lose_left = game.add.sprite(271, 199, 'game.chest_lose_left');
            game.chest_lose_leftAnimation = game.chest_lose_left.animations.add('game.chest_lose_left', [0,1,2,3,4,5], 8, false);
            game.chest_lose_left.visible = false;   

            game.chest_lose_right = game.add.sprite(528, 199, 'game.chest_lose_right');
            game.chest_lose_rightAnimation = game.chest_lose_right.animations.add('game.chest_lose_right', [0,1,2,3,4,5], 8, false);
            game.chest_lose_right.visible = false;

            game.chest_open3 = game.add.sprite(271, 199, 'game.chest_open3');
            game.chest_open3.visible = false;

            game.chest_open2 = game.add.sprite(271, 199, 'game.chest_open2');
            game.chest_open2.visible = false;

            game.gnome4_win2 = game.add.sprite(415, 232, 'game.gnome4_win2');
            game.gnome4_win2.visible = false;

            game.gold = game.add.sprite(544, 264, 'game.gold');
            game.gold.visible = false;

            game.gnome4_key = game.add.sprite(159, 232, 'game.gnome4_key');
            game.gnome4_keyAnimation = game.gnome4_key.animations.add('game.gnome4_key', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 8, false);
            game.gnome4_key.visible = false;

            game.gnome4_win = game.add.sprite(415, 123, 'game.gnome4_win');
            game.gnome4_winAnimation = game.gnome4_win.animations.add('game.gnome4_win', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
            game.gnome4_win.visible = false;

            game.gnome4_win3 = game.add.sprite(415, 232, 'game.gnome4_win3');
            game.gnome4_win3Animation = game.gnome4_win3.animations.add('game.gnome4_win3', [0,1,2,3,4,4,4,4,4,4,4,4,4], 8, false);
            game.gnome4_win3.visible = false;

            game.gnome4_lose = game.add.sprite(159, 233, 'game.gnome4_lose');
            game.gnome4_loseAnimation = game.gnome4_lose.animations.add('game.gnome4_lose', [0,1,2,3,4], 8, false);
            game.gnome4_lose.visible = false;

            game.gnome4_lose2 = game.add.sprite(143, 233, 'game.gnome4_lose');
            game.gnome4_lose2Animation = game.gnome4_lose2.animations.add('game.gnome4_lose', [5,5,5,5,5,5], 8, false);
            game.gnome4_lose2.visible = false; 

            function gnome4Hide(){
                game.gnome4_1.visible = false;
                game.gnome4_2.visible = false;

                game.gnome4_1.animations.stop();
                game.gnome4_2.animations.stop();
            }

            // function randomPoint(){
            //     if(position == 1){
            //         x = 0;
            //     } else {
            //         x = 320;
            //     }



            //     spiderPoint = game.add.sprite(207+x,263, 'game.spiderPoint');
            //     spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
            //     spiderPoint.visible = false;

            //     var randBaba = randomNumber(1,2);

            //     switch(randBaba) {
            //         case 1:
            //         goldPoint.visible = true;
            //         goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
            //             goldPoint.animations.stop();
            //             goldPoint.visible = false;
            //         });
            //         break;

            //         default:
            //         spiderPoint.visible = true;
            //         spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
            //             spiderPoint.animations.stop();
            //             spiderPoint.visible = false;
            //         });
            //         break;
            //     }
            // }


            // questionPoint1 = game.add.sprite(207,295, 'game.questionPoint');
            // questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
            // questionPoint1.animations.getAnimation('questionPoint1').play();
            // questionPoint1.inputEnabled = true;
            // questionPoint1.input.useHandCursor = true;
            // questionPoint1.events.onInputUp.add(function(){
            //     lockDisplay();

            //     monkeyHangingHide();
            //     mankeyLeftPoint.visible = true;
            //     setTimeout("bitMonkey.play();", 500);
            //     take_or_risk_anim.visible = false;
            //     mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
            //         winPoint1.play();
            //         goldPoint = game.add.sprite(207,263, 'game.goldPoint');
            //         goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            //         goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
            //             mankeyLeftPoint.visible = false;
            //             winPoint2.play();
            //             mankeyWinPoint.visible = true;
            //             mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
            //                 mankeyWinPoint.visible = false;
            //                 monkeyHanging();

            //                 take_or_risk_anim.visible = true;

            //                 goldPoint.visible = false;

            //                 questionPoint1 = game.add.sprite(207,295, 'game.questionPoint');
            //                 questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
            //                 questionPoint1.animations.getAnimation('questionPoint1').play();
            //             });
            //         });
            //     });


            //     setTimeout("unlockDisplay();",4000);

            // });

            // questionPoint2 = game.add.sprite(528,295, 'game.questionPoint');
            // questionPoint2.animations.add('questionPoint2', [0,1,2,3,4,5,6], 10, true);
            // questionPoint2.animations.getAnimation('questionPoint2').play();
            // questionPoint2.inputEnabled = true;
            // questionPoint2.input.useHandCursor = true;
            // questionPoint2.events.onInputDown.add(function(){
            //     lockDisplay();
            //     setTimeout("bitMonkey.play();", 500);
            //     monkeyHangingHide();
            //     take_or_risk_anim.visible = false;
            //     mankeyRightPoint.visible = true;
            //     mankeyRightPoint.animations.getAnimation('mankeyRightPoint').play().onComplete.add(function(){
            //         winPoint1.play();
            //         losePoint.play();
            //         spiderPoint = game.add.sprite(528,263, 'game.spiderPoint');
            //         spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
            //         spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
            //             mankeyRightPoint.visible = false;

            //             mankeyLosePoint.visible = true;
            //             mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
            //                 updateBalanceGame4(game, scorePosions, balanceR);
            //             });
            //         });

            //     });


            //     setTimeout("unlockDisplay();",4500);
            // });

            // mankeyLeftPoint = game.add.sprite(305,74, 'game.mankeyLeftPoint');
            // mankeyLeftPoint.animations.add('mankeyLeftPoint', [0,1,2,3,4,5,6,7], 10, false);
            // mankeyLeftPoint.visible = false;

            // mankeyRightPoint = game.add.sprite(305,74, 'game.mankeyRightPoint');
            // mankeyRightPoint.animations.add('mankeyRightPoint', [0,1,2,3,4,5,6,7], 10, false);
            // mankeyRightPoint.visible = false;

            // mankeyWinPoint = game.add.sprite(305,74, 'game.mankeyWinPoint');
            // mankeyWinPoint.animations.add('mankeyWinPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            // mankeyWinPoint.visible = false;

            // mankeyLosePoint = game.add.sprite(305,74, 'game.mankeyLosePoint');
            // mankeyLosePoint.animations.add('mankeyLosePoint', [0,1,2,3,4,5,6], 10, false);
            // mankeyLosePoint.visible = false;

            full_and_sound();
        };

        game4.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game4', game4);

})();


}