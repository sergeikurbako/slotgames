function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        game.state.start('game1');
    }, timeInterval);
}

function winResultGame4(game, x,y) {
    monkeyHangingHide();
    mankeyLeftPoint.visible = true;
    setTimeout("bitMonkey.play();", 500);
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        winPoint1.play();
        goldPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.goldPoint');
        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;
            winPoint2.play();
            mankeyWinPoint.visible = true;
            mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
            });
        });
    });
}

function loseResultGame4(game, x,y) {
    setTimeout("bitMonkey.play();", 500);
    monkeyHangingHide();
    take_or_risk_anim.visible = false;
    mankeyLeftPoint.visible = true;
    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
        //winPoint1.play();
        losePoint.play();
        spiderPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.spiderPoint');
        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
        spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
            mankeyLeftPoint.visible = false;

            mankeyLosePoint.visible = true;
            mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
                updateBalanceGame4(game, scorePosions, balanceR);
            });
        });

    });
}

function getResultGame4(game, x,y) {
    if(ropeValues[5] != 0){
        lockDisplay();
        winResultGame4(game, x,y);
    } else {
        lockDisplay();
        loseResultGame4(game, x,y);
    }
}


function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //изображения
            //Добавление фона
            background1 = game.add.sprite(95-mobileX,23-mobileY, 'game.background1');
            backgroundGame4 = game.add.sprite(95-mobileX,55-mobileY, 'game.backgroundGame4');

            winPoint1 = game.add.audio('game.winPoint1');
            winPoint2 = game.add.audio('game.winPoint2');
            losePoint = game.add.audio('game.losePoint');

            take_or_risk_bg = game.add.sprite(320-mobileX,294-mobileY, 'game.take_or_risk_bg');

            bitMonkey = game.add.audio('game.bitMonkey');

            take_or_risk_anim = game.add.sprite(320-mobileX,294-mobileY, 'game.take_or_risk_anim');
            take_or_risk_anim.animations.add('take_or_risk_anim', [0,1], 2, true);
            take_or_risk_anim.animations.getAnimation('take_or_risk_anim').play();

            flashFive = game.add.sprite(400-mobileX,354-mobileY, 'game.flashFive');
            flashFive.animations.add('flashFive', [0,1,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3], 6, true);
            flashFive.animations.getAnimation('flashFive').play();


            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [485-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);


            //анимации

            mankeyHanging1 = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyHanging1');
            mankeyHanging1.animations.add('mankeyHanging1', [0,1,2,3,4,5], 5, false);
            mankeyHanging1.visible = false;

            mankeyHanging2 = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyHanging2');
            mankeyHanging2.animations.add('mankeyHanging2', [0,1,2,3], 5, false);
            mankeyHanging2.visible = false;

            mankeyHanging3 = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyHanging3');
            mankeyHanging3.animations.add('mankeyHanging3', [0,1,2,3], 5, false);
            mankeyHanging3.visible = false;

            function monkeyHanging(){

                var randBaba = randomNumber(1,7);

                monkeyHangingHide();

                switch(randBaba) {
                    case 1:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;
                    case 2:
                        mankeyHanging2.visible = true;
                        mankeyHanging2.animations.getAnimation('mankeyHanging2').play().onComplete.add(function(){
                            mankeyHanging2.animations.stop();
                            mankeyHanging2.visible = false;
                            monkeyHanging();
                        });
                        break;
                    case 3:
                        mankeyHanging3.visible = true;
                        mankeyHanging3.animations.getAnimation('mankeyHanging3').play().onComplete.add(function(){
                            mankeyHanging3.animations.stop();
                            mankeyHanging3.visible = false;
                            monkeyHanging();
                        });
                        break;

                    case 4:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;
                    case 5:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;
                    case 6:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;
                    case 7:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;

                    default:
                        mankeyHanging1.visible = true;
                        mankeyHanging1.animations.getAnimation('mankeyHanging1').play().onComplete.add(function(){
                            mankeyHanging1.animations.stop();
                            mankeyHanging1.visible = false;
                            monkeyHanging();
                        });
                        break;
                }
            }

            function monkeyHangingHide(){
                mankeyHanging1.visible = false;
                mankeyHanging2.visible = false;
                mankeyHanging3.visible = false;

                mankeyHanging1.animations.stop();
                mankeyHanging2.animations.stop();
                mankeyHanging3.animations.stop();
            }

            monkeyHanging();


            questionPoint1 = game.add.sprite(207-mobileX,295-mobileY, 'game.questionPoint');
            questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
            questionPoint1.animations.getAnimation('questionPoint1').play();
            questionPoint1.inputEnabled = true;
            questionPoint1.input.useHandCursor = true;
            questionPoint1.events.onInputUp.add(function(){
                if(ropeValues[5] != 0){
                    lockDisplay();

                    monkeyHangingHide();
                    mankeyLeftPoint.visible = true;
                    setTimeout("bitMonkey.play();", 500);
                    take_or_risk_anim.visible = false;
                    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
                        winPoint1.play();
                        goldPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.goldPoint');
                        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                        goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
                            mankeyLeftPoint.visible = false;
                            winPoint2.play();
                            mankeyWinPoint.visible = true;
                            mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
                                updateBalanceGame4(game, scorePosions, balanceR);
                                game.state.start('game1');
                                /*mankeyWinPoint.visible = false;
                                monkeyHanging();

                                take_or_risk_anim.visible = true;

                                goldPoint.visible = false;

                                questionPoint1 = game.add.sprite(207-mobileX,295-mobileY, 'game.questionPoint');
                                questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
                                questionPoint1.animations.getAnimation('questionPoint1').play();*/
                            });
                        });
                    });


                    setTimeout("unlockDisplay();",4000);
                } else {
                    lockDisplay();
                    setTimeout("bitMonkey.play();", 500);
                    monkeyHangingHide();
                    take_or_risk_anim.visible = false;
                    mankeyLeftPoint.visible = true;
                    mankeyLeftPoint.animations.getAnimation('mankeyLeftPoint').play().onComplete.add(function(){
                        //winPoint1.play();
                        losePoint.play();
                        spiderPoint = game.add.sprite(207-mobileX,263-mobileY, 'game.spiderPoint');
                        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                        spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
                            mankeyLeftPoint.visible = false;

                            mankeyLosePoint.visible = true;
                            mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
                                updateBalanceGame4(game, scorePosions, balanceR);
                                game.state.start('game1');
                            });
                        });

                    });


                    setTimeout("unlockDisplay();",4500);
                }

            });

            questionPoint2 = game.add.sprite(527-mobileX,295-mobileY, 'game.questionPoint');
            questionPoint2.animations.add('questionPoint2', [0,1,2,3,4,5,6], 10, true);
            questionPoint2.animations.getAnimation('questionPoint2').play();
            questionPoint2.inputEnabled = true;
            questionPoint2.input.useHandCursor = true;
            questionPoint2.events.onInputDown.add(function(){
                if(ropeValues[5] != 0){
                    lockDisplay();

                    monkeyHangingHide();
                    mankeyRightPoint.visible = true;
                    setTimeout("bitMonkey.play();", 500);
                    take_or_risk_anim.visible = false;
                    mankeyRightPoint.animations.getAnimation('mankeyRightPoint').play().onComplete.add(function(){
                        winPoint1.play();
                        goldPoint = game.add.sprite(527-mobileX,263-mobileY, 'game.goldPoint');
                        goldPoint.animations.add('goldPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                        goldPoint.animations.getAnimation('goldPoint').play().onComplete.add(function(){
                            mankeyRightPoint.visible = false;
                            winPoint2.play();
                            mankeyWinPoint.visible = true;
                            mankeyWinPoint.animations.getAnimation('mankeyWinPoint').play().onComplete.add(function(){
                                updateBalanceGame4(game, scorePosions, balanceR);
                                game.state.start('game1');
                                /*mankeyWinPoint.visible = false;
                                 monkeyHanging();

                                 take_or_risk_anim.visible = true;

                                 goldPoint.visible = false;

                                 questionPoint1 = game.add.sprite(207-mobileX,295-mobileY, 'game.questionPoint');
                                 questionPoint1.animations.add('questionPoint1', [0,1,2,3,4,5,6], 10, true);
                                 questionPoint1.animations.getAnimation('questionPoint1').play();*/
                            });
                        });
                    });


                    setTimeout("unlockDisplay();",4000);
                } else {
                    lockDisplay();
                    setTimeout("bitMonkey.play();", 500);
                    monkeyHangingHide();
                    take_or_risk_anim.visible = false;
                    mankeyRightPoint.visible = true;
                    mankeyRightPoint.animations.getAnimation('mankeyRightPoint').play().onComplete.add(function(){
                        //winPoint1.play();
                        losePoint.play();
                        spiderPoint = game.add.sprite(527-mobileX,263-mobileY, 'game.spiderPoint');
                        spiderPoint.animations.add('spiderPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12], 10, false);
                        spiderPoint.animations.getAnimation('spiderPoint').play().onComplete.add(function(){
                            mankeyRightPoint.visible = false;

                            mankeyLosePoint.visible = true;
                            mankeyLosePoint.animations.getAnimation('mankeyLosePoint').play().onComplete.add(function(){
                                updateBalanceGame4(game, scorePosions, balanceR);
                            });
                        });

                    });


                    setTimeout("unlockDisplay();",4500);
                }
            });

            mankeyLeftPoint = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyLeftPoint');
            mankeyLeftPoint.animations.add('mankeyLeftPoint', [0,1,2,3,4,5,6,7], 10, false);
            mankeyLeftPoint.visible = false;

            mankeyRightPoint = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyRightPoint');
            mankeyRightPoint.animations.add('mankeyRightPoint', [0,1,2,3,4,5,6,7], 10, false);
            mankeyRightPoint.visible = false;

            mankeyWinPoint = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyWinPoint');
            mankeyWinPoint.animations.add('mankeyWinPoint', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, false);
            mankeyWinPoint.visible = false;

            mankeyLosePoint = game.add.sprite(305-mobileX,74-mobileY, 'game.mankeyLosePoint');
            mankeyLosePoint.animations.add('mankeyLosePoint', [0,1,2,3,4,5,6], 10, false);
            mankeyLosePoint.visible = false;
        };

        game4.update = function () {
        };

        game.state.add('game4', game4);

    })();


}