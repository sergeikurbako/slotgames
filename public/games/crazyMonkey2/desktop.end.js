(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background', needUrlPath + '/img/canvas-bg.svg');
        game.load.image('game.background1', needUrlPath + '/img/shape319.png');

        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');
        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/1.png');
        game.load.image('game.number2', needUrlPath + '/img/2.png');
        game.load.image('game.number3', needUrlPath + '/img/3.png');
        game.load.image('game.number4', needUrlPath + '/img/4.png');
        game.load.image('game.number5', needUrlPath + '/img/5.png');
        game.load.image('game.number6', needUrlPath + '/img/6.png');
        game.load.image('game.number7', needUrlPath + '/img/7.png');
        game.load.image('game.number8', needUrlPath + '/img/8.png');
        game.load.image('game.number9', needUrlPath + '/img/9.png');

        game.load.image('game.non_full', needUrlPath + '/img/full.png');
        game.load.image('game.full', needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotate', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stop', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        game.load.spritesheet('game.mask', needUrlPath + '/img/mask.png', 96, 96);
        game.load.spritesheet('game.monkey', needUrlPath + '/img/monkey.png', 96, 96);
        game.load.audio('winMonkey', needUrlPath + '/sound/winMonkey.mp3');


        game.load.image('game.backgroundGame3', needUrlPath + '/img/shape164.svg');
        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sound/sound30.mp3');

        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/monkeyBoom.png', 96, 96); //текущее кол-во кадров = 11

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shape167.png');

        game.load.image('card_39', needUrlPath + '/img/shape169.png');
        game.load.image('card_40', needUrlPath + '/img/shape171.png');
        game.load.image('card_41', needUrlPath + '/img/shape173.png');
        game.load.image('card_42', needUrlPath + '/img/shape175.png');
        game.load.image('card_43', needUrlPath + '/img/shape177.png');
        game.load.image('card_44', needUrlPath + '/img/shape179.png');
        game.load.image('card_45', needUrlPath + '/img/shape181.png');
        game.load.image('card_46', needUrlPath + '/img/shape183.png');
        game.load.image('card_47', needUrlPath + '/img/shape185.png');
        game.load.image('card_48', needUrlPath + '/img/shape187.png');
        game.load.image('card_49', needUrlPath + '/img/shape189.png');
        game.load.image('card_50', needUrlPath + '/img/shape191.png');
        game.load.image('card_51', needUrlPath + '/img/shape193.png');

        game.load.image('card_26', needUrlPath + '/img/shape195.png');
        game.load.image('card_27', needUrlPath + '/img/shape197.png');
        game.load.image('card_28', needUrlPath + '/img/shape199.png');
        game.load.image('card_29', needUrlPath + '/img/shape201.png');
        game.load.image('card_30', needUrlPath + '/img/shape203.png');
        game.load.image('card_31', needUrlPath + '/img/shape205.png');
        game.load.image('card_32', needUrlPath + '/img/shape207.png');
        game.load.image('card_33', needUrlPath + '/img/shape209.png');
        game.load.image('card_34', needUrlPath + '/img/shape211.png');
        game.load.image('card_35', needUrlPath + '/img/shape213.png');
        game.load.image('card_36', needUrlPath + '/img/shape215.png');
        game.load.image('card_37', needUrlPath + '/img/shape217.png');
        game.load.image('card_38', needUrlPath + '/img/shape219.png');

        game.load.image('card_0', needUrlPath + '/img/shape221.png');
        game.load.image('card_1', needUrlPath + '/img/shape223.png');
        game.load.image('card_2', needUrlPath + '/img/shape225.png');
        game.load.image('card_3', needUrlPath + '/img/shape227.png');
        game.load.image('card_4', needUrlPath + '/img/shape229.png');
        game.load.image('card_5', needUrlPath + '/img/shape231.png');
        game.load.image('card_6', needUrlPath + '/img/shape233.png');
        game.load.image('card_7', needUrlPath + '/img/shape235.png');
        game.load.image('card_8', needUrlPath + '/img/shape237.png');
        game.load.image('card_9', needUrlPath + '/img/shape239.png');
        game.load.image('card_10', needUrlPath + '/img/shape241.png');
        game.load.image('card_11', needUrlPath + '/img/shape243.png');
        game.load.image('card_12', needUrlPath + '/img/shape245.png');

        game.load.image('card_13', needUrlPath + '/img/shape247.png');
        game.load.image('card_14', needUrlPath + '/img/shape249.png');
        game.load.image('card_15', needUrlPath + '/img/shape251.png');
        game.load.image('card_16', needUrlPath + '/img/shape253.png');
        game.load.image('card_17', needUrlPath + '/img/shape255.png');
        game.load.image('card_18', needUrlPath + '/img/shape257.png');
        game.load.image('card_19', needUrlPath + '/img/shape259.png');
        game.load.image('card_20', needUrlPath + '/img/shape261.png');
        game.load.image('card_21', needUrlPath + '/img/shape263.png');
        game.load.image('card_22', needUrlPath + '/img/shape265.png');
        game.load.image('card_23', needUrlPath + '/img/shape267.png');
        game.load.image('card_24', needUrlPath + '/img/shape269.png');
        game.load.image('card_25', needUrlPath + '/img/shape271.png');
        game.load.image('card_52', needUrlPath + '/img/shape273.png');

        game.load.image('pick', needUrlPath + '/img/shape281.png');

        game.load.spritesheet('game.monkey1', needUrlPath + '/img/monkey1.png', 160, 144);
        game.load.spritesheet('game.monkey2', needUrlPath + '/img/monkey2.png', 160, 144);
        game.load.spritesheet('game.monkey3', needUrlPath + '/img/monkey3.png', 160, 144);
        game.load.spritesheet('game.monkey4', needUrlPath + '/img/monkey4.png', 160, 144);
        game.load.spritesheet('game.monkey5', needUrlPath + '/img/monkey5.png', 160, 144);
        game.load.spritesheet('game.monkeyWin', needUrlPath + '/img/monkeyWin.png', 160, 144);
        game.load.spritesheet('game.monkeyTakeMushroom1', needUrlPath + '/img/monkeyTakeMushroom1.png', 224, 144);
        game.load.spritesheet('game.monkeyTakeMushroom2', needUrlPath + '/img/monkeyTakeMushroom2.png', 224, 144);
        game.load.spritesheet('game.butterfly1', needUrlPath + '/img/butterfly1.png', 240, 96);
        game.load.spritesheet('game.butterfly2', needUrlPath + '/img/butterfly2.png', 240, 96);
        game.load.spritesheet('game.butterfly3x', needUrlPath + '/img/butterfly3x.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies1', needUrlPath + '/img/butterflyFlies1.png', 240, 96);
        game.load.spritesheet('game.butterflyFlies2', needUrlPath + '/img/butterflyFlies2.png', 240, 96);
        game.load.spritesheet('game.mushroomGrow', needUrlPath + '/img/mushroomGrow.png', 64, 80);
        game.load.spritesheet('game.mushroomJump', needUrlPath + '/img/mushroomJump.png', 64, 80);

        game.load.image('game.backgroundGame2', needUrlPath + '/img/shape1150.png');
        game.load.spritesheet('game.rope', needUrlPath + '/img/rope.png', 16, 320);

        game.load.spritesheet('game.monkey21', needUrlPath + '/img/monkey21.png', 176, 480);
        game.load.spritesheet('game.monkey22', needUrlPath + '/img/monkey22.png', 176, 480);
        game.load.spritesheet('game.monkey23', needUrlPath + '/img/monkey23.png', 176, 480);
        game.load.spritesheet('game.monkey24', needUrlPath + '/img/monkey24.png', 176, 480);

        game.load.spritesheet('game.monkey21H', needUrlPath + '/img/monkey21H.png', 176, 480);
        game.load.spritesheet('game.monkey22H', needUrlPath + '/img/monkey22H.png', 176, 480);
        game.load.spritesheet('game.monkey23H', needUrlPath + '/img/monkey23H.png', 176, 480);
        game.load.spritesheet('game.monkey24H', needUrlPath + '/img/monkey24H.png', 176, 480);

        game.load.spritesheet('game.monkeyTakeRope1', needUrlPath + '/img/monkeyTakeRope1.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2', needUrlPath + '/img/monkeyTakeRope2.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope1H', needUrlPath + '/img/monkeyTakeRope1H.png', 176, 480);
        game.load.spritesheet('game.monkeyTakeRope2H', needUrlPath + '/img/monkeyTakeRope2H.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBanana', needUrlPath + '/img/monkeyEatBanana.png', 176, 480);
        game.load.spritesheet('game.monkeyEatBananaH', needUrlPath + '/img/monkeyEatBananaH.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer1', needUrlPath + '/img/monkeyEatHammer1.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer12', needUrlPath + '/img/monkeyEatHammer12.png', 176, 480);
        game.load.spritesheet('game.monkeyEatHammer2', needUrlPath + '/img/monkeyEatHammer2.png', 160, 480);
        game.load.spritesheet('game.monkeyEatHammer22', needUrlPath + '/img/monkeyEatHammer22.png', 176, 480);
        game.load.spritesheet('game.monkeyHeadache', needUrlPath + '/img/monkeyHeadache.png', 176, 480);

        game.load.audio('game.winRope1', needUrlPath + '/sound/winRope1.mp3');
        game.load.audio('game.winRope2', needUrlPath + '/sound/winRope2.mp3');
        game.load.audio('game.bitMonkey', needUrlPath + '/sound/bitMonkey.mp3');

        game.load.image('game.backgroundGame4', needUrlPath + '/img/shape943.svg');

        game.load.spritesheet('game.mankeyHanging1', needUrlPath + '/img/mankeyHanging1.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging2', needUrlPath + '/img/mankeyHanging2.png', 240, 304);
        game.load.spritesheet('game.mankeyHanging3', needUrlPath + '/img/mankeyHanging3.png', 240, 304);
        game.load.spritesheet('game.mankeyLeftPoint', needUrlPath + '/img/mankeyLeftPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyLosePoint', needUrlPath + '/img/mankeyLosePoint.png', 240, 304);
        game.load.spritesheet('game.mankeyRightPoint', needUrlPath + '/img/mankeyRightPoint.png', 240, 304);
        game.load.spritesheet('game.mankeyWinPoint', needUrlPath + '/img/mankeyWinPoint.png', 240, 304);

        game.load.spritesheet('game.goldPoint', needUrlPath + '/img/goldPoint.png', 96, 128);
        game.load.spritesheet('game.questionPoint', needUrlPath + '/img/questionPoint.png', 96, 80);
        game.load.spritesheet('game.spiderPoint', needUrlPath + '/img/spiderPoint.png', 96, 128);
        game.load.spritesheet('game.flashFive', needUrlPath + '/img/flashFive.png', 32, 48);
        game.load.spritesheet('game.take_or_risk_anim', needUrlPath + '/img/take_or_risk_anim.png', 192, 160);

        game.load.image('game.take_or_risk_bg', needUrlPath + '/img/shape953.svg');
        game.load.audio('game.winPoint1', needUrlPath + '/sound/winPoint1.mp3');
        game.load.audio('game.winPoint2', needUrlPath + '/sound/winPoint2.mp3');
        game.load.audio('game.losePoint', needUrlPath + '/sound/losePoint.mp3');

        game.load.image('game.monkeyR', needUrlPath + '/img/monkeyR.png');
        game.load.image('game.butterflyR', needUrlPath + '/img/butterflyR.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.jpg');
        game.load.image('cell1', needUrlPath + '/img/cell1.jpg');
        game.load.image('cell2', needUrlPath + '/img/cell2.jpg');
        game.load.image('cell3', needUrlPath + '/img/cell3.jpg');
        game.load.image('cell4', needUrlPath + '/img/cell4.jpg');
        game.load.image('cell5', needUrlPath + '/img/cell5.jpg');
        game.load.image('cell6', needUrlPath + '/img/cell6.jpg');
        game.load.image('cell7', needUrlPath + '/img/cell7.jpg');
        game.load.image('cell8', needUrlPath + '/img/cell8.jpg');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.jpg', 96, 96);

        game.load.image('bonusGame', needUrlPath + '/img/image536.jpg');
        game.load.image('wildSymbol', needUrlPath + '/img/image537.jpg');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.jpg');
        game.load.image('takeOrRisk2', needUrlPath + '/img/image475.jpg');
        game.load.image('take', needUrlPath + '/img/image554.jpg');

        game.load.image('topScoreGame2', needUrlPath + '/img/image457.png');

        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

