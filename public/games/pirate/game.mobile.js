function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

isMobile = true;

var width = 829.7;
var height = 598.95;

var mobileX = 93;
var mobileY = 23;

if(isMobile) {
    width = 640;
    height = 480;
}
window.game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');

function createLevelButtons() {
    var lvl1 = game.add.sprite(0,30, 'game.non_full');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(20, 30, 'game.non_full');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');
    }, this);

    var lvl3 = game.add.sprite(40, 30, 'game.non_full');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');

    }, this);

    var lvl4 = game.add.sprite(60, 30, 'game.non_full');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');

    }, this);
}

function hideNumbers() {
    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;
}

function showNumbers(n) {

    if(n == 1){
        number1.visible = true;
    } 

    if(n == 3) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
    } 

    if(n == 5) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
    }

    if(n == 7) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
    }

    if(n == 9) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    }
}
//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 5;

    var lines = {
        1: {
            coord: 199-mobileY,
            sprite: null,
            btncoord: 250,
            button: null
        },
        2: {
            coord: 71-mobileY,
            sprite: null
        },
        3: {
            coord: 322-mobileY,
            sprite: null,
            btncoord: 295,
            button: null
        },
        4: {
            coord: 130-mobileY,
            sprite: null
        },
        5: {
            coord: 95-mobileY,
            sprite: null,
            btncoord: 340,
            button: null
        },
        6: {
            coord: 102-mobileY,
            sprite: null
        },
        7: {
            coord: 228-mobileY,
            sprite: null,
            btncoord: 385,
            button: null
        },
        8: {
            coord: 226-mobileY,
            sprite: null
        },
        9: {
            coord: 120-mobileY,
            sprite: null,
            btncoord: 430,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }

    var game1 = {};


    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }

    var game1 = {};


    game1.preload = function () {

    };

    game1.create = function () {
		
		var playSound = game.add.audio('play');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');
        takeWin = game.add.audio('takeWin');
        takeWin.addMarker('take', 0, 0.6);
        winCover = game.add.audio('winCover');
        winPech = game.add.audio('game.winPech');
        
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,21-mobileY, 'game.totalBet');
        
        var positions = [
            game.world.centerX - 224,
            game.world.centerX - 112,
            game.world.centerX + 0,
            game.world.centerX + 112,
            game.world.centerX + 223
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 84+mobileY, 96, 282, 'game.bar');
            bars[i].anchor.setTo(0.5, 0.5); //53
            bars[i].tilePosition.y = randomNumber(0, 8) * 96;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        line1x = game.add.sprite(250,510, 'game.line1_d');
        line1x.scale.setTo(0.7, 0.7);
        line3x = game.add.sprite(295,510, 'game.line3_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line5x.scale.setTo(0.7, 0.7);
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line7x.scale.setTo(0.7, 0.7);
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line9x.scale.setTo(0.7, 0.7);
        selectGamex = game.add.sprite(70,510, 'game.selectGame_d');
        selectGamex.scale.setTo(0.7, 0.7);
        payTablex = game.add.sprite(150,510, 'game.payTable_d');
        payTablex.scale.setTo(0.7, 0.7);
        betonex = game.add.sprite(490,510, 'game.betone_d');
        betonex.scale.setTo(0.7, 0.7);
        betmaxx = game.add.sprite(535,510, 'game.betmax_d');
        betmaxx.scale.setTo(0.7, 0.7);
        automaricstartx = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstartx.scale.setTo(0.7, 0.7);

        line1x.visible = false;
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;
        selectGamex.visible = false;
        payTablex.visible = false;
        betonex.visible = false;
        betmaxx.visible = false;
        automaricstartx.visible = false;

        game.check_win = 0;

        function buttonClicked() {
            
            if (spinning) {
                return;
            }

            if(game.check_win == 0) {
                lines[1].button.visible = false;
                lines[3].button.visible = false;
                lines[5].button.visible = false;
                lines[7].button.visible = false;
                lines[9].button.visible = false;

                line1x.visible = true;
                line3x.visible = true;
                line5x.visible = true;
                line7x.visible = true;
                line9x.visible = true;
                selectGamex.visible = true;
                payTablex.visible = true;
                betonex.visible = true;
                betmaxx.visible = true;
                automaricstartx.visible = true;
                button.loadTexture('game.start_d');
            }
            
        }

        function buttonRelease() {
            if(game.check_win == 1) {
                hideLines();
                selectLine(3);
                takeWin.play('take');
                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                line1x.visible = false;
                line3x.visible = false;
                line5x.visible = false;
                line7x.visible = false;
                line9x.visible = false;
                selectGamex.visible = false;
                payTablex.visible = false;
                betonex.visible = false;
                betmaxx.visible = false;
                automaricstartx.visible = false;
                button.loadTexture('game.start');

                game.check_win = 0;

                flashNamber1.animations.stop();
                flashNamber2.animations.stop();
                flashNamber3.animations.stop();

                flashNamber1.visible = false;
                flashNamber2.visible = false;
                flashNamber3.visible = false;

                blin.animations.stop();
                blin.visible = false;
                pech.animations.stop();
                pech.visible = false;
            } else {
                if (spinning) {
                    return;
                }
                hideLines();
                barsCurrentSpins = [0, 0, 0, 0, 0];
                spinningBars = bars.length;
                spinning = true;
                playSound.play();
            }
        }



        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(134-mobileX, lines[i].coord, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 510, 'btnline' + i);
                lines[i].button.scale.setTo(0.7,0.7);
                lines[i].button.inputEnabled = true;
                lines[i].button.input.useHandCursor = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {

                        hideLines();
                        preselectLine(n);

                        hideNumbers();
                        showNumbers(n);

                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                    lines[n].button.events.onInputOut.add(function () {
                        lines[n].button.loadTexture('btnline' + n);
                    }, this);
                    lines[n].button.events.onInputOver.add(function () {
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                })(i);
            }
        }

        number1 = game.add.sprite(15,183-mobileY, 'game.number1');
        number2 = game.add.sprite(15,54-mobileY, 'game.number2');
        number3 = game.add.sprite(15,310-mobileY, 'game.number3');
        number4 = game.add.sprite(15,118-mobileY, 'game.number4');
        number5 = game.add.sprite(15,246-mobileY, 'game.number5');
        number6 = game.add.sprite(15,86-mobileY, 'game.number6');
        number7 = game.add.sprite(15,278-mobileY, 'game.number7');
        number8 = game.add.sprite(15,214-mobileY, 'game.number8');
        number9 = game.add.sprite(15,150-mobileY, 'game.number9');

        preselectLine(9);

        //создание анимаций
        function hideBaba(){
            baba1.visible = false;
            baba2.visible = false;
            baba3.visible = false;
            baba4.visible = false;
            baba5.visible = false;
            baba6.visible = false;
        }

        function showRandBaba(){
            var randBaba = randomNumber(1,6);

            switch(randBaba) {
                case 1:
                    hideBaba();
                    baba1.visible = true;
                    baba1.animations.getAnimation('baba1').play().onComplete.add(function(){
                        baba1.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 2:
                    hideBaba();
                    baba2.visible = true;
                    baba2.animations.getAnimation('baba2').play().onComplete.add(function(){
                        baba2.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 3:
                    hideBaba();
                    baba3.visible = true;
                    baba3.animations.getAnimation('baba3').play().onComplete.add(function(){
                        baba3.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 4:
                    hideBaba();
                    baba4.visible = true;
                    baba4.animations.getAnimation('baba4').play().onComplete.add(function(){
                        baba4.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 5:
                    hideBaba();
                    baba5.visible = true;
                    baba5.animations.getAnimation('baba5').play().onComplete.add(function(){
                        baba5.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 6:
                    hideBaba();
                    baba6.visible = true;
                    baba6.animations.getAnimation('baba6').play().onComplete.add(function(){
                        baba6.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }

        baba1 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba1');
        baba1.animations.add('baba1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5, false);
        baba1.visible = false;

        baba2 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba2');
        baba2.animations.add('baba2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba2.visible = false;

        baba3 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba3');
        baba3.animations.add('baba3', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5, false);
        baba3.visible = false;

        baba4 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba4');
        baba4.animations.add('baba4', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba4.visible = false;

        baba5 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba5');
        baba5.animations.add('baba5', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba5.visible = false;

        baba6 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba6');
        baba6.animations.add('baba6', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5, false);
        baba6.visible = false;

        
        showRandBaba();

        cat = game.add.sprite(221-mobileX,374-mobileY, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557-mobileX,390-mobileY, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();


        button = game.add.sprite(588, 228, 'game.start');

        //кнопки
        double = game.add.sprite(550, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
        });

        bet1 = game.add.sprite(546, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputDown.add(function(){
            bet1.loadTexture('game.bet1_p');
            document.getElementById('betMode').style.display = 'block';
            widthVisibleZone = $('.betWrapper .visibleZone').height();
            console.log(widthVisibleZone);
            $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
            $('canvas').css('display', 'none');
        });
        bet1.events.onInputUp.add(function(){
            bet1.loadTexture('bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });

        var dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
            //game.state.start('game4');
        });

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });

        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);

        takeOrRisk = game.add.sprite(265-mobileX,358-mobileY, 'game.takeOrRisk');
       	
        createLevelButtons();
    };

    game1.update = function () {
        if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 96;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('game.start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {

                    blin = game.add.sprite(365-mobileX,156-mobileY, 'game.blin');
                    blin.animations.add('blin', [0,1,2,3,4,5,6,7], 5, false);
                    blin.visible = true;
                    blin.animations.getAnimation('blin').play();

                    pech = game.add.sprite(477-mobileX,156-mobileY, 'game.pech');
                    pech.animations.add('pech', [0,1,2], 2, false);
                    pech.visible = true;
                    pech.animations.getAnimation('pech').play();

                    

                    preselectLine(3);
                    tadaSound.play();

                    hideNumbers();

                    game.check_win = 1;

                    

                    flashNamber1 = game.add.sprite(93+16-mobileX,182-mobileY, 'game.flashNamber1');
                    flashNamber1.animations.add('flashNamber1', [0,1], 1, true);
                    flashNamber1.animations.getAnimation('flashNamber1').play();

                    flashNamber2 = game.add.sprite(93+16-mobileX,54-mobileY, 'game.flashNamber2');
                    flashNamber2.animations.add('flashNamber2', [0,1], 1, true);
                    flashNamber2.animations.getAnimation('flashNamber2').play();

                    flashNamber3 = game.add.sprite(93+16-mobileX,310-mobileY, 'game.flashNamber3');
                    flashNamber3.animations.add('flashNamber3', [0,1], 1, true);
                    flashNamber3.animations.getAnimation('flashNamber3').play();

                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                } else if( currentLine == 5){
                    winPech.play();
                    game.check_win = 0;
                    hideLines();
                    lockDisplay();

                    pech = game.add.sprite(477-mobileX,156-mobileY, 'game.pech');
                    pech.animations.add('pech', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,], 6, false);
                    pech.visible = true;
                    pech.animations.getAnimation('pech').play();

                    pech2 = game.add.sprite(365-mobileX,156-mobileY, 'game.pech');
                    pech2.animations.add('pech2', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,], 6, false);
                    pech2.visible = true;
                    pech2.animations.getAnimation('pech2').play();

                    pech3 = game.add.sprite(253-mobileX,156-mobileY, 'game.pech');
                    pech3.animations.add('pech3', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,], 6, false);
                    pech3.visible = true;
                    pech3.animations.getAnimation('pech3').play().onComplete.add(function(){
                        pech3.visible = false;
                        unlockDisplay();
                        game.state.start('game4');
                    });

                    
                } else {
                    lines[1].button.visible = true;
                    lines[3].button.visible = true;
                    lines[5].button.visible = true;
                    lines[7].button.visible = true;
                    lines[9].button.visible = true;

                    line1x.visible = false;
                    line3x.visible = false;
                    line5x.visible = false;
                    line7x.visible = false;
                    line9x.visible = false;
                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                }
            }
        }
        
    };

    game.state.add('game1', game1);


})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================
function showRandBaba(){
    var randBaba = randomNumber(7,10);

    switch(randBaba) {
        case 7:
            hideBaba();
            baba7.visible = true;
            baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                baba7.animations.stop();
                showRandBaba();
            });
            break;
        case 8:
            hideBaba();
            baba8.visible = true;
            baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                baba8.animations.stop();
                showRandBaba();
            });
            break;
        case 9:
            hideBaba();
            baba9.visible = true;
            baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                baba9.animations.stop();
                showRandBaba();
            });
            break;
        case 10:
            hideBaba();
            baba10.visible = true;
            baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                baba10.animations.stop();
                showRandBaba();
            });
            break;
        case 12:
            hideBaba();
            baba12.visible = true;
            baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                baba12.animations.stop();
                showRandBaba();
            });
            break;

    }
}

function showBabaLegs(x,y){
    babaLegs = game.add.sprite(x,y, 'game.babaLegs');
}

function createBaba(x,y){
    baba7 = game.add.sprite(x,y, 'game.baba7');
    baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
    baba7.visible = false;

    baba8 = game.add.sprite(x,y, 'game.baba8');
    baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
    baba8.visible = false;

    baba9 = game.add.sprite(x,y, 'game.baba9');
    baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
    baba9.visible = false;

    baba10 = game.add.sprite(x,y, 'game.baba10');
    baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
    baba10.visible = false;

    baba11 = game.add.sprite(x,y, 'game.baba11');
    baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
    baba11.visible = false;

    baba12 = game.add.sprite(x,y, 'game.baba12');
    baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
    baba12.visible = false;
}

function showBoogiman(){
    arrayXY = [[150,300],[170,320],[[130],[300]],[[160],[310]]];

    var rand = randomNumber(1,2);

    switch (rand) {
        case 1:
            var x = arrayXY[0][0];
            var y = arrayXY[0][1];

            boogiman = game.add.sprite(x,y, 'game.boogiman');
            boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            boogiman.inputEnabled = true;
	        boogiman.input.useHandCursor = true;
	        boogiman.events.onInputUp.add(function(){
	            lockDisplay();

	            hideBaba();
	            babaLegs.visible = false;

	            if(boogiman != undefined){
	                boogiman.animations.stop();
	                boogiman.visible = false;
	            } else {
	                boogiman2.animations.stop();
	                boogiman2.visible = false;
	            }


	            babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
	            babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
	            babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
	                babaStrike1_wolf.visible = false;
	                
	                upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
	                upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
	                upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
	                    upWolf.visible = false;
	                });

	                babaLoseFromWolf.play();

	                arrow1.visible = false;
	                arrow2.visible = false;

	                babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
	                babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
	                babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
	                    babaStrike2_wolf.visible = false;

	                    babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
	                    babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
	                    babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
	                        babaAndFolf11.visible = false;

	                        babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
	                        babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
	                        babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
	                            babaAndFolf12.visible = false;

	                            babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
	                            babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
	                            babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
	                                babaAndFolf21.visible = false;

	                                babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
	                                babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
	                                babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
	                                    babaAndFolf22.visible = false;
	                                    game.state.start('game1'); 
	                                });
	                            });

	                        });
	                    });
	                });
	            });

	            setTimeout("unlockDisplay();",6000);
	            
	        });
            boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                boogiman.visible = false;

                boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                boogiman2.inputEnabled = true;
		        boogiman2.input.useHandCursor = true;
		        boogiman2.events.onInputDown.add(function(){
		            lockDisplay();

		            setTimeout("winCards.play();",1000);

		            hideBaba();
		            babaLegs.visible = false;
		            arrow1.visible = false;
		            arrow2.visible = false;
		            if(boogiman != undefined){
		                boogiman.animations.stop();
		                boogiman.visible = false;
		            } else {
		                boogiman2.animations.stop();
		                boogiman2.visible = false;
		            }

		            babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
		            babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
		            babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
		                babaTakeKeks1.visible = false;

		                babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
		                babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
		                babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
		                    babaTakeKeks2.visible = false;
		                    game.state.start('game1');                  
		                });
		            });

		            setTimeout("unlockDisplay();",3000);
		        });
                boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                    boogiman2.visible = false;

                    showBoogiman();
                });
            });

            break;
        case 2:
            var x = arrayXY[1][0];
            var y = arrayXY[1][1];

            boogiman = game.add.sprite(x,y, 'game.boogiman');
            boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            boogiman.inputEnabled = true;
	        boogiman.input.useHandCursor = true;
	        boogiman.events.onInputUp.add(function(){
	            lockDisplay();

	            hideBaba();
	            babaLegs.visible = false;

	            if(boogiman != undefined){
	                boogiman.animations.stop();
	                boogiman.visible = false;
	            } else {
	                boogiman2.animations.stop();
	                boogiman2.visible = false;
	            }


	            babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
	            babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
	            babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
	                babaStrike1_wolf.visible = false;
	                
	                upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
	                upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
	                upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
	                    upWolf.visible = false;
	                });

	                babaLoseFromWolf.play();

	                arrow1.visible = false;
	                arrow2.visible = false;

	                babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
	                babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
	                babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
	                    babaStrike2_wolf.visible = false;

	                    babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
	                    babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
	                    babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
	                        babaAndFolf11.visible = false;

	                        babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
	                        babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
	                        babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
	                            babaAndFolf12.visible = false;

	                            babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
	                            babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
	                            babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
	                                babaAndFolf21.visible = false;

	                                babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
	                                babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
	                                babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
	                                    babaAndFolf22.visible = false;
	                                    game.state.start('game1'); 
	                                });
	                            });

	                        });
	                    });
	                });
	            });

	            setTimeout("unlockDisplay();",6000);
	            
	        });
            boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                boogiman.visible = false;

                boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                boogiman2.inputEnabled = true;
		        boogiman2.input.useHandCursor = true;
		        boogiman2.events.onInputDown.add(function(){
		            lockDisplay();

		            setTimeout("winCards.play();",1000);

		            hideBaba();
		            babaLegs.visible = false;
		            arrow1.visible = false;
		            arrow2.visible = false;
		            if(boogiman != undefined){
		                boogiman.animations.stop();
		                boogiman.visible = false;
		            } else {
		                boogiman2.animations.stop();
		                boogiman2.visible = false;
		            }

		            babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
		            babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
		            babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
		                babaTakeKeks1.visible = false;

		                babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
		                babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
		                babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
		                    babaTakeKeks2.visible = false;
		                    game.state.start('game1');                  
		                });
		            });

		            setTimeout("unlockDisplay();",3000);
		        });
                boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                    boogiman2.visible = false;

                    showBoogiman();
                });
            });

            break;
        case 3:
            var x = arrayXY[2][0];
            var y = arrayXY[2][1];

            boogiman = game.add.sprite(x,y, 'game.boogiman');
            boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            boogiman.inputEnabled = true;
	        boogiman.input.useHandCursor = true;
	        boogiman.events.onInputUp.add(function(){
	            lockDisplay();

	            hideBaba();
	            babaLegs.visible = false;

	            if(boogiman != undefined){
	                boogiman.animations.stop();
	                boogiman.visible = false;
	            } else {
	                boogiman2.animations.stop();
	                boogiman2.visible = false;
	            }


	            babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
	            babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
	            babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
	                babaStrike1_wolf.visible = false;
	                
	                upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
	                upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
	                upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
	                    upWolf.visible = false;
	                });

	                babaLoseFromWolf.play();

	                arrow1.visible = false;
	                arrow2.visible = false;

	                babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
	                babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
	                babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
	                    babaStrike2_wolf.visible = false;

	                    babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
	                    babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
	                    babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
	                        babaAndFolf11.visible = false;

	                        babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
	                        babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
	                        babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
	                            babaAndFolf12.visible = false;

	                            babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
	                            babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
	                            babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
	                                babaAndFolf21.visible = false;

	                                babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
	                                babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
	                                babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
	                                    babaAndFolf22.visible = false;
	                                    game.state.start('game1'); 
	                                });
	                            });

	                        });
	                    });
	                });
	            });

	            setTimeout("unlockDisplay();",6000);
	            
	        });
            boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                boogiman.visible = false;

                boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                boogiman2.inputEnabled = true;
		        boogiman2.input.useHandCursor = true;
		        boogiman2.events.onInputDown.add(function(){
		            lockDisplay();

		            setTimeout("winCards.play();",1000);

		            hideBaba();
		            babaLegs.visible = false;
		            arrow1.visible = false;
		            arrow2.visible = false;
		            if(boogiman != undefined){
		                boogiman.animations.stop();
		                boogiman.visible = false;
		            } else {
		                boogiman2.animations.stop();
		                boogiman2.visible = false;
		            }

		            babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
		            babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
		            babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
		                babaTakeKeks1.visible = false;

		                babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
		                babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
		                babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
		                    babaTakeKeks2.visible = false;
		                    game.state.start('game1');                  
		                });
		            });

		            setTimeout("unlockDisplay();",3000);
		        });
                boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                    boogiman2.visible = false;

                    showBoogiman();
                });
            });
            break;
        case 4:
            var x = arrayXY[3][0];
            var y = arrayXY[3][1];

            boogiman = game.add.sprite(x,y, 'game.boogiman');
            boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            boogiman.inputEnabled = true;
	        boogiman.input.useHandCursor = true;
	        boogiman.events.onInputUp.add(function(){
	            lockDisplay();

	            hideBaba();
	            babaLegs.visible = false;

	            if(boogiman != undefined){
	                boogiman.animations.stop();
	                boogiman.visible = false;
	            } else {
	                boogiman2.animations.stop();
	                boogiman2.visible = false;
	            }


	            babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
	            babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
	            babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
	                babaStrike1_wolf.visible = false;
	                
	                upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
	                upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
	                upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
	                    upWolf.visible = false;
	                });

	                babaLoseFromWolf.play();

	                arrow1.visible = false;
	                arrow2.visible = false;

	                babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
	                babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
	                babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
	                    babaStrike2_wolf.visible = false;

	                    babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
	                    babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
	                    babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
	                        babaAndFolf11.visible = false;

	                        babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
	                        babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
	                        babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
	                            babaAndFolf12.visible = false;

	                            babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
	                            babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
	                            babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
	                                babaAndFolf21.visible = false;

	                                babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
	                                babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
	                                babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
	                                    babaAndFolf22.visible = false;
	                                    game.state.start('game1'); 
	                                });
	                            });

	                        });
	                    });
	                });
	            });

	            setTimeout("unlockDisplay();",6000);
	            
	        });
            boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                boogiman.visible = false;

                boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                boogiman2.inputEnabled = true;
		        boogiman2.input.useHandCursor = true;
		        boogiman2.events.onInputDown.add(function(){
		            lockDisplay();

		            setTimeout("winCards.play();",1000);

		            hideBaba();
		            babaLegs.visible = false;
		            arrow1.visible = false;
		            arrow2.visible = false;
		            if(boogiman != undefined){
		                boogiman.animations.stop();
		                boogiman.visible = false;
		            } else {
		                boogiman2.animations.stop();
		                boogiman2.visible = false;
		            }

		            babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
		            babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
		            babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
		                babaTakeKeks1.visible = false;

		                babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
		                babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
		                babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
		                    babaTakeKeks2.visible = false;
		                    game.state.start('game1');                  
		                });
		            });

		            setTimeout("unlockDisplay();",3000);
		        });
                boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                    boogiman2.visible = false;

                    showBoogiman();
                });
            });
            break;
    }
    
}

function hideBaba(){
    baba7.visible = false;
    baba8.visible = false;
    baba9.visible = false;
    baba10.visible = false;
    baba11.visible = false;
    baba12.visible = false;
}


(function () {

var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {

        var mobileX = 94;
        var mobileY = 22;
        //Добавление фона
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.totalBet');
        backgroundForGame2 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundForGame2');
        titleForGame2 = game.add.sprite(222-mobileX,54-mobileY, 'game.titleForGame2');
        
        winCards = game.add.audio("game.winCards");
        babaLoseFromWolf = game.add.audio('game.babaLoseFromWolf');
        //добавление кнопок

        
        arrow1 = game.add.sprite(295-mobileX,232-mobileY, 'game.arrow1');
        arrow1.inputEnabled = true;
        arrow1.input.useHandCursor = true;
        arrow1.events.onInputUp.add(function(){
            lockDisplay();

            hideBaba();
            babaLegs.visible = false;

            if(boogiman != undefined){
                boogiman.animations.stop();
                boogiman.visible = false;
            } else {
                boogiman2.animations.stop();
                boogiman2.visible = false;
            }


            babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
            babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
            babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
                babaStrike1_wolf.visible = false;
                
                upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
                upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
                upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
                    upWolf.visible = false;
                });

                babaLoseFromWolf.play();

                arrow1.visible = false;
                arrow2.visible = false;

                babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
                babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
                babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
                    babaStrike2_wolf.visible = false;

                    babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
                    babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
                    babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
                        babaAndFolf11.visible = false;

                        babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
                        babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
                        babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
                            babaAndFolf12.visible = false;

                            babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
                            babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
                            babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
                                babaAndFolf21.visible = false;

                                babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
                                babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
                                babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
                                    babaAndFolf22.visible = false;
                                    game.state.start('game1'); 
                                });
                            });

                        });
                    });
                });
            });

            setTimeout("unlockDisplay();",6000);
            
        });

        arrow2 = game.add.sprite(595-mobileX,232-mobileY, 'game.arrow2');
        arrow2.inputEnabled = true;
        arrow2.input.useHandCursor = true;
        arrow2.events.onInputDown.add(function(){
            lockDisplay();

            setTimeout("winCards.play();",1000);

            hideBaba();
            babaLegs.visible = false;
            arrow1.visible = false;
            arrow2.visible = false;
            if(boogiman != undefined){
                boogiman.animations.stop();
                boogiman.visible = false;
            } else {
                boogiman2.animations.stop();
                boogiman2.visible = false;
            }

            babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
            babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
            babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
                babaTakeKeks1.visible = false;

                babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
                babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
                babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
                    babaTakeKeks2.visible = false;
                    game.state.start('game1');                  
                });
            });

            setTimeout("unlockDisplay();",3000);
        });

        arrow1.animations.add('arrow1', [0,1], 2, true);
        arrow2.animations.add('arrow2', [0,1], 2, true);
        arrow1.animations.getAnimation('arrow1').play();
        arrow2.animations.getAnimation('arrow2').play();

        showBoogiman();
        showBabaLegs(117-mobileX,452-mobileY);
        createBaba(101-mobileX,292-mobileY);
        showRandBaba();

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        createLevelButtons();
    };

    game2.update = function () {
        
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

function randomCard() {
    var arr = ['2b','3b','4b','5b','6b','7b','8b','9b','10b','jb','qb','kb','ab','2ch','3ch','4ch','5ch','6ch','7ch','8ch','9ch','10ch','jch','qch','kch','ach','2k','3k','4k','5k','6k','7k','8k','9k',
        '10k','jk','qk','kk','ak','2p','3p','4p','5p','6p','7p','8p','9p','10p','jp','qp'];

    var rand = Math.floor(Math.random() * arr.length);

    return 'game.card_'+arr[rand];
}

function hidePick() {
    pick2.visible = false;
    pick3.visible = false;
    pick4.visible = false;
    pick5.visible = false;
}

function openCardSound() {
    openCardAudio.play();
}



(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {
    	var mobileX = 93;
    	var mobileY = 22;

        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.totalBet');
        backgroundGame3 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame3');
        
        openCardAudio = game.add.audio("game.openCardAudio");
        winCards = game.add.audio("game.winCards");

        lockDisplay();
        setTimeout('unlockDisplay();', 500);
        setTimeout('openCardSound(); card1 = game.add.sprite(128-mobileX,133-14-mobileY, randomCard());',500);

        card1 = game.add.sprite(128-mobileX,133-14-mobileY, 'game.card_garage');

        card2 = game.add.sprite(253-mobileX,133-14-mobileY, 'game.card_garage');
        card2.inputEnabled = true;
        card2.input.useHandCursor = true;
        card2.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            
            setTimeout('openCardSound(); card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick();',
                3000);
        });

        card3 = game.add.sprite(365-mobileX,133-14-mobileY, 'game.card_garage');
        card3.inputEnabled = true;
        card3.input.useHandCursor = true;
        card3.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            card3.loadTexture(randomCard());
            openCardSound();
            winCards.play(); 
            setTimeout('card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick();',
                3000);
        });

        card4 = game.add.sprite(480-mobileX,133-14-mobileY, 'game.card_garage');
        card4.inputEnabled = true;
        card4.input.useHandCursor = true;
        card4.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick();',
                3000);
        });

        card5 = game.add.sprite(592-mobileX,133-14-mobileY, 'game.card_garage');
        card5.inputEnabled = true;
        card5.input.useHandCursor = true;
        card5.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('game.state.start("game1"); card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick();',
                3000);
        });

        pick2 = game.add.sprite(605-10-mobileX,300-22-mobileY, 'game.pick');
        pick3 = game.add.sprite(483-mobileX,300-22-mobileY, 'game.pick');
        pick4 = game.add.sprite(385-17-mobileX,300-22-mobileY, 'game.pick');
        pick5 = game.add.sprite(275-20-mobileX,300-22-mobileY, 'game.pick');
        pick2.visible = false;
        pick3.visible = false;
        pick4.visible = false;
        pick5.visible = false;

        function hideBaba(){
            baba1.visible = false;
            baba2.visible = false;
            baba3.visible = false;
            baba4.visible = false;
            baba5.visible = false;
            baba6.visible = false;
        }

        function showRandBaba(){
            var randBaba = randomNumber(1,6);

            switch(randBaba) {
                case 1:
                    hideBaba();
                    baba1.visible = true;
                    baba1.animations.getAnimation('baba1').play().onComplete.add(function(){
                        baba1.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 2:
                    hideBaba();
                    baba2.visible = true;
                    baba2.animations.getAnimation('baba2').play().onComplete.add(function(){
                        baba2.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 3:
                    hideBaba();
                    baba3.visible = true;
                    baba3.animations.getAnimation('baba3').play().onComplete.add(function(){
                        baba3.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 4:
                    hideBaba();
                    baba4.visible = true;
                    baba4.animations.getAnimation('baba4').play().onComplete.add(function(){
                        baba4.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 5:
                    hideBaba();
                    baba5.visible = true;
                    baba5.animations.getAnimation('baba5').play().onComplete.add(function(){
                        baba5.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 6:
                    hideBaba();
                    baba6.visible = true;
                    baba6.animations.getAnimation('baba6').play().onComplete.add(function(){
                        baba6.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }


        baba1 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba1');
        baba1.animations.add('baba1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5, false);
        baba1.visible = false;

        baba2 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba2');
        baba2.animations.add('baba2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba2.visible = false;

        baba3 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba3');
        baba3.animations.add('baba3', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5, false);
        baba3.visible = false;

        baba4 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba4');
        baba4.animations.add('baba4', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba4.visible = false;

        baba5 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba5');
        baba5.animations.add('baba5', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba5.visible = false;

        baba6 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba6');
        baba6.animations.add('baba6', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5, false);
        baba6.visible = false;

        
        showRandBaba();

        cat = game.add.sprite(221-mobileX,374-mobileY, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557-mobileX,390-mobileY, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });
                

        takeOrRisk = game.add.sprite(265-mobileX,358-mobileY, 'game.takeOrRisk');

        createLevelButtons();
    };

    game3.update = function () {

    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

function randomBox() {
    //добавление массивов изображений
    var arr = [];

    var blin = ['blin2','openBlin'];
    var chiсken = ['chiсken','openChiсken'];
    var fish = ['fish','openFish'];
    var pie = ['pie','openPie'];
    var pig = ['pig','openPig'];
    var porridge = ['porridge','openPorridge'];
    var smoke = ['smoke','openSmoke'];
    

    arr.push(blin,chiсken,fish,pie,pig,porridge,smoke);
    //arr.push(smoke);

    var rand = Math.floor(Math.random() * arr.length);

    return arr[rand];
}

function showBabaLegs(x,y){
    babaLegs = game.add.sprite(x,y, 'game.babaLegs');
}

function hideBaba(){
    baba7.visible = false;
    baba8.visible = false;
    baba9.visible = false;
    baba10.visible = false;
    baba11.visible = false;
    baba12.visible = false;

    baba7.animations.stop();
    baba8.animations.stop();
    baba9.animations.stop();
    baba10.animations.stop();
    baba11.animations.stop();
    baba12.animations.stop();
}

function showRandBaba(){
    var randBaba = randomNumber(7,10);

    switch(randBaba) {
        case 7:
            hideBaba();
            baba7.visible = true;
            baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                baba7.animations.stop();
                showRandBaba();
            });
            break;
        case 8:
            hideBaba();
            baba8.visible = true;
            baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                baba8.animations.stop();
                showRandBaba();
            });
            break;
        case 9:
            hideBaba();
            baba9.visible = true;
            baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                baba9.animations.stop();
                showRandBaba();
            });
            break;
        case 10:
            hideBaba();
            baba10.visible = true;
            baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                baba10.animations.stop();
                showRandBaba();
            });
            break;
        case 12:
            hideBaba();
            baba12.visible = true;
            baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                baba12.animations.stop();
                showRandBaba();
            });
            break;

    }
}

function createBaba(x,y){
    baba7 = game.add.sprite(x,y, 'game.baba7');
    baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
    baba7.visible = false;

    baba8 = game.add.sprite(x,y, 'game.baba8');
    baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
    baba8.visible = false;

    baba9 = game.add.sprite(x,y, 'game.baba9');
    baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
    baba9.visible = false;

    baba10 = game.add.sprite(x,y, 'game.baba10');
    baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
    baba10.visible = false;

    baba11 = game.add.sprite(x,y, 'game.baba11');
    baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
    baba11.visible = false;

    baba12 = game.add.sprite(x,y, 'game.baba12');
    baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
    baba12.visible = false;
}

function playAnim(x,y){

    var mobileX = 93;
    var mobileY = 22;

    hideBaba();
    babaLegs.visible = false;

    var box = randomBox();

    babaOpenBox1.play();
    setTimeout("babaOpenBox2.play();",1000);

    if(box[0] == 'smoke'){
        box1 = game.add.sprite(143+x-93,150+y-22, 'game.' + box[1]);
        box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

        box1.animations.getAnimation('box1').play().onComplete.add(function () {
            
            babaOpenBadBox.play();

            box1n = game.add.sprite(143+x-93,150+y-22, 'game.' + box[0]);
            box1n.animations.add('box1n', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 5, false);
            box1n.animations.getAnimation('box1n').play().onComplete.add(function(){
                hidedButtonArray = [];
                game.state.start('game1');
            });
        });

        babaOpenBox = game.add.sprite(122+x-93,182+y-22, 'game.babaOpenBox');         
        babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
        babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
            babaOpenBox.visible = false;

            cat2.visible = false;
            catSad = game.add.sprite(271-93,70-22, 'game.catSad');
            catSad.animations.add('catSad', [0,1], 5, true);
            catSad.animations.getAnimation('catSad').play();

            //showBabaLegs(137,452);
            babaLoseStick = game.add.sprite(122+x-93,292+y-22, 'game.babaLoseStick');
            babaLoseStick.animations.add('babaLoseStick', [0,1,2,3,4,5,6], 5, false);
            babaLoseStick.animations.getAnimation('babaLoseStick').play().onComplete.add(function(){
                babaLoseStick.visible = false;

                babSad = game.add.sprite(122+x-93,293+y-22, 'game.babSad');
                babSad.animations.add('babSad', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 5, false);
                babSad.animations.getAnimation('babSad').play().onComplete.add(function(){
                    hidedButtonArray = [];
                    game.state.start('game1');
                });
                babaSadLags = game.add.sprite(137+x-93,452+y-22, 'game.babaSadLags');
            });

            
            
        });
    } else {
        box1 = game.add.sprite(143+x-93,182+y-22, 'game.' + box[1]);
        box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

        box1.animations.getAnimation('box1').play().onComplete.add(function () {
            if(box[0] != 'blin2'){
                setTimeout("babaOpenBox3.play();",0);
                box1n = game.add.sprite(143+x-93,182+y-22, 'game.' + box[0]);
                box1n.animations.add('box1n', [0,1,2], 5, true);
                box1n.animations.getAnimation('box1n').play();
            } else {
                setTimeout("babaOpenBox3.play();",0);
                box1n = game.add.sprite(143+x-93,182+y-22, 'game.' + box[0]);
                box1n.animations.add('box1n', [0,1,2,3,4,5,6,7], 5, false);
                box1n.animations.getAnimation('box1n').play();
            }
        });

        babaOpenBox = game.add.sprite(122+x-93,182+y-22, 'game.babaOpenBox');         
        babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
        babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
            babaOpenBox.visible = false;

            showBabaLegs(137+x-93,452+y-22);
            baba12 = game.add.sprite(122+x-93,292+y-22, 'game.baba12');
            baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                baba12.visible = false;

                createBaba(122+x-93,292+y-22);
                showRandBaba();
            });

            
            
        });
    }
}

var hidedButtonArray = [];


(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {

    	var mobileX = 93;
    	var mobileY = 22;

        babaOpenBox1 = game.add.sound('game.babaOpenBox1');
        babaOpenBox2 = game.add.sound('game.babaOpenBox2');
        babaOpenBox3 = game.add.sound('game.babaOpenBox3');
        babaOpenBadBox = game.add.sound('game.babaOpenBadBox');

        //Добавление фона
        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.totalBet');
        backgroundGame4 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame4');
        showBabaLegs(350-mobileX,465-mobileY);

        pechka1 =  game.add.sprite(143-mobileX,182-mobileY,'game.pechka');
        pechka1.inputEnabled = true;
        pechka1.input.useHandCursor = true;
        pechka1.events.onInputDown.add(function(){
        
            playAnim(0,0);
            
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
        });

        pechka2 =  game.add.sprite(255-mobileX,198-mobileY,'game.pechka');
        pechka2.inputEnabled = true;
        pechka2.input.useHandCursor = true;
        pechka2.events.onInputDown.add(function(){

            playAnim(112,16);
            
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
        });

        pechka3 =  game.add.sprite(367-mobileX,182-mobileY,'game.pechka');
        pechka3.inputEnabled = true;
        pechka3.input.useHandCursor = true;
        pechka3.events.onInputDown.add(function(){

            playAnim(112*2,0);
            
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
        });

        pechka4 =  game.add.sprite(479-mobileX,198-mobileY,'game.pechka');
        pechka4.inputEnabled = true;
        pechka4.input.useHandCursor = true;
        pechka4.events.onInputDown.add(function(){

            playAnim(112*3,16);
            
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
        });

        pechka5 =  game.add.sprite(591-mobileX,182-mobileY,'game.pechka');
        pechka5.inputEnabled = true;
        pechka5.input.useHandCursor = true;
        pechka5.events.onInputDown.add(function(){
            playAnim(112*4,0);
            
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
        });

        //создание анимаций

        cat2 = game.add.sprite(271-mobileX,70-mobileY, 'game.cat2');
        cat2.animations.add('cat2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50], 5, true);
        cat2.animations.getAnimation('cat2').play();

        

        createBaba(334-mobileX,306-mobileY);         
        
        showRandBaba();

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        createLevelButtons();
    };

    game4.update = function () {

    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {

        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.load.image('device', 'landscape.png');

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        if(!isMobile) {
            game.load.image('start', 'start.png');
            game.load.image('start_p', 'start_p.png');
            game.load.image('start_d', 'start_d.png');
        } else {
            game.load.image('start', 'spin.png');
            game.load.image('start_p', 'spin_p.png');
            game.load.image('start_d', 'spin_d.png');
        }

        game.load.image('bet1', 'bet1.png');
        game.load.image('bet1_p', 'bet1_p.png');
        game.load.image('home', 'home.png');
        game.load.image('home_p', 'home_p.png');
        game.load.image('dollar', 'dollar.png');
        game.load.image('gear', 'gear.png');
        game.load.image('double', 'double.png');

        game.load.image('game.start', 'spin.png');
        game.load.image('game.start_p', 'spin_p.png');
        game.load.image('game.start_d', 'spin_d.png');
        
        game.load.image('game.bet1', 'bet1.png');
        game.load.image('game.bet1_p', 'bet1_p.png');
        game.load.image('game.home', 'home.png');
        game.load.image('game.home_p', 'home_p.png');
        game.load.image('game.dollar', 'dollar.png');
        game.load.image('game.gear', 'gear.png');
        game.load.image('game.double', 'double.png');

        game.load.image('game.background', 'img/canvas-bg.svg'); 
        game.load.image('game.background1', 'img/background1.png'); 
        game.load.image('game.totalBet', 'img/totalBet.png');
        
              
        game.load.image('game.start', 'img/image1445.png');
        game.load.image('game.start_p', 'img/image1447.png');
        game.load.image('game.start_d', 'img/image1451.png');

        game.load.image('game.selectGame', 'img/image1419.png');
        game.load.image('game.selectGame_p', 'img/image1421.png');
        game.load.image('game.selectGame_d', 'img/image1425.png');
        game.load.image('game.payTable', 'img/image1428.png');
        game.load.image('game.payTable_p', 'img/image1430.png');
        game.load.image('game.payTable_d', 'img/image1433.png');
        game.load.image('game.automaricstart', 'img/image1436.png');
        game.load.image('game.automaricstart_p', 'img/image1438.png');
        game.load.image('game.automaricstart_d', 'img/image1442.png');
        game.load.image('game.betone', 'img/image1471.png');
        game.load.image('game.betone_p', 'img/image1473.png');
        game.load.image('game.betone_d', 'img/image1477.png');
        game.load.image('game.line1', 'img/image1505.png');
        game.load.image('game.line1_p', 'img/image1507.png');
        game.load.image('game.line1_d', 'img/image1511.png');
        game.load.image('game.betmax', 'img/image1480.png');
        game.load.image('game.betmax_p', 'img/image1482.png');
        game.load.image('game.betmax_d', 'img/image1485.png');
        game.load.image('game.line3', 'img/image1496.png');
        game.load.image('game.line3_p', 'img/image1498.png');
        game.load.image('game.line3_d', 'img/image1502.png');
        game.load.image('game.line5', 'img/image1488.png');
        game.load.image('game.line5_p', 'img/image1490.png');
        game.load.image('game.line5_d', 'img/image1493.png');
        game.load.image('game.line7', 'img/image1462.png');
        game.load.image('game.line7_p', 'img/image1464.png');
        game.load.image('game.line7_d', 'img/image1468.png');
        game.load.image('game.line9', 'img/image1454.png');
        game.load.image('game.line9_p', 'img/image1456.png');
        game.load.image('game.line9_d', 'img/image1459.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.bar', 'img/bars.png');
        game.load.image('game.bar_move', 'img/bar_move.png');
        
        //карты
        game.load.image('game.cards_bg', 'img/shape107.png');
        game.load.image('game.card_garage', 'img/shape107.png');

        game.load.image('game.card_2b', 'img/shape109.png');
        game.load.image('game.card_3b', 'img/shape111.png');
        game.load.image('game.card_4b', 'img/shape113.png');
        game.load.image('game.card_5b', 'img/shape115.png');
        game.load.image('game.card_6b', 'img/shape117.png');
        game.load.image('game.card_7b', 'img/shape119.png');
        game.load.image('game.card_8b', 'img/shape121.png');
        game.load.image('game.card_9b', 'img/shape123.png');
        game.load.image('game.card_10b', 'img/shape125.png');
        game.load.image('game.card_jb', 'img/shape127.png');
        game.load.image('game.card_qb', 'img/shape129.png');
        game.load.image('game.card_kb', 'img/shape131.png');
        game.load.image('game.card_ab', 'img/shape133.png');

        game.load.image('game.card_2ch', 'img/shape135.png');
        game.load.image('game.card_3ch', 'img/shape137.png');
        game.load.image('game.card_4ch', 'img/shape139.png');
        game.load.image('game.card_5ch', 'img/shape141.png');
        game.load.image('game.card_6ch', 'img/shape143.png');
        game.load.image('game.card_7ch', 'img/shape145.png');
        game.load.image('game.card_8ch', 'img/shape147.png');
        game.load.image('game.card_9ch', 'img/shape149.png');
        game.load.image('game.card_10ch', 'img/shape151.png');
        game.load.image('game.card_jch', 'img/shape153.png');
        game.load.image('game.card_qch', 'img/shape155.png');
        game.load.image('game.card_kch', 'img/shape157.png');
        game.load.image('game.card_ach', 'img/shape159.png');

        game.load.image('game.card_2k', 'img/shape161.png');
        game.load.image('game.card_3k', 'img/shape163.png');
        game.load.image('game.card_4k', 'img/shape165.png');
        game.load.image('game.card_5k', 'img/shape167.png');
        game.load.image('game.card_6k', 'img/shape169.png');
        game.load.image('game.card_7k', 'img/shape171.png');
        game.load.image('game.card_8k', 'img/shape173.png');
        game.load.image('game.card_9k', 'img/shape175.png');
        game.load.image('game.card_10k', 'img/shape177.png');
        game.load.image('game.card_jk', 'img/shape179.png');
        game.load.image('game.card_qk', 'img/shape181.png');
        game.load.image('game.card_kk', 'img/shape183.png');
        game.load.image('game.card_ak', 'img/shape185.png');

        game.load.image('game.card_2p', 'img/shape187.png');
        game.load.image('game.card_3p', 'img/shape189.png');
        game.load.image('game.card_4p', 'img/shape191.png');
        game.load.image('game.card_5p', 'img/shape193.png');
        game.load.image('game.card_6p', 'img/shape195.png');
        game.load.image('game.card_7p', 'img/shape197.png');
        game.load.image('game.card_8p', 'img/shape199.png');
        game.load.image('game.card_9p', 'img/shape201.png');
        game.load.image('game.card_10p', 'img/shape203.png');
        game.load.image('game.card_jp', 'img/shape205.png');
        game.load.image('game.card_qp', 'img/shape207.png');
        game.load.image('game.card_kp', 'img/shape209.png');
        game.load.image('game.card_ap', 'img/shape211.png');
        game.load.image('game.card_joker', 'img/shape213.png');

        game.load.image('game.pick', 'img/shape221.png');
        
        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');
        for (var i = 1; i <= 6; ++i) {
            game.load.image('pagePaytable_' + i, 'img/page_' + i + '.png');            
        }

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');

        for (var i = 1; i <= 9; ++i) {

            if(i==5 || i==7 || i==9 || i==8){
                game.load.image('line_' + i, 'lines/select/' + i + '.png');
            } else {    
                game.load.image('line_' + i, 'lines/select/' + i + '.png');
            }

            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('bet', 'bet.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');
        game.load.audio('takeWin', 'takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');
        game.load.audio('game.babaOpenBox1', 'sound/babaOpenBox1.mp3');
        game.load.audio('game.babaOpenBox2', 'sound/babaOpenBox2.mp3');
        game.load.audio('game.babaOpenBox3', 'sound/babaOpenBox3.mp3');
        game.load.audio('game.babaOpenBadBox', 'sound/babaOpenBadBox.mp3');
        game.load.audio('game.babaLoseFromWolf', 'sound/lose.mp3');
        
        


        game.load.spritesheet('game.baba1', 'img/baba1.png', 96, 176);
        game.load.spritesheet('game.baba2', 'img/baba2.png', 96, 176);
        game.load.spritesheet('game.baba3', 'img/baba3.png', 96, 176);
        game.load.spritesheet('game.baba4', 'img/baba4.png', 96, 176);
        game.load.spritesheet('game.baba5', 'img/baba5.png', 96, 176);
        game.load.spritesheet('game.baba6', 'img/baba6.png', 96, 176);

        game.load.spritesheet('game.baba7', 'img/baba7.png', 128, 160);
        game.load.spritesheet('game.baba8', 'img/baba8.png', 128, 160);
        game.load.spritesheet('game.baba9', 'img/baba9.png', 128, 160);
        game.load.spritesheet('game.baba10', 'img/baba10.png', 128, 160);
        game.load.spritesheet('game.baba11', 'img/baba11.png', 128, 160);
        game.load.spritesheet('game.baba12', 'img/baba12.png', 112, 160);

        game.load.spritesheet('game.cat', 'img/cat.png', 48, 64);
        game.load.spritesheet('game.testo', 'img/testo.png', 144, 64);

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.blin', 'img/blin.png', 96, 96);
        game.load.spritesheet('game.pech', 'img/pech.png', 96, 96);
        
        game.load.image('game.backgroundGame3', 'img/shape104.png');
        
        game.load.audio('game.openCardAudio', 'sound/sound31.mp3');
        game.load.audio('game.winPech', 'sound/winPech.mp3');
        

        game.load.image('game.backgroundGame4', 'img/shape1545.png');
        game.load.image('game.babaLegs', 'img/shape1191.png');
        game.load.spritesheet('game.cat2', 'img/cat2.png', 80, 80);
        
        game.load.spritesheet('game.openPie', 'img/openPie.png', 96, 112);
        game.load.spritesheet('game.openPorridge', 'img/openPorridge.png', 96, 112);
        game.load.spritesheet('game.openBlin', 'img/openBlin.png', 96, 112);
        game.load.spritesheet('game.openChiсken', 'img/openChiсken.png', 96, 112);
        game.load.spritesheet('game.openFish', 'img/openFish.png', 96, 112);
        game.load.spritesheet('game.openPig', 'img/openPig.png', 96, 112);
        game.load.spritesheet('game.openSmoke', 'img/openSmoke.png', 96, 144);

        game.load.spritesheet('game.blin2', 'img/blin2.png', 96, 112);
        game.load.spritesheet('game.chiсken', 'img/chiсken.png', 96, 112);
        game.load.spritesheet('game.fish', 'img/fish.png', 96, 112);
        game.load.spritesheet('game.pie', 'img/pie.png', 96, 112);
        game.load.spritesheet('game.pig', 'img/pig.png', 96, 112);
        game.load.spritesheet('game.porridge', 'img/porridge.png', 96, 112);
        game.load.spritesheet('game.smoke', 'img/smoke.png', 96, 144);

        game.load.image('game.babaLegsFromOpenBox', 'img/babaLegsFromOpenBox.svg');
        game.load.spritesheet('game.babaOpenBox', 'img/babaOpenBox.png', 128, 300);
        game.load.spritesheet('game.babaLoseStick', 'img/babaLoseStick.png', 224, 192);
        game.load.spritesheet('game.babSad', 'img/babSad.png', 128, 160);
        game.load.image('game.babaSadLags', 'img/babaSadLags.png');
        game.load.spritesheet('game.catSad', 'img/catSad.png', 80, 80);

        game.load.image('game.backgroundForGame2', 'img/shape1189.png');
        game.load.image('game.titleForGame2', 'img/shape1399.png');
        game.load.spritesheet('game.arrow1', 'img/arrow1.png', 32, 32);
        game.load.spritesheet('game.arrow2', 'img/arrow2.png', 32, 32);
        game.load.spritesheet('game.boogiman', 'img/boogiman.png', 96, 80);
        game.load.spritesheet('game.babaStrike1_wolf', 'img/babaStrike1_wolf.png', 208, 224);
        game.load.spritesheet('game.babaStrike2_wolf', 'img/babaStrike2_wolf.png', 208, 192);
        game.load.spritesheet('game.upWolf', 'img/upWolf.png', 160, 448);
        game.load.spritesheet('game.babaAndFolf11', 'img/babaAndFolf11.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf12', 'img/babaAndFolf12.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf21', 'img/babaAndFolf21.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf22', 'img/babaAndFolf22.png', 500, 471);
        game.load.spritesheet('game.babaTakeKeks1', 'img/babaTakeKeks1.png', 400, 300);
        game.load.spritesheet('game.babaTakeKeks2', 'img/babaTakeKeks2.png', 400, 300);

        game.load.image('game.pechka', 'img/shape1666.png');

        game.load.image('game.takeOrRisk', 'img/takeOrRisk.png');
        
    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();