function randomNumber(min, max) {
	return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var isMobile = false;
var mobileX = null;
var mobileY = null;
if (navigator.userAgent.match(/Android/i)
	|| navigator.userAgent.match(/webOS/i)
	|| navigator.userAgent.match(/iPhone/i)
	|| navigator.userAgent.match(/iPad/i)
	|| navigator.userAgent.match(/iPod/i)
	|| navigator.userAgent.match(/BlackBerry/i)
	|| navigator.userAgent.match(/Windows Phone/i)
	) {
	isMobile = true;
mobileX = -94;
mobileY = -22;
}
var width = 829.7;
var height = 598.95;

if(isMobile) {
	width = 640;
	height = 480;
}
var game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');
var flickerInterval = '';
var fullStatus = false;
var soundStatus = true;
function createLevelButtons() {
	var lvl1 = game.add.sprite(0,0, 'x');
	lvl1.inputEnabled = true;
	lvl1.input.useHandCursor = true;
	lvl1.events.onInputUp.add(function () {
		game.state.start('game1');
	}, this);

	var lvl2 = game.add.sprite(30, 0, 'x');
	lvl2.inputEnabled = true;
	lvl2.input.useHandCursor = true;
	lvl2.events.onInputUp.add(function () {
		game.state.start('game2');        
		game2.ropePositionX = 257;
	}, this);

	var lvl3 = game.add.sprite(60, 0, 'x');
	lvl3.inputEnabled = true;
	lvl3.input.useHandCursor = true;
	lvl3.events.onInputUp.add(function () {
		game.state.start('game3');
		game3.freeze = false;
	}, this);

	var lvl4 = game.add.sprite(90, 0, 'x');
	lvl4.inputEnabled = true;
	lvl4.input.useHandCursor = true;
	lvl4.events.onInputUp.add(function () {
		game.state.start('game4');        
		game4.freeze = false;
	}, this);
}
function full_and_sound(){
	if (!fullStatus)
		full = game.add.sprite(740+mobileX,30+mobileY, 'non_full');
	else
		full = game.add.sprite(740+mobileX,30+mobileY, 'full');
	full.inputEnabled = true;
	full.input.useHandCursor = true;
	full.events.onInputUp.add(function(){
		if (fullStatus == false){
			full.loadTexture('full');
			fullStatus = true;
			if(document.documentElement.requestFullScreen) {
				document.documentElement.requestFullScreen();
			} else if(document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if(document.documentElement.webkitRequestFullScreen) {
				document.documentElement.webkitRequestFullScreen();
			}
		} else {
			full.loadTexture('non_full');
			fullStatus = false;
			if(document.cancelFullScreen) {
				document.cancelFullScreen();
			} else if(document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if(document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			}
		}
	});
	if (soundStatus)
		sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_on');
	else
		sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_off');
	sound.inputEnabled = true;
	sound.input.useHandCursor = true;
	sound.events.onInputUp.add(function(){
		if (soundStatus == true){
			sound.loadTexture('sound_off');
			soundStatus =false;
			game.sound.mute = true;
		} else {
			sound.loadTexture('sound_on');
			soundStatus = true;
			game.sound.mute = false;
		}
	});
}
function selectLine(n) {
	game1.currentLine = n;

	for (var i = 1; i <= 9; ++i) {
		game1.lines[i].sprite.visible = false;
		game1.lines[i].number.visible = false;
	}
	for (var i = 1; i <= n; ++i) {
		game1.lines[i].sprite.visible = true;
		game1.lines[i].number.visible = true;
		game1.lines[i].sprite.loadTexture('line_' + i);
		if (i === 5)
			game1.lines[5].sprite.position.y = 111+mobileY;
		if (i === 7)
			game1.lines[7].sprite.position.y = 261+mobileY;

	}
};
function preselectLine(n) {
	for (var i = 1; i <= 9; ++i) {
		game1.lines[i].sprite.visible = false;
		game1.lines[i].number.visible = false;
	}
	for (var i = 1; i <= n; ++i) {
		game1.lines[i].sprite.loadTexture('linefull_' + i);        
		game1.lines[i].sprite.visible = true;
		game1.lines[i].number.visible = true;
		if (i === 5)
			game1.lines[5].sprite.position.y = 96+mobileY;
		if (i === 7)
			game1.lines[7].sprite.position.y = 230+mobileY;

	}
    // game1.lines[5].sprite.rotation = 1;
  }
  function disableLinesBtn(){
  	for (var i = 1; i <= 9; ++i) {
  		if (i % 2 != 0) {
  			game1.lines[i].button.loadTexture('btnline_d' + i);
  		}
  	}
  };
  function unDisableLines(){
  	for (var i = 1; i <= 9; ++i) {
  		if (i % 2 != 0) {
  			game1.lines[i].button.loadTexture('btnline' + i);            
  		}
  	}    
  };
  function winLine(n) {
  	game1.lines[n].sprite.loadTexture('linefull_' + n);
  	game1.lines[n].sprite.visible = true;
  	winNumber(n);
  };
  function winNumber(i){
  	flickerInterval = setInterval(              
  		function(){
  			if (game1.takeWin){                        
  				game1.lines[i].number.visible = false;
  			} 
  			unflicker(i);
  		}, 1000);
  };
  function unflicker(i){
  	setTimeout(function() {
  		game1.lines[i].number.visible = true;
  	}, 500);
  }
  function shuffle(arr) {
  	return arr.sort(function() {return 0.5 - Math.random()});
  }
//локация 1
var game1 = {
	bars: [],
	currentLine : 9,
	spinStatus : false,
	barsCurrentSpins : [0, 0, 0, 0, 0],
	barsTotalSpins : [15, 27, 39, 51, 63 ],
	countPlayBars : 0,
	takeWin : false,
	pages : [],
	settingsMode : false,
	currentPage : null,
	lines : {
		1: {
			coord: 197,
			sprite: null,
			btncoord : 250,
			button : null,
			number : null,
			numbercoord : 182
		},
		2: {
			coord: 70,
			sprite: null,
			number : null,
			numbercoord :54
		},
		3: {
			coord: 325,
			sprite: null,
			btncoord : 295,
			button : null,
			number : null,
			numbercoord : 310
		},
		4: {
			coord: 134,
			sprite: null,
			number : null,
			numbercoord : 114
		},
		5: {
			coord: 50,
			sprite: null,
			btncoord : 340,
			button : null,
			number : null,
			numbercoord : 246
		},
		6: {
			coord: 102,
			sprite: null,
			number : null,
			numbercoord :84
		},
		7: {
			coord: 318,
			sprite: null,
			btncoord : 385,
			button : null,
			number : null,
			numbercoord : 278
		},
		8: {
			coord: 226,
			sprite: null,
			number : null,
			numbercoord : 214
		},
		9: {
			coord: 122,
			sprite: null,
			btncoord : 430,
			button : null,
			number : null,
			numbercoord : 150
		},

	},
	create:function(){
		game1.bars[0] = game.add.tileSprite(142+mobileX, 54+mobileY, 96, 288, 'game1.bar');
		game1.bars[0].tilePosition.y =  randomNumber(0,8)*96 ;       
		game1.bars[1] = game.add.tileSprite(254+mobileX, 54+mobileY, 96, 288, 'game1.bar');
		game1.bars[1].tilePosition.y =  randomNumber(0,8)*96;
		game1.bars[2] = game.add.tileSprite(366+mobileX , 54+mobileY, 96, 288, 'game1.bar');
		game1.bars[2].tilePosition.y =  randomNumber(0,8)*96;
		game1.bars[3] = game.add.tileSprite(478+mobileX, 54+mobileY, 96, 288, 'game1.bar');
		game1.bars[3].tilePosition.y =  randomNumber(0,8)*96;        
		game1.bars[4] = game.add.tileSprite(590+mobileX, 54+mobileY, 96, 288, 'game1.bar');
		game1.bars[4].tilePosition.y =  randomNumber(0,8)*96;	
		game1.gold_row_win_1 = game.add.sprite(142+mobileX, 54+mobileY, 'gold_row_win');
		game1.gold_row_win_1Animation = game1.gold_row_win_1.animations.add('gold_row_win', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 10, false);
		game1.gold_row_win_1.visible = false;
		game1.gold_row_win_2 = game.add.sprite(254+mobileX, 54+mobileY, 'gold_row_win');
		game1.gold_row_win_2Animation = game1.gold_row_win_2.animations.add('gold_row_win', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 10, false);
		game1.gold_row_win_2.visible = false;
		game1.gold_row_win_3 = game.add.sprite(366+mobileX, 54+mobileY, 'gold_row_win');
		game1.gold_row_win_3Animation = game1.gold_row_win_3.animations.add('gold_row_win', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 10, false);
		game1.gold_row_win_3.visible = false;
		game1.bg = game.add.sprite(94+mobileX,54+mobileY, 'game1.bg');
		game1.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game1.bg_top1');
		
		game1.fireplace = game.add.sprite(318+mobileX, 390+mobileY, 'fireplace');
		game1.fireplaceAnimation = game1.fireplace.animations.add('fireplace', [0,1,2,3], 6, true);
		game1.fireplace.visible = true;   		
		game1.gear2 = game.add.sprite(478+mobileX, 406+mobileY, 'gear2');
		game1.gear2Animation = game1.gear2.animations.add('gear2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, true);
		game1.gear2.visible = false;   		
		game1.gnome_anim1_1 = game.add.sprite(94+mobileX, 326+mobileY, 'gnome_anim1_1'); 
		if (!isMobile){
			game1.gnome_anim1_1Animation = game1.gnome_anim1_1.animations.add('gnome_anim1_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
			game1.gnome_anim1_1Animation.play();
			game1.gnome_anim1_2 = game.add.sprite(94+mobileX, 326+mobileY, 'gnome_anim1_2'); 
			game1.gnome_anim1_2Animation = game1.gnome_anim1_2.animations.add('gnome_anim1_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
			game1.gnome_anim1_2.visible = false;
			game1.gnome_anim1_1Animation.onComplete.add(function(){
				game1.gnome_anim1_2Animation.play();
				game1.gnome_anim1_1.visible = false;
				game1.gnome_anim1_2.visible = true;
			});
			game1.gnome_anim1_2Animation.onComplete.add(function(){
				game1.gnome_anim1_1Animation.play();
				game1.gnome_anim1_1.visible = true;
				game1.gnome_anim1_2.visible = false;
			});
		};
		for (var i = 1; i <= 9; ++i) {
			game1.lines[i].sprite = game.add.sprite(134+mobileX, game1.lines[i].coord+mobileY, 'line_' + i);
			game1.lines[i].sprite.visible = false;
			game1.lines[i].number = game.add.sprite(94+mobileX, game1.lines[i].numbercoord+mobileY, 'win_' + i);
			game1.lines[i].number.visible = false;
		}
		for (var i = 1; i <= 5; ++i) {
			game1.page = game.add.sprite(94+mobileX, 22+mobileY, 'game1.page_' + i);
			game1.page.visible = false;
			game1.pages[i] = game1.page;
		}
		game1.light_settings = game.add.sprite(685+mobileX, 22+mobileY, 'light_settings');
		game1.light_settingsAnim = game1.light_settings.animations.add('light_settings', [0,1,2,3], 5, true);		
		game1.light_settings.visible = false;
		if (isMobile)
			game1.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
		prev_page = game.add.sprite(115+mobileX, 441+mobileY, 'prev_page');
		prev_page.visible = false;
		prev_page.inputEnabled = true;
		prev_page.input.useHandCursor = true;
		prev_page.events.onInputUp.add(function(){
			if (game1.settingsMode)  {
				pageSound.play();
				if (game1.currentPage == 1)
					game1.currentPage = 5;
				else{
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage -=1;
				}
			}
			game1.pages[game1.currentPage].visible = true;
		});
		exit_btn = game.add.sprite(324+mobileX, 441+mobileY, 'exit_btn');
		exit_btn.visible = false;
		exit_btn.inputEnabled = true;
		exit_btn.input.useHandCursor = true;
		exit_btn.events.onInputUp.add(function(){
			pageSound.play();
			for (var i = 1; i <= 5; ++i) {
				game1.pages[i].visible = false;
			}            
			prev_page.visible = false;
			exit_btn.visible = false;
			next_page.visible = false;
			game1.light_settings.visible = false;
			game1.settingsMode = false;
			if (!isMobile){
				unDisableLines()
				game1.automatic_start.loadTexture('automatic_start');
				game1.bet_max.loadTexture('bet_max');
				game1.bet_one.loadTexture('bet_one');
				game1.paytable.loadTexture('paytable');
				game1.select_game.loadTexture('select_game');
			} else {
				doubleb.visible = true;
				dollar.visible = true;
				home.visible = true;
				button.visible = true;
				bet1.visible = true;
				gear.visible = true;
			}
		});
		next_page = game.add.sprite(510+mobileX, 441+mobileY, 'next_page');
		next_page.visible = false;
		next_page.inputEnabled = true;
		next_page.input.useHandCursor = true;
		next_page.events.onInputUp.add(function(){
			if (game1.settingsMode)  {
				pageSound.play();
				if (game1.currentPage == 5){
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage = 1;
				} else if (game1.currentPage == 1){
					game1.currentPage +=1;
				} else {                    
					game1.pages[game1.currentPage].visible = false;
					game1.currentPage +=1;
				}
			}
			game1.pages[game1.currentPage].visible = true;
		});
		if(!isMobile){
			game.add.sprite(0,0, 'main_window');
			full_and_sound();
		}
		createLevelButtons();
		if(!isMobile){
			for (var i = 1; i <= 9; ++i) {
				if (i % 2 != 0) {
					game1.lines[i].sound = game.add.audio('line' + i);
					game1.lines[i].button = game.add.sprite(game1.lines[i].btncoord+mobileX, 510+mobileY, 'btnline' + i);
					game1.lines[i].button.scale.setTo(0.7, 0.7);
					game1.lines[i].button.inputEnabled = true;
					game1.lines[i].button.input.useHandCursor = true;
					(function (n) {
						game1.lines[n].button.events.onInputOver.add(function(){
							if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
								return;
							game1.lines[n].button.loadTexture('btnline_p' + n);
						});
						game1.lines[n].button.events.onInputOut.add(function(){
							if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
								return;
							game1.lines[n].button.loadTexture('btnline' + n);
						});
						game1.lines[n].button.events.onInputUp.add(function () {
							if(game1.spinStatus || game1.takeWin || game1.settingsMode)
								return;
							hideLines();
							selectLine(n);
							game1.lines[n].sound.play();
						}, this);
					})(i);
				}
			}            
		}
		if (isMobile){
			button = game.add.sprite(544, 188, 'spin');
			button.inputEnabled = true;
			button.input.useHandCursor = true;

			bet1 = game.add.sprite(548, 274, 'bet1');
			bet1.inputEnabled = true;
			bet1.input.useHandCursor = true;
			bet1.events.onInputDown.add(function(){
				bet1.loadTexture('bet1_p');
			});
			bet1.events.onInputUp.add(function(){
				document.getElementById('betMode').style.display = 'block';
				widthVisibleZone = $('.betWrapper .visibleZone').height();
				$('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
				$('canvas').css('display', 'none');
				bet1.loadTexture('bet1');
			});
			home = game.add.sprite(3, 3, 'home');
			home.inputEnabled = true;
			home.input.useHandCursor = true;
			home.events.onInputDown.add(function(){
				home.loadTexture('home_p');
			});
			home.events.onInputUp.add(function(){
				home.loadTexture('home');
			});
			dollar = game.add.sprite(435, 3, 'dollar');
			dollar.inputEnabled = true;
			dollar.input.useHandCursor = true;
			dollar.events.onInputDown.add(function(){
			});

			gear = game.add.sprite(539, 3, 'gear');
			gear.inputEnabled = true;
			gear.input.useHandCursor = true;
			gear.events.onInputDown.add(function(){
				game1.pages[1].visible = true;
				prev_page.visible = true;
				exit_btn.visible = true;
				next_page.visible = true;
				game1.light_settings.visible = true;
				game1.light_settingsAnim.play();
				game1.settingsMode = true;
				game1.currentPage = 1;
				doubleb.visible = false;
				dollar.visible = false;
				home.visible = false;
				button.visible = false;
				bet1.visible = false;
				gear.visible = false;
			});

			doubleb = game.add.sprite(546, 133, 'double');
			doubleb.inputEnabled = true;
			doubleb.input.useHandCursor = true;
			doubleb.events.onInputDown.add(function(){
				game.state.start('game2');
				game2.ropePositionX = 257;
			});
			function buttonClicked() {
				if (game1.spinStatus) {
					return;
				}
				button.loadTexture('spin_p');
			}

			button.events.onInputDown.add(buttonClicked, this);
			button.events.onInputUp.add(btnStartUp, this);
		}
		if(!isMobile){
			game1.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
			game1.startButton.scale.setTo(0.7, 0.7);
			game1.startButton.inputEnabled = true;
			game1.startButton.input.useHandCursor = true;
			game1.startButton.events.onInputUp.add(btnStartUp, this);
			game1.startButton.events.onInputOver.add(function(){
				if(game1.spinStatus)
					return;
				game1.startButton.loadTexture('start_p');
			});
			game1.startButton.events.onInputOut.add(function(){
				if(game1.spinStatus)
					return;
				game1.startButton.loadTexture('start');
			});

			game1.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');        
			game1.automatic_start.scale.setTo(0.7, 0.7);
			game1.automatic_start.inputEnabled = true;
			game1.automatic_start.input.useHandCursor = true;
			game1.automatic_start.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.automatic_start.loadTexture('automatic_start_p');
			});
			game1.automatic_start.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.automatic_start.loadTexture('automatic_start');
			});
			game1.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
			game1.bet_max.scale.setTo(0.7, 0.7);
			game1.bet_max.inputEnabled = true;
			game1.bet_max.input.useHandCursor = true;
			game1.bet_max.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_max.loadTexture('bet_max_p');
			});
			game1.bet_max.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_max.loadTexture('bet_max');
			});
			game1.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
			game1.bet_one.scale.setTo(0.7, 0.7);
			game1.bet_one.inputEnabled = true;
			game1.bet_one.input.useHandCursor = true;
			game1.bet_one.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_one.loadTexture('bet_one_p');
			});
			game1.bet_one.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.bet_one.loadTexture('bet_one');
			});
			game1.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
			game1.paytable.scale.setTo(0.7, 0.7);
			game1.paytable.inputEnabled = true;
			game1.paytable.input.useHandCursor = true;
			game1.paytable.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.paytable.loadTexture('paytable_p');
			});
			game1.paytable.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.paytable.loadTexture('paytable');
			});
			game1.paytable.events.onInputUp.add(function(){
				game1.pages[1].visible = true;
				prev_page.visible = true;
				exit_btn.visible = true;
				next_page.visible = true;				
				game1.light_settings.visible = true;
				game1.light_settingsAnim.play();
				game1.settingsMode = true;
				game1.currentPage = 1;
				game1.automatic_start.loadTexture('automatic_start_d');
				game1.bet_max.loadTexture('bet_max_d');
				game1.bet_one.loadTexture('bet_one_d');
				game1.paytable.loadTexture('paytable_d');
				game1.select_game.loadTexture('select_game_d');
				game1.lines[3].button.loadTexture('btnline_d' + 3);
				game1.lines[5].button.loadTexture('btnline_d' + 5);
				game1.lines[7].button.loadTexture('btnline_d' + 7);
			}, this);
			game1.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
			game1.select_game.scale.setTo(0.7, 0.7);
			game1.select_game.inputEnabled = true;
			game1.select_game.input.useHandCursor = true;
			game1.select_game.events.onInputOver.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.select_game.loadTexture('select_game_p');
			});
			game1.select_game.events.onInputOut.add(function(){
				if(game1.spinStatus || game1.settingsMode)
					return;
				game1.select_game.loadTexture('select_game');
			});
		}
		stopSound = game.add.audio('stop');
		rotateSound = game.add.audio('rotate');
		tadaSound = game.add.audio('tada');
		pageSound = game.add.audio('page');
		safeWinSound = game.add.audio('safeWin');
		takeWinSound = game.add.audio('takeWin');
		takeWinSound.addMarker('take', 0, 0.6);
		rotateSound.loop = true;

		game1.fireplaceAnimation.play();
		if(!isMobile){
			game1.lines[1].button.events.onInputUp.add(function () {
				if (game1.settingsMode)  {
					pageSound.play();
					if (game1.currentPage == 1)
						game1.currentPage = 5;
					else{
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage -=1;
					}
					game1.pages[game1.currentPage].visible = true;
				}
			});      
			game1.lines[9].button.events.onInputUp.add(function () {
				if (game1.settingsMode)  {
					pageSound.play();
					if (game1.currentPage == 5){
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage = 1;
					} else if (game1.currentPage == 1){
						game1.currentPage +=1;
					} else {                    
						game1.pages[game1.currentPage].visible = false;
						game1.currentPage +=1;
					}
					game1.pages[game1.currentPage].visible = true;
				}
			});
			game1.lines[1].button.events.onInputOver.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[1].button.loadTexture('btnline_p' + 1);
			});
			game1.lines[1].button.events.onInputOut.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[1].button.loadTexture('btnline' + 1);
			});
			game1.lines[9].button.events.onInputOver.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[9].button.loadTexture('btnline_p' + 9);
			});
			game1.lines[9].button.events.onInputOut.add(function(){
				if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
					game1.lines[9].button.loadTexture('btnline' + 9);
			});
		}
		function btnStartUp(){
			if(game1.spinStatus)
				return;
			if (game1.settingsMode){
				pageSound.play();
				for (var i = 1; i <= 5; ++i) {
					game1.pages[i].visible = false;
				}            
				prev_page.visible = false;
				exit_btn.visible = false;
				next_page.visible = false;
				game1.light_settings.visible = false;
				game1.settingsMode = false;
				unDisableLines()
				game1.automatic_start.loadTexture('automatic_start');
				game1.bet_max.loadTexture('bet_max');
				game1.bet_one.loadTexture('bet_one');
				game1.paytable.loadTexture('paytable');
				game1.select_game.loadTexture('select_game');
			} else{
				if (game1.takeWin){
					game1.takeWin = false;
					unDisableLines();
					selectLine(game1.currentLine);
					clearInterval(flickerInterval);
					takeWinSound.play('take');
				} else{
					if (!isMobile){
						game1.startButton.loadTexture('start_d');
						game1.automatic_start.loadTexture('automatic_start_d');
						game1.bet_max.loadTexture('bet_max_d');
						game1.bet_one.loadTexture('bet_one_d');
						game1.paytable.loadTexture('paytable_d');
						game1.select_game.loadTexture('select_game_d');
						disableLinesBtn();
					} else   
					button.loadTexture('spin');
					game1.gear2Animation.play();
					game1.gear2.visible = true;
					game1.countPlayBars = 5;
					game1.barsCurrentSpins = [0, 0, 0, 0, 0];
					game1.spinStatus = true;
					hideLines();
					rotateSound.play();
				}
			}
		};
		function hideLines() {
			for (var i = 1; i <= 9; ++i) {
				game1.lines[i].sprite.visible = false;
			}
		}   
		game1.gold_row_win_1Animation.onComplete.add(function(){
			game.state.start('game3');
		});
		preselectLine(game1.currentLine);     
	},
	update:function(){
		if (game1.spinStatus){
			for (var i in game1.bars) {
				game1.barsCurrentSpins[i]++;
				if (game1.barsCurrentSpins[i] < game1.barsTotalSpins[i]) {
					game1.bars[i].tilePosition.y += 96;
				} else if (game1.barsCurrentSpins[i] == game1.barsTotalSpins[i]) {
					game1.countPlayBars--;
					stopSound.play();
				}
			}
			if (game1.countPlayBars === 0){
				game1.spinStatus = false;
				rotateSound.stop();
				game1.gear2.visible = false;
				if (!isMobile){   
					game1.startButton.loadTexture('start');
					game1.automatic_start.loadTexture('automatic_start');
					game1.bet_max.loadTexture('bet_max');
					game1.bet_one.loadTexture('bet_one');
					game1.paytable.loadTexture('paytable');
					game1.select_game.loadTexture('select_game');
				}
				if(game1.currentLine == 3)   {
					game1.takeWin = true;
					winLine(3);
					tadaSound.play();
				} else if (game1.currentLine == 5){
					safeWinSound.play();
					game1.gold_row_win_1Animation.play();
					game1.gold_row_win_1.visible = true;
					game1.gold_row_win_2Animation.play();
					game1.gold_row_win_2.visible = true;
					game1.gold_row_win_3Animation.play();
					game1.gold_row_win_3.visible = true;
				} else {
					if(!isMobile){  
						unDisableLines();
					}
					selectLine(game1.currentLine);   
				}
			}
		}
	}
};
game.state.add('game1', game1); 

var game2 = {
	cards : [],
	ropePositionX : 257,
	cardValues : {1:5, 2:7, 3:12, 4:13, 5:15},
    // cardValues : {1:'5b', 2:'7c', 3:'12p', 4:'13t', 5:'15'},
    cardIndexes : null,
    create : function() {
    	mobileY = -54;
    	game2.bg = game.add.sprite(94+mobileX,54+mobileY, 'game1.bg');
    	game2.cardIndexes = shuffle(Object.keys(game2.cardValues));
    	game2.dealer_card = game.add.sprite(124+mobileX, 118+mobileY, 'card_'+game2.cardValues[game2.cardIndexes[0]]);
    	game2.bg2 = game.add.sprite(94+mobileX,54+mobileY, 'game2.bg');
    	game2.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
    	if(!isMobile){
    		game.add.sprite(0,0, 'main_window');
    	}
    	pickCardSound = game.add.audio('pickCard');
    	cardWin = game.add.audio('cardWin');
    	game2.win = game.add.sprite(240+mobileX,358+mobileY, 'game2.win');
    	game2.win.visible = false;
    	game2.lose = game.add.sprite(240+mobileX,358+mobileY, 'game2.lose');
    	game2.lose.visible = false;
    	game2.forward = game.add.sprite(240+mobileX,358+mobileY, 'game2.forward');
    	game2.forward.visible = false;
    	for(var i=1; i<=4; ++i) {
    		game2.cards[i] = game.add.sprite(game2.ropePositionX+mobileX, 118+mobileY, 'shirt_cards');
    		game2.ropePositionX += 112;
    		if(isMobile){
    			game2.cards[i].inputEnabled = true;
    			game2.cards[i].input.useHandCursor = true;
    		}
    	}
    	game2.gnome_anim1_1 = game.add.sprite(94+mobileX, 326+mobileY, 'gnome_anim1_1'); 
    	if (!isMobile){
    		game2.gnome_anim1_1Animation = game2.gnome_anim1_1.animations.add('gnome_anim1_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
    		game2.gnome_anim1_1Animation.play();
    		game2.gnome_anim1_2 = game.add.sprite(94+mobileX, 326+mobileY, 'gnome_anim1_2'); 
    		game2.gnome_anim1_2Animation = game2.gnome_anim1_2.animations.add('gnome_anim1_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
    		game2.gnome_anim1_2.visible = false;
    		game2.gnome_anim1_1Animation.onComplete.add(function(){
    			game2.gnome_anim1_2Animation.play();
    			game2.gnome_anim1_1.visible = false;
    			game2.gnome_anim1_2.visible = true;
    		});
    		game2.gnome_anim1_2Animation.onComplete.add(function(){
    			game2.gnome_anim1_1Animation.play();
    			game2.gnome_anim1_1.visible = true;
    			game2.gnome_anim1_2.visible = false;
    		});
    	};
    	game1.fireplace = game.add.sprite(318+mobileX, 390+mobileY, 'fireplace');
    	game1.fireplaceAnimation = game1.fireplace.animations.add('fireplace', [0,1,2,3], 6, true);
    	game1.fireplace.visible = true; 
    	game1.fireplaceAnimation.play();
    	var buttonsPositionsX = {1:{pos:295,i:3}, 2:{pos:340,i:5}, 3:{pos:385,i:7}, 4:{pos:430,i:9}};
    	var buttons = {};
        // var cardIndexes = {1 : '2b', 2: '2c', 3: '2p', 4: '2t'};
        if(!isMobile)
        	game.add.sprite(250+mobileX, 510+mobileY, 'btnline_d1').scale.setTo(0.7, 0.7);;
        for(var i in buttonsPositionsX) {
        	if(!isMobile){
        		var button = game.add.sprite(buttonsPositionsX[i].pos+mobileX, 510+mobileY, 'btnline' + buttonsPositionsX[i].i);
        		button.scale.setTo(0.7, 0.7);
        		button.inputEnabled = true;
        		button.input.useHandCursor = true;
        		(function(n, btn, pic){
        			btn.events.onInputUp.add(function () {
        				var card = game2.cards[n];
        				card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
        				game.add.sprite(card.position.x+13+mobileX, 282+mobileY, 'pick_card');
        				pickCardSound.play();
        				var dealerValue = game2.cardValues[game2.cardIndexes[0]];
        				var userValue = game2.cardValues[game2.cardIndexes[n]];
        				console.log('dealer ' + dealerValue);
        				console.log('user ' + userValue);
        				if(dealerValue < userValue) {                            
        					game2.win.visible = true;
        					cardWin.play();
        				} else if(dealerValue == userValue) {
        					game2.forward.visible = true;
        				} else {
        					game2.lose.visible = true;
        				}

        				for(var c in game2.cards) {
        					game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
        				}
        				for(var b in buttons) {
        					buttons[b].loadTexture('btnline_d' + b);
        					buttons[b].inputEnabled = false;
        				}


        			}, this);
        		})(i, button, buttonsPositionsX[i].i);
        	} else {
        		(function(n, btn, pic){
        			btn.events.onInputUp.add(function () {
        				var card = game2.cards[n];
        				card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
        				game.add.sprite(card.position.x+13, 282+mobileY, 'pick_card');        				
        				pickCardSound.play();
        				var dealerValue = game2.cardValues[game2.cardIndexes[0]];
        				var userValue = game2.cardValues[game2.cardIndexes[n]];
        				console.log('dealer ' + dealerValue);
        				console.log('user ' + userValue);
        				if(dealerValue < userValue) {                            
        					game2.win.visible = true;
        					cardWin.play();
        				} else if(dealerValue == userValue) {
        					game2.forward.visible = true;
        				} else {
        					game2.lose.visible = true;
        				}

        				for(var c in game2.cards) {
        					game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
        				}
        				for(var b in game2.cards) {
        					game2.cards[b].inputEnabled = false;
        				}                        

        			}, this);
        		})(i, game2.cards[i], buttonsPositionsX[i].i)
        	}


        	if (!isMobile){
        		buttons[buttonsPositionsX[i].i] = button;
        	} 
        }   
        if (isMobile)
        	game2.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');     
        if(!isMobile){
        	game2.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
        	game2.startButton.scale.setTo(0.7, 0.7);
        	game2.startButton.inputEnabled = true;
        	game2.startButton.input.useHandCursor = true;
        	game2.startButton.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.startButton.loadTexture('start_p');
        	});
        	game2.startButton.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.startButton.loadTexture('start');
        	});
        	game2.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
        	game2.automatic_start.scale.setTo(0.7, 0.7);
        	game2.automatic_start.inputEnabled = true;
        	game2.automatic_start.input.useHandCursor = true;
        	game2.automatic_start.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.automatic_start.loadTexture('automatic_start_p');
        	});
        	game2.automatic_start.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.automatic_start.loadTexture('automatic_start');
        	});
        	game2.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
        	game2.bet_max.scale.setTo(0.7, 0.7);
        	game2.bet_max.inputEnabled = true;
        	game2.bet_max.input.useHandCursor = true;
        	game2.bet_max.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_max.loadTexture('bet_max_p');
        	});
        	game2.bet_max.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_max.loadTexture('bet_max');
        	});
        	game2.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
        	game2.bet_one.scale.setTo(0.7, 0.7);
        	game2.bet_one.inputEnabled = true;
        	game2.bet_one.input.useHandCursor = true;
        	game2.bet_one.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_one.loadTexture('bet_one_p');
        	});
        	game2.bet_one.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.bet_one.loadTexture('bet_one');
        	});
        	game2.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
        	game2.paytable.scale.setTo(0.7, 0.7);
        	game2.paytable.inputEnabled = true;
        	game2.paytable.input.useHandCursor = true;
        	game2.paytable.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.paytable.loadTexture('paytable_p');
        	});
        	game2.paytable.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.paytable.loadTexture('paytable');
        	});
        	game2.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
        	game2.select_game.scale.setTo(0.7, 0.7);
        	game2.select_game.inputEnabled = true;
        	game2.select_game.input.useHandCursor = true;
        	game2.select_game.events.onInputOver.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.select_game.loadTexture('select_game_p');
        	});
        	game2.select_game.events.onInputOut.add(function(){
        		if(game2.spinStatus)
        			return;
        		game2.select_game.loadTexture('select_game');
        	});
        }
        createLevelButtons();
        if(!isMobile)
        	full_and_sound();
      }

    };
    game.state.add('game2', game2);
    var game3 = {
    	freeze : false,
    	buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    	goldCount : 5,
    	gnomePositionsX : {1:62, 3:190, 5:318, 7:446, 9:574},
    	selectedSafeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
    	closedDoor : [1,3,5,7,9],
    	selectedGold : null,
    	safe_hit_arr : [],
    	safe_arr : [],
    	safe_door_arr : [],
    	safe_door_arr_anim : [],
    	lever_arr : [],
    	lever_p_arr : [],
    	chest_open_arr : [],

    	create : function() {
    		mobileY = -54;
    		game3.bg = game.add.sprite(94+mobileX,54+mobileY, 'game3.bg');
    		game3.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
    		hitSound = game.add.audio('hit');
    		hitSound.addMarker('hits', 0.3, 5);     
    		winSound = game.add.audio('winGame3');
    		game3_1Sound = game.add.audio('game3_1');
    		game3_2Sound = game.add.audio('game3_2');
    		game3_loseSound = game.add.audio('game3_lose');
    		game3_win_2Sound = game.add.audio('game3_win_2');
    		for(var i in game3.gnomePositionsX) {
    			game3.lever_arr[i] = game.add.sprite(94+mobileX+((i-1)/2)*128, 390+mobileY, 'game3.lever'+i);
    			game3.lever_arr[i].visible = false;
    			if(isMobile){
    				game3.lever_p_arr[i] = game.add.sprite(94+mobileX+((i-1)/2)*128, 390+mobileY, 'game3.lever_p'+i);
    				game3.lever_p_arr[i].visible = true;
    				game3.lever_p_arr[i].inputEnabled = true;
    				game3.lever_p_arr[i].input.useHandCursor = true;
    			}
    			game3.chest_open_arr[i] = game.add.sprite(94+mobileX+((i-1)/2)*128, 54+mobileY, 'game3.chest_open');
    			game3.chest_open_arr[i].visible = false;
    		}    		
    		game3.gnome_left = game.add.sprite(304+mobileX, 326+mobileY, 'game3.gnome_left');
    		game3.gnome_leftAnimation = game3.gnome_left.animations.add('game3.gnome_left', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 8, false);
    		game3.gnome_leftAnimation.play();    	
    		game3.gnome_right = game.add.sprite(304+mobileX, 310+mobileY, 'game3.gnome_right');
    		game3.gnome_rightAnimation = game3.gnome_right.animations.add('game3.gnome_right', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,17,17,17], 8, false);
    		game3.gnome_right.visible = false;
    		game3.gnome_leftAnimation.onComplete.add(function(){
    			game3.gnome_left.visible = false;
    			game3.gnome_right.visible = true;
    			game3.gnome_rightAnimation.play();
    		});
    		game3.gnome_rightAnimation.onComplete.add(function(){
    			game3.gnome_right.visible = false;
    			game3.gnome_left.visible = true;
    			game3.gnome_leftAnimation.play();
    		});
    		game3.action1 = game.add.sprite(94+mobileX, 326+mobileY, 'game3.action1');
    		game3.action1Animation = game3.action1.animations.add('game3.action1', [0,1,2,3,4,5,6,7,8,9,10,11], 14, false);
    		game3.action1.visible = false;  
    		game3.chest_action1 = game.add.sprite(94+mobileX, 54+mobileY, 'game3.chest_action1');
    		game3.chest_action1Animation = game3.chest_action1.animations.add('game3.chest_action1', [0,1,2,3,4,5,6,7,8], 12, false);
    		game3.chest_action1.visible = false;
    		game3.chest_win = game.add.sprite(94+mobileX, 54+mobileY, 'game3.chest_win');
    		game3.chest_winAnimation = game3.chest_win.animations.add('game3.chest_win', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 20, false);
    		game3.chest_win.visible = false;    	
    		game3.gnome_win = game.add.sprite(62+mobileX, 265+mobileY, 'game3.gnome_win');
    		game3.gnome_winAnimation = game3.gnome_win.animations.add('game3.gnome_win', [0,1,2,3,4,5,6,7,8,9,10], 14, false);
    		game3.gnome_win.visible = false;
    		game3.gnome_win2 = game.add.sprite(62+mobileX, 326+mobileY, 'game3.gnome_win2');
    		game3.gnome_win2Animation = game3.gnome_win2.animations.add('game3.gnome_win2', [0,1,2], 12, false);
    		game3.gnome_win2.visible = false;
    		game3.chest_lose = game.add.sprite(94+mobileX, 54+mobileY, 'game3.chest_lose');
    		game3.chest_loseAnimation = game3.chest_lose.animations.add('game3.chest_lose', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 12, false);
    		game3.chest_lose.visible = false;
    		game3.chest_lose2 = game.add.sprite(94+mobileX, 54+mobileY, 'game3.chest_lose');
    		game3.chest_lose2Animation = game3.chest_lose2.animations.add('game3.chest_lose', [15,16,17,18,19,20,21,22], 12, false);
    		game3.chest_lose2.visible = false;
    		game3.gnome_lose = game.add.sprite(62+mobileX, 166+mobileY, 'game3.gnome_lose');
    		game3.gnome_loseAnimation = game3.gnome_lose.animations.add('game3.gnome_lose', [0,1,2,3,4,5,6,7,8,9], 12, false);
    		game3.gnome_lose.visible = false;    	  
    		game3.gnome_lose2 = game.add.sprite(62+mobileX, 310+mobileY, 'game3.gnome_lose2');
    		game3.gnome_lose2Animation = game3.gnome_lose2.animations.add('game3.gnome_lose2', [0,1,2,3,4,5,6,7,8,9,6,7,8,9], 12, false);
    		game3.gnome_lose2.visible = false;    		
    		game3.action1Animation.onComplete.add(function(){
    			game3.chest_action1.position.x = 94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.chest_action1.visible = true;
    			game3.chest_action1Animation.play();
    		});
    		game3.chest_action1Animation.onComplete.add(function(){
    			game3_2Sound.play();
    			game3.chest_action1.visible = false;
    			game3.chest_open_arr[game3.selectedGold].visible = true;
    			if(game3.selectedGold == 5){
    				game3.chest_lose.visible = true;
    				game3.chest_lose.position.x = 94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    				game3.chest_loseAnimation.play();
    			} else {
    				game3.chest_win.visible = true;
    				game3.chest_win.position.x = 94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    				game3.chest_winAnimation.play();
    			}
    		});   
    		game3.chest_loseAnimation.onComplete.add(function(){    			    			
    			game3.chest_lose.visible = false;
    			game3.chest_lose2.visible = true;
    			game3.gnome_lose.visible = true;
    			game3.gnome_lose.position.x = 94-32 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.chest_lose2.position.x = 94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.chest_lose2Animation.play();
    			game3.gnome_loseAnimation.play();
    			setTimeout(function(){
    				game3_loseSound.play();
    			},300);
    		});
    		game3.gnome_loseAnimation.onComplete.add(function(){
    			game3.gnome_lose.visible = false;
    			game3.gnome_lose2.visible = true;
    			game3.gnome_lose2.position.x = 94-32 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.gnome_lose2Animation.play();
    		});
    		game3.gnome_lose2Animation.onComplete.add(function(){
    			game.state.start('game1');
    		});
    		game3.chest_winAnimation.onComplete.add(function(){    			
    			
    			game3.chest_win.visible = false;
    			game3.action1.visible = false;
    			game3.gnome_win.visible = true;
    			game3.gnome_win.position.x = -32+94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.gnome_winAnimation.play();
    			setTimeout(function(){
    				game3_win_2Sound.play();
    			},150);
    		});
    		game3.gnome_winAnimation.onComplete.add(function(){
    			game3.gnome_win.visible = false;
    			game3.gnome_win2.visible = true;
    			game3.gnome_win2.position.x = -32+94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.gnome_win2Animation.play();
    		});
    		game3.gnome_win2Animation.onComplete.add(function(){
    			game3.freeze = false;
    			game3.gnome_win2.visible = false;
    			game3.gnome_left.visible = true;
    			game3.gnome_left.position.x = -32+94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.gnome_right.position.x = -32+94 +mobileX+ ((game3.selectedGold-1)/2)*128;
    			game3.gnome_leftAnimation.play();
    		});
    		if(!isMobile){
    			game.add.sprite(0,0, 'main_window');
    		}
    		for(var i in game3.gnomePositionsX) {
    			if (!isMobile){
    				var button = game.add.sprite(game3.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
    				button.scale.setTo(0.7, 0.7);
    				button.inputEnabled = true;
    				button.input.useHandCursor = true;     
    			}   
    			if (!isMobile){      
    				(function(n, btn){
    					btn.events.onInputUp.add(function () {
    						if(game3.freeze) {
    							return;
    						}
    						game3.freeze = true;
    						game3.goldCount--;
    						btn.loadTexture('btnline_d' + n);
    						btn.inputEnabled = false;
    						game3.action1.position.x = game3.gnomePositionsX[n]+mobileX;
    						game3.gnome_left.visible = false;
    						game3.gnome_right.visible = false;
    						game3.action1.visible = true; 
    						game3.action1Animation.play();
    						game3.selectedGold = n;
    						game3.lever_arr[n].visible = true;
    						game3_1Sound.play();
    					}, this);
    				})(i, button);
    			} else {
    				(function(n, btn){
    					btn.events.onInputUp.add(function () {
    						if(game3.freeze) {
    							return;
    						}
    						game3.freeze = true;
    						game3.goldCount--;
    						btn.inputEnabled = false;
    						btn.visible = false;
    						game3.action1.position.x = game3.gnomePositionsX[n]+mobileX;
    						game3.gnome_left.visible = false;
    						game3.gnome_right.visible = false;
    						game3.action1.visible = true; 
    						game3.action1Animation.play();
    						game3.selectedGold = n;    						
    						game3.lever_arr[n].visible = true;
    						game3.lever_p_arr[n].visible = false;
    						game3_1Sound.play();
    					}, this);
    				})(i, game3.lever_p_arr[i]);
    			}              
    		}
    		if (isMobile)
    			game3.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
    		if(!isMobile){
    			game3.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
    			game3.startButton.scale.setTo(0.7, 0.7);
    			game3.startButton.inputEnabled = true;
    			game3.startButton.input.useHandCursor = true;
    			game3.startButton.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.startButton.loadTexture('start_p');
    			});
    			game3.startButton.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.startButton.loadTexture('start');
    			});
    			game3.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
    			game3.automatic_start.scale.setTo(0.7, 0.7);
    			game3.automatic_start.inputEnabled = true;
    			game3.automatic_start.input.useHandCursor = true;
    			game3.automatic_start.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.automatic_start.loadTexture('automatic_start_p');
    			});
    			game3.automatic_start.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.automatic_start.loadTexture('automatic_start');
    			});
    			game3.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
    			game3.bet_max.scale.setTo(0.7, 0.7);
    			game3.bet_max.inputEnabled = true;
    			game3.bet_max.input.useHandCursor = true;
    			game3.bet_max.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_max.loadTexture('bet_max_p');
    			});
    			game3.bet_max.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_max.loadTexture('bet_max');
    			});
    			game3.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
    			game3.bet_one.scale.setTo(0.7, 0.7);
    			game3.bet_one.inputEnabled = true;
    			game3.bet_one.input.useHandCursor = true;
    			game3.bet_one.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_one.loadTexture('bet_one_p');
    			});
    			game3.bet_one.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.bet_one.loadTexture('bet_one');
    			});
    			game3.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
    			game3.paytable.scale.setTo(0.7, 0.7);
    			game3.paytable.inputEnabled = true;
    			game3.paytable.input.useHandCursor = true;
    			game3.paytable.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.paytable.loadTexture('paytable_p');
    			});
    			game3.paytable.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.paytable.loadTexture('paytable');
    			});
    			game3.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
    			game3.select_game.scale.setTo(0.7, 0.7);
    			game3.select_game.inputEnabled = true;
    			game3.select_game.input.useHandCursor = true;
    			game3.select_game.events.onInputOver.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.select_game.loadTexture('select_game_p');
    			});
    			game3.select_game.events.onInputOut.add(function(){
    				if(game3.spinStatus)
    					return;
    				game3.select_game.loadTexture('select_game');
    			});
    		}
    		createLevelButtons();
    		if(!isMobile)
    			full_and_sound();
    	}

    };
    game.state.add('game3', game3);

    var game4 = {
    	buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    	selectedDoor : null,
    	freeze : false,
    	create : function() {
    		mobileY = -54;
    		game4.bg = game.add.sprite(94+mobileX,54+mobileY, 'game4.bg');
    		game4.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');

    		game4_winSound = game.add.audio('game4_win');
    		game4_winSound.addMarker('win', 0.2, 5); 
    		game4_loseSound = game.add.audio('game4_lose');  
    		game4.gnome_1 = game.add.sprite(366+mobileX, 327+mobileY, 'game4.gnome_1');
    		game4.gnome_1Animation = game4.gnome_1.animations.add('game4.gnome_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
    		game4.gnome_1Animation.play();     
    		game4.gnome_2 = game.add.sprite(366+mobileX, 327+mobileY, 'game4.gnome_2');
    		game4.gnome_2Animation = game4.gnome_2.animations.add('game4.gnome_2', [0,1,2,3,4,5,6,7,8,9], 8, false);       		
    		game4.gnome_2.visible = false;
    		game4.gnome_1Animation.onComplete.add(function(){
    			game4.gnome_2Animation.play();
    			game4.gnome_1.visible = false;
    			game4.gnome_2.visible = true;
    		});
    		game4.gnome_2Animation.onComplete.add(function(){
    			game4.gnome_1Animation.play();
    			game4.gnome_1.visible = true;
    			game4.gnome_2.visible = false;
    		});		
    		if(isMobile){
    			game4.chest_right = game.add.sprite(526+mobileX, 198+mobileY, 'game4.chest_right');
    			game4.chest_right.inputEnabled = true;
    			game4.chest_right.input.useHandCursor = true;
    			game4.chest_left = game.add.sprite(270+mobileX, 198+mobileY, 'game4.chest_left');
    			game4.chest_left.inputEnabled = true;
    			game4.chest_left.input.useHandCursor = true;
    		}
    		game4.chest_win = game.add.sprite(526+mobileX, 198+mobileY, 'game4.chest_win');
    		game4.chest_winAnimation = game4.chest_win.animations.add('game4.chest_win', [0,1,2,3,4,5], 8, false);
    		game4.chest_win.visible = false;    	
    		game4.chest_lose = game.add.sprite(270+mobileX, 198+mobileY, 'game4.chest_lose');
    		game4.chest_loseAnimation = game4.chest_lose.animations.add('game4.chest_lose', [0,1,2,3,4,5], 8, false);
    		game4.chest_lose.visible = false;
    		game4.chest_open = game.add.sprite(270+mobileX, 198+mobileY, 'game4.chest_open');
    		game4.chest_open.visible = false;    		
    		game4.chest_open2 = game.add.sprite(270+mobileX, 198+mobileY, 'game4.chest_open2');
    		game4.chest_open2.visible = false;
    		game4.gnome_win2 = game.add.sprite(415+mobileX, 232+mobileY, 'game4.gnome_win2');
    		game4.gnome_win2.visible = false;
    		game4.gold = game.add.sprite(544+mobileX, 264+mobileY, 'game4.gold');
    		game4.gold.visible = false;    
    		game4.gnome_key = game.add.sprite(159+mobileX, 232+mobileY, 'game4.gnome_key');
    		game4.gnome_keyAnimation = game4.gnome_key.animations.add('game4.gnome_key', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 8, false);
    		game4.gnome_key.visible = false;    	
    		game4.gnome_win = game.add.sprite(415+mobileX, 123+mobileY, 'game4.gnome_win');
    		game4.gnome_winAnimation = game4.gnome_win.animations.add('game4.gnome_win', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 8, false);
    		game4.gnome_win.visible = false;    
    		game4.gnome_win3 = game.add.sprite(415+mobileX, 232+mobileY, 'game4.gnome_win3');
    		game4.gnome_win3Animation = game4.gnome_win3.animations.add('game4.gnome_win3', [0,1,2,3,4,4,4,4,4,4,4,4,4], 8, false);
    		game4.gnome_win3.visible = false;     
    		game4.gnome_lose = game.add.sprite(159+mobileX, 233+mobileY, 'game4.gnome_lose');
    		game4.gnome_loseAnimation = game4.gnome_lose.animations.add('game4.gnome_lose', [0,1,2,3,4], 8, false);
    		game4.gnome_lose.visible = false; 
    		game4.gnome_lose2 = game.add.sprite(143+mobileX, 233+mobileY, 'game4.gnome_lose');
    		game4.gnome_lose2Animation = game4.gnome_lose2.animations.add('game4.gnome_lose', [5,5,5,5,5,5], 8, false);
    		game4.gnome_lose2.visible = false; 
    		game4.gnome_keyAnimation.onComplete.add(function(){
    			if (game4.selectedDoor == 9) {
    				game4.chest_win.visible = true; 
    				game4.chest_winAnimation.play();    
    			} else {
    				game4.chest_lose.visible = true; 
    				game4.chest_loseAnimation.play();  
    			}
    		});
    		game4.chest_winAnimation.onComplete.add(function(){
    			game4.gnome_winAnimation.play();
    			game4.gnome_win.visible = true; 
    			game4.gnome_key.visible = false;
    			game4.chest_open.position.x = 526+mobileX;
    			game4.chest_open.visible = true;
    		});
    		game4.gnome_winAnimation.onComplete.add(function(){
    			game4.gnome_win.visible = false; 
    			game4.gnome_win2.visible = true;
    			game4.gold.visible = true;
    			setTimeout(function() {
    				game4.gnome_win3.visible = true; 
    				game4.gnome_win3Animation.play();
    				game4.gnome_win2.visible = false;
    			}, 100);    			
    		});
    		game4.gnome_win3Animation.onComplete.add(function(){
    			game.state.start('game1');
    		});
    		game4.chest_loseAnimation.onComplete.add(function(){   
    			game4.gnome_key.visible = false;
    			game4.chest_open2.visible = true;
    			game4.gnome_loseAnimation.play();
    			game4.gnome_lose.visible = true; 
    		});
    		game4.gnome_loseAnimation.onComplete.add(function(){  
    			game4.gnome_lose.visible = false; 
    			game4.gnome_lose2Animation.play();
    			game4.gnome_lose2.visible = true; 
    		});    		
    		game4.gnome_lose2Animation.onComplete.add(function(){
    			game.state.start('game1');
    		});
    		if(!isMobile){
    			game.add.sprite(0,0, 'main_window');
    		}
    		if(!isMobile){
    			game4.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
    			game4.startButton.scale.setTo(0.7, 0.7);
    			game4.startButton.inputEnabled = true;
    			game4.startButton.input.useHandCursor = true;
    			game4.startButton.events.onInputOver.add(function(){
    				game4.startButton.loadTexture('start_p');
    			});
    			game4.startButton.events.onInputOut.add(function(){
    				game4.startButton.loadTexture('start');
    			});

    			game4.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
    			game4.automatic_start.scale.setTo(0.7, 0.7);
    			game4.automatic_start.inputEnabled = true;
    			game4.automatic_start.input.useHandCursor = true;
    			game4.automatic_start.events.onInputOver.add(function(){
    				game4.automatic_start.loadTexture('automatic_start_p');
    			});
    			game4.automatic_start.events.onInputOut.add(function(){
    				game4.automatic_start.loadTexture('automatic_start');
    			});
    			game4.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
    			game4.bet_max.scale.setTo(0.7, 0.7);
    			game4.bet_max.inputEnabled = true;
    			game4.bet_max.input.useHandCursor = true;
    			game4.bet_max.events.onInputOver.add(function(){
    				game4.bet_max.loadTexture('bet_max_p');
    			});
    			game4.bet_max.events.onInputOut.add(function(){
    				game4.bet_max.loadTexture('bet_max');
    			});
    			game4.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
    			game4.bet_one.scale.setTo(0.7, 0.7);
    			game4.bet_one.inputEnabled = true;
    			game4.bet_one.input.useHandCursor = true;
    			game4.bet_one.events.onInputOver.add(function(){
    				game4.bet_one.loadTexture('bet_one_p');
    			});
    			game4.bet_one.events.onInputOut.add(function(){
    				game4.bet_one.loadTexture('bet_one');
    			});
    			game4.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
    			game4.paytable.scale.setTo(0.7, 0.7);
    			game4.paytable.inputEnabled = true;
    			game4.paytable.input.useHandCursor = true;
    			game4.paytable.events.onInputOver.add(function(){
    				game4.paytable.loadTexture('paytable_p');
    			});
    			game4.paytable.events.onInputOut.add(function(){
    				game4.paytable.loadTexture('paytable');
    			});
    			game4.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
    			game4.select_game.scale.setTo(0.7, 0.7);
    			game4.select_game.inputEnabled = true;
    			game4.select_game.input.useHandCursor = true;
    			game4.select_game.events.onInputOver.add(function(){
    				game4.select_game.loadTexture('select_game_p');
    			});
    			game4.select_game.events.onInputOut.add(function(){
    				game4.select_game.loadTexture('select_game');
    			});
    		}
    		for(var i in game4.buttonsPositionsX) {
    			if(!isMobile){
    				var button = game.add.sprite(game4.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
    				button.scale.setTo(0.7, 0.7);
    				if (i == 3 || i == 5 || i == 7){
    					button = game.add.sprite(game4.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline_d' + i);
    					button.scale.setTo(0.7, 0.7);
    					button.inputEnabled = false;
    				} else {             
    					button.inputEnabled = true;
    					button.input.useHandCursor = true;                
    					(function(n, btn){
    						btn.events.onInputUp.add(function () {
    							if(game4.freeze || n==7 || n==3 || n==5) {
    								return;
    							}
    							game4.freeze = true;
    							btn.inputEnabled = false;
    							if (n == 1){
    								btn.loadTexture('btnline_d' + 1);                         
    							} else {      						
    								btn.loadTexture('btnline_d' + 9);
    								game4.gnome_key.position.x = 415+mobileX;  
    							}      					
    							game4.gnome_1.visible = false;
    							game4.gnome_2.visible = false;
    							game4.gnome_key.visible = true; 
    							game4.gnome_keyAnimation.play();
    							game4_winSound.play('win');
    							game4.selectedDoor = n;
    						}, this);
    					})(i, button);
    				}
    			} else {
    				game4.chest_left.events.onInputUp.add(function () {
    					if(game4.freeze) {
    						return;
    					}
    					game4.freeze = true;
    					game4.chest_left.inputEnabled = false;
    					game4.chest_left.visible = false;
    					game4.gnome_1.visible = false;
    					game4.gnome_2.visible = false;
    					game4.gnome_key.visible = true; 
    					game4.gnome_keyAnimation.play();
    					game4_winSound.play('win');
    					game4.selectedDoor = 1;
    				}, this);
    				game4.chest_right.events.onInputUp.add(function () {
    					if(game4.freeze) {
    						return;
    					}
    					game4.freeze = true;
    					game4.gnome_key.position.x = 415+mobileX;
    					game4.chest_left.inputEnabled = false;
    					game4.chest_left.visible = false;
    					game4.gnome_1.visible = false;
    					game4.gnome_2.visible = false;
    					game4.gnome_key.visible = true; 
    					game4.gnome_keyAnimation.play();
    					game4_winSound.play('win');
    					game4.selectedDoor = 9;
    				}, this);
    			}
    		}  
    		if (isMobile)
    			game4.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
    		createLevelButtons();
    		if(!isMobile)
    			full_and_sound();
    	}


    };
    game.state.add('game4', game4);

    var gamePreload = {

    	preload:function(){
    		if (isMobile){
    			game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
    				if(progress % 8 == 0) {
    					document.getElementById('preloaderBar').style.width = progress + '%';                    
    				}
    			});
    		}
    		game.scale.scaleMode = 2;
    		game.scale.pageAlignHorizontally = true;
    		game.scale.pageAlignVertically = true;
    		game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        // game.load.image('main_window', 'img/main_window.png');
        if(!isMobile){
        	game.load.image('main_window', 'img/shape1206.svg');
        }
        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');
        game.load.image('game3.bg', 'img/bg_game3.png');
        game.load.image('game4.bg', 'img/bg_game4.png');
        game.load.image('game1.bg_top1', 'img/main_bg_1.png');
        game.load.image('game2.bg_top1', 'img/main_bg_top1.png');
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('x', 'img/x.png');
        for (var i = 1; i <= 5; ++i) {
        	game.load.image('game1.page_' + i, 'img/page_' + i + '.png');            
        }

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');

        // game.load.image('game1.bar', 'img/shape_full.png');
        game.load.image('game1.bar', 'img/shape_fullv3.png');
        if(!isMobile){
        	game.load.image('start', 'img/btns/btn_start.png');
        	game.load.image('start_p', 'img/btns/btn_start_p.png');
        	game.load.image('start_d', 'img/btns/btn_start_d.png');
        	game.load.image('automatic_start', 'img/btns/btn_automatic_start.png');
        	game.load.image('automatic_start_p', 'img/btns/btn_automatic_start_p.png');
        	game.load.image('automatic_start_d', 'img/btns/btn_automatic_start_d.png');
        	game.load.image('bet_max', 'img/btns/btn_bet_max.png');
        	game.load.image('bet_max_p', 'img/btns/btn_bet_max_p.png');
        	game.load.image('bet_max_d', 'img/btns/btn_bet_max_d.png');
        	game.load.image('bet_one', 'img/btns/btn_bet_one.png');
        	game.load.image('bet_one_p', 'img/btns/btn_bet_one_p.png');
        	game.load.image('bet_one_d', 'img/btns/btn_bet_one_d.png');
        	game.load.image('paytable', 'img/btns/btn_paytable.png');
        	game.load.image('paytable_p', 'img/btns/btn_paytable_p.png');
        	game.load.image('paytable_d', 'img/btns/btn_paytable_d.png');
        	game.load.image('select_game', 'img/btns/btn_select_game.png');
        	game.load.image('select_gamev2', 'img/btns/btn_select_gamev2.png');
        	game.load.image('select_game_p', 'img/btns/btn_select_game_p.png');
        	game.load.image('select_game_d', 'img/btns/btn_select_game_d.png');
        	game.load.image('non_full', 'img/non_full.png');
        	game.load.image('full', 'img/full.png');
        	game.load.image('sound_on', 'img/sound_on.png');
        	game.load.image('sound_off', 'img/sound_off.png');
        }
        if(isMobile) {
        	game.load.image('spin', 'img/spin.png');
        	game.load.image('spin_p', 'img/spin_p.png');
        	game.load.image('spin_d', 'img/spin_d.png');
        	game.load.image('bet1', 'img/bet1.png');
        	game.load.image('bet1_p', 'img/bet1_p.png');
        	game.load.image('home', 'img/home.png');
        	game.load.image('home_p', 'img/home_p.png');
        	game.load.image('dollar', 'img/dollar.png');
        	game.load.image('gear', 'img/gear.png');
        	game.load.image('double', 'img/double.png');
        }
        game.load.image('shirt_cards', 'img/shirt_cards.png');
        game.load.image('pick_card', 'img/pick_game2.png');
        game.load.image('card_15', 'img/cards/joker.png');
        game.load.image('card_2b', 'img/cards/2b.png');
        game.load.image('card_5', 'img/cards/5b.png');
        game.load.image('card_2c', 'img/cards/2c.png');
        game.load.image('card_7', 'img/cards/7c.png');
        game.load.image('card_2p', 'img/cards/2p.png');
        game.load.image('card_12', 'img/cards/12p.png');
        game.load.image('card_2t', 'img/cards/2t.png');
        game.load.image('card_13', 'img/cards/13t.png');

        game.load.image('game2.win', 'img/win.png');
        game.load.image('game2.lose', 'img/lose.png');
        game.load.image('game2.forward', 'img/forward.png');

        game.load.image('game3.chest_open', 'img/game3_chest_open.png');
        game.load.spritesheet('game3.action1', 'img/game3_action1.png', 192, 176, 12);
        game.load.spritesheet('game3.gnome_left', 'img/game3_gnome_left.png', 160, 176, 23);
        game.load.spritesheet('game3.gnome_right', 'img/game3_gnome_right.png', 192, 192, 18);
        game.load.spritesheet('game3.chest_action1', 'img/game3_chest_action1.png', 128, 112, 9);
        game.load.spritesheet('game3.chest_win', 'img/game3_chest_win.png', 128, 240, 22);
        game.load.spritesheet('game3.gnome_win', 'img/game3_gnome_win.png', 160, 237, 11);
        game.load.spritesheet('game3.gnome_win2', 'img/game3_gnome_win2.png', 160, 176, 3);
        game.load.spritesheet('game3.chest_lose', 'img/game3_chest_lose.png', 128, 112, 23);
        game.load.spritesheet('game3.gnome_lose', 'img/game3_gnome_lose.png', 192, 336, 10);
        game.load.spritesheet('game3.gnome_lose2', 'img/game3_gnome_lose2.png', 192, 192, 10);
        for (var i = 1; i <= 9; ++i) {
        	if (i % 2 != 0) {           
        		game.load.image('game3.lever' + i, 'img/lever' + i + '.png');            
        		game.load.image('game3.lever_p' + i, 'img/lever_p' + i + '.png');            
        	}
        }

        game.load.image('game4.chest_open', 'img/game4_chest_open.png');
        game.load.image('game4.chest_open2', 'img/game4_chest_open2.png');
        game.load.image('game4.gold', 'img/game4_gold.png');
        game.load.image('game4.gnome_win2', 'img/game4_gnome_win2.png');
        game.load.image('game4.chest_left', 'img/chest_left.png');
        game.load.image('game4.chest_right', 'img/chest_right.png');
        game.load.spritesheet('game4.gnome_1', 'img/game4_gnome_1.png', 192, 176, 15);
        game.load.spritesheet('game4.gnome_2', 'img/game4_gnome_2.png', 192, 176, 10);
        game.load.spritesheet('game4.gnome_key', 'img/game4_gnome_key.png', 192, 176, 20);
        game.load.spritesheet('game4.chest_win', 'img/game4_chest_win.png', 128, 128, 6);
        game.load.spritesheet('game4.chest_lose', 'img/game4_chest_lose.png', 128, 128, 6);
        game.load.spritesheet('game4.gnome_win', 'img/game4_gnome_win.png', 208, 285, 15);
        game.load.spritesheet('game4.gnome_win3', 'img/game4_gnome_win3.png', 160, 176, 5);
        game.load.spritesheet('game4.gnome_lose', 'img/game4_gnome_lose.png', 208, 176, 6);

        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('pickCard', 'sounds/pickCard.mp3');
        game.load.audio('cardWin', 'sounds/cardWin.mp3');
        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('safeWin', 'sounds/safeWin.mp3');
        game.load.audio('codeDoor', 'sounds/codeDoor.mp3');
        game.load.audio('game4_win', 'sounds/game4_win.mp3');
        game.load.audio('game4_lose', 'sounds/game4_lose.mp3');
        game.load.audio('page', 'sounds/page.mp3');
        game.load.audio('game3_1', 'sounds/game3_1.mp3');
        game.load.audio('game3_2', 'sounds/game3_2.mp3');
        game.load.audio('game3_lose', 'sounds/game3_lose.mp3');
        game.load.audio('game3_win_2', 'sounds/game3_win_2.mp3');
        for (var i = 1; i <= 9; ++i) {
        	game.load.image('line_' + i, 'img/lines/select/line' + i + '.png');
        	game.load.image('linefull_' + i, 'img/lines/win/linefull' + i + '.png');
        	game.load.image('win_' + i, 'img/win_' + i + '.png');

        	if (i % 2 != 0) {
        		game.load.audio('line' + i, 'sounds/line' + i + '.wav');
        		if(!isMobile){
        			game.load.image('btnline' + i, 'img/btns/btn' + i + '.png');
        			game.load.image('btnline_p' + i, 'img/btns/btn' + i + '_p.png');
        			game.load.image('btnline_d' + i, 'img/btns/btn' + i + '_d.png');
        		}
        	}
        }

        game.load.spritesheet('light_settings', 'img/light_settings.png', 48, 112, 4);
        game.load.spritesheet('fireplace', 'img/fireplace.png', 128, 48, 4);
        game.load.spritesheet('gear2', 'img/gear2.png', 96, 32, 16);
        game.load.spritesheet('gnome_anim1_1', 'img/gnome_anim1_1.png', 240, 176, 17);
        game.load.spritesheet('gnome_anim1_2', 'img/gnome_anim1_2.png', 240, 176, 17);
        game.load.spritesheet('gold_row_win', 'img/gold_row_win.png', 96, 96, 3);
        game.load.audio('rotate', 'sounds/rotate.wav');

      },
      create:function(){
        game.state.start('game1'); //переключение на 1 игру
        document.getElementById('preloader').style.display = 'none';
      }

    };
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру

game.state.start('gamePreload'); //начало игры с локации загрузчика

$(document).ready(function(){
	var HeightNow = document.documentElement.clientHeight; 
	function checkWidth() {

		var realHeight = document.documentElement.clientHeight; 
            if (HeightNow > realHeight){    // Если текущий размер  меньше размера в полном экране, то выставляем заглушку
            	console.log('выход из полноэкранного режима');
            	full.loadTexture('non_full');
            	fullStatus = false;
            }
            HeightNow = realHeight; 
          };
          $( window ).resize(function() {
          	var realHeight = document.documentElement.clientHeight; 
          });
          window.addEventListener('resize', checkWidth); 
          checkWidth();
        });