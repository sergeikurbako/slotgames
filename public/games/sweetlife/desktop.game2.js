var cloud_l_l = [];
var cloud_r_l = [];
var cloud_l_s = [];
var cloud_r_s = [];
var tree = [];
var beehive_l = [];
var beehive_r = [];
function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        
        hithit_game2 = game.add.audio('hithit_game2');
        final_win_game2 = game.add.audio('final_win_game2');
        win_first_game2 = game.add.audio('win_first_game2');
        lose_game2 = game.add.audio('lose_game2');
        //изображения
        backgroundGame3 = game.add.sprite(94,54, 'game.backgroundGame2');

        for (var i = 1; i <= 20; ++i) {
            cloud_l_l[i] = game.add.sprite(94,286-360*(i-1), 'cloud_1');
            cloud_r_l[i] = game.add.sprite(573,283-360*(i-1), 'cloud_1');
            cloud_l_s[i] = game.add.sprite(100,106-360*(i-1), 'cloud_2');
            cloud_r_s[i] = game.add.sprite(609,103-360*(i-1), 'cloud_2');
        }
        for (var i = 1; i <= 30; ++i) { 
            treeNumber = randomNumber(1,2);
            tree[i] = game.add.sprite(262,306-255*(i-1), 'tree_' + treeNumber); 
        }
        for (var i = 1; i <= 30; ++i) { 
            beehive_l[i] = game.add.sprite(262,450-255*(i-1), 'beehive_wait'); 
            beehive_l[i].animations.add('beehive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
            beehive_r[i] = game.add.sprite(486,450-255*(i-1), 'beehive_wait');
            beehive_r[i].animations.add('beehive_wait', [0,1,2,3,4,5,6,7], 8, true).play();
        }

        topBarImage2 = game.add.sprite(94,22, 'topScoreGame3');
        
        leaves_1 = game.add.sprite(279,400, 'leaves_1');
        leaves_1.visible = false;
        leaves_2 = game.add.sprite(279,400, 'leaves_2');
        leaves_2.visible = false;
        bee_lose_1_l = game.add.sprite(262,195, 'bee_lose_1');
        bee_lose_1_l.visible = false;
        bee_lose_1_r = game.add.sprite(486,195, 'bee_lose_1');
        bee_lose_1_r.visible = false;
        bee_lose_2_l = game.add.sprite(262,195, 'bee_lose_2');
        bee_lose_2_l.visible = false;
        bee_lose_2_r = game.add.sprite(486,195, 'bee_lose_2');
        bee_lose_2_r.visible = false;
        bear_lose_down_2 = game.add.sprite(279,148, 'bear_lose_down');
        bear_lose_down_2.visible = false;
        bear_waitt = game.add.sprite(327,148, 'game2.bear_wait');
        bear_waitt.animations.add('bear_wait', [0,1,2,2,2,2,2,2,2,2,1,0,3,4,5,5,5,5,5,5,5,5,4,3], 8, true).play();
        bear_hit_left = game.add.sprite(279,148, 'bear_hit_left');        
        bear_hit_left.visible = false;
        bear_hit_right = game.add.sprite(279,148, 'bear_hit_right');
        bear_hit_right.visible = false;
        bear_lose_left_1 = game.add.sprite(279,148, 'bear_lose_left_1');
        bear_lose_left_1.visible = false;
        bear_lose_right_1 = game.add.sprite(279,148, 'bear_lose_right_1');
        bear_lose_right_1.visible = false;
        bear_win_left_1 = game.add.sprite(279,148, 'bear_win_left_1');
        bear_win_left_1.visible = false;
        bear_win_left_2 = game.add.sprite(279,148, 'bear_win_left_2');
        bear_win_left_2.visible = false;
        bear_win_right_1 = game.add.sprite(279,148, 'bear_win_right_1');
        bear_win_right_1.visible = false;
        bear_win_right_2 = game.add.sprite(279,148, 'bear_win_right_2');
        bear_win_right_2.visible = false;

        background = game.add.sprite(0,0, 'game.background');


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [485, 59, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);


        //анимация

        selectGame = game.add.sprite(70,510, 'selectGame_d');
        selectGame.scale.setTo(0.7, 0.7);
        selectGame.inputEnabled = false;

        payTable = game.add.sprite(150,510, 'payTable_d');
        payTable.scale.setTo(0.7, 0.7);
        payTable.inputEnabled = false;

        betone = game.add.sprite(490,510, 'betone_d');
        betone.scale.setTo(0.7, 0.7);
        betone.inputEnabled = false;


        betmax = game.add.sprite(535,510, 'betmax_d');
        betmax.scale.setTo(0.7, 0.7);
        betmax.inputEnabled = false;

        automaricstart = game.add.sprite(685,510, 'automaricstart_d');
        automaricstart.scale.setTo(0.7, 0.7);
        automaricstart.inputEnabled = false;

        startButton = game.add.sprite(597, 510, 'startButton');
        startButton.scale.setTo(0.7,0.7);
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('startButton_p');
        });
        startButton.events.onInputOut.add(function(){
            startButton.loadTexture('startButton');
        });
        startButton.events.onInputDown.add(function(){
            hideDoubleToAndTakeOrRiskTexts();
            game.state.start('game1');
        });

        buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
        buttonLine1.scale.setTo(0.7,0.7);
        buttonLine1.inputEnabled = false;

        buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
        buttonLine3.scale.setTo(0.7,0.7);
        buttonLine3.inputEnabled = true;
        buttonLine3.input.useHandCursor = true;
        buttonLine3.events.onInputOver.add(function(){
            buttonLine3.loadTexture('buttonLine3_p');
        });
        buttonLine3.events.onInputOut.add(function(){
            buttonLine3.loadTexture('buttonLine3');
        });
        buttonLine3.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid, game);
        });

        buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
        buttonLine5.scale.setTo(0.7,0.7);
        buttonLine5.inputEnabled = false;

        buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
        buttonLine7.scale.setTo(0.7,0.7);
        buttonLine7.inputEnabled = true;
        buttonLine7.input.useHandCursor = true;
        buttonLine7.events.onInputOver.add(function(){
            buttonLine7.loadTexture('buttonLine7_p');
        });
        buttonLine7.events.onInputOut.add(function(){
            buttonLine7.loadTexture('buttonLine7');
        });
        buttonLine7.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });

        buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
        buttonLine9.scale.setTo(0.7,0.7);
        buttonLine9.inputEnabled = false;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 398, 100);
        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}