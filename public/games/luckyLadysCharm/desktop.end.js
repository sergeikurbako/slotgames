(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        /* *
        *  файлы с фиксироваными именами переменных
        *
        * */
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        /* кнопки звука и режима экрана */
        game.load.image('game.non_full', needUrlPath + '/img/full.png');
        game.load.image('game.full', needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        /* кнопки */
        game.load.image('startButton', needUrlPath + '/img/desktopButtons/startButton.png');
        game.load.image('startButton_p', needUrlPath + '/img/desktopButtons/startButton_p.png');
        game.load.image('startButton_d', needUrlPath + '/img/desktopButtons/startButton_d.png');
        game.load.image('selectGame', needUrlPath + '/img/desktopButtons/selectGame.png');
        game.load.image('selectGame_p', needUrlPath + '/img/desktopButtons/selectGame_p.png');
        game.load.image('selectGame_d', needUrlPath + '/img/desktopButtons/selectGame_d.png');
        game.load.image('payTable', needUrlPath + '/img/desktopButtons/payTable.png');
        game.load.image('payTable_p', needUrlPath + '/img/desktopButtons/payTable_p.png');
        game.load.image('payTable_d', needUrlPath + '/img/desktopButtons/payTable_d.png');
        game.load.image('automaricstart', needUrlPath + '/img/desktopButtons/automaricstart.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/desktopButtons/automaricstart_p.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/desktopButtons/automaricstart_d.png');
        game.load.image('betone', needUrlPath + '/img/desktopButtons/betone.png');
        game.load.image('betone_p', needUrlPath + '/img/desktopButtons/betone_p.png');
        game.load.image('betone_d', needUrlPath + '/img/desktopButtons/betone_d.png');
        game.load.image('betmax', needUrlPath + '/img/desktopButtons/betmax.png');
        game.load.image('betmax_p', needUrlPath + '/img/desktopButtons/betmax_p.png');
        game.load.image('betmax_d', needUrlPath + '/img/desktopButtons/betmax_d.png');
        game.load.image('buttonLine1', needUrlPath + '/img/desktopButtons/buttonLine1.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/desktopButtons/buttonLine1_p.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/desktopButtons/buttonLine1_d.png');
        game.load.image('buttonLine3', needUrlPath + '/img/desktopButtons/buttonLine3.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/desktopButtons/buttonLine3_p.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/desktopButtons/buttonLine3_d.png');
        game.load.image('buttonLine5', needUrlPath + '/img/desktopButtons/buttonLine5.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/desktopButtons/buttonLine5_p.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/desktopButtons/buttonLine5_d.png');
        game.load.image('buttonLine7', needUrlPath + '/img/desktopButtons/buttonLine7.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/desktopButtons/buttonLine7_p.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/desktopButtons/buttonLine7_d.png');
        game.load.image('buttonLine9', needUrlPath + '/img/desktopButtons/buttonLine9.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/desktopButtons/buttonLine9_p.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/desktopButtons/buttonLine9_d.png');

        /* номера линий */
        game.load.image('number1', needUrlPath + '/img/game1/lineNumbers/lineNumber1.png');
        game.load.image('number2', needUrlPath + '/img/game1/lineNumbers/lineNumber2.png');
        game.load.image('number3', needUrlPath + '/img/game1/lineNumbers/lineNumber3.png');
        game.load.image('number4', needUrlPath + '/img/game1/lineNumbers/lineNumber4.png');
        game.load.image('number5', needUrlPath + '/img/game1/lineNumbers/lineNumber5.png');
        game.load.image('number6', needUrlPath + '/img/game1/lineNumbers/lineNumber6.png');
        game.load.image('number7', needUrlPath + '/img/game1/lineNumbers/lineNumber7.png');
        game.load.image('number8', needUrlPath + '/img/game1/lineNumbers/lineNumber8.png');
        game.load.image('number9', needUrlPath + '/img/game1/lineNumbers/lineNumber9.png');

        /* линии */
        game.load.image('linefull1', needUrlPath + '/img/game1/lines/linefull1.png');
        game.load.image('linefull2', needUrlPath + '/img/game1/lines/linefull2.png');
        game.load.image('linefull3', needUrlPath + '/img/game1/lines/linefull3.png');
        game.load.image('linefull4', needUrlPath + '/img/game1/lines/linefull4.png');
        game.load.image('linefull5', needUrlPath + '/img/game1/lines/linefull5.png');
        game.load.image('linefull6', needUrlPath + '/img/game1/lines/linefull6.png');
        game.load.image('linefull7', needUrlPath + '/img/game1/lines/linefull7.png');
        game.load.image('linefull8', needUrlPath + '/img/game1/lines/linefull8.png');
        game.load.image('linefull9', needUrlPath + '/img/game1/lines/linefull9.png');

        /* звуки слотов */
        game.load.audio('rotateSound', needUrlPath + '/sound/game1/rotateSound.mp3');
        game.load.audio('stopSound', needUrlPath + '/sound/game1/stopSound.mp3');
        //game.load.audio('tada', needUrlPath + '/sound/game1/tada.wav');
        //game.load.audio('play', needUrlPath + '/sound/game1/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/game1/takeWin.mp3');
        game.load.audio('win', needUrlPath + '/sound/game1/win.mp3');
        //game.load.audio('winMonkey', needUrlPath + '/sound/game1/winBonusGame.mp3'); //выпадение бонусной игры

        /* звуки нажатия кнопок выбора линий */
        /*game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');*/

        /* звуки выигрышных линий */
        game.load.audio('soundWinLine1', needUrlPath + '/sound/game1/randomWinLineSounds/win1.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/game1/randomWinLineSounds/win2.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/game1/randomWinLineSounds/win3.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/game1/randomWinLineSounds/win4.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/game1/randomWinLineSounds/win5.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/game1/randomWinLineSounds/win6.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/game1/randomWinLineSounds/win7.mp3');


        /* значения слотов */
        game.load.image('cell0', needUrlPath + '/img/game1/slotValues/cell0.jpg');
        game.load.image('cell1', needUrlPath + '/img/game1/slotValues/cell1.jpg');
        game.load.image('cell2', needUrlPath + '/img/game1/slotValues/cell2.jpg');
        game.load.image('cell3', needUrlPath + '/img/game1/slotValues/cell3.jpg');
        game.load.image('cell4', needUrlPath + '/img/game1/slotValues/cell4.jpg');
        game.load.image('cell5', needUrlPath + '/img/game1/slotValues/cell5.jpg');
        game.load.image('cell6', needUrlPath + '/img/game1/slotValues/cell6.jpg');
        game.load.image('cell7', needUrlPath + '/img/game1/slotValues/cell7.jpg');
        game.load.image('cell7', needUrlPath + '/img/game1/slotValues/cell8.jpg');
        game.load.image('cell7', needUrlPath + '/img/game1/slotValues/cell9.jpg');
        game.load.spritesheet('cellAnims', needUrlPath + '/img/game1/slotValues/cellAnims.png', 100, 100); //анимация вращения слота
        //game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/slotValues/selectionOfTheManyCellAnim.png', 96, 96); //анимированое значение слота перехода в третью игру

        game.load.spritesheet('cell0Anim', needUrlPath + '/img/game1/slotValues/cell0Anim.png', 100, 100);
        game.load.spritesheet('cell1Anim', needUrlPath + '/img/game1/slotValues/cell1Anim.png', 100, 100);
        game.load.spritesheet('cell2Anim', needUrlPath + '/img/game1/slotValues/cell2Anim.png', 100, 100);
        game.load.spritesheet('cell3Anim', needUrlPath + '/img/game1/slotValues/cell3Anim.png', 100, 100);
        game.load.spritesheet('cell4Anim', needUrlPath + '/img/game1/slotValues/cell4Anim.png', 100, 100);
        game.load.spritesheet('cell5Anim', needUrlPath + '/img/game1/slotValues/cell5Anim.png', 100, 100);
        game.load.spritesheet('cell6Anim', needUrlPath + '/img/game1/slotValues/cell6Anim.png', 100, 100);
        game.load.spritesheet('cell7Anim', needUrlPath + '/img/game1/slotValues/cell7Anim.png', 100, 100);
        game.load.spritesheet('cell8Anim', needUrlPath + '/img/game1/slotValues/cell8Anim.png', 100, 100);
        game.load.spritesheet('cell9Anim', needUrlPath + '/img/game1/slotValues/cell9Anim.png', 100, 100);

        game.load.image('square1', needUrlPath + '/img/game1/slotValues/square1.png');
        game.load.image('square2', needUrlPath + '/img/game1/slotValues/square2.png');
        game.load.image('square3', needUrlPath + '/img/game1/slotValues/square3.png');
        game.load.image('square4', needUrlPath + '/img/game1/slotValues/square4.png');
        game.load.image('square5', needUrlPath + '/img/game1/slotValues/square5.png');
        game.load.image('square6', needUrlPath + '/img/game1/slotValues/square6.png');
        game.load.image('square7', needUrlPath + '/img/game1/slotValues/square7.png');
        game.load.image('square8', needUrlPath + '/img/game1/slotValues/square8.png');
        game.load.image('square9', needUrlPath + '/img/game1/slotValues/square9.png');
        game.load.image('square9', needUrlPath + '/img/game1/slotValues/square9.png');

        game.load.image('border', needUrlPath + '/img/border.png'); //внешняя часть автомата (слота)

        //карты
        game.load.image('m1', needUrlPath + '/img/game2/cards/m1.png');
        game.load.image('m2', needUrlPath + '/img/game2/cards/m2.png');
        game.load.image('m3', needUrlPath + '/img/game2/cards/m3.png');
        game.load.image('m4', needUrlPath + '/img/game2/cards/m4.png');

        game.load.image('card1', needUrlPath + '/img/game2/cards/card1.png');
        game.load.image('card2', needUrlPath + '/img/game2/cards/card2.png');
        game.load.image('card3', needUrlPath + '/img/game2/cards/card3.png');
        game.load.image('card4', needUrlPath + '/img/game2/cards/card4.png');

        game.load.image('buttonRed', needUrlPath + '/img/game2/buttonRed.png');
        game.load.image('buttonBlack', needUrlPath + '/img/game2/buttonBlack.png');


        /* *
         *  файлы с произвольными именами переменных
         *
         * */

        
        game.load.image('backgroundGame2', needUrlPath + '/img/game2/bgForGame2.jpg');
        
        game.load.spritesheet('cardAnim', needUrlPath + '/img/game2/cardAnim.png', 157, 234);


        game.load.image('slots', needUrlPath + '/img/game1/slots.png');
        game.load.audio('sound22', needUrlPath + '/sound/game2/9_sound22.mp3');
        game.load.audio('buttonSound', needUrlPath + '/sound/game1/buttonSound.mp3');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

