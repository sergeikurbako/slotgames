function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        background = game.add.sprite(0,0, 'game.background');
        background1 = game.add.sprite(94,54, 'game.background1');
        topBarImage2 = game.add.sprite(94,22, 'topScoreGame3');
        backgroundGame3 = game.add.sprite(94,54, 'game.backgroundGame2');

        addTableTitle(game, 'winTitleGame2', 239,359);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [485, 59, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[124,118], [257,118], [369,118], [481,118], [593,118]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация
        fireplace = game.add.sprite(318, 390, 'fireplace');
        fireplace.animations.add('fireplace', [0,1,2,3], 6, true).play();

        function showGnome(){
            game1.gnome_anim1_1 = game.add.sprite(94, 326, 'gnome_anim1_1');
            game1.gnome_anim1_1Animation = game1.gnome_anim1_1.animations.add('gnome_anim1_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_1Animation.play();
            game1.gnome_anim1_2 = game.add.sprite(94, 326, 'gnome_anim1_2'); 
            game1.gnome_anim1_2Animation = game1.gnome_anim1_2.animations.add('gnome_anim1_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_2.visible = false;
            game1.gnome_anim1_1Animation.onComplete.add(function(){
                game1.gnome_anim1_2Animation.play();
                game1.gnome_anim1_1.visible = false;
                game1.gnome_anim1_2.visible = true;
            });
            game1.gnome_anim1_2Animation.onComplete.add(function(){
                game1.gnome_anim1_1Animation.play();
                game1.gnome_anim1_1.visible = true;
                game1.gnome_anim1_2.visible = false;
            });
        };

        showGnome();

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 340, 360);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}