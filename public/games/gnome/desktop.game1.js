var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var gamename = 'gnomerus';
var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        
        // изображения
        
        game.add.sprite(0,0, 'game.background');
        game.add.sprite(94,54, 'game.background1');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');
        
        
        
        addTableTitle(game, 'play1To',239,359);

        slotPosition = [[142, 54], [142, 150], [142, 246], [254, 54], [254, 150], [254, 246], [365, 54], [365, 150], [365, 246], [477, 54], [477, 150], [477, 246], [589, 54], [589, 150], [589, 246]];
        addSlots(game, slotPosition);

        var linePosition = [[134,197], [134,70], [134,325], [134,134], [134,96], [134,102], [134,230], [134,226], [134,122]];
        var numberPosition = [[94,182], [94,54], [94,310], [94,118], [94,246], [94,86], [94,278], [94,214], [94,150]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        var pageCount = 5;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [118, 355, 16], [695, 353, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        // анимация

        fireplace = game.add.sprite(318, 390, 'fireplace');
        fireplace.animations.add('fireplace', [0,1,2,3], 6, true).play();
        gear2 = game.add.sprite(478, 406, 'gear2');
        gear2Animation = gear2.animations.add('gear2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 10, true);
        function showGnome(){
            game1.gnome_anim1_1 = game.add.sprite(94, 326, 'gnome_anim1_1');
            game1.gnome_anim1_1Animation = game1.gnome_anim1_1.animations.add('gnome_anim1_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_1Animation.play();
            game1.gnome_anim1_2 = game.add.sprite(94, 326, 'gnome_anim1_2'); 
            game1.gnome_anim1_2Animation = game1.gnome_anim1_2.animations.add('gnome_anim1_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 6, false);
            game1.gnome_anim1_2.visible = false;
            game1.gnome_anim1_1Animation.onComplete.add(function(){
                game1.gnome_anim1_2Animation.play();
                game1.gnome_anim1_1.visible = false;
                game1.gnome_anim1_2.visible = true;
            });
            game1.gnome_anim1_2Animation.onComplete.add(function(){
                game1.gnome_anim1_1Animation.play();
                game1.gnome_anim1_1.visible = true;
                game1.gnome_anim1_2.visible = false;
            });
        };
        showGnome();


        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[115, 441], [324, 441], [510, 441]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);
        light_settings = game.add.sprite(685, 22, 'light_settings');
        light_settingsAnim = light_settings.animations.add('light_settings', [0,1,2,3], 8, true).play();       
        light_settings.visible = false;

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
   if(bet >= extralife && checkHelm == false) {
            // mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            // mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 10, false);
            // mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
            //     mushroomJump = game.add.sprite(353,423, 'game.mushroomJump');
            //     mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 10, true);
            //     mushroomJump.animations.getAnimation('mushroomJump').play();
            // });
            console.log('checkHelm = true');
            // checkHelm = true;
            checkHelm = false;
        }
        
        if(bet < extralife && checkHelm == true) {
            console.log('checkHelm = false');
            checkHelm = false;

            // mushroomGrow.visible = false;
            // mushroomJump.visible = false;

            // mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            // mushroomGrow.animations.add('mushroomGrow', [5,4,3,2,1,0], 10, false);
            // mushroomGrow.animations.getAnimation('mushroomGrow').play();
        }
        
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
                // function hideMonkey(){
                //     monkey1.visible = false;
                //     monkey2.visible = false;
                //     monkey3.visible = false;
                //     monkey4.visible = false;
                //     monkey5.visible = false;
                // }

                // hideMonkey();

                // monkeyTakeMushroom1 = game.add.sprite(195,359, 'game.monkeyTakeMushroom1');
                // monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                // monkeyTakeMushroom1.visible = false;

                // monkeyTakeMushroom2 = game.add.sprite(195,359, 'game.monkeyTakeMushroom2');
                // monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                // monkeyTakeMushroom2.visible = false;

                // function monkeyTakeMushroomAnim() {
                //     monkeyTakeMushroom1.visible = true;
                //     monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                //         monkeyTakeMushroom1.visible = false;

                //         monkeyTakeMushroom2.visible = true;
                //         monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                //     });
                // }

                // monkeyTakeMushroomAnim();
            }
        }
    };

    game.state.add('game1', game1);

};
