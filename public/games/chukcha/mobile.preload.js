(function(){
    var preload = {};
    console.log("preload");
    console.log("game = ", game);
    
    preload.preload = function(){
        
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });
        
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        
        game.load.image('start', 'img/btns/mobile/spin.png');
        game.load.image('start_p', 'img/btns/mobile/spin_p.png');
        game.load.image('start_d', 'img/btns/mobile/spin_d.png');
        game.load.image('bet1', 'img/btns/mobile/bet1.png');
        game.load.image('bet1_p', 'img/btns/mobile/bet1_p.png');
        game.load.image('home', 'img/btns/mobile/home.png');
        game.load.image('home_p', 'img/btns/mobile/home_p.png');
        game.load.image('dollar', 'img/btns/mobile/dollar.png');
        game.load.image('gear', 'img/btns/mobile/gear.png');
        game.load.image('double', 'img/btns/mobile/double.png');
        game.load.image('collect', 'img/btns/mobile/collect.png');
        game.load.image('collect_p', 'img/btns/mobile/collect_p.png');
        game.load.image('collect_d', 'img/btns/mobile/collect_d.png');
        
        game.load.image('x', 'img/x.png');
        game.load.image('game.background1', 'img/main_bg.jpg');
        game.load.image('game.background2', 'img/bg_game_2.png');
        game.load.image('game3.bg', 'img/bg_game_3.png');
        game.load.image('game4.bg', 'img/bg_game_4.png');
        game.load.image('game1.bg_top', 'img/main_top_1.png');
        game.load.image('game1.bg_top_win', 'img/main_top_1_win.png');
        game.load.image('game2.bg_top', 'img/main_top_2.png');
        game.load.image('game3.bg_top', 'img/main_top_3.png');
        game.load.image('game.bottom_pan', 'img/info.png');
        
        game.load.image('game2.dealer', 'img/dealer.png');
        game.load.image('game2.pick', 'img/pick.png');
        game.load.image('card_bg', 'img/cards/back.png');
        
        game.load.image('game3.hole', 'img/ice-hole.png');
        game.load.image('game3.fisher', 'img/fisher.png');
        
        game.load.image('game4.shaman', 'img/shaman.png');
        game.load.image('game4.green_eyes', 'img/green_eyes.png');
        game.load.image('game4.your_choice', 'img/your_choice.png');
        game.load.image('game4.totem', 'img/totem.png');
        
        for(var i =0; i < 52; i++){
            game.load.image('card_' + i, 'img/cards/'+i + '.png');
        }
        
//        game.load.image('game.background', 'img/machine.png');
//        game.load.image('start', 'img/btns/btn_start.png');
//        game.load.image('start_p', 'img/btns/btn_start_p.png');
//        game.load.image('start_d', 'img/btns/btn_start_d.png');
//        game.load.image('automaricStart', 'img/btns/btn_automatic_start.png');
//        game.load.image('automaricStart_p', 'img/btns/btn_automatic_start_p.png');
//        game.load.image('automaricStart_d', 'img/btns/btn_automatic_start_d.png');
//        game.load.image('betMax', 'img/btns/btn_bet_max.png');
//        game.load.image('betMax_p', 'img/btns/btn_bet_max_p.png');
//        game.load.image('betMax_d', 'img/btns/btn_bet_max_d.png');
//        game.load.image('betOne', 'img/btns/btn_bet_one.png');
//        game.load.image('betOne_p', 'img/btns/btn_bet_one_p.png');
//        game.load.image('betOne_d', 'img/btns/btn_bet_one_d.png');
//        game.load.image('payTable', 'img/btns/btn_paytable.png');
//        game.load.image('payTable_p', 'img/btns/btn_paytable_p.png');
//        game.load.image('payTable_d', 'img/btns/btn_paytable_d.png');
//        game.load.image('selectGame', 'img/btns/btn_select_game.png');
//        game.load.image('selectGame_p', 'img/btns/btn_select_game_p.png');
//        game.load.image('selectGame_d', 'img/btns/btn_select_game_d.png');
        game.load.image('game.non_full', 'img/non_full.png');
        game.load.image('game.full', 'img/full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');
        game.load.image('bonusGame', 'img/bonusGame.png');
        game.load.spritesheet('game1.press_1_res', 'img/press_1_res.png', 96,96,3);
        game.load.spritesheet('game1.press_1', 'img/press_1.png', 96, 96, 2);
        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/bonus.png',96, 96, 2);
        game.load.spritesheet('smoke', 'img/smoke.png', 42, 56, 5);
        game.load.spritesheet('playTo', 'img/playTo.png', 94, 32, 2);
        game.load.spritesheet('takeOrRisk', 'img/takeOrRisk.png', 95, 32, 2);
        
        game.load.spritesheet('fire', 'img/sprite_fire.png', 35, 41, 5);
        game.load.spritesheet('dance', 'img/momo.png',263,77,67);
        game.load.spritesheet('mermaid', 'img/mermaid.png', 199,221,6);
        game.load.spritesheet('mermaidBorder', 'img/mermaid_border.png', 72,92,2);
        game.load.spritesheet('fish_big', 'img/big-fish.png', 199,221,3);
        game.load.spritesheet('bFishBorder', 'img/bFish_border.png', 70,80,2);
        game.load.spritesheet('fish_small', 'img/small-fish.png', 199,221,4);
        game.load.spritesheet('sFishBorder', 'img/sFish_border.png', 71, 32, 2);
        game.load.spritesheet('squid', 'img/squid.png', 199,221,4);
        game.load.spritesheet('squidBorder', 'img/squid_border.png', 52,79,2);
        game.load.spritesheet('shield', 'img/shield.png', 199,221,3);
        game.load.spritesheet('bear', 'img/bear.png', 199,221,5);
        game.load.spritesheet('choice', 'img/choice.png', 215,177,11);
        game.load.spritesheet('pain', 'img/pain.png', 215,177,12);
        game.load.spritesheet('lightning', 'img/lightning.png', 360, 473, 4);
        game.load.spritesheet('gold_rain', 'img/gold_rain.png', 120, 356, 4);
        game.load.spritesheet('shot', 'img/shoot.png', 199, 221, 11);
        game.load.spritesheet('cellAnim', 'img/shape_full.png',96,96,9);
        
        game.load.image('cell0', 'img/cell0.png');
        game.load.image('cell1', 'img/cell1.png');
        game.load.image('cell2', 'img/cell2.png');
        game.load.image('cell3', 'img/cell3.png');
        game.load.image('cell4', 'img/cell4.png');
        game.load.image('cell5', 'img/cell5.png');
        game.load.image('cell6', 'img/cell6.png');
        game.load.image('cell7', 'img/cell7.png');
        game.load.image('cell8', 'img/cell8.png');
        
        for(var i = 1; i < 6; i++){
            game.load.image('game.page_' + i, 'img/page_' + i + '.png');
        }
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line' + i, 'img/lines/select/line' + i + '.png');
            game.load.image('linefull' + i, 'img/lines/win/linefull' + i + '.png');
            game.load.image('num_p_' + i, 'img/num_p_' + i + '.png');
            game.load.image('num_d_' + i, 'img/num_d_' + i + '.png');
//            if (i % 2 != 0) {
//                game.load.audio('line' + i + 'Sound', 'sounds/line' + i + '.mp3');
//                if(!isMobile){
//                    game.load.image('buttonLine' + i, 'img/btns/btn' + i + '.png');
//                    game.load.image('buttonLine'+i+'_p', 'img/btns/btn' + i + '_p.png');
//                    game.load.image('buttonLine'+i+'_d', 'img/btns/btn' + i + '_d.png');
//                }
//            }
        }
        
        game.load.audio('bet_one', 'sounds/betOne.mp3');
        game.load.audio('stopSound', 'sounds/stop.mp3');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('winLine', 'sounds/winLine.mp3');
        game.load.audio('rotateSound', 'sounds/rotate.mp3');
        game.load.audio('tada', 'sounds/tada.mp3');
        game.load.audio('automatic', 'sounds/automatic.mp3');
        game.load.audio('openCard', 'sounds/opencard.mp3');
        game.load.audio('winCard', 'sounds/openuser.mp3');
        
        game.load.audio('shoot', 'sounds/bonusman_kidok.mp3');
        game.load.audio('bear', 'sounds/bonus_gr_med.mp3');
        game.load.audio('mermaid', 'sounds/bonus_find_rusalka.mp3');
        game.load.audio('sFish', 'sounds/bonus_find_ruba.mp3');
        game.load.audio('bFish', 'sounds/bonus_find_big_ruba.mp3');
        game.load.audio('squid', 'sounds/bonus_find_meduza.mp3');
        game.load.audio('shield', 'sounds/bonus_find_buben.mp3');
        game.load.audio('bonusGame', 'sounds/bonus_game.mp3');
        
        game.load.audio('pain', 'sounds/super_bonus_die.mp3');
        game.load.audio('gold_rain', 'sounds/super_bonus_win.mp3');
        game.load.audio('choice', 'sounds/super_bonus_man_say.mp3');
        game.load.audio('lightning', 'sounds/super_bonus_molnia.mp3');
        game.load.audio('superbonus', 'sounds/superbonus.mp3');
    }
    
     preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    }
    game.state.add('preload', preload);
    
})();

game.state.start('preload');

