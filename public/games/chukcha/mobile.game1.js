var game = new Phaser.Game(640, 480, Phaser.AUTO, null, null, 'ld29', null, false, false);

var slotPosition = [[145-mobileX, 85-mobileY], [145-mobileX, 195-mobileY], [145-mobileX, 304-mobileY], [257-mobileX, 85-mobileY], [257-mobileX, 195-mobileY], [257-mobileX, 304-mobileY], [369-mobileX, 85-mobileY], [369-mobileX, 195-mobileY], [369-mobileX, 304-mobileY], [481-mobileX, 85-mobileY], [481-mobileX, 195-mobileY], [481-mobileX, 304-mobileY], [593-mobileX, 85-mobileY], [593-mobileX, 195-mobileY], [593-mobileX, 304-mobileY]];

var linePosition = [[135-mobileX,240-mobileY], [135-mobileX,97-mobileY], [135-mobileX,382-mobileY], [135-mobileX,159-mobileY], [135-mobileX,119-mobileY], [135-mobileX,132-mobileY], [135-mobileX,261-mobileY], [135-mobileX,272-mobileY], [135-mobileX,151-mobileY]];

var numberPosition = [[97-mobileX,230-mobileY], [97-mobileX,86-mobileY], [97-mobileX,374-mobileY], [97-mobileX,150-mobileY], [97-mobileX,310-mobileY], [97-mobileX,118-mobileY], [97-mobileX,342-mobileY], [97-mobileX,262-mobileY], [97-mobileX,198-mobileY]];
                      
var scorePosions = [[265-mobileX, 26-mobileY, 20], [470-mobileX, 26-mobileY, 20], [640-mobileX, 26-mobileY, 20], [92-mobileX, 56-mobileY, 20], [695-mobileX, 56-mobileY, 20]];

var linesPlay = ['line1','line2','line3','line4','line5','line6','line7','line8','line9'];

var checkGame = 1, 
    topBarImage,
    tableTitle = {
        playTo: null,
        playToAnim: null,
        bonusGame: null,
        takeOrRisk: null,
        takeOrRiskAnim: null,
        current: null,
        currentAnim: null
    };

function createGame1(){
    game.stage.disableVisibilityChange = true;
    game1 = {
        create: function(){
            checkGame = 1;
//            game.add.sprite(0,0, 'game.background');
            game.add.sprite(93-mobileX,23-mobileY, 'game.background1');
            game.add.sprite(560-mobileX,450-mobileY, 'game.bottom_pan');
            
            topBarImage = game.add.sprite(139-mobileX,24-mobileY, 'game1.bg_top');
            
            tableTitle.playTo = game.add.sprite(585-mobileX,460-mobileY, 'playTo');
            tableTitle.playToAnim = tableTitle.playTo.animations.add('playTo', [0, 1], 5, true);
            tableTitle.playToAnim.play();
            tableTitle.current = 'playTo';
            tableTitle.currentAnim = 'playToAnim';
            
            tableTitle.takeOrRisk = game.add.sprite(585-mobileX,460-mobileY, 'takeOrRisk');
            tableTitle.takeOrRisk.visible = false;
            tableTitle.takeOrRiskAnim = tableTitle.takeOrRisk.animations.add('takeOrRisk', [0, 1], 5, true);
            
            tableTitle.bonusGame = game.add.sprite(585-mobileX,460-mobileY, 'bonusGame');
            tableTitle.bonusGame.visible = false;
            
            
//            addTableTitle(game, 'play1To',235,343);
            addSlots(game, slotPosition);
            // betScore, linesScore, balanceScore, betline1Score, betline2Score
            addScore(game, scorePosions, bet, lines, balance, betline);
            addLinesAndNumbers(game, linePosition, numberPosition);
            showLines(linesPlay);
            showNumbers(numberArray);

            // кнопки
            addButtonsGame1Mobile(game);
            full_and_sound();
            
            makeStaticAnimations();
        }
    }
    game.state.add('game1', game1);
}