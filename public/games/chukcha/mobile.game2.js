var cardPosition = [[152-mobileX,159-mobileY], [259-mobileX,159-mobileY], [366-mobileX,159-mobileY], [473-mobileX,159-mobileY], [580-mobileX,159-mobileY]];
var pick;
function createGame2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        game.add.sprite(93-mobileX,23-mobileY, 'game.background1');
        game.add.sprite(125-mobileX,85-mobileY, 'game.background2');
        game.add.sprite(139-mobileX,24-mobileY, 'game2.bg_top');
        game.add.sprite(560-mobileX,450-mobileY, 'game.bottom_pan');
        

//        hideTableTitle();

        //счет
        step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
//        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
        
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);
        //карты
        addCards(game, cardPosition);
        game.add.sprite(165-mobileX,300-mobileY, 'game2.dealer');
        pick = game.add.sprite(161-mobileX, 300-mobileY, 'game2.pick');
        pick.visible = false;
        
        openDCard(dcard);
        
        // кнопки
        addButtonsGame2Mobile(game);

        showDoubleToAndTakeOrRiskTexts(game, totalWin, 585-mobileX, 465-mobileY);

        full_and_sound();
        makeStaticAnimations();
    };
    game.state.add('game2', game2);

    // звуки
}