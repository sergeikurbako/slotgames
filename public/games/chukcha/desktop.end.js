(function(){
    var preload = {};
    console.log("preload");
    console.log("game = ", game);
    
    preload.preload = function(){
        
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });
        
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.stage.disableVisibilityChange = true;
        		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        game.load.image('game.background1', needUrlPath + '/img/main_bg.jpg');
        game.load.image('game.background2', needUrlPath + '/img/bg_game_2.png');
        game.load.image('game3.bg', needUrlPath + '/img/bg_game_3.png');
        game.load.image('game4.bg', needUrlPath + '/img/bg_game_4.png');
        game.load.image('game1.bg_top', needUrlPath + '/img/main_top_1.png');
        game.load.image('game1.bg_top_win', needUrlPath + '/img/main_top_1_win.png');
        game.load.image('game2.bg_top', needUrlPath + '/img/main_top_2.png');
        game.load.image('game3.bg_top', needUrlPath + '/img/main_top_3.png');
        game.load.image('game.bottom_pan', needUrlPath + '/img/info.png');
        
        game.load.image('game2.dealer', needUrlPath + '/img/dealer.png');
        game.load.image('game2.pick', needUrlPath + '/img/pick.png');
        game.load.image('card_bg', needUrlPath + '/img/cards/back.png');
        
        game.load.image('game3.hole', needUrlPath + '/img/ice-hole.png');
        game.load.image('game3.fisher', needUrlPath + '/img/fisher.png');
        
        game.load.image('game4.shaman', needUrlPath + '/img/shaman.png');
        game.load.image('game4.green_eyes', needUrlPath + '/img/green_eyes.png');
        game.load.image('game4.your_choice', needUrlPath + '/img/your_choice.png');
        game.load.image('game4.totem', needUrlPath + '/img/totem.png');
        
        for(var i =0; i < 52; i++){
            game.load.image('card_' + i, needUrlPath + '/img/cards/'+i + '.png');
        }
        
        game.load.image('game.background', needUrlPath + '/img/machine.png');
        game.load.image('start', needUrlPath + '/img/btns/btn_start.png');
        game.load.image('start_p', needUrlPath + '/img/btns/btn_start_p.png');
        game.load.image('start_d', needUrlPath + '/img/btns/btn_start_d.png');
        game.load.image('automaricStart', needUrlPath + '/img/btns/btn_automatic_start.png');
        game.load.image('automaricStart_p', needUrlPath + '/img/btns/btn_automatic_start_p.png');
        game.load.image('automaricStart_d', needUrlPath + '/img/btns/btn_automatic_start_d.png');
        game.load.image('betMax', needUrlPath + '/img/btns/btn_bet_max.png');
        game.load.image('betMax_p', needUrlPath + '/img/btns/btn_bet_max_p.png');
        game.load.image('betMax_d', needUrlPath + '/img/btns/btn_bet_max_d.png');
        game.load.image('betOne', needUrlPath + '/img/btns/btn_bet_one.png');
        game.load.image('betOne_p', needUrlPath + '/img/btns/btn_bet_one_p.png');
        game.load.image('betOne_d', needUrlPath + '/img/btns/btn_bet_one_d.png');
        game.load.image('payTable', needUrlPath + '/img/btns/btn_paytable.png');
        game.load.image('payTable_p', needUrlPath + '/img/btns/btn_paytable_p.png');
        game.load.image('payTable_d', needUrlPath + '/img/btns/btn_paytable_d.png');
        game.load.image('selectGame', needUrlPath + '/img/btns/btn_select_game.png');
        game.load.image('selectGame_p', needUrlPath + '/img/btns/btn_select_game_p.png');
        game.load.image('selectGame_d', needUrlPath + '/img/btns/btn_select_game_d.png');
        game.load.image('game.non_full', needUrlPath + '/img/non_full.png');
        game.load.image('game.full', needUrlPath + '/img/full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');
        game.load.image('bonusGame', needUrlPath + '/img/bonusGame.png');
        game.load.spritesheet('game1.press_1_res', needUrlPath + '/img/press_1_res.png', 96,96,3);
        game.load.spritesheet('game1.press_1', needUrlPath + '/img/press_1.png', 96, 96, 2);
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/bonus.png',96, 96, 2);
        game.load.spritesheet('smoke', needUrlPath + '/img/smoke.png', 42, 56, 5);
        game.load.spritesheet('playTo', needUrlPath + '/img/playTo.png', 94, 32, 2);
        game.load.spritesheet('takeOrRisk', needUrlPath + '/img/takeOrRisk.png', 95, 32, 2);
        
        game.load.spritesheet('fire', needUrlPath + '/img/sprite_fire.png', 35, 41, 5);
        game.load.spritesheet('dance', needUrlPath + '/img/momo.png',263,77,67);
        game.load.spritesheet('mermaid', needUrlPath + '/img/mermaid.png', 199,221,6);
        game.load.spritesheet('mermaidBorder', needUrlPath + '/img/mermaid_border.png', 72,92,2);
        game.load.spritesheet('fish_big', needUrlPath + '/img/big-fish.png', 199,221,3);
        game.load.spritesheet('bFishBorder', needUrlPath + '/img/bFish_border.png', 70,80,2);
        game.load.spritesheet('fish_small', needUrlPath + '/img/small-fish.png', 199,221,4);
        game.load.spritesheet('sFishBorder', needUrlPath + '/img/sFish_border.png', 71, 32, 2);
        game.load.spritesheet('squid', needUrlPath + '/img/squid.png', 199,221,4);
        game.load.spritesheet('squidBorder', needUrlPath + '/img/squid_border.png', 52,79,2);
        game.load.spritesheet('shield', needUrlPath + '/img/shield.png', 199,221,3);
        game.load.spritesheet('bear', needUrlPath + '/img/bear.png', 199,221,5);
        game.load.spritesheet('choice', needUrlPath + '/img/choice.png', 215,177,11);
        game.load.spritesheet('pain', needUrlPath + '/img/pain.png', 215,177,12);
        game.load.spritesheet('lightning', needUrlPath + '/img/lightning.png', 360, 473, 4);
        game.load.spritesheet('gold_rain', needUrlPath + '/img/gold_rain.png', 120, 356, 4);
        game.load.spritesheet('shot', needUrlPath + '/img/shoot.png', 199, 221, 11);
        game.load.spritesheet('cellAnim', needUrlPath + '/img/shape_full.png',96,96,9);
        
        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');
        
        for(var i = 1; i < 6; i++){
            game.load.image('game.page_' + i, needUrlPath + '/img/page_' + i + '.png');
        }
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line' + i, needUrlPath + '/img/lines/select/line' + i + '.png');
            game.load.image('linefull' + i, needUrlPath + '/img/lines/win/linefull' + i + '.png');
            game.load.image('num_p_' + i, needUrlPath + '/img/num_p_' + i + '.png');
            game.load.image('num_d_' + i, needUrlPath + '/img/num_d_' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i + 'Sound', needUrlPath + '/sounds/line' + i + '.mp3');
                if(!isMobile){
                    game.load.image('buttonLine' + i, needUrlPath + '/img/btns/btn' + i + '.png');
                    game.load.image('buttonLine'+i+'_p', needUrlPath + '/img/btns/btn' + i + '_p.png');
                    game.load.image('buttonLine'+i+'_d', needUrlPath + '/img/btns/btn' + i + '_d.png');
                }
            }
        }
        
        game.load.audio('bet_one', needUrlPath + '/sounds/betOne.mp3');
        game.load.audio('stopSound', needUrlPath + '/sounds/stop.mp3');
        game.load.audio('takeWin', needUrlPath + '/sounds/takeWin.mp3');
        game.load.audio('winLine', needUrlPath + '/sounds/winLine.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sounds/rotate.mp3');
        game.load.audio('tada', needUrlPath + '/sounds/tada.mp3');
        game.load.audio('automatic', needUrlPath + '/sounds/automatic.mp3');
        game.load.audio('openCard', needUrlPath + '/sounds/opencard.mp3');
        game.load.audio('winCard', needUrlPath + '/sounds/openuser.mp3');
        
        game.load.audio('shoot', needUrlPath + '/sounds/bonusman_kidok.mp3');
        game.load.audio('bear', needUrlPath + '/sounds/bonus_gr_med.mp3');
        game.load.audio('mermaid', needUrlPath + '/sounds/bonus_find_rusalka.mp3');
        game.load.audio('sFish', needUrlPath + '/sounds/bonus_find_ruba.mp3');
        game.load.audio('bFish', needUrlPath + '/sounds/bonus_find_big_ruba.mp3');
        game.load.audio('squid', needUrlPath + '/sounds/bonus_find_meduza.mp3');
        game.load.audio('shield', needUrlPath + '/sounds/bonus_find_buben.mp3');
        game.load.audio('bonusGame', needUrlPath + '/sounds/bonus_game.mp3');
        
        game.load.audio('pain', needUrlPath + '/sounds/super_bonus_die.mp3');
        game.load.audio('gold_rain', needUrlPath + '/sounds/super_bonus_win.mp3');
        game.load.audio('choice', needUrlPath + '/sounds/super_bonus_man_say.mp3');
        game.load.audio('lightning', needUrlPath + '/sounds/super_bonus_molnia.mp3');
        game.load.audio('superbonus', needUrlPath + '/sounds/superbonus.mp3');
    }
    
     preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    }
    game.state.add('preload', preload);
    
})();

game.state.start('preload');

