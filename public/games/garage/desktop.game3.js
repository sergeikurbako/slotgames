ropeValues = [1,2,3,4,5,1];

function randomBox() {
    //добавление массивов изображений
    var arr = [];

    var boxClock = 'game.clockBox';
    var boxHammer = 'game.HummerBox';
    var boxJack = 'game.jackBox';
    var boxFlashlight = 'game.franchBox';
    var boxSaw = 'game.sawBox';
    var boxHammer2 = 'game.hammer2Box';
    var boxWrench = 'game.flashBox';
    var boxDatail = 'game.detailBox';
    //var boxPolice = 'game.policeBox';

    arr.push(boxClock,boxHammer,boxJack,boxFlashlight,boxSaw,boxHammer2,boxWrench,boxDatail);

    var rand = Math.floor(Math.random() * arr.length);

    return arr[rand];
}

function randomBox2() {
    //добавление массивов изображений
    var arr = [];

    var boxPolice = 'game.policeBox';

    arr.push(boxPolice);

    var rand = Math.floor(Math.random() * arr.length);

    return arr[rand];
}

var hammer2Counter;
var SawCounter;
var franchCounter;

function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            ropeStep = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса
            
            //звуки
            openBoxSound = game.add.sound('game.openBoxSound');
            openPoliceBox = game.add.sound('game.openPoliceBox');
            preOpenBox = game.add.sound('game.preOpenBox');
            
            
            //изображения
            background = game.add.sprite(0,0, 'game.background');
            background2 = game.add.sprite(95,54, 'game.background3');

            box_1 = game.add.sprite(130,185, 'game.box_1');
            box_2 = game.add.sprite(230,185, 'game.box_2');
            box_3 = game.add.sprite(330,185, 'game.box_3');
            box_4 = game.add.sprite(430,185, 'game.box_4');
            box_5 = game.add.sprite(530,185, 'game.box_5');

            //счет
            scorePosions = [[260, 25, 20], [435, 25, 20], [610, 25, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);



            //кнопки и анимации событий нажатия

            ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша   
            
            winRopeNumberPosition = [[200,330],[200+105,330],[200+210,330],[200+300,330],[200+400,330]];
            timeoutForShowWinRopeNumber = 2000;
            timeoutGame3ToGame4 = 2000;
            typeWinRopeNumberAnim = 0;
            checkHelm = false;
            winRopeNumberSize = 22;

            //переменные для подсчета кол-ва полученых ключей, молотков и пил
            hammer2Counter = 0;
            SawCounter = 0;
            franchCounter = 0;

            var red_column_1 = game.add.sprite(303,150, 'game.red_column_1_0');
            var red_column_2 = game.add.sprite(399,150, 'game.red_column_2_0');
            var red_column_3 = game.add.sprite(495,150, 'game.red_column_3_0');

            columnValue1 = game.add.text(312, 120, 0, {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
            columnValue2 = game.add.text(410, 120, 0, {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
            columnValue3 = game.add.text(505, 120, 0, {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
            var boxValue;
            function checkBoxValueAndShowColumnUp() {
                if(boxValue == 'game.franchBox') {
                    franchCounter += 1;
                    red_column_1.loadTexture('game.red_column_1_'+franchCounter);
                    columnValue1.visible = false;
                    columnValue1 = game.add.text(312, 120, franchCounter, {
                        font: '22px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
                if(boxValue == 'game.hammer2Box') {
                    hammer2Counter += 1;
                    red_column_2.loadTexture('game.red_column_2_'+hammer2Counter);
                    columnValue2.visible = false;
                    columnValue2 = game.add.text(410, 120, hammer2Counter, {
                        font: '22px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
                if(boxValue == 'game.sawBox') {
                    SawCounter += 1;
                    red_column_3.loadTexture('game.red_column_3_'+SawCounter);
                    columnValue3.visible = false;
                    columnValue3 = game.add.text(505, 120, SawCounter, {
                        font: '22px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }


            //добавляем в ropesAnim анимации

            ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
                if(ropeValues[ropeStep] > 0) {
                    boxValue = randomBox();
                    preOpenBox.play();
                    box = game.add.sprite(130,185, boxValue);
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        openBoxSound.play();
                        checkBoxValueAndShowColumnUp();
                    });

                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                } else {
                    preOpenBox.play();
                    box = game.add.sprite(130,185, randomBox2());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        //openBoxSound.play();
                        //red_column_1.loadTexture('game.red_column_1_2');
                    });
                }

            };

            ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
                if(ropeValues[ropeStep] > 0) {
                    boxValue = randomBox();
                    box2 = game.add.sprite(230,185, randomBox());
                    box2.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    preOpenBox.play();
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);

                    box2.animations.getAnimation('box').play().onComplete.add(function () {
                        openBoxSound.play();
                        checkBoxValueAndShowColumnUp();
                    });
                    box_2.visible = false;
                } else {
                    box2 = game.add.sprite(230,185, randomBox2());
                    box2.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    preOpenBox.play();
                    box2.animations.getAnimation('box').play().onComplete.add(function () {
                        //openBoxSound.play();
                        //red_column_1.loadTexture('game.red_column_1_2');
                    });
                }
            };

            ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
                if(ropeValues[ropeStep] > 0) {
                    boxValue = randomBox();
                    preOpenBox.play();
                    box = game.add.sprite(330,185, randomBox());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        openBoxSound.play();
                        checkBoxValueAndShowColumnUp();
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                } else {
                    preOpenBox.play();
                    box = game.add.sprite(330,185, randomBox2());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        //openBoxSound.play();
                        //red_column_1.loadTexture('game.red_column_1_3');
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                }
            };

            ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
                if(ropeValues[ropeStep] > 0) {
                    boxValue = randomBox();
                    preOpenBox.play();
                    box = game.add.sprite(430,185, randomBox());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        openBoxSound.play();
                        checkBoxValueAndShowColumnUp();
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                } else {
                    preOpenBox.play();
                    box = game.add.sprite(430,185, randomBox2());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        //openBoxSound.play();
                        //red_column_2.loadTexture('game.red_column_2_3');
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                }
            };

            ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
                if(ropeValues[ropeStep] > 0) {
                    boxValue = randomBox();
                    preOpenBox.play();
                    box = game.add.sprite(530,185, randomBox());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        openBoxSound.play();
                        checkBoxValueAndShowColumnUp();
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                } else {
                    preOpenBox.play();
                    box = game.add.sprite(530,185, randomBox2());
                    box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
                    box.animations.getAnimation('box').play().onComplete.add(function () {
                        //openBoxSound.play();
                        //red_column_3.loadTexture('game.red_column_3_2');
                    });
                    lockDisplay();
                    setTimeout('unlockDisplay()',2000);
                }
            };

            addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);


            //анимации

            cat = game.add.sprite(95,406, 'game.cat');

            cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);

            cat.animations.getAnimation('cat').play();

            var group1 = game.add.group();

            sourceArray = [['game.man1', [222,390], 21, 5], ['game.man2', [222,390], 14, 5]];
            playAnimQueue(sourceArray, group1);

            game.world.bringToTop(group1);

            full_and_sound();


        };

        game3.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game3', game3);

})();

}