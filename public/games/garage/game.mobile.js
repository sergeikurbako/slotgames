function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

isMobile = true;

var width = 835;
var height = 610;

if(isMobile) {
    width = 640;
    height = 480;
}
window.game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');

function createLevelButtons() {
    var lvl1 = game.add.sprite(0,30, 'game.non_full');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(20, 30, 'game.non_full');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');
    }, this);

    var lvl3 = game.add.sprite(40, 30, 'game.non_full');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');

    }, this);

    var lvl4 = game.add.sprite(60, 30, 'game.non_full');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');

    }, this);
}

//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 5;

    var lines = {
        1: {
            coord: 242,
            sprite: null,
            btncoord: 250,
            button: null
        },
        2: {
            coord: 99,
            sprite: null
        },
        3: {
            coord: 385,
            sprite: null,
            btncoord: 295,
            button: null
        },
        4: {
            coord: 153,
            sprite: null
        },
        5: {
            coord: 335 - 200,
            sprite: null,
            btncoord: 340,
            button: null
        },
        6: {
            coord: 126,
            sprite: null
        },
        7: {
            coord: 361 - 100,
            sprite: null,
            btncoord: 385,
            button: null
        },
        8: {
            coord: 266,
            sprite: null
        },
        9: {
            coord: 218 - 68,
            sprite: null,
            btncoord: 430,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }

    var game1 = {};

    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }


    game1.preload = function () {

    };

    game1.create = function () {
        game.add.sprite(0,0, 'game.background');

        var playSound = game.add.audio('play');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');

        number1 = game.add.sprite(0,230-25, 'game.number1');
        number2 = game.add.sprite(0,90-25, 'game.number2');
        number3 = game.add.sprite(0,380-25, 'game.number3');
        number4 = game.add.sprite(0,150-25, 'game.number4');
        number5 = game.add.sprite(0,315-25, 'game.number5');
        number6 = game.add.sprite(0,120-25, 'game.number6');
        number7 = game.add.sprite(0,345-25, 'game.number7');
        number8 = game.add.sprite(0,270-25, 'game.number8');
        number9 = game.add.sprite(0,200-25, 'game.number9');

        nt0 = game.add.sprite(600-97,440, 'game.nt0');
        nt0 = game.add.sprite(582-97,440, 'game.nt0');
        nt8 = game.add.sprite(566-97,440, 'game.nt8');
        nt1 = game.add.sprite(550-97,440, 'game.nt1');

        number0_table1 = game.add.sprite(285-97,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(495-97,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(690-97,7, 'game.number0_table1');

        var positions = [
            game.world.centerX - 225,
            game.world.centerX - 113,
            game.world.centerX - 1,
            game.world.centerX + 111,
            game.world.centerX + 223
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 16, 96, 320, 'game.bar');
            bars[i].anchor.setTo(0.5, 0.5);
            bars[i].tilePosition.y = randomNumber(0, 8) * 112 - 8;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        win1 = game.add.sprite(40, 220, 'game.win1');
        win2 = game.add.sprite(40, 80, 'game.win2');
        win3 = game.add.sprite(40, 367, 'game.win3');
        win4 = game.add.sprite(40, 140, 'game.win4');
        win5 = game.add.sprite(40, 110, 'game.win5');
        win6 = game.add.sprite(40, 110, 'game.win6');
        win7 = game.add.sprite(40, 241, 'game.win7');
        win8 = game.add.sprite(40, 260, 'game.win8');
        win9 = game.add.sprite(40, 125, 'game.win9');
        win1.visible = false;
        win2.visible = false;
        win3.visible = false;
        win4.visible = false;
        win5.visible = false;
        win6.visible = false;
        win7.visible = false;
        win8.visible = false;
        win9.visible = false;

        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(40, lines[i].coord - 20, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 510, 'btnline' + i);
                lines[i].button.scale.setTo(0.8,0.8);
                lines[i].button.inputEnabled = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {
                        hideLines();
                        preselectLine(n);
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                    lines[n].button.events.onInputOut.add(function () {
                        lines[n].button.loadTexture('btnline' + n);
                    }, this);
                    lines[n].button.events.onInputOver.add(function () {
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                })(i);
            }
        }

        preselectLine(9);

        button = game.add.sprite(588, 228, 'game.start');

        win1_dotted = game.add.sprite(40, 220, 'game.win1_dotted');
        win2_dotted = game.add.sprite(40, 80, 'game.win2_dotted');
        win3_dotted = game.add.sprite(40, 367, 'game.win3_dotted');
        win4_dotted = game.add.sprite(40, 140, 'game.win4_dotted');
        win5_dotted = game.add.sprite(40, 110, 'game.win5_dotted');
        win6_dotted = game.add.sprite(40, 110, 'game.win6_dotted');
        win7_dotted = game.add.sprite(40, 241, 'game.win7_dotted');
        win8_dotted = game.add.sprite(40, 260, 'game.win8_dotted');
        win9_dotted = game.add.sprite(40, 125, 'game.win9_dotted');
        win1_dotted.visible = false;
        win2_dotted.visible = false;
        win3_dotted.visible = false;
        win4_dotted.visible = false;
        win5_dotted.visible = false;
        win6_dotted.visible = false;
        win7_dotted.visible = false;
        win8_dotted.visible = false;
        win9_dotted.visible = false;

        //кнопки
        double = game.add.sprite(550, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
            win1_dotted.visible = true;
            win2_dotted.visible = true;
            win3_dotted.visible = true;
            win4_dotted.visible = true;
            win5_dotted.visible = true;
            win6_dotted.visible = true;
            win7_dotted.visible = true;
            win8_dotted.visible = true;
            win9_dotted.visible = true;
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
            win1_dotted.visible = false;
            win2_dotted.visible = false;
            win3_dotted.visible = false;
            win4_dotted.visible = false;
            win5_dotted.visible = false;
            win6_dotted.visible = false;
            win7_dotted.visible = false;
            win8_dotted.visible = false;
            win9_dotted.visible = false;
        });

        bet1 = game.add.sprite(546, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputDown.add(function(){
            bet1.loadTexture('game.bet1_p');
            document.getElementById('betMode').style.display = 'block';
            widthVisibleZone = $('.betWrapper .visibleZone').height();
            console.log(widthVisibleZone);
            $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
            $('canvas').css('display', 'none');
        });
        bet1.events.onInputUp.add(function(){
            bet1.loadTexture('bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });

        var dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
            //game.state.start('game4');
        });

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });


        function buttonClicked() {
            if (spinning) {
                return;
            }
            button.loadTexture('game.start_p');

        }

        function buttonRelease() {
            if (spinning) {
                return;
            }
            hideLines();
            barsCurrentSpins = [0, 0, 0, 0, 0];
            spinningBars = bars.length;
            spinning = true;
            playSound.play();
        }

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);



        createLevelButtons();

        //создание анимаций

        man1 = game.add.sprite(225-97,393-25, 'game.man1');
        man2 = game.add.sprite(225-97,393-25, 'game.man2');
        manR1 = game.add.sprite(97,390, 'game.manR1');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;


        

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        cat = game.add.sprite(0,409-25, 'game.cat');
        super_key_in_tablo = game.add.sprite(542-97,439-25, 'game.super_key_in_tablo');

        //n1 = game.add.sprite(630,442, 'game.n1');

        //создание анимации для человека
        
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        super_key_in_tablo.animations.add('super_key_in_tablo', [0,1,2,3,4,5,6,7,8,9,10], 1, true);

        //n1.animations.add('n1', [0,1], 1, true);

        
        cat.animations.getAnimation('cat').play();
        super_key_in_tablo.animations.getAnimation('super_key_in_tablo').play();

        //n1.animations.getAnimation('n1').play();

    };

    game1.update = function () {
        if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 112;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('game.start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {
                    preselectLine(3);
                    tadaSound.play();
                } else {
                    /*flashNamber1 = game.add.sprite(0,209, 'game.flashNamber1');
                    flashNamber1.animations.add('flashNamber1', [0,1], 1, false);
                    flashNamber1.animations.getAnimation('flashNamber1').play();*/
                }
            }
        }
    };

    game.state.add('game1', game1);


})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
        //Добавление фона
        background = game.add.sprite(0,34, 'game.background2');
        background2 = game.add.sprite(0,0, 'game.background5');

        arrowSound = game.add.audio('game.arrowSound');
        arrowSound.loop = true;
        arrowSound.play();

        openKeySound = game.add.audio('game.openKeySound');
        openKeyBoxLoseSound = game.add.audio('game.openKeyBoxLoseSound');
        openKeyBoxPreLoseSound = game.add.audio('game.openKeyBoxPreLoseSound');
        openKeyBoxWinSound = game.add.audio('game.openKeyBoxWinSound');

        //кнопки

        /*startButton = game.add.sprite(544, 188, 'game.start');
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputDown.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });

        double = game.add.sprite(546, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
            arrowSound.stop();
            openKeySound.play();

            lock5.visible = false;
            arrowRedBlue5.visible = false;

            var findAKey = game.add.sprite(369-97,329-25, 'game.findAKey');
            findAKey.animations.add('findAKey', [0,1], 1.5, true);
            findAKey.visible = false;
            findAKey.animations.getAnimation('findAKey').play();

            var openLock2 = game.add.sprite(400-97,329-25, 'game.openLock2');
            openLock2.animations.add('openLock2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, false);
            openLock2.visible = true;
            openLock2.animations.getAnimation('openLock2').play().onComplete.add(function(){
                openLock2.visible = false;
                findAKey.visible = true;
            });
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
        });

        bet1 = game.add.sprite(548, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputOver.add(function(){
            bet1.loadTexture('game.bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });*/

        /*var dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
            //game.state.start('game4');
        });*/

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        //создание цифр для верхнего табло
        number0_table1 = game.add.sprite(285-97,32-25, 'game.number0_table1');
        number0_table1 = game.add.sprite(495-97,32-25, 'game.number0_table1');
        number0_table1 = game.add.sprite(690-97,32-25, 'game.number0_table1');

        //lock
        lock1 = game.add.sprite(400-97,281-25, 'game.lock');
        lock2 = game.add.sprite(400-97,233-25, 'game.lock');
        lock3 = game.add.sprite(400-97,185-25, 'game.lock');
        lock4 = game.add.sprite(400-97,137-25, 'game.lock');
        lock5 = game.add.sprite(400-97,329-25, 'game.lock');

        var i_box_for_keys = 0;
        box_for_keys = game.add.sprite(270-97,259-25, 'game.box_for_keys');
        box_for_keys.inputEnabled = true;
        box_for_keys.input.useHandCursor = true;
        box_for_keys.events.onInputDown.add(function(){
            
            if(i_box_for_keys == 0){
                lockDisplay();
                setTimeout("unlockDisplay();", 3000);
                i_box_for_keys = 1;
                arrowSound.stop();
                openKeyBoxLoseSound.play();
                var openLoseLeftCase = game.add.sprite(175-97,291-25, 'game.openLoseLeftCase');
                openLoseLeftCase.animations.add('openLoseLeftCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
                openLoseLeftCase.visible = true;
                openLoseLeftCase.animations.getAnimation('openLoseLeftCase').play().onComplete.add(function(){
                    openLoseLeftCase.visible = false;
                });

                openKeyBoxLoseSound.play();
                var openLoseRightCase = game.add.sprite(465-97,291-25, 'game.openLoseRightCase');
                openLoseRightCase.animations.add('openLoseRightCase', [0,1,2,3,4,4,4,4,4,4,4], 5, false);
                openLoseRightCase.visible = true;
                openLoseRightCase.animations.getAnimation('openLoseRightCase').play().onComplete.add(function(){
                    openLoseRightCase.visible = false;
                });
            } else {
                lockDisplay();
                setTimeout("unlockDisplay();", 7000);

                i_box_for_keys = 0;

                arrowSound.stop();
                openKeyBoxWinSound.play();
                openKeyBoxPreLoseSound.play();
                var openWinRightCase = game.add.sprite(465-97,291-25, 'game.openWinRightCase');
                openWinRightCase.animations.add('openWinRightCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                openWinRightCase.visible = true;
                openWinRightCase.animations.getAnimation('openWinRightCase').play().onComplete.add(function(){
                    openWinRightCase.visible = false;
                });

                openKeyBoxWinSound.play();
                var openWinLeftCase = game.add.sprite(175-97,291-25, 'game.openWinLeftCase');
                openWinLeftCase.animations.add('openWinLeftCase', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                openWinLeftCase.visible = true;
                openWinLeftCase.animations.getAnimation('openWinLeftCase').play().onComplete.add(function(){
                    openWinLeftCase.visible = false;

                    openKeySound.play();
                    lock5.visible = false;
                    arrowRedBlue5.visible = false;
                    var findAKey = game.add.sprite(369-97,329-25, 'game.findAKey');
                    findAKey.animations.add('findAKey', [0,1], 1.5, true);
                    findAKey.visible = false;
                    findAKey.animations.getAnimation('findAKey').play();
                    var openLock2 = game.add.sprite(400-97,329-25, 'game.openLock2');
                    openLock2.animations.add('openLock2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, false);
                    openLock2.visible = true;
                    openLock2.animations.getAnimation('openLock2').play().onComplete.add(function(){
                        openLock2.visible = false;
                        findAKey.visible = true;
                        game.state.start('game1');
                    });
                });
            }

        });

        //создание анимаций
        man1 = game.add.sprite(225-97,393-25, 'game.man1');
        man2 = game.add.sprite(225-97,393-25, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);
        
        move_super_key1 = game.add.sprite(537-95,109-23, 'game.move_super_key1');
        move_super_key1.inputEnabled = true;
        move_super_key1.input.useHandCursor = true;
        move_super_key1.events.onInputDown.add(function(){
            if(check_super_key == 0) {
                game.add.sprite(396,369, 'game.small_red_key');
                check_super_key = 1;
            }
        });
        move_super_key1.animations.add('move_super_key1', [0,1,2,3,4,5,6,7,8,9,10,11,12,14,16,18,20,22,24,26,28,30,32], 12, true);

        move_super_key2 = game.add.sprite(537-95,109-23, 'game.move_super_key2');
        move_super_key2.inputEnabled = true;
        move_super_key2.input.useHandCursor = true;
        move_super_key2.events.onInputDown.add(function(){
            if(check_super_key == 0) {
                game.add.sprite(396,369, 'game.small_red_key');
                check_super_key = 1;
            }
        });
        move_super_key2.animations.add('move_super_key2', [1,2,3,4], 12, true);

        move_super_key1.visible = false;
        move_super_key2.visible = false;

        var superkeyNumber = 1;

        function superkeyAnim(){
            if(superkeyNumber == 1){
                move_super_key1.visible = true;
                move_super_key1.animations.getAnimation('move_super_key1').play().onComplete.add(function(){
                    move_super_key1.visible = false;
                    superkeyNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                move_super_key2.visible = true;
                move_super_key2.animations.getAnimation('move_super_key2').play().onComplete.add(function(){
                    move_super_key2.visible = false;
                    superkeyNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        superkeyAnim(superkeyNumber);

        cat = game.add.sprite(97-97,409-25, 'game.cat');
        arrowRedBlue1 = game.add.sprite(367-97,115-25, 'game.arrowRedBlue');
        arrowRedBlue2 = game.add.sprite(367-97,164-25, 'game.arrowRedBlue');
        arrowRedBlue3 = game.add.sprite(367-97,212-25, 'game.arrowRedBlue');
        arrowRedBlue4 = game.add.sprite(367-97,259-25, 'game.arrowRedBlue');
        arrowRedBlue5 = game.add.sprite(367-97,308-25, 'game.arrowRedBlue');
        
        cat.animations.add('cat', [0,1,2,3,4,5], 2, true);
        arrowRedBlue1.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue2.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue3.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue4.animations.add('arrowRedBlue', [0,1], 1.5, true);
        arrowRedBlue5.animations.add('arrowRedBlue', [0,1], 1.5, true);

        
        cat.animations.getAnimation('cat').play();
        arrowRedBlue1.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue2.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue3.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue4.animations.getAnimation('arrowRedBlue').play();
        arrowRedBlue5.animations.getAnimation('arrowRedBlue').play();


        createLevelButtons();
    };

    game2.update = function () {
        /*if(superkey.position.y >= 20){
            superkey.body.velocity.y=-50;
        } else {
            superkey.body.velocity.y=50;
        }*/
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

function randomCard() {
    var arr = ['2b','3b','4b','5b','6b','7b','8b','9b','10b','jb','qb','kb','ab','2ch','3ch','4ch','5ch','6ch','7ch','8ch','9ch','10ch','jch','qch','kch','ach','2k','3k','4k','5k','6k','7k','8k','9k',
        '10k','jk','qk','kk','ak','2p','3p','4p','5p','6p','7p','8p','9p','10p','jp','qp','kp','ap','joker'];

    var rand = Math.floor(Math.random() * arr.length);

    return 'game.card_'+arr[rand];
}

function hidePick() {
    pick2.visible = false;
    pick3.visible = false;
    pick4.visible = false;
    pick5.visible = false;
}

function openCardSound() {
    openCardAudio.play();
}

function lineButtonHide() {
    line3 = game.add.sprite(295,510, 'game.line3');
}

function lineButtonShow() {
    line3.visible = true;
    line5.visible = true;
    line7.visible = true;
    line9.visible = true;

    line3x.visible = false;
    line5x.visible = false;
    line7x.visible = false;
    line9x.visible = false;
}

function lineButtonHide() {
    line3.visible = false;
    line5.visible = false;
    line7.visible = false;
    line9.visible = false;

    line3x.visible = true;
    line5x.visible = true;
    line7x.visible = true;
    line9x.visible = true;
}

(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {

        lockDisplay();
        setTimeout("unlockDisplay();",500);

        background = game.add.sprite(0,0, 'game.background');
        cards_bg = game.add.sprite(0,65, 'game.cards_bg');

        function humanThought_forward() {
            humanThought = game.add.sprite(325-95,305-25, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,2], 2, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_loss() {
            humanThought = game.add.sprite(325-95,305-25, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,1], 2, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_win() {
            humanThought = game.add.sprite(325-95,305-25, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,0], 2, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }

        /*var dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
            //game.state.start('game4');
        });*/

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });



        //создание цифр для верхнего табло
        number0_table1 = game.add.sprite(285-97,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(495-97,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(690-97,7, 'game.number0_table1');



        //изображения для данной страницы


        openCardAudio = game.add.audio("game.openCardAudio");
        winCards = game.add.audio("game.winCards");

        setTimeout('openCardSound(); card1 = game.add.sprite(130-97,137-25, randomCard());',500);

        card1 = game.add.sprite(130-97,137-25, 'game.card_garage');
        card2 = game.add.sprite(263-97,137-25, 'game.card_garage');
        card2.inputEnabled = true;
        card2.input.useHandCursor = true;
        card2.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            humanThought_win();

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            
            setTimeout(' card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick();',
                3000);
        });
        card3 = game.add.sprite(373-97,137-25, 'game.card_garage');
        card3.inputEnabled = true;
        card3.input.useHandCursor = true;
        card3.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_loss();
            setTimeout('unlockDisplay();',3000);
            card3.loadTexture(randomCard());
            openCardSound();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick(); game.state.start("game1");',
                3000);

        });
        card4 = game.add.sprite(483-97,137-25, 'game.card_garage');
        card4.inputEnabled = true;
        card4.input.useHandCursor = true;
        card4.events.onInputDown.add(function(){
            humanThought_win();
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick();',
                3000);
        });
        card5 = game.add.sprite(593-97,137-25, 'game.card_garage');
        card5.inputEnabled = true;
        card5.input.useHandCursor = true;
        card5.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_loss();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick();  game.state.start("game1");',
                3000);
        });

        pick2 = game.add.sprite(605-95,300-25, 'game.pick');
        pick3 = game.add.sprite(495-95,300-25, 'game.pick');
        pick4 = game.add.sprite(385-95,300-25, 'game.pick');
        pick5 = game.add.sprite(275-95,300-25, 'game.pick');
        pick2.visible = false;
        pick3.visible = false;
        pick4.visible = false;
        pick5.visible = false;

        //создание анимаций
        man1 = game.add.sprite(225-97,393-25, 'game.man1');
        man2 = game.add.sprite(225-97,393-25, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        cat = game.add.sprite(97-95,409-25, 'game.cat');

        //создание анимации для человека
        
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        
        cat.animations.getAnimation('cat').play();

        //кнопки

        /*startButton = game.add.sprite(544, 188, 'game.start');
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputDown.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });

        double = game.add.sprite(548, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
        });

        bet1 = game.add.sprite(548, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputOver.add(function(){
            bet1.loadTexture('game.bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });*/

        createLevelButtons();
    };

    game3.update = function () {

    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

function randomBox() {
    //добавление массивов изображений
    var arr = [];

    var boxClock = 'game.clockBox';
    var boxHammer = 'game.HummerBox';
    var boxJack = 'game.jackBox';
    var boxFlashlight = 'game.franchBox';
    var boxSaw = 'game.sawBox';
    var boxHammer2 = 'game.hammer2Box';
    var boxWrench = 'game.flashBox';
    var boxDatail = 'game.detailBox';
    var boxPolice = 'game.policeBox';

    arr.push(boxClock,boxHammer,boxJack,boxFlashlight,boxSaw,boxHammer2,boxWrench,boxDatail,boxPolice);

    var rand = Math.floor(Math.random() * arr.length);

    return arr[rand];
}

(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {


        //Добавление фона
        background2 = game.add.sprite(0,0, 'game.background5');
        background2 = game.add.sprite(0,34, 'game.background3');

        red_column_1 = game.add.sprite(305-97,153-24, 'game.red_column_1_1');
        red_column_2 = game.add.sprite(401-97,153-24, 'game.red_column_2_1');
        red_column_3 = game.add.sprite(497-97,153-24, 'game.red_column_3_1');

        openBoxSound = game.add.sound('game.openBoxSound');
        openPoliceBox = game.add.sound('game.openPoliceBox');
        preOpenBox = game.add.sound('game.preOpenBox');

        //создание цифр для верхнего табло
        number0_table1 = game.add.sprite(285-95,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(495-95,7, 'game.number0_table1');
        number0_table1 = game.add.sprite(690-95,7, 'game.number0_table1');

        box_1 = game.add.sprite(130-95,185-25, 'game.box_1');
        box_1.inputEnabled = true;
        box_1.input.useHandCursor = true;
        box_1.events.onInputDown.add(function(){
            preOpenBox.play();
            box = game.add.sprite(130-95,185-25, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(200-95,320-25, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_2');
            });
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
            box_1.inputEnabled = false;
            box_1.input.useHandCursor = false;
        });

        box2 = game.add.sprite(230-95,185-25, randomBox());
        box2.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);

        box_2 = game.add.sprite(230-95,185-25, 'game.box_2');
        box_2.inputEnabled = true;
        box_2.input.useHandCursor = true;
        box_2.events.onInputDown.add(function(){
            preOpenBox.play();
            lockDisplay();
            setTimeout('unlockDisplay()',2000);

            box2.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(300-95,320-25, 'game.number1_for_box');
                red_column_2.loadTexture('game.red_column_2_2');
            });
            box_2.visible = false;
            box_2_number.visible = false;
            box_2.inputEnabled = false;
            box_2.input.useHandCursor = false;
        });

        box_3 = game.add.sprite(330-95,185-25, 'game.box_3');
        box_3.inputEnabled = true;
        box_3.input.useHandCursor = true;
        box_3.events.onInputDown.add(function(){
            preOpenBox.play();
            box = game.add.sprite(330-95,185-25, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(400-95,320-25, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_3');
            });
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
            box_3.inputEnabled = false;
            box_3.input.useHandCursor = false;
        });

        box_4 = game.add.sprite(430-95,185-25, 'game.box_4');
        box_4.inputEnabled = true;
        box_4.input.useHandCursor = true;
        box_4.events.onInputDown.add(function(){
            preOpenBox.play();
            box = game.add.sprite(430-95,185-25, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(500-95,320-25, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_2');
            });
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
            box_4.inputEnabled = false;
            box_4.input.useHandCursor = false;
        });

        box_5 = game.add.sprite(530-95,185-25, 'game.box_5');
        box_5.inputEnabled = true;
        box_5.input.useHandCursor = true;
        box_5.events.onInputDown.add(function(){
            //openBoxSound.play();
            preOpenBox.play();
            box = game.add.sprite(530-97,185-25, randomBox());
            box.animations.add('box', [0,1,1,2,3,4,5,6,7,8,9,10], 7, false);
            box.animations.getAnimation('box').play().onComplete.add(function () {
                openBoxSound.play();
                game.add.sprite(600-97,320-25, 'game.number1_for_box');
                red_column_1.loadTexture('game.red_column_1_2');
            });
            lockDisplay();
            setTimeout('unlockDisplay()',2000);
            box_5.inputEnabled = false;
            box_5.input.useHandCursor = false;
        });

        box_1_number = game.add.sprite(130-95,185-25, 'game.box_1_number');
        box_2_number = game.add.sprite(230-95,185-25, 'game.box_2_number');
        box_3_number = game.add.sprite(330-95,185-25, 'game.box_3_number');
        box_4_number = game.add.sprite(430-95,185-25, 'game.box_4_number');
        box_5_number = game.add.sprite(530-95,185-25, 'game.box_5_number');
        
        box_1_number.animations.add('box_1_number', [0,1], 1, true);
        box_2_number.animations.add('box_2_number', [0,1], 1, true);
        box_3_number.animations.add('box_3_number', [0,1], 1, true);
        box_4_number.animations.add('box_4_number', [0,1], 1, true);
        box_5_number.animations.add('box_5_number', [0,1], 1, true);
        box_1_number.animations.getAnimation('box_1_number').play();
        box_2_number.animations.getAnimation('box_2_number').play();
        box_3_number.animations.getAnimation('box_3_number').play();
        box_4_number.animations.getAnimation('box_4_number').play();
        box_5_number.animations.getAnimation('box_5_number').play();



        //создание анимаций
        man1 = game.add.sprite(225-97,393-25, 'game.man1');
        man2 = game.add.sprite(225-97,393-25, 'game.man2');

        man1.animations.add('man1', [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,11,11,12,12,13,13,14,14,15,16,17,18,19,20], 5, false);
        man2.animations.add('man2', [1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
        man1.visible = false;
        man2.visible = false;

        var manNumber = 1;
        function manAnim(manNumber) {
            if(manNumber == 1){
                man1.visible = true;
                man1.animations.getAnimation('man1').play().onComplete.add(function(){
                    man1.visible = false;
                    manNumber = 2;
                    manAnim(manNumber);
                });
            } else {
                man2.visible = true;
                man2.animations.getAnimation('man2').play().onComplete.add(function(){
                    man2.visible = false;
                    manNumber = 1;
                    manAnim(manNumber);
                });
            }
        }

        manAnim(manNumber);

        cat = game.add.sprite(0,409-25, 'game.cat');

        //создание анимации для человека
        
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 5, true);
        
        cat.animations.getAnimation('cat').play();

        //кнопки

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        /*startButton = game.add.sprite(544, 188, 'game.start');
        startButton.inputEnabled = true;
        startButton.input.useHandCursor = true;
        startButton.events.onInputDown.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputOver.add(function(){
            startButton.loadTexture('game.start_p');
        });
        startButton.events.onInputUp.add(function(){
            startButton.loadTexture('game.start');
        });

        double = game.add.sprite(546, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
        });

        bet1 = game.add.sprite(548, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputOver.add(function(){
            bet1.loadTexture('game.bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });*/


        //

        createLevelButtons();
    };

    game4.update = function () {

    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {

        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.load.image('device', 'landscape.png');

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        //game.scale.setupScale(800, 600);

        function preloadMonkey() {
            game.load.image('monkey1', 'monkey1/monkey.png');
            game.load.image('monkey_left', 'monkey1/monkey_left.png');
            game.load.spritesheet('monkey_eyes_around', 'monkey1/eyes_around.png', 35, 9, 8);
            game.load.spritesheet('monkey_eyes_close', 'monkey1/eyes_close.png', 34, 9, 7);
            game.load.spritesheet('monkey_lips_move', 'monkey1/lips_move.png', 60, 50, 7);
            game.load.spritesheet('monkey_banan_move', 'monkey1/banan_move.png', 124, 99, 8);
            game.load.spritesheet('monkey_see_slot', 'monkey1/see_slot.png', 116, 95, 2);
            game.load.spritesheet('monkey_set_slot', 'monkey1/set_slot.png', 140, 95, 3);
            game.load.image('monkey1_h', 'monkey1/monkey_h.png');
            game.load.image('monkey_see_slot_h', 'monkey1/see_slot_h.png');
            game.load.spritesheet('monkey_banan_move_h', 'monkey1/banan_move_h.png', 100, 41, 8);
        }

        function preloadLevelButtons() {
            game.load.image('btnline11', 'lines/line11.png');
            game.load.image('btnline_p11', 'lines/line11_p.png');
            game.load.image('btnline13', 'lines/line13.png');
            game.load.image('btnline_p13', 'lines/line13_p.png');
            game.load.image('btnline15', 'lines/line15.png');
            game.load.image('btnline_p15', 'lines/line15_p.png');
        }

        game.load.image('bars', 'bars.png');
        game.load.image('collect', 'cards/collect.png');
        if(!isMobile) {
            game.load.image('start', 'start.png');
            game.load.image('start_p', 'start_p.png');
            game.load.image('start_d', 'start_d.png');
        } else {
            game.load.image('start', 'spin.png');
            game.load.image('start_p', 'spin_p.png');
            game.load.image('start_d', 'spin_d.png');
        }

        game.load.image('bet1', 'bet1.png');
        game.load.image('bet1_p', 'bet1_p.png');
        game.load.image('home', 'home.png');
        game.load.image('home_p', 'home_p.png');
        game.load.image('dollar', 'dollar.png');
        game.load.image('gear', 'gear.png');
        game.load.image('double', 'double.png');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        preloadMonkey();
        preloadLevelButtons();


        game.load.image('dealer', 'cards/dealer.png');
        game.load.image('pick', 'cards/pick.png');
        game.load.image('card_back', 'cards/cards/back.png');
        game.load.image('shlem', 'cards/shlem.png');
        game.load.spritesheet('monkey2', 'cards/monkey.png', 182, 209, 15);
        game.load.spritesheet('message', 'cards/message.png', 179, 65, 5);
        var cardValues = {
            1: 11,
            2: 7,
            3: 3,
            4: 14,
            5: 13
        };
        for(var i in cardValues) {
            game.load.image('card_'+i, 'cards/cards/'+i+'.png');
        }

        game.load.audio('opencard', 'cards/opencard.wav');
        game.load.audio('openuser', 'cards/openuser.wav');

        game.load.image('background3', 'bonus/back.png');
        game.load.spritesheet('rope', 'bonus/rope.png', 21, 335, 17);
        game.load.audio('b1_keypress', 'bonus/b1_keypress.wav');
        game.load.audio('b1_prizedown', 'bonus/b1_prizedwn.wav');
        game.load.audio('b1_shit', 'bonus/b1_shit.wav');

        for(var i=0; i<=3; ++i) {
            game.load.image('prize_' + i, 'bonus/prizes/' + i + '.png');
        }

        game.load.spritesheet('monkey_stand', 'bonus/monkey_take.png', 183, 185, 12);
        game.load.spritesheet('monkey_stand_h', 'bonus/monkey_take_h.png', 183, 190, 12);
        game.load.spritesheet('monkey_eat', 'bonus/monkey_eat.png', 138, 220, 14);
        game.load.spritesheet('monkey_eat_h', 'bonus/monkey_eat_h.png', 138, 220, 14);
        game.load.spritesheet('monkey_stun', 'bonus/monkey_stun.png', 227, 244, 5);

        
        /*GARAGE*/
        game.load.image('game.background', 'img/shape394.png');
        game.load.image('game.background2', 'img/image558.png');
        game.load.image('game.background3', 'img/shape777.png');
        game.load.image('game.background4', 'img/slot_bg.png');
        game.load.image('game.background5', 'img/image543.png');

        /*game.load.image('game.start', 'img/start.svg');
        game.load.image('game.start_p', 'img/start_p.svg');
        game.load.image('game.start_d', 'img/start_d.svg');*/

        game.load.spritesheet('game.man1', 'img/man1.png', 192, 112);
        game.load.spritesheet('game.man2', 'img/man2.png', 192, 112);

        game.load.spritesheet('game.cat', 'img/cat.png', 128, 96);
        game.load.spritesheet('game.n1', 'img/n1.png',16,16);
        game.load.spritesheet('game.arrowRedBlue', 'img/arrowRedBlue.png',96,48);
        game.load.spritesheet('game.humanThought', 'img/humanThought.png',96,96);


        game.load.image('game.start', 'spin.png');
        game.load.image('game.start_p', 'spin_p.png');
        game.load.image('game.start_d', 'spin_d.png');
        game.load.image('game.selectGame', 'img/shape1420.svg');
        game.load.image('game.selectGame_p', 'img/shape1422.svg');
        game.load.image('game.selectGame_d', 'img/shape1426.svg');
        game.load.image('game.payTable', 'img/shape1429.svg');
        game.load.image('game.payTable_p', 'img/shape1431.svg');
        game.load.image('game.payTable_d', 'img/shape1434.svg');
        game.load.image('game.automaricstart', 'img/shape1437.svg');
        game.load.image('game.automaricstart_p', 'img/shape1439.svg');
        game.load.image('game.automaricstart_d', 'img/shape1443.svg');
        game.load.image('game.betone', 'img/shape1472.svg');
        game.load.image('game.betone_p', 'img/shape1474.svg');
        game.load.image('game.betone_d', 'img/shape1478.svg');
        game.load.image('game.line1', 'img/shape1506.svg');
        game.load.image('game.betmax', 'img/shape1481.svg');
        game.load.image('game.betmax_p', 'img/shape1483.svg');
        game.load.image('game.betmax_d', 'img/shape1486.svg');
        game.load.image('game.line1_p', 'img/shape1508.svg');
        game.load.image('game.line1_d', 'img/shape1512.svg');
        game.load.image('game.line3', 'img/shape1497.svg');
        game.load.image('game.line3_p', 'img/shape1499.svg');
        game.load.image('game.line3_d', 'img/shape1503.svg');
        game.load.image('game.line5', 'img/shape1489.svg');
        game.load.image('game.line5_p', 'img/shape1491.svg');
        game.load.image('game.line5_d', 'img/shape1494.svg');
        game.load.image('game.line7', 'img/shape1463.svg');
        game.load.image('game.line7_p', 'img/shape1465.svg');
        game.load.image('game.line7_d', 'img/shape1469.svg');
        game.load.image('game.line9', 'img/shape1455.svg');
        game.load.image('game.line9_p', 'img/shape1457.svg');
        game.load.image('game.line9_d', 'img/shape1460.svg');

        game.load.image('game.number1', 'img/shape485.svg');
        game.load.image('game.number2', 'img/shape505.svg');
        game.load.image('game.number3', 'img/shape463.svg');
        game.load.image('game.number4', 'img/shape495.svg');
        game.load.image('game.number5', 'img/shape475.svg');
        game.load.image('game.number6', 'img/shape500.svg');
        game.load.image('game.number7', 'img/shape468.svg');
        game.load.image('game.number8', 'img/shape480.svg');
        game.load.image('game.number9', 'img/shape490.svg');

        game.load.image('game.nt0', 'img/shape199.svg');
        game.load.image('game.nt1', 'img/shape201.svg');
        game.load.image('game.nt2', 'img/shape203.svg');
        game.load.image('game.nt3', 'img/shape205.svg');
        game.load.image('game.nt4', 'img/shape207.svg');
        game.load.image('game.nt5', 'img/shape209.svg');
        game.load.image('game.nt6', 'img/shape211.svg');
        game.load.image('game.nt7', 'img/shape213.svg');
        game.load.image('game.nt8', 'img/shape215.svg');
        game.load.image('game.nt9', 'img/shape217.svg');

        game.load.image('game.number0_table1', 'img/image175.png');
        game.load.image('game.number1_table1', 'img/image177.png');
        game.load.image('game.number2_table1', 'img/image179.png');
        game.load.image('game.number3_table1', 'img/image181.png');
        game.load.image('game.number4_table1', 'img/image183.png');
        game.load.image('game.number5_table1', 'img/image185.png');
        game.load.image('game.number6_table1', 'img/image187.png');
        game.load.image('game.number7_table1', 'img/image189.png');
        game.load.image('game.number8_table1', 'img/image191.png');
        game.load.image('game.number9_table1', 'img/image193.png');

        game.load.image('game.win1', 'img/shape459.svg');
        game.load.image('game.win2', 'img/shape443.svg');
        game.load.image('game.win3', 'img/shape445.svg');
        game.load.image('game.win4', 'img/shape447.svg');
        game.load.image('game.win5', 'img/shape449.svg');
        game.load.image('game.win6', 'img/shape451.svg');
        game.load.image('game.win7', 'img/shape453.svg');
        game.load.image('game.win8', 'img/shape455.svg');
        game.load.image('game.win9', 'img/shape457.svg');

        game.load.image('game.win1_dotted', 'img/shape440.svg');
        game.load.image('game.win2_dotted', 'img/shape424.svg');
        game.load.image('game.win3_dotted', 'img/shape426.svg');
        game.load.image('game.win4_dotted', 'img/shape428.svg');
        game.load.image('game.win5_dotted', 'img/shape430.svg');
        game.load.image('game.win6_dotted', 'img/shape432.svg');
        game.load.image('game.win7_dotted', 'img/shape434.svg');
        game.load.image('game.win8_dotted', 'img/shape436.svg');
        game.load.image('game.win9_dotted', 'img/shape438.svg');


        game.load.image('game.bar', 'img/bars.png');
        game.load.image('game.bar_move', 'img/bar_move.png');

        game.load.image('game.playto', 'img/shape1333.svg');
        game.load.image('game.credits', 'img/shape1335.svg');

        //lock
        game.load.image('game.lock', 'img/shape604.png');
        game.load.image('game.box_for_keys', 'img/shape622.png');
        game.load.image('game.arrows', 'img/shape606.svg');
        game.load.image('game.find_a_kay', 'img/shape611.svg');

        game.load.image('game.cards_bg', 'img/shape222.png');
        //карты
        game.load.image('game.card_garage', 'img/shape225.png');

        game.load.image('game.card_2b', 'img/shape227.png');
        game.load.image('game.card_3b', 'img/shape229.png');
        game.load.image('game.card_4b', 'img/shape231.png');
        game.load.image('game.card_5b', 'img/shape233.png');
        game.load.image('game.card_6b', 'img/shape235.png');
        game.load.image('game.card_7b', 'img/shape237.png');
        game.load.image('game.card_8b', 'img/shape239.png');
        game.load.image('game.card_9b', 'img/shape241.png');
        game.load.image('game.card_10b', 'img/shape243.png');
        game.load.image('game.card_jb', 'img/shape245.png');
        game.load.image('game.card_qb', 'img/shape247.png');
        game.load.image('game.card_kb', 'img/shape249.png');
        game.load.image('game.card_ab', 'img/shape251.png');

        game.load.image('game.card_2ch', 'img/shape253.png');
        game.load.image('game.card_3ch', 'img/shape255.png');
        game.load.image('game.card_4ch', 'img/shape257.png');
        game.load.image('game.card_5ch', 'img/shape259.png');
        game.load.image('game.card_6ch', 'img/shape261.png');
        game.load.image('game.card_7ch', 'img/shape263.png');
        game.load.image('game.card_8ch', 'img/shape265.png');
        game.load.image('game.card_9ch', 'img/shape267.png');
        game.load.image('game.card_10ch', 'img/shape269.png');
        game.load.image('game.card_jch', 'img/shape271.png');
        game.load.image('game.card_qch', 'img/shape273.png');
        game.load.image('game.card_kch', 'img/shape275.png');
        game.load.image('game.card_ach', 'img/shape277.png');

        game.load.image('game.card_2k', 'img/shape279.png');
        game.load.image('game.card_3k', 'img/shape281.png');
        game.load.image('game.card_4k', 'img/shape283.png');
        game.load.image('game.card_5k', 'img/shape285.png');
        game.load.image('game.card_6k', 'img/shape287.png');
        game.load.image('game.card_7k', 'img/shape289.png');
        game.load.image('game.card_8k', 'img/shape291.png');
        game.load.image('game.card_9k', 'img/shape293.png');
        game.load.image('game.card_10k', 'img/shape295.png');
        game.load.image('game.card_jk', 'img/shape297.png');
        game.load.image('game.card_qk', 'img/shape299.png');
        game.load.image('game.card_kk', 'img/shape301.png');
        game.load.image('game.card_ak', 'img/shape303.png');

        game.load.image('game.card_2p', 'img/shape305.png');
        game.load.image('game.card_3p', 'img/shape307.png');
        game.load.image('game.card_4p', 'img/shape309.png');
        game.load.image('game.card_5p', 'img/shape311.png');
        game.load.image('game.card_6p', 'img/shape313.png');
        game.load.image('game.card_7p', 'img/shape315.png');
        game.load.image('game.card_8p', 'img/shape317.png');
        game.load.image('game.card_9p', 'img/shape319.png');
        game.load.image('game.card_10p', 'img/shape321.png');
        game.load.image('game.card_jp', 'img/shape323.png');
        game.load.image('game.card_qp', 'img/shape325.png');
        game.load.image('game.card_kp', 'img/shape327.png');
        game.load.image('game.card_ap', 'img/shape329.png');

        game.load.image('game.card_joker', 'img/shape331.png');
        game.load.image('game.card_red', 'img/shape223.png');
        game.load.image('game.pick', 'img/shape340.png');

        //boxes
        game.load.image('game.box_1', 'img/shape943.svg');
        game.load.image('game.box_2', 'img/shape947.svg');
        game.load.image('game.box_3', 'img/shape951.svg');
        game.load.image('game.box_4', 'img/shape955.svg');
        game.load.image('game.box_5', 'img/shape959.svg');

        game.load.image('game.red_column_1_0', 'img/shape779.png');
        game.load.image('game.red_column_1_1', 'img/shape781.png');
        game.load.image('game.red_column_1_2', 'img/shape783.png');
        game.load.image('game.red_column_1_3', 'img/shape785.png');
        game.load.image('game.red_column_1_4', 'img/shape787.png');
        game.load.image('game.red_column_1_5', 'img/shape789.png');

        game.load.image('game.red_column_2_0', 'img/shape793.png');
        game.load.image('game.red_column_2_1', 'img/shape795.png');
        game.load.image('game.red_column_2_2', 'img/shape797.png');
        game.load.image('game.red_column_2_3', 'img/shape799.png');
        game.load.image('game.red_column_2_4', 'img/shape801.png');
        game.load.image('game.red_column_2_5', 'img/shape803.png');

        game.load.image('game.red_column_3_0', 'img/shape807.png');
        game.load.image('game.red_column_3_1', 'img/shape809.png');
        game.load.image('game.red_column_3_2', 'img/shape811.png');
        game.load.image('game.red_column_3_3', 'img/shape813.png');
        game.load.image('game.red_column_3_4', 'img/shape815.png');
        game.load.image('game.red_column_3_5', 'img/shape817.png');

        var winLeftCaseArray = ['shape626.svg','shape628.svg','shape630.svg','shape632.svg','shape634.svg','shape636.svg','shape638.svg','shape640.svg','shape642.svg','shape644.svg','shape646.svg'];
        var loseLeftCaseArray = ['shape687.svg','shape689.svg','shape691.svg','shape693.svg','shape695.svg'];
        var winRightCaseArray = ['shape697.svg','shape699.svg','shape701.svg','shape703.svg','shape705.svg','shape707.svg','shape709.svg','shape711.svg','shape713.svg','shape715.svg','shape717.svg'];
        var loseRightCaseArray = ['shape722.svg','shape724.svg','shape726.svg','shape728.svg'];
        var unlockKeyArray = ['shape648.svg','shape650.svg','shape652.svg','shape654.svg','shape656.svg','shape658.svg','shape660.svg','shape662.svg','shape664.svg','shape666.svg','shape668.svg','shape670.svg','shape672.svg','shape674.svg','shape676.svg','shape678.svg','shape680.svg','shape682.svg'];
        var arrowArray = ['shape606.svg','shape608.svg'];

        winLeftCaseArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        loseLeftCaseArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        winRightCaseArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        loseRightCaseArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        unlockKeyArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        arrowArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });

        game.load.image('game.superkey', 'img/shape760.svg');

        var boxClockArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape837.svg','shape839.svg','shape841.svg','shape843.svg','shape845.svg'];
        var boxHammerArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape847.svg','shape849.svg','shape851.svg','shape853.svg','shape855.svg'];
        var boxJackArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape857.svg','shape859.svg','shape861.svg','shape863.svg','shape865.svg'];
        var boxFlashlightArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape867.svg','shape869.svg','shape871.svg','shape873.svg','shape875.svg'];
        var boxSawArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape877.svg','shape879.svg','shape881.svg','shape883.svg','shape885.svg'];
        var boxHammer2Array = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape887.svg','shape889.svg','shape891.svg','shape893.svg','shape895.svg'];
        var boxWrenchArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape897.svg','shape899.svg','shape901.svg','shape903.svg','shape905.svg'];
        var boxCogwheelArray = ['shape821.svg','shape827.svg','shape829.svg','shape831.svg','shape833.svg','shape835.svg','shape907.svg','shape909.svg','shape911.svg','shape913.svg','shape915.svg'];
        var boxPoliceArray = ['shape821.svg','shape917.svg','shape919.svg','shape921.svg','shape923.svg','shape925.svg','shape927.svg','shape929.svg','shape931.svg','shape933.svg','shape935.svg','shape937.svg','shape939.svg','shape941.svg'];

        boxClockArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxHammerArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxJackArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxFlashlightArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxCogwheelArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxSawArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxHammer2Array.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxWrenchArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });
        boxPoliceArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });

        game.load.spritesheet('game.flashingNumber1', 'img/flashingNumber1.png',641,33);
        game.load.spritesheet('game.flashingNumber2', 'img/flashingNumber2.png',641,33);
        game.load.spritesheet('game.flashingNumber3', 'img/flashingNumber3.png',641,33);
        game.load.spritesheet('game.flashingNumber4', 'img/flashingNumber4.png',641,33);
        game.load.spritesheet('game.flashingNumber5', 'img/flashingNumber5.png',641,33);
        game.load.spritesheet('game.flashingNumber6', 'img/flashingNumber6.png',641,33);
        game.load.spritesheet('game.flashingNumber7', 'img/flashingNumber7.png',641,33);
        game.load.spritesheet('game.flashingNumber8', 'img/flashingNumber8.png',641,33);
        game.load.spritesheet('game.flashingNumber9', 'img/flashingNumber9.png',641,33);

        var openKeyBoxArray = ['shape25.svg','shape27.svg','shape29.svg','shape31.svg','shape33.svg','shape35.svg','shape37.svg','shape39.svg','shape41.svg','shape43.svg','shape45.svg','shape47.svg','shape49.svg','shape51.svg','shape53.svg','shape55.svg','shape57.svg','shape59.svg','shape61.svg','shape63.svg','shape65.svg'];
        openKeyBoxArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });

        var openBoxArray = ['shape70.svg','shape72.svg','shape74.svg','shape76.svg','shape78.svg','shape80.svg','shape82.svg','shape84.svg','shape86.svg','shape88.svg','shape90.svg','shape92.svg','shape94.svg','shape96.svg','shape98.svg','shape100.svg','shape102.svg','shape104.svg','shape106.svg','shape108.svg','shape110.svg','shape112.svg','shape114.svg','shape116.svg','shape118.svg','shape120.svg','shape122.svg'];
        openBoxArray.forEach(function (item,i) {
            var caseName = item + i;
            game.load.image('game.'+caseName, 'img/'+item);
        });

        game.load.spritesheet('game.box_1_number', 'img/box_1_number.png', 177, 225);
        game.load.spritesheet('game.box_2_number', 'img/box_2_number.png', 177, 225);
        game.load.spritesheet('game.box_3_number', 'img/box_3_number.png', 177, 225);
        game.load.spritesheet('game.box_4_number', 'img/box_4_number.png', 177, 225);
        game.load.spritesheet('game.box_5_number', 'img/box_5_number.png', 177, 225);

        game.load.image('game.bet1', 'bet1.png');
        game.load.image('game.bet1_p', 'bet1_p.png');
        game.load.image('game.home', 'home.png');
        game.load.image('game.home_p', 'home_p.png');
        game.load.image('game.dollar', 'dollar.png');
        game.load.image('game.gear', 'gear.png');
        game.load.image('game.double', 'double.png');

        game.load.image('game.yellow_car','img/non_full.png');
        game.load.image('game.red_car','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');

        game.load.audio('game4.loss', 'bonus2/b2_loss.wav');
        game.load.audio('game4.win', 'bonus2/b2_win.wav');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        game.load.image('game4.back', 'bonus2/back.png');
        game.load.spritesheet('game4.cards', 'bonus2/cards.png', 147, 143);
        game.load.spritesheet('game4.monkey1', 'bonus2/monkey1.png', 193, 188);
        game.load.spritesheet('game4.monkey2', 'bonus2/monkey2.png', 147, 188);
        game.load.audio('game4.loss', 'bonus2/b2_loss.wav');
        game.load.audio('game4.win', 'bonus2/b2_win.wav');

        function preloadLevelButtons() {
            game.load.image('btnline11', 'lines/line11.png');
            game.load.image('btnline_p11', 'lines/line11_p.png');
            game.load.image('btnline13', 'lines/line13.png');
            game.load.image('btnline_p13', 'lines/line13_p.png');
            game.load.image('btnline15', 'lines/line15.png');
            game.load.image('btnline_p15', 'lines/line15_p.png');
        }

        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('bet', 'bet.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        preloadLevelButtons();

        game.load.image('background2', 'cards/back.png');
        game.load.image('shlem', 'cards/shlem.png');
        game.load.image('dealer', 'cards/dealer.png');
        game.load.image('pick', 'cards/pick.png');
        game.load.image('card_back', 'cards/cards/back.png');
        game.load.spritesheet('monkey2', 'cards/monkey.png', 182, 209, 15);
        game.load.spritesheet('message', 'cards/message.png', 179, 65, 5);
        var cardValues = {
            1: 11,
            2: 7,
            3: 3,
            4: 14,
            5: 13
        };
        for(var i in cardValues) {
            game.load.image('card_'+i, 'cards/cards/'+i+'.png');
        }

        game.load.audio('opencard', 'cards/opencard.wav');
        game.load.audio('openuser', 'cards/openuser.wav');

        game.load.image('background3', 'bonus/back.png');
        game.load.spritesheet('rope', 'bonus/rope.png', 21, 335, 17);
        game.load.audio('b1_keypress', 'bonus/b1_keypress.wav');
        game.load.audio('b1_prizedown', 'bonus/b1_prizedwn.wav');
        game.load.audio('b1_shit', 'bonus/b1_shit.wav');

        for(var i=0; i<=3; ++i) {
            game.load.image('prize_' + i, 'bonus/prizes/' + i + '.png');
        }

        game.load.audio('game.openCardAudio', 'sound/sound31.mp3');
        game.load.audio('game.arrowSound', 'sound/sound7.mp3');
        game.load.audio('game.openBoxSound', 'sound/sound17.mp3');
        game.load.audio('game.openKeySound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxLoseSound', 'sound/sound8.mp3');
        game.load.audio('game.openKeyBoxPreLoseSound', 'sound/sound4.mp3');
        game.load.audio('game.openKeyBoxPreWinSound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxWinSound', 'sound/sound4.mp3');
        game.load.audio('game.openPoliceBox', 'sound/sound10.mp3');
        game.load.audio('game.preOpenBox', 'sound/sound1.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');
        game.load.audio('game.betOneSound', 'sound/sound20.mp3');
        game.load.audio('game.betMaxSound', 'sound/sound33.mp3');



        game.load.spritesheet('game.openLockFromSlot', 'img/openLockFromSlot.png', 96, 113);
        game.load.spritesheet('game.openBoxFromSlot', 'img/openBoxFromSlot.png', 96, 113);
        game.load.spritesheet('game.openLock2', 'img/openLock2.png', 48, 48);

        game.load.spritesheet('game.openLoseLeftCase', 'img/openLoseLeftCase.png', 192, 80);
        game.load.spritesheet('game.openLoseRightCase', 'img/openLoseRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinRightCase', 'img/openWinRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinLeftCase', 'img/openWinLeftCase.png', 192, 80);
        game.load.spritesheet('game.findAKey', 'img/findAKey.png', 96, 16);

        game.load.spritesheet('game.clockBox', 'img/clockBox.png', 177, 225);
        game.load.spritesheet('game.detailBox', 'img/detailBox.png', 177, 225);
        game.load.spritesheet('game.flashBox', 'img/flashBox.png', 177, 225);
        game.load.spritesheet('game.franchBox', 'img/franchBox.png', 177, 225);
        game.load.spritesheet('game.hammer2Box', 'img/hammer2Box.png', 177, 225);
        game.load.spritesheet('game.HummerBox', 'img/HummerBox.png', 177, 225);
        game.load.spritesheet('game.jackBox', 'img/jackBox.png', 177, 225);
        game.load.spritesheet('game.policeBox', 'img/policeBox.png', 177, 225);
        game.load.spritesheet('game.sawBox', 'img/sawBox.png', 177, 225);

        game.load.image('game.number1_for_box', 'img/image177.png');
        game.load.image('game.number2_for_box', 'img/image179.png');
        game.load.image('game.number3_for_box', 'img/image181.png');
        game.load.image('game.number4_for_box', 'img/image183.png');
        game.load.image('game.number5_for_box', 'img/image185.png');
        game.load.image('game.number6_for_box', 'img/image187.png');
        game.load.image('game.number7_for_box', 'img/image189.png');
        game.load.image('game.number8_for_box', 'img/image191.png');
        game.load.image('game.number9_for_box', 'img/image193.png');
        game.load.image('game.number0_for_box', 'img/image175.png');
        game.load.image('game.number0_for_box', 'img/image175.png');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 641, 33);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 641, 33);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 641, 33);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 641, 33);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 641, 33);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 641, 33);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 641, 33);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 641, 33);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 641, 33);

        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.spritesheet('game.move_super_key1', 'img/move_super_key1.png', 119, 160);
        game.load.spritesheet('game.move_super_key2', 'img/move_super_key2.png', 119, 160);
        game.load.image('game.small_red_key', 'img/small_red_key.png');

        game.load.spritesheet('game.super_key_in_tablo', 'img/super_key_in_tablo.png', 160, 48);
        game.load.spritesheet('game.take', 'img/take.png', 64, 32);
        game.load.spritesheet('game.take_or_risk', 'img/take_or_risk.png', 128, 32);
        game.load.spritesheet('game.play_1_to', 'img/play_1_to.png', 128, 48);
        game.load.image('game.lock_bonus_game', 'img/shape1292.svg');
        game.load.image('game.box_bonus_game', 'img/shape1295.svg');

        game.load.image('game.manR1', 'img/manR1.png');
        

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();