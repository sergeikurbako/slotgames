(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

		
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
		
        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');
        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/1.png');
        game.load.image('game.number2', needUrlPath + '/img/2.png');
        game.load.image('game.number3', needUrlPath + '/img/3.png');
        game.load.image('game.number4', needUrlPath + '/img/4.png');
        game.load.image('game.number5', needUrlPath + '/img/5.png');
        game.load.image('game.number6', needUrlPath + '/img/6.png');
        game.load.image('game.number7', needUrlPath + '/img/7.png');
        game.load.image('game.number8', needUrlPath + '/img/8.png');
        game.load.image('game.number9', needUrlPath + '/img/9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shape225.png');

        game.load.image('card_0', needUrlPath + '/img/shape279.png');
        game.load.image('card_1', needUrlPath + '/img/shape281.png');
        game.load.image('card_2', needUrlPath + '/img/shape283.png');
        game.load.image('card_3', needUrlPath + '/img/shape285.png');
        game.load.image('card_4', needUrlPath + '/img/shape287.png');
        game.load.image('card_5', needUrlPath + '/img/shape289.png');
        game.load.image('card_6', needUrlPath + '/img/shape291.png');
        game.load.image('card_7', needUrlPath + '/img/shape293.png');
        game.load.image('card_8', needUrlPath + '/img/shape295.png');
        game.load.image('card_9', needUrlPath + '/img/shape297.png');
        game.load.image('card_10', needUrlPath + '/img/shape299.png');
        game.load.image('card_11', needUrlPath + '/img/shape301.png');
        game.load.image('card_12', needUrlPath + '/img/shape303.png');

        game.load.image('card_13', needUrlPath + '/img/shape305.png');
        game.load.image('card_14', needUrlPath + '/img/shape307.png');
        game.load.image('card_15', needUrlPath + '/img/shape309.png');
        game.load.image('card_16', needUrlPath + '/img/shape311.png');
        game.load.image('card_17', needUrlPath + '/img/shape313.png');
        game.load.image('card_18', needUrlPath + '/img/shape315.png');
        game.load.image('card_19', needUrlPath + '/img/shape317.png');
        game.load.image('card_20', needUrlPath + '/img/shape319.png');
        game.load.image('card_21', needUrlPath + '/img/shape321.png');
        game.load.image('card_22', needUrlPath + '/img/shape323.png');
        game.load.image('card_23', needUrlPath + '/img/shape325.png');
        game.load.image('card_24', needUrlPath + '/img/shape327.png');
        game.load.image('card_25', needUrlPath + '/img/shape329.png');

        game.load.image('card_26', needUrlPath + '/img/shape253.png');
        game.load.image('card_27', needUrlPath + '/img/shape255.png');
        game.load.image('card_28', needUrlPath + '/img/shape257.png');
        game.load.image('card_29', needUrlPath + '/img/shape259.png');
        game.load.image('card_30', needUrlPath + '/img/shape261.png');
        game.load.image('card_31', needUrlPath + '/img/shape263.png');
        game.load.image('card_32', needUrlPath + '/img/shape265.png');
        game.load.image('card_33', needUrlPath + '/img/shape267.png');
        game.load.image('card_34', needUrlPath + '/img/shape269.png');
        game.load.image('card_35', needUrlPath + '/img/shape271.png');
        game.load.image('card_36', needUrlPath + '/img/shape273.png');
        game.load.image('card_37', needUrlPath + '/img/shape275.png');
        game.load.image('card_38', needUrlPath + '/img/shape277.png');

        game.load.image('card_39', needUrlPath + '/img/shape227.png');
        game.load.image('card_40', needUrlPath + '/img/shape229.png');
        game.load.image('card_41', needUrlPath + '/img/shape231.png');
        game.load.image('card_42', needUrlPath + '/img/shape233.png');
        game.load.image('card_43', needUrlPath + '/img/shape235.png');
        game.load.image('card_44', needUrlPath + '/img/shape237.png');
        game.load.image('card_45', needUrlPath + '/img/shape239.png');
        game.load.image('card_46', needUrlPath + '/img/shape241.png');
        game.load.image('card_47', needUrlPath + '/img/shape243.png');
        game.load.image('card_48', needUrlPath + '/img/shape245.png');
        game.load.image('card_49', needUrlPath + '/img/shape247.png');
        game.load.image('card_50', needUrlPath + '/img/shape249.png');
        game.load.image('card_51', needUrlPath + '/img/shape251.png');


        game.load.image('card_52', needUrlPath + '/img/shape331.png');

        game.load.image('pick', needUrlPath + '/img/shape340.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');
        game.load.image('cell8', needUrlPath + '/img/cell9.png');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.png', 96, 112);
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/openBoxFromSlot.png', 96, 113);

        /* надписи в баре верхнего табло со счетом */
        game.load.image('topBarType1', needUrlPath + '/img/topBarType1.png'); // исходные надписи
        game.load.image('topBarType2', needUrlPath + '/img/topBarType2.png'); // надписи с win (появляется при выигрыше с влотах)
        game.load.image('topBarType3', needUrlPath + '/img/topBarType3.png'); // бар для третье игры
        game.load.image('topBarType4', needUrlPath + '/img/topBarType4.png'); // бар для четвертой игры

        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('bonusGame', needUrlPath + '/img/bonusGame.png');
        game.load.image('takeOrRisk1', needUrlPath + '/img/image474.png');
        game.load.image('take', needUrlPath + '/img/image554.png');


        /* sources */

        game.load.image('game.background', needUrlPath + '/img/canvas-bg.png');
        game.load.image('game.canvasbg', needUrlPath + '/img/canvas-bg.png');
        game.load.image('game.background2', needUrlPath + '/img/image558.png');
        game.load.image('game.background3', needUrlPath + '/img/shape777.png');
        game.load.image('game.background4', needUrlPath + '/img/slot_bg.png');
        game.load.image('topScoreGame2', needUrlPath + '/img/topBarType2.png');



        game.load.spritesheet('game.man1', needUrlPath + '/img/man1.png', 192, 112);
        game.load.spritesheet('game.man2', needUrlPath + '/img/man2.png', 192, 112);
        game.load.spritesheet('game.cat', needUrlPath + '/img/cat.png', 128, 96);
        game.load.spritesheet('game.n1', needUrlPath + '/img/n1.png',16,16);
        game.load.spritesheet('game.arrowRedBlue', needUrlPath + '/img/arrowRedBlue.png',96,48);
        game.load.spritesheet('game.humanThought', needUrlPath + '/img/humanThought.png',96,96);

        //lock
        game.load.image('game.lock', needUrlPath + '/img/shape604.png');
        game.load.image('game.box_for_keys', needUrlPath + '/img/shape622.png');
        game.load.image('game.arrows', needUrlPath + '/img/shape606.svg');
        game.load.image('game.find_a_kay', needUrlPath + '/img/shape611.svg');

        //boxes
        game.load.image('game.box_1', needUrlPath + '/img/shape943.svg');
        game.load.image('game.box_2', needUrlPath + '/img/shape947.svg');
        game.load.image('game.box_3', needUrlPath + '/img/shape951.svg');
        game.load.image('game.box_4', needUrlPath + '/img/shape955.svg');
        game.load.image('game.box_5', needUrlPath + '/img/shape959.svg');

        game.load.image('game.red_column_1_0', needUrlPath + '/img/shape779.png');
        game.load.image('game.red_column_1_1', needUrlPath + '/img/shape781.png');
        game.load.image('game.red_column_1_2', needUrlPath + '/img/shape783.png');
        game.load.image('game.red_column_1_3', needUrlPath + '/img/shape785.png');
        game.load.image('game.red_column_1_4', needUrlPath + '/img/shape787.png');
        //game.load.image('game.red_column_1_5', needUrlPath + '/img/shape789.png');

        game.load.image('game.red_column_2_0', needUrlPath + '/img/shape793.png');
        game.load.image('game.red_column_2_1', needUrlPath + '/img/shape795.png');
        game.load.image('game.red_column_2_2', needUrlPath + '/img/shape797.png');
        game.load.image('game.red_column_2_3', needUrlPath + '/img/shape799.png');
        game.load.image('game.red_column_2_4', needUrlPath + '/img/shape801.png');
        game.load.image('game.red_column_2_5', needUrlPath + '/img/shape803.png');

        game.load.image('game.red_column_3_0', needUrlPath + '/img/shape807.png');
        game.load.image('game.red_column_3_1', needUrlPath + '/img/shape809.png');
        game.load.image('game.red_column_3_2', needUrlPath + '/img/shape811.png');
        game.load.image('game.red_column_3_3', needUrlPath + '/img/shape813.png');
        game.load.image('game.red_column_3_4', needUrlPath + '/img/shape815.png');
        //game.load.image('game.red_column_3_5', needUrlPath + '/img/shape817.png');

        game.load.image('game.superkey', needUrlPath + '/img/shape760.svg');

        game.load.spritesheet('game.box_1_number', needUrlPath + '/img/box_1_number.png', 177, 225);
        game.load.spritesheet('game.box_2_number', needUrlPath + '/img/box_2_number.png', 177, 225);
        game.load.spritesheet('game.box_3_number', needUrlPath + '/img/box_3_number.png', 177, 225);
        game.load.spritesheet('game.box_4_number', needUrlPath + '/img/box_4_number.png', 177, 225);
        game.load.spritesheet('game.box_5_number', needUrlPath + '/img/box_5_number.png', 177, 225);

        // game.load.image('game.yellow_car','img/non_full.png');
        // game.load.image('game.red_car','img/non_full.png');
        // game.load.image('game.yellow_','img/non_full.png');
        // game.load.image('game.yellow_','img/non_full.png');
        // game.load.image('game.yellow_','img/non_full.png');
        // game.load.image('game.yellow_','img/non_full.png');

        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winMonkey', needUrlPath + '/sound/winCover.mp3');
        game.load.audio('game.arrowSound', needUrlPath + '/sound/sound7.mp3');
        game.load.audio('game.openBoxSound', needUrlPath + '/sound/sound17.mp3');
        game.load.audio('game.openKeySound', needUrlPath + '/sound/sound5.mp3');
        game.load.audio('game.openKeyBoxLoseSound', needUrlPath + '/sound/sound8.mp3');
        game.load.audio('game.openKeyBoxPreLoseSound', needUrlPath + '/sound/sound4.mp3');
        game.load.audio('game.openKeyBoxPreWinSound', needUrlPath + '/sound/sound5.mp3');
        game.load.audio('game.openKeyBoxWinSound', needUrlPath + '/sound/sound4.mp3');
        game.load.audio('game.openPoliceBox', needUrlPath + '/sound/sound10.mp3');
        game.load.audio('game.preOpenBox', needUrlPath + '/sound/sound1.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');
        game.load.audio('game.betOneSound', needUrlPath + '/sound/sound20.mp3');
        game.load.audio('game.betMaxSound', needUrlPath + '/sound/sound33.mp3');
        game.load.audio('takeWin', needUrlPath + '/takeWin.mp3');
        game.load.audio('takeBox', needUrlPath + '/sound/sound40.mp3');


        game.load.spritesheet('game.openLockFromSlot', needUrlPath + '/img/openLockFromSlot.png', 96, 113);
        game.load.spritesheet('game.openBoxFromSlot', needUrlPath + '/img/openBoxFromSlot.png', 96, 113);
        game.load.spritesheet('game.openLock2', needUrlPath + '/img/openLock2.png', 48, 48);

        game.load.spritesheet('game.openLoseLeftCase', needUrlPath + '/img/openLoseLeftCase.png', 192, 80);
        game.load.spritesheet('game.openLoseRightCase', needUrlPath + '/img/openLoseRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinRightCase', needUrlPath + '/img/openWinRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinLeftCase', needUrlPath + '/img/openWinLeftCase.png', 192, 80);
        game.load.spritesheet('game.findAKey', needUrlPath + '/img/findAKey.png', 96, 16);

        game.load.spritesheet('game.clockBox', needUrlPath + '/img/clockBox.png', 177, 225);
        game.load.spritesheet('game.detailBox', needUrlPath + '/img/detailBox.png', 177, 225);
        game.load.spritesheet('game.flashBox', needUrlPath + '/img/flashBox.png', 177, 225);
        game.load.spritesheet('game.franchBox', needUrlPath + '/img/franchBox.png', 177, 225);
        game.load.spritesheet('game.hammer2Box', needUrlPath + '/img/hammer2Box.png', 177, 225);
        game.load.spritesheet('game.HummerBox', needUrlPath + '/img/HummerBox.png', 177, 225);
        game.load.spritesheet('game.jackBox', needUrlPath + '/img/jackBox.png', 177, 225);
        game.load.spritesheet('game.policeBox', needUrlPath + '/img/policeBox.png', 177, 225);
        game.load.spritesheet('game.sawBox', needUrlPath + '/img/sawBox.png', 177, 225);

        game.load.image('game.number1_for_box', needUrlPath + '/img/image177.png');
        game.load.image('game.number2_for_box', needUrlPath + '/img/image179.png');
        game.load.image('game.number3_for_box', needUrlPath + '/img/image181.png');
        game.load.image('game.number4_for_box', needUrlPath + '/img/image183.png');
        game.load.image('game.number5_for_box', needUrlPath + '/img/image185.png');
        game.load.image('game.number6_for_box', needUrlPath + '/img/image187.png');
        game.load.image('game.number7_for_box', needUrlPath + '/img/image189.png');
        game.load.image('game.number8_for_box', needUrlPath + '/img/image191.png');
        game.load.image('game.number9_for_box', needUrlPath + '/img/image193.png');
        game.load.image('game.number0_for_box', needUrlPath + '/img/image175.png');
        game.load.image('game.number0_for_box', needUrlPath + '/img/image175.png');

        game.load.spritesheet('game.move_super_key1', needUrlPath + '/img/move_super_key1.png', 119, 160);
        game.load.spritesheet('game.move_super_key2', needUrlPath + '/img/move_super_key2.png', 119, 160);
        game.load.image('game.small_red_key', needUrlPath + '/img/small_red_key.png');

        game.load.spritesheet('game.super_key_in_tablo', needUrlPath + '/img/super_key_in_tablo.png', 160, 48);
        game.load.spritesheet('game.take', needUrlPath + '/img/take.png', 64, 32);
        game.load.spritesheet('game.take_or_risk', needUrlPath + '/img/take_or_risk.png', 128, 32);
        game.load.spritesheet('game.play_1_to', needUrlPath + '/img/play_1_to.png', 128, 48);
        game.load.image('game.lock_bonus_game', needUrlPath + '/img/shape1292.svg');
        game.load.image('game.box_bonus_game', needUrlPath + '/img/shape1295.svg');

        game.load.image('boxForKeyLeftC', needUrlPath + '/img/boxForKeyLeftC.png');
        game.load.image('boxForKeyRightC', needUrlPath + '/img/boxForKeyRightC.png');

        game.load.image('bg-bottom', needUrlPath + '/img/bg-bottom.png');
        game.load.image('cardBg', needUrlPath + '/img/shape222.png');

        game.load.image('winTitleGame2', needUrlPath + '/img/winTitleGame2.png');
        game.load.image('loseTitleGame2', needUrlPath + '/img/loseTitleGame2.png');

        game.load.image('game.backgroundGame1Score1', needUrlPath + '/img/topBarType1.png');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

