ropeValues = [1,2,3,4,5,1];

function game3() {
    (function () {


        var game3 = {
            soldierPositionsX : {1:110, 3:222, 5:334, 7:446},

            create : function() {
                checkGame = 3;
                isMobile = false;
                mobileX = 0;
                mobileY = 0;

                //сбрамываем необходимые переменные
                autostart = false;
                checkAutoStart = false;
                checkUpdateBalance = false;
                stepTotalWinR = 0;
                ropeStep = 0;
                checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса

                
                game3.bg = game.add.sprite(94+mobileX,54+mobileY, 'game3.bg');
                game3.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
                game.add.sprite(0,0, 'game.background');

                game3.soldierhead = game.add.sprite(94, 262, 'game2.soldierhead_animation');
                game3.soldierheadAnimation = game3.soldierhead.animations.add('game2.soldierhead', [0,1,2,3], 5, false);
                game3.soldierhead.visible = false;
                game3.soldier = game.add.sprite(94, 262, 'game2.soldier_animation');
                game3.soldierAnimation = game3.soldier.animations.add('game2.soldier', [0,1,2,3,4,3,2,1,0], 5, false);
                game3.soldier.visible = true;
                game3.soldierAnimation.onComplete.add(function(){
                    game3.soldierhead.visible = true;
                    game3.soldier.visible = false;
                    game3.soldierheadAnimation.play();
                });
                game3.soldierheadAnimation.onComplete.add(function(){
                    game3.soldierhead.visible = false;
                    game3.soldier.visible = true;
                    game3.soldierAnimation.play();
                });

                hitSound = game.add.audio('hit');
                hitSound.addMarker('hits', 0.3, 5);
                winSound = game.add.audio('winGame3');
                dynamiteSound = game.add.audio('dynamite');
                boomSound = game.add.audio('boom');
                fire_extinguisher_action_sound = game.add.audio('fire_extinguisher_action');
                if (checkHelm == true)
                    game3.big_fire = game.add.sprite(143+mobileX, 422+mobileY, 'game3.big_fire');



                //счет
                scorePosions = [[260, 26, 20], [435, 26, 20], [610, 26, 20], [475, 68, 30]];
                addScore(game, scorePosions, bet, '', balance, betline);

                //кнопки
                ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша
                buttonsGame3Mobile = []; //изображения-кнопки
                winRopeNumberPosition = [[195+50,215],[245+112,215],[245+224,215],[245+336,215],[245+448,215]];
                timeoutForShowWinRopeNumber = 3000;
                timeoutGame3ToGame4 = 4500;
                typeWinRopeNumberAnim = 0;                
                winRopeNumberSize = 22;


                ropesAnim[0] = function (ropeValues, ropeStep) {
                    var randBaba = randomNumber(1,4);
                    game3.soldierhead.visible = false;
                    game3.soldier.visible = false;

                    game3.bottle = game.add.sprite(222, 214, 'game3.bottle');
                    game3.bottle.visible = false;
                    game3.book = game.add.sprite(222, 214, 'game3.book');
                    game3.book.visible = false;
                    game3.gold = game.add.sprite(222, 214, 'game3.gold');
                    game3.goldAnim = game3.gold.animations.add('game3.gold', [0,1,2], 5, true);
                    game3.gold.visible = false;
                    game3.cup = game.add.sprite(222, 214, 'game3.cup');
                    game3.cupAnim = game3.cup.animations.add('game3.cup', [0,1,2,3,4,5,6], 5, true);
                    game3.cup.visible = false;
                    game3.dynamite = game.add.sprite(222, 214, 'game3.dynamite');
                    game3.dynamiteAnim = game3.dynamite.animations.add('game3.dynamite', [0,1,2,3,4,5,6,7], 5, false);
                    game3.dynamite.visible = false;
                    game3.dynamite2 = game.add.sprite(222, 214, 'game3.dynamite');
                    game3.dynamite2Anim = game3.dynamite2.animations.add('game3.dynamite', [0,1,2,3,4], 5, false);
                    game3.dynamite2.visible = false;
                    game3.dynamite2_f = game.add.sprite(222, 214, 'game3.dynamite');
                    game3.dynamite2_fAnim = game3.dynamite2_f.animations.add('game3.dynamite', [5,6,7], 5, false);
                    game3.dynamite2_f.visible = false;
                    game3.dynamite_off = game.add.sprite(222, 214, 'game3.dynamite_off');
                    game3.dynamite_offAnim = game3.dynamite_off.animations.add('game3.dynamite_off', [0,1], 5, false);
                    game3.dynamite_off.visible = false;
                    game3.safe_door = game.add.sprite(206, 182, 'game3.safe_door');
                    game3.safe_door_anim = game3.safe_door.animations.add('game3.safe_door', [0,1,2], 5, false);
                    game3.safe_door.visible = false;
                    game3.safe_hit = game.add.sprite(206, 182, 'game3.safe_hit_1' );
                    game3.safe_hit_anim = game3.safe_hit.animations.add('game3.safe_hit_1', [1,2], 10, false);
                    game3.safe_hit.visible = false;
                    game3.soldierStand = game.add.sprite(110, 86, 'game3.hit_safe');
                    game3.soldierStandHit = game3.soldierStand.animations.add('game3.hit_safe', [0,1,2,3,4], 5, false);
                    game3.soldier_win = game.add.sprite(110, 86, 'game3.soldier_win');
                    game3.soldier_winAnim = game3.soldier_win.animations.add('game3.soldier_win', [0,1,2,2,2], 5, false);
                    game3.soldier_win.visible = false;
                    game3.destroy_door = game.add.sprite(206, 182, 'game3.destroy_door_3');
                    game3.destroy_door.visible = false;
                    game3.boom_animation = game.add.sprite(110, 86, 'game3.boom_animation');
                    game3.boom_animationAnim = game3.boom_animation.animations.add('game3.boom_animation', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                    game3.boom_animation.visible = false;
                    game3.boom_animation_after = game.add.sprite(110, 86, 'game3.boom_animation');
                    game3.boom_animation_afterAnim = game3.boom_animation_after.animations.add('game3.boom_animation', [8,9,10], 5, true);
                    game3.boom_animation_after.visible = false;
                    game3.big_fire_extinguisher_action = game.add.sprite(110, 86, 'game3.big_fire_extinguisher_action');
                    game3.big_fire_extinguisher_actionAnim = game3.big_fire_extinguisher_action.animations.add('game3.big_fire_extinguisher_action', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
                    game3.big_fire_extinguisher_action.visible = false;
                    if (checkHelm == true){
                        game3.big_fire2 = game.add.sprite(126, 230, 'game1.big_fire');                        
                        game3.big_fire.visible = false;
                    }
                    game3.door_knob = game.add.sprite(222, 230, 'game3.door_knob');
                    game3.door_knobAnim = game3.door_knob.animations.add('game3.door_knob', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2], 15, false);
                    game3.door_knob.visible = false;

                    hitSound.play('hits');
                    game3.soldierStandHit.play().onComplete.add(function(){
                        game3.safe_hit.visible = true;
                        game3.safe_hit_anim.play().onComplete.add(function(){
                            game3.safe_hit.visible = false;
                            game3.door_knob.visible = true;
                            game3.door_knobAnim.play().onComplete.add(function(){
                                if(ropeValues[ropeStep] > 0) {
                                    game3.door_knob.visible = false;
                                    game3.safe_door.visible = true;
                                    game3.safe_door_anim.play();
                                    game3.soldierStand.visible = false;
                                    game3.soldier_win.visible = true;
                                    winSound.play();
                                    switch(randBaba) {
                                        case 1:
                                        game3.bottle.visible = true;
                                        break;
                                        case 2:
                                        game3.book.visible = true;
                                        break;
                                        case 3:
                                        console.log(3);
                                        game3.gold.visible = true;
                                        game3.goldAnim.play();
                                        break;
                                        case 4:
                                        game3.cup.visible = true;
                                        game3.cupAnim.play();
                                        break;
                                    }
                                    game3.soldier_winAnim.play().onComplete.add(function(){
                                        setTimeout('unlockDisplay()',100);
                                        if (checkHelm == true)
                                            game3.big_fire2.visible = false;
                                        game3.soldier_win.visible = false;
                                        game3.soldierAnimation.play();
                                        game3.soldier.visible = true;
                                        if (checkHelm == true){
                                            game3.big_fire.visible = true;
                                        }
                                    });
                                } else {                              
                                  game3.safe_door.visible = true;
                                  game3.safe_door_anim.play();
                                  if (checkHelm == true){
                                   game3.door_knob.visible = false;
                                   game3.dynamite2.visible = true;
                                   dynamiteSound.play();
                                   game3.dynamite2Anim.play().onComplete.add(function(){
                                    game3.soldierStand.visible = false;
                                    if (checkHelm == true)
                                        game3.big_fire2.visible = false;
                                    game3.dynamite2.visible = false;
                                    game3.dynamite2_f.visible = true;
                                    game3.dynamite2_fAnim.play().onComplete.add(function(){
                                        game3.dynamite_off.visible = true;
                                        game3.dynamite_offAnim.play();
                                    });
                                    fire_extinguisher_action_sound.play();
                                    checkHelm = false;
                                    loseTryInGame3 = true;
                                    game3.big_fire_extinguisher_action.visible = true;
                                    game3.big_fire_extinguisher_actionAnim.play().onComplete.add(function(){
                                        setTimeout('unlockDisplay()',100);
                                        game3.big_fire_extinguisher_action.visible = false;
                                        game3.soldierAnimation.play();
                                        game3.soldier.visible = true;
                                    });
                                });
                               } else {
                                   game3.door_knob.visible = false;
                                   game3.dynamite.visible = true;
                                   dynamiteSound.play();
                                   game3.dynamiteAnim.play().onComplete.add(function(){
                                    game3.soldierStand.visible = false;
                                    game3.destroy_door.visible = true;
                                    game3.boom_animation.visible = true;
                                    boomSound.play();
                                    game3.boom_animationAnim.play().onComplete.add(function(){
                                        game3.boom_animation_afterAnim.play();
                                        game3.boom_animation_after.visible = true;
                                    });
                                });
                               }
                           }

                       });
});
});
lockDisplay();
};
ropesAnim[1] = function (ropeValues, ropeStep) {
    var randBaba = randomNumber(1,4);
    console.log(checkHelm);
    game3.soldierhead.visible = false;
    game3.soldier.visible = false;

    game3.bottle = game.add.sprite(334, 214, 'game3.bottle');
    game3.bottle.visible = false;
    game3.book = game.add.sprite(334, 214, 'game3.book');
    game3.book.visible = false;
    game3.gold = game.add.sprite(334, 214, 'game3.gold');
    game3.goldAnim = game3.gold.animations.add('game3.gold', [0,1,2], 5, true);
    game3.gold.visible = false;
    game3.cup = game.add.sprite(334, 214, 'game3.cup');
    game3.cupAnim = game3.cup.animations.add('game3.cup', [0,1,2,3,4,5,6], 5, true);
    game3.cup.visible = false;
    game3.dynamite = game.add.sprite(334, 214, 'game3.dynamite');
    game3.dynamiteAnim = game3.dynamite.animations.add('game3.dynamite', [0,1,2,3,4,5,6,7], 5, false);
    game3.dynamite.visible = false;
    game3.dynamite2 = game.add.sprite(334, 214, 'game3.dynamite');
    game3.dynamite2Anim = game3.dynamite2.animations.add('game3.dynamite', [0,1,2,3,4], 5, false);
    game3.dynamite2.visible = false;
    game3.dynamite2_f = game.add.sprite(334, 214, 'game3.dynamite');
    game3.dynamite2_fAnim = game3.dynamite2_f.animations.add('game3.dynamite', [5,6,7], 5, false);
    game3.dynamite2_f.visible = false;
    game3.dynamite_off = game.add.sprite(334, 214, 'game3.dynamite_off');
    game3.dynamite_offAnim = game3.dynamite_off.animations.add('game3.dynamite_off', [0,1], 5, false);
    game3.dynamite_off.visible = false;
    game3.safe_door = game.add.sprite(318, 182, 'game3.safe_door');
    game3.safe_door_anim = game3.safe_door.animations.add('game3.safe_door', [0,1,2], 5, false);
    game3.safe_door.visible = false;
    game3.safe_hit = game.add.sprite(318, 182, 'game3.safe_hit_3' );
    game3.safe_hit_anim = game3.safe_hit.animations.add('game3.safe_hit_3', [1,2], 10, false);
    game3.safe_hit.visible = false;
    game3.soldierStand = game.add.sprite(222, 86, 'game3.hit_safe');
    game3.soldierStandHit = game3.soldierStand.animations.add('game3.hit_safe', [0,1,2,3,4], 5, false);
    game3.soldier_win = game.add.sprite(222, 86, 'game3.soldier_win');
    game3.soldier_winAnim = game3.soldier_win.animations.add('game3.soldier_win', [0,1,2,2,2], 5, false);
    game3.soldier_win.visible = false;
    game3.destroy_door = game.add.sprite(318, 182, 'game3.destroy_door_3');
    game3.destroy_door.visible = false;
    game3.boom_animation = game.add.sprite(222, 86, 'game3.boom_animation');
    game3.boom_animationAnim = game3.boom_animation.animations.add('game3.boom_animation', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.boom_animation.visible = false;
    game3.boom_animation_after = game.add.sprite(222, 86, 'game3.boom_animation');
    game3.boom_animation_afterAnim = game3.boom_animation_after.animations.add('game3.boom_animation', [8,9,10], 5, true);
    game3.boom_animation_after.visible = false;
    game3.door_knob = game.add.sprite(334, 230, 'game3.door_knob');
    game3.door_knobAnim = game3.door_knob.animations.add('game3.door_knob', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2], 15, false);
    game3.door_knob.visible = false;
    game3.big_fire_extinguisher_action = game.add.sprite(222, 86, 'game3.big_fire_extinguisher_action');
    game3.big_fire_extinguisher_actionAnim = game3.big_fire_extinguisher_action.animations.add('game3.big_fire_extinguisher_action', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.big_fire_extinguisher_action.visible = false;
    if (checkHelm == true){
        game3.big_fire2 = game.add.sprite(238, 230, 'game1.big_fire');
        game3.big_fire.visible = false;
    }

    hitSound.play('hits');
    game3.soldierStandHit.play().onComplete.add(function(){
        game3.safe_hit.visible = true;
        game3.safe_hit_anim.play().onComplete.add(function(){
            game3.safe_hit.visible = false;
            game3.door_knob.visible = true;
            game3.door_knobAnim.play().onComplete.add(function(){
                if(ropeValues[ropeStep] > 0) {
                    game3.door_knob.visible = false;
                    game3.safe_door.visible = true;
                    game3.safe_door_anim.play();
                    game3.soldierStand.visible = false;
                    game3.soldier_win.visible = true;
                    winSound.play();
                    switch(randBaba) {
                        case 1:
                        game3.bottle.visible = true;
                        break;
                        case 2:
                        game3.book.visible = true;
                        break;
                        case 3:
                        game3.gold.visible = true;
                        game3.goldAnim.play();
                        break;
                        case 4:
                        game3.cup.visible = true;
                        game3.cupAnim.play();
                        break;
                    }
                    game3.soldier_winAnim.play().onComplete.add(function(){
                        setTimeout('unlockDisplay()',100);
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.soldier_win.visible = false;
                        game3.soldierAnimation.play();
                        game3.soldier.visible = true;
                        if (checkHelm == true){
                            game3.big_fire.visible = true;
                        }
                    });
                } else {
                  game3.safe_door.visible = true;
                  game3.safe_door_anim.play();
                  if (checkHelm == true){
                     game3.door_knob.visible = false;
                     game3.dynamite2.visible = true;
                     dynamiteSound.play();
                     game3.dynamite2Anim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.dynamite2.visible = false;
                        game3.dynamite2_f.visible = true;
                        game3.dynamite2_fAnim.play().onComplete.add(function(){
                            game3.dynamite_off.visible = true;
                            game3.dynamite_offAnim.play();
                        });
                        fire_extinguisher_action_sound.play();
                        checkHelm = false;
                        loseTryInGame3 = true;
                        game3.big_fire_extinguisher_action.visible = true;
                        game3.big_fire_extinguisher_actionAnim.play().onComplete.add(function(){
                            setTimeout('unlockDisplay()',100);
                            game3.big_fire_extinguisher_action.visible = false;
                            game3.soldierAnimation.play();
                            game3.soldier.visible = true;
                        });
                    });
                 } else {
                     game3.door_knob.visible = false;
                     game3.dynamite.visible = true;
                     dynamiteSound.play();
                     game3.dynamiteAnim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        game3.destroy_door.visible = true;
                        game3.boom_animation.visible = true;
                        boomSound.play();
                        game3.boom_animationAnim.play().onComplete.add(function(){
                            game3.boom_animation_afterAnim.play();
                            game3.boom_animation_after.visible = true;
                        });
                    });
                 }
             }

         });
        });
});
lockDisplay();
};
ropesAnim[2] = function (ropeValues, ropeStep) {
    var randBaba = randomNumber(1,4);
    console.log(checkHelm);
    game3.soldierhead.visible = false;
    game3.soldier.visible = false;

    game3.bottle = game.add.sprite(446, 214, 'game3.bottle');
    game3.bottle.visible = false;
    game3.book = game.add.sprite(446, 214, 'game3.book');
    game3.book.visible = false;
    game3.gold = game.add.sprite(446, 214, 'game3.gold');
    game3.goldAnim = game3.gold.animations.add('game3.gold', [0,1,2], 5, true);
    game3.gold.visible = false;
    game3.cup = game.add.sprite(446, 214, 'game3.cup');
    game3.cupAnim = game3.cup.animations.add('game3.cup', [0,1,2,3,4,5,6], 5, true);
    game3.cup.visible = false;
    game3.dynamite = game.add.sprite(446, 214, 'game3.dynamite');
    game3.dynamiteAnim = game3.dynamite.animations.add('game3.dynamite', [0,1,2,3,4,5,6,7], 5, false);
    game3.dynamite.visible = false;
    game3.dynamite2 = game.add.sprite(446, 214, 'game3.dynamite');
    game3.dynamite2Anim = game3.dynamite2.animations.add('game3.dynamite', [0,1,2,3,4], 5, false);
    game3.dynamite2.visible = false;
    game3.dynamite2_f = game.add.sprite(446, 214, 'game3.dynamite');
    game3.dynamite2_fAnim = game3.dynamite2_f.animations.add('game3.dynamite', [5,6,7], 5, false);
    game3.dynamite2_f.visible = false;
    game3.dynamite_off = game.add.sprite(446, 214, 'game3.dynamite_off');
    game3.dynamite_offAnim = game3.dynamite_off.animations.add('game3.dynamite_off', [0,1], 5, false);
    game3.dynamite_off.visible = false;
    game3.safe_door = game.add.sprite(430, 182, 'game3.safe_door');
    game3.safe_door_anim = game3.safe_door.animations.add('game3.safe_door', [0,1,2], 5, false);
    game3.safe_door.visible = false;
    game3.safe_hit = game.add.sprite(430, 182, 'game3.safe_hit_5' );
    game3.safe_hit_anim = game3.safe_hit.animations.add('game3.safe_hit_5', [1,2], 10, false);
    game3.safe_hit.visible = false;
    game3.soldierStand = game.add.sprite(334, 86, 'game3.hit_safe');
    game3.soldierStandHit = game3.soldierStand.animations.add('game3.hit_safe', [0,1,2,3,4], 5, false);
    game3.soldier_win = game.add.sprite(334, 86, 'game3.soldier_win');
    game3.soldier_winAnim = game3.soldier_win.animations.add('game3.soldier_win', [0,1,2,2,2], 5, false);
    game3.soldier_win.visible = false;
    game3.destroy_door = game.add.sprite(430, 182, 'game3.destroy_door_3');
    game3.destroy_door.visible = false;
    game3.boom_animation = game.add.sprite(334, 86, 'game3.boom_animation');
    game3.boom_animationAnim = game3.boom_animation.animations.add('game3.boom_animation', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.boom_animation.visible = false;
    game3.boom_animation_after = game.add.sprite(334, 86, 'game3.boom_animation');
    game3.boom_animation_afterAnim = game3.boom_animation_after.animations.add('game3.boom_animation', [8,9,10], 5, true);
    game3.boom_animation_after.visible = false;
    game3.door_knob = game.add.sprite(446, 230, 'game3.door_knob');
    game3.door_knobAnim = game3.door_knob.animations.add('game3.door_knob', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2], 15, false);
    game3.door_knob.visible = false;
    game3.big_fire_extinguisher_action = game.add.sprite(334, 86, 'game3.big_fire_extinguisher_action');
    game3.big_fire_extinguisher_actionAnim = game3.big_fire_extinguisher_action.animations.add('game3.big_fire_extinguisher_action', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.big_fire_extinguisher_action.visible = false;
    if (checkHelm == true){
        game3.big_fire2 = game.add.sprite(350, 230, 'game1.big_fire');
        game3.big_fire.visible = false;
    }
    hitSound.play('hits');
    game3.soldierStandHit.play().onComplete.add(function(){
        game3.safe_hit.visible = true;
        game3.safe_hit_anim.play().onComplete.add(function(){
            game3.safe_hit.visible = false;
            game3.door_knob.visible = true;
            game3.door_knobAnim.play().onComplete.add(function(){
                if(ropeValues[ropeStep] > 0) {
                    game3.door_knob.visible = false;
                    game3.safe_door.visible = true;
                    game3.safe_door_anim.play();
                    game3.soldierStand.visible = false;
                    game3.soldier_win.visible = true;
                    winSound.play();
                    switch(randBaba) {
                        case 1:
                        game3.bottle.visible = true;
                        break;
                        case 2:
                        game3.book.visible = true;
                        break;
                        case 3:
                        game3.gold.visible = true;
                        game3.goldAnim.play();
                        break;
                        case 4:
                        game3.cup.visible = true;
                        game3.cupAnim.play();
                        break;
                    }
                    game3.soldier_winAnim.play().onComplete.add(function(){
                        setTimeout('unlockDisplay()',100);
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.soldier_win.visible = false;
                        game3.soldierAnimation.play();
                        game3.soldier.visible = true;
                        if (checkHelm == true){
                            game3.big_fire.visible = true;
                        }
                    });
                } else {
                  game3.safe_door.visible = true;
                  game3.safe_door_anim.play();
                  if (checkHelm == true){
                     game3.door_knob.visible = false;
                     game3.dynamite2.visible = true;
                     dynamiteSound.play();
                     game3.dynamite2Anim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.dynamite2.visible = false;
                        game3.dynamite2_f.visible = true;
                        game3.dynamite2_fAnim.play().onComplete.add(function(){
                            game3.dynamite_off.visible = true;
                            game3.dynamite_offAnim.play();
                        });
                        fire_extinguisher_action_sound.play();
                        checkHelm = false;
                        loseTryInGame3 = true;
                        game3.big_fire_extinguisher_action.visible = true;
                        game3.big_fire_extinguisher_actionAnim.play().onComplete.add(function(){
                            setTimeout('unlockDisplay()',100);
                            game3.big_fire_extinguisher_action.visible = false;
                            game3.soldierAnimation.play();
                            game3.soldier.visible = true;
                        });
                    });
                 } else {
                     game3.door_knob.visible = false;
                     game3.dynamite.visible = true;
                     dynamiteSound.play();
                     game3.dynamiteAnim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        game3.destroy_door.visible = true;
                        game3.boom_animation.visible = true;
                        boomSound.play();
                        game3.boom_animationAnim.play().onComplete.add(function(){
                            game3.boom_animation_afterAnim.play();
                            game3.boom_animation_after.visible = true;
                        });
                    });
                 }
             }

         });
        });
});
lockDisplay();
};
ropesAnim[3] = function (ropeValues, ropeStep) {
    var randBaba = randomNumber(1,4);

    game3.soldierhead.visible = false;
    game3.soldier.visible = false;

    game3.bottle = game.add.sprite(558, 214, 'game3.bottle');
    game3.bottle.visible = false;
    game3.book = game.add.sprite(558, 214, 'game3.book');
    game3.book.visible = false;
    game3.gold = game.add.sprite(558, 214, 'game3.gold');
    game3.goldAnim = game3.gold.animations.add('game3.gold', [0,1,2], 5, true);
    game3.gold.visible = false;
    game3.cup = game.add.sprite(558, 214, 'game3.cup');
    game3.cupAnim = game3.cup.animations.add('game3.cup', [0,1,2,3,4,5,6], 5, true);
    game3.cup.visible = false;
    game3.dynamite = game.add.sprite(558, 214, 'game3.dynamite');
    game3.dynamiteAnim = game3.dynamite.animations.add('game3.dynamite', [0,1,2,3,4,5,6,7], 5, false);
    game3.dynamite.visible = false;
    game3.dynamite2 = game.add.sprite(558, 214, 'game3.dynamite');
    game3.dynamite2Anim = game3.dynamite2.animations.add('game3.dynamite', [0,1,2,3,4], 5, false);
    game3.dynamite2.visible = false;
    game3.dynamite2_f = game.add.sprite(558, 214, 'game3.dynamite');
    game3.dynamite2_fAnim = game3.dynamite2_f.animations.add('game3.dynamite', [5,6,7], 5, false);
    game3.dynamite2_f.visible = false;
    game3.dynamite_off = game.add.sprite(558, 214, 'game3.dynamite_off');
    game3.dynamite_offAnim = game3.dynamite_off.animations.add('game3.dynamite_off', [0,1], 5, false);
    game3.dynamite_off.visible = false;
    game3.safe_door = game.add.sprite(542, 182, 'game3.safe_door');
    game3.safe_door_anim = game3.safe_door.animations.add('game3.safe_door', [0,1,2], 5, false);
    game3.safe_door.visible = false;
    game3.safe_hit = game.add.sprite(542, 182, 'game3.safe_hit_7' );
    game3.safe_hit_anim = game3.safe_hit.animations.add('game3.safe_hit_7', [1,2], 10, false);
    game3.safe_hit.visible = false;
    game3.soldierStand = game.add.sprite(446, 86, 'game3.hit_safe');
    game3.soldierStandHit = game3.soldierStand.animations.add('game3.hit_safe', [0,1,2,3,4], 5, false);
    game3.soldier_win = game.add.sprite(446, 86, 'game3.soldier_win');
    game3.soldier_winAnim = game3.soldier_win.animations.add('game3.soldier_win', [0,1,2,2,2], 5, false);
    game3.soldier_win.visible = false;
    game3.destroy_door = game.add.sprite(542, 182, 'game3.destroy_door_3');
    game3.destroy_door.visible = false;
    game3.boom_animation = game.add.sprite(446, 86, 'game3.boom_animation');
    game3.boom_animationAnim = game3.boom_animation.animations.add('game3.boom_animation', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.boom_animation.visible = false;
    game3.boom_animation_after = game.add.sprite(446, 86, 'game3.boom_animation');
    game3.boom_animation_afterAnim = game3.boom_animation_after.animations.add('game3.boom_animation', [8,9,10], 5, true);
    game3.boom_animation_after.visible = false;
    game3.door_knob = game.add.sprite(558, 230, 'game3.door_knob');
    game3.door_knobAnim = game3.door_knob.animations.add('game3.door_knob', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2], 15, false);
    game3.door_knob.visible = false;
    game3.big_fire_extinguisher_action = game.add.sprite(446, 86, 'game3.big_fire_extinguisher_action');
    game3.big_fire_extinguisher_actionAnim = game3.big_fire_extinguisher_action.animations.add('game3.big_fire_extinguisher_action', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
    game3.big_fire_extinguisher_action.visible = false;
    if (checkHelm == true){
        game3.big_fire2 = game.add.sprite(462, 230, 'game1.big_fire');
        game3.big_fire.visible = false;
    }

    hitSound.play('hits');
    game3.soldierStandHit.play().onComplete.add(function(){
        game3.safe_hit.visible = true;
        game3.safe_hit_anim.play().onComplete.add(function(){
            game3.safe_hit.visible = false;
            game3.door_knob.visible = true;
            game3.door_knobAnim.play().onComplete.add(function(){
                if(ropeValues[ropeStep] > 0) {
                    game3.door_knob.visible = false;
                    game3.safe_door.visible = true;
                    game3.safe_door_anim.play();
                    game3.soldierStand.visible = false;
                    game3.soldier_win.visible = true;
                    winSound.play();
                    switch(randBaba) {
                        case 1:
                        game3.bottle.visible = true;
                        break;
                        case 2:
                        game3.book.visible = true;
                        break;
                        case 3:
                        game3.gold.visible = true;
                        game3.goldAnim.play();
                        break;
                        case 4:
                        game3.cup.visible = true;
                        game3.cupAnim.play();
                        break;
                    }
                    game3.soldier_winAnim.play().onComplete.add(function(){
                        setTimeout('unlockDisplay()',100);
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.soldier_win.visible = false;
                        game3.soldierAnimation.play();
                        game3.soldier.visible = true;
                        if (checkHelm == true){
                            game3.big_fire.visible = true;
                        }
                    });
                } else {
                  game3.safe_door.visible = true;
                  game3.safe_door_anim.play();
                  if (checkHelm == true){
                     game3.door_knob.visible = false;
                     game3.dynamite2.visible = true;
                     dynamiteSound.play();
                     game3.dynamite2Anim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        if (checkHelm == true)
                            game3.big_fire2.visible = false;
                        game3.dynamite2.visible = false;
                        game3.dynamite2_f.visible = true;
                        game3.dynamite2_fAnim.play().onComplete.add(function(){
                            game3.dynamite_off.visible = true;
                            game3.dynamite_offAnim.play();
                        });
                        fire_extinguisher_action_sound.play();
                        checkHelm = false;
                        loseTryInGame3 = true;
                        game3.big_fire_extinguisher_action.visible = true;
                        game3.big_fire_extinguisher_actionAnim.play().onComplete.add(function(){
                            setTimeout('unlockDisplay()',100);
                            game3.big_fire_extinguisher_action.visible = false;
                            game3.soldierAnimation.play();
                            game3.soldier.visible = true;
                        });
                    });
                 } else {
                     game3.door_knob.visible = false;
                     game3.dynamite.visible = true;
                     dynamiteSound.play();
                     game3.dynamiteAnim.play().onComplete.add(function(){
                        game3.soldierStand.visible = false;
                        game3.destroy_door.visible = true;
                        game3.boom_animation.visible = true;
                        boomSound.play();
                        game3.boom_animationAnim.play().onComplete.add(function(){
                            game3.boom_animation_afterAnim.play();
                            game3.boom_animation_after.visible = true;
                        });
                    });
                 }
             }

         });
        });
});
lockDisplay();
};
full_and_sound();
addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);
},
update: function () {
    if (game.scale.isFullScreen)
    {
      full.loadTexture('game.full');
      fullStatus = true;
  }
  else
  {
   full.loadTexture('game.non_full');
   fullStatus = false;
}

}
}
game.state.add('game3', game3);

})();

}