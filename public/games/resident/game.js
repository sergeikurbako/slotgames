function randomNumber(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}
var isMobile = false;
var mobileX = null;
var mobileY = null;
if (navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
) {
    isMobile = true;
    mobileX = -94;
    mobileY = -54;
}
var width = 829.7;
var height = 598.95;

if(isMobile) {
    width = 640;
    height = 480;
}
var game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');
var flickerInterval = '';
var fullStatus = false;
var soundStatus = true;
function createLevelButtons() {
    var lvl1 = game.add.sprite(0,0, 'x');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(30, 0, 'x');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');
        game2.ropePositionX = 257;
    }, this);

    var lvl3 = game.add.sprite(60, 0, 'x');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');
        game3.freeze = false;
    }, this);

    var lvl4 = game.add.sprite(90, 0, 'x');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');
        game4.freeze = false;
    }, this);
}
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(740+mobileX,30+mobileY, 'non_full');
    else
        full = game.add.sprite(740+mobileX,30+mobileY, 'full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_on');
    else
        sound = game.add.sprite(740+mobileX,55+mobileY, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}
function selectLine(n) {
    game1.currentLine = n;

    for (var i = 1; i <= 9; ++i) {
        game1.lines[i].sprite.visible = false;
        game1.lines[i].number.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        game1.lines[i].sprite.visible = true;
        game1.lines[i].number.visible = true;
        game1.lines[i].sprite.loadTexture('line_' + i);
        if (i === 5)
            game1.lines[5].sprite.position.y = 144+mobileY;
        if (i === 7)
            game1.lines[7].sprite.position.y = 318+mobileY;

    }
};
function preselectLine(n) {
    for (var i = 1; i <= 9; ++i) {
        game1.lines[i].sprite.visible = false;
        game1.lines[i].number.visible = false;
    }
    for (var i = 1; i <= n; ++i) {
        game1.lines[i].sprite.loadTexture('linefull_' + i);
        game1.lines[i].sprite.visible = true;
        game1.lines[i].number.visible = true;
        if (i === 5)
            game1.lines[5].sprite.position.y = 131+mobileY;
        if (i === 7)
            game1.lines[7].sprite.position.y = 263+mobileY;

    }
    // game1.lines[5].sprite.rotation = 1;
}
function disableLinesBtn(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            game1.lines[i].button.loadTexture('btnline_d' + i);
        }
    }
};
function unDisableLines(){
    for (var i = 1; i <= 9; ++i) {
        if (i % 2 != 0) {
            game1.lines[i].button.loadTexture('btnline' + i);
        }
    }
};
function winLine(n) {
    game1.lines[n].sprite.loadTexture('linefull_' + n);
    game1.lines[n].sprite.visible = true;
    winNumber(n);
};
function winNumber(i){
    flickerInterval = setInterval(
        function(){
            if (game1.takeWin){
                game1.lines[i].number.visible = false;
            }
            unflicker(i);
        }, 1000);
};
function unflicker(i){
    setTimeout(function() {
        game1.lines[i].number.visible = true;
    }, 500);
}
function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}
//локация 1
var game1 = {
    bars: [],
    currentLine : 9,
    spinStatus : false,
    barsCurrentSpins : [0, 0, 0, 0, 0],
    barsTotalSpins : [15, 27, 39, 51, 63 ],
    countPlayBars : 0,
    takeWin : false,
    pages : [],
    settingsMode : false,
    currentPage : null,
    lines : {
        1: {
            coord: 243,
            sprite: null,
            btncoord : 250,
            button : null,
            number : null,
            numbercoord : 230
        },
        2: {
            coord: 110,
            sprite: null,
            number : null,
            numbercoord :86
        },
        3: {
            coord: 383,
            sprite: null,
            btncoord : 295,
            button : null,
            number : null,
            numbercoord : 374
        },
        4: {
            coord: 158,
            sprite: null,
            number : null,
            numbercoord : 150
        },
        5: {
            coord: 144,
            sprite: null,
            btncoord : 340,
            button : null,
            number : null,
            numbercoord : 310
        },
        6: {
            coord: 132,
            sprite: null,
            number : null,
            numbercoord :118
        },
        7: {
            coord: 318,
            sprite: null,
            btncoord : 385,
            button : null,
            number : null,
            numbercoord : 342
        },
        8: {
            coord: 270,
            sprite: null,
            number : null,
            numbercoord : 262
        },
        9: {
            coord: 159,
            sprite: null,
            btncoord : 430,
            button : null,
            number : null,
            numbercoord : 198
        },

    },
    create:function(){

        game1.bars[0] = game.add.tileSprite(142+mobileX, 87+mobileY, 96, 322, 'game1.bar');
        game1.bars[0].tilePosition.y =  randomNumber(0,8)*112 ;
        game1.bars[1] = game.add.tileSprite(254+mobileX, 87+mobileY, 96, 322, 'game1.bar');
        game1.bars[1].tilePosition.y =  randomNumber(0,8)*112;
        game1.bars[2] = game.add.tileSprite(366+mobileX , 87+mobileY, 96, 322, 'game1.bar');
        game1.bars[2].tilePosition.y =  randomNumber(0,8)*112;
        game1.bars[3] = game.add.tileSprite(478+mobileX, 87+mobileY, 96, 322, 'game1.bar');
        game1.bars[3].tilePosition.y =  randomNumber(0,8)*112;
        game1.bars[4] = game.add.tileSprite(590+mobileX, 87+mobileY, 96, 322, 'game1.bar');
        game1.bars[4].tilePosition.y =  randomNumber(0,8)*112;
        game1.safe_row_win_1 = game.add.sprite(142+mobileX, 87+mobileY, 'safe_row_win');
        game1.safe_row_win_1Animation = game1.safe_row_win_1.animations.add('safe_row_win', [0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5], 15, false);
        game1.safe_row_win_1.visible = false;
        game1.safe_row_win_2 = game.add.sprite(254+mobileX, 87+mobileY, 'safe_row_win');
        game1.safe_row_win_2Animation = game1.safe_row_win_2.animations.add('safe_row_win', [0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5], 15, false);
        game1.safe_row_win_2.visible = false;
        game1.safe_row_win_3 = game.add.sprite(366+mobileX, 87+mobileY, 'safe_row_win');
        game1.safe_row_win_3Animation = game1.safe_row_win_3.animations.add('safe_row_win', [0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5], 15, false);
        game1.safe_row_win_3.visible = false;
        game1.bg = game.add.sprite(94+mobileX,86+mobileY, 'game1.bg');
        game1.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game1.bg_top1');
        game1.smoke = game.add.sprite(94+mobileX, 406+mobileY, 'smoke');
        game1.smokeAnimation = game1.smoke.animations.add('smoke', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 10, true);
        game1.smoke.visible = true;
        // game1.smoke = game.add.sprite(94+mobileX, 422+mobileY, 'game1.big_fire');
        //появление огнетушителя
        game1.big_fire_emergence = game.add.sprite(94+mobileX, 422+mobileY, 'big_fire_emergence');
        game1.big_fire_emergence.animations.add('big_fire_emergence', [0,1,2,3], 10, true).play();
//экран с wild simbol, take win и т.д.
        game1.big_fire_emergence = game.add.sprite(542+mobileX, 439+mobileY, 'wild_simbol');

        game1.page = game.add.sprite(238+mobileX, 390+mobileY, 'page');
        game1.pageAnimation = game1.page.animations.add('page', [0,1,2,3,4,5,6,7], 5, true);
        game1.page.visible = true;
        for (var i = 1; i <= 9; ++i) {
            game1.lines[i].sprite = game.add.sprite(134+mobileX, game1.lines[i].coord+mobileY, 'line_' + i);
            game1.lines[i].sprite.visible = false;
            game1.lines[i].number = game.add.sprite(94+mobileX, game1.lines[i].numbercoord+mobileY, 'win_' + i);
            game1.lines[i].number.visible = false;
        }
        if (isMobile)
            game1.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
        for (var i = 1; i <= 5; ++i) {
            game1.page = game.add.sprite(94+mobileX, 22+mobileY, 'game1.page_' + i);
            game1.page.visible = false;
            game1.pages[i] = game1.page;
        }
        prev_page = game.add.sprite(95+mobileX, 456+mobileY, 'prev_page');
        prev_page.visible = false;
        prev_page.inputEnabled = true;
        prev_page.input.useHandCursor = true;
        prev_page.events.onInputUp.add(function(){
            if (game1.settingsMode)  {
                pageSound.play();
                if (game1.currentPage == 1)
                    game1.currentPage = 5;
                else{
                    game1.pages[game1.currentPage].visible = false;
                    game1.currentPage -=1;
                }
            }
            game1.pages[game1.currentPage].visible = true;
        });
        exit_btn = game.add.sprite(304+mobileX, 456+mobileY, 'exit_btn');
        exit_btn.visible = false;
        exit_btn.inputEnabled = true;
        exit_btn.input.useHandCursor = true;
        exit_btn.events.onInputUp.add(function(){
            pageSound.play();
            for (var i = 1; i <= 5; ++i) {
                game1.pages[i].visible = false;
            }
            prev_page.visible = false;
            exit_btn.visible = false;
            next_page.visible = false;
            game1.settingsMode = false;
            if (!isMobile){
                unDisableLines()
                game1.automatic_start.loadTexture('automatic_start');
                game1.bet_max.loadTexture('bet_max');
                game1.bet_one.loadTexture('bet_one');
                game1.paytable.loadTexture('paytable');
                game1.select_game.loadTexture('select_game');
            } else {
                doubleb.visible = true;
                dollar.visible = true;
                home.visible = true;
                button.visible = true;
                bet1.visible = true;
                gear.visible = true;
            }
        });
        next_page = game.add.sprite(528+mobileX, 456+mobileY, 'next_page');
        next_page.visible = false;
        next_page.inputEnabled = true;
        next_page.input.useHandCursor = true;
        next_page.events.onInputUp.add(function(){
            if (game1.settingsMode)  {
                pageSound.play();
                if (game1.currentPage == 5){
                    game1.pages[game1.currentPage].visible = false;
                    game1.currentPage = 1;
                } else if (game1.currentPage == 1){
                    game1.currentPage +=1;
                } else {
                    game1.pages[game1.currentPage].visible = false;
                    game1.currentPage +=1;
                }
            }
            game1.pages[game1.currentPage].visible = true;
        });
        if(!isMobile){
            game.add.sprite(0,0, 'main_window');
            full_and_sound();
        }
        createLevelButtons();
        if(!isMobile){
            for (var i = 1; i <= 9; ++i) {
                if (i % 2 != 0) {
                    game1.lines[i].sound = game.add.audio('line' + i);
                    game1.lines[i].button = game.add.sprite(game1.lines[i].btncoord+mobileX, 510+mobileY, 'btnline' + i);
                    game1.lines[i].button.scale.setTo(0.7, 0.7);
                    game1.lines[i].button.inputEnabled = true;
                    game1.lines[i].button.input.useHandCursor = true;
                    (function (n) {
                        game1.lines[n].button.events.onInputOver.add(function(){
                            if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
                                return;
                            game1.lines[n].button.loadTexture('btnline_p' + n);
                        });
                        game1.lines[n].button.events.onInputOut.add(function(){
                            if(game1.spinStatus || game1.takeWin  || game1.settingsMode)
                                return;
                            game1.lines[n].button.loadTexture('btnline' + n);
                        });
                        game1.lines[n].button.events.onInputUp.add(function () {
                            if(game1.spinStatus || game1.takeWin || game1.settingsMode)
                                return;
                            hideLines();
                            selectLine(n);
                            game1.lines[n].sound.play();
                        }, this);
                    })(i);
                }
            }
        }
        if (isMobile){
            button = game.add.sprite(544, 188, 'spin');
            button.inputEnabled = true;
            button.input.useHandCursor = true;

            bet1 = game.add.sprite(548, 274, 'bet1');
            bet1.inputEnabled = true;
            bet1.input.useHandCursor = true;
            bet1.events.onInputDown.add(function(){
                bet1.loadTexture('bet1_p');
            });
            bet1.events.onInputUp.add(function(){
                document.getElementById('betMode').style.display = 'block';
                widthVisibleZone = $('.betWrapper .visibleZone').height();
                console.log(widthVisibleZone);
                $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
                $('canvas').css('display', 'none');
                bet1.loadTexture('bet1');
            });
            home = game.add.sprite(3, 3, 'home');
            home.inputEnabled = true;
            home.input.useHandCursor = true;
            home.events.onInputDown.add(function(){
                home.loadTexture('home_p');
            });
            home.events.onInputUp.add(function(){
                home.loadTexture('home');
            });
            dollar = game.add.sprite(435, 3, 'dollar');
            dollar.inputEnabled = true;
            dollar.input.useHandCursor = true;
            dollar.events.onInputDown.add(function(){
            });

            gear = game.add.sprite(539, 3, 'gear');
            gear.inputEnabled = true;
            gear.input.useHandCursor = true;
            gear.events.onInputDown.add(function(){
                game1.pages[1].visible = true;
                prev_page.visible = true;
                exit_btn.visible = true;
                next_page.visible = true;
                game1.settingsMode = true;
                game1.currentPage = 1;
                doubleb.visible = false;
                dollar.visible = false;
                home.visible = false;
                button.visible = false;
                bet1.visible = false;
                gear.visible = false;
            });

            doubleb = game.add.sprite(546, 133, 'double');
            doubleb.inputEnabled = true;
            doubleb.input.useHandCursor = true;
            doubleb.events.onInputDown.add(function(){
                game.state.start('game2');
                game2.ropePositionX = 257;
            });
            function buttonClicked() {
                if (game1.spinStatus) {
                    return;
                }
                button.loadTexture('spin_p');
            }

            button.events.onInputDown.add(buttonClicked, this);
            button.events.onInputUp.add(btnStartUp, this);
        }
        if(!isMobile){
            game1.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
            game1.startButton.scale.setTo(0.7, 0.7);
            game1.startButton.inputEnabled = true;
            game1.startButton.input.useHandCursor = true;
            game1.startButton.events.onInputUp.add(btnStartUp, this);
            game1.startButton.events.onInputOver.add(function(){
                if(game1.spinStatus)
                    return;
                game1.startButton.loadTexture('start_p');
            });
            game1.startButton.events.onInputOut.add(function(){
                if(game1.spinStatus)
                    return;
                game1.startButton.loadTexture('start');
            });

            game1.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
            game1.automatic_start.scale.setTo(0.7, 0.7);
            game1.automatic_start.inputEnabled = true;
            game1.automatic_start.input.useHandCursor = true;
            game1.automatic_start.events.onInputOver.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.automatic_start.loadTexture('automatic_start_p');
            });
            game1.automatic_start.events.onInputOut.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.automatic_start.loadTexture('automatic_start');
            });
            game1.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
            game1.bet_max.scale.setTo(0.7, 0.7);
            game1.bet_max.inputEnabled = true;
            game1.bet_max.input.useHandCursor = true;
            game1.bet_max.events.onInputOver.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.bet_max.loadTexture('bet_max_p');
            });
            game1.bet_max.events.onInputOut.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.bet_max.loadTexture('bet_max');
            });
            game1.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
            game1.bet_one.scale.setTo(0.7, 0.7);
            game1.bet_one.inputEnabled = true;
            game1.bet_one.input.useHandCursor = true;
            game1.bet_one.events.onInputOver.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.bet_one.loadTexture('bet_one_p');
            });
            game1.bet_one.events.onInputOut.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.bet_one.loadTexture('bet_one');
            });
            game1.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
            game1.paytable.scale.setTo(0.7, 0.7);
            game1.paytable.inputEnabled = true;
            game1.paytable.input.useHandCursor = true;
            game1.paytable.events.onInputOver.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.paytable.loadTexture('paytable_p');
            });
            game1.paytable.events.onInputOut.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.paytable.loadTexture('paytable');
            });
            game1.paytable.events.onInputUp.add(function(){
                game1.pages[1].visible = true;
                prev_page.visible = true;
                exit_btn.visible = true;
                next_page.visible = true;
                game1.settingsMode = true;
                game1.currentPage = 1;
                game1.automatic_start.loadTexture('automatic_start_d');
                game1.bet_max.loadTexture('bet_max_d');
                game1.bet_one.loadTexture('bet_one_d');
                game1.paytable.loadTexture('paytable_d');
                game1.select_game.loadTexture('select_game_d');
                game1.lines[3].button.loadTexture('btnline_d' + 3);
                game1.lines[5].button.loadTexture('btnline_d' + 5);
                game1.lines[7].button.loadTexture('btnline_d' + 7);
            }, this);
            game1.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
            game1.select_game.scale.setTo(0.7, 0.7);
            game1.select_game.inputEnabled = true;
            game1.select_game.input.useHandCursor = true;
            game1.select_game.events.onInputOver.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.select_game.loadTexture('select_game_p');
            });
            game1.select_game.events.onInputOut.add(function(){
                if(game1.spinStatus || game1.settingsMode)
                    return;
                game1.select_game.loadTexture('select_game');
            });
        }
        stopSound = game.add.audio('stop');
        rotateSound = game.add.audio('rotate');
        tadaSound = game.add.audio('tada');
        pageSound = game.add.audio('page');
        safeWinSound = game.add.audio('safeWin');
        takeWinSound = game.add.audio('takeWin');
        takeWinSound.addMarker('take', 0, 0.6);
        rotateSound.loop = true;

        game1.smokeAnimation.play();
        game1.pageAnimation.play();
        if(!isMobile){
            game1.lines[1].button.events.onInputUp.add(function () {
                if (game1.settingsMode)  {
                    pageSound.play();
                    if (game1.currentPage == 1)
                        game1.currentPage = 5;
                    else{
                        game1.pages[game1.currentPage].visible = false;
                        game1.currentPage -=1;
                    }
                    game1.pages[game1.currentPage].visible = true;
                }
            });
            game1.lines[9].button.events.onInputUp.add(function () {
                if (game1.settingsMode)  {
                    pageSound.play();
                    if (game1.currentPage == 5){
                        game1.pages[game1.currentPage].visible = false;
                        game1.currentPage = 1;
                    } else if (game1.currentPage == 1){
                        game1.currentPage +=1;
                    } else {
                        game1.pages[game1.currentPage].visible = false;
                        game1.currentPage +=1;
                    }
                    game1.pages[game1.currentPage].visible = true;
                }
            });
            game1.lines[1].button.events.onInputOver.add(function(){
                if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                    game1.lines[1].button.loadTexture('btnline_p' + 1);
            });
            game1.lines[1].button.events.onInputOut.add(function(){
                if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                    game1.lines[1].button.loadTexture('btnline' + 1);
            });
            game1.lines[9].button.events.onInputOver.add(function(){
                if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                    game1.lines[9].button.loadTexture('btnline_p' + 9);
            });
            game1.lines[9].button.events.onInputOut.add(function(){
                if(!game1.spinStatus && !game1.takeWin  && game1.settingsMode)
                    game1.lines[9].button.loadTexture('btnline' + 9);
            });
        }
        function btnStartUp(){
            if(game1.spinStatus)
                return;
            if (game1.settingsMode){
                pageSound.play();
                for (var i = 1; i <= 5; ++i) {
                    game1.pages[i].visible = false;
                }
                prev_page.visible = false;
                exit_btn.visible = false;
                next_page.visible = false;
                game1.settingsMode = false;
                unDisableLines()
                game1.automatic_start.loadTexture('automatic_start');
                game1.bet_max.loadTexture('bet_max');
                game1.bet_one.loadTexture('bet_one');
                game1.paytable.loadTexture('paytable');
                game1.select_game.loadTexture('select_game');
                console.log(game1.settingsMode);
            } else{
                if (game1.takeWin){
                    game1.takeWin = false;
                    unDisableLines();
                    selectLine(game1.currentLine);
                    clearInterval(flickerInterval);
                    takeWinSound.play('take');
                } else{
                    if (!isMobile){
                        game1.startButton.loadTexture('start_d');
                        game1.automatic_start.loadTexture('automatic_start_d');
                        game1.bet_max.loadTexture('bet_max_d');
                        game1.bet_one.loadTexture('bet_one_d');
                        game1.paytable.loadTexture('paytable_d');
                        game1.select_game.loadTexture('select_game_d');
                        disableLinesBtn();
                    } else
                        button.loadTexture('spin');
                    game1.countPlayBars = 5;
                    game1.barsCurrentSpins = [0, 0, 0, 0, 0];
                    game1.spinStatus = true;
                    hideLines();
                    rotateSound.play();
                }
            }
        };
        function hideLines() {
            for (var i = 1; i <= 9; ++i) {
                game1.lines[i].sprite.visible = false;
            }
        }
        game1.safe_row_win_1Animation.onComplete.add(function(){
            game.state.start('game3');
        });
        preselectLine(game1.currentLine);
    },
    update:function(){
        if (game1.spinStatus){
            for (var i in game1.bars) {
                game1.barsCurrentSpins[i]++;
                if (game1.barsCurrentSpins[i] < game1.barsTotalSpins[i]) {
                    game1.bars[i].tilePosition.y += 112;
                } else if (game1.barsCurrentSpins[i] == game1.barsTotalSpins[i]) {
                    game1.countPlayBars--;
                    stopSound.play();
                }
            }
            if (game1.countPlayBars === 0){
                game1.spinStatus = false;
                rotateSound.stop();
                if (!isMobile){
                    game1.startButton.loadTexture('start');
                    game1.automatic_start.loadTexture('automatic_start');
                    game1.bet_max.loadTexture('bet_max');
                    game1.bet_one.loadTexture('bet_one');
                    game1.paytable.loadTexture('paytable');
                    game1.select_game.loadTexture('select_game');
                }
                if(game1.currentLine == 3)   {
                    game1.takeWin = true;
                    winLine(3);
                    tadaSound.play();
                } else if (game1.currentLine == 5){
                    safeWinSound.play();
                    game1.safe_row_win_1Animation.play();
                    game1.safe_row_win_1.visible = true;
                    game1.safe_row_win_2Animation.play();
                    game1.safe_row_win_2.visible = true;
                    game1.safe_row_win_3Animation.play();
                    game1.safe_row_win_3.visible = true;
                } else {
                    if(!isMobile){
                        unDisableLines();
                    }
                    selectLine(game1.currentLine);
                }
            }
        }
    }
};
game.state.add('game1', game1);

var game2 = {
    cards : [],
    ropePositionX : 257,
    cardValues : {1:5, 2:7, 3:12, 4:13, 5:15},
    // cardValues : {1:'5b', 2:'7c', 3:'12p', 4:'13t', 5:'15'},
    cardIndexes : null,
    create : function() {
        game2.bg = game.add.sprite(94+mobileX,86+mobileY, 'game1.bg');
        game2.bg2 = game.add.sprite(94+mobileX,54+mobileY, 'game2.bg');
        game2.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
        if(!isMobile){
            game.add.sprite(0,0, 'main_window');
        }
        pickCardSound = game.add.audio('pickCard');
        cardWin = game.add.audio('cardWin');
        for(var i=1; i<=4; ++i) {
            game2.cards[i] = game.add.sprite(game2.ropePositionX+mobileX, 136+mobileY, 'shirt_cards');
            game2.cards[i].inputEnabled = true;
            game2.cards[i].input.useHandCursor = true;
            game2.ropePositionX += 112;
        }

        var buttonsPositionsX = {1:{pos:295,i:3}, 2:{pos:340,i:5}, 3:{pos:385,i:7}, 4:{pos:430,i:9}};
        var buttons = {};
        // var cardIndexes = {1 : '2b', 2: '2c', 3: '2p', 4: '2t'};
        game2.cardIndexes = shuffle(Object.keys(game2.cardValues));
        game2.dealer_card = game.add.sprite(125+mobileX, 134+mobileY, 'card_'+game2.cardValues[game2.cardIndexes[0]]);
        game.add.sprite(141+mobileX, 296+mobileY, 'dealer');
        if(!isMobile)
            game.add.sprite(250+mobileX, 510+mobileY, 'btnline_d1').scale.setTo(0.7, 0.7);;
        for(var i in buttonsPositionsX) {
            if(!isMobile){
                var button = game.add.sprite(buttonsPositionsX[i].pos+mobileX, 510+mobileY, 'btnline' + buttonsPositionsX[i].i);
                button.scale.setTo(0.7, 0.7);
                button.inputEnabled = true;
                button.input.useHandCursor = true;
                (function(n, btn, pic){
                    btn.events.onInputUp.add(function () {
                        var card = game2.cards[n];
                        card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
                        game.add.sprite(card.position.x+13+mobileX, 301+mobileY, 'pick_card');
                        game2.soldier_lose = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_lose');
                        game2.soldier_loseAnimation = game2.soldier_lose.animations.add('game2.soldier_lose', [0,1], 5, false);
                        game2.soldier_lose.visible = false;
                        game2.soldier_win = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_win');
                        game2.soldier_winAnimation = game2.soldier_win.animations.add('game2.soldier_win', [0,1,2,3], 5, false);
                        game2.soldier_win.visible = false;
                        game2.soldierheadAnimation.stop();
                        game2.soldierAnimation.stop();
                        game2.soldier.visible = false;
                        game2.soldierhead.visible = false;
                        pickCardSound.play();
                        var dealerValue = game2.cardValues[game2.cardIndexes[0]];
                        var userValue = game2.cardValues[game2.cardIndexes[n]];
                        console.log('dealer ' + dealerValue);
                        console.log('user ' + userValue);
                        if(dealerValue < userValue) {
                            game2.soldier_win.visible = true;
                            game2.soldier_winAnimation.play();
                            cardWin.play();
                        } else {
                            game2.soldier_lose.visible = true;
                            game2.soldier_loseAnimation.play();
                        }

                        for(var c in game2.cards) {
                            game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
                        }
                        for(var b in buttons) {
                            buttons[b].loadTexture('btnline_d' + b);
                            buttons[b].inputEnabled = false;
                        }


                    }, this);
                })(i, button, buttonsPositionsX[i].i);
            } else {
                (function(n, btn, pic){
                    btn.events.onInputUp.add(function () {
                        var card = game2.cards[n];
                        card.loadTexture('card_'+game2.cardValues[game2.cardIndexes[n]]);
                        game.add.sprite(card.position.x+13, 301+mobileY, 'pick_card');
                        game2.soldier_lose = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_lose');
                        game2.soldier_loseAnimation = game2.soldier_lose.animations.add('game2.soldier_lose', [0,1], 5, false);
                        game2.soldier_lose.visible = false;
                        game2.soldier_win = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_win');
                        game2.soldier_winAnimation = game2.soldier_win.animations.add('game2.soldier_win', [0,1,2,3], 5, false);
                        game2.soldier_win.visible = false;
                        game2.soldierheadAnimation.stop();
                        game2.soldierAnimation.stop();
                        game2.soldier.visible = false;
                        game2.soldierhead.visible = false;
                        pickCardSound.play();
                        var dealerValue = game2.cardValues[game2.cardIndexes[0]];
                        var userValue = game2.cardValues[game2.cardIndexes[n]];
                        console.log('dealer ' + dealerValue);
                        console.log('user ' + userValue);
                        if(dealerValue < userValue) {
                            game2.soldier_win.visible = true;
                            game2.soldier_winAnimation.play();
                            cardWin.play();
                        } else {
                            game2.soldier_lose.visible = true;
                            game2.soldier_loseAnimation.play();
                        }

                        for(var c in game2.cards) {
                            game2.cards[c].loadTexture('card_'+game2.cardValues[game2.cardIndexes[c]]);
                        }
                        for(var b in game2.cards) {
                            game2.cards[b].inputEnabled = false;
                        }

                    }, this);
                })(i, game2.cards[i], buttonsPositionsX[i].i)
            }


            if (!isMobile){
                buttons[buttonsPositionsX[i].i] = button;
            }
        }
        game2.soldierhead = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldierhead_animation');
        game2.soldierheadAnimation = game2.soldierhead.animations.add('game2.soldierhead', [0,1,2,3], 5, false);
        game2.soldierhead.visible = false;
        game2.soldier = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_animation');
        game2.soldierAnimation = game2.soldier.animations.add('game2.soldier', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
        game2.soldier.visible = true;
        game2.soldierAnimation.onComplete.add(function(){
            game2.soldierhead.visible = true;
            game2.soldier.visible = false;
            game2.soldierheadAnimation.play();
        });
        game2.soldierheadAnimation.onComplete.add(function(){
            game2.soldierhead.visible = false;
            game2.soldier.visible = true;
            game2.soldierAnimation.play();
        });
        if(!isMobile){
            game2.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
            game2.startButton.scale.setTo(0.7, 0.7);
            game2.startButton.inputEnabled = true;
            game2.startButton.input.useHandCursor = true;
            game2.startButton.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.startButton.loadTexture('start_p');
            });
            game2.startButton.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.startButton.loadTexture('start');
            });
            game2.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
            game2.automatic_start.scale.setTo(0.7, 0.7);
            game2.automatic_start.inputEnabled = true;
            game2.automatic_start.input.useHandCursor = true;
            game2.automatic_start.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.automatic_start.loadTexture('automatic_start_p');
            });
            game2.automatic_start.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.automatic_start.loadTexture('automatic_start');
            });
            game2.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
            game2.bet_max.scale.setTo(0.7, 0.7);
            game2.bet_max.inputEnabled = true;
            game2.bet_max.input.useHandCursor = true;
            game2.bet_max.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_max.loadTexture('bet_max_p');
            });
            game2.bet_max.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_max.loadTexture('bet_max');
            });
            game2.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
            game2.bet_one.scale.setTo(0.7, 0.7);
            game2.bet_one.inputEnabled = true;
            game2.bet_one.input.useHandCursor = true;
            game2.bet_one.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_one.loadTexture('bet_one_p');
            });
            game2.bet_one.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.bet_one.loadTexture('bet_one');
            });
            game2.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
            game2.paytable.scale.setTo(0.7, 0.7);
            game2.paytable.inputEnabled = true;
            game2.paytable.input.useHandCursor = true;
            game2.paytable.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.paytable.loadTexture('paytable_p');
            });
            game2.paytable.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.paytable.loadTexture('paytable');
            });
            game2.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
            game2.select_game.scale.setTo(0.7, 0.7);
            game2.select_game.inputEnabled = true;
            game2.select_game.input.useHandCursor = true;
            game2.select_game.events.onInputOver.add(function(){
                if(game2.spinStatus)
                    return;
                game2.select_game.loadTexture('select_game_p');
            });
            game2.select_game.events.onInputOut.add(function(){
                if(game2.spinStatus)
                    return;
                game2.select_game.loadTexture('select_game');
            });
        }
        if (isMobile)
            game2.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
        createLevelButtons();
        if(!isMobile)
            full_and_sound();
        game2.soldierAnimation.play();
    }

};
game.state.add('game2', game2);
var game3 = {
    freeze : false,
    buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    safeCount : 4,
    soldierPositionsX : {1:110, 3:222, 5:334, 7:446},
    selectedSafeNormal : {1:1, 3:2, 5:3, 7:4},
    closedDoor : [1,3,5,7],
    selectedSafe : null,
    safe_hit_arr : [],
    safe_arr : [],
    safe_door_arr : [],
    safe_door_arr_anim : [],

    create : function() {
        var checkHelm = true;

        game3.bg = game.add.sprite(94+mobileX,54+mobileY, 'game3.bg');
        game3.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');
        if(!isMobile){
            game.add.sprite(0,0, 'main_window');
        }
        hitSound = game.add.audio('hit');
        hitSound.addMarker('hits', 0.3, 5);
        winSound = game.add.audio('winGame3');
        dynamiteSound = game.add.audio('dynamite');
        boomSound = game.add.audio('boom');
        fire_extinguisher_action_sound = game.add.audio('fire_extinguisher_action');

        game3.soldierhead = game.add.sprite(94+mobileX, 262+mobileY, 'game2.soldierhead_animation');
        game3.soldierheadAnimation = game3.soldierhead.animations.add('game2.soldierhead', [0,1,2,3], 5, false);
        game3.soldierhead.visible = false;
        game3.soldier = game.add.sprite(94+mobileX, 262+mobileY, 'game2.soldier_animation');
        game3.soldierAnimation = game3.soldier.animations.add('game2.soldier', [0,1,2,3,4,3,2,1,0], 5, false);
        game3.soldier.visible = true;
        game3.soldierAnimation.onComplete.add(function(){
            game3.soldierhead.visible = true;
            game3.soldier.visible = false;
            game3.soldierheadAnimation.play();
        });
        game3.soldierheadAnimation.onComplete.add(function(){
            game3.soldierhead.visible = false;
            game3.soldier.visible = true;
            game3.soldierAnimation.play();
        });
        game3.safe_shine = game.add.sprite(206+mobileX, 182+mobileY, 'game3.safe_shine');
        game3.safe_shineAnimation = game3.safe_shine.animations.add('game2.safe_shine', [0,1,2,3,4,5,6,7,8], 5, true);
        game3.safe_shine.visible = true;
        game3.big_fire = game.add.sprite(143+mobileX, 422+mobileY, 'game3.big_fire');

        game3.door_knob = game.add.sprite(222+mobileX, 230+mobileY, 'game3.door_knob');
        game3.door_knobAnim = game3.door_knob.animations.add('game3.door_knob', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2], 15, false);
        game3.door_knob.visible = false;
        game3.bottle = game.add.sprite(222+mobileX, 214+mobileY, 'game3.bottle');
        game3.bottle.visible = false;
        game3.book = game.add.sprite(334+mobileX, 214+mobileY, 'game3.book');
        game3.book.visible = false;
        game3.dynamite = game.add.sprite(446+mobileX, 214+mobileY, 'game3.dynamite');
        game3.dynamiteAnim = game3.dynamite.animations.add('game3.dynamite', [0,1,2,3,4,5,6,7], 5, false);
        game3.dynamite.visible = false;
        game3.dynamite2 = game.add.sprite(334+mobileX, 214+mobileY, 'game3.dynamite');
        game3.dynamite2Anim = game3.dynamite2.animations.add('game3.dynamite', [0,1,2,3,4], 5, false);
        game3.dynamite2.visible = false;
        game3.dynamite2_f = game.add.sprite(334+mobileX, 214+mobileY, 'game3.dynamite');
        game3.dynamite2_fAnim = game3.dynamite2_f.animations.add('game3.dynamite', [5,6,7], 5, false);
        game3.dynamite2_f.visible = false;
        game3.dynamite_off = game.add.sprite(334+mobileX, 214+mobileY, 'game3.dynamite_off');
        game3.dynamite_offAnim = game3.dynamite_off.animations.add('game3.dynamite_off', [0,1], 5, false);
        game3.dynamite_off.visible = false;
        game3.gold = game.add.sprite(558+mobileX, 214+mobileY, 'game3.gold');
        game3.goldAnim = game3.gold.animations.add('game3.gold', [0,1,2], 5, true);
        game3.gold.visible = false;
        // кубок (в появлении не учавствует)
        game3.cup = game.add.sprite(558+mobileX, 214+mobileY, 'game3.cup');
        game3.cupAnim = game3.cup.animations.add('game3.cup', [0,1,2,3,4,5,6], 5, true);
        game3.cup.visible = false;
        for(var i in game3.soldierPositionsX) {
            game3.safe_door_arr[i] = game.add.sprite(94+112*game3.selectedSafeNormal[i]+mobileX, 182+mobileY, 'game3.safe_door');
            if (isMobile){
                game3.safe_arr[i] = game.add.sprite(94+112*game3.selectedSafeNormal[i]+mobileX, 182+mobileY, 'game3.safe_'+i);
                game3.safe_arr[i].inputEnabled = true;
                game3.safe_arr[i].input.useHandCursor = true;
                game3.safe_arr[i].visible = true;
            }
            game3.safe_door_arr_anim[i] = game3.safe_door_arr[i].animations.add('game3.safe_door', [0,1,2], 5, false);
            game3.safe_door_arr[i].visible = false;
        }
        game3.safe_hit_1 = game.add.sprite(206+mobileX, 182+mobileY, 'game3.safe_hit_1' );
        game3.safe_hit_1_anim = game3.safe_hit_1.animations.add('game3.safe_hit_1', [1,2], 10, false);
        game3.safe_hit_1.visible = false;
        game3.safe_hit_3 = game.add.sprite(318+mobileX, 182+mobileY, 'game3.safe_hit_3' );
        game3.safe_hit_3_anim = game3.safe_hit_3.animations.add('game3.safe_hit_3', [1,2], 10, false);
        game3.safe_hit_3.visible = false;
        game3.safe_hit_5 = game.add.sprite(430+mobileX, 182+mobileY, 'game3.safe_hit_5' );
        game3.safe_hit_5_anim = game3.safe_hit_5.animations.add('game3.safe_hit_5', [1,2], 10, false);
        game3.safe_hit_5.visible = false;
        game3.safe_hit_7 = game.add.sprite(542+mobileX, 182+mobileY, 'game3.safe_hit_7' );
        game3.safe_hit_7_anim = game3.safe_hit_7.animations.add('game3.safe_hit_7', [1,2], 10, false);
        game3.safe_hit_7.visible = false;
        game3.soldierStand = game.add.sprite(game3.soldierPositionsX[1]+mobileX, 86+mobileY, 'game3.hit_safe');
        game3.soldierStandHit = game3.soldierStand.animations.add('game3.hit_safe', [0,1,2,3,4], 5, false);
        game3.soldierStand.visible = false;
        game3.soldier_win = game.add.sprite(game3.soldierPositionsX[1]+mobileX, 86+mobileY, 'game3.soldier_win');
        game3.soldier_winAnim = game3.soldier_win.animations.add('game3.soldier_win', [0,1,2,2,2], 5, false);
        game3.soldier_win.visible = false;
        game3.big_fire_extinguisher_action = game.add.sprite(game3.soldierPositionsX[3]+mobileX, 86+mobileY, 'game3.big_fire_extinguisher_action');
        game3.big_fire_extinguisher_actionAnim = game3.big_fire_extinguisher_action.animations.add('game3.big_fire_extinguisher_action', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
        game3.big_fire_extinguisher_action.visible = false;
        game3.destroy_door_3 = game.add.sprite(430+mobileX, 182+mobileY, 'game3.destroy_door_3');
        game3.destroy_door_3.visible = false;
        game3.boom_animation = game.add.sprite(game3.soldierPositionsX[5]+mobileX, 86+mobileY, 'game3.boom_animation');
        game3.boom_animationAnim = game3.boom_animation.animations.add('game3.boom_animation', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
        game3.boom_animation.visible = false;
        game3.boom_animation_after = game.add.sprite(game3.soldierPositionsX[5]+mobileX, 86+mobileY, 'game3.boom_animation');
        game3.boom_animation_afterAnim = game3.boom_animation_after.animations.add('game3.boom_animation', [8,9,10], 5, true);
        game3.boom_animation_after.visible = false;
        game3.big_fire2 = game.add.sprite(126+mobileX, 230+mobileY, 'game1.big_fire');
        game3.big_fire2.visible = false;
        game3.soldierStandHit.onComplete.add(function(){
            if (game3.selectedSafe == 1){
                game3.safe_hit_1.visible = true;
                game3.safe_hit_1_anim.play();
            }
            if (game3.selectedSafe == 3){
                game3.safe_hit_3.visible = true;
                game3.safe_hit_3_anim.play();
            }
            if (game3.selectedSafe == 5){
                game3.safe_hit_5.visible = true;
                game3.safe_hit_5_anim.play();
            }
            if (game3.selectedSafe == 7){
                game3.safe_hit_7.visible = true;
                game3.safe_hit_7_anim.play();
            }
        });
        game3.safe_hit_1_anim.onComplete.add(function(){
            game3.safe_hit_1.visible = false;
            game3.door_knob.visible = true;
            game3.door_knob.position.x = 222+mobileX;
            game3.door_knobAnim.play();
        });
        game3.safe_hit_3_anim.onComplete.add(function(){
            game3.safe_hit_3.visible = false;
            game3.door_knob.visible = true;
            game3.door_knob.position.x = 334+mobileX;
            game3.door_knobAnim.play();
        });
        game3.safe_hit_5_anim.onComplete.add(function(){
            game3.safe_hit_5.visible = false;
            game3.door_knob.visible = true;
            game3.door_knob.position.x = 446+mobileX;
            game3.door_knobAnim.play();
        });
        game3.safe_hit_7_anim.onComplete.add(function(){
            game3.safe_hit_7.visible = false;
            game3.door_knob.visible = true;
            game3.door_knob.position.x = 558+mobileX;
            game3.door_knobAnim.play();
        });
        game3.door_knobAnim.onComplete.add(function(){
            game3.door_knob.visible = false;
            game3.safe_door_arr[game3.selectedSafe].visible = true;
            game3.safe_door_arr_anim[game3.selectedSafe].play();
            if ((game3.selectedSafe !== '5') && (game3.selectedSafe !== '3')){
                game3.soldierStand.visible = false;
                game3.soldier_winAnim.play();
                game3.soldier_win.position.x = game3.soldierPositionsX[game3.selectedSafe]+mobileX;
                game3.soldier_win.visible = true;
                winSound.play();
            }
            if (game3.selectedSafe == 1)
                game3.bottle.visible = true;
            if (game3.selectedSafe == 3){
                game3.dynamite2.visible = true;
                game3.dynamite2Anim.play();
                dynamiteSound.play();
            }
            if (game3.selectedSafe == 5){
                game3.dynamite.visible = true;
                game3.dynamiteAnim.play();
                dynamiteSound.play();
            }
            if (game3.selectedSafe == 7){
                game3.gold.visible = true;
                game3.goldAnim.play();
                // аналогичное появление кубка
                // game3.cup.visible = true;
                // game3.cupAnim.play();
            }
        });
        game3.soldier_winAnim.onComplete.add(function(){
            game3.freeze = false;
            game3.big_fire2.visible = false;
            game3.soldier_win.visible = false;
            game3.soldierAnimation.play();
            game3.soldier.visible = true;
            game3.big_fire.visible = true;
        });
        game3.dynamiteAnim.onComplete.add(function(){
            game3.soldierStand.visible = false;
            game3.destroy_door_3.visible = true;
            game3.boom_animationAnim.play();
            game3.boom_animation.visible = true;
            boomSound.play();
        });
        game3.boom_animationAnim.onComplete.add(function(){
            game3.boom_animation_afterAnim.play();
            game3.boom_animation_after.visible = true;
        });
        game3.dynamite2Anim.onComplete.add(function(){
            game3.soldierStand.visible = false;
            game3.big_fire2.visible = false;
            game3.big_fire_extinguisher_action.visible = true;
            game3.big_fire_extinguisher_actionAnim.play();
            game3.dynamite2.visible = false;
            game3.dynamite2_f.visible = true;
            game3.dynamite2_fAnim.play();
            fire_extinguisher_action_sound.play();
            checkHelm = false;
        });
        game3.dynamite2_fAnim.onComplete.add(function(){
            game3.dynamite_off.visible = true;
            game3.dynamite_offAnim.play();
        });
        game3.big_fire_extinguisher_actionAnim.onComplete.add(function(){
            game3.freeze = false;
            game3.big_fire_extinguisher_action.visible = false;
            game3.soldierAnimation.play();
            game3.soldier.visible = true;
        });
        if (!isMobile){
            button = game.add.sprite(game3.buttonsPositionsX[9]+mobileX, 510+mobileY, 'btnline_d' + 9);
            button.inputEnabled = false;
            button.scale.setTo(0.7, 0.7);
        }
        for(var i in game3.soldierPositionsX) {
            if (!isMobile){
                var button = game.add.sprite(game3.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
                button.scale.setTo(0.7, 0.7);

            }
            if (!isMobile){
                button.inputEnabled = true;
                button.scale.setTo(0.7, 0.7);
                button.input.useHandCursor = true;
                (function(n, btn){
                    btn.events.onInputUp.add(function () {
                        if(game3.freeze || n==9) {
                            return;
                        }
                        game3.freeze = true;
                        game3.safeCount--;
                        btn.loadTexture('btnline_d' + n);
                        btn.inputEnabled = false;
                        // keypress.play(); //sound
                        game3.soldierStand.position.x = game3.soldierPositionsX[n]+mobileX;
                        // safes[n].animations.play('use', 10, false, false);
                        game3.soldierhead.visible = false;
                        game3.soldier.visible = false;
                        game3.big_fire.visible = false;
                        game3.soldierStand.visible = true;
                        game3.soldierStandHit.play();
                        game3.selectedSafe = n;
                        // расположение огнетушителя
                        // n = 1,3,5,7,9 поэтому (n+1)/2
                        if (checkHelm == true){
                            game3.big_fire2.position.x = 126+(game3.selectedSafeNormal[n]-1)*112;
                            game3.big_fire2.visible = true;
                        }
                        // game3.closedDoor.splice(game3.selectedSafeNormal[n]-1, 1);
                        game3.safe_shine.visible = false;
                        game3.safe_shineAnimation.stop();
                        hitSound.play('hits');
                    }, this);
                })(i, button);
            } else {
                (function(n, btn){
                    btn.events.onInputUp.add(function () {
                        if(game3.freeze) {
                            return;
                        }
                        game3.freeze = true;
                        game3.safeCount--;
                        btn.inputEnabled = false;
                        btn.visible = false;
                        game3.soldierStand.position.x = game3.soldierPositionsX[n]+mobileX;
                        game3.soldierhead.visible = false;
                        game3.soldier.visible = false;
                        game3.big_fire.visible = false;
                        game3.soldierStand.visible = true;
                        game3.soldierStandHit.play();
                        game3.selectedSafe = n;
                        game3.safe_shine.visible = false;
                        game3.safe_shineAnimation.stop();
                        hitSound.play('hits');
                    }, this);
                })(i, game3.safe_arr[i]);
            }
        }
        if (isMobile)
            game3.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
        if(!isMobile){
            game3.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
            game3.startButton.scale.setTo(0.7, 0.7);
            game3.startButton.inputEnabled = true;
            game3.startButton.input.useHandCursor = true;
            game3.startButton.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.startButton.loadTexture('start_p');
            });
            game3.startButton.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.startButton.loadTexture('start');
            });
            game3.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
            game3.automatic_start.scale.setTo(0.7, 0.7);
            game3.automatic_start.inputEnabled = true;
            game3.automatic_start.input.useHandCursor = true;
            game3.automatic_start.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.automatic_start.loadTexture('automatic_start_p');
            });
            game3.automatic_start.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.automatic_start.loadTexture('automatic_start');
            });
            game3.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
            game3.bet_max.scale.setTo(0.7, 0.7);
            game3.bet_max.inputEnabled = true;
            game3.bet_max.input.useHandCursor = true;
            game3.bet_max.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.bet_max.loadTexture('bet_max_p');
            });
            game3.bet_max.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.bet_max.loadTexture('bet_max');
            });
            game3.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
            game3.bet_one.scale.setTo(0.7, 0.7);
            game3.bet_one.inputEnabled = true;
            game3.bet_one.input.useHandCursor = true;
            game3.bet_one.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.bet_one.loadTexture('bet_one_p');
            });
            game3.bet_one.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.bet_one.loadTexture('bet_one');
            });
            game3.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
            game3.paytable.scale.setTo(0.7, 0.7);
            game3.paytable.inputEnabled = true;
            game3.paytable.input.useHandCursor = true;
            game3.paytable.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.paytable.loadTexture('paytable_p');
            });
            game3.paytable.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.paytable.loadTexture('paytable');
            });
            game3.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
            game3.select_game.scale.setTo(0.7, 0.7);
            game3.select_game.inputEnabled = true;
            game3.select_game.input.useHandCursor = true;
            game3.select_game.events.onInputOver.add(function(){
                if(game3.spinStatus)
                    return;
                game3.select_game.loadTexture('select_game_p');
            });
            game3.select_game.events.onInputOut.add(function(){
                if(game3.spinStatus)
                    return;
                game3.select_game.loadTexture('select_game');
            });
        }
        createLevelButtons();
        if(!isMobile)
            full_and_sound();
        game3.soldierAnimation.play();
        game3.safe_shineAnimation.play();
    }

};
game.state.add('game3', game3);

var game4 = {
    buttonsPositionsX : {1:250, 3:295, 5:340, 7:385, 9:430},
    selectedDoor : null,
    freeze : false,
    create : function() {
        game4.bg = game.add.sprite(94+mobileX,54+mobileY, 'game4.bg');
        game4.bg_top = game.add.sprite(95+mobileX,22+mobileY, 'game2.bg_top1');

        codeDoorSound = game.add.audio('codeDoor');
        game4_winSound = game.add.audio('game4_win');
        game4_loseSound = game.add.audio('game4_lose');

        game4.buttonleft = game.add.sprite(222+mobileX,54+mobileY, 'game4.buttonleft');
        game4.buttonleft.visible = false;
        game4.buttonright = game.add.sprite(542+mobileX,54+mobileY, 'game4.buttonright');
        game4.buttonright.visible = false;
        game4.girl = game.add.sprite(255+mobileX,183+mobileY, 'game4.girl');
        game4.girl.visible = false;
        game4.doorLeft = game.add.sprite(238+mobileX,166+mobileY, 'game4.door');
        game4.doorLeft.visible = true;
        if(isMobile){
            game4.doorLeft.inputEnabled = true;
            game4.doorLeft.input.useHandCursor = true;
        }
        game4.doorRight = game.add.sprite(558+mobileX,166+mobileY, 'game4.door');
        game4.doorRight.visible = true;
        if(isMobile){
            game4.doorRight.inputEnabled = true;
            game4.doorRight.input.useHandCursor = true;
        }
        game4.green_door = game.add.sprite(173+mobileX,255+mobileY, 'game4.green_door');
        game4.green_door.visible = false;

        game4.soldierhead = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldierhead_animation');
        game4.soldierheadAnimation = game4.soldierhead.animations.add('game4.soldierhead', [0,1,2,3], 5, false);
        game4.soldierhead.visible = false;
        game4.soldier = game.add.sprite(304+mobileX, 262+mobileY, 'game2.soldier_animation');
        game4.soldierAnimation = game4.soldier.animations.add('game2.soldier', [0,1,2,3,4,5,6,7,8,9,10], 5, false);
        game4.soldier.visible = true;
        game4.soldierAnimation.play();
        game4.soldierAnimation.onComplete.add(function(){
            game4.soldierhead.visible = true;
            game4.soldier.visible = false;
            game4.soldierheadAnimation.play();
        });
        game4.soldierheadAnimation.onComplete.add(function(){
            game4.soldierhead.visible = false;
            game4.soldier.visible = true;
            game4.soldierAnimation.play();
        });
        game4.eyes_animation = game.add.sprite(255+mobileX, 183+mobileY, 'game4.eyes_animation');
        game4.eyes_animationAnim = game4.eyes_animation.animations.add('game4.eyes_animation', [0,1,2,3], 10, true);
        game4.eyes_animation.visible = false;
        game4.security = game.add.sprite(558+mobileX, 183+mobileY, 'game4.security');
        game4.securityAnim = game4.security.animations.add('game4.security', [0,1,2,3,4,5,6,7,8], 5, false);
        game4.security.visible = false;
        game4.select_door = game.add.sprite(71+mobileX, 198+mobileY, 'game4.select_door');
        // game4.select_door = game.add.sprite(382, 198, 'game4.select_door');
        game4.select_doorAnim = game4.select_door.animations.add('game4.select_door', [0,1,2,3,4,5,6,7,8], 6, false);
        game4.select_door.visible = false;
        game4.bust = game.add.sprite(271+mobileX, 231+mobileY, 'game4.bust');
        game4.bustAnim = game4.bust.animations.add('game4.bust', [0,1,2,3,4,5,6], 5, true);
        game4.bust.visible = false;
        game4.one_hearts = game.add.sprite(319+mobileX, 221+mobileY, 'game4.one_hearts');
        game4.one_heartsAnim = game4.one_hearts.animations.add('game4.one_hearts', [5,6,7,8,9,1,2,3,4], 10, true);
        game4.one_hearts.visible = false;
        game4.two_hearts = game.add.sprite(300+mobileX, 241+mobileY, 'game4.two_hearts');
        game4.two_heartsAnim = game4.two_hearts.animations.add('game4.two_hearts', [0,1,2,3,4,5,6,7,8,9], 10, true);
        game4.two_hearts.visible = false;
        game4.soldier_lose = game.add.sprite(71+mobileX, 198+mobileY, 'game2.soldier_lose');
        game4.soldier_loseAnimation = game4.soldier_lose.animations.add('game2.soldier_lose', [0,1], 5, false);
        game4.soldier_lose.visible = false;
        game4.soldier_win = game.add.sprite(71+mobileX, 198+mobileY, 'game2.soldier_win');
        game4.soldier_winAnimation = game4.soldier_win.animations.add('game2.soldier_win', [0,1,2,3], 5, false);
        game4.soldier_win.visible = false;
        if (isMobile)
            game4.bottom_line = game.add.sprite(94+mobileX,439, 'game1.bottom_line');
        if(!isMobile){
            game.add.sprite(0,0, 'main_window');
        }

        if(!isMobile){
            game4.startButton = game.add.sprite(610+mobileX, 510+mobileY, 'start');
            game4.startButton.scale.setTo(0.7, 0.7);
            game4.startButton.inputEnabled = true;
            game4.startButton.input.useHandCursor = true;
            game4.startButton.events.onInputOver.add(function(){
                game4.startButton.loadTexture('start_p');
            });
            game4.startButton.events.onInputOut.add(function(){
                game4.startButton.loadTexture('start');
            });

            game4.automatic_start = game.add.sprite(685+mobileX, 510+mobileY, 'automatic_start');
            game4.automatic_start.scale.setTo(0.7, 0.7);
            game4.automatic_start.inputEnabled = true;
            game4.automatic_start.input.useHandCursor = true;
            game4.automatic_start.events.onInputOver.add(function(){
                game4.automatic_start.loadTexture('automatic_start_p');
            });
            game4.automatic_start.events.onInputOut.add(function(){
                game4.automatic_start.loadTexture('automatic_start');
            });
            game4.bet_max = game.add.sprite(545+mobileX, 510+mobileY, 'bet_max');
            game4.bet_max.scale.setTo(0.7, 0.7);
            game4.bet_max.inputEnabled = true;
            game4.bet_max.input.useHandCursor = true;
            game4.bet_max.events.onInputOver.add(function(){
                game4.bet_max.loadTexture('bet_max_p');
            });
            game4.bet_max.events.onInputOut.add(function(){
                game4.bet_max.loadTexture('bet_max');
            });
            game4.bet_one = game.add.sprite(500+mobileX, 510+mobileY, 'bet_one');
            game4.bet_one.scale.setTo(0.7, 0.7);
            game4.bet_one.inputEnabled = true;
            game4.bet_one.input.useHandCursor = true;
            game4.bet_one.events.onInputOver.add(function(){
                game4.bet_one.loadTexture('bet_one_p');
            });
            game4.bet_one.events.onInputOut.add(function(){
                game4.bet_one.loadTexture('bet_one');
            });
            game4.paytable = game.add.sprite(160+mobileX, 510+mobileY, 'paytable');
            game4.paytable.scale.setTo(0.7, 0.7);
            game4.paytable.inputEnabled = true;
            game4.paytable.input.useHandCursor = true;
            game4.paytable.events.onInputOver.add(function(){
                game4.paytable.loadTexture('paytable_p');
            });
            game4.paytable.events.onInputOut.add(function(){
                game4.paytable.loadTexture('paytable');
            });
            game4.select_game = game.add.sprite(70+mobileX, 510+mobileY, 'select_game');
            game4.select_game.scale.setTo(0.7, 0.7);
            game4.select_game.inputEnabled = true;
            game4.select_game.input.useHandCursor = true;
            game4.select_game.events.onInputOver.add(function(){
                game4.select_game.loadTexture('select_game_p');
            });
            game4.select_game.events.onInputOut.add(function(){
                game4.select_game.loadTexture('select_game');
            });
        }
        for(var i in game4.buttonsPositionsX) {
            if(!isMobile){
                var button = game.add.sprite(game4.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline' + i);
                button.scale.setTo(0.7, 0.7);
                if (i == 9 || i == 1 || i == 5){
                    button = game.add.sprite(game4.buttonsPositionsX[i]+mobileX, 510+mobileY, 'btnline_d' + i);
                    button.scale.setTo(0.7, 0.7);
                    button.inputEnabled = false;
                } else {
                    button.inputEnabled = true;
                    button.input.useHandCursor = true;
                    (function(n, btn){
                        btn.events.onInputUp.add(function () {
                            if(game4.freeze || n==9 || n==5 || n==1) {
                                return;
                            }
                            game4.freeze = true;
                            btn.inputEnabled = false;
                            if (n == 3){
                                btn.loadTexture('btnline_d' + 3);
                                game4.select_door.position.x = 71+mobileX;
                            } else {
                                game4.select_door.position.x = 392+mobileX;
                                btn.loadTexture('btnline_d' + 7);
                            }
                            game4.soldierhead.visible = false;
                            game4.soldier.visible = false;
                            game4.select_door.visible = true;
                            game4.select_doorAnim.play();
                            game4.selectedDoor = n;
                            codeDoorSound.play();
                            // game4.safe_shine.visible = false;
                            // game4.safe_shineAnimation.stop();
                        }, this);
                    })(i, button);
                }
            } else {
                game4.doorLeft.events.onInputUp.add(function () {
                    if(game4.freeze) {
                        return;
                    }
                    game4.freeze = true;
                    game4.doorLeft.inputEnabled = false;
                    game4.select_door.position.x = 71+mobileX;
                    game4.soldierhead.visible = false;
                    game4.soldier.visible = false;
                    game4.select_door.visible = true;
                    game4.select_doorAnim.play();
                    game4.selectedDoor = 3;
                    codeDoorSound.play();
                    // game4.safe_shine.visible = false;
                    // game4.safe_shineAnimation.stop();
                }, this);
                game4.doorRight.events.onInputUp.add(function () {
                    if(game4.freeze) {
                        return;
                    }
                    game4.freeze = true;
                    game4.doorRight.inputEnabled = false;
                    game4.select_door.position.x = 392+mobileX;
                    game4.soldierhead.visible = false;
                    game4.soldier.visible = false;
                    game4.select_door.visible = true;
                    game4.select_doorAnim.play();
                    game4.selectedDoor = 9;
                    codeDoorSound.play();
                    // game4.safe_shine.visible = false;
                    // game4.safe_shineAnimation.stop();
                }, this);
            }
        }
        game4.select_doorAnim.onComplete.add(function(){
            if (game4.selectedDoor == 3) {
                game4.doorLeft.visible = false;
                game4.green_door.visible = true;
                game4.girl.visible = true;
                game4.eyes_animationAnim.play()
                game4.eyes_animation.visible = true;
                game4.bustAnim.play()
                game4.bust.visible = true;
                game4.one_heartsAnim.play()
                game4.one_hearts.visible = true;
                game4.two_heartsAnim.play()
                game4.two_hearts.visible = true;
                game4.buttonleft.visible = true;
                game4.soldier_win.position.x = 71+mobileX;
                game4.soldier_win.visible = true;
                game4.soldier_winAnimation.play();
                game4.select_door.visible = false;
                game4_winSound.play();
            } else {
                game4.doorRight.visible = false;
                game4.green_door.visible = true;
                game4.green_door.position.x = 493+mobileX;
                game4.security.visible = true;
                game4.securityAnim.play()
                game4.buttonright.visible = true;
                game4.soldier_lose.position.x = 392+mobileX;
                game4.soldier_lose.visible = true;
                game4.soldier_loseAnimation.play();
                game4.select_door.visible = false;
                game4_loseSound.play();
            }
        });
        createLevelButtons();
        if(!isMobile)
            full_and_sound();
    }


};
game.state.add('game4', game4);

var gamePreload = {

    preload:function(){
        if (isMobile){
            game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
                console.log(progress);
                if(progress % 8 == 0) {
                    document.getElementById('preloaderBar').style.width = progress + '%';
                }
            });
        }
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        // game.load.image('main_window', 'img/main_window.png');
        if(!isMobile){
            game.load.image('main_window', 'img/shape1206.svg');
        }
        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');
        game.load.image('game3.bg', 'img/bg_game3.png');
        game.load.image('game4.bg', 'img/bg_game4.png');
        game.load.image('game1.bg_top1', 'img/main_bg_1.png');
        game.load.image('game1.bg_top2', 'img/main_bg_2.png');
        game.load.image('game2.bg_top1', 'img/main_bg_top1.png');
        game.load.image('game2.bg_top2', 'img/main_bg_top2.png');
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('game1.big_fire', 'img/big_fire.png');
        game.load.image('x', 'img/x.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('game1.page_' + i, 'img/page_' + i + '.png');
        }

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');

        // game.load.image('game1.bar', 'img/shape_full.png');
        game.load.image('game1.bar', 'img/shape_fullv3.png');
        if(!isMobile){
            game.load.image('start', 'img/btns/btn_start.png');
            game.load.image('start_p', 'img/btns/btn_start_p.png');
            game.load.image('start_d', 'img/btns/btn_start_d.png');
            game.load.image('automatic_start', 'img/btns/btn_automatic_start.png');
            game.load.image('automatic_start_p', 'img/btns/btn_automatic_start_p.png');
            game.load.image('automatic_start_d', 'img/btns/btn_automatic_start_d.png');
            game.load.image('bet_max', 'img/btns/btn_bet_max.png');
            game.load.image('bet_max_p', 'img/btns/btn_bet_max_p.png');
            game.load.image('bet_max_d', 'img/btns/btn_bet_max_d.png');
            game.load.image('bet_one', 'img/btns/btn_bet_one.png');
            game.load.image('bet_one_p', 'img/btns/btn_bet_one_p.png');
            game.load.image('bet_one_d', 'img/btns/btn_bet_one_d.png');
            game.load.image('paytable', 'img/btns/btn_paytable.png');
            game.load.image('paytable_p', 'img/btns/btn_paytable_p.png');
            game.load.image('paytable_d', 'img/btns/btn_paytable_d.png');
            game.load.image('select_game', 'img/btns/btn_select_game.png');
            game.load.image('select_gamev2', 'img/btns/btn_select_gamev2.png');
            game.load.image('select_game_p', 'img/btns/btn_select_game_p.png');
            game.load.image('select_game_d', 'img/btns/btn_select_game_d.png');
            game.load.image('non_full', 'img/non_full.png');
            game.load.image('full', 'img/full.png');
            game.load.image('sound_on', 'img/sound_on.png');
            game.load.image('sound_off', 'img/sound_off.png');
        }
        if(isMobile) {
            game.load.image('spin', 'img/spin.png');
            game.load.image('spin_p', 'img/spin_p.png');
            game.load.image('spin_d', 'img/spin_d.png');
            game.load.image('bet1', 'img/bet1.png');
            game.load.image('bet1_p', 'img/bet1_p.png');
            game.load.image('home', 'img/home.png');
            game.load.image('home_p', 'img/home_p.png');
            game.load.image('dollar', 'img/dollar.png');
            game.load.image('gear', 'img/gear.png');
            game.load.image('double', 'img/double.png');
        }
        game.load.image('shirt_cards', 'img/shirt_cards.png');
        game.load.image('dealer', 'img/dealer.png');
        game.load.image('pick_card', 'img/pick_game2.png');
        game.load.image('card_15', 'img/cards/joker.png');
        game.load.image('card_2b', 'img/cards/2b.png');
        game.load.image('card_5', 'img/cards/5b.png');
        game.load.image('card_2c', 'img/cards/2c.png');
        game.load.image('card_7', 'img/cards/7c.png');
        game.load.image('card_2p', 'img/cards/2p.png');
        game.load.image('card_12', 'img/cards/12p.png');
        game.load.image('card_2t', 'img/cards/2t.png');
        game.load.image('card_13', 'img/cards/13t.png');

        game.load.image('wild_simbol', 'img/image537.png');

        game.load.image('game3.bottle', 'img/bottle.png');
        game.load.image('game3.book', 'img/book.png');
        game.load.image('game3.destroy_door_3', 'img/destroy_door_3.png');
        game.load.image('game3.big_fire', 'img/game3_big_fire.png');
        game.load.spritesheet('game3.dynamite', 'img/dynamite.png', 64, 80, 8);
        game.load.spritesheet('game3.gold', 'img/gold.png', 64, 80, 3);
        game.load.spritesheet('game3.cup', 'img/cup.png', 64, 80, 7);
        game.load.spritesheet('game3.dynamite_off', 'img/dynamite_off.png', 64, 80, 2);
        game.load.spritesheet('game3.soldier_win', 'img/soldier_animation_good_open.png', 208, 240, 3);

        game.load.image('game4.buttonleft', 'img/buttonleft.png');
        game.load.image('game4.buttonright', 'img/buttonright.png');
        game.load.image('game4.door', 'img/door.png');
        game.load.image('game4.girl', 'img/girl.png');
        game.load.image('game4.green_door', 'img/green_door.png');
        game.load.spritesheet('game4.eyes_animation', 'img/eyes_animation.png', 96, 176, 4);
        game.load.spritesheet('game4.one_hearts', 'img/one_hearts.png', 32, 32, 10);
        game.load.spritesheet('game4.two_hearts', 'img/two_hearts.png', 48, 32, 10);
        game.load.spritesheet('game4.security', 'img/security.png', 128, 224, 9);
        game.load.spritesheet('game4.bust', 'img/bust.png', 64, 48, 7);
        game.load.spritesheet('game4.select_door', 'img/select_door.png', 144, 240, 9);

        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('pickCard', 'sounds/pickCard.mp3');
        game.load.audio('cardWin', 'sounds/cardWin.mp3');
        game.load.audio('hit', 'sounds/hit.mp3');
        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('dynamite', 'sounds/dynamite.mp3');
        game.load.audio('boom', 'sounds/boom.mp3');
        game.load.audio('safeWin', 'sounds/safeWin.mp3');
        game.load.audio('codeDoor', 'sounds/codeDoor.mp3');
        game.load.audio('game4_win', 'sounds/game4_win.mp3');
        game.load.audio('game4_lose', 'sounds/game4_lose.mp3');
        game.load.audio('page', 'sounds/page.mp3');
        game.load.audio('fire_extinguisher_action', 'sounds/fire_extinguisher_action.mp3');
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'img/lines/select/line' + i + '.png');
            game.load.image('linefull_' + i, 'img/lines/win/linefull' + i + '.png');
            game.load.image('win_' + i, 'img/win_' + i + '.png');

            if (i % 2 != 0) {
                game.load.audio('line' + i, 'sounds/line' + i + '.wav');
                if(!isMobile){
                    game.load.image('btnline' + i, 'img/btns/btn' + i + '.png');
                    game.load.image('btnline_p' + i, 'img/btns/btn' + i + '_p.png');
                    game.load.image('btnline_d' + i, 'img/btns/btn' + i + '_d.png');
                }
            }
        }

        game.load.spritesheet('smoke', 'img/smoke.png', 80, 80, 20);
        game.load.spritesheet('page', 'img/page_animation2.png', 128, 112, 8);
        game.load.spritesheet('big_fire_emergence', 'img/big_fire_emergence.png', 64, 80, 4);
        game.load.spritesheet('safe_row_win', 'img/safe_row_win.png', 96, 112, 6);
        game.load.spritesheet('game2.soldierhead_animation', 'img/game2_soldierhead_animation.png', 192, 240, 4);
        game.load.spritesheet('game2.soldier_animation', 'img/game2_soldier_animation.png', 192, 240, 11);
        game.load.spritesheet('game2.soldier_lose', 'img/game2_soldier_lose.png', 192, 240, 2);
        game.load.spritesheet('game2.soldier_win', 'img/game2_soldier_win.png', 192, 240, 4);
        game.load.spritesheet('game3.safe_shine', 'img/safe_shine.png', 96, 128, 9);
        game.load.spritesheet('game3.hit_safe', 'img/soldier_animation_safe_open.png', 208, 240, 5);
        game.load.spritesheet('game3.boom_animation', 'img/boom_animation.png', 208, 240, 11);
        game.load.spritesheet('game3.safe_door', 'img/safe_door.png', 96, 128, 3);
        game.load.spritesheet('game3.door_knob', 'img/door_knob.png', 32, 32, 4);
        game.load.spritesheet('game3.big_fire_extinguisher_action', 'img/big_fire_extinguisher_action.png', 208, 240, 11);

        game.load.audio('rotate', 'sounds/rotate.wav');
        for (var i = 1; i <= 7; ++i) {
            if (i % 2 != 0) {
                game.load.spritesheet('game3.safe_hit_'+i, 'img/safe_hit_' +i+ '.png', 112, 128, 3);
                game.load.image('game3.safe_'+ i, 'img/safe_'+ i +'.png');
            }
        }

    },
    create:function(){
        game.state.start('game1'); //переключение на 1 игру
        document.getElementById('preloader').style.display = 'none';
    }

};
game.state.add('gamePreload', gamePreload); //добавление загрузчика в игру

game.state.start('gamePreload'); //начало игры с локации загрузчика

$(document).ready(function(){
    var HeightNow = document.documentElement.clientHeight;
    console.log(HeightNow);
    function checkWidth() {

        var realHeight = document.documentElement.clientHeight;
        if (HeightNow > realHeight){    // Если текущий размер  меньше размера в полном экране, то выставляем заглушку
            console.log('выход из полноэкранного режима');
            full.loadTexture('non_full');
            fullStatus = false;
        }
        HeightNow = realHeight;
    };
    $( window ).resize(function() {
        var realHeight = document.documentElement.clientHeight;
    });
    window.addEventListener('resize', checkWidth);
    checkWidth();
});