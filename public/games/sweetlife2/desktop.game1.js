var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

var gamename = 'sweetlife2rus';
var checkHelm = false;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        //var betOneSound = game.add.audio('game.betOneSound');
        //var betMaxSound = game.add.audio('game.betMaxSound');

        
        
        
        
        

        slotPosition = [[142, 90], [142, 202], [142, 314], [254, 90], [254, 202], [254, 314], [365, 90], [365, 202], [365, 314], [477, 90], [477, 202], [477, 314], [589, 90], [589, 202], [589, 314]];
        addSlots(game, slotPosition);
        // изображения

        game.add.sprite(94,22, 'game.background1');
        topBarImage = game.add.sprite(94,22, 'topScoreGame1');
        game.add.sprite(0,0, 'game.background');

        addTableTitle(game, 'play1To',558,422);

        var linePosition = [[134,245], [134,109], [134,380], [134,156], [134,130], [134,130], [134,261], [134,268], [134,155]];
        var numberPosition = [[94,230], [94,86], [94,374], [94,150], [94,310], [94,118], [94,342], [94,262], [94,198]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);

        var pageCount = 5;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        // анимация

        game.bear = game.add.sprite(125, 391, 'bear');
        game.bearAnimation = game.bear.animations.add('bear', [0,1,2,3,4,5,6,2,1,0,0,0,0,0,0,0,0], 8, true);
        game.bearAnimation.play();     
        game.ears = game.add.sprite(126, 391, 'ears');
        game.earsAnimation = game.ears.animations.add('ears', [0,1,2,1,0,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 12, true);
        game.earsAnimation.play();
        game.win = game.add.sprite(125, 391, 'game.win1');
        game.winAnimation = game.win.animations.add('game.win1', [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 10, false);
        game.win.visible = false;  
        game.bee = game.add.sprite(414, 390, 'bee');
        game.beeAnimation = game.bee.animations.add('bee', [0,1,2,3,4,5,6,7,6,5,4,3,2,1], 8, true);
        game.beeAnimation.play();
        game.winAnimation.onComplete.add(function(){
            game.win.visible = false;
            game.bear.visible = true;
            game.ears.visible = true;
            game.bee.visible = true;
        }); 
        // function winAnim(){
        //     game.bear.visible = false;
        //     game.ears.visible = false;
        //     game.bee.visible = false;
        //     game.win.visible = true; 
        //     game.winAnimation.play();
        // };

        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[140, 447], [324, 447], [502, 447]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);

        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
        }
    };

    game.state.add('game1', game1);

};
