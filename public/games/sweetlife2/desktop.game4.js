function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 50;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, 50);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        unlockDisplay();
        game.state.start('game1');
    }, timeInterval);
}

function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {
            //Добавление фона
            background = game.add.sprite(94,22, 'topScoreGame4');
            backgroundGame4 = game.add.sprite(95,55, 'game.backgroundGame4');
            background = game.add.sprite(0,0, 'game.background');
            open_game4 = game.add.audio('open_game4');
            win_game4 = game.add.audio('win_game4');
            number_win = game.add.audio('number_win');
            // winPoint1 = game.add.audio('game.winPoint1');
            // winPoint2 = game.add.audio('game.winPoint2');

            game.chicken = game.add.sprite(286, 246, 'game3.chicken');
            game.chicken.animations.add('game3.chicken', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31], 8, true).play();
            game.smoke = game.add.sprite(350, 182, 'game3.smoke');
            game.smoke.animations.add('game3.smoke', [0,1,2,3,4,5,6,7], 8, true).play();   
            game.bear_wait_1 = game.add.sprite(254, 262, 'game3.bear_wait_1');
            game.bear_wait_1Anim = game.bear_wait_1.animations.add('game3.bear_wait_1', [0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3,4,5,6,7,4,5,6,7,8,9,10,11], 8, false).play();
            game.bear_wait_2 = game.add.sprite(254, 262, 'game3.bear_wait_2');
            game.bear_wait_2.visible = false;
            game.bear_wait_2Anim = game.bear_wait_2.animations.add('game3.bear_wait_2', [0,1,2,3,4,5,6,7,8,9], 8, false)
            game.bear_wait_1Anim.onComplete.add(function(){
                game.bear_wait_1.visible = false;
                game.bear_wait_2.visible = true;
                game.bear_wait_2Anim.play();
            });
            game.bear_wait_2Anim.onComplete.add(function(){
                game.bear_wait_2.visible = false;
                game.bear_wait_1.visible = true;
                game.bear_wait_1Anim.play();
            });    
            game.bear_right_honey_0 = game.add.sprite(254, 262, 'game3.bear_right_empty_1');
            game.bear_right_honey_0 .visible = false;
            game.bear_right_honey_0Anim = game.bear_right_honey_0.animations.add('game3.bear_right_empty_1', [0,1,2,3,4,5,6], 8, false);  
            game.bear_right_honey_1 = game.add.sprite(254, 262, 'game3.bear_right_honey_1');
            game.bear_right_honey_1 .visible = false;
            game.bear_right_honey_1Anim = game.bear_right_honey_1.animations.add('game3.bear_right_honey_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
            game.bear_right_honey_2 = game.add.sprite(254, 262, 'game3.bear_right_honey_2');
            game.bear_right_honey_2 .visible = false;
            game.bear_right_honey_2Anim = game.bear_right_honey_2.animations.add('game3.bear_right_honey_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
            game.bear_right_honey_3 = game.add.sprite(254, 262, 'game3.bear_right_honey_3');
            game.bear_right_honey_3 .visible = false;
            game.bear_right_honey_3Anim = game.bear_right_honey_3.animations.add('game3.bear_right_honey_3', [0,1,1,1,1,1,1], 8, false);
            game.bear_right_honey_0Anim.onComplete.add(function(){
                game.bear_right_honey_0.visible = false;
                game.bear_right_honey_1.visible = true;
                game.bear_right_honey_1Anim.play();
            });   
            game.bear_right_honey_1Anim.onComplete.add(function(){
                game.bear_right_honey_1.visible = false;
                game.bear_right_honey_2.visible = true;
                game.bear_right_honey_2Anim.play();
                win_game4.play()
            });
            game.bear_right_honey_2Anim.onComplete.add(function(){
                game.bear_right_honey_2.visible = false;
                game.bear_right_honey_3.visible = true;
                game.bear_right_honey_3Anim.play();
                number_win.play();
            }); 
            game.bear_right_honey_3Anim.onComplete.add(function(){
             updateBalanceGame4(game, scorePosions, balanceR);
         });
            game.bear_left_honey_0 = game.add.sprite(254, 262, 'game3.bear_left_empty_1');
            game.bear_left_honey_0 .visible = false;
            game.bear_left_honey_0Anim = game.bear_left_honey_0.animations.add('game3.bear_left_empty_1', [0,1,2,3,4,5,6], 8, false);  
            game.bear_left_honey_1 = game.add.sprite(254, 262, 'game3.bear_left_honey_1');
            game.bear_left_honey_1 .visible = false;
            game.bear_left_honey_1Anim = game.bear_left_honey_1.animations.add('game3.bear_left_honey_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
            game.bear_left_honey_2 = game.add.sprite(254, 262, 'game3.bear_left_honey_2');
            game.bear_left_honey_2 .visible = false;
            game.bear_left_honey_2Anim = game.bear_left_honey_2.animations.add('game3.bear_left_honey_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
            game.bear_left_honey_3 = game.add.sprite(254, 262, 'game3.bear_left_honey_3');
            game.bear_left_honey_3 .visible = false;
            game.bear_left_honey_3Anim = game.bear_left_honey_3.animations.add('game3.bear_left_honey_3', [0,1,1,1,1,1,1], 8, false);
            game.bear_left_honey_0Anim.onComplete.add(function(){
                game.bear_left_honey_0.visible = false;
                game.bear_left_honey_1.visible = true;
                game.bear_left_honey_1Anim.play();
            });   
            game.bear_left_honey_1Anim.onComplete.add(function(){
                game.bear_left_honey_1.visible = false;
                game.bear_left_honey_2.visible = true;
                game.bear_left_honey_2Anim.play();
                win_game4.play()
            });
            game.bear_left_honey_2Anim.onComplete.add(function(){
                game.bear_left_honey_2.visible = false;
                game.bear_left_honey_3.visible = true;
                game.bear_left_honey_3Anim.play();
                number_win.play();
            }); 

            game.bear_right_honey_3Anim.onComplete.add(function(){

             updateBalanceGame4(game, scorePosions, balanceR);
         });
            game.bear_left_empty_1 = game.add.sprite(254, 262, 'game3.bear_left_empty_1');
            game.bear_left_empty_1 .visible = false;
            game.bear_left_empty_1Anim = game.bear_left_empty_1.animations.add('game3.bear_left_empty_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
            game.bear_left_empty_2 = game.add.sprite(254, 246, 'game3.bear_left_empty_2');
            game.bear_left_empty_2 .visible = false;
            game.bear_left_empty_2Anim = game.bear_left_empty_2.animations.add('game3.bear_left_empty_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
            game.bear_left_empty_3 = game.add.sprite(254, 246, 'game3.bear_left_empty_3');
            game.bear_left_empty_3 .visible = false;
            game.bear_left_empty_3Anim = game.bear_left_empty_3.animations.add('game3.bear_left_empty_3', [0,1,2,3,4,5,6,6,6,6], 8, false);
            game.bear_left_empty_1Anim.onComplete.add(function(){
                game.bear_left_empty_1.visible = false;
                game.bear_left_empty_2.visible = true;
                game.bear_left_empty_2Anim.play();
            });
            game.bear_left_empty_2Anim.onComplete.add(function(){
                game.bear_left_empty_2.visible = false;
                game.bear_left_empty_3.visible = true;
                game.bear_left_empty_3Anim.play();
            });     
            game.bear_left_empty_3Anim.onComplete.add(function(){
             updateBalanceGame4(game, scorePosions, balanceR);
         });
            game.bear_right_empty_1 = game.add.sprite(254, 262, 'game3.bear_right_empty_1');
            game.bear_right_empty_1 .visible = false;
            game.bear_right_empty_1Anim = game.bear_right_empty_1.animations.add('game3.bear_right_empty_1', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);   
            game.bear_right_empty_2 = game.add.sprite(254, 246, 'game3.bear_right_empty_2');
            game.bear_right_empty_2 .visible = false;
            game.bear_right_empty_2Anim = game.bear_right_empty_2.animations.add('game3.bear_right_empty_2', [0,1,2,3,4,5,6,7,8,9,10,11], 8, false);
            game.bear_right_empty_3 = game.add.sprite(254, 246, 'game3.bear_right_empty_3');
            game.bear_right_empty_3 .visible = false;
            game.bear_right_empty_3Anim = game.bear_right_empty_3.animations.add('game3.bear_right_empty_3', [0,1,2,3,4,5,6,6,6,6], 8, false);
            game.bear_right_empty_1Anim.onComplete.add(function(){
                game.bear_right_empty_1.visible = false;
                game.bear_right_empty_2.visible = true;
                game.bear_right_empty_2Anim.play();
            });
            game.bear_right_empty_2Anim.onComplete.add(function(){
                game.bear_right_empty_2.visible = false;
                game.bear_right_empty_3.visible = true;
                game.bear_right_empty_3Anim.play();
            });     
            game.bear_right_empty_3Anim.onComplete.add(function(){
              updateBalanceGame4(game, scorePosions, balanceR);
          });
            //счет
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);

            //кнопок

            var position = 1;

            var selectGame = game.add.sprite(70,510, 'selectGame_d');
            selectGame.scale.setTo(0.7, 0.7);
            selectGame.inputEnabled = false;

            var payTable = game.add.sprite(150,510, 'payTable_d');
            payTable.scale.setTo(0.7, 0.7);
            payTable.inputEnabled = false;

            var betone = game.add.sprite(490,510, 'betone_d');
            betone.scale.setTo(0.7, 0.7);
            betone.inputEnabled = false;


            var betmax = game.add.sprite(535,510, 'betmax_d');
            betmax.scale.setTo(0.7, 0.7);
            betmax.inputEnabled = false;

            var automaricstart = game.add.sprite(685,510, 'automaricstart_d');
            automaricstart.scale.setTo(0.7, 0.7);
            automaricstart.inputEnabled = false;

            var startButton = game.add.sprite(597, 510, 'startButton_d');
            startButton.scale.setTo(0.7,0.7);
            startButton.inputEnabled = false;

            var buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
            buttonLine1.scale.setTo(0.7,0.7);
            buttonLine1.inputEnabled = false;

            var buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
            buttonLine3.scale.setTo(0.7,0.7);
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            var buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
            buttonLine5.scale.setTo(0.7,0.7);
            buttonLine5.inputEnabled = false;

            var buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
            buttonLine7.scale.setTo(0.7,0.7);
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            var buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
            buttonLine9.scale.setTo(0.7,0.7);
            buttonLine9.inputEnabled = false;

            buttonLine3.events.onInputOver.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3_p');
                }
            });
            buttonLine3.events.onInputOut.add(function(){
                if(buttonLine3.inputEnabled == true) {
                    buttonLine3.loadTexture('buttonLine3');
                }
            });

            buttonLine7.events.onInputOver.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7_p');
                }
            });
            buttonLine7.events.onInputOut.add(function(){
                if(buttonLine7.inputEnabled == true) {
                    buttonLine7.loadTexture('buttonLine7');
                }
            });

            buttonLine3.events.onInputDown.add(function(){
                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;
                lockDisplay();
                gnome4Hide();
                if(ropeValues[5] != 0){
                 game.bear_left_honey_0Anim.play();
                 game.bear_left_honey_0.visible = true;
                 setTimeout(function() {                    
                    open_game4.play();
                }, 900);
             } else {
                game.bear_left_empty_1Anim.play();
                game.bear_left_empty_1.visible = true;
                setTimeout(function() {                    
                    open_game4.play();
                }, 900);
            }

        });
            buttonLine7.events.onInputDown.add(function(){

                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;
                lockDisplay();
                gnome4Hide();
                if(ropeValues[5] != 0){
                    game.bear_right_honey_0Anim.play();
                    game.bear_right_honey_0.visible = true;
                    setTimeout(function() {                    
                        open_game4.play();
                    }, 900);
                } else {
                    game.bear_right_empty_1Anim.play();
                    game.bear_right_empty_1.visible = true;
                    setTimeout(function() {                    
                        open_game4.play();
                    }, 900);
                }

            });

            function gnome4Hide(){
                game.bear_wait_1.visible = false;
                game.bear_wait_2.visible = false;

                game.bear_wait_1Anim.stop();
                game.bear_wait_2Anim.stop();
            }

            full_and_sound();
        };

        game4.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game4', game4);

})();


}