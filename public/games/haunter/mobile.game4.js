function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });
    
    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}

function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //звуки
            winCover = game.add.audio('winCover');


            //изображения
            //Добавление фона
            backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame4');
            backgroundTotal = game.add.sprite(95-mobileX,23-mobileY, 'game.backgroundTotal');


            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [435-mobileX, 26-mobileY, 20], [610-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);


            //логика и анимации
            gramofon4 = game.add.sprite(414-mobileX,406-mobileY, 'game.gramofon4');
            gramofon4.animations.add('gramofon4', [01,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 4, true);
            gramofon4.animations.getAnimation('gramofon4').play();


            //gramofonR2 = game.add.sprite(530,406, 'game.gramofonR2');

            barmen_part41 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part41');
            barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
            barmen_part41.animations.getAnimation('barmen_part41').play();

            backgroundBludo = game.add.sprite(329-mobileX,160-mobileY, 'game.backgroundBludo');
            bludoTitle = game.add.sprite(159-mobileX,310-mobileY, 'game.bludoTitle');

            arrows = game.add.sprite(345-mobileX,175-mobileY, 'game.arrows');
            arrows.animations.add('arrows', [0,1], 3, true);
            arrows.animations.getAnimation('arrows').play();

            bludo1 = game.add.sprite(125-mobileX,130-mobileY, 'game.bludo');
            bludo1.inputEnabled = true;
            bludo1.events.onInputDown.add(function(){

                bludo1.inputEnabled = false;

                if(ropeValues[5] != 0){
                    lockDisplay();
                    winCover.play();

                    bludo1.visible = false;
                    chickenBludo1.visible = true;

                    chikenAnin1 = game.add.sprite(167-mobileX,122-mobileY, 'game.chikenAnin');
                    chikenAnin1.animations.add('chikenAnin1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
                    chikenAnin1.animations.getAnimation('chikenAnin1').play();

                    updateBalanceGame4(game, scorePosions, balanceR);
                    setTimeout('unlockDisplay(); game.state.start("game1");',8000);
                } else {
                    lockDisplay();

                    bludo1.visible = false;
                    fishBludo1.visible = true;

                    updateBalanceGame4(game, scorePosions, balanceR);
                    setTimeout('unlockDisplay(); game.state.start("game1");',4000);
                }

            });

            bludo2 = game.add.sprite(495-mobileX,130-mobileY, 'game.bludo');
            bludo2.inputEnabled = true;
            bludo2.events.onInputDown.add(function(){

                bludo2.inputEnabled = false;

                if(ropeValues[5] != 0){
                    lockDisplay();
                    winCover.play();

                    bludo2.visible = false;
                    chickenBludo2.visible = true;

                    chikenAnin2 = game.add.sprite(542-mobileX,122-mobileY, 'game.chikenAnin');
                    chikenAnin2.animations.add('chikenAnin2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
                    chikenAnin2.animations.getAnimation('chikenAnin2').play();

                    setTimeout('unlockDisplay(); game.state.start("game1");',8000);
                } else {
                    lockDisplay();

                    bludo2.visible = false;
                    fishBludo2.visible = true;

                    setTimeout('unlockDisplay(); game.state.start("game1");',4000);
                }

            });

            chickenBludo1 = game.add.sprite(121-mobileX,153-mobileY, 'game.chickenBludo');
            chickenBludo2 = game.add.sprite(496-mobileX,153-mobileY, 'game.chickenBludo');
            fishBludo1 = game.add.sprite(121-mobileX,153-mobileY, 'game.fishBludo');
            fishBludo2 = game.add.sprite(496-mobileX,153-mobileY, 'game.fishBludo');
            chickenBludo1.visible = false;
            chickenBludo2.visible = false;
            fishBludo1.visible = false;
            fishBludo2.visible = false;


        };

        game4.update = function () {
        };

        game.state.add('game4', game4);

    })();


}