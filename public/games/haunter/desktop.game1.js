var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');



        // изображения

        game.add.sprite(0,0, 'game.background');
        game.add.sprite(130,86, 'game.slot_lines');
        game.add.sprite(93,23, 'game.insideBackground');
        game.add.sprite(93,23, 'game.backgroundGame1Score1');

        topBarImage = game.add.sprite(93,23, 'game.backgroundGame1Score1');

        slotPosition = [[144, 87], [144, 193], [144, 299], [254, 87], [254, 193], [254, 299], [365, 87], [365, 193], [365, 299], [477, 87], [477, 193], [477, 299], [589, 87], [589, 193], [589, 299]];
        addSlots(game, slotPosition);

        game.add.sprite(540,404, 'game.shape52');
        addTableTitle(game, 'play1To',540,432);



        var linePosition = [[134,199+50], [134,71+33], [134,322+64], [134,130+33], [134,95+34], [134,102+33], [134,228+34], [134,226+50], [134,120+34]];
        var numberPosition = [[93,183+50], [93,54+33], [93,310+64], [93,118+33], [93,246+64], [93,86+33], [93,278+64], [93,214+50], [93,150+50]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);



        // кнопки
        addButtonsGame1(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230, 26, 20], [435, 26, 20], [610, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        grammofon1 = game.add.sprite(413,404, 'game.grammofon1');
        grammofon1.animations.add('grammofon1', [01,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
        grammofon1.animations.getAnimation('grammofon1').play();

        gramofonR1 = game.add.sprite(535,404, 'game.gramofonR1');


        barmen_part11 = game.add.sprite(93,388, 'game.barmen_part11');
        barmen_part11.animations.add('barmen_part11', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part11.animations.getAnimation('barmen_part11').play();

        /*barmen_part12 = game.add.sprite(93,388, 'game.barmen_part12');
        barmen_part12.animations.add('barmen_part12', [0,1,2,3,4], 3, false);
        barmen_part12.visible = false;

        barmenR = game.add.sprite(389,388, 'game.barmenR');

        var barmen1Number = 1;
        function barmen1Anim(barmen1Number) {
            if(barmen1Number == 1){
                barmen_part11.visible = true;
                barmen_part11.animations.getAnimation('barmen_part11').play().onComplete.add(function(){
                    barmen_part11.animations.stop();
                    barmen_part11.visible = false;
                    barmen1Number = 2;
                    barmen1Anim(barmen1Number);
                });
            } else {
                barmen_part12.visible = true;
                barmen_part12.animations.getAnimation('barmen_part12').play().onComplete.add(function(){
                    barmen_part12.animations.stop();
                    barmen_part12.visible = false;
                    barmen1Number = 1;
                    barmen1Anim(barmen1Number);
                });
            }
        }*/

        //barmen1Anim(barmen1Number);



        coverRotation = game.add.sprite(93,388, 'game.coverRotation');
        coverRotation.animations.add('coverRotation', [0,1,2,3,4,5,6,7,8], 5, true);
        coverRotation.visible = false;


        full_and_sound();

    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
        /*if(bet >= extralife && checkHelm == false) {
            mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            mushroomGrow.animations.add('mushroomGrow', [0,1,2,3,4,5], 10, false);
            mushroomGrow.animations.getAnimation('mushroomGrow').play().onComplete.add(function(){
                mushroomJump = game.add.sprite(353,423, 'game.mushroomJump');
                mushroomJump.animations.add('mushroomJump', [0,1,2,3,4,5,6,7,8,9], 10, true);
                mushroomJump.animations.getAnimation('mushroomJump').play();
            });

            checkHelm = true;
        }
        
        if(bet < extralife && checkHelm == true) {
            checkHelm = false;

            mushroomGrow.visible = false;
            mushroomJump.visible = false;

            mushroomGrow = game.add.sprite(353,423, 'game.mushroomGrow');
            mushroomGrow.animations.add('mushroomGrow', [5,4,3,2,1,0], 10, false);
            mushroomGrow.animations.getAnimation('mushroomGrow').play();
        }
        
        //проверка на выпадение игры с веревками. Нужно для показа анимации
        if(checkRopeGameAnim == 1) {
            checkRopeGameAnim = 0; //делаем 0, чтобы не произошло зацикливаниие
            if(checkHelm == true) {
                function hideMonkey(){
                    monkey1.visible = false;
                    monkey2.visible = false;
                    monkey3.visible = false;
                    monkey4.visible = false;
                    monkey5.visible = false;
                }

                hideMonkey();

                monkeyTakeMushroom1 = game.add.sprite(195,359, 'game.monkeyTakeMushroom1');
                monkeyTakeMushroom1.animations.add('monkeyTakeMushroom1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16], 10, false);
                monkeyTakeMushroom1.visible = false;

                monkeyTakeMushroom2 = game.add.sprite(195,359, 'game.monkeyTakeMushroom2');
                monkeyTakeMushroom2.animations.add('monkeyTakeMushroom2', [1,2,3,4], 10, false);
                monkeyTakeMushroom2.visible = false;

                function monkeyTakeMushroomAnim() {
                    monkeyTakeMushroom1.visible = true;
                    monkeyTakeMushroom1.animations.getAnimation('monkeyTakeMushroom1').play().onComplete.add(function(){
                        monkeyTakeMushroom1.visible = false;

                        monkeyTakeMushroom2.visible = true;
                        monkeyTakeMushroom2.animations.getAnimation('monkeyTakeMushroom2').play();
                    });
                }

                monkeyTakeMushroomAnim();
            }
        }*/
    };

    game.state.add('game1', game1);

};
