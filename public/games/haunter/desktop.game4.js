function game4() {
    (function () {

        var button;

        var game4 = {};

        game4.preload = function () {

        };

        game4.create = function () {

            //звуки
            winCover = game.add.audio('winCover');


            //изображения
            //Добавление фона
            background = game.add.sprite(0,0, 'game.background');
            backgroundGame3 = game.add.sprite(94,54, 'game.backgroundGame4');
            backgroundTotal = game.add.sprite(95,23, 'game.backgroundTotal');

            //счет
            scorePosions = [[260, 26, 20], [435, 26, 20], [610, 26, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);


            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 8000;
            timeout2Game4ToGame1 = 4000;

            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    winCover.play();

                    bludo1.visible = false;
                    chickenBludo1.visible = true;

                    chikenAnin1 = game.add.sprite(167,122, 'game.chikenAnin');
                    chikenAnin1.animations.add('chikenAnin1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
                    chikenAnin1.animations.getAnimation('chikenAnin1').play();
                } else {
                    bludo1.visible = false;
                    fishBludo1.visible = true;
                }
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {
                if(ropeValues[5] != 0){
                    winCover.play();

                    bludo2.visible = false;
                    chickenBludo2.visible = true;

                    chikenAnin2 = game.add.sprite(542,122, 'game.chikenAnin');
                    chikenAnin2.animations.add('chikenAnin2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
                    chikenAnin2.animations.getAnimation('chikenAnin2').play();
                } else {
                    bludo2.visible = false;
                    fishBludo2.visible = true;
                }
            };
            

            addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1);



            //анимации
            gramofon4 = game.add.sprite(414,406, 'game.gramofon4');
            gramofon4.animations.add('gramofon4', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 4, true);
            gramofon4.animations.getAnimation('gramofon4').play();


            //gramofonR2 = game.add.sprite(530,406, 'game.gramofonR2');

            barmen_part41 = game.add.sprite(94,390, 'game.barmen_part41');
            barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
            barmen_part41.animations.getAnimation('barmen_part41').play();

            backgroundBludo = game.add.sprite(329,160, 'game.backgroundBludo');
            bludoTitle = game.add.sprite(159,310, 'game.bludoTitle');

            arrows = game.add.sprite(345,175, 'game.arrows');
            arrows.animations.add('arrows', [0,1], 3, true);
            arrows.animations.getAnimation('arrows').play();

            bludo1 = game.add.sprite(125,130, 'game.bludo');
            bludo2 = game.add.sprite(495,130, 'game.bludo');

            chickenBludo1 = game.add.sprite(121,153, 'game.chickenBludo');
            chickenBludo2 = game.add.sprite(496,153, 'game.chickenBludo');
            fishBludo1 = game.add.sprite(121,153, 'game.fishBludo');
            fishBludo2 = game.add.sprite(496,153, 'game.fishBludo');
            chickenBludo1.visible = false;
            chickenBludo2.visible = false;
            fishBludo1.visible = false;
            fishBludo2.visible = false;


            full_and_sound();
        };

        game4.update = function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
           full.loadTexture('game.non_full');
           fullStatus = false;
       }
   };

   game.state.add('game4', game4);

})();


}