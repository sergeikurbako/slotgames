function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        background = game.add.sprite(0,0, 'game.background');
        backgroundGame3 = game.add.sprite(94,54, 'game.backgroundGame3');
        backgroundTotal = game.add.sprite(95,23, 'game.backgroundTotal');

        addTableTitle(game, 'winTitleGame2', 540,432);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 84, 30]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[113+23,129], [263,129], [375,129], [490,129], [602,129]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        barmen_part41 = game.add.sprite(94,390, 'game.barmen_part41');
        barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part41.animations.getAnimation('barmen_part41').play();

        grammofon3 = game.add.sprite(414,406, 'game.grammofon3');
        grammofon3.animations.add('grammofon3', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 4, true);
        grammofon3.animations.getAnimation('grammofon3').play();

        fontsize = 20;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 547, 445, fontsize);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}