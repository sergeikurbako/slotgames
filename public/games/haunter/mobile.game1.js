var game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example', 'ld29', null, false, false);

var checkHelm = false;

var mobileX = 93;
var mobileY = 23;

function game1() {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки

        var playSound = game.add.audio('play');

        // изображения

        game.add.sprite(130-mobileX,86-mobileY, 'game.slot_lines');
        game.add.sprite(93-mobileX,23-mobileY, 'game.insideBackground');
        topBarImage = game.add.sprite(93-mobileX,23-mobileY, 'game.backgroundGame1Score1');

        slotPosition = [[144-mobileX, 87-mobileY], [144-mobileX, 193-mobileY], [144-mobileX, 299-mobileY], [254-mobileX, 87-mobileY], [254-mobileX, 193-mobileY], [254-mobileX, 299-mobileY], [365-mobileX, 87-mobileY], [365-mobileX, 193-mobileY], [365-mobileX, 299-mobileY], [477-mobileX, 87-mobileY], [477-mobileX, 193-mobileY], [477-mobileX, 299-mobileY], [589-mobileX, 87-mobileY], [589-mobileX, 193-mobileY], [589-mobileX, 299-mobileY]];
        addSlots(game, slotPosition);

        var linePosition = [[134-mobileX,199+50-mobileY], [134-mobileX,71+33-mobileY], [134-mobileX,322+64-mobileY], [134-mobileX,130+33-mobileY], [134-mobileX,95+34-mobileY], [134-mobileX,102+33-mobileY], [134-mobileX,228+34-mobileY], [134-mobileX,226+50-mobileY], [134-mobileX,120+34-mobileY]];
        var numberPosition = [[93-mobileX,183+50-mobileY], [93-mobileX,54+33-mobileY], [93-mobileX,310+64-mobileY], [93-mobileX,118+33-mobileY], [93-mobileX,246+64-mobileY], [93-mobileX,86+33-mobileY], [93-mobileX,278+64-mobileY], [93-mobileX,214+50-mobileY], [93-mobileX,150+50-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        showLines(linesArray);
        showNumbers(numberArray);

        game.add.sprite(540-mobileX,404-mobileY, 'game.shape52');
        addTableTitle(game, 'play1To',540-mobileX,432-mobileY);

        

        // кнопки
        addButtonsGame1Mobile(game);

        
        
        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230-mobileX, 26-mobileY, 20], [435-mobileX, 26-mobileY, 20], [610-mobileX, 26-mobileY, 20], [99-mobileX, 59-mobileY, 16], [707-mobileX, 59-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        grammofon1 = game.add.sprite(413-mobileX,404-mobileY, 'game.grammofon1');
        grammofon1.animations.add('grammofon1', [01,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
        grammofon1.animations.getAnimation('grammofon1').play();

        gramofonR1 = game.add.sprite(535-mobileX,404-mobileY, 'game.gramofonR1');


        barmen_part11 = game.add.sprite(93-mobileX,388-mobileY, 'game.barmen_part11');
        barmen_part11.animations.add('barmen_part11', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part11.animations.getAnimation('barmen_part11').play();

        coverRotation = game.add.sprite(93-mobileX,388-mobileY, 'game.coverRotation');
        coverRotation.animations.add('coverRotation', [0,1,2,3,4,5,6,7,8], 5, true);
        coverRotation.visible = false;

    };



    game1.update = function () {

    };

    game.state.add('game1', game1);

};
