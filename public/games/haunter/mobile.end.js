(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.image('line1', 'lines/select/1.png');
        game.load.image('line2', 'lines/select/2.png');
        game.load.image('line3', 'lines/select/3.png');
        game.load.image('line4', 'lines/select/4.png');
        game.load.image('line5', 'lines/select/5.png');
        game.load.image('line6', 'lines/select/6.png');
        game.load.image('line7', 'lines/select/7.png');
        game.load.image('line8', 'lines/select/8.png');
        game.load.image('line9', 'lines/select/9.png');

        game.load.image('linefull1', 'lines/win/1.png');
        game.load.image('linefull2', 'lines/win/2.png');
        game.load.image('linefull3', 'lines/win/3.png');
        game.load.image('linefull4', 'lines/win/4.png');
        game.load.image('linefull5', 'lines/win/5.png');
        game.load.image('linefull6', 'lines/win/6.png');
        game.load.image('linefull7', 'lines/win/7.png');
        game.load.image('linefull8', 'lines/win/8.png');
        game.load.image('linefull9', 'lines/win/9.png');

        game.load.audio('line1Sound', 'lines/sounds/line1.wav');
        game.load.audio('line3Sound', 'lines/sounds/line3.wav');
        game.load.audio('line5Sound', 'lines/sounds/line5.wav');
        game.load.audio('line7Sound', 'lines/sounds/line7.wav');
        game.load.audio('line9Sound', 'lines/sounds/line9.wav');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotateSound', 'sound/rotate.wav');
        game.load.audio('stopSound', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');
        game.load.audio('takeWin', 'sound/takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');

        game.load.audio('soundWinLine8', 'sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', 'img/shape220.png');

        game.load.image('card_39', 'img/shape222.png');
        game.load.image('card_40', 'img/shape224.png');
        game.load.image('card_41', 'img/shape226.png');
        game.load.image('card_42', 'img/shape228.png');
        game.load.image('card_43', 'img/shape230.png');
        game.load.image('card_44', 'img/shape231.png');
        game.load.image('card_45', 'img/shape232.png');
        game.load.image('card_46', 'img/shape233.png');
        game.load.image('card_47', 'img/shape234.png');
        game.load.image('card_48', 'img/shape235.png');
        game.load.image('card_49', 'img/shape236.png');
        game.load.image('card_50', 'img/shape237.png');
        game.load.image('card_51', 'img/shape238.png');

        game.load.image('card_26', 'img/shape239.png');
        game.load.image('card_27', 'img/shape240.png');
        game.load.image('card_28', 'img/shape241.png');
        game.load.image('card_29', 'img/shape242.png');
        game.load.image('card_30', 'img/shape243.png');
        game.load.image('card_31', 'img/shape244.png');
        game.load.image('card_32', 'img/shape245.png');
        game.load.image('card_33', 'img/shape246.png');
        game.load.image('card_34', 'img/shape247.png');
        game.load.image('card_35', 'img/shape248.png');
        game.load.image('card_36', 'img/shape249.png');
        game.load.image('card_37', 'img/shape250.png');
        game.load.image('card_38', 'img/shape251.png');

        game.load.image('card_0', 'img/shape252.png');
        game.load.image('card_1', 'img/shape253.png');
        game.load.image('card_2', 'img/shape254.png');
        game.load.image('card_3', 'img/shape255.png');
        game.load.image('card_4', 'img/shape256.png');
        game.load.image('card_5', 'img/shape257.png');
        game.load.image('card_6', 'img/shape258.png');
        game.load.image('card_7', 'img/shape259.png');
        game.load.image('card_8', 'img/shape260.png');
        game.load.image('card_9', 'img/shape261.png');
        game.load.image('card_10', 'img/shape262.png');
        game.load.image('card_11', 'img/shape263.png');
        game.load.image('card_12', 'img/shape264.png');

        game.load.image('card_13', 'img/shape265.png');
        game.load.image('card_14', 'img/shape266.png');
        game.load.image('card_15', 'img/shape267.png');
        game.load.image('card_16', 'img/shape268.png');
        game.load.image('card_17', 'img/shape269.png');
        game.load.image('card_18', 'img/shape270.png');
        game.load.image('card_19', 'img/shape271.png');
        game.load.image('card_20', 'img/shape265.png');
        game.load.image('card_21', 'img/shape266.png');
        game.load.image('card_22', 'img/shape267.png');
        game.load.image('card_23', 'img/shape268.png');
        game.load.image('card_24', 'img/shape269.png');
        game.load.image('card_25', 'img/shape270.png');
        game.load.image('card_52', 'img/shape271.png');

        game.load.image('pick', 'img/shape340.png');

        game.load.image('cell0', 'img/cell0.png');
        game.load.image('cell1', 'img/cell1.png');
        game.load.image('cell2', 'img/cell2.png');
        game.load.image('cell3', 'img/cell3.png');
        game.load.image('cell4', 'img/cell4.png');
        game.load.image('cell5', 'img/cell5.png');
        game.load.image('cell6', 'img/cell6.png');
        game.load.image('cell7', 'img/cell7.png');
        game.load.image('cell8', 'img/cell8.png');

        game.load.spritesheet('cellAnim', 'img/cellAnim.jpg', 96, 112);



        /* haunter sources */

        game.load.image('game.background4', 'img/slot_bg.png');
        game.load.image('game.background5', 'img/slot_bg.png');
        game.load.image('game.insideBackground', 'img/background1.png');
        game.load.image('game.slot_lines', 'img/slot_lines.png');

        game.load.spritesheet('game.grammofon1', 'img/grammofon1.png', 128, 96);
        game.load.spritesheet('game.barmen_part11', 'img/barmen_part11.png', 320, 112);
        game.load.spritesheet('game.barmen_part12', 'img/barmen_part12.png', 320, 112);
        game.load.spritesheet('game.barmen_part41', 'img/barmen_part41.png', 320, 112);
        game.load.spritesheet('game.barmen_part42', 'img/barmen_part42.png', 320, 112);
        game.load.spritesheet('game.barmenWin1', 'img/barmenWin1.png', 320, 112);
        game.load.spritesheet('game.barmenWin2', 'img/barmenWin2.png', 320, 112);
        game.load.spritesheet('game.coverRotation', 'img/coverRotation.png', 96, 112);

        game.load.audio('winCover', 'sound/winCover.mp3');

        game.load.image('game.backgroundGame4', 'img/shape489.png');
        game.load.image('game.backgroundGame1Score1', 'img/backgroundGame1Score1.png');
        game.load.image('game.shape52', 'img/shape52.png');
        game.load.image('game.backgroundTotal', 'img/shape14.svg');
        game.load.image('game.totalBet', 'img/totalBetX5.png');
        game.load.image('game.totalExit', 'img/shape510.svg');

        game.load.spritesheet('game.openCaver', 'img/openCaver.png', 112, 128);
        game.load.spritesheet('game.gramofon4', 'img/gramofon4.png', 128, 96);

        game.load.audio('game.preOpenWinCover', 'sound/preOpenWinCover.mp3');
        game.load.audio('game.openWinCover', 'sound/openWinCover.mp3');

        game.load.image('game.backgroundGame3', 'img/792.png');
        game.load.spritesheet('game.grammofon3', 'img/grammofon3.png', 128, 96);

        game.load.image('game.backgroundGame2', 'img/793.png');
        game.load.spritesheet('game.arrows', 'img/arrows.png', 144, 80);
        game.load.image('game.backgroundBludo', 'img/shape523.png');
        game.load.image('game.bludoTitle', 'img/shape521.png');
        game.load.image('game.bludo', 'img/shape659.png');
        game.load.image('game.chickenBludo', 'img/shape532.png');
        game.load.image('game.fishBludo', 'img/shape656.png');
        game.load.spritesheet('game.chikenAnin', 'img/chikenAnin.png', 128, 96);

        game.load.image('game.barmenR', 'img/barmenR.png');
        game.load.image('game.barmenR2', 'img/barmenR2.png');
        //game.load.image('game.barmenR3', 'img/barmenR3.png');

        game.load.image('game.gramofonR1', 'img/gramofonR1.png');
        game.load.image('game.gramofonR2', 'img/gramofonR2.png');
        game.load.image('game.gramofonR3', 'img/gramofonR3.png');

        game.load.image('play1To', 'img/image546.png');
        game.load.image('bonusGame', 'img/bonusGame.png');
        game.load.image('takeOrRisk1', 'img/image474.jpg');
        game.load.image('take', 'img/image554.jpg');

        game.load.image('winTitleGame2', 'img/image486.png');
        game.load.image('loseTitleGame2', 'img/image484.png');
        game.load.image('forwadTitleGame2', 'img/image504.png');

        game.load.image('topScoreGame2', 'img/image457.png');

        game.load.audio('winMonkey', 'sound/winMonkey.mp3');

        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/coverRotation.png', 96, 112); //текущее кол-во кадров = 11

        game.load.audio('openCard', 'sound/sound31.mp3');
        game.load.audio('winCard', 'sound/sound30.mp3');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

