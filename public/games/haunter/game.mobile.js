function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(arr) {
    return arr.sort(function() {return 0.5 - Math.random()});
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

isMobile = true;

var width = 829.7;
var height = 598.95;

var mobileX = 93;
var mobileY = 23;

if(isMobile) {
    width = 640;
    height = 480;
}
window.game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example');

function createLevelButtons() {
    var lvl1 = game.add.sprite(0,30, 'game.non_full');
    lvl1.inputEnabled = true;
    lvl1.input.useHandCursor = true;
    lvl1.events.onInputUp.add(function () {
        game.state.start('game1');
    }, this);

    var lvl2 = game.add.sprite(20, 30, 'game.non_full');
    lvl2.inputEnabled = true;
    lvl2.input.useHandCursor = true;
    lvl2.events.onInputUp.add(function () {
        game.state.start('game2');
    }, this);

    var lvl3 = game.add.sprite(40, 30, 'game.non_full');
    lvl3.inputEnabled = true;
    lvl3.input.useHandCursor = true;
    lvl3.events.onInputUp.add(function () {
        game.state.start('game3');

    }, this);

    var lvl4 = game.add.sprite(60, 30, 'game.non_full');
    lvl4.inputEnabled = true;
    lvl4.input.useHandCursor = true;
    lvl4.events.onInputUp.add(function () {
        game.state.start('game4');

    }, this);
}

function hideNumbers() {
    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;
}

function showNumbers(n) {

    if(n == 1){
        number1.visible = true;
    } 

    if(n == 3) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
    } 

    if(n == 5) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
    }

    if(n == 7) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
    }

    if(n == 9) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    }
}
//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

(function () {

    var bars = [];
    var rotateSound;
    var stopSound;
    var tadaSound;
    var spinning = false;
    var barsCurrentSpins = [0, 0, 0, 0, 0];
    var barsTotalSpins = [];
    var spinningBars = 0;
    var button;
    var currentLine = 5;

    var lines = {
        1: {
            coord: 239-mobileY,
            sprite: null,
            btncoord: 250,
            button: null
        },
        2: {
            coord: 96-mobileY,
            sprite: null
        },
        3: {
            coord: 382-mobileY,
            sprite: null,
            btncoord: 295,
            button: null
        },
        4: {
            coord: 150-mobileY,
            sprite: null
        },
        5: {
            coord: 132-mobileY,
            sprite: null,
            btncoord: 340,
            button: null
        },
        6: {
            coord: 123-mobileY,
            sprite: null
        },
        7: {
            coord: 361 - 103-mobileY,
            sprite: null,
            btncoord: 385,
            button: null
        },
        8: {
            coord: 263-mobileY,
            sprite: null
        },
        9: {
            coord: 218 - 71-mobileY,
            sprite: null,
            btncoord: 430,
            button: null
        }
    };

    var tmpSpins = 15;
    for (var i = 0; i < 5; ++i) {
        barsTotalSpins[i] = tmpSpins;
        tmpSpins += 12;
    }

    var game1 = {};


    function hideLines() {
        console.log(lines);
        for (var i in lines) {
            lines[i].sprite.visible = false;
        }
    }

    function selectLine(n) {
        currentLine = n;

        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.visible = true;
            lines[i].sprite.loadTexture('line_' + i);
        }
    }

    function preselectLine(n) {
        for (var i = 1; i <= lines.count; ++i) {
            lines[i].sprite.visible = false;
        }
        for (var i = 1; i <= n; ++i) {
            lines[i].sprite.loadTexture('linefull_' + i);
            lines[i].sprite.visible = true;
        }
    }

    var game1 = {};


    game1.preload = function () {

    };

    game1.create = function () {
		
		var playSound = game.add.audio('play');
        rotateSound = game.add.audio('rotate');
        rotateSound.loop = true;
        stopSound = game.add.audio('stop');
        tadaSound = game.add.audio('tada');
        takeWin = game.add.audio('takeWin');
        takeWin.addMarker('take', 0, 0.6);
        winCover = game.add.audio('winCover');
        
        game.add.sprite(130-mobileX,86-mobileY, 'game.slot_lines');
        game.add.sprite(0,0, 'game.insideBackground');
        
        var positions = [
            game.world.centerX - 222,
            game.world.centerX - 110,
            game.world.centerX + 1,
            game.world.centerX + 112,
            game.world.centerX + 224
        ];

        for (var i = 0; i < 5; ++i) {
            bars[i] = game.add.tileSprite(positions[i], game.world.centerY - 39 + mobileY, 96, 320, 'game.bar');
            bars[i].anchor.setTo(0.5, 0.5);
            bars[i].tilePosition.y = randomNumber(0, 8) * 112 - 8;
        }

        window.test = function () {
            bars[0].tilePosition.y -= 1;
        };

        function randomiseSpin() {
            return [
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106,
                randomNumber(1, 3) * 106
            ];
        }

        line1x = game.add.sprite(250,510, 'game.line1_d');
        line1x.scale.setTo(0.7, 0.7);
        line3x = game.add.sprite(295,510, 'game.line3_d');
        line3x.scale.setTo(0.7, 0.7);
        line5x = game.add.sprite(340,510, 'game.line5_d');
        line5x.scale.setTo(0.7, 0.7);
        line7x = game.add.sprite(385,510, 'game.line7_d');
        line7x.scale.setTo(0.7, 0.7);
        line9x = game.add.sprite(430,510, 'game.line9_d');
        line9x.scale.setTo(0.7, 0.7);
        selectGamex = game.add.sprite(70,510, 'game.selectGame_d');
        selectGamex.scale.setTo(0.7, 0.7);
        payTablex = game.add.sprite(150,510, 'game.payTable_d');
        payTablex.scale.setTo(0.7, 0.7);
        betonex = game.add.sprite(490,510, 'game.betone_d');
        betonex.scale.setTo(0.7, 0.7);
        betmaxx = game.add.sprite(535,510, 'game.betmax_d');
        betmaxx.scale.setTo(0.7, 0.7);
        automaricstartx = game.add.sprite(685,510, 'game.automaricstart_d');
        automaricstartx.scale.setTo(0.7, 0.7);

        line1x.visible = false;
        line3x.visible = false;
        line5x.visible = false;
        line7x.visible = false;
        line9x.visible = false;
        selectGamex.visible = false;
        payTablex.visible = false;
        betonex.visible = false;
        betmaxx.visible = false;
        automaricstartx.visible = false;

        game.check_win = 0;

        function buttonClicked() {
            
            if (spinning) {
                return;
            }

            if(game.check_win == 0) {
                lines[1].button.visible = false;
                lines[3].button.visible = false;
                lines[5].button.visible = false;
                lines[7].button.visible = false;
                lines[9].button.visible = false;

                line1x.visible = true;
                line3x.visible = true;
                line5x.visible = true;
                line7x.visible = true;
                line9x.visible = true;
                selectGamex.visible = true;
                payTablex.visible = true;
                betonex.visible = true;
                betmaxx.visible = true;
                automaricstartx.visible = true;
                button.loadTexture('game.start_d');
            }
            
        }

        function buttonRelease() {
            if(game.check_win == 1) {
                hideLines();
                selectLine(3);
                takeWin.play('take');
                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                lines[1].button.visible = true;
                lines[3].button.visible = true;
                lines[5].button.visible = true;
                lines[7].button.visible = true;
                lines[9].button.visible = true;

                line1x.visible = false;
                line3x.visible = false;
                line5x.visible = false;
                line7x.visible = false;
                line9x.visible = false;
                selectGamex.visible = false;
                payTablex.visible = false;
                betonex.visible = false;
                betmaxx.visible = false;
                automaricstartx.visible = false;
                button.loadTexture('game.start');

                game.check_win = 0;

                flashNamber1.animations.stop();
                flashNamber2.animations.stop();
                flashNamber3.animations.stop();
            } else {
                if (spinning) {
                    return;
                }
                hideLines();
                barsCurrentSpins = [0, 0, 0, 0, 0];
                spinningBars = bars.length;
                spinning = true;
                playSound.play();
            }
        }


        for (var i = 1; i <= 9; ++i) {
            lines[i].sprite = game.add.sprite(134-mobileX, lines[i].coord, 'line_' + i);
            lines[i].sprite.visible = false;
            if (i % 2 != 0) {
                lines[i].sound = game.add.audio('line' + i);
                lines[i].button = game.add.sprite(lines[i].btncoord, 510, 'btnline' + i);
                lines[i].button.scale.setTo(0.7,0.7);
                lines[i].button.inputEnabled = true;
                lines[i].button.input.useHandCursor = true;
                (function (n) {
                    lines[n].button.events.onInputDown.add(function () {

                        hideLines();
                        preselectLine(n);

                        hideNumbers();
                        showNumbers(n);

                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                    lines[n].button.events.onInputUp.add(function () {
                        hideLines();
                        selectLine(n);
                        lines[n].button.loadTexture('btnline' + n);
                        lines[n].sound.play();
                    }, this);
                    lines[n].button.events.onInputOut.add(function () {
                        lines[n].button.loadTexture('btnline' + n);
                    }, this);
                    lines[n].button.events.onInputOver.add(function () {
                        lines[n].button.loadTexture('btnline_p' + n);
                    }, this);
                })(i);
            }
        }

        number1 = game.add.sprite(93-mobileX,231-mobileY, 'game.number1');
        number2 = game.add.sprite(93-mobileX,87-mobileY, 'game.number2');
        number3 = game.add.sprite(93-mobileX,375-mobileY, 'game.number3');
        number4 = game.add.sprite(93-mobileX,151-mobileY, 'game.number4');
        number5 = game.add.sprite(93-mobileX,311-mobileY, 'game.number5');
        number6 = game.add.sprite(93-mobileX,119-mobileY, 'game.number6');
        number7 = game.add.sprite(93-mobileX,343-mobileY, 'game.number7');
        number8 = game.add.sprite(93-mobileX,263-mobileY, 'game.number8');
        number9 = game.add.sprite(93-mobileX,198-mobileY, 'game.number9');

        preselectLine(9);

        //создание анимаций
        grammofon1 = game.add.sprite(413-mobileX,404-mobileY, 'game.grammofon1');
        grammofon1.animations.add('grammofon1', [01,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
        grammofon1.animations.getAnimation('grammofon1').play();

        barmen_part11 = game.add.sprite(93-mobileX,388-mobileY, 'game.barmen_part11');
        barmen_part11.animations.add('barmen_part11', [0,1,2,3,4,5,6,7,8,9,10,11], 3, false);
        barmen_part11.visible = false;

        barmen_part12 = game.add.sprite(93-mobileX,388-mobileY, 'game.barmen_part12');
        barmen_part12.animations.add('barmen_part12', [0,1,2,3,4], 3, false);
        barmen_part12.visible = false;

        var barmen1Number = 1;
        function barmen1Anim(barmen1Number) {
            if(barmen1Number == 1){
                barmen_part11.visible = true;
                barmen_part11.animations.getAnimation('barmen_part11').play().onComplete.add(function(){
                    barmen_part11.visible = false;
                    barmen1Number = 2;
                    barmen1Anim(barmen1Number);
                });
            } else {
                barmen_part12.visible = true;
                barmen_part12.animations.getAnimation('barmen_part12').play().onComplete.add(function(){
                    barmen_part12.visible = false;
                    barmen1Number = 1;
                    barmen1Anim(barmen1Number);
                });
            }
        }

        barmen1Anim(barmen1Number);

        coverRotation = game.add.sprite(93-mobileX,388-mobileY, 'game.coverRotation');
        coverRotation.animations.add('coverRotation', [0,1,2,3,4,5,6,7,8], 5, true);
        coverRotation.visible = false;

        shape52 = game.add.sprite(540-mobileX,404-mobileY, 'game.shape52');


        button = game.add.sprite(588, 228, 'game.start');

        //кнопки
        double = game.add.sprite(550, 133, 'game.double');
        double.inputEnabled = true;
        double.input.useHandCursor = true;
        double.events.onInputDown.add(function(){
            double.loadTexture('game.double');
        });
        double.events.onInputUp.add(function(){
            double.loadTexture('game.double');
        });

        bet1 = game.add.sprite(546, 274, 'game.bet1');
        bet1.inputEnabled = true;
        bet1.input.useHandCursor = true;
        bet1.events.onInputDown.add(function(){
            bet1.loadTexture('game.bet1_p');
            document.getElementById('betMode').style.display = 'block';
            widthVisibleZone = $('.betWrapper .visibleZone').height();
            console.log(widthVisibleZone);
            $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
            $('canvas').css('display', 'none');
        });
        bet1.events.onInputUp.add(function(){
            bet1.loadTexture('bet1');
        });
        bet1.events.onInputOut.add(function(){
            bet1.loadTexture('game.bet1');
        });

        var dollar = game.add.sprite(435, 3, 'dollar');
        dollar.inputEnabled = true;
        dollar.input.useHandCursor = true;
        dollar.events.onInputDown.add(function(){
            //game.state.start('game4');
        });

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });

        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        button.anchor.setTo(0.5, 0.5);
        button.inputEnabled = true;
        button.input.useHandCursor = true;
        button.events.onInputDown.add(buttonClicked, this);
        button.events.onInputUp.add(buttonRelease, this);

        bonusGame = game.add.sprite(540-mobileX,432-mobileY, 'game.bonusGame');
       	
        createLevelButtons();

    };

    game1.update = function () {
        mobileY = 23;
        mobileX = 95;
        if (spinning) {
            for (var i in bars) {
                barsCurrentSpins[i]++;
                if (barsCurrentSpins[i] < barsTotalSpins[i]) {
                    bars[i].tilePosition.y += 112;
                } else if (barsCurrentSpins[i] == barsTotalSpins[i]) {
                    spinningBars--;
                }
            }
            if (!spinningBars) {
                spinning = false;
                rotateSound.stop();
                button.loadTexture('game.start');
                selectLine(currentLine);
                console.log('spin end');
                if (currentLine == 3) {
                    preselectLine(3);
                    tadaSound.play();

                    hideNumbers();

                    barmenWin.visible = true;
                    barmenWin.animations.getAnimation('barmenWin').play();

                    barmenWin1 = game.add.sprite(0,388-mobileY, 'game.barmenWin1');
                    barmenWin1.animations.add('barmenWin1', [0,1,2,3,4,5,6,7,8,9,10,11], 10, false);
                    barmenWin1.visible = false;

                    barmenWin2 = game.add.sprite(0,388-mobileY, 'game.barmenWin2');
                    barmenWin2.animations.add('barmenWin2', [0,1,2,3], 10, false);
                    barmenWin2.visible = false;

                    var barmenWinNumber = 1;
                    function barmenWinAnim(barmenWinNumber) {
                        if(barmenWinNumber == 1){
                            barmenWin1.visible = true;
                            barmenWin1.animations.getAnimation('barmenWin1').play().onComplete.add(function(){
                                barmenWin1.visible = false;
                                barmenWinNumber = 2;
                                barmenWinAnim(barmenWinNumber);
                            });
                        } else {
                            barmenWin2.visible = true;
                            barmenWin2.animations.getAnimation('barmenWin2').play().onComplete.add(function(){
                                barmenWin2.visible = false;
                                barmenWinNumber = 1;
                                barmenWinAnim(barmenWinNumber);
                            });
                        }
                    }

                    barmenWinAnim(barmenWinNumber);

                    game.check_win = 1;

                    flashNamber1 = game.add.sprite(93-mobileX,231-mobileY, 'game.flashNamber1');
                    flashNamber1.animations.add('flashNamber1', [0,1], 1, true);
                    flashNamber1.animations.getAnimation('flashNamber1').play();

                    flashNamber2 = game.add.sprite(93-mobileX,87-mobileY, 'game.flashNamber2');
                    flashNamber2.animations.add('flashNamber2', [0,1], 1, true);
                    flashNamber2.animations.getAnimation('flashNamber2').play();

                    flashNamber3 = game.add.sprite(93-mobileX,375-mobileY, 'game.flashNamber3');
                    flashNamber3.animations.add('flashNamber3', [0,1], 1, true);
                    flashNamber3.animations.getAnimation('flashNamber3').play();

                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                } else if (currentLine == 5) {
                    winCover.play();
                    game.check_win = 0;
                    hideLines();
                    lockDisplay();
                    var coverRotation1 = game.add.sprite(255-mobileX,190-mobileY, 'game.coverRotation');
                    coverRotation1.animations.add('coverRotation1', [0,1,2,3,4,5,6,7,8], 5, true);
                    coverRotation1.visible = true;
                    coverRotation1.animations.getAnimation('coverRotation1').play().onComplete.add(function(){
                        
                    });
                    var coverRotation2 = game.add.sprite(366-mobileX,190-mobileY, 'game.coverRotation');
                    coverRotation2.animations.add('coverRotation2', [0,1,2,3,4,5,6,7,8], 5, true);
                    coverRotation2.visible = true;
                    coverRotation2.animations.getAnimation('coverRotation2').play().onComplete.add(function(){
                        
                    });
                    var coverRotation3 = game.add.sprite(477-mobileX,190-mobileY, 'game.coverRotation');
                    coverRotation3.animations.add('coverRotation3', [0,1,2,3,4,5,6,7,8], 5, true);
                    coverRotation3.visible = true;
                    coverRotation3.animations.getAnimation('coverRotation3').play().onComplete.add(function(){
                        
                        //
                    });

                    setTimeout('unlockDisplay(); game.state.start("game4");',8000);
                } else {
                    lines[1].button.visible = true;
                    lines[3].button.visible = true;
                    lines[5].button.visible = true;
                    lines[7].button.visible = true;
                    lines[9].button.visible = true;

                    line1x.visible = false;
                    line3x.visible = false;
                    line5x.visible = false;
                    line7x.visible = false;
                    line9x.visible = false;
                    selectGamex.visible = false;
                    payTablex.visible = false;
                    betonex.visible = false;
                    betmaxx.visible = false;
                    automaricstartx.visible = false;
                }
            }
        }
    };

    game.state.add('game1', game1);


})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================

(function () {

var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
    	var mobileX = 95;
    	var mobileY = 23;

        //Добавление фона
        backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame2');
        backgroundTotal = game.add.sprite(0,0, 'game.backgroundTotal');

        winCover = game.add.audio('winCover');

        //добавление кнопок

        

        grammofon3 = game.add.sprite(414-mobileX,406-mobileY, 'game.grammofon3');
        grammofon3.animations.add('grammofon3', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 4, true);
        grammofon3.animations.getAnimation('grammofon3').play();

        barmen_part41 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part41');
        barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part41.visible = false;

        barmen_part42 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part42');
        barmen_part42.animations.add('barmen_part42', [0,1,2,3,4,5,6,7], 3, true);
        barmen_part42.visible = false;

        var barmen4Number = 1;
        function barmen4Anim(barmen1Number) {
            if(barmen4Number == 1){
                barmen_part41.visible = true;
                barmen_part41.animations.getAnimation('barmen_part41').play().onComplete.add(function(){
                    barmen_part41.visible = false;
                    barmen4Number = 2;
                    barmen4Anim(barmen4Number);
                });
            } else {
                barmen_part42.visible = true;
                barmen_part42.animations.getAnimation('barmen_part42').play().onComplete.add(function(){
                    barmen_part42.visible = false;
                    barmen4Number = 1;
                    barmen4Anim(barmen4Number);
                });
            }
        }

        barmen4Anim(barmen4Number);


        backgroundBludo = game.add.sprite(329-mobileX,160-mobileY, 'game.backgroundBludo');
        bludoTitle = game.add.sprite(159-mobileX,310-mobileY, 'game.bludoTitle');

        arrows = game.add.sprite(345-mobileX,175-mobileY, 'game.arrows');
        arrows.animations.add('arrows', [0,1], 3, true);
        arrows.animations.getAnimation('arrows').play();        

        bludo1 = game.add.sprite(125-mobileX,130-mobileY, 'game.bludo');
        bludo1.inputEnabled = true;
        bludo1.input.useHandCursor = true;
        bludo1.events.onInputDown.add(function(){
            lockDisplay();
            winCover.play();

            bludo1.visible = false;
            chickenBludo.visible = true;
            chikenAnin.visible = true;
            chikenAnin.animations.getAnimation('chikenAnin').play();

            setTimeout('unlockDisplay(); game.state.start("game1");',8000);
        });

        bludo2 = game.add.sprite(495-mobileX,130-mobileY, 'game.bludo');
        bludo2.inputEnabled = true;
        bludo2.input.useHandCursor = true;
        bludo2.events.onInputDown.add(function(){
            lockDisplay();

            bludo2.visible = false;
            fishBludo2.visible = true;

            setTimeout('unlockDisplay(); game.state.start("game1");',2000);
        });

        chickenBludo = game.add.sprite(121-mobileX,153-mobileY, 'game.chickenBludo');
        fishBludo1 = game.add.sprite(121-mobileX,153-mobileY, 'game.fishBludo');
        fishBludo2 = game.add.sprite(496-mobileX,153-mobileY, 'game.fishBludo');
        chickenBludo.visible = false;
        fishBludo1.visible = false;
        fishBludo2.visible = false;

        chikenAnin = game.add.sprite(167-mobileX,122-mobileY, 'game.chikenAnin');
        chikenAnin.animations.add('chikenAnin', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 5, true);
        chikenAnin.visible = false;
        
        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        bonusGame = game.add.sprite(540-mobileX,432-mobileY, 'game.bonusGame');
        
        createLevelButtons();
    };

    game2.update = function () {
        
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================

function randomCard() {
    var arr = ['2b','3b','4b','5b','6b','7b','8b','9b','10b','jb','qb','kb','ab','2ch','3ch','4ch','5ch','6ch','7ch','8ch','9ch','10ch','jch','qch','kch','ach','2k','3k','4k','5k','6k','7k','8k','9k',
        '10k','jk','qk','kk','ak','2p','3p','4p','5p','6p','7p','8p','9p','10p','jp','qp'];

    var rand = Math.floor(Math.random() * arr.length);

    return 'game.card_'+arr[rand];
}

function hidePick() {
    pick2.visible = false;
    pick3.visible = false;
    pick4.visible = false;
    pick5.visible = false;
}

function openCardSound() {
    openCardAudio.play();
}


(function () {

    var button;

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {
    	var mobileX = 95;
    	var mobileY = 23;

        background = game.add.sprite(0,0, 'game.background');
        backgroundGame3 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundGame3');
        backgroundTotal = game.add.sprite(95-mobileX,23-mobileY, 'game.backgroundTotal');
        

        function humanThought_forward() {
            humanThought = game.add.sprite(325-mobileX,305-mobileY, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,2,2,2], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_loss() {
            humanThought = game.add.sprite(325-mobileX,305-mobileY, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,1,1,1], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }
        function humanThought_win() {
            humanThought = game.add.sprite(325-mobileX,305-mobileY, 'game.humanThought');
            humanThought.animations.add('humanThought', [4,3,0,0,0], 3, false);
            humanThought.animations.getAnimation('humanThought').play(null,null,true,true);
        }

        

        openCardAudio = game.add.audio("game.openCardAudio");
        winCards = game.add.audio("game.winCards");

        lockDisplay();
        setTimeout("unlockDisplay()",500);
        setTimeout('openCardSound(); card1 = game.add.sprite(135-mobileX,133-mobileY, randomCard());',500);

        card1 = game.add.sprite(134-mobileX,133-mobileY, 'game.card_garage');
        card2 = game.add.sprite(263-mobileX,133-mobileY, 'game.card_garage');
        card2.inputEnabled = true;
        card2.input.useHandCursor = true;
        card2.events.onInputDown.add(function(){
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            humanThought_win();

            card2.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            
            setTimeout('openCardSound(); card3.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick5.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick();',
                3000);
        });

        card3 = game.add.sprite(373-mobileX,133-mobileY, 'game.card_garage');
        card3.inputEnabled = true;
        card3.input.useHandCursor = true;
        card3.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_win();
            setTimeout('unlockDisplay();',3000);
            card3.loadTexture(randomCard());
            openCardSound();
            winCards.play(); 
            setTimeout('card2.loadTexture(randomCard()); card4.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick4.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage"); hidePick();',
                3000);
        });

        card4 = game.add.sprite(483-mobileX,133-mobileY, 'game.card_garage');
        card4.inputEnabled = true;
        card4.input.useHandCursor = true;
        card4.events.onInputDown.add(function(){
            humanThought_win();
            lockDisplay();
            setTimeout('unlockDisplay();',3000);
            card4.loadTexture(randomCard());
            openCardSound();
            winCards.play();
            setTimeout('card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card5.loadTexture(randomCard());',1000);
            pick3.visible = true;
            setTimeout('card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage"); ' +
                'card4.loadTexture("game.card_garage"); ' +
                'card5.loadTexture("game.card_garage");hidePick();',
                3000);
        });

        card5 = game.add.sprite(593-mobileX,133-mobileY, 'game.card_garage');
        card5.inputEnabled = true;
        card5.input.useHandCursor = true;
        card5.events.onInputDown.add(function(){
            lockDisplay();
            humanThought_loss();
            setTimeout('unlockDisplay();',3000);
            openCardSound();
            card5.loadTexture(randomCard());
            pick2.visible = true;
            setTimeout('openCardSound(); card2.loadTexture(randomCard()); card3.loadTexture(randomCard()); card4.loadTexture(randomCard());',1000);

            setTimeout('game.state.start("game1"); card2.loadTexture("game.card_garage"); card1.loadTexture(randomCard());' +
                'card3.loadTexture("game.card_garage");' +
                'card4.loadTexture("game.card_garage");' +
                'card5.loadTexture("game.card_garage");hidePick();',
                3000);
        });

        pick2 = game.add.sprite(605-mobileX,300-mobileY, 'game.pick');
        pick3 = game.add.sprite(495-mobileX,300-mobileY, 'game.pick');
        pick4 = game.add.sprite(385-mobileX,300-mobileY, 'game.pick');
        pick5 = game.add.sprite(275-mobileX,300-mobileY, 'game.pick');
        pick2.visible = false;
        pick3.visible = false;
        pick4.visible = false;
        pick5.visible = false;

        grammofon3 = game.add.sprite(414-mobileX,406-mobileY, 'game.grammofon3');
        grammofon3.animations.add('grammofon3', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 4, true);
        grammofon3.animations.getAnimation('grammofon3').play();

        barmen_part41 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part41');
        barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part41.visible = false;

        barmen_part42 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part42');
        barmen_part42.animations.add('barmen_part42', [0,1,2,3,4,5,6,7], 3, true);
        barmen_part42.visible = false;

        var barmen4Number = 1;
        function barmen4Anim(barmen1Number) {
            if(barmen4Number == 1){
                barmen_part41.visible = true;
                barmen_part41.animations.getAnimation('barmen_part41').play().onComplete.add(function(){
                    barmen_part41.visible = false;
                    barmen4Number = 2;
                    barmen4Anim(barmen4Number);
                });
            } else {
                barmen_part42.visible = true;
                barmen_part42.animations.getAnimation('barmen_part42').play().onComplete.add(function(){
                    barmen_part42.visible = false;
                    barmen4Number = 1;
                    barmen4Anim(barmen4Number);
                });
            }
        }

        barmen4Anim(barmen4Number);

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });

        bonusGame = game.add.sprite(540-mobileX,432-mobileY, 'game.bonusGame');
        
        createLevelButtons();
    };

    game3.update = function () {

    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================

(function () {

    var button;

    var game4 = {};

    game4.preload = function () {

    };

    game4.create = function () {

    	var mobileX = 95;
    	var mobileY = 23;

        preOpenWinCover = game.add.sound('game.preOpenWinCover');
        openWinCover = game.add.sound('game.openWinCover');

        //Добавление фона
        background = game.add.sprite(0,0, 'game.background');
        background2 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame4');
        backgroundTotal = game.add.sprite(95-mobileX,23-mobileY, 'game.backgroundTotal');

        

        gramofon4 = game.add.sprite(415-mobileX,406-mobileY, 'game.gramofon4');
        gramofon4.animations.add('gramofon4', [01,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 4, true);
        gramofon4.animations.getAnimation('gramofon4').play();

        barmen_part41 = game.add.sprite(95-mobileX,390-mobileY, 'game.barmen_part41');
        barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
        barmen_part41.visible = false;

        barmen_part42 = game.add.sprite(95-mobileX,390-mobileY, 'game.barmen_part42');
        barmen_part42.animations.add('barmen_part42', [0,1,2,3,4,5,6,7], 3, true);
        barmen_part42.visible = false;

        var barmen4Number = 1;
        function barmen4Anim(barmen1Number) {
            if(barmen4Number == 1){
                barmen_part41.visible = true;
                barmen_part41.animations.getAnimation('barmen_part41').play().onComplete.add(function(){
                    barmen_part41.visible = false;
                    barmen4Number = 2;
                    barmen4Anim(barmen4Number);
                });
            } else {
                barmen_part42.visible = true;
                barmen_part42.animations.getAnimation('barmen_part42').play().onComplete.add(function(){
                    barmen_part42.visible = false;
                    barmen4Number = 1;
                    barmen4Anim(barmen4Number);
                });
            }
        }

        barmen4Anim(barmen4Number);

        openCaver1 = game.add.sprite(145-mobileX,148-mobileY, 'game.openCaver');
        openCaver1.animations.add('openCaver1', [0,1,2,3,4,5,6], 30, false);
        openCaver1.inputEnabled = true;
        openCaver1.input.useHandCursor = true;
        openCaver1.visible = true;
        openCaver1.events.onInputDown.add(function(){
            openBlueCaver1();
        });

        openCaver2 = game.add.sprite(255-mobileX,148-mobileY, 'game.openCaver');
        openCaver2.animations.add('openCaver2', [0,1,2,3,4,5,6], 30, false);
        openCaver2.visible = true;
        openCaver2.inputEnabled = true;
        openCaver2.input.useHandCursor = true;
        openCaver2.events.onInputDown.add(function(){
            openBlueCaver2();
        });

        openCaver3 = game.add.sprite(365-mobileX,148-mobileY, 'game.openCaver');
        openCaver3.animations.add('openCaver3', [0,1,2,3,4,5,6], 30, false);
        openCaver3.visible = true;
        openCaver3.inputEnabled = true;
        openCaver3.input.useHandCursor = true;
        openCaver3.events.onInputDown.add(function(){
            openBlueCaver3();
        });

        openCaver4 = game.add.sprite(475-mobileX,148-mobileY, 'game.openCaver');
        openCaver4.animations.add('openCaver4', [0,1,2,3,4,5,6], 30, false);
        openCaver4.visible = true;
        openCaver4.inputEnabled = true;
        openCaver4.input.useHandCursor = true;
        openCaver4.events.onInputDown.add(function(){
            openBlueCaver4();
        });

        openCaver5 = game.add.sprite(585-mobileX,148-mobileY, 'game.openCaver');
        openCaver5.animations.add('openCaver5', [0,1,2,3,4,5,6], 30, false);
        openCaver5.visible = true;
        openCaver5.inputEnabled = true;
        openCaver5.input.useHandCursor = true;
        openCaver5.events.onInputDown.add(function(){
            openBlueCaver5();
        });

        function openBlueCaver1() {
            preOpenWinCover.play();
            
            openCaver1.animations.getAnimation('openCaver1').play().onComplete.add(function () {
                openWinCover.play();
                openCaver1.visible = false;
                totalBet1 = game.add.sprite(145-mobileX,148-mobileY, 'game.totalBet');
            });
            
            openCaver1.inputEnabled = false;
            openCaver1.input.useHandCursor = false;            
           
            lockDisplay();
            setTimeout('unlockDisplay()',1000);
        }
        function openBlueCaver2() {

            preOpenWinCover.play();
            
            openCaver2.animations.getAnimation('openCaver2').play().onComplete.add(function () {
                openWinCover.play();
                openCaver2.visible = false;
                totalBet2 = game.add.sprite(255-mobileX,148-mobileY, 'game.totalBet');
            });
            openCaver2.inputEnabled = false;
            openCaver2.input.useHandCursor = false;
            lockDisplay();
            setTimeout('unlockDisplay()',1000);  

            
        }
        function openBlueCaver3() {
            preOpenWinCover.play();
            
            openCaver3.animations.getAnimation('openCaver3').play().onComplete.add(function () {
                openWinCover.play();
                openCaver3.visible = false;
                totalBet3 = game.add.sprite(365-mobileX,148-mobileY, 'game.totalBet');
            });
            openCaver3.inputEnabled = false;
            openCaver3.input.useHandCursor = false;
            lockDisplay();
            setTimeout('unlockDisplay()',1000);  

            
        }
        function openBlueCaver4() {
            preOpenWinCover.play();
            
            openCaver4.animations.getAnimation('openCaver4').play().onComplete.add(function () {
                openWinCover.play();
                openCaver4.visible = false;
                totalBet4 = game.add.sprite(475-mobileX,148-mobileY, 'game.totalBet');
            });
            openCaver4.inputEnabled = false;
            openCaver4.input.useHandCursor = false;
            lockDisplay();
            setTimeout('unlockDisplay()',1000);  

            
        }
        function openBlueCaver5() {
            preOpenWinCover.play();
            
            openCaver5.animations.getAnimation('openCaver5').play().onComplete.add(function () {
                openCaver5.visible = false;
                totalBet5 = game.add.sprite(575-mobileX,148-mobileY, 'game.totalExit');

            });
            openCaver5.inputEnabled = false;
            openCaver5.input.useHandCursor = false;
            lockDisplay();
            setTimeout('unlockDisplay(); game.state.start("game1");',1000);  



            
        }

        var gear = game.add.sprite(539, 3, 'gear');
        gear.inputEnabled = true;
        gear.input.useHandCursor = true;
        gear.events.onInputDown.add(function(){
            //game.state.start('game3');
        });



        var home = game.add.sprite(3, 3, 'home');
        home.inputEnabled = true;
        home.input.useHandCursor = true;
        home.events.onInputDown.add(function(){
            home.loadTexture('home_p');
        });
        home.events.onInputUp.add(function(){
            home.loadTexture('home');
        });


        createLevelButtons();
    };

    game4.update = function () {

    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {

        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.load.image('device', 'landscape.png');

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        if(!isMobile) {
            game.load.image('start', 'start.png');
            game.load.image('start_p', 'start_p.png');
            game.load.image('start_d', 'start_d.png');
        } else {
            game.load.image('start', 'spin.png');
            game.load.image('start_p', 'spin_p.png');
            game.load.image('start_d', 'spin_d.png');
        }

        game.load.image('bet1', 'bet1.png');
        game.load.image('bet1_p', 'bet1_p.png');
        game.load.image('home', 'home.png');
        game.load.image('home_p', 'home_p.png');
        game.load.image('dollar', 'dollar.png');
        game.load.image('gear', 'gear.png');
        game.load.image('double', 'double.png');

        game.load.image('game.start', 'spin.png');
        game.load.image('game.start_p', 'spin_p.png');
        game.load.image('game.start_d', 'spin_d.png');
        
        game.load.image('game.bet1', 'bet1.png');
        game.load.image('game.bet1_p', 'bet1_p.png');
        game.load.image('game.home', 'home.png');
        game.load.image('game.home_p', 'home_p.png');
        game.load.image('game.dollar', 'dollar.png');
        game.load.image('game.gear', 'gear.png');
        game.load.image('game.double', 'double.png');

        game.load.image('game.background', 'img/canvas-bg.svg');
        game.load.image('game.canvasbg', 'img/canvas-bg.png');
        game.load.image('game.background2', 'img/image558.png');
        game.load.image('game.background3', 'img/shape777.svg');
        game.load.image('game.background4', 'img/slot_bg.png');
        game.load.image('game.background5', 'img/slot_bg.png');
        game.load.image('game.insideBackground', 'img/background1.png');
        game.load.image('game.slot_lines', 'img/slot_lines.png');
        
        

        game.load.image('game.start', 'img/image1445.png');
        game.load.image('game.start_p', 'img/image1447.png');
        game.load.image('game.start_d', 'img/image1451.png');

        game.load.spritesheet('game.man', 'img/man.png', 192, 112);
        game.load.spritesheet('game.cat', 'img/cat.png', 128, 96);
        game.load.spritesheet('game.n1', 'img/n1.png',16,16);
        game.load.spritesheet('game.arrowRedBlue', 'img/arrowRedBlue.png',96,48);
        game.load.spritesheet('game.humanThought', 'img/humanThought.png',96,96);

        game.load.image('game.selectGame', 'img/image1419.png');
        game.load.image('game.selectGame_p', 'img/image1421.png');
        game.load.image('game.selectGame_d', 'img/image1425.png');
        game.load.image('game.payTable', 'img/image1428.png');
        game.load.image('game.payTable_p', 'img/image1430.png');
        game.load.image('game.payTable_d', 'img/image1433.png');
        game.load.image('game.automaricstart', 'img/image1436.png');
        game.load.image('game.automaricstart_p', 'img/image1438.png');
        game.load.image('game.automaricstart_d', 'img/image1442.png');
        game.load.image('game.betone', 'img/image1471.png');
        game.load.image('game.betone_p', 'img/image1473.png');
        game.load.image('game.betone_d', 'img/image1477.png');
        game.load.image('game.line1', 'img/image1505.png');
        game.load.image('game.line1_p', 'img/image1507.png');
        game.load.image('game.line1_d', 'img/image1511.png');
        game.load.image('game.betmax', 'img/image1480.png');
        game.load.image('game.betmax_p', 'img/image1482.png');
        game.load.image('game.betmax_d', 'img/image1485.png');
        game.load.image('game.line3', 'img/image1496.png');
        game.load.image('game.line3_p', 'img/image1498.png');
        game.load.image('game.line3_d', 'img/image1502.png');
        game.load.image('game.line5', 'img/image1488.png');
        game.load.image('game.line5_p', 'img/image1490.png');
        game.load.image('game.line5_d', 'img/image1493.png');
        game.load.image('game.line7', 'img/image1462.png');
        game.load.image('game.line7_p', 'img/image1464.png');
        game.load.image('game.line7_d', 'img/image1468.png');
        game.load.image('game.line9', 'img/image1454.png');
        game.load.image('game.line9_p', 'img/image1456.png');
        game.load.image('game.line9_d', 'img/image1459.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.nt0', 'img/shape199.svg');
        game.load.image('game.nt1', 'img/shape201.svg');
        game.load.image('game.nt2', 'img/shape203.svg');
        game.load.image('game.nt3', 'img/shape205.svg');
        game.load.image('game.nt4', 'img/shape207.svg');
        game.load.image('game.nt5', 'img/shape209.svg');
        game.load.image('game.nt6', 'img/shape211.svg');
        game.load.image('game.nt7', 'img/shape213.svg');
        game.load.image('game.nt8', 'img/shape215.svg');
        game.load.image('game.nt9', 'img/shape217.svg');

        game.load.image('game.number0_table1', 'img/image175.png');
        game.load.image('game.number1_table1', 'img/image177.png');
        game.load.image('game.number2_table1', 'img/image179.png');
        game.load.image('game.number3_table1', 'img/image181.png');
        game.load.image('game.number4_table1', 'img/image183.png');
        game.load.image('game.number5_table1', 'img/image185.png');
        game.load.image('game.number6_table1', 'img/image187.png');
        game.load.image('game.number7_table1', 'img/image189.png');
        game.load.image('game.number8_table1', 'img/image191.png');
        game.load.image('game.number9_table1', 'img/image193.png');

        game.load.image('game.win1', 'img/shape459.svg');
        game.load.image('game.win2', 'img/shape443.svg');
        game.load.image('game.win3', 'img/shape445.svg');
        game.load.image('game.win4', 'img/shape447.svg');
        game.load.image('game.win5', 'img/shape449.svg');
        game.load.image('game.win6', 'img/shape451.svg');
        game.load.image('game.win7', 'img/shape453.svg');
        game.load.image('game.win8', 'img/shape455.svg');
        game.load.image('game.win9', 'img/shape457.svg');

        game.load.image('game.win1_dotted', 'img/shape440.svg');
        game.load.image('game.win2_dotted', 'img/shape424.svg');
        game.load.image('game.win3_dotted', 'img/shape426.svg');
        game.load.image('game.win4_dotted', 'img/shape428.svg');
        game.load.image('game.win5_dotted', 'img/shape430.svg');
        game.load.image('game.win6_dotted', 'img/shape432.svg');
        game.load.image('game.win7_dotted', 'img/shape434.svg');
        game.load.image('game.win8_dotted', 'img/shape436.svg');
        game.load.image('game.win9_dotted', 'img/shape438.svg');


        game.load.image('game.bar', 'img/bars.png');
        
        //карты
        game.load.image('game.cards_bg', 'img/shape2222.png');
        game.load.image('game.card_garage', 'img/shape220.png');

        game.load.image('game.card_2b', 'img/shape222.png');
        game.load.image('game.card_3b', 'img/shape223.png');
        game.load.image('game.card_4b', 'img/shape224.png');
        game.load.image('game.card_5b', 'img/shape225.png');
        game.load.image('game.card_6b', 'img/shape226.png');
        game.load.image('game.card_7b', 'img/shape227.png');
        game.load.image('game.card_8b', 'img/shape228.png');
        game.load.image('game.card_9b', 'img/shape229.png');
        game.load.image('game.card_10b', 'img/shape230.png');
        game.load.image('game.card_jb', 'img/shape231.png');
        game.load.image('game.card_qb', 'img/shape232.png');
        game.load.image('game.card_kb', 'img/shape233.png');
        game.load.image('game.card_ab', 'img/shape234.png');

        game.load.image('game.card_2ch', 'img/shape235.png');
        game.load.image('game.card_3ch', 'img/shape236.png');
        game.load.image('game.card_4ch', 'img/shape237.png');
        game.load.image('game.card_5ch', 'img/shape238.png');
        game.load.image('game.card_6ch', 'img/shape239.png');
        game.load.image('game.card_7ch', 'img/shape240.png');
        game.load.image('game.card_8ch', 'img/shape241.png');
        game.load.image('game.card_9ch', 'img/shape242.png');
        game.load.image('game.card_10ch', 'img/shape243.png');
        game.load.image('game.card_jch', 'img/shape244.png');
        game.load.image('game.card_qch', 'img/shape245.png');
        game.load.image('game.card_kch', 'img/shape246.png');
        game.load.image('game.card_ach', 'img/shape247.png');

        game.load.image('game.card_2k', 'img/shape248.png');
        game.load.image('game.card_3k', 'img/shape249.png');
        game.load.image('game.card_4k', 'img/shape250.png');
        game.load.image('game.card_5k', 'img/shape251.png');
        game.load.image('game.card_6k', 'img/shape252.png');
        game.load.image('game.card_7k', 'img/shape253.png');
        game.load.image('game.card_8k', 'img/shape254.png');
        game.load.image('game.card_9k', 'img/shape255.png');
        game.load.image('game.card_10k', 'img/shape256.png');
        game.load.image('game.card_jk', 'img/shape257.png');
        game.load.image('game.card_qk', 'img/shape258.png');
        game.load.image('game.card_kk', 'img/shape259.png');
        game.load.image('game.card_ak', 'img/shape260.png');

        game.load.image('game.card_2p', 'img/shape261.png');
        game.load.image('game.card_3p', 'img/shape262.png');
        game.load.image('game.card_4p', 'img/shape263.png');
        game.load.image('game.card_5p', 'img/shape264.png');
        game.load.image('game.card_6p', 'img/shape265.png');
        game.load.image('game.card_7p', 'img/shape266.png');
        game.load.image('game.card_8p', 'img/shape267.png');
        game.load.image('game.card_9p', 'img/shape268.png');
        game.load.image('game.card_10p', 'img/shape269.png');
        game.load.image('game.card_jp', 'img/shape270.png');
        game.load.image('game.card_qp', 'img/shape271.png');

        game.load.image('game.card_red', 'img/shape223.svg');
        game.load.image('game.pick', 'img/shape340.png');

        //boxes
        game.load.image('game.box_1', 'img/shape943.svg');
        game.load.image('game.box_2', 'img/shape947.svg');
        game.load.image('game.box_3', 'img/shape951.svg');
        game.load.image('game.box_4', 'img/shape955.svg');
        game.load.image('game.box_5', 'img/shape959.svg');

        game.load.image('game.red_column_1_0', 'img/shape779.svg');
        game.load.image('game.red_column_1_1', 'img/shape781.svg');
        game.load.image('game.red_column_1_2', 'img/shape783.svg');
        game.load.image('game.red_column_1_3', 'img/shape785.svg');
        game.load.image('game.red_column_1_4', 'img/shape787.svg');
        game.load.image('game.red_column_1_5', 'img/shape789.svg');

        game.load.image('game.red_column_2_0', 'img/shape793.svg');
        game.load.image('game.red_column_2_1', 'img/shape795.svg');
        game.load.image('game.red_column_2_2', 'img/shape797.svg');
        game.load.image('game.red_column_2_3', 'img/shape799.svg');
        game.load.image('game.red_column_2_4', 'img/shape801.svg');
        game.load.image('game.red_column_2_5', 'img/shape803.svg');

        game.load.image('game.red_column_3_0', 'img/shape807.svg');
        game.load.image('game.red_column_3_1', 'img/shape809.svg');
        game.load.image('game.red_column_3_2', 'img/shape811.svg');
        game.load.image('game.red_column_3_3', 'img/shape813.svg');
        game.load.image('game.red_column_3_4', 'img/shape815.svg');
        game.load.image('game.red_column_3_5', 'img/shape817.svg');

        game.load.image('game.superkey', 'img/shape760.svg');

        game.load.image('game.flashingNumber1', 'img/shape485.svg');
        game.load.image('game.flashingNumber2', 'img/shape505.svg');
        game.load.image('game.flashingNumber3', 'img/shape463.svg');
        game.load.image('game.flashingNumber4', 'img/shape495.svg');
        game.load.image('game.flashingNumber5', 'img/shape465.svg');
        game.load.image('game.flashingNumber6', 'img/shape500.svg');
        game.load.image('game.flashingNumber7', 'img/shape468.svg');
        game.load.image('game.flashingNumber8', 'img/shape480.svg');
        game.load.image('game.flashingNumber9', 'img/shape490.svg');

        game.load.spritesheet('game.box_1_number', 'img/box_1_number.png', 177, 225);
        game.load.spritesheet('game.box_2_number', 'img/box_2_number.png', 177, 225);
        game.load.spritesheet('game.box_3_number', 'img/box_3_number.png', 177, 225);
        game.load.spritesheet('game.box_4_number', 'img/box_4_number.png', 177, 225);
        game.load.spritesheet('game.box_5_number', 'img/box_5_number.png', 177, 225);

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');

        game.load.image('game.yellow_car','img/non_full.png');
        game.load.image('game.red_car','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');
        game.load.image('game.yellow_','img/non_full.png');

        game.load.audio('game4.loss', 'bonus2/b2_loss.wav');
        game.load.audio('game4.win', 'bonus2/b2_win.wav');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');
        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        function preloadLevelButtons() {
            game.load.image('btnline11', 'lines/line11.png');
            game.load.image('btnline_p11', 'lines/line11_p.png');
            game.load.image('btnline13', 'lines/line13.png');
            game.load.image('btnline_p13', 'lines/line13_p.png');
            game.load.image('btnline15', 'lines/line15.png');
            game.load.image('btnline_p15', 'lines/line15_p.png');
        }

        game.load.image('bars', 'bars.png');
        game.load.image('start', 'start.png');
        game.load.image('start_p', 'start_p.png');
        game.load.image('start_d', 'start_d.png');
        game.load.image('bet', 'bet.png');
        game.load.image('canvasbg', 'canvas-bg.png');
        game.load.audio('sound', 'spin.mp3');
        game.load.audio('rotate', 'rotate.wav');
        game.load.audio('stop', 'stop.wav');
        game.load.audio('tada', 'tada.wav');
        game.load.audio('play', 'play.mp3');

        for (var i = 1; i <= 9; ++i) {
            game.load.image('line_' + i, 'lines/select/' + i + '.png');
            game.load.image('linefull_' + i, 'lines/win/' + i + '.png');
            if (i % 2 != 0) {
                game.load.audio('line' + i, 'lines/sounds/line' + i + '.wav');
                game.load.image('btnline' + i, 'lines/line' + i + '.png');
                game.load.image('btnline_p' + i, 'lines/line' + i + '_p.png');
                game.load.image('btnline_d' + i, 'lines/line' + i + '_d.png');
            }
        }

        preloadLevelButtons();

        game.load.image('background2', 'cards/back.png');
        game.load.image('shlem', 'cards/shlem.png');
        game.load.image('dealer', 'cards/dealer.png');
        game.load.image('pick', 'cards/pick.png');
        game.load.image('card_back', 'cards/cards/back.png');
        game.load.spritesheet('monkey2', 'cards/monkey.png', 182, 209, 15);
        game.load.spritesheet('message', 'cards/message.png', 179, 65, 5);
        var cardValues = {
            1: 11,
            2: 7,
            3: 3,
            4: 14,
            5: 13
        };
        for(var i in cardValues) {
            game.load.image('card_'+i, 'cards/cards/'+i+'.png');
        }

        game.load.audio('opencard', 'cards/opencard.wav');
        game.load.audio('openuser', 'cards/openuser.wav');

        game.load.image('background3', 'bonus/back.png');
        game.load.spritesheet('rope', 'bonus/rope.png', 21, 335, 17);
        game.load.audio('b1_keypress', 'bonus/b1_keypress.wav');
        game.load.audio('b1_prizedown', 'bonus/b1_prizedwn.wav');
        game.load.audio('b1_shit', 'bonus/b1_shit.wav');

        for(var i=0; i<=3; ++i) {
            game.load.image('prize_' + i, 'bonus/prizes/' + i + '.png');
        }

        game.load.audio('game.openCardAudio', 'sound/sound31.mp3');
        game.load.audio('game.arrowSound', 'sound/sound7.mp3');
        game.load.audio('game.openBoxSound', 'sound/sound17.mp3');
        game.load.audio('game.openKeySound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxLoseSound', 'sound/sound8.mp3');
        game.load.audio('game.openKeyBoxPreLoseSound', 'sound/sound4.mp3');
        game.load.audio('game.openKeyBoxPreWinSound', 'sound/sound5.mp3');
        game.load.audio('game.openKeyBoxWinSound', 'sound/sound4.mp3');
        game.load.audio('game.openPoliceBox', 'sound/sound10.mp3');
        game.load.audio('game.preOpenBox', 'sound/sound1.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');
        game.load.audio('game.betOneSound', 'sound/sound20.mp3');
        game.load.audio('game.betMaxSound', 'sound/sound33.mp3');
        game.load.audio('takeWin', 'takeWin.mp3');
        game.load.audio('takeBox', 'sound/sound40.mp3');


        game.load.spritesheet('game.openLockFromSlot', 'img/openLockFromSlot.png', 96, 113);
        game.load.spritesheet('game.openBoxFromSlot', 'img/openBoxFromSlot.png', 96, 113);
        game.load.spritesheet('game.openLock2', 'img/openLock2.png', 48, 48);

        game.load.spritesheet('game.openLoseLeftCase', 'img/openLoseLeftCase.png', 192, 80);
        game.load.spritesheet('game.openLoseRightCase', 'img/openLoseRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinRightCase', 'img/openWinRightCase.png', 192, 80);
        game.load.spritesheet('game.openWinLeftCase', 'img/openWinLeftCase.png', 192, 80);
        game.load.spritesheet('game.findAKey', 'img/findAKey.png', 96, 16);

        game.load.spritesheet('game.clockBox', 'img/clockBox.png', 177, 225);
        game.load.spritesheet('game.detailBox', 'img/detailBox.png', 177, 225);
        game.load.spritesheet('game.flashBox', 'img/flashBox.png', 177, 225);
        game.load.spritesheet('game.franchBox', 'img/franchBox.png', 177, 225);
        game.load.spritesheet('game.hammer2Box', 'img/hammer2Box.png', 177, 225);
        game.load.spritesheet('game.HummerBox', 'img/HummerBox.png', 177, 225);
        game.load.spritesheet('game.jackBox', 'img/jackBox.png', 177, 225);
        game.load.spritesheet('game.policeBox', 'img/policeBox.png', 177, 225);
        game.load.spritesheet('game.sawBox', 'img/sawBox.png', 177, 225);

        game.load.image('game.number1_for_box', 'img/image177.png');
        game.load.image('game.number2_for_box', 'img/image179.png');
        game.load.image('game.number3_for_box', 'img/image181.png');
        game.load.image('game.number4_for_box', 'img/image183.png');
        game.load.image('game.number5_for_box', 'img/image185.png');
        game.load.image('game.number6_for_box', 'img/image187.png');
        game.load.image('game.number7_for_box', 'img/image189.png');
        game.load.image('game.number8_for_box', 'img/image191.png');
        game.load.image('game.number9_for_box', 'img/image193.png');
        game.load.image('game.number0_for_box', 'img/image175.png');
        game.load.image('game.number0_for_box', 'img/image175.png');

        game.load.spritesheet('game.flashNamber1', 'img/flashingNumber1.png', 640, 32);
        game.load.spritesheet('game.flashNamber2', 'img/flashingNumber2.png', 640, 32);
        game.load.spritesheet('game.flashNamber3', 'img/flashingNumber3.png', 640, 32);
        game.load.spritesheet('game.flashNamber4', 'img/flashingNumber4.png', 640, 32);
        game.load.spritesheet('game.flashNamber5', 'img/flashingNumber5.png', 640, 32);
        game.load.spritesheet('game.flashNamber6', 'img/flashingNumber6.png', 640, 32);
        game.load.spritesheet('game.flashNamber7', 'img/flashingNumber7.png', 640, 32);
        game.load.spritesheet('game.flashNamber8', 'img/flashingNumber8.png', 640, 32);
        game.load.spritesheet('game.flashNamber9', 'img/flashingNumber9.png', 640, 32);

        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.spritesheet('game.move_super_key', 'img/move_super_key.png', 119, 160);
        game.load.image('game.small_red_key', 'img/small_red_key.png');

        game.load.spritesheet('game.super_key_in_tablo', 'img/super_key_in_tablo.png', 160, 48);
        game.load.spritesheet('game.take', 'img/take.png', 64, 32);
        game.load.spritesheet('game.take_or_risk', 'img/take_or_risk.png', 128, 32);
        game.load.spritesheet('game.play_1_to', 'img/play_1_to.png', 128, 48);
        game.load.image('game.lock_bonus_game', 'img/shape1292.svg');
        game.load.image('game.box_bonus_game', 'img/shape1295.svg');

        game.load.spritesheet('game.grammofon1', 'img/grammofon1.png', 128, 96);
        game.load.spritesheet('game.barmen_part11', 'img/barmen_part11.png', 320, 112);
        game.load.spritesheet('game.barmen_part12', 'img/barmen_part12.png', 320, 112);
        game.load.spritesheet('game.barmen_part41', 'img/barmen_part41.png', 320, 112);
        game.load.spritesheet('game.barmen_part42', 'img/barmen_part42.png', 320, 112);
        game.load.spritesheet('game.barmenWin1', 'img/barmenWin1.png', 320, 112);
        game.load.spritesheet('game.barmenWin2', 'img/barmenWin2.png', 320, 112);
        game.load.spritesheet('game.coverRotation', 'img/coverRotation.png', 96, 112);

        game.load.audio('winCover', 'sound/winCover.mp3');

        game.load.image('game.backgroundGame4', 'img/shape489.png');
        game.load.image('game.shape52', 'img/shape52.png');
        game.load.image('game.backgroundTotal', 'img/shape14.svg');
        game.load.image('game.totalBet', 'img/totalBetX5.png');
        game.load.image('game.totalExit', 'img/shape510.svg');

        game.load.spritesheet('game.openCaver', 'img/openCaver.png', 112, 128);
        game.load.spritesheet('game.gramofon4', 'img/gramofon4.png', 128, 96);

        game.load.audio('game.preOpenWinCover', 'sound/preOpenWinCover.mp3');
        game.load.audio('game.openWinCover', 'sound/openWinCover.mp3');

        game.load.image('game.backgroundGame3', 'img/792.png');
        game.load.spritesheet('game.grammofon3', 'img/grammofon3.png', 128, 96);

        game.load.image('game.backgroundGame2', 'img/793.png');
        game.load.spritesheet('game.arrows', 'img/arrows.png', 144, 80);
        game.load.image('game.backgroundBludo', 'img/shape523.png');
        game.load.image('game.bludoTitle', 'img/shape521.png');
        game.load.image('game.bludo', 'img/shape659.png');
        game.load.image('game.chickenBludo', 'img/shape532.png');
        game.load.image('game.fishBludo', 'img/shape656.png');
        game.load.spritesheet('game.chikenAnin', 'img/chikenAnin.png', 128, 96);

        game.load.image('game.bonusGame', 'img/bonusGame.png');
    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();