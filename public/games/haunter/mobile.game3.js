var stepTotalWinR = 0;

var ropeStep = 0;
function showWin(x, y, win, stepTotalWinR) {
    if(ropeValues[ropeStep] > 0 && ropeStep == 4) {
        ropeStep += 1;
        var text = game.add.text(x,y, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        setTimeout("game.state.start('game4')", 1000);
    } else {
        if(win == 0) {
            updateBalanceGame3(game, scorePosions, balanceR);
        } else {
            ropeStep += 1;
            var text = game.add.text(x,y, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }
    }
}

function updateBalanceGame3(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference - 1, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}






function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            ropeStep = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса

            //звуки
            preOpenWinCover = game.add.sound('game.preOpenWinCover');
            openWinCover = game.add.sound('game.openWinCover');


            //изображения
            background2 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame4');
            backgroundTotal = game.add.sprite(95-mobileX,23-mobileY, 'game.backgroundTotal');

            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [435-mobileX, 26-mobileY, 20], [610-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, '', balance, betline);



            /*



            */

            //анимации и логика

            gramofon4 = game.add.sprite(415-mobileX,406-mobileY, 'game.gramofon4');
            gramofon4.animations.add('gramofon4', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29], 4, true);
            gramofon4.animations.getAnimation('gramofon4').play();
            //gramofonR2 = game.add.sprite(530,406, 'game.gramofonR2');

            barmen_part41 = game.add.sprite(94-mobileX,390-mobileY, 'game.barmen_part41');
            barmen_part41.animations.add('barmen_part41', [0,1,2,3,4,5,6,7,8,9,10,11], 3, true);
            barmen_part41.animations.getAnimation('barmen_part41').play();

            barmenR2 = game.add.sprite(400-mobileX,394-mobileY, 'game.barmenR2');

            openCaver1 = game.add.sprite(145-mobileX,148-mobileY, 'game.openCaver');
            openCaver1.inputEnabled = true;
            openCaver1.animations.add('openCaver1', [0,1,2,3,4,5,6], 30, false);
            openCaver1.events.onInputDown.add(function(){

                openCaver1.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) {
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                openBlueCaver1(ropeValues[ropeStep]);

                setTimeout(showWin,500, 195-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);

                ropeStep += 1;
            });

            openCaver2 = game.add.sprite(255-mobileX,148-mobileY, 'game.openCaver');
            openCaver2.inputEnabled = true;
            openCaver2.animations.add('openCaver2', [0,1,2,3,4,5,6], 30, false);
            openCaver2.events.onInputDown.add(function(){

                openCaver2.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }


                openBlueCaver2(ropeValues[ropeStep]);
                setTimeout(showWin,500, 195+110-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);

                ropeStep += 1;
            });

            openCaver3 = game.add.sprite(365-mobileX,148-mobileY, 'game.openCaver');
            openCaver3.inputEnabled = true;
            openCaver3.animations.add('openCaver3', [0,1,2,3,4,5,6], 30, false);
            openCaver3.events.onInputDown.add(function(){

                openCaver3.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                openBlueCaver3(ropeValues[ropeStep]);
                setTimeout(showWin,500, 195+220-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);

                ropeStep += 1;
            });

            openCaver4 = game.add.sprite(475-mobileX,148-mobileY, 'game.openCaver');
            openCaver4.inputEnabled = true;
            openCaver4.animations.add('openCaver4', [0,1,2,3,4,5,6], 30, false);
            openCaver4.events.onInputDown.add(function(){

                openCaver4.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                openBlueCaver4(ropeValues[ropeStep]);
                setTimeout(showWin,500, 195+330-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);

                ropeStep += 1;
            });

            openCaver5 = game.add.sprite(585-mobileX,148-mobileY, 'game.openCaver');
            openCaver5.inputEnabled = true;
            openCaver5.animations.add('openCaver5', [0,1,2,3,4,5,6], 30, false);
            openCaver5.events.onInputDown.add(function(){

                openCaver5.inputEnabled = false;

                if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                openBlueCaver5(ropeValues[ropeStep]);
                setTimeout(showWin, 500, 195+440-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);

                ropeStep += 1;
            });

            function openBlueCaver1(ropeValue) {
                preOpenWinCover.play();

                openCaver1.animations.getAnimation('openCaver1').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver1.visible = false;
                    if(ropeValue > 0) {
                        totalBet1 = game.add.sprite(145-mobileX,148-mobileY, 'game.totalBet');
                    } else {
                        totalBet1 = game.add.sprite(145-mobileX,148-mobileY, 'game.totalExit');
                    }
                });
                /*openCaver1.inputEnabled = false;
                 openCaver1.input.useHandCursor = false;*/
                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            }
            function openBlueCaver2(ropeValue) {
                preOpenWinCover.play();

                openCaver2.animations.getAnimation('openCaver2').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver2.visible = false;
                    if(ropeValue > 0) {
                        totalBet2 = game.add.sprite(255-mobileX,148-mobileY, 'game.totalBet');
                    } else {
                        totalBet2 = game.add.sprite(255-mobileX,148-mobileY, 'game.totalExit');
                    }
                });
                /*openCaver2.inputEnabled = false;
                 openCaver2.input.useHandCursor = false;*/
                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            }
            function openBlueCaver3(ropeValue) {
                preOpenWinCover.play();

                openCaver3.animations.getAnimation('openCaver3').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver3.visible = false;
                    if(ropeValue > 0) {
                        totalBet3 = game.add.sprite(365-mobileX,148-mobileY, 'game.totalBet');
                    } else {
                        totalBet3 = game.add.sprite(365-mobileX,148-mobileY, 'game.totalExit');
                    }
                });
                /*openCaver3.inputEnabled = false;
                 openCaver3.input.useHandCursor = false;*/
                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            }
            function openBlueCaver4(ropeValue) {
                preOpenWinCover.play();

                openCaver4.animations.getAnimation('openCaver4').play().onComplete.add(function () {
                    openWinCover.play();
                    openCaver4.visible = false;
                    if(ropeValue > 0) {
                        totalBet4 = game.add.sprite(475-mobileX,148-mobileY, 'game.totalBet');
                    } else {
                        totalBet4 = game.add.sprite(475-mobileX,148-mobileY, 'game.totalExit');
                    }
                });
                /*openCaver4.inputEnabled = false;
                 openCaver4.input.useHandCursor = false;*/
                lockDisplay();
                setTimeout('unlockDisplay()',1000);
            }
            function openBlueCaver5(ropeValue) {
                preOpenWinCover.play();

                openCaver5.animations.getAnimation('openCaver5').play().onComplete.add(function () {
                    openCaver5.visible = false;
                    if(ropeValue > 0) {
                        totalBet5 = game.add.sprite(585-mobileX,148-mobileY, 'game.totalBet');
                    } else {
                        totalBet5 = game.add.sprite(585-mobileX,148-mobileY, 'game.totalExit');
                    }


                });
                /*openCaver5.inputEnabled = false;
                 openCaver5.input.useHandCursor = false;*/
                lockDisplay();
                setTimeout('unlockDisplay();', 1000);
            }


            var ropeStep = 0;
            //часть логики занесена в анимацию, так как в разных игра она может отличаться для данной бонусной игры
            //0 - банан, 1 - кувалда, 2 - кирпич
            var win = 0;
            var ropeStep = 0;
            var checkUpLvl = true; //проверка доступа ко второй бонусной игре

        };

        game3.update = function () {
        };

        game.state.add('game3', game3);

    })();

}