window.game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example');

isMobile = true;

var mobileX = 93;
var mobileY = 23;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
var gamename = 'island2rus'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0], linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0], linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0], linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0], linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0], linePosition[4][1]+12, lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0], linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0], linePosition[6][1]+56, lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0], linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0], linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
var linesArray = [];
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 11:
                line1.visible = false;
                break;
                case 12:
                line2.visible = false;
                break;
                case 13:
                line3.visible = false;
                break;
                case 14:
                line4.visible = false;
                break;
                case 15:
                line5.visible = false;
                break;
                case 16:
                line6.visible = false;
                break;
                case 17:
                line7.visible = false;
                break;
                case 18:
                line8.visible = false;
                break;
                case 19:
                line9.visible = false;
                break;
            }
        });
    }
}

function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = true;
                break;
                case 2:
                linefull2.visible = true;
                break;
                case 3:
                linefull3.visible = true;
                break;
                case 4:
                linefull4.visible = true;
                break;
                case 5:
                linefull5.visible = true;
                break;
                case 6:
                linefull6.visible = true;
                break;
                case 7:
                linefull7.visible = true;
                break;
                case 8:
                linefull8.visible = true;
                break;
                case 9:
                linefull9.visible = true;
                break;
                case 11:
                line1.visible = true;
                break;
                case 12:
                line2.visible = true;
                break;
                case 13:
                line3.visible = true;
                break;
                case 14:
                line4.visible = true;
                break;
                case 15:
                line5.visible = true;
                break;
                case 16:
                line6.visible = true;
                break;
                case 17:
                line7.visible = true;
                break;
                case 18:
                line8.visible = true;
                break;
                case 19:
                line9.visible = true;
                break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = false;
                break;
                case 2:
                number2.visible = false;
                break;
                case 3:
                number3.visible = false;
                break;
                case 4:
                number4.visible = false;
                break;
                case 5:
                number5.visible = false;
                break;
                case 6:
                number6.visible = false;
                break;
                case 7:
                number7.visible = false;
                break;
                case 8:
                number8.visible = false;
                break;
                case 9:
                number9.visible = false;
                break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                number1.visible = true;
                break;
                case 2:
                number2.visible = true;
                break;
                case 3:
                number3.visible = true;
                break;
                case 4:
                number4.visible = true;
                break;
                case 5:
                number5.visible = true;
                break;
                case 6:
                number6.visible = true;
                break;
                case 7:
                number7.visible = true;
                break;
                case 8:
                number8.visible = true;
                break;
                case 9:
                number9.visible = true;
                break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [];
    soundForBattons.push(line1Sound, line3Sound, line5Sound, line7Sound, line9Sound);

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535,510, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines([]);
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons();
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines([]);
        linesArray = [11];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines([]);
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки и логика для игры с последовательным выбором
var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
function addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm){
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[0](ropeValues, ropeStep, checkHelm);

        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }


        ropesAnim[1](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine5.events.onInputDown.add(function(){

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[2](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[3](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine9.events.onInputDown.add(function(){

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[4](ropeValues, ropeStep);
        setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
}

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
function addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1) {
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });
    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        selectionsAmin[0](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }

    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        selectionsAmin[1](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }
    });
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
var buttonsArray = [];
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
            //collect.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
            collect.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');

    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        if(typeof finalValues != "undefined") {
            totalWinR = finalValues[15];
            linesR = finalValues[16];
            betlineR = finalValues[17];

            checkSpinResult(totalWinR);

            slotRotation(game, finalValues);
        }




    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('rowWin');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topBarType1'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 575-mobileX, 434-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 575, 434);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);

    $.ajax({
       type: "POST",
       url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
       dataType: 'html',
       success: function (data) {
     //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=20&extralife=45&jackpots=1822.16|4122.16|6122.16';
     // data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|1&extralife=45&jackpots=1822.16|4122.16|6122.16';
     //alert(data);
     dataSpinRequest = data;

     parseSpinAnswer(dataSpinRequest);
 },
 error: function (xhr, ajaxOptions, thrownError) {
   var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
   alert(errorText);
}
});
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        //type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=init',
        url: 'test.php',
        dataType: 'html',
        success: function (data) {

            dataString = data;

            initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        //url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            startDataArray = dataString.split('&');

            if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }

                    /*game1();
                     game2();
                     game3();
                     game4();*/
                 });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('game.insideBackground'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();

    /*dataDoubleRequest = data.split('&');
    parseDoubleAnswer(dataDoubleRequest);*/

    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y, fontsize) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: fontsize+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: fontsize+'px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
    if(!isMobile) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    } else {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout("card1.loadTexture('card_'+dcard); openCard.play(); enableInputCards(); unlockDisplay();", 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {
    openCard.play();
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_'+valuesOfAllCards[i]);
    });
}

function hideAllCards(cardArray) {
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}





//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var loseTryInGame3 = false;
function showWinGame3(winRopeNumberSize, x, y, win, stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim) {

    if(typeWinRopeNumberAnim == 0) {
        if(loseTryInGame3 == false) {
            if(ropeValues[4] > 0 && ropeStep == 5) {

                var text = game.add.text(x,152 - mobileY + 32 - 32*5, win, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "Press Start 2P"',
                    fill: '#fcfe6e',
                    stroke: '#000000',
                    strokeThickness: 3,
                });

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game4')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {

                    game.add.sprite(185-80 - mobileX,152 - mobileY + 32 - 32*ropeStep, 'winTryInGame3'); //для появления галочки в верхней левой таблице в игре

                    var text = game.add.text(x,152 - mobileY + 32 - 32*ropeStep, win, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }
        }

    }

}

function updateBalanceGame3(game, scorePosions, balanceR) {
    if(!isMobile) {
        hideButtons([[buttonLine1, 'buttonLine1'],[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], ]);
    }

    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}



//функции для игры с выбором из двух вариантов

function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
    }, timeInterval);
}





//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
        betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });
    gear.events.onInputUp.add(function(){
        // hideButtons([[payTable, 'payTable'], [betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
        hideButtons([]);
        pagePaytables[1].visible = true;
        prev_page.visible = true;
        exit_btn.visible = true;
        next_page.visible = true;
        settingsMode = true;
        currentPage = 1;
        betline1Score.visible = false;
        betline2Score.visible = false;
        betScore.visible = false;
        linesScore.visible = false;
        balanceScore.visible = false;
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_d');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}

var pagePaytables =[];
var settingsMode = false;
var currentPage = null;

function addPaytable(pageCount, pageCoord, btnCoord){
    pageSound = game.add.audio('page');
    for (var i = 1; i <= pageCount; ++i) {
        pagePaytable = game.add.sprite(pageCoord[i-1][0], pageCoord[i-1][1], 'pagePaytable_' + i);
        pagePaytable.visible = false;
        pagePaytables[i] = pagePaytable;
    }

    prev_page = game.add.sprite(btnCoord[0][0], btnCoord[0][1], 'prev_page');
    prev_page.visible = false;
    prev_page.inputEnabled = true;
    prev_page.input.useHandCursor = true;
    prev_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == 1)
                currentPage = pageCount;
            else{
                pagePaytables[currentPage].visible = false;
                currentPage -=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
    exit_btn = game.add.sprite(btnCoord[1][0], btnCoord[1][1], 'exit_btn');
    exit_btn.visible = false;
    exit_btn.inputEnabled = true;
    exit_btn.input.useHandCursor = true;
    exit_btn.events.onInputUp.add(function(){
        pageSound.play();
        for (var i = 1; i <= pageCount; ++i) {
            pagePaytables[i].visible = false;
        }            
        prev_page.visible = false;
        exit_btn.visible = false;
        next_page.visible = false;
        settingsMode = false;
        betline1Score.visible = true;
        betline2Score.visible = true;
        betScore.visible = true;
        linesScore.visible = true;
        balanceScore.visible = true;
        showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
        // showButtons([[betmax, 'betmax'], [betone, 'betone'], [automaricstart, 'automaricstart'], [selectGame, 'selectGame'], [payTable, 'payTable'], [buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"]]);
    });
    next_page = game.add.sprite(btnCoord[2][0], btnCoord[2][1], 'next_page');
    next_page.visible = false;
    next_page.inputEnabled = true;
    next_page.input.useHandCursor = true;
    next_page.events.onInputUp.add(function(){
        if (settingsMode)  {
            pageSound.play();
            if (currentPage == pageCount){
                pagePaytables[currentPage].visible = false;
                currentPage = 1;
            } else if (currentPage == 1){
                currentPage +=1;
            } else {                    
                pagePaytables[currentPage].visible = false;
                currentPage +=1;
            }
        }
        pagePaytables[currentPage].visible = true;
    });
};

var coin1; var coin2;
var buttonsImages = [];
var collect;
function addButtonsGame2Mobile(game, selectAnOption, buttonsImages) {
    collect = game.add.sprite(538, 200, 'collect');
    collect.inputEnabled = true;
    collect.input.useHandCursor = true;
    collect.events.onInputUp.add(function(){
        collect.loadTexture('collect');
    });
    collect.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    collect.scale.setTo(0.7,0.7);

    coin1 = buttonsImages[0];
    coin2 = buttonsImages[1];
    coin1.inputEnabled = true;
    coin1.input.useHandCursor = true;
    coin2.inputEnabled = true;
    coin2.input.useHandCursor = true;
    coin1.events.onInputDown.add(function(){
        requestDoubleV2(gamename, 1, lines, bet, sid, selectAnOption);
    });
    coin2.events.onInputDown.add(function(){
        requestDoubleV2(gamename, 2, lines, bet, sid, selectAnOption);
    });
}

var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
var buttonsGame3Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок
function addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm) {
    //логика для событий нажатия на изображения
    buttonsGame3Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){

            button.inputEnabled = false;

            if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
            }

            ropesAnim[i](ropeValues,ropeStep, checkHelm);

            //setTimeout(showWin, 500, 195+440-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);
            setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[i][0],winRopeNumberPosition[i][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

            ropeStep += 1;
        });
    });
}

//функции для игры с выбором из двух вариантов
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
var buttonsGame4Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок
function addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile,  timeout1Game4ToGame1, timeout2Game4ToGame1) {
    buttonsGame4Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){
            button.inputEnabled = false;

            selectionsAmin[i](ropeValues, ropeStep);

            if(ropeValues[5] != 0){
                lockDisplay();
                setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
            } else {
                lockDisplay();
                setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
            }
        });
    });
}






//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}


function requestDoubleV2(gamename, selectedCard, lines, bet, sid, selectAnOption) {
    //hideButtons([[startButton, 'startButton']]);
    lockDisplay();

    /*data = 'result=ok&info=|40|29|30|31|7&dwin=40&balance=100004.00&dcard2=1&select=1&jackpots=1826.60|4126.60|6126.60';
    dataDoubleRequest = data.split('&');
    parseDoubleAnswerV2(dataDoubleRequest, selectAnOption);*/

    $.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswerV2(dataDoubleRequest, selectAnOption);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

function parseDoubleAnswerV2(dataDoubleRequest, selectAnOption) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            /*if (item.indexOf('info=') + 1) {
             var cellValuesString = item.replace('info=|', '');
             valuesOfAllCards = cellValuesString.split('|');
         }*/
         if (item.indexOf('select=') + 1) {
            selectedCardR = item.replace('select=', '');
        }
        if(item.indexOf('jackpots=') + 1) {
            var jackpotsString = item.replace('jackpots=','');
            jackpots =  jackpotsString.split('|');
        }
    });

        showDoubleResultV2(dwin, selectedCardR, selectAnOption);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

function showDoubleResultV2(dwin, selectedCardR, selectAnOption) {
    if (isMobile) {

        hideDoubleToAndTakeOrRiskTexts();
        tableTitle.visible = true;

        if(dwin > 0) {
            changeTableTitle('winTitleGame2');

            selectAnOption[selectedCardR][1]();

            setTimeout('tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize); unlockDisplay();', 3000);
        } else {
            changeTableTitle('loseTitleGame2');

            selectAnOption[selectedCardR][0]();

            setTimeout('tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('game.state.start("game1"); unlockDisplay();', 3000);
        }

    }
}


//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

requestInit();

(function () {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки

        var playSound = game.add.audio('play');

        // изображения
        topBarImage = game.add.sprite(93-mobileX,23-mobileX, 'topBarType2');
        game1.bg_top = game.add.sprite(94-mobileX,22-mobileY, 'bg_top1');
        game1.bg = game.add.sprite(94-mobileX,54-mobileY, 'game1.bg');


        slotPosition = [[142-mobileX, 88-mobileY], [142-mobileX, 200-mobileY], [142-mobileX, 312-mobileY], [254-mobileX, 88-mobileY], [254-mobileX, 200-mobileY], [254-mobileX, 312-mobileY], [366-mobileX, 88-mobileY], [366-mobileX, 200-mobileY], [366-mobileX, 312-mobileY], [478-mobileX, 88-mobileY], [478-mobileX, 200-mobileY], [478-mobileX, 312-mobileY], [590-mobileX, 88-mobileY], [590-mobileX, 200-mobileY], [590-mobileX, 312-mobileY]];
        addSlots(game, slotPosition);

        addTableTitle(game, 'play1To',557-mobileX,422-mobileY);



        var linePosition = [[134-mobileX,199+50-mobileY], [134-mobileX,71+33-mobileY], [134-mobileX,322+64-mobileY], [134-mobileX,130+33-mobileY], [134-mobileX,95+34-mobileY], [134-mobileX,102+33-mobileY], [134-mobileX,228+34-mobileY], [134-mobileX,226+50-mobileY], [134-mobileX,120+34-mobileY]];
        var numberPosition = [[93-mobileX,183+50-mobileY], [93-mobileX,54+33-mobileY], [93-mobileX,310+64-mobileY], [93-mobileX,118+33-mobileY], [93-mobileX,246+64-mobileY], [93-mobileX,86+33-mobileY], [93-mobileX,278+64-mobileY], [93-mobileX,214+50-mobileY], [93-mobileX,150+50-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);


        var pageCount = 5;
        // кнопки
        addButtonsGame1Mobile(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230-mobileX, 26-mobileY, 20], [435-mobileX, 26-mobileY, 20], [610-mobileX, 26-mobileY, 20], [99-mobileX, 59-mobileY, 16], [707-mobileX, 59-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        game1.strawberry_bar_anim_1 = game.add.sprite(142-mobileX, 88-mobileY, 'bonus');
        game1.strawberry_bar_anim_1Animation = game1.strawberry_bar_anim_1.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_1.visible = false;
        game1.strawberry_bar_anim_2 = game.add.sprite(254-mobileX, 88-mobileY, 'bonus');
        game1.strawberry_bar_anim_2Animation = game1.strawberry_bar_anim_2.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_2.visible = false;
        game1.strawberry_bar_anim_3 = game.add.sprite(366-mobileX, 88-mobileY, 'bonus');
        game1.strawberry_bar_anim_3Animation = game1.strawberry_bar_anim_3.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_3.visible = false;
        game1.water_anim = game.add.sprite(542-mobileX, 486-mobileY, 'water_anim');
        game1.water_animAnimation = game1.water_anim.animations.add('water_anim', [0,1,2,3], 8, true);
        game1.water_animAnimation.play();
        game1.water_bottom_1 = game.add.sprite(94-mobileX, 470-mobileY, 'water_bottom_1');
        game1.water_bottom_1Animation = game1.water_bottom_1.animations.add('water_bottom_1', [0,1,2,3,4,5,6,7,8,9], 8, false);
        game1.water_bottom_1Animation.play();
        game1.water_bottom_2 = game.add.sprite(94-mobileX, 470-mobileY, 'water_bottom_2');
        game1.water_bottom_2Animation = game1.water_bottom_2.animations.add('water_bottom_2', [0,1,2,3], 8, false);
        game1.water_bottom_2.visible = false;
        game1.man_1 = game.add.sprite(94-mobileX, 390-mobileY, 'game1.man_1');
        game1.man_1Animation = game1.man_1.animations.add('game1.man_1', [0,1,2,3,4,5,6,7,8,9,10], 8, true);
        game1.man_1Animation.play();
        game1.win = game.add.sprite(94-mobileX, 390-mobileY, 'game1.win');
        game1.winAnimation = game1.win.animations.add('game1.win', [0,1,2,3,4,5], 8, false);
        game1.win.visible = false;
        game1.bird_anim_1 = game.add.sprite(462-mobileX, 406-mobileY, 'bird_anim_1');
        game1.bird_anim_1Animation = game1.bird_anim_1.animations.add('bird_anim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44], 8, false);
        game1.bird_anim_1Animation.play();

        game1.bird_anim_2 = game.add.sprite(462-mobileX, 406-mobileY, 'bird_anim_2');
        game1.bird_anim_2Animation = game1.bird_anim_2.animations.add('bird_anim_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
        game1.bird_anim_2.visible = false;
        game1.winAnimation.onComplete.add(function(){
            game1.win.visible = false;
        });
        game1.bird_anim_1Animation.onComplete.add(function(){
            game1.bird_anim_1.visible = false;
            game1.bird_anim_2.visible = true;
            game1.bird_anim_2Animation.play();
        });
        game1.bird_anim_2Animation.onComplete.add(function(){
            game1.bird_anim_1.visible = true;
            game1.bird_anim_2.visible = false;
            game1.bird_anim_1Animation.play();
        });
        game1.water_bottom_1Animation.onComplete.add(function(){
            game1.water_bottom_1.visible = false;
            game1.water_bottom_2.visible = true;
            game1.water_bottom_2Animation.play();
        });
        game1.water_bottom_2Animation.onComplete.add(function(){
            game1.water_bottom_1.visible = true;
            game1.water_bottom_2.visible = false;
            game1.water_bottom_1Animation.play();
        });
        var pageCoord = [[94-mobileX, 22-mobileY], [94-mobileX, 22-mobileY], [94-mobileX, 22-mobileY], [94-mobileX, 22-mobileY], [94-mobileX, 22-mobileY]];   
        var btnCoord = [[107-mobileX, 442-mobileY], [312-mobileX, 442-mobileY], [521-mobileX, 442-mobileY]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);
    };



    game1.update = function () {

    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================
var selectAnOption = [[0,0], [0,0], [0,0]];

var sound40;
var flicker;

var pick_left;
var pick_right;

function restartGame() {
    if(typeof(triger_right) != undefined) {
        triger_right.visible = false;
    } else {
        triger_left.visible = false;
    }

    flip_coin.visible = false;

    flip_coin = game.add.sprite(302-mobileX, 102-mobileY, 'game2.flip_coin');
    flip_coinAnimationeagle = flip_coin.animations.add('game2.flip_coin_eagle', [0,1,2,3,4,5,6,7,8,9], 12, false);
    flip_coinAnimationtails = flip_coin.animations.add('game2.flip_coin_tails', [0,1,2,3,4,5,6,7,8,10], 12, false);

    pick_left = game.add.sprite(94-mobileX,342-mobileY, 'pick_left');
    pick_left.visible = true;
    pick_right = game.add.sprite(94-mobileX,342-mobileY, 'pick_right');
    pick_right.visible = false;

    flick();
}

function flick(){
    if (flicker){
        pick_left.visible = false;
        pick_right.visible = true;
        setTimeout(function() {
            flicker = false;
            flick();
        }, 630);
    }	else {
        pick_left.visible = true;
        pick_right.visible = false;
        setTimeout(function() {
            flicker = true;
            flick();
        }, 630);
    }
};

(function () {

    var game2 = {
        button : [],
        pick_dice : null,
        pick : [],
        flicker : false,
        create : function() {

            game2.bg_top = game.add.sprite(88-mobileX,22-mobileY, 'bg_top1');
            game.add.sprite(94-mobileX,22-mobileY, 'bg_top2');
            game2.bg2 = game.add.sprite(94-mobileX,54-mobileY, 'game2.bg');

            flip_coin = game.add.sprite(302-mobileX, 102-mobileY, 'game2.flip_coin');
            flip_coinAnimationeagle = flip_coin.animations.add('game2.flip_coin_eagle', [0,1,2,3,4,5,6,7,8,9], 12, false);
            flip_coinAnimationtails = flip_coin.animations.add('game2.flip_coin_tails', [0,1,2,3,4,5,6,7,8,10], 12, false);

            var coinHistory = ['tails','tails','tails','tails'];
            var coinHistoryImage = [];
            coinHistoryImage.push(game.add.sprite(126-mobileX, 118-mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126-mobileX, 166-mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126-mobileX, 214-mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126-mobileX, 262-mobileY, 'tails'));

            sound40 = game.add.audio('7_sound40');
            sound40.loop = true;
            sound40.play();

            pick_left = game.add.sprite(94-mobileX,342-mobileY, 'pick_left');
            pick_left.visible = true;
            pick_right = game.add.sprite(94-mobileX,342-mobileY, 'pick_right');
            pick_right.visible = false;
            triger_left = game.add.sprite(94-mobileX,342-mobileY, 'triger_left');
            triger_left.visible = false;
            triger_right = game.add.sprite(94-mobileX,342-mobileY, 'triger_right');
            triger_right.visible = false;


            flick();
            if(!isMobile){
                game.add.sprite(0,0, 'main_window');
            }


            checkGame = 2;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;

            addTableTitle(game, 'winTitleGame2', 557-mobileX,115-mobileY);
            hideTableTitle();

            // кнопки и анимации событий

            //анимация вариантов выбора

            selectAnOption[1][0] = function () { //проигрышный вариант для левой кнопки
                n = 1;
                coin_val= 2;
                pick_dice = 1;
                triger_left.visible = true;
                flip_coinAnimationtails.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('tails');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[1][1] = function () { //выигрышный вариант для левой кнопки
                n = 1;
                coin_val= 1;
                triger_left.visible = true;
                pick_dice = 1;
                flip_coinAnimationeagle.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('eagle');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[2][0] = function () { //проигрышный вариант для правой кнопки
                n = 9;
                coin_val= 1;
                pick_dice = 9;
                triger_right.visible = true;
                flip_coinAnimationeagle.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('eagle');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[2][1] = function () { //проигрышный вариант для правой кнопки
                n = 9;
                coin_val= 2;
                triger_right.visible = true;
                pick_dice = 9;
                flip_coinAnimationtails.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('tails');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            buttonImage1 = game.add.sprite(45,347, 'buttonImage1');
            buttonImage2 = game.add.sprite(526,347, 'buttonImage2');

            buttonsImages[0] = buttonImage1;
            buttonsImages[1] = buttonImage2;

            group1 = game.add.group();
            group1.add(buttonImage1);
            group1.add(buttonImage2);
            game.world.bringToTop(group1);

            addButtonsGame2Mobile(game, selectAnOption, buttonsImages);


            //счет
            var step = 1;
            var inputWin = totalWinR;
            var totalWin = totalWinR;

            // inputWin, totalWin, balanceR, step
            scorePosions = [[260-mobileX, 26-mobileX, 20], [485-mobileX, 26-mobileX, 20], [640-mobileX, 26-mobileX, 20], [475-mobileX, 84-mobileX, 30]];
            addScore(game, scorePosions, inputWin, totalWin, balanceR, '');

            fontsize = 16;
            showDoubleToAndTakeOrRiskTexts(game, totalWin, 567-mobileX, 160-mobileY, fontsize);


            //задержка для отображения showDoubleToAndTakeOrRiskTexts
            lockDisplay();
            setTimeout('unlockDisplay();', 2000);

        }
    };

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================


(function () {

    var game3 = {
        arrow_arr : [],
        dolphin_arr : [],
        shark_arr : [],
        fin_arr : [],
        button : [],
        pick_line : null,
        selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
        buttonsPositionsX : {1:250-mobileX, 3:295-mobileX, 5:340-mobileX, 7:385-mobileX, 9:430-mobileX},
    };

    game3.preload = function () {};

    game3.create = function () {

        checkGame = 3;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;
        stepTotalWinR = 0;
        ropeStep = 0;
        checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса

        //звуки


        //изображения
        game3.bg_top = game.add.sprite(94-mobileX,22-mobileY, 'bg_top4');
        game3.bg = game.add.sprite(94-mobileX,22-mobileY, 'game4.bg');


        game3.arrow_arr = [];
        game3.fin_arr = [];
        for (var i = 1; i <= 5; ++i) {
            game3.arrow_arr[i] = game.add.sprite(130-mobileX+(i-1)*127, 272-mobileY, 'game4.arrow');
            game3.arrow_arr[i].animations.add('game4.arrow', [0,1,2,3,4], 6, true).play();
            if (i % 2 != 0) {
                game3.fin_arr[i] = game.add.sprite(104-mobileX+(i-1)*127, 214-mobileY, 'game4.fin');
                game3.fin_arr[i].animations.add('game4.fin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, true).play();
                game3.fin_arr[i].inputEnabled = true;
            } else {
                game3.fin_arr[i] = game.add.sprite(104-mobileX+(i-1)*127, 214-mobileY, 'game4.fin');
                game3.fin_arr[i].animations.add('game4.fin', [7,8,9,10,11,12,0,1,2,3,4,5,6], 12, true).play();
                game3.fin_arr[i].inputEnabled = true;
            }
        }

        //счет
        scorePosions = [[260-mobileX, 25-mobileY, 20], [435-mobileX, 25-mobileY, 20], [610-mobileX, 25-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
        addScore(game, scorePosions, '', '', balance, betline);



        //кнопки и анимации событий нажатия

        ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша
        buttonsGame3Mobile = [];

        winRopeNumberPosition = [[185-mobileX,152],[185-mobileX,120],[185-mobileX,88],[185-mobileX,56],[185-mobileX,24]];
        timeoutForShowWinRopeNumber = 3000;
        timeoutGame3ToGame4 = 3000;
        typeWinRopeNumberAnim = 0;
        checkHelm = false;
        winRopeNumberSize = 22;

        buttonsGame3Mobile[0] = game3.fin_arr[1];
        buttonsGame3Mobile[1] = game3.fin_arr[2];
        buttonsGame3Mobile[2] = game3.fin_arr[3];
        buttonsGame3Mobile[3] = game3.fin_arr[4];
        buttonsGame3Mobile[4] = game3.fin_arr[5];

        //добавляем в ropesAnim анимации

        ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
            n = 1;

            if(game3.freeze) {
                return;
            }

            game3.freeze = true;
            game3.pick_line = game3.selectedRopeNormal[n];

            game3.captain_pick.visible = false;
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i].visible = false;
            };
            game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_under.position.x = 53 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_swim_1.visible = true;
            game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
            game3.fin_arr[game3.pick_line].visible = false;
                game3.captain_swim_1.visible = false;
                game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127 - mobileX;
                game3.captain_swim_2.visible = true;
                game3.fin_end.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                game3.fin_end.visible = true;
                game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                    game3.fin_end.visible = false;
                    if (ropeValues[ropeStep] == 0) {
                        game3.shark.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.shark.visible = true;
                        game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                            game.state.start('game1');
                        });
                    } else {
                        game3.dolphin.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.dolphin.visible = true;
                        game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                            game3.dolphin.visible = false;
                            game3.captain_swim_2.visible = false;
                            game3.captain_pick.visible = true;
                            game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
                            game3.fin_end.visible = false;
                            game3.freeze = false;
                        });
                    }
                });
                game3.fin_end.animations.getAnimation('game4.fin_end').play();
            });
            lockDisplay();
            setTimeout('unlockDisplay()',5000);
        };
        ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
            n = 3;

            if(game3.freeze) {
                return;
            }

            game3.freeze = true;
            game3.pick_line = game3.selectedRopeNormal[n];

            game3.captain_pick.visible = false;
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i].visible = false;
            };
            game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_under.position.x = 53 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_swim_1.visible = true;
            game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
            game3.fin_arr[game3.pick_line].visible = false;
                game3.captain_swim_1.visible = false;
                game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127 - mobileX;
                game3.captain_swim_2.visible = true;
                game3.fin_end.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                game3.fin_end.visible = true;
                game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                    game3.fin_end.visible = false;
                    if (ropeValues[ropeStep] == 0) {
                        game3.shark.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.shark.visible = true;
                        game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                            game.state.start('game1');
                        });
                    } else {
                        game3.dolphin.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.dolphin.visible = true;
                        game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                            game3.dolphin.visible = false;
                            game3.captain_swim_2.visible = false;
                            game3.captain_pick.visible = true;
                            game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
                            game3.fin_end.visible = false;
                            game3.freeze = false;
                        });
                    }
                });
                game3.fin_end.animations.getAnimation('game4.fin_end').play();
            });
            lockDisplay();
            setTimeout('unlockDisplay()',5000);
        };
        ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
            n = 5;

            if(game3.freeze) {
                return;
            }

            game3.freeze = true;
            game3.pick_line = game3.selectedRopeNormal[n];

            game3.captain_pick.visible = false;
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i].visible = false;
            };
            game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_under.position.x = 53 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_swim_1.visible = true;
            game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
            game3.fin_arr[game3.pick_line].visible = false;
                game3.captain_swim_1.visible = false;
                game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127 - mobileX;
                game3.captain_swim_2.visible = true;
                game3.fin_end.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                game3.fin_end.visible = true;
                game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                    game3.fin_end.visible = false;
                    if (ropeValues[ropeStep] == 0) {
                        game3.shark.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.shark.visible = true;
                        game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                            game.state.start('game1');
                        });
                    } else {
                        game3.dolphin.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.dolphin.visible = true;
                        game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                            game3.dolphin.visible = false;
                            game3.captain_swim_2.visible = false;
                            game3.captain_pick.visible = true;
                            game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
                            game3.fin_end.visible = false;
                            game3.freeze = false;
                        });
                    }
                });
                game3.fin_end.animations.getAnimation('game4.fin_end').play();
            });
            lockDisplay();
            setTimeout('unlockDisplay()',5000);
        };
        ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
            n = 7;

            if(game3.freeze) {
                return;
            }

            game3.freeze = true;
            game3.pick_line = game3.selectedRopeNormal[n];

            game3.captain_pick.visible = false;
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i].visible = false;
            };
            game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_under.position.x = 53 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_swim_1.visible = true;
            game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
            game3.fin_arr[game3.pick_line].visible = false;
                game3.captain_swim_1.visible = false;
                game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127 - mobileX;
                game3.captain_swim_2.visible = true;
                game3.fin_end.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                game3.fin_end.visible = true;
                game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                    game3.fin_end.visible = false;
                    if (ropeValues[ropeStep] == 0) {
                        game3.shark.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.shark.visible = true;
                        game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                            game.state.start('game1');
                        });
                    } else {
                        game3.dolphin.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.dolphin.visible = true;
                        game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                            game3.dolphin.visible = false;
                            game3.captain_swim_2.visible = false;
                            game3.captain_pick.visible = true;
                            game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
                            game3.fin_end.visible = false;
                            game3.freeze = false;
                        });
                    }
                });
                game3.fin_end.animations.getAnimation('game4.fin_end').play();
            });
            lockDisplay();
            setTimeout('unlockDisplay()',5000);
        };
        ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
            n = 9;

            if(game3.freeze) {
                return;
            }

            game3.freeze = true;
            game3.pick_line = game3.selectedRopeNormal[n];

            game3.captain_pick.visible = false;
            for (var i = 1; i <= 5; ++i) {
                game3.arrow_arr[i].visible = false;
            };
            game3.captain_swim_1.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_under.position.x = 53 + (game3.pick_line-1)*127 - mobileX;
            game3.captain_swim_1.visible = true;
            game3.captain_swim_1.animations.getAnimation('game4.captain_swim_1').play().onComplete.add(function(){
            game3.fin_arr[game3.pick_line].visible = false;
                game3.captain_swim_1.visible = false;
                game3.captain_swim_2.position.x = 37 + (game3.pick_line-1)*127 - mobileX;
                game3.captain_swim_2.visible = true;
                game3.fin_end.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                game3.fin_end.visible = true;
                game3.captain_swim_2.animations.getAnimation('game4.captain_swim_2').play().onComplete.add(function(){
                    game3.fin_end.visible = false;
                    if (ropeValues[ropeStep] == 0) {
                        game3.shark.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.shark.visible = true;
                        game3.shark.animations.getAnimation('game4.shark').play().onComplete.add(function(){
                            game.state.start('game1');
                        });
                    } else {
                        game3.dolphin.position.x = 104 + (game3.pick_line-1)*127 - mobileX;
                        game3.dolphin.visible = true;
                        game3.dolphin.animations.getAnimation('game4.dolphin').play().onComplete.add(function(){
                            game3.dolphin.visible = false;
                            game3.captain_swim_2.visible = false;
                            game3.captain_pick.visible = true;
                            game3.captain_pick.position.x = 101 + (game3.pick_line-1)*127 - mobileX;
                            game3.fin_end.visible = false;
                            game3.freeze = false;
                        });
                    }
                });
                game3.fin_end.animations.getAnimation('game4.fin_end').play();
            });
            lockDisplay();
            setTimeout('unlockDisplay()',5000);
        };



        //анимации

        game3.water = game.add.sprite(94-mobileX, 256-mobileY, 'game4.water');
        game3.water.animations.add('game4.water', [0,1,2,3,4,5], 12, true).play();
        game3.dolphin = game.add.sprite(104-mobileX, 170-mobileY, 'game4.dolphin');
        game3.dolphin.animations.add('game4.dolphin', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);
        game3.dolphin.visible = false;
        game3.shark = game.add.sprite(130-mobileX, 170-mobileY, 'game4.shark');
        game3.shark.animations.add('game4.shark', [0,1,2,3,4,5,6,7,8,9,10,11,12,12,12,12,12,12], 8, false);
        game3.shark.visible = false;
        game3.captain_lose = game.add.sprite(355-mobileX, 340-mobileY, 'game4.captain_lose');
        game3.captain_lose.animations.add('game4.captain_lose', [0,1,2,3,4,5,6,7,8,9], 8, false);
        game3.captain_lose.visible = false;
        game3.captain_pick = game.add.sprite(355-mobileX, 327-mobileY, 'game4.captain_pick');
        game3.captain_pick.animations.add('game4.captain_pick', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,12,11,10,9,8,8,8,8,14,15,16,17,18,19,20,21,22,23,24,0,0,0], 8, true).play();
        game3.captain_swim_1 = game.add.sprite(355-mobileX, 327-mobileY, 'game4.captain_swim_1');
        game3.captain_swim_1.animations.add('game4.captain_swim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 10, false);
        game3.captain_swim_1.visible = false;
        game3.captain_win = game.add.sprite(355-mobileX, 327-mobileY, 'game4.captain_win');
        game3.captain_win.animations.add('game4.captain_win', [0,1,2,3,4], 8, false);
        game3.captain_win.visible = false;
        game3.fin_left_end = game.add.sprite(104-mobileX, 199-mobileY, 'game4.fin_left_end');
        game3.fin_left_end.animations.add('game4.fin_left_end', [0,1,2,3,4,5], 12, false);
        game3.fin_left_end.visible = false;
        game3.fin_right_end = game.add.sprite(104-mobileX, 199-mobileY, 'game4.fin_right_end');
        game3.fin_right_end.animations.add('game4.fin_right_end', [0,1,2,3,4,5,6], 12, false);
        game3.fin_right_end.visible = false;
        game3.fin_end = game.add.sprite(104-mobileX, 214-mobileY, 'game4.fin_end');
        game3.fin_end.animations.add('game4.fin_end', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, false);
        game3.fin_end.visible = false;
        game3.captain_under = game.add.sprite(307-mobileX, 407-mobileY, 'game4.captain_under');
        game3.captain_under.animations.add('game4.captain_under', [0,1,2,3,4,5], 8, true).play();
        game3.captain_swim_2 = game.add.sprite(37-mobileX, 327-mobileY, 'game4.captain_swim_2');
        game3.captain_swim_2.animations.add('game4.captain_swim_2', [0,1,2,3,4,5,0,1,2,3,4,5,0], 12, false);
        game3.captain_swim_2.visible = false;


        addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);



    };

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================
(function () {
    var game4 = {
        freeze : false,
        climb : false,
        buttonsPositionsX : {1:250-mobileX, 3:295-mobileX, 5:340-mobileX, 7:385-mobileX, 9:430-mobileX},
        ropesafeCount : 5,
        menPositionsX : {1:94, 3:222, 5:350, 7:478, 9:606},
        selectedRopeNormal : {1:1, 3:2, 5:3, 7:4, 9:5},
        barrel1: [3,4,5,9],
        barrel2: [1,2,6,7,8,10],
        baller_open : {1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0},
        selectedSafe : null,
        safe_hit_arr : [],
        barrel_arr : [],
        barrel_arr_open : null,
        barrel_top_arr : null,
        barrel_lose_arr : null,
        barrel_d_arr : null,
        spin_dice : true,
        deck1_val1 : null,
        deck1_val2 : null,
        dice_arr : [],
        dice_d_arr : [],
        pick_dice : null,
        bars_dice: [],
        currentbarrel : 10,
        stone_arr : [],
        hook_arr : [],
        blue_screen_arr : [],
        hookfinal_arr : [],
        bottom_bg_arr : [],
        safe_door_arr : [],
        safe_door_arr_anim : [],
        barrel_arr_i : null,
        tween1: null,
        tween2: null,
        tween3: null,
        tween4: null,
        bg_main : null,
        floor: 1,
        selectedRope : null,
        button : [],
        push_baller : [],
        currentBar : 1,
        bars_line: [],
        barsCurrentSpins : [0, 0, 0],
        barsTotalSpins : [27, 51, 75],
        countPlayBars : 0,
        bar_arr: [],
        bar_arr_w: [],
        baller_count : null,
        first_spin : true,
        meat_arr: [],
        pick_meat: null,

        create : function() {

            stopSound = game.add.audio('stop');
            rotateSound = game.add.audio('rotate');

            game4.bg = game.add.sprite(94-mobileX,54-mobileY, 'game3.bg');
            for (var i = 0; i <= 1; ++i) {
                game4.meat_arr[i] = game.add.sprite(260-mobileX+i*256, 54-mobileY, 'game3.meat');
                game4.meat_arr[i].animations.add('game3.meat', [0,1,2,3,4,5,6,7,8,9,10,11,10,9,8,7,6,5,4,3,2,1], 8, true).play();
                game4.meat_arr[i].inputEnabled = true;
            }
            if(!isMobile){
                game.add.sprite(0,0, 'main_window');
            }

            //
//game3.barrel_lose_arr.animations.getAnimation('barrel_lose').stop();
            //
            game4.shadow1 = game.add.sprite(286-mobileX, 374-mobileY, 'shadow1');
            game4.shadow2 = game.add.sprite(94-mobileX, 374-mobileY, 'shadow2');
            // game3.shadow2.visible = false;
            // game3.shadow3 = game.add.sprite(286+mobileX, 374+mobileY, 'shadow3');
            // game3.shadow3.visible = false;

            game4.pick = game.add.sprite(342-mobileX, 198-mobileY, 'game3.pick');
            game4.pick.animations.add('game3.pick', [0,1,2,3,4,5,6,5,6,5,6,4,3,2,1,0,0,0,7,8,9,10,11,12,11,12,11,12,10,9,8,7,0,0,0], 8, true).play();


            game4.bg_top = game.add.sprite(94-mobileX,22-mobileY, 'bg_top3');

            game4.countPlayBars = 3;
            game4.spinStatus = true;



            //кнопки и анимации событий нажатия

            //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
            selectionsAmin = [];
            timeout1Game4ToGame1 = 4000;
            timeout2Game4ToGame1 = 3000;


            // анимация для кнопки 3
            selectionsAmin[0] = function (ropeValues, ropeStep) {

                game4.get_meat_star = game.add.sprite(132-mobileX, 43-mobileY, 'game3.get_meat_star');
                game4.get_meat_star.animations.add('game3.get_meat_star', [0,1,2,3,4,5], 8, false);
                game4.get_meat_star.visible = false;
                game4.get_meat_win = game.add.sprite(132-mobileX, 43-mobileY, 'game3.get_meat_win');
                game4.get_meat_win.animations.add('game3.get_meat_win', [0,1,2], 8, false);
                game4.get_meat_win.visible = false;

                game4.freeze = true;
                game4.pick.visible = false;
                game4.get_meat_star.visible = true;

                game4.get_meat_lose_1 = game.add.sprite(132-mobileX, 43-mobileY, 'game3.get_meat_lose_1');
                game4.get_meat_lose_1.animations.add('game3.get_meat_lose_1', [0,1,2], 8, false);
                game4.get_meat_lose_1.visible = false;
                game4.get_meat_lose_2 = game.add.sprite(132-mobileX, 43-mobileY, 'game3.get_meat_lose_2');
                game4.get_meat_lose_2.animations.add('game3.get_meat_lose_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false);
                game4.get_meat_lose_2.visible = false;

                game4.get_meat_star.animations.getAnimation('game3.get_meat_star').onComplete.add(function(){
                    game4.get_meat_star.visible = false ;
                    if (game4.pick_meat == 1) {
                        game4.get_meat_win.visible = true;
                        game4.get_meat_win.animations.getAnimation('game3.get_meat_win').play();
                    } else {
                        game4.get_meat_lose_1.visible = true;
                        game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').play();
                    }
                });
                game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').onComplete.add(function(){
                    game4.get_meat_lose_1.visible = false ;
                    game4.get_meat_lose_2.visible = true;
                    game4.get_meat_lose_2.animations.getAnimation('game3.get_meat_lose_2').play();
                });

                if(ropeValues[5] != 0){
                    game4.pick_meat = 1;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[0].visible = false;
                } else {
                    game4.pick_meat = 0;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[0].visible = false;
                }
            };

            // анимация для кнопки 7
            selectionsAmin[1] = function (ropeValues, ropeStep) {

                game4.get_meat_star = game.add.sprite(388-mobileX, 43-mobileY, 'game3.get_meat_star');
                game4.get_meat_star.animations.add('game3.get_meat_star', [0,1,2,3,4,5], 8, false);
                game4.get_meat_star.visible = false;
                game4.get_meat_win = game.add.sprite(388-mobileX, 43-mobileY, 'game3.get_meat_win');
                game4.get_meat_win.animations.add('game3.get_meat_win', [0,1,2], 8, false);
                game4.get_meat_win.visible = false;

                game4.freeze = true;
                game4.pick.visible = false;
                game4.get_meat_star.visible = true;

                game4.get_meat_lose_1 = game.add.sprite(388-mobileX, 43-mobileY, 'game3.get_meat_lose_1');
                game4.get_meat_lose_1.animations.add('game3.get_meat_lose_1', [0,1,2], 8, false);
                game4.get_meat_lose_1.visible = false;
                game4.get_meat_lose_2 = game.add.sprite(388-mobileX, 43-mobileY, 'game3.get_meat_lose_2');
                game4.get_meat_lose_2.animations.add('game3.get_meat_lose_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 8, false);
                game4.get_meat_lose_2.visible = false;

                game4.get_meat_star.animations.getAnimation('game3.get_meat_star').onComplete.add(function(){
                    game4.get_meat_star.visible = false ;
                    if (game4.pick_meat == 1) {
                        game4.get_meat_win.visible = true;
                        game4.get_meat_win.animations.getAnimation('game3.get_meat_win').play();
                    } else {
                        game4.get_meat_lose_1.visible = true;
                        game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').play();
                    }
                });
                game4.get_meat_lose_1.animations.getAnimation('game3.get_meat_lose_1').onComplete.add(function(){
                    game4.get_meat_lose_1.visible = false ;
                    game4.get_meat_lose_2.visible = true;
                    game4.get_meat_lose_2.animations.getAnimation('game3.get_meat_lose_2').play();
                });

                if(ropeValues[5] != 0){
                    game4.pick_meat = 1;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[1].visible = false;
                } else {
                    game4.pick_meat = 0;
                    game4.get_meat_star.animations.getAnimation('game3.get_meat_star').play();
                    game4.meat_arr[1].visible = false;
                }
            };

            buttonsGame4Mobile[0] = game4.meat_arr[0];
            buttonsGame4Mobile[1] = game4.meat_arr[1];

            addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile, timeout1Game4ToGame1, timeout2Game4ToGame1)

            //счет
            scorePosions = [[260-mobileX, 26-mobileY, 20], [435-mobileX, 26-mobileY, 20], [610-mobileX, 26-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
            addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);


        }
    };
    game.state.add('game4', game4);


})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('startButton', 'img/mobileButtons/spin.png');
        game.load.image('startButton_p', 'img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', 'img/mobileButtons/spin_d.png');
        game.load.image('bet1', 'img/mobileButtons/bet1.png');
        game.load.image('bet1_p', 'img/mobileButtons/bet1_p.png');
        game.load.image('home', 'img/mobileButtons/home.png');
        game.load.image('home_p', 'img/mobileButtons/home_p.png');
        game.load.image('dollar', 'img/mobileButtons/dollar.png');
        game.load.image('gear', 'img/mobileButtons/gear.png');
        game.load.image('double', 'img/mobileButtons/double.png');
        game.load.image('collect', 'img/mobileButtons/collect.png');
        game.load.image('collect_p', 'img/mobileButtons/collect_p.png');
        game.load.image('collect_d', 'img/mobileButtons/collect_d.png');

        game.load.image('game.number1', 'img/1.png');
        game.load.image('game.number2', 'img/2.png');
        game.load.image('game.number3', 'img/3.png');
        game.load.image('game.number4', 'img/4.png');
        game.load.image('game.number5', 'img/5.png');
        game.load.image('game.number6', 'img/6.png');
        game.load.image('game.number7', 'img/7.png');
        game.load.image('game.number8', 'img/8.png');
        game.load.image('game.number9', 'img/9.png');

        game.load.image('game.non_full','img/full.png');
        game.load.image('game.full','img/non_full.png');
        game.load.image('sound_on', 'img/sound_on.png');
        game.load.image('sound_off', 'img/sound_off.png');

        game.load.image('line1', 'lines/select/1.png');
        game.load.image('line2', 'lines/select/2.png');
        game.load.image('line3', 'lines/select/3.png');
        game.load.image('line4', 'lines/select/4.png');
        game.load.image('line5', 'lines/select/5.png');
        game.load.image('line6', 'lines/select/6.png');
        game.load.image('line7', 'lines/select/7.png');
        game.load.image('line8', 'lines/select/8.png');
        game.load.image('line9', 'lines/select/9.png');

        game.load.image('linefull1', 'lines/win/1.png');
        game.load.image('linefull2', 'lines/win/2.png');
        game.load.image('linefull3', 'lines/win/3.png');
        game.load.image('linefull4', 'lines/win/4.png');
        game.load.image('linefull5', 'lines/win/5.png');
        game.load.image('linefull6', 'lines/win/6.png');
        game.load.image('linefull7', 'lines/win/7.png');
        game.load.image('linefull8', 'lines/win/8.png');
        game.load.image('linefull9', 'lines/win/9.png');

        game.load.audio('line1Sound', 'lines/sounds/line1.wav');
        game.load.audio('line3Sound', 'lines/sounds/line3.wav');
        game.load.audio('line5Sound', 'lines/sounds/line5.wav');
        game.load.audio('line7Sound', 'lines/sounds/line7.wav');
        game.load.audio('line9Sound', 'lines/sounds/line9.wav');

        game.load.audio('sound', 'sound/spin.mp3');
        game.load.audio('rotateSound', 'sound/rotate.wav');
        game.load.audio('stopSound', 'sound/stop.wav');
        game.load.audio('tada', 'sound/tada.wav');
        game.load.audio('play', 'sound/play.mp3');
        game.load.audio('takeWin', 'sound/takeWin.mp3');
        game.load.audio('game.winCards', 'sound/sound30.mp3');

        game.load.audio('soundWinLine8', 'sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', 'sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', 'sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', 'sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', 'sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', 'sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', 'sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', 'sound/winLines/sound19.mp3');

        game.load.audio('page', 'sounds/page.mp3');
        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');
        for (var i = 1; i <= 5; ++i) {
            game.load.image('pagePaytable_' + i, 'img/page_' + i + '.png');            
        }

        //карты
        game.load.image('card_bg', 'img/shirt_cards.png');

        game.load.image('card_39', 'img/cards/2b.png');
        game.load.image('card_40', 'img/cards/3b.png');
        game.load.image('card_41', 'img/cards/4b.png');
        game.load.image('card_42', 'img/cards/5b.png');
        game.load.image('card_43', 'img/cards/6b.png');
        game.load.image('card_44', 'img/cards/7b.png');
        game.load.image('card_45', 'img/cards/8b.png');
        game.load.image('card_46', 'img/cards/9b.png');
        game.load.image('card_47', 'img/cards/10b.png');
        game.load.image('card_48', 'img/cards/11b.png');
        game.load.image('card_49', 'img/cards/12b.png');
        game.load.image('card_50', 'img/cards/13b.png');
        game.load.image('card_51', 'img/cards/14b.png');

        game.load.image('card_26', 'img/cards/2c.png');
        game.load.image('card_27', 'img/cards/3c.png');
        game.load.image('card_28', 'img/cards/4c.png');
        game.load.image('card_29', 'img/cards/5c.png');
        game.load.image('card_30', 'img/cards/6c.png');
        game.load.image('card_31', 'img/cards/7c.png');
        game.load.image('card_32', 'img/cards/8c.png');
        game.load.image('card_33', 'img/cards/9c.png');
        game.load.image('card_34', 'img/cards/10c.png');
        game.load.image('card_35', 'img/cards/11c.png');
        game.load.image('card_36', 'img/cards/12c.png');
        game.load.image('card_37', 'img/cards/13c.png');
        game.load.image('card_38', 'img/cards/14c.png');

        game.load.image('card_0', 'img/cards/2t.png');
        game.load.image('card_1', 'img/cards/3t.png');
        game.load.image('card_2', 'img/cards/4t.png');
        game.load.image('card_3', 'img/cards/5t.png');
        game.load.image('card_4', 'img/cards/6t.png');
        game.load.image('card_5', 'img/cards/7t.png');
        game.load.image('card_6', 'img/cards/8t.png');
        game.load.image('card_7', 'img/cards/9t.png');
        game.load.image('card_8', 'img/cards/10t.png');
        game.load.image('card_9', 'img/cards/11t.png');
        game.load.image('card_10', 'img/cards/12t.png');
        game.load.image('card_11', 'img/cards/13t.png');
        game.load.image('card_12', 'img/cards/14t.png');

        game.load.image('card_13', 'img/cards/2p.png');
        game.load.image('card_14', 'img/cards/3p.png');
        game.load.image('card_15', 'img/cards/4p.png');
        game.load.image('card_16', 'img/cards/5p.png');
        game.load.image('card_17', 'img/cards/6p.png');
        game.load.image('card_18', 'img/cards/7p.png');
        game.load.image('card_19', 'img/cards/8p.png');
        game.load.image('card_20', 'img/cards/9p.png');
        game.load.image('card_21', 'img/cards/10p.png');
        game.load.image('card_22', 'img/cards/11p.png');
        game.load.image('card_23', 'img/cards/12p.png');
        game.load.image('card_24', 'img/cards/13p.png');
        game.load.image('card_25', 'img/cards/14p.png');

        game.load.image('card_52', 'img/cards/joker.png');

        //game.load.image('pick', 'img/cards/shape340.png');

        game.load.image('cell0', 'img/cell0.png');
        game.load.image('cell1', 'img/cell1.png');
        game.load.image('cell2', 'img/cell2.png');
        game.load.image('cell3', 'img/cell3.png');
        game.load.image('cell4', 'img/cell4.png');
        game.load.image('cell5', 'img/cell5.png');
        game.load.image('cell6', 'img/cell6.png');
        game.load.image('cell7', 'img/cell7.png');
        game.load.image('cell8', 'img/cell8.png');

        game.load.spritesheet('cellAnim', 'img/cellAnim.png', 96, 112);
        game.load.spritesheet('selectionOfTheManyCellAnim', 'img/bonus.png', 96, 96); //текущее кол-во кадров = 11

        game.load.image('play1To', 'img/play1To.png');
        game.load.image('bonusGame', 'img/bonusGame.png');
        game.load.image('takeOrRisk1', 'img/takeOrRisk1.png');
        game.load.image('take', 'img/take.png');

        game.load.image('winTitleGame2', 'img/winTitleGame2.png');
        game.load.image('loseTitleGame2', 'img/loseTitleGame2.png');
        game.load.image('forwadTitleGame2', 'img/forwadTitleGame2.png');

        game.load.image('topBarType1', 'img/topBarType1.png');
        game.load.image('topBarType2', 'img/topBarType2.png');
        game.load.image('game.background', 'img/canvas-bg.svg');

        game.load.audio('winMonkey', 'sound/winMonkey.mp3');
        game.load.audio('openCard', 'sound/sound31.mp3');
        game.load.audio('winCard', 'sound/sound30.mp3');


        /* sources */

        if(!isMobile){
            game.load.image('main_window', 'img/shape1206.svg');
        }
        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');
        game.load.image('game3.bg', 'img/game3_bg.png');
        game.load.image('game4.bg', 'img/bg_game4.png');
        game.load.image('bg_top1', 'img/main_bg_top1.png');
        game.load.image('bg_top2', 'img/main_bg_top2.png');
        game.load.image('bg_top3', 'img/main_bg_top3.png');
        game.load.image('bg_top4', 'img/main_bg_top4.png');
        game.load.image('bg_top3_2', 'img/main_bg_top3_2.png');
        game.load.image('bg_3_2', 'img/bg_3_2.png');
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('x', 'img/x.png');
        for (var i = 1; i <= 6; ++i) {
            game.load.image('game1.page_' + i, 'img/page_' + i + '.png');
        }

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');

        game.load.spritesheet('bonus', 'img/bonus.png', 96, 96, 8);
        game.load.spritesheet('game1.pirate_win', 'img/pirate_win.png', 224, 144, 18);

        game.load.spritesheet('bird_anim_1', 'img/bird_anim_1_80x96_45.png', 80, 96, 45);
        game.load.spritesheet('bird_anim_2', 'img/bird_anim_2_80x96_22.png', 80, 96, 22);
        game.load.spritesheet('water_anim', 'img/water_anim_192x16_4.png', 192, 16, 4);
        game.load.spritesheet('water_bottom_1', 'img/water_bottom_1_368x32_10.png', 368, 32, 10);
        game.load.spritesheet('water_bottom_2', 'img/water_bottom_2_368x32_4.png', 368, 32, 4);
        game.load.spritesheet('game1.man_1', 'img/game1.man_1_368_112_11.png', 368, 112, 11);
        game.load.spritesheet('game1.win', 'img/game1.win_368_112_6.png', 368, 112, 6);

        game.load.image('game1.bar', 'img/shape_fullv3.png');
        game.load.image('dice_spin', 'img/dice_spin.png');
        game.load.image('border_card', 'img/border.png');

        game.load.image('shirt_cards', 'img/shirt_cards.png');
        // game.load.image('dealer', 'img/dealer.png');
        game.load.image('pick_card', 'img/pick_game2.png');
        game.load.image('card_15', 'img/cards/joker.png');
        game.load.image('card_2b', 'img/cards/2b.png');
        game.load.image('card_5', 'img/cards/5b.png');
        game.load.image('card_2c', 'img/cards/2c.png');
        game.load.image('card_7', 'img/cards/7c.png');
        game.load.image('card_2p', 'img/cards/2p.png');
        game.load.image('card_12', 'img/cards/12p.png');
        game.load.image('card_2t', 'img/cards/2t.png');
        game.load.image('card_13', 'img/cards/13t.png');

        game.load.image('shadow1', 'img/shadow1.png');
        game.load.image('shadow2', 'img/shadow2.png');
        game.load.image('shadow3', 'img/shadow3.png');
        game.load.spritesheet('game3.meat', 'img/meat_64x256_12.png', 64, 256, 12);
        game.load.spritesheet('game3.pick', 'img/game3.pick_192x256_13.png', 192, 256, 13);
        game.load.spritesheet('game3.get_meat_star', 'img/get_meat_star_192x416_6.png', 192, 416, 6);
        game.load.spritesheet('game3.get_meat_win', 'img/get_meat_win_192x416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_1', 'img/get_meat_lose_1_192_416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_2', 'img/get_meat_lose_2_240x432_16.png', 240, 432, 16);

        game.load.spritesheet('game4.arrow', 'img/arrow_64x48_5.png', 64, 48, 5);
        game.load.spritesheet('game4.captain_lose', 'img/captain_lose_128x112_10.png', 128, 112, 10);
        game.load.spritesheet('game4.captain_pick', 'img/captain_pick_128x112_25.png', 128, 112, 25);
        game.load.spritesheet('game4.captain_swim_1', 'img/captain_swim_1_128x112_19.png', 128, 112, 19);
        game.load.spritesheet('game4.captain_swim_2', 'img/captain_swim_2_256x160_6.png', 256, 160, 6);
        game.load.spritesheet('game4.captain_under', 'img/captain_under_224x80_6.png', 224, 80, 6);
        game.load.spritesheet('game4.captain_win', 'img/captain_win_128x112_5.png', 128, 112, 5);
        game.load.spritesheet('game4.dolphin', 'img/dolphin_128х128_13.png', 128, 128, 13);
        game.load.spritesheet('game4.shark', 'img/shark_128х144_13.png', 128, 144, 13);
        game.load.spritesheet('game4.fin', 'img/fin_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.fin_left_end', 'img/fin_left_end_128x64_6.png', 128, 64, 6);
        game.load.spritesheet('game4.fin_right_end', 'img/fin_right_end_128x64_7.png', 128, 64, 7);
        game.load.spritesheet('game4.fin_end', 'img/fin_left_end_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.water', 'img/water_640x224_6.png', 640, 224, 6);

        game.load.spritesheet('game2.win_lose', 'img/game2_win_lose.png', 112, 96, 5);

        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('pickCard', 'sounds/pickCard.mp3');
        game.load.audio('cardWin', 'sounds/cardWin.mp3');
        game.load.audio('hit', 'sounds/hit.mp3');
        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('rowWin', 'sounds/rowWin.mp3');
        game.load.audio('page', 'sounds/page.mp3');

        game.load.audio('rotate', 'sounds/rotate.wav');

        game.load.image('winTryInGame3', 'img/winTryInGame3.png');

        game.load.image('game1.bg', 'img/main_bg.png');
        game.load.image('game2.bg', 'img/bg_game2.png');
        game.load.image('game3.bg', 'img/game3_bg.png');
        game.load.image('game4.bg', 'img/bg_game4.png');
        game.load.image('bg_top1', 'img/main_bg_top1.png');
        game.load.image('bg_top2', 'img/main_bg_top2.png');
        game.load.image('bg_top3', 'img/main_bg_top3.png');
        game.load.image('bg_top4', 'img/main_bg_top4.png');
        game.load.image('bg_top3_2', 'img/main_bg_top3_2.png');
        game.load.image('bg_3_2', 'img/bg_3_2.png');
        game.load.image('game1.bottom_line', 'img/bottom_line.png');
        game.load.image('x', 'img/x.png');

        game.load.image('prev_page', 'img/prev_page.png');
        game.load.image('exit_btn', 'img/exit_btn.png');
        game.load.image('next_page', 'img/next_page.png');

        game.load.spritesheet('bonus', 'img/bonus.png', 96, 96, 8);
        game.load.spritesheet('game1.pirate_win', 'img/pirate_win.png', 224, 144, 18);

        game.load.spritesheet('bird_anim_1', 'img/bird_anim_1_80x96_45.png', 80, 96, 45);
        game.load.spritesheet('bird_anim_2', 'img/bird_anim_2_80x96_22.png', 80, 96, 22);
        game.load.spritesheet('water_anim', 'img/water_anim_192x16_4.png', 192, 16, 4);
        game.load.spritesheet('water_bottom_1', 'img/water_bottom_1_368x32_10.png', 368, 32, 10);
        game.load.spritesheet('water_bottom_2', 'img/water_bottom_2_368x32_4.png', 368, 32, 4);
        game.load.spritesheet('game1.man_1', 'img/game1.man_1_368_112_11.png', 368, 112, 11);
        game.load.spritesheet('game1.win', 'img/game1.win_368_112_6.png', 368, 112, 6);

        game.load.image('game1.bar', 'img/shape_fullv3.png');
        game.load.image('dice_spin', 'img/dice_spin.png');
        game.load.image('border_card', 'img/border.png');

        game.load.image('pick_left', 'img/pick_left.png');
        game.load.image('pick_right', 'img/pick_right.png');
        game.load.image('triger_left', 'img/triger_left.png');
        game.load.image('triger_right', 'img/triger_right.png');
        game.load.image('game2.win', 'img/game2.win.png');
        game.load.image('game2.loss', 'img/game2.loss.png');
        game.load.image('left_pick', 'img/left_pick.png');
        game.load.image('right_pick', 'img/right_pick.png');
        game.load.image('eagle', 'img/eagle.png');
        game.load.image('tails', 'img/tails.png');
        game.load.spritesheet('game2.flip_coin', 'img/flip_coin_192x352_11.png', 208, 352, 11);

        game.load.image('shadow1', 'img/shadow1.png');
        game.load.image('shadow2', 'img/shadow2.png');
        game.load.image('shadow3', 'img/shadow3.png');
        game.load.spritesheet('game3.meat', 'img/meat_64x256_12.png', 64, 256, 12);
        game.load.spritesheet('game3.pick', 'img/game3.pick_192x256_13.png', 192, 256, 13);
        game.load.spritesheet('game3.get_meat_star', 'img/get_meat_star_192x416_6.png', 192, 416, 6);
        game.load.spritesheet('game3.get_meat_win', 'img/get_meat_win_192x416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_1', 'img/get_meat_lose_1_192_416_3.png', 192, 416, 3);
        game.load.spritesheet('game3.get_meat_lose_2', 'img/get_meat_lose_2_240x432_16.png', 240, 432, 16);

        game.load.spritesheet('game4.arrow', 'img/arrow_64x48_5.png', 64, 48, 5);
        game.load.spritesheet('game4.captain_lose', 'img/captain_lose_128x112_10.png', 128, 112, 10);
        game.load.spritesheet('game4.captain_pick', 'img/captain_pick_128x112_25.png', 128, 112, 25);
        game.load.spritesheet('game4.captain_swim_1', 'img/captain_swim_1_128x112_19.png', 128, 112, 19);
        game.load.spritesheet('game4.captain_swim_2', 'img/captain_swim_2_256x160_6.png', 256, 160, 6);
        game.load.spritesheet('game4.captain_under', 'img/captain_under_224x80_6.png', 224, 80, 6);
        game.load.spritesheet('game4.captain_win', 'img/captain_win_128x112_5.png', 128, 112, 5);
        game.load.spritesheet('game4.dolphin', 'img/dolphin_128х128_13.png', 128, 128, 13);
        game.load.spritesheet('game4.shark', 'img/shark_128х144_13.png', 128, 144, 13);
        game.load.spritesheet('game4.fin', 'img/fin_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.fin_left_end', 'img/fin_left_end_128x64_6.png', 128, 64, 6);
        game.load.spritesheet('game4.fin_right_end', 'img/fin_right_end_128x64_7.png', 128, 64, 7);
        game.load.spritesheet('game4.fin_end', 'img/fin_left_end_128x64_13.png', 128, 64, 13);
        game.load.spritesheet('game4.water', 'img/water_640x224_6.png', 640, 224, 6);

        game.load.spritesheet('game2.win_lose', 'img/game2_win_lose.png', 112, 96, 5);

        game.load.audio('stop', 'sounds/stop.wav');
        game.load.audio('rotate', 'sounds/rotate.wav');
        game.load.audio('tada', 'sounds/tada.wav');
        game.load.audio('takeWin', 'sounds/takeWin.mp3');
        game.load.audio('pickCard', 'sounds/pickCard.mp3');
        game.load.audio('cardWin', 'sounds/cardWin.mp3');
        game.load.audio('hit', 'sounds/hit.mp3');
        game.load.audio('winGame3', 'sounds/winGame3.mp3');
        game.load.audio('rowWin', 'sounds/rowWin.mp3');
        game.load.audio('page', 'sounds/page.mp3');

        game.load.image('buttonImage1', 'img/buttonImage1.png');
        game.load.image('buttonImage2', 'img/buttonImage2.png');

        game.load.audio('7_sound40', 'sounds/7_sound40.mp3');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

