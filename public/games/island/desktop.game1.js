var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

isMobile = false;
mobileX = 0;
mobileY = 0;

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        
        
        // звуки

        var playSound = game.add.audio('play');

        // изображения
        topBarImage = game.add.sprite(93,23, 'topBarType2');
        game.add.sprite(0,0, 'game.background');
        game1.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top1');
        game1.bg = game.add.sprite(94+mobileX,54+mobileY, 'game1.bg');

        
        slotPosition = [[142, 88], [142, 200], [142, 312], [254, 88], [254, 200], [254, 312], [366, 88], [366, 200], [366, 312], [478, 88], [478, 200], [478, 312], [590, 88], [590, 200], [590, 312]];
        addSlots(game, slotPosition);

        addTableTitle(game, 'play1To',557,422);



        var linePosition = [[134,199+50], [134,71+33], [134,322+64], [134,130+33], [134,95+34], [134,102+33], [134,228+34], [134,226+50], [134,120+34]];
        var numberPosition = [[93,183+50], [93,54+33], [93,310+64], [93,118+33], [93,246+64], [93,86+33], [93,278+64], [93,214+50], [93,150+50]];
        addLinesAndNumbers(game, linePosition, numberPosition);

        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);


        var pageCount = 5;

        // кнопки
        addButtonsGame1(game, pageCount);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230, 26, 20], [435, 26, 20], [610, 26, 20], [99, 59, 16], [707, 59, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        game1.strawberry_bar_anim_1 = game.add.sprite(142+mobileX, 88+mobileY, 'bonus');
        game1.strawberry_bar_anim_1Animation = game1.strawberry_bar_anim_1.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_1.visible = false;
        game1.strawberry_bar_anim_2 = game.add.sprite(254+mobileX, 88+mobileY, 'bonus');
        game1.strawberry_bar_anim_2Animation = game1.strawberry_bar_anim_2.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_2.visible = false;
        game1.strawberry_bar_anim_3 = game.add.sprite(366+mobileX, 88+mobileY, 'bonus');
        game1.strawberry_bar_anim_3Animation = game1.strawberry_bar_anim_3.animations.add('bonus', [0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6,0,1,2,3,4,5,6], 8, false);
        game1.strawberry_bar_anim_3.visible = false;
        game1.water_anim = game.add.sprite(542+mobileX, 486+mobileY, 'water_anim');
        game1.water_animAnimation = game1.water_anim.animations.add('water_anim', [0,1,2,3], 8, true);
        game1.water_animAnimation.play();
        game1.water_bottom_1 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_1');
        game1.water_bottom_1Animation = game1.water_bottom_1.animations.add('water_bottom_1', [0,1,2,3,4,5,6,7,8,9], 8, false);
        game1.water_bottom_1Animation.play();
        game1.water_bottom_2 = game.add.sprite(94+mobileX, 470+mobileY, 'water_bottom_2');
        game1.water_bottom_2Animation = game1.water_bottom_2.animations.add('water_bottom_2', [0,1,2,3], 8, false);
        game1.water_bottom_2.visible = false;
        game1.man_1 = game.add.sprite(94+mobileX, 390+mobileY, 'game1.man_1');
        game1.man_1Animation = game1.man_1.animations.add('game1.man_1', [0,1,2,3,4,5,6,7,8,9,10], 8, true);
        game1.man_1Animation.play();
        game1.win = game.add.sprite(94+mobileX, 390+mobileY, 'game1.win');
        game1.winAnimation = game1.win.animations.add('game1.win', [0,1,2,3,4,5], 8, false);
        game1.win.visible = false;
        game1.bird_anim_1 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_1');
        game1.bird_anim_1Animation = game1.bird_anim_1.animations.add('bird_anim_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44], 8, false);
        game1.bird_anim_1Animation.play();

        game1.bird_anim_2 = game.add.sprite(462+mobileX, 406+mobileY, 'bird_anim_2');
        game1.bird_anim_2Animation = game1.bird_anim_2.animations.add('bird_anim_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
        game1.bird_anim_2.visible = false;
        game1.winAnimation.onComplete.add(function(){
            game1.win.visible = false;
        });
        game1.bird_anim_1Animation.onComplete.add(function(){
            game1.bird_anim_1.visible = false;
            game1.bird_anim_2.visible = true;
            game1.bird_anim_2Animation.play();
        });
        game1.bird_anim_2Animation.onComplete.add(function(){
            game1.bird_anim_1.visible = true;
            game1.bird_anim_2.visible = false;
            game1.bird_anim_1Animation.play();
        });
        game1.water_bottom_1Animation.onComplete.add(function(){
            game1.water_bottom_1.visible = false;
            game1.water_bottom_2.visible = true;
            game1.water_bottom_2Animation.play();
        });
        game1.water_bottom_2Animation.onComplete.add(function(){
            game1.water_bottom_1.visible = true;
            game1.water_bottom_2.visible = false;
            game1.water_bottom_1Animation.play();
        });

        var pageCoord = [[94, 22], [94, 22], [94, 22], [94, 22], [94, 22]];   
        var btnCoord = [[107, 442], [312, 442], [521, 442]];   
        //pageCount, pageCoord, btnCoord
        addPaytable(pageCount, pageCoord, btnCoord);
        
        full_and_sound();
    };



    game1.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game1', game1);

};
