var selectAnOption = [[0,0], [0,0], [0,0]];

var sound40;
var flicker;

var pick_left;
var pick_right;

function restartGame() {
    if(typeof(triger_right) != undefined) {
        triger_right.visible = false;
    } else {
        triger_left.visible = false;
    }

    flip_coin.visible = false;

    flip_coin = game.add.sprite(302-mobileX, 102-mobileY, 'game2.flip_coin');
    flip_coinAnimationeagle = flip_coin.animations.add('game2.flip_coin_eagle', [0,1,2,3,4,5,6,7,8,9], 12, false);
    flip_coinAnimationtails = flip_coin.animations.add('game2.flip_coin_tails', [0,1,2,3,4,5,6,7,8,10], 12, false);

    pick_left = game.add.sprite(94-mobileX,342-mobileY, 'pick_left');
    pick_left.visible = true;
    pick_right = game.add.sprite(94-mobileX,342-mobileY, 'pick_right');
    pick_right.visible = false;

    flick();
}

function game2() {
    var game2 = {
        button : [],
        pick_dice : null,
        pick : [],
        flicker : false,
        create : function() {

            game2.bg_top = game.add.sprite(94+mobileX,22+mobileY, 'bg_top1');
            game.add.sprite(94+mobileX,22+mobileY, 'bg_top2');
            game2.bg2 = game.add.sprite(94+mobileX,54+mobileY, 'game2.bg');

            flip_coin = game.add.sprite(302+mobileX, 102+mobileY, 'game2.flip_coin');
            flip_coinAnimationeagle = flip_coin.animations.add('game2.flip_coin_eagle', [0,1,2,3,4,5,6,7,8,9], 12, false);
            flip_coinAnimationtails = flip_coin.animations.add('game2.flip_coin_tails', [0,1,2,3,4,5,6,7,8,10], 12, false);

            var coinHistory = ['tails','tails','tails','tails'];
            var coinHistoryImage = [];
            coinHistoryImage.push(game.add.sprite(126+mobileX, 118+mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126+mobileX, 166+mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126+mobileX, 214+mobileY, 'tails'));
            coinHistoryImage.push(game.add.sprite(126+mobileX, 262+mobileY, 'tails'));

            pick_left = game.add.sprite(94+mobileX,342+mobileY, 'pick_left');
            pick_left.visible = true;
            pick_right = game.add.sprite(94+mobileX,342+mobileY, 'pick_right');
            pick_right.visible = false;
            triger_left = game.add.sprite(94+mobileX,342+mobileY, 'triger_left');
            triger_left.visible = false;
            triger_right = game.add.sprite(94+mobileX,342+mobileY, 'triger_right');
            triger_right.visible = false;

            sound40 = game.add.audio('7_sound40');
            sound40.loop = true;
            sound40.play();

            flick();
            if(!isMobile){
                game.add.sprite(0,0, 'main_window');
            }

            checkGame = 2;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;

            addTableTitle(game, 'winTitleGame2', 557,115);
            hideTableTitle();

            // кнопки и анимации событий

            //анимация вариантов выбора
            
            selectAnOption[1][0] = function () { //проигрышный вариант для левой кнопки
                n = 1;
                coin_val= 2;
                pick_dice = 1;
                triger_left.visible = true;
                flip_coinAnimationtails.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('tails');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[1][1] = function () { //выигрышный вариант для левой кнопки
                n = 1;
                coin_val= 1;
                triger_left.visible = true;
                pick_dice = 1;
                flip_coinAnimationeagle.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('eagle');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[2][0] = function () { //проигрышный вариант для правой кнопки
                n = 9;
                coin_val= 1;
                pick_dice = 9;
                triger_right.visible = true;
                flip_coinAnimationeagle.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('eagle');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            selectAnOption[2][1] = function () { //проигрышный вариант для правой кнопки
                n = 9;
                coin_val= 2;
                triger_right.visible = true;
                pick_dice = 9;
                flip_coinAnimationtails.play();
                sound40.stop();
                setTimeout("restartGame(); sound40.play();", 3000);

                //пересчитываем историю выпадения монет
                coinHistory.unshift('tails');
                coinHistoryImage.forEach(function (item, i) {
                    item.loadTexture(coinHistory[i]);
                });
            };

            addButtonsGame2v2(game, selectAnOption);

            
            //счет
            var step = 1;
            var inputWin = totalWinR;
            var totalWin = totalWinR;

            // inputWin, totalWin, balanceR, step
            scorePosions = [[260, 26, 20], [485, 26, 20], [640, 26, 20], [475, 84, 30]];
            addScore(game, scorePosions, inputWin, totalWin, balanceR, '');

            fontsize = 16;
            showDoubleToAndTakeOrRiskTexts(game, totalWin, 567, 160, fontsize);


            //задержка для отображения showDoubleToAndTakeOrRiskTexts
            lockDisplay();
            setTimeout('unlockDisplay();', 2000);

            full_and_sound();
        },
        update: function () {
            if (game.scale.isFullScreen)
            {
              full.loadTexture('game.full');
              fullStatus = true;
          }
          else
          {
             full.loadTexture('game.non_full');
             fullStatus = false;
         }
     }
 };
 game.state.add('game2', game2);

}