//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(1420,27, 'game.non_full');
    else
        full = game.add.sprite(1420,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(1420,53, 'sound_on');
    else
        sound = game.add.sprite(1420,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var linefull1; var linefull2; var linefull3; var linefull4; var linefull5; var linefull6; var linefull7; var linefull8; var linefull9; var linefull10;
var lineNumber1left; var lineNumber2left; var lineNumber3left; var lineNumber4left; var lineNumber5left; var lineNumber6left; var lineNumber7left; var lineNumber8left; var lineNumber9left; var lineNumber1right; var lineNumber2right; var lineNumber3right; var lineNumber4right; var lineNumber5right; var lineNumber6right; var lineNumber7right; var lineNumber8right; var lineNumber9right;
//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9', 'linefull10'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);
    linefull10 = game.add.sprite(linePosition[9][0], linePosition[9][1], linefullNames[9]);

    lineNumber1left = game.add.sprite(numberPosition[0][0],numberPosition[0][1], 'number1left');
    lineNumber2left = game.add.sprite(numberPosition[1][0],numberPosition[1][1], 'number2left');
    lineNumber3left = game.add.sprite(numberPosition[2][0],numberPosition[2][1], 'number3left');
    lineNumber4left = game.add.sprite(numberPosition[3][0],numberPosition[3][1], 'number4left');
    lineNumber5left = game.add.sprite(numberPosition[4][0],numberPosition[4][1], 'number5left');
    lineNumber6left = game.add.sprite(numberPosition[5][0],numberPosition[5][1], 'number6left');
    lineNumber7left = game.add.sprite(numberPosition[6][0],numberPosition[6][1], 'number7left');
    lineNumber8left = game.add.sprite(numberPosition[7][0],numberPosition[7][1], 'number8left');
    lineNumber9left = game.add.sprite(numberPosition[8][0],numberPosition[8][1], 'number9left');
    lineNumber10left = game.add.sprite(numberPosition[9][0],numberPosition[9][1], 'number10left');
    lineNumber1right = game.add.sprite(numberPosition[10][0],numberPosition[10][1], 'number1right');
    lineNumber2right = game.add.sprite(numberPosition[11][0],numberPosition[11][1], 'number2right');
    lineNumber3right = game.add.sprite(numberPosition[12][0],numberPosition[12][1], 'number3right');
    lineNumber4right = game.add.sprite(numberPosition[13][0],numberPosition[13][1], 'number4right');
    lineNumber5right = game.add.sprite(numberPosition[14][0],numberPosition[14][1], 'number5right');
    lineNumber6right = game.add.sprite(numberPosition[15][0],numberPosition[15][1], 'number6right');
    lineNumber7right = game.add.sprite(numberPosition[16][0],numberPosition[16][1], 'number7right');
    lineNumber8right = game.add.sprite(numberPosition[17][0],numberPosition[17][1], 'number8right');
    lineNumber9right = game.add.sprite(numberPosition[18][0],numberPosition[18][1], 'number9right');
    lineNumber10right = game.add.sprite(numberPosition[19][0],numberPosition[19][1], 'number10right');

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        linefull10.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 10:
                linefull10.visible = false;
                break;
            }
        });
    }
}

var linesArray = []; // в случае если в функцию передается число, то открываются соответстивующий набор линий
function showLines(linesArray) {

    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        linefull10.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = true;
                break;
                case 2:
                linefull2.visible = true;
                break;
                case 3:
                linefull3.visible = true;
                break;
                case 4:
                linefull4.visible = true;
                break;
                case 5:
                linefull5.visible = true;
                break;
                case 6:
                linefull6.visible = true;
                break;
                case 7:
                linefull7.visible = true;
                break;
                case 8:
                linefull8.visible = true;
                break;
                case 9:
                linefull9.visible = true;
                break;
                case 10:
                linefull10.visible = true;
                break;

            }
        });
    }

}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        lineNumber1left.visible = false;
        lineNumber2left.visible = false;
        lineNumber3left.visible = false;
        lineNumber4left.visible = false;
        lineNumber5left.visible = false;
        lineNumber6left.visible = false;
        lineNumber7left.visible = false;
        lineNumber8left.visible = false;
        lineNumber9left.visible = false;
        lineNumber10left.visible = false;
        lineNumber1right.visible = false;
        lineNumber2right.visible = false;
        lineNumber3right.visible = false;
        lineNumber4right.visible = false;
        lineNumber5right.visible = false;
        lineNumber6right.visible = false;
        lineNumber7right.visible = false;
        lineNumber8right.visible = false;
        lineNumber9right.visible = false;
        lineNumber10right.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                lineNumber1left.visible = false;
                lineNumber1right.visible = false;
                break;
                case 2:
                lineNumber2left.visible = false;
                lineNumber2right.visible = false;
                break;
                case 3:
                lineNumber3left.visible = false;
                lineNumber3right.visible = false;
                break;
                case 4:
                lineNumber4left.visible = false;
                lineNumber4right.visible = false;
                break;
                case 5:
                lineNumber5left.visible = false;
                lineNumber5right.visible = false;
                break;
                case 6:
                lineNumber6left.visible = false;
                lineNumber6right.visible = false;
                break;
                case 7:
                lineNumber7left.visible = false;
                lineNumber7right.visible = false;
                break;
                case 8:
                lineNumber8left.visible = false;
                lineNumber8right.visible = false;
                break;
                case 9:
                lineNumber9left.visible = false;
                lineNumber9right.visible = false;
                break;
                case 10:
                lineNumber10left.visible = false;
                lineNumber10right.visible = false;
                break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        lineNumber1left.visible = true;
        lineNumber2left.visible = true;
        lineNumber3left.visible = true;
        lineNumber4left.visible = true;
        lineNumber5left.visible = true;
        lineNumber6left.visible = true;
        lineNumber7left.visible = true;
        lineNumber8left.visible = true;
        lineNumber9left.visible = true;
        lineNumber10left.visible = true;
        lineNumber1right.visible = true;
        lineNumber2right.visible = true;
        lineNumber3right.visible = true;
        lineNumber4right.visible = true;
        lineNumber5right.visible = true;
        lineNumber6right.visible = true;
        lineNumber7right.visible = true;
        lineNumber8right.visible = true;
        lineNumber9right.visible = true;
        lineNumber10right.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                lineNumber1left.visible = true;
                lineNumber1right.visible = true;
                break;
                case 2:
                lineNumber2left.visible = true;
                lineNumber2right.visible = true;
                break;
                case 3:
                lineNumber3left.visible = true;
                lineNumber3right.visible = true;
                break;
                case 4:
                lineNumber4left.visible = true;
                lineNumber4right.visible = true;
                break;
                case 5:
                lineNumber5left.visible = true;
                lineNumber5right.visible = true;
                break;
                case 6:
                lineNumber6left.visible = true;
                lineNumber6right.visible = true;
                break;
                case 7:
                lineNumber7left.visible = true;
                lineNumber7right.visible = true;
                break;
                case 8:
                lineNumber8left.visible = true;
                lineNumber8right.visible = true;
                break;
                case 9:
                lineNumber9left.visible = true;
                lineNumber9right.visible = true;
                break;
                case 10:
                lineNumber10left.visible = true;
                lineNumber10right.visible = true;
                break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}


//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
function hideButtons(buttonsArray) {
    if(buttonsArray.length == 0) {
        if(autostart == false){
            automaricstart.loadTexture('automaricstart_d');
            automaricstart.inputEnabled = false;
            automaricstart.input.useHandCursor = false;
        }

        selectGame.loadTexture('selectGame_d');
        selectGame.inputEnabled = false;
        selectGame.input.useHandCursor = false;

        payTable.loadTexture('payTable_d');
        payTable.inputEnabled = false;
        payTable.input.useHandCursor = false;

        betone.loadTexture('betone_d');
        betone.inputEnabled = false;
        betone.input.useHandCursor = false;

        betmax.loadTexture('betmax_d');
        betmax.inputEnabled = false;
        betmax.input.useHandCursor = false;

        startButton.loadTexture('startButton_d');
        startButton.inputEnabled = false;
        startButton.input.useHandCursor = false;

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

    } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]+'_d');
            item[0].inputEnabled = false;
            item[0].input.useHandCursor = false;
        })
    }
}

function showButtons(buttonsArray) {
    if(buttonsArray.length == 0) {

        if(autostart == false){
            automaricstart.loadTexture('automaricstart');
            automaricstart.inputEnabled = true;
            automaricstart.input.useHandCursor = true;

            selectGame.loadTexture('selectGame');
            selectGame.inputEnabled = true;
            selectGame.input.useHandCursor = true;

            payTable.loadTexture('payTable');
            payTable.inputEnabled = true;
            payTable.input.useHandCursor = true;

            betone.loadTexture('betone');
            betone.inputEnabled = true;
            betone.input.useHandCursor = true;

            betmax.loadTexture('betmax');
            betmax.inputEnabled = true;
            betmax.input.useHandCursor = true;

            startButton.loadTexture('startButton');
            startButton.inputEnabled = true;
            startButton.input.useHandCursor = true;

            buttonLine1.loadTexture('buttonLine1');
            buttonLine1.inputEnabled = true;
            buttonLine1.input.useHandCursor = true;

            buttonLine3.loadTexture('buttonLine3');
            buttonLine3.inputEnabled = true;
            buttonLine3.input.useHandCursor = true;

            buttonLine5.loadTexture('buttonLine5');
            buttonLine5.inputEnabled = true;
            buttonLine5.input.useHandCursor = true;

            buttonLine7.loadTexture('buttonLine7');
            buttonLine7.inputEnabled = true;
            buttonLine7.input.useHandCursor = true;

            buttonLine9.loadTexture('buttonLine9');
            buttonLine9.inputEnabled = true;
            buttonLine9.input.useHandCursor = true;
        }

    } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]);
            item[0].inputEnabled = true;
            item[0].input.useHandCursor = true;
        })
    }

}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;
var slot1bg, slot2bg, slot3bg, slot4bg, slot5bg, slot6bg, slot7bg, slot8bg, slot9bg, slot10bg, slot11bg, slot12bg, slot13bg, slot14bg, slot15bg;
var slots = []; //массив содержащий все объекты не анимированных значений слотов
var numberOfSlotValues = 9; // кол-во возможных значений слотов
var slotShadowArray = []; //массив в котором содержатся тени для ячеек
function addSlots(game, slotPosition, numberOfSlotValues) {

    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения

    var slotValueNames = [];
    var slotCellAnimName = 'cellAnims';

    for (var i = 0; i < numberOfSlotValues; i++) {
        slotValueNames.push('cell'+i);
    } //var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[randomNumber(0, numberOfSlotValues-1)]);

    slots = [slot1, slot2, slot3, slot4, slot5, slot6, slot7, slot8, slot9, slot10, slot11, slot12, slot13, slot14, slot15];


    slotShadowArray[0] = game.add.sprite(slotPosition[0][0],slotPosition[0][1], 'cellShadow');
    slotShadowArray[1] = game.add.sprite(slotPosition[1][0],slotPosition[1][1], 'cellShadow');
    slotShadowArray[2] = game.add.sprite(slotPosition[2][0],slotPosition[2][1], 'cellShadow');
    slotShadowArray[3] = game.add.sprite(slotPosition[3][0],slotPosition[3][1], 'cellShadow');
    slotShadowArray[4] = game.add.sprite(slotPosition[4][0],slotPosition[4][1], 'cellShadow');
    slotShadowArray[5] = game.add.sprite(slotPosition[5][0],slotPosition[5][1], 'cellShadow');
    slotShadowArray[6] = game.add.sprite(slotPosition[6][0],slotPosition[6][1], 'cellShadow');
    slotShadowArray[7] = game.add.sprite(slotPosition[7][0],slotPosition[7][1], 'cellShadow');
    slotShadowArray[8] = game.add.sprite(slotPosition[8][0],slotPosition[8][1], 'cellShadow');
    slotShadowArray[9] = game.add.sprite(slotPosition[9][0],slotPosition[0][1], 'cellShadow');
    slotShadowArray[10] = game.add.sprite(slotPosition[10][0],slotPosition[10][1], 'cellShadow');
    slotShadowArray[11] = game.add.sprite(slotPosition[11][0],slotPosition[11][1], 'cellShadow');
    slotShadowArray[12] = game.add.sprite(slotPosition[12][0],slotPosition[12][1], 'cellShadow');
    slotShadowArray[13] = game.add.sprite(slotPosition[13][0],slotPosition[13][1], 'cellShadow');
    slotShadowArray[14] = game.add.sprite(slotPosition[14][0],slotPosition[14][1], 'cellShadow');

    slotShadowArray.forEach(function (item) {
        item.visible = false;
    });

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    //в цикле создаем массивы с измененными на +1 значениями внутри
    var arrayOfNumbers = [];
    for (var i = 0; i < numberOfSlotValues; i++) {
        arrayOfNumbers[i] = i;
    }

    //Где arrays - содержит массивы с номерами кадров
    var arrays = [];
    for (var i = 0; i < 15; i++) {
        var zeroValue = arrayOfNumbers.shift();
        arrayOfNumbers.push(zeroValue);
        arrays[i] = arrayOfNumbers.slice();
    }

    slot1Anim.animations.add('slot1Anim', arrays[0], 20, true);
    slot2Anim.animations.add('slot2Anim', arrays[1], 20, true);
    slot3Anim.animations.add('slot3Anim', arrays[2], 20, true);
    slot4Anim.animations.add('slot4Anim', arrays[3], 20, true);
    slot5Anim.animations.add('slot5Anim', arrays[4], 20, true);
    slot6Anim.animations.add('slot6Anim', arrays[5], 20, true);
    slot7Anim.animations.add('slot7Anim', arrays[6], 20, true);
    slot8Anim.animations.add('slot8Anim', arrays[7], 20, true);
    slot9Anim.animations.add('slot9Anim', arrays[8], 20, true);
    slot10Anim.animations.add('slot10Anim', arrays[9], 20, true);
    slot11Anim.animations.add('slot11Anim', arrays[10], 20, true);
    slot12Anim.animations.add('slot12Anim', arrays[11], 20, true);
    slot13Anim.animations.add('slot13Anim', arrays[12], 20, true);
    slot14Anim.animations.add('slot14Anim', arrays[13], 20, true);
    slot15Anim.animations.add('slot15Anim', arrays[14], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;


}

var slotBgArray = [];
function addSlotBg(slotPosition) {
    //задники ячеек и второй слой ячеек (нужно для скрытия линий в выигрышных ячейках)
    for(var i = 0; i < 15; i++) {
        var number = i + 1;
        slotBgArray[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'slot'+ number +'bg');
        slotBgArray[i].visible = false;
    }
}

function hideSlotBg() {
    for(var i = 0; i < 15; i++) {
        slotBgArray[i].visible = false;
    }
}

var finalValues; var wlValues; var balance; var balanceR; var totalWin; var totalWinR; var dcard; var lines; var linesR; var betline; var betlineR; var bet; var extralife;//totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');
    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        totalWinR = finalValues[15];
        linesR = finalValues[16];
        betlineR = finalValues[17];

        checkSpinResult(totalWinR);

        slotRotation(game, finalValues);
    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons();

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('winMonkey');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {

                showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий

                showNumberWinLine(game, wlWinValuesArray[currentIndex], 370, 345);

                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines();
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            showButtons([]);
            checkAutoStart = false;
        }
    }

}

function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}
//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons();
    //alert('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    // $.ajax({
    //     type: "POST",
    //     url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
    //     dataType: 'html',
    //     success: function (data) {
        if (gameNumber == 3)
            data = 'result=ok&state=0&info=|3|2|7|5|2|1|1|2|0|5|1|4|6|1|3|3|1|1&wl=|3|0|0|0|0&balance=5511&dcard=19&swCnt=|3|0|0|0|0|0|0|0|0&swCntId=|1|0|0|0|0|0|0|0|0&rope=|&extralife=0&jackpots=2374.58|4674.58|6674.58';
            // bonus game
            if (gameNumber == 4)
                data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|1&extralife=45&jackpots=1822.16|4122.16|6122.16';
            //alert(data);
            data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=20&extralife=45&jackpots=1822.16|4122.16|6122.16';

            
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
    //     },
    //     error: function (xhr, ajaxOptions, thrownError) {
    //         var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
    //         alert(errorText);
    //     }
    // });
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons();

    hideNumbersAmin();
    hideLines();

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


// ajax-запросы

//init-запрос
function requestInit() {
    // $.ajax({
    //     type: "GET",
    //     //url: 'http://api.gmloto.ru/index.php?action=init',
    //     url: 'test.php',
    //     dataType: 'html',
    //     success: function (data) {
     data = "result=ok&state=0&SID=aeea5r0ai19oht0rvj3c5dd2p2&user=1271|user1271|100000.00";
     
     dataString = data;

     initDataArray = dataString.split('&');

     initDataArray.forEach(function (item) {
        if(item.indexOf('SID=') + 1) {
            sid = item.replace('SID=','');
        }
        if(item.indexOf('user=') + 1) {
            user = item.replace('user=','');
        }
    });

     if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
        requestState();
    } else {
        var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
        alert(errorText);
    }

    //     },
    //     error: function (xhr, ajaxOptions, thrownError) {
    //         var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
    //         alert(errorText);
    //     }
    // });
}

//state-запрос
var jackpots;
function requestState() {
    // $.ajax({
    //     type: "GET",
    //     //url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
    //     url: 'test.php',
    //     dataType: 'html',
    //     success: function (data) {
      data = "result=ok&state=0&min=1&id=user1271&balance=100000.00&extralife=45&jackpots=1826.24|4126.24|6126.24"

      var dataString = data;

      startDataArray = dataString.split('&');

      if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

        startDataArray = dataString.split('&');

        startDataArray.forEach(function (item) {
            if(item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if(item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=','');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
            if(item.indexOf('id=') + 1) {
                id = item.replace('id=','');
            }
            if(item.indexOf('min=') + 1) {
                min = item.replace('min=','');
            }

            game1();
            game2();

        });
    } else {
        var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
        alert(errorText);
    }

// },
// error: function (xhr, ajaxOptions, thrownError) {
//     var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
//     alert(errorText);
// }
// });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var actionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        actionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время

                showButtons([]);


                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('game.background1'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines();
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines();
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineMinus.loadTexture('minus');
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];

        press_bet.play();

        if((betlineCounter+1) == (betlineOptions.length)) {
            betlinePlus.loadTexture('plus_d');
        }
    }
}

function downBetline() {
    if(betlineCounter > 0) {
        betlinePlus.loadTexture('plus');
        betlineCounter -= 1;
        betline = betlineOptions[betlineCounter];

        press_bet.play();

        if(betlineCounter == 0) {
            betlineMinus.loadTexture('minus_d');
        }
    }
}

var lineCounter = 10;
var lineOptions = [1,2,3,4,5,6,7,8,9,10];
function upLine() {
    if(lineCounter < (lineOptions.length)) {
        linesMinus.loadTexture('minus');
        lineCounter += 1;
        lines = lineOptions[lineCounter-1];

        press_bet.play();

        if((lineCounter) == (lineOptions.length)) {
            linesPlus.loadTexture('plus_d');
        }
    }
}

function downLine() {
    if(lineCounter > 1) {
        linesPlus.loadTexture('plus');
        lineCounter -= 1;

        if(lineCounter == lineOptions.length) {
            lines = lineOptions[lineCounter] - 2;
        } else {
            lines = lineOptions[lineCounter] - 1;
        }


        press_bet.play();

        if(lineCounter == 1) {
            linesMinus.loadTexture('minus_d');
        }
    }
}



function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();
    // $.ajax({
    //     type: "POST",
    //     url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
    //     dataType: 'html',
    //     success: function (data) {
       data = 'result=ok&info=|40|29|30|31|7&dwin=40&balance=100004.00&dcard2=1&select=3&jackpots=1826.60|4126.60|6126.60';

       dataDoubleRequest = data.split('&');
       parseDoubleAnswer(dataDoubleRequest);

    //     },
    //     error: function (xhr, ajaxOptions, thrownError) {
    //         var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
    //         alert(errorText);
    //     }
    // });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: '22px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: '22px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];


function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}


/*
 *
 *  функции для игры типа bookOfRa
 *
 *
 * */

// для первого экрана игры типа bookOfRa
function addButtonsGame1TypeBOR(game) {

    // кнопки
    selectGame = game.add.sprite(70+20,530, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150+20,530, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490+20,530, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535+20,530, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685+20,530, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    requestSpinTypeBOR(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597+20, 530, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                requestSpinTypeBOR(gamename, betline, lines, bet, sid);
            } else {
                winSound.stop();
                takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton, 'startButton'], [betone, 'betone'], [betmax, 'betmax'], [payTable, 'payTable'], [selectGame, 'selectGame']]);
            takeWin.stop();
            changeTableTitleTypeBOR('Please place your bet');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(actionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 530, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);

    buttonLine3 = game.add.sprite(300, 530, 'buttonLine3_d');
    buttonLine3.scale.setTo(0.7,0.7);

    buttonLine5 = game.add.sprite(340, 530, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);

    buttonLine7 = game.add.sprite(380, 530, 'buttonLine7_d');
    buttonLine7.scale.setTo(0.7,0.7);

    buttonLine9 = game.add.sprite(420, 530, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
}

function addButtonsGame2TypeBOR(game) {

    // кнопки
    selectGame = game.add.sprite(70+20,530, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150+20,530, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490+20,530, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        if(betone.inputEnabled == true) {
            betone.loadTexture('betone_p');
        }
    });
    betone.events.onInputDown.add(function(){
        showMButton(buttonRed);
        requestDoubleTypeBOR(gamename, 'red', lines, bet, sid);
    });
    betone.events.onInputOut.add(function(){
        if(betone.inputEnabled == true) {
            betone.loadTexture('betone');
        }
    });


    betmax = game.add.sprite(535+20,530, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        if(betmax.inputEnabled == true) {
            betmax.loadTexture('betmax_p');
        }
    });
    betmax.events.onInputDown.add(function(){
        showMButton(buttonBlack);
        requestDoubleTypeBOR(gamename, 'black', lines, bet, sid);
    });
    betmax.events.onInputOut.add(function(){
        if(betmax.inputEnabled == true) {
            betmax.loadTexture('betmax');
        }
    });

    automaricstart = game.add.sprite(685+20,530, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;
    betmax.input.useHandCursor = false;

    startButton = game.add.sprite(597+20, 530, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 530, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);

    buttonLine3 = game.add.sprite(300, 530, 'buttonLine3_d');
    buttonLine3.scale.setTo(0.7,0.7);

    buttonLine5 = game.add.sprite(340, 530, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);

    buttonLine7 = game.add.sprite(380, 530, 'buttonLine7_d');
    buttonLine7.scale.setTo(0.7,0.7);

    buttonLine9 = game.add.sprite(420, 530, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
}

var checkFirstRotation = 0;
function requestSpinTypeBOR(gamename, betline, lines, bet, sid) {

    hideSquares();

    if(checkFirstRotation == 1)  {
        hideSlotBg();
    } else {
        checkFirstRotation = 1;
    }

    winCellData = [];
    winCellAnims = [];
    wlWinValuesArray = [];
    squares = [];
    allCellAnim = [];

    //создаем фон для слотов (нужно для скрытия линий в выигрышных ячейках)
    addSlotBg(slotPosition);

    //позиции ячеек относящиеся к линиям
    cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
    slotAnimPosition = slotPosition; //позиции анимированных ячеек

    //создаем массив содержащий все анимации
    addAllCellAnim(game, slotPosition, 8, framesArray);

    //цветные рамки для ячеек
    addSquares(game, slotPosition, 11);

    //убираем первоначально показваемые линии
    if(linefull1.visible == true) {
        hideLinesTypeBOR([]);
    }

    if(autostart == false) {
        hideButtonsTypeBORD([[autoplay, 'autoplay']]);
    }
    hideButtonsTypeBORD([[start, 'start'], [paytable, 'paytable']]);

    gamble.inputEnabled = false;

    linesPlus.inputEnabled = false;
    linesPlus.input.useHandCursor = false;
    linesMinus.inputEnabled = false;
    linesMinus.input.useHandCursor = false;
    betlinePlus.inputEnabled = false;
    betlinePlus.input.useHandCursor = false;
    betlineMinus.inputEnabled = false;
    betlineMinus.input.useHandCursor = false;
    betlineMinus.loadTexture('minus_d');
    betlinePlus.loadTexture('plus_d');
    linesPlus.loadTexture('plus_d');
    linesMinus.loadTexture('minus_d');

    hideSlotBg();
    hideWinSpinResult();
    hideCellShadow();
    hideLinesTypeBOR([]);
    hideSquares();

    //alert('betline: '+betline+', lines: '+lines+', bet: '+bet+', sid: '+sid);
    //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|0&extralife=45&jackpots=1822.16|4122.16|6122.16';
    var cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
    data = 'result=ok&state=0&info=|5|5|1|2|5|1|4|5|1|7|7|5|3|7|1|20|9|4&wl=|20|0|40|0|0&balance=77750.00&dcard=2&extralife=45&jackpots=1822.16|4122.16|6122.16';
    //data = 'result=ok&state=0&info=|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|20|9|4&wl=|20|0|0|0|0&balance=77750.00&dcard=2&extralife=45&jackpots=1822.16|4122.16|6122.16';
    //alert(data);
    dataSpinRequest = data;

    parseSpinAnswerTypeBOR(dataSpinRequest);

    /*$.ajax({
     type: "POST",
     url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
     dataType: 'html',
     success: function (data) {
     //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|0&extralife=45&jackpots=1822.16|4122.16|6122.16';
     //alert(data);
     dataSpinRequest = data;

     parseSpinAnswerTypeBOR(dataSpinRequest);
     },
     error: function (xhr, ajaxOptions, thrownError) {
     var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
     alert(errorText);
     }
 });*/
}

function parseSpinAnswerTypeBOR(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');
    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        totalWinR = finalValues[15];
        linesR = finalValues[16];
        betlineR = finalValues[17];

        checkSpinResult(totalWinR);

        slotRotationTypeBOR(game, finalValues);
    }
}

function slotRotationTypeBOR(game, finalValues) {
    checkRotaion = true;

    changeTableTitleTypeBOR('GOOD LUCK');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });


    slot1.visible = false;
    slot2.visible = false;
    slot3.visible = false;
    slot4.visible = false;
    slot5.visible = false;
    slot6.visible = false;
    slot7.visible = false;
    slot8.visible = false;
    slot9.visible = false;
    slot10.visible = false;
    slot11.visible = false;
    slot12.visible = false;
    slot13.visible = false;
    slot14.visible = false;
    slot15.visible = false;

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);

        slot1.visible = true;
        slot2.visible = true;
        slot3.visible = true;
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);

        slot4.visible = true;
        slot5.visible = true;
        slot6.visible = true;
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);

        slot7.visible = true;
        slot8.visible = true;
        slot9.visible = true;
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);

        slot10.visible = true;
        slot11.visible = true;
        slot12.visible = true;
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);

        slot13.visible = true;
        slot14.visible = true;
        slot15.visible = true;
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;

        showSpinResultTypeBOR(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var winSound; //звук играющий когда нужно забрать выигрышь или выбрать игру на удвоение
var soundWinLines = []; // массив для объектов звуков
var soundWinLineDurations = []; //массив содержищий длительности мелодии, которые проигрываются при выигрыше
var wlWinValuesArray = []; //массив с номерами выигрышных линий
var winCellAnim = [];
function showSpinResultTypeBOR(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('winMonkey');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        hideTableTitleTypeBOR();

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });


        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)


        var winCellData = getWinCellData(wlWinValuesArray, cellPositionOnLines, finalValues);
        winCellAnim = getWinCellAnim(winCellData, allCellAnim);

        //запускаются выигрышные анимации
        var rand = randomNumber(0,2);
        var currentIndex = -1;
        showWinCellAnim(winCellAnim);
        viewWinCellAnimStep1(wlWinValuesArray, currentIndex, rand, winCellAnim, winCellData);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            requestSpinTypeBOR(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitleTypeBOR('Please place your bet');
            updateBalance(game, scorePosions, balanceOld, balance);

            showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable'], [automaricstart, 'automaricstart']]);

            checkAutoStart = false;
        }
    }

}

var tableTitleText; //текст отображающийся в информационном табло
function addTitles(){

    var tableTitleTextFontSize = 32;
    if(checkGame == 2) {
        tableTitleTextFontSize = 18;
    }
    tableTitleText = game.add.text(170, 851, 'Please place your bet', {
        font: tableTitleTextFontSize+'px "Press Start 2P"',
        fill: '#f0f000',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });

    creditText = game.add.text(210, 922, 'credit', {
        font: '19px "Press Start 2P"',
        fill: '#ffffff',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });

    oneCreditText = game.add.text(385, 922, 'lines', {
        font: '19px "Press Start 2P"',
        fill: '#ffffff',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });

    oneCreditText2 = game.add.text(745, 922, 'bet', {
        font: '18px "Press Start 2P"',
        fill: '#ffffff',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });

    betText1 = game.add.text(560, 922, 'bet/line', {
        font: '19px "Press Start 2P"',
        fill: '#ffffff',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });
}

function changeTableTitleTypeBOR(text) {
    tableTitleText.setText(text);
}

function hideTableTitleTypeBOR() {
    tableTitleText.setText('');
}

var totalWinPosion;
var titleTotalWinTypeBOR = '';
function showStepTotalWinRTypeBOR(game, totalWinPosion, stepTotalWinR) {
    if(titleTotalWinTypeBOR == '') {
        titleTotalWinTypeBOR = game.add.text(totalWinPosion[0], totalWinPosion[1], stepTotalWinR + ' WON', {
            font: '30px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    } else {
        titleTotalWinTypeBOR.visible = true;
        titleTotalWinTypeBOR.setText(stepTotalWinR + ' WON');
    }
}

function takePrizeTypeBOR(game, scorePosions, balanceOld, balance) {
    //changeTableTitleTypeBOR('TAKE');
    hideButtons([[start, 'start'], [paytable, 'paytable']]);

    updateBalanceTypeBOR(game, scorePosions, balanceOld, balance);
    updateTotalWinRTypeBOR(game, scorePosions, totalWinR);

    gamble.inputEnabled = false;
    gamble.input.useHandCursor = false;

    linesPlus.inputEnabled = false;
    linesPlus.input.useHandCursor = false;
    linesMinus.inputEnabled = false;
    linesMinus.input.useHandCursor = false;
    betlinePlus.inputEnabled = false;
    betlinePlus.input.useHandCursor = false;
    betlineMinus.inputEnabled = false;
    betlineMinus.input.useHandCursor = false;
    if(betlineCounter > 0) {
        betlineMinus.loadTexture('minus_d');
    }
    if(betlineCounter > (betlineOptions.length-1)) {
        betlinePlus.loadTexture('plus_d');
    }
    if(lineCounter < (lineOptions.length-1)) {
        linesPlus.loadTexture('plus_d');
    }
    if(lineCounter > 1) {
        linesMinus.loadTexture('minus_d');
    }

    gamble.loadTexture('gamble_d');
}

function updateBalanceTypeBOR(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[start, 'start']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        actionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время

                showButtons([[start, 'start'], [paytable, 'paytable'], [autoplay, 'autoplay']]);

                collect.visible = false;
                collect.inputEnabled = false;

                takeWin.stop();
                changeTableTitleTypeBOR('Please place your bet');
                clearInterval(textCounter);

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    setTimeout(requestSpinTypeBOR, 1000, gamename, betline, lines, bet, sid);
                } else {
                    linesPlus.inputEnabled = true;
                    linesPlus.input.useHandCursor = true;
                    linesMinus.inputEnabled = true;
                    linesMinus.input.useHandCursor = true;
                    betlinePlus.inputEnabled = true;
                    betlinePlus.input.useHandCursor = true;
                    betlineMinus.inputEnabled = true;
                    betlineMinus.input.useHandCursor = true;
                    if(betlineCounter > 0) {
                        betlineMinus.loadTexture('minus');
                    }
                    if(betlineCounter > (betlineOptions.length-1)) {
                        betlinePlus.loadTexture('plus');
                    }
                    if(lineCounter < (lineOptions.length-1)) {
                        linesPlus.loadTexture('plus');
                    }
                    if(lineCounter > 1) {
                        linesMinus.loadTexture('minus');
                    }

                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

function updateTotalWinRTypeBOR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        titleTotalWinTypeBOR.visible = false;
        titleTotalWinTypeBOR = game.add.text(totalWinPosion[0], totalWinPosion[1], (parseInt(totalWinR) + parseInt(currentDifference)) + ' WON', {
            font: '28px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {

        titleTotalWinTypeBOR.visible = false;
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var lineGroup;
function addLinesTypeBOR(game, linePosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);

    lineGroup = game.add.group();

    lineGroup.add(linefull1);
    lineGroup.add(linefull2);
    lineGroup.add(linefull3);
    lineGroup.add(linefull4);
    lineGroup.add(linefull5);

    game.world.bringToTop(lineGroup);

}

function hideLinesTypeBOR(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        linefull10.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                linefull1.visible = false;
                break;
                case 2:
                linefull2.visible = false;
                break;
                case 3:
                linefull3.visible = false;
                break;
                case 4:
                linefull4.visible = false;
                break;
                case 5:
                linefull5.visible = false;
                break;
                case 6:
                linefull6.visible = false;
                break;
                case 7:
                linefull7.visible = false;
                break;
                case 8:
                linefull8.visible = false;
                break;
                case 9:
                linefull9.visible = false;
                break;
                case 10:
                linefull9.visible = false;
                break;
            }
        });
    }
}

function showLinesTypeBOR(linesArray) {
    if(!isNaN(parseFloat(linesArray)) && isFinite(linesArray)) {

        switch (linesArray) {
            case 1:
            linefull1.visible = true;
            break;
            case 2:
            linefull1.visible = true;
            linefull2.visible = true;
            break;
            case 3:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            break;
            case 4:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            break;
            case 5:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            break;
            case 6:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            break;
            case 7:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            linefull7.visible = true;
            break;
            case 8:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            linefull7.visible = true;
            linefull8.visible = true;
            break;
            case 9:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            linefull7.visible = true;
            linefull8.visible = true;
            linefull9.visible = true;
            break;
            case 10:
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            linefull7.visible = true;
            linefull8.visible = true;
            linefull9.visible = true;
            linefull10.visible = true;
            break;
        }
    } else {
        if(linesArray.length == 0) {
            linefull1.visible = true;
            linefull2.visible = true;
            linefull3.visible = true;
            linefull4.visible = true;
            linefull5.visible = true;
            linefull6.visible = true;
            linefull7.visible = true;
            linefull8.visible = true;
            linefull10.visible = true;
        } else {
            linesArray.forEach(function (item) {
                switch (item) {
                    case 1:
                    linefull1.visible = true;
                    break;
                    case 2:
                    linefull2.visible = true;
                    break;
                    case 3:
                    linefull3.visible = true;
                    break;
                    case 4:
                    linefull4.visible = true;
                    break;
                    case 5:
                    linefull5.visible = true;
                    break;
                    case 6:
                    linefull6.visible = true;
                    break;
                    case 7:
                    linefull7.visible = true;
                    break;
                    case 8:
                    linefull8.visible = true;
                    break;
                    case 9:
                    linefull9.visible = true;
                    break;
                    case 10:
                    linefull9.visible = true;
                    break;
                }
            });
        }
    }
}


/* *
 * создание и выведение анимации ячеек делится на три основных этапа:
 * 1) Получение значения выигрывших ячеек на линии (их должно быть > 2)
 * 2) Получения в массив анимации этих ячеек для каждой из линий в отдельности
 * 3) Выведение этих анимаций через период времени равный мелодии и продолжение цикла показа под музыку п(после одного круга)
 * */
// в allCellAnim порядковый номер массива соответствует координате ячейки, а внутри все анимации значений
var allCellAnim = []; //массив в котором по порядку идут объекты фазер-анимаций
var kArray = [];
var framesArray = []; // массив содержащий массивы с номерами кадров анимаций
function addAllCellAnim(game, slotPosition, numberOfSlotValues, framesArray) {

    for (var i = 0; i < 15; i++) {
        kArray = [];
        for (var k = 0; k < numberOfSlotValues-1; k++) {

            switch (k) {
                case 0:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[0], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 1:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[1], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 2:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[2], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 3:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[3], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 4:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[4], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 5:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[5], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 6:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[6], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 7:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[7], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 8:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[8], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;
                case 9:
                var cellXAnim = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'cell'+k+'Anim');
                cellXAnim.animations.add('cellXAnim', framesArray[9], 10, true);
                cellXAnim.animations.getAnimation('cellXAnim').play();
                cellXAnim.visible = false;
                break;

                //TODO после получения апи, нужнодобавить варианты анимаций
            }

            kArray.push(cellXAnim);
        }
        allCellAnim.push(kArray);
    }
}


var squares = []; //квадраты обрамляющие победившие ячейки
function addSquares(game, slotPosition, numberSquares) {
    //добавляем спрайты
    for (var i = 0; i < 15; i++) {
        kArray = [];
        for (var k = 1; k < numberSquares-1; k++) {
            var squeare = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'square'+k);
            squeare.visible = false;
            kArray.push(squeare);
        }
        squares.push(kArray);
    }
}

function hideSquares() {
    squares.forEach(function (item) {
        item.forEach(function (item2) {
            item2.visible = false;
        });
    });
}

//массив с позициями слотов относящихся к линиям (номер линии = номер ключа +1)
var cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];

//Получение массива с информацией о выигрышных ячейках [ [numberLine,winValue, [winValuePosition]],  ...]
function getWinCellData(wlWinValuesArray, cellPositionOnLines, finalValues) {

    var winCellData = [];

    //получаем выигрышные линии
    wlWinValuesArray.forEach(function (item, i) {
        winCellData[i] = [];
        winCellData[i].push(item);
    });

    //получаем выигрышные значения
    winCellData.forEach(function (item, i) {
        var numberWinLines = item[0]; // номер выиргырной линии
        var winCellPosition = cellPositionOnLines[numberWinLines - 1]; //массив с номерами позиций выигрышных ячеек

        var winValue = getWinCellValue(winCellPosition, finalValues);

        winCellData[i].push(winValue);
    });

    //получаем координаты выигрышных ячеек
    winCellData.forEach(function (cellData, i) {
        var winCellPosition = [];
        cellPositionOnLines[cellData[0] - 1].forEach(function (cellPos, k) {
            if(finalValues[cellPos] == cellData[1]) {
                winCellPosition.push(cellPos);
            }
        });

        winCellData[i].push(winCellPosition);
    });

    return winCellData;
}

//Получения в массив анимации этих ячеек для каждой из линий в отдельности
function getWinCellAnim(winCellData, allCellAnim) {
    var winCellAnims = [];

    winCellData.forEach(function (cellData) {//получаем данные по отдельной линии
        var numberLine = cellData[0];
        var winValue = cellData[1];
        var winValuePosition = cellData[2];

        var arrayAnims = [];
        winValuePosition.forEach(function (pos) { //записываем в массив winCellAnims анимации для каждой линии
            arrayAnims.push(allCellAnim[pos][winValue]);
            //slots[pos].visible = false;

            //делаем видимым второй слой bg под анимированными выигрышными ячейками, для того, чтобы скрыть линии
            slotBgArray[pos].visible = true;
        });

        winCellAnims.push(arrayAnims);
    });

    return winCellAnims;
}

function showWinCellAnim(winCellAnim) {
    winCellAnim.forEach(function (anims, i) {
        anims.forEach(function (anim) {
            anim.visible = true;
        });
    });
}

function hideWinCellAnim(winCellAnim) {
    winCellAnim.forEach(function (anims, i) {
        anims.forEach(function (anim) {
            anim.visible = false;
        });
    });
}

//Выведение этих анимаций через период времени равный мелодии и продолжение цикла показа под музыку п(после одного круга)

function viewWinCellAnimStep1(wlWinValuesArray, currentIndex, rand, winCellAnim, winCellData) {

    if(++currentIndex > (wlWinValuesArray.length - 1)) {

        //прячем линии и квадраты
        hideLines([wlWinValuesArray[currentIndex - 1]]);
        winCellData[currentIndex - 1][2].forEach(function (pos) {
            squares[pos][winCellData[currentIndex - 1][0] - 1].visible = false;
        });

        // включаем кнопку перехода на второй экран
        gamble.loadTexture('gamble');

        //запускаем бесконечный цикл показа анимаций
        viewWinCellAnimStep2(winCellAnim, winCellData);

    } else {
        //затемняем ячейки
        showCellShadow();

        stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
        showStepTotalWinRTypeBOR(game, totalWinPosion, parseInt(stepTotalWinR));


        //changeTableTitleTypeBOR(wlWinValuesArray[currentIndex] + ' WON');
        soundWinLines[rand].play();

        //прячем линии и квадраты
        if(currentIndex != 0) {
            hideLines([wlWinValuesArray[currentIndex - 1]]);

            winCellData[currentIndex - 1][2].forEach(function (pos) {
                squares[pos][winCellData[currentIndex - 1][0] - 1].visible = false;
            });
        }

        //выводим линии
        showLines([wlWinValuesArray[currentIndex]]);

        //выводим квадраты
        winCellData[currentIndex][2].forEach(function (pos) {
            squares[pos][winCellData[currentIndex][0] - 1].visible = true;
        });



        /*winCellAnim[currentIndex].forEach(function (anim, i) {
         anim.visible = true;
         });

         if(checkStep0 == false) { //для нулевого элемента массива не выполняется из-за checkStep0
         winCellAnim[currentIndex - 1].forEach(function (anim, i) {
         anim.visible = false;
         });
     }*/

     setTimeout(viewWinCellAnimStep1, soundWinLineDurations[rand], wlWinValuesArray, currentIndex, rand, winCellAnim, winCellData);
 }
}

var switchingLine; //интервал для переключения отображаемых линий
function viewWinCellAnimStep2(winCellAnim, winCellData) {

    // включаем кнопку перехода на второй экран
    gamble.inputEnabled = true;
    gamble.input.useHandCursor = true;


    showButtons([[paytable, 'paytable'], [autoplay, 'autoplay']]);

    collect.visible = true;
    collect.inputEnabled = true;


    //changeTableTitleTypeBOR('GAMBLE UP TO 5x OR TAKE WIN');

    if(autostart == true){
        takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
    } else {

        winSound = game.add.audio('win');
        winSound.loop = true;
        winSound.play();

        checkAutoStart = false;

        //переключение анимаций в интрервале
        var currentIndex = 0;
        switchingLine = setInterval(function() {
            //если была показана последняя анимация, то убираем ее и начинаем круг сначала
            if(currentIndex == winCellAnim.length) {
                hideLines([wlWinValuesArray[currentIndex - 1]]);

                winCellData[currentIndex - 1][2].forEach(function (pos) {
                    squares[pos][winCellData[currentIndex - 1][0] - 1].visible = false;
                });

                currentIndex = 0;
            }

            //если показывается не первая анимация, то убираем предыдущие
            if(currentIndex != 0) {
                hideLines([wlWinValuesArray[currentIndex - 1]]);

                winCellData[currentIndex - 1][2].forEach(function (pos) {
                    squares[pos][winCellData[currentIndex - 1][0] - 1].visible = false;
                });
            }

            //ваводим анимации
            showLines([wlWinValuesArray[currentIndex]]);

            winCellData[currentIndex][2].forEach(function (pos) {
                squares[pos][winCellData[currentIndex][0] - 1].visible = true;
            });

            currentIndex += 1;
        }, 1000);
    }
}

function getWinCellValue(winCellPosition, finalValues) {
    var winValue;

    var arrayCounterWinValue = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    winCellPosition.forEach(function (item) {
        if(finalValues[item] == 0) {
            arrayCounterWinValue[0] += 1;
        }
        if(finalValues[item] == 1) {
            arrayCounterWinValue[1] += 1;
        }
        if(finalValues[item] == 2) {
            arrayCounterWinValue[2] += 1;
        }
        if(finalValues[item] == 3) {
            arrayCounterWinValue[3] += 1;
        }
        if(finalValues[item] == 4) {
            arrayCounterWinValue[4] += 1;
        }
        if(finalValues[item] == 5) {
            arrayCounterWinValue[5] += 1;
        }
        if(finalValues[item] == 6) {
            arrayCounterWinValue[6] += 1;
        }
        if(finalValues[item] == 7) {
            arrayCounterWinValue[7] += 1;
        }
        /*if(finalValues[item] == 8) {
         arrayCounterWinValue[8] += 1;
         }
         if(finalValues[item] == 9) {
         arrayCounterWinValue[9] += 1;
         }
         if(finalValues[item] == 10) {
         arrayCounterWinValue[10] += 1;
         }
         if(finalValues[item] == 11) {
         arrayCounterWinValue[11] += 1;
         }
         if(finalValues[item] == 12) {
         arrayCounterWinValue[12] += 1;
     }*/
 });

    var counter = 0;
    arrayCounterWinValue.forEach(function (item) {
        if(+(item) > 2) {
            winValue = counter;
        }
        counter += 1;
    });

    return winValue;
}



//функции для второго экрана

var gambleAmount; //сумма текущего выигрыша
var gambleToWin; //удвоенная сумма выигрыша

var gambleAmountText;
var gambleToWinText;
function addGamble(totalWinR, gamblePosition) {

    gambleAmountText = game.add.text(gamblePosition[0][0], gamblePosition[0][1], totalWinR, {
        font: '32px "Press Start 2P"',
        fill: '#ffff00',
        stroke: '#000000',
        strokeThickness: 3,
        align: 'center'
    });

    gambleToWinText = game.add.text(gamblePosition[1][0], gamblePosition[1][1], totalWinR*2, {
        font: '32px "Press Start 2P"',
        fill: '#ffff00',
        stroke: '#000000',
        strokeThickness: 4,
        align: 'center'
    });
}

function hideGambles() {
    gambleAmountText.visible = false;
    gambleToWinText.visible = false;
}

function showGambles() {
    gambleAmountText.visible = true;
    gambleToWinText.visible = true;
}

function updateGambles(dwin) {
    gambleAmountText.setText(dwin/2);
    gambleToWinText.setText(dwin);
}

var cardAnim;
function addCardAnim() {
    cardAnim = game.add.sprite(625,400, 'cardAnim');
    cardAnim.scale.setTo(1.045, 1.045);
    cardAnim.animations.add('cardAnim', [0,1], 10, true);
    cardAnim.animations.getAnimation('cardAnim').play();
}

function hideCardAnim() {
    cardAnim.visible = false;
}

function showCardAnim() {
    cardAnim.visible = true;
}

function requestDoubleTypeBOR(gamename, selectedCard, lines, bet, sid) {
    sound16.stop();
    redButtonAnim.visible = false;
    blackButtonAnim.visible = false;
    collectButtonAnim.visible = false;

    hideButtonsTypeBORD([[buttonRed, 'buttonRed'], [buttonBlack, 'buttonBlack'], [collect, 'collect'], [autoplay, 'autoplay'], [redButtonOnScreen, 'redButtonOnScreen'], [blackButtonOnScreen, 'blackButtonOnScreen']]);

    var data = 'result=ok&info=|40|29|30|31|7&dwin=100&balance=100004.00&dcard2=1&select='+selectedCard+'&jackpots=1826.60|4126.60|6126.60';
    dataDoubleRequest = data.split('&');
    parseDoubleAnswerTypeBOR(dataDoubleRequest);

    /*$.ajax({
     type: "POST",
     url: 'http://api.gmloto.ru/index.php?action=double&min='+min+'&betline='+selectedCard+'&lines='+lines+'&bet='+bet+'&game='+gamename+'&SID='+sid,
     dataType: 'html',
     success: function (data) {

     dataDoubleRequest = data.split('&');
     parseDoubleAnswerTypeBOR(dataDoubleRequest);

     },
     error: function (xhr, ajaxOptions, thrownError) {
     var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
     alert(errorText);
     }
 });*/
}

function parseDoubleAnswerTypeBOR(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResultTypeBOR(dwin, selectedCardR, valuesOfAllCards);

    }
}

var buttonRed, buttonBlack; //кнопки цвета
var buttonMPosition = []; //позиции кнопок
function addMButtons(buttonMPosition) {
    buttonRed = game.add.sprite(buttonMPosition[0][0], buttonMPosition[0][1], 'buttonRed');
    buttonBlack = game.add.sprite(buttonMPosition[1][0], buttonMPosition[1][1], 'buttonBlack');
    buttonRed.scale.setTo(1.045, 1.045);
    buttonBlack.scale.setTo(1.045, 1.045);

    buttonRed.visible = false;
    buttonBlack.visible = false;
}

function showMButton(button) {
    button.visible = true;
}

function hideMButton(button) {
    if(button = '') {
        buttonRed.visible = false;
        buttonBlack.visible = false;
    } else {
        button.visible = false;
    }
}

var savedM = [0,0,0,0,0]; //знаячения сохраненных мастей карт
var savedMObject = []; //объекты фазер-картинок мастей
var m1, m2, m3, m4, m5; //картинки с мастями карт
function addMs() {
    m1 = game.add.sprite(cardPositionTypeBOR[1][1][0], cardPositionTypeBOR[1][1][1], 'm1');
    m2 = game.add.sprite(cardPositionTypeBOR[1][2][0], cardPositionTypeBOR[1][2][1], 'm2');
    m3 = game.add.sprite(cardPositionTypeBOR[1][3][0], cardPositionTypeBOR[1][3][1], 'm3');
    m4 = game.add.sprite(cardPositionTypeBOR[1][4][0], cardPositionTypeBOR[1][4][1], 'm4');
    m5 = game.add.sprite(cardPositionTypeBOR[1][5][0], cardPositionTypeBOR[1][5][1], 'm4');

    m1.scale.setTo(1.045, 1.06);

    m2.scale.setTo(1.045, 1.06);
    m3.scale.setTo(1.045, 1.06);
    m4.scale.setTo(1.045, 1.06);
    m5.scale.setTo(1.045, 1.06);

    /*m1.visible = false;
     m2.visible = false;
     m3.visible = false;
     m4.visible = false;
     m5.visible = false;*/


     savedMObject.push(m1, m2, m3, m4, m5);
 }

 function updateMs(savedM) {
    savedM.forEach(function (value, i) {
        if(value > 0) {
            savedMObject[i].visible = true;
            savedMObject[i].loadTexture('m'+value);
        }
    });
}

function showM(m, value) {
    m.loadTexture('m'+value);
    m.visible = true;
}

var card1, card2, card3, card4;
function addCardsTypeBOR(cardPositionTypeBOR) {
    card1 = game.add.sprite(cardPositionTypeBOR[0][0], cardPositionTypeBOR[0][1], 'card1');
    card2 = game.add.sprite(cardPositionTypeBOR[0][0], cardPositionTypeBOR[0][1], 'card2');
    card3 = game.add.sprite(cardPositionTypeBOR[0][0], cardPositionTypeBOR[0][1], 'card3');
    card4 = game.add.sprite(cardPositionTypeBOR[0][0], cardPositionTypeBOR[0][1], 'card4');

    card1.scale.setTo(1.045, 1.045);
    card2.scale.setTo(1.045, 1.045);
    card3.scale.setTo(1.045, 1.045);
    card4.scale.setTo(1.045, 1.045);

    card1.visible = false;
    card2.visible = false;
    card3.visible = false;
    card4.visible = false;
}

function hideCardsTypeBOR() {
    card1.visible = false;
    card2.visible = false;
    card3.visible = false;
    card4.visible = false;
}

var cardPositionTypeBOR = [];
function showDoubleResultTypeBOR(dcard) {
    hideCardAnim();
    hideGambles();

    dcard = dcard2;

    //отображаем карту
    switch (+dcard) {
        case 1:
        card1.visible = true;
        break;
        case 2:
        card2.visible = true;
        break;
        case 3:
        card3.visible = true;
        break;
        case 4:
        card4.visible = true;
        break;
    }

    //сохраняем масть
    savedM.splice(savedM.length - 1);
    savedM.unshift(+(dcard));

    //пересчет gamble
    updateMs(savedM);

    //результат
    if(dwin == 0) {
        setTimeout("unlockDisplay(); game.state.start('game1');", 1000);
    } else {
        setTimeout("sound16.play(); hideMButton(); showGambles(); updateGambles(dwin); showCardAnim(); hideCardsTypeBOR(); unlockDisplay(); showButtons([[buttonRed, 'buttonRed'], [buttonBlack, 'buttonBlack'], [collect, 'collect'], [autoplay, 'autoplay'], [redButtonOnScreen, 'redButtonOnScreen'], [blackButtonOnScreen, 'blackButtonOnScreen']]);", 1000);
    }
}




//для BORDelux
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;
var gamble; var linesPlus, linesMinus; var betlinePlus, betlineMinus;
var press_bet;
function addButtonsGame1TypeBORDelux(game) {

    var autoplay_start =  game.add.audio('autoplay_start');
    var autoplay_stop =  game.add.audio('autoplay_stop');
    press_bet =  game.add.audio('press_bet');

    // кнопки
    autoplay = game.add.sprite(1180,830, 'autoplay');
    autoplay.inputEnabled = true;
    autoplay.input.useHandCursor = true;
    autoplay.events.onInputOver.add(function(){
        if(autoplay.inputEnabled == true){
            autoplay.loadTexture('autoplay_h');
        }
    });
    autoplay.events.onInputOut.add(function(){
        if(autoplay.inputEnabled == true){
            autoplay.loadTexture('autoplay');
        }
    });
    autoplay.events.onInputDown.add(function(){
        if(autoplay.inputEnabled == true){
            autoplay.loadTexture('autoplay_p');
        }

        if(autostart == true) {
            autoplay_stop.play();
        } else {
            autoplay_start.play();
        }

        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;

                    startAnim.visible = false;
                    requestSpinTypeBOR(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                winSound.stop();
                autostart = true;
                takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    start = game.add.sprite(1180,950, 'start');
    start.inputEnabled = true;
    start.input.useHandCursor = true;
    start.events.onInputOver.add(function(){
        if(start.inputEnabled == true) {
            startAnim.visible = false;
            start.loadTexture('start_h');
        }
    });
    start.events.onInputOut.add(function(){
        if(start.inputEnabled == true) {
            startAnim.visible = true;
            start.loadTexture('start');
        }
    });
    start.events.onInputDown.add(function(){
        start.loadTexture('start_p');
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {

                requestSpinTypeBOR(gamename, betline, lines, bet, sid);
            } else {
                winSound.stop();
                takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[start, 'start'], [paytable, 'paytable']]);
            takeWin.stop();
            changeTableTitleTypeBOR('Please place your bet');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(actionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    collect = game.add.sprite(1180,950, 'collect');
    collect.inputEnabled = true;
    collect.input.useHandCursor = true;
    collect.events.onInputOver.add(function(){
        if(collect.inputEnabled == true) {
            collect.loadTexture('collect_h');
        }
    });
    collect.events.onInputOut.add(function(){
        if(collect.inputEnabled == true) {
            collect.loadTexture('collect');
        }
    });
    collect.events.onInputDown.add(function(){
        collect.loadTexture('collect_p');
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            winSound.stop();
            takePrizeTypeBOR(game, scorePosions, balanceOld, balance);
        } else {
            collect.visible = false;
            collect.inputEnabled = false;
            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[start, 'start'], [paytable, 'paytable']]);
            takeWin.stop();
            changeTableTitleTypeBOR('Please place your bet');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(actionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });
    collect.visible = false;

    //анимированная кнопка start
    var startAnim = game.add.sprite(1180,950, 'startAnim');
    startAnim.animations.add('startAnim', [0,1], 2, true);
    startAnim.animations.getAnimation('startAnim').play();

    gamble = game.add.sprite(1000,950, 'gamble_d');
    gamble.inputEnabled = false;
    gamble.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideWinSpinResult();
            winSound.stop();
            game.state.start('game2');
        }
    });
    gamble.events.onInputOver.add(function(){
        gamble.loadTexture('gamble_h');
    });
    gamble.events.onInputOut.add(function(){
        gamble.loadTexture('gamble');
    });


    paytable = game.add.sprite(820,950, 'paytable');
    paytable.inputEnabled = true;
    paytable.input.useHandCursor = true;
    paytable.events.onInputOver.add(function(){
        paytable.loadTexture('paytable_h');
    });
    paytable.events.onInputOut.add(function(){
        paytable.loadTexture('paytable');
    });
    paytable.events.onInputDown.add(function(){
        paytable.loadTexture('paytable_p');
    });


    linesPlus = game.add.sprite(426,952, 'plus_d');
    linesPlus.inputEnabled = true;
    linesPlus.input.useHandCursor = true;
    linesPlus.events.onInputOver.add(function(){
        if(lineCounter < (lineOptions.length-1)) {
            linesPlus.loadTexture('plus_h');
        }
    });
    linesPlus.events.onInputOut.add(function(){
        if(lineCounter < (lineOptions.length-1)) {
            linesPlus.loadTexture('plus');
        }
    });
    linesPlus.events.onInputDown.add(function(){
        if(lineCounter < (lineOptions.length-1)) {
            linesPlus.loadTexture('plus_p');
        }
        hideSlotBg();
        hideWinSpinResult();
        hideCellShadow();
        hideLinesTypeBOR([]);
        hideSquares();

        upLine(lineOptions);

        hideLinesTypeBOR([]);
        showLinesTypeBOR(lines);

        updateBetinfo(game, scorePosions, lines, betline);
    });

    linesMinus = game.add.sprite(328,952, 'minus');
    linesMinus.inputEnabled = true;
    linesMinus.input.useHandCursor = true;
    linesMinus.events.onInputOver.add(function(){
        if(lineCounter > 1) {
            linesMinus.loadTexture('minus_h');
        }
    });
    linesMinus.events.onInputOut.add(function(){
        if(lineCounter > 1) {
            linesMinus.loadTexture('minus');
        }
    });
    linesMinus.events.onInputDown.add(function(){
        if(lineCounter > 1) {
            linesMinus.loadTexture('minus_p');
        }
        hideSlotBg();
        hideWinSpinResult();
        hideCellShadow();
        hideLinesTypeBOR([]);
        hideSquares();

        downLine(lineOptions);

        hideLinesTypeBOR([]);
        showLinesTypeBOR(lines);

        updateBetinfo(game, scorePosions, lines, betline);
    });

    betlinePlus = game.add.sprite(643,952, 'plus');
    betlinePlus.inputEnabled = true;
    betlinePlus.input.useHandCursor = true;
    betlinePlus.events.onInputOver.add(function(){
        if(betlineCounter < (betlineOptions.length-1)) {
            betlinePlus.loadTexture('plus_h');
        }
    });
    betlinePlus.events.onInputOut.add(function(){
        if(betlineCounter < (betlineOptions.length-1)) {
            betlinePlus.loadTexture('plus');
        }
    });
    betlinePlus.events.onInputDown.add(function(){
        if(betlineCounter < (betlineOptions.length-1)) {
            betlinePlus.loadTexture('plus_p');
        }

        upBetline(betlineOptions);
        updateBetinfo(game, scorePosions, lines, betline);
    });

    if(betlineCounter == 0) {
        betlineMinus = game.add.sprite(486,952, 'minus_d');
    } else {
        betlineMinus = game.add.sprite(486,952, 'minus');
    }
    betlineMinus.inputEnabled = true;
    betlineMinus.input.useHandCursor = true;
    betlineMinus.events.onInputOver.add(function(){
        if(betlineCounter > 0) {
            betlineMinus.loadTexture('minus_h');
        }
    });
    betlineMinus.events.onInputOut.add(function(){
        if(betlineCounter > 0) {
            betlineMinus.loadTexture('minus');
        }
    });
    betlineMinus.events.onInputDown.add(function(){
        if(betlineCounter > 0) {
            betlineMinus.loadTexture('minus_p');
        }

        downBetline(betlineOptions);
        updateBetinfo(game, scorePosions, lines, betline);
    });
}

var redButtonAnim; var blackButtonAnim; var collectButtonAnim;
var sound16;
function addButtonsGame2TypeBORDelux(game) {

    // кнопки
    autoplay = game.add.sprite(1180,830, 'autoplay');
    autoplay.inputEnabled = true;
    autoplay.input.useHandCursor = true;
    autoplay.events.onInputOver.add(function(){
        autoplay.loadTexture('autoplay_h');
    });
    autoplay.events.onInputOut.add(function(){
        autoplay.loadTexture('autoplay');
    });
    autoplay.events.onInputDown.add(function(){
        autoplay.loadTexture('autoplay_p');
        var number = randomNumber(0,1);
        if(number == 0) {
            requestDoubleTypeBOR(gamename, 'red', lines, bet, sid);
        } else {
            requestDoubleTypeBOR(gamename, 'black', lines, bet, sid);
        }
    });

    collect = game.add.sprite(1180,950, 'collect');
    collect.inputEnabled = true;
    collect.input.useHandCursor = true;
    collect.events.onInputOver.add(function(){
        if(collect.inputEnabled == true) {
            collectButtonAnim.visible = false;
            collect.loadTexture('collect_p');
        }
    });
    collect.events.onInputOut.add(function(){
        if(collect.inputEnabled == true) {
            collectButtonAnim.visible = true;
            collect.loadTexture('collect');
        }
    });
    collect.events.onInputDown.add(function(){
        sound16.stop();
        game.state.start('game1');
    });

    buttonRed = game.add.sprite(820,950, 'buttonRed');
    buttonRed.inputEnabled = true;
    buttonRed.input.useHandCursor = true;
    buttonRed.events.onInputOver.add(function(){
        redButtonAnim.visible = false;
        buttonRed.loadTexture('buttonRed_p');
    });
    buttonRed.events.onInputOut.add(function(){
        if(buttonRed.inputEnabled == true) {
            redButtonAnim.visible = true;
            buttonRed.loadTexture('buttonRed');
        }
    });
    buttonRed.events.onInputDown.add(function(){
        showMButton(buttonRed);
        requestDoubleTypeBOR(gamename, 'red', lines, bet, sid);
        buttonRed.loadTexture('buttonRed_d');
    });


    buttonBlack = game.add.sprite(1000,950, 'buttonBlack');
    buttonBlack.inputEnabled = true;
    buttonBlack.input.useHandCursor = true;
    buttonBlack.events.onInputOver.add(function(){
        blackButtonAnim.visible = false;
        buttonBlack.loadTexture('buttonBlack_p');
    });
    buttonBlack.events.onInputOut.add(function(){
        if(buttonRed.inputEnabled == true) {
            blackButtonAnim.visible = true;
            buttonBlack.loadTexture('buttonBlack');
        }
    });
    buttonBlack.events.onInputDown.add(function(){
        showMButton(buttonBlack);
        requestDoubleTypeBOR(gamename, 'black', lines, bet, sid);
        buttonBlack.loadTexture('buttonBlack_d');
    });

    redButtonOnScreen = game.add.sprite(230,470, 'redButtonOnScreen');
    redButtonOnScreen.inputEnabled = true;
    redButtonOnScreen.input.useHandCursor = true;
    redButtonOnScreen.events.onInputOver.add(function(){
        redButtonOnScreen.loadTexture('redButtonOnScreen_p');
    });
    redButtonOnScreen.events.onInputOut.add(function(){
        if(redButtonOnScreen.inputEnabled == true) {
            redButtonOnScreen.loadTexture('redButtonOnScreen');
        }
    });
    redButtonOnScreen.events.onInputDown.add(function(){
        redButtonOnScreen.loadTexture('redButtonOnScreen_d');
        requestDoubleTypeBOR(gamename, 'red', lines, bet, sid);
    });


    blackButtonOnScreen = game.add.sprite(905,470, 'blackButtonOnScreen');
    blackButtonOnScreen.inputEnabled = true;
    blackButtonOnScreen.input.useHandCursor = true;
    blackButtonOnScreen.events.onInputOver.add(function(){
        blackButtonOnScreen.loadTexture('blackButtonOnScreen_p');
    });
    blackButtonOnScreen.events.onInputOut.add(function(){
        if(blackButtonOnScreen.inputEnabled == true) {
            blackButtonOnScreen.loadTexture('blackButtonOnScreen');
        }
    });
    blackButtonOnScreen.events.onInputDown.add(function(){
        blackButtonOnScreen.loadTexture('blackButtonOnScreen_d');
        requestDoubleTypeBOR(gamename, 'black', lines, bet, sid);
    });

    //анимации кнопок
    redButtonAnim = game.add.sprite(820,950, 'redButtonAnim');
    redButtonAnim.animations.add('redButtonAnim', [0,1], 2, true);
    redButtonAnim.animations.getAnimation('redButtonAnim').play();

    blackButtonAnim = game.add.sprite(1000,950, 'blackButtonAnim');
    blackButtonAnim.animations.add('blackButtonAnim', [0,1], 2, true);
    blackButtonAnim.animations.getAnimation('blackButtonAnim').play();

    collectButtonAnim = game.add.sprite(1180,950, 'collectButtonAnim');
    collectButtonAnim.animations.add('collectButtonAnim', [0,1], 2, true);
    collectButtonAnim.animations.getAnimation('collectButtonAnim').play();

    linesPlus = game.add.sprite(328,952, 'minus_d');
    /*linesPlus.inputEnabled = true;
     linesPlus.input.useHandCursor = true;
     linesPlus.events.onInputOver.add(function(){
     linesPlus.loadTexture('minus_h');
     });
     linesPlus.events.onInputOut.add(function(){
     linesPlus.loadTexture('minus');
     });
     linesPlus.events.onInputDown.add(function(){
     linesPlus.loadTexture('minus_p');
 });*/

 linesMinus = game.add.sprite(426,952, 'plus_d');
    /*linesMinus.inputEnabled = true;
     linesMinus.input.useHandCursor = true;
     linesMinus.events.onInputOver.add(function(){
     linesMinus.loadTexture('plus_h');
     });
     linesMinus.events.onInputOut.add(function(){
     linesMinus.loadTexture('plus');
     });
     linesMinus.events.onInputDown.add(function(){
     linesMinus.loadTexture('plus_p');
 });*/

 betlinePlus = game.add.sprite(643,952, 'plus_d');

 betlineMinus = game.add.sprite(486,952, 'minus_d');

}


//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
function hideButtonsTypeBORD(buttonsArray) {

    if(buttonsArray.length == 0) {
        if(autostart == false){
            autoplay.loadTexture('autoplay_d');
            autoplay.inputEnabled = false;
            autoplay.input.useHandCursor = false;
        }

        /*selectGame.loadTexture('selectGame_d');
         selectGame.inputEnabled = false;
         selectGame.input.useHandCursor = false;*/

         paytable.loadTexture('paytable_d');
         paytable.inputEnabled = false;
         paytable.input.useHandCursor = false;

         start.loadTexture('start_d');
         start.inputEnabled = false;
         start.input.useHandCursor = false;

     } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]+'_d');
            item[0].inputEnabled = false;
            item[0].input.useHandCursor = false;
        })
    }
}

function showButtonsTypeBORD(buttonsArray) {

    if(buttonsArray.length == 0) {

        if(autostart == false){
            autoplay.loadTexture('autoplay');
            autoplay.inputEnabled = true;
            autoplay.input.useHandCursor = true;

            paytable.loadTexture('paytable');
            paytable.inputEnabled = true;
            paytable.input.useHandCursor = true;

            start.loadTexture('start');
            start.inputEnabled = true;
            start.input.useHandCursor = true;
        }

    } else {
        buttonsArray.forEach(function (item) {
            item[0].loadTexture(item[1]);
            item[0].inputEnabled = true;
            item[0].input.useHandCursor = true;
        })
    }

}

function hideWinSpinResult() {
    clearInterval(switchingLine);

    hideLines([1,2,3,4,5,6,7,8,9,10]);
    squares.forEach(function(item){
        item.forEach(function(item2){
            item2.visible = false;
        });
    });
    hideWinCellAnim(winCellAnim);clearInterval(switchingLine);

    hideLines([1,2,3,4,5,6,7,8,9,10]);
    squares.forEach(function(item){
        item.forEach(function(item2){
            item2.visible = false;
        });
    });
    hideWinCellAnim(winCellAnim);
}

function showCellShadow() {
    slotShadowArray.forEach(function (item) {
        item.visible = true;
    });
}

function hideCellShadow() {
    slotShadowArray.forEach(function (item) {
        item.visible = false;
    });
}
