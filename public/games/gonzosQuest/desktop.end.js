(function(){
	var preload = {};

	preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
        	document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        	needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
        	var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
        	needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
        	var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
        	needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }

        if (location.href.indexOf('slotgames') !== -1) {
        	var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
        	needUrlPath = 'http://slotgames/games/elGallo';
        }

        if (location.href.indexOf('slotgames.dev') !== -1) {
        	var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
        	needUrlPath = 'http://slotgames.dev/games/elGallo';
        }

        var part2Url = '';
        if (location.href.indexOf('ezsl.tk') !== -1) {
        	var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
        	gamename = gamename.substring(0, gamename.indexOf('/?'));
            //part2Url = location.href.substring(location.href.indexOf('?'));
            part2Url = '';
            needUrlPath = 'http://ezsl.tk/games/elGallo';
          }
          if (location.href.indexOf('game.play777games.com') !== -1) {
          	var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
          	gamename = gamename.substring(0, gamename.indexOf('/?'));
          	var part2Url = location.href.substring(location.href.indexOf('?'));
          	needUrlPath = 'https://game.play777games.com/games/' + gamename;
          }


          
          /* bg */
          game.load.image('bg1', needUrlPath + '/img/BG1.png');
          game.load.image('bg2', needUrlPath + '/img/BG2.png');
          game.load.image('BlackBG', needUrlPath + '/img/BlackBG.png');

          /* treeLeavesGroup */
          game.load.image('treeLeavesTopLeft', needUrlPath + '/img/BG/148.png');
          game.load.image('treeLeavesBottomLeft', needUrlPath + '/img/BG/177.png');
          game.load.image('treeLeavesBottomRight', needUrlPath + '/img/BG/178.png');
          game.load.image('treeLeavesBottomRight2', needUrlPath + '/img/BG/179.png');
          game.load.image('rightAncient', needUrlPath + '/img/BG/153.png');
          game.load.image('leftAncient', needUrlPath + '/img/BG/154.png');
          game.load.image('leftAncientFS', needUrlPath + '/img/BG/206.png');
          game.load.image('rightAncientFS', needUrlPath + '/img/BG/207.png');

          /* panelGroup */
          game.load.image('score', needUrlPath + '/img/bottom_line/661.png');
          game.load.image('multiplier', needUrlPath + '/img/numbers/315.png');
          game.load.image('multiplier_top', needUrlPath + '/img/numbers/315_top.png');
          game.load.image('multiplier_topFS', needUrlPath + '/img/numbers/315FS_top.png');
          game.load.image('multiplier_bottom', needUrlPath + '/img/numbers/315_bottom.png');
          game.load.image('multiplier_bottomFS', needUrlPath + '/img/numbers/315FS_bottom.png');
          game.load.image('multiplierGold', needUrlPath + '/img/numbers/340.png');
          game.load.image('buttonPanel', needUrlPath + '/img/desktopButtons/786.png');
          game.load.image('buttonPanelFS', needUrlPath + '/img/desktopButtons/794.png');

          /* buttonGroup */
          game.load.image('fullButton', needUrlPath + '/img/bottom_line/680.png');
          game.load.image('nonFull', needUrlPath + '/img/bottom_line/679.png');
          game.load.image('soundOn', needUrlPath + '/img/bottom_line/3.png');
          game.load.image('soundOn_h', needUrlPath + '/img/bottom_line/3_h.png');
          game.load.image('soundOff', needUrlPath + '/img/bottom_line/2.png');
          game.load.image('soundOff_h', needUrlPath + '/img/bottom_line/2_h.png');
          game.load.image('settings', needUrlPath + '/img/bottom_line/1.png');
          game.load.image('settings_h', needUrlPath + '/img/bottom_line/1_h.png');
          game.load.image('autoStart', needUrlPath + '/img/bottom_line/4.png');
          game.load.image('autoStart_h', needUrlPath + '/img/bottom_line/4_h.png');
          game.load.image('bet_level', needUrlPath + '/img/desktopButtons/bet_level.png');
          game.load.image('bet_level_d', needUrlPath + '/img/desktopButtons/bet_level_d.png');
          game.load.image('bet_level_h', needUrlPath + '/img/desktopButtons/bet_level_h.png');
          game.load.image('bet_level_p', needUrlPath + '/img/desktopButtons/bet_level_p.png');
          game.load.image('coin_value', needUrlPath + '/img/desktopButtons/coin_value.png');
          game.load.image('coin_value_d', needUrlPath + '/img/desktopButtons/coin_value_d.png');
          game.load.image('coin_value_h', needUrlPath + '/img/desktopButtons/coin_value_h.png');
          game.load.image('coin_value_p', needUrlPath + '/img/desktopButtons/coin_value_p.png');
          game.load.image('max_bet', needUrlPath + '/img/desktopButtons/max_bet.png');
          game.load.image('max_bet_d', needUrlPath + '/img/desktopButtons/max_bet_d.png');
          game.load.image('max_bet_h', needUrlPath + '/img/desktopButtons/max_bet_h.png');
          game.load.image('max_bet_p', needUrlPath + '/img/desktopButtons/max_bet_p.png');
          game.load.image('start', needUrlPath + '/img/desktopButtons/start.png');
          game.load.image('start_d', needUrlPath + '/img/desktopButtons/start_d.png');
          game.load.image('start_h', needUrlPath + '/img/desktopButtons/start_h.png');
          game.load.image('start_p', needUrlPath + '/img/desktopButtons/start_p.png');
          game.load.image('Start_free', needUrlPath + '/img/desktopButtons/Start_free.png');
          game.load.image('Start_free_d', needUrlPath + '/img/desktopButtons/Start_free_d.png');
          game.load.image('Start_free_h', needUrlPath + '/img/desktopButtons/Start_free_h.png');
          game.load.image('Start_free_p', needUrlPath + '/img/desktopButtons/Start_free_p.png');
          game.load.image('paytable', needUrlPath + '/img/desktopButtons/paytable.png');
          game.load.image('paytable_h', needUrlPath + '/img/desktopButtons/paytable_h.png');

          /* slotLayer1Group */
          game.load.image('cell0', needUrlPath + '/img/game1/slotValues/cell0.png');
          game.load.image('cell1', needUrlPath + '/img/game1/slotValues/cell1.png');
          game.load.image('cell2', needUrlPath + '/img/game1/slotValues/cell2.png');
          game.load.image('cell3', needUrlPath + '/img/game1/slotValues/cell3.png');
          game.load.image('cell4', needUrlPath + '/img/game1/slotValues/cell4.png');
          game.load.image('cell5', needUrlPath + '/img/game1/slotValues/cell5.png');
          game.load.image('cell6', needUrlPath + '/img/game1/slotValues/cell6.png');
          game.load.image('cell7', needUrlPath + '/img/game1/slotValues/cell7.png');
          game.load.image('cell8', needUrlPath + '/img/game1/slotValues/cell8.png');

          /* slotLayer2Group */
          game.load.image('line1', needUrlPath + '/img/game1/lines/363.png');
          game.load.image('line2', needUrlPath + '/img/game1/lines/363.png');
          game.load.image('line3', needUrlPath + '/img/game1/lines/363.png');
          game.load.image('line4', needUrlPath + '/img/game1/lines/387.png');
          game.load.image('line5', needUrlPath + '/img/game1/lines/390.png');
          game.load.image('line6', needUrlPath + '/img/game1/lines/393.png');
          game.load.image('line7', needUrlPath + '/img/game1/lines/396.png');
          game.load.image('line8', needUrlPath + '/img/game1/lines/399.png');
          game.load.image('line9', needUrlPath + '/img/game1/lines/403.png');
          game.load.image('line10', needUrlPath + '/img/game1/lines/408.png');
          game.load.image('line11', needUrlPath + '/img/game1/lines/413.png');
          game.load.image('line12', needUrlPath + '/img/game1/lines/413.png');
          game.load.image('line13', needUrlPath + '/img/game1/lines/408.png');
          game.load.image('line14', needUrlPath + '/img/game1/lines/396.png');
          game.load.image('line15', needUrlPath + '/img/game1/lines/393.png');
          game.load.image('line16', needUrlPath + '/img/game1/lines/399.png');
          game.load.image('line17', needUrlPath + '/img/game1/lines/403.png');
          game.load.image('line18', needUrlPath + '/img/game1/lines/432.png');
          game.load.image('line19', needUrlPath + '/img/game1/lines/435.png');
          game.load.image('line20', needUrlPath + '/img/game1/lines/438.png');
          game.load.image('square', needUrlPath + '/img/game1/365.png');

          /* numberGroup */
          game.load.image('number1', needUrlPath + '/img/game1/lineNumbers/1.png');
          game.load.image('number2', needUrlPath + '/img/game1/lineNumbers/2.png');
          game.load.image('number3', needUrlPath + '/img/game1/lineNumbers/3.png');
          game.load.image('number4', needUrlPath + '/img/game1/lineNumbers/4.png');
          game.load.image('number5', needUrlPath + '/img/game1/lineNumbers/5.png');
          game.load.image('number6', needUrlPath + '/img/game1/lineNumbers/6.png');
          game.load.image('number7', needUrlPath + '/img/game1/lineNumbers/7.png');
          game.load.image('number8', needUrlPath + '/img/game1/lineNumbers/8.png');
          game.load.image('number9', needUrlPath + '/img/game1/lineNumbers/9.png');
          game.load.image('number10', needUrlPath + '/img/game1/lineNumbers/10.png');
          game.load.image('number11', needUrlPath + '/img/game1/lineNumbers/11.png');
          game.load.image('number12', needUrlPath + '/img/game1/lineNumbers/12.png');
          game.load.image('number13', needUrlPath + '/img/game1/lineNumbers/13.png');
          game.load.image('number14', needUrlPath + '/img/game1/lineNumbers/14.png');
          game.load.image('number15', needUrlPath + '/img/game1/lineNumbers/15.png');
          game.load.image('number16', needUrlPath + '/img/game1/lineNumbers/16.png');
          game.load.image('number17', needUrlPath + '/img/game1/lineNumbers/17.png');
          game.load.image('number18', needUrlPath + '/img/game1/lineNumbers/18.png');
          game.load.image('number19', needUrlPath + '/img/game1/lineNumbers/19.png');
          game.load.image('number20', needUrlPath + '/img/game1/lineNumbers/20.png');

          /* payment*/ 
          game.load.image('page_1', needUrlPath + '/img/payment/page_1.png');
          game.load.image('page_2', needUrlPath + '/img/payment/page_2.png');
          game.load.image('page_3', needUrlPath + '/img/payment/page_3.png');
          game.load.image('right_btn', needUrlPath + '/img/payment/256.png');
          game.load.image('right_btn_h', needUrlPath + '/img/payment/255.png');
          game.load.image('right_btn_p', needUrlPath + '/img/payment/259.png');
          game.load.image('center_btn', needUrlPath + '/img/payment/262.png');
          game.load.image('center_btn_h', needUrlPath + '/img/payment/261.png');
          game.load.image('center_btn_p', needUrlPath + '/img/payment/265.png');
          game.load.image('left_btn', needUrlPath + '/img/payment/268.png');
          game.load.image('left_btn_h', needUrlPath + '/img/payment/267.png');
          game.load.image('left_btn_p', needUrlPath + '/img/payment/271.png');

          /* multiplier numbers X */ 
          game.load.image('x1_b', needUrlPath + '/img/numbers/x1_b.png');
          game.load.image('x2_b', needUrlPath + '/img/numbers/x2_b.png');
          game.load.image('x3_b', needUrlPath + '/img/numbers/x3_b.png');
          game.load.image('x5_b', needUrlPath + '/img/numbers/x5_b.png');
          game.load.image('x6_b', needUrlPath + '/img/numbers/x6_b.png');
          game.load.image('x9_b', needUrlPath + '/img/numbers/x9_b.png');
          game.load.image('x15_b', needUrlPath + '/img/numbers/x15_b.png');

          game.load.image('x1_o', needUrlPath + '/img/numbers/x1_o.png');
          game.load.image('x2_o', needUrlPath + '/img/numbers/x2_o.png');
          game.load.image('x3_o', needUrlPath + '/img/numbers/x3_o.png');
          game.load.image('x5_o', needUrlPath + '/img/numbers/x5_o.png');
          game.load.image('x6_o', needUrlPath + '/img/numbers/x6_o.png');
          game.load.image('x9_o', needUrlPath + '/img/numbers/x9_o.png');
          game.load.image('x15_o', needUrlPath + '/img/numbers/x15_o.png');

          /* winAnim */
          game.load.spritesheet('slot_border_win_1', needUrlPath + '/img/game1/slotAnim/slot_border_win_1.png', 152, 151, 21);
          game.load.spritesheet('slot_border_win_2', needUrlPath + '/img/game1/slotAnim/slot_border_win_2.png', 152, 151, 14);
          game.load.spritesheet('boom', needUrlPath + '/img/game1/graphics/boom(400x400).png', 400, 400, 6);

          /* Soldier Anim*/ 
          game.load.spritesheet('enterSoldier', needUrlPath + '/img/game1/enter/1_200x320_13.png', 200, 320, 13);
          game.load.spritesheet('holdSoldier_1', needUrlPath + '/img/game1/hold/1_1_200x320_20.png', 212, 320, 20);
          game.load.spritesheet('holdSoldier_2', needUrlPath + '/img/game1/hold/1_2_200x320_16.png', 212, 320, 16);
          game.load.spritesheet('ambient_1', needUrlPath + '/img/game1/ambient/1_1(224x320).png', 224, 320, 18);
          game.load.spritesheet('ambient_2', needUrlPath + '/img/game1/ambient/1_2(224x320).png', 224, 320, 18);
          game.load.spritesheet('idle_1', needUrlPath + '/img/game1/idle/4_1(240x336).png', 240, 336, 17);
          game.load.spritesheet('idle_2', needUrlPath + '/img/game1/idle/4_2(240x336).png', 240, 336, 17);
          game.load.spritesheet('idle_3', needUrlPath + '/img/game1/idle/4_3(240x336).png', 240, 336, 17);
          game.load.spritesheet('idle_4', needUrlPath + '/img/game1/idle/4_4(240x336).png', 240, 336, 17);
          game.load.spritesheet('idle_5', needUrlPath + '/img/game1/idle/4_5(240x336).png', 240, 336, 11);
          game.load.spritesheet('freeFalls_1', needUrlPath + '/img/game1/freeFalls/4(240x384).png', 240, 384);
          game.load.spritesheet('freeFalls_2', needUrlPath + '/img/game1/freeFalls/5_1(736x320).png', 736, 320);
          game.load.spritesheet('freeFalls_3', needUrlPath + '/img/game1/freeFalls/5_2(736x320).png', 736, 320);
          game.load.spritesheet('freeFalls_4', needUrlPath + '/img/game1/freeFalls/5_3(736x320).png', 736, 320);
          game.load.spritesheet('freeFalls_5', needUrlPath + '/img/game1/freeFalls/5_4(736x320).png', 736, 320);
          game.load.spritesheet('freeFalls_6', needUrlPath + '/img/game1/freeFalls/5_5(736x320).png', 736, 320);
          game.load.spritesheet('freeFalls_7', needUrlPath + '/img/game1/freeFalls/6_1(172x320).png', 176, 320);
          game.load.spritesheet('freeFalls_8', needUrlPath + '/img/game1/freeFalls/6_2(172x320).png', 176, 320);
          game.load.spritesheet('freeFallsOut_1', needUrlPath + '/img/game1/freeFallsOut/6_1(496x320).png', 496, 320);
          game.load.spritesheet('freeFallsOut_2', needUrlPath + '/img/game1/freeFallsOut/6_2(496x320).png', 496, 320);
          game.load.spritesheet('freeFallsOut_3', needUrlPath + '/img/game1/freeFallsOut/6_3(496x320).png', 496, 320);
          game.load.spritesheet('freeFallsOut_4', needUrlPath + '/img/game1/freeFallsOut/6_4(496x320).png', 496, 320);
          game.load.spritesheet('freeFallsOut_5', needUrlPath + '/img/game1/freeFallsOut/6_5(496x320).png', 496, 320);
          game.load.spritesheet('freeFallsOutCircle_1', needUrlPath + '/img/game1/freeFallsOut/7_1(528x528).png', 528, 528);
          game.load.spritesheet('freeFallsOutCircle_2', needUrlPath + '/img/game1/freeFallsOut/7_2(528x528).png', 528, 528);
          game.load.spritesheet('freeFallsOutCircle_3', needUrlPath + '/img/game1/freeFallsOut/7_3(528x528).png', 528, 528);
          game.load.spritesheet('border_flick', needUrlPath + '/img/desktopButtons/border_flick.png', 136, 35);
          game.load.spritesheet('multiWin', needUrlPath + '/img/game1/graphics/327(200x200).png', 200, 200);
          game.load.spritesheet('free_win_1', needUrlPath + '/img/game1/slotAnim/free_win_1.png', 135, 134);
          game.load.spritesheet('free_win_2', needUrlPath + '/img/game1/slotAnim/free_win_2.png', 135, 134);
          game.load.spritesheet('free_win_3', needUrlPath + '/img/game1/slotAnim/free_win_3.png', 135, 134);
          game.load.spritesheet('smoke', needUrlPath + '/img/game1/graphics/66(110x96).png', 110, 96);
          game.load.spritesheet('bigWin_soldier_1', needUrlPath + '/img/game1/bigWin_anim/5.png', 272, 320);
          game.load.spritesheet('bigWin_soldier_2', needUrlPath + '/img/game1/bigWin_anim/9.png', 464, 320);
          game.load.spritesheet('bigWin_soldier_3', needUrlPath + '/img/game1/bigWin_anim/10_1.png', 336, 320);
          game.load.spritesheet('bigWin_soldier_4', needUrlPath + '/img/game1/bigWin_anim/10_2.png', 336, 320);
          game.load.spritesheet('bigWin_soldier_5', needUrlPath + '/img/game1/bigWin_anim/10_3.png', 336, 320);
          game.load.spritesheet('bigWin_soldier_6', needUrlPath + '/img/game1/bigWin_anim/10_4.png', 336, 320);
          game.load.spritesheet('bigWin_soldier_7', needUrlPath + '/img/game1/bigWin_anim/11.png', 592, 320);
          game.load.spritesheet('bigWin_soldier_8', needUrlPath + '/img/game1/bigWin_anim/12_1.png', 240, 320);
          game.load.spritesheet('bigWin_soldier_9', needUrlPath + '/img/game1/bigWin_anim/12_2.png', 240, 320);
          game.load.spritesheet('bigWin_gold_1', needUrlPath + '/img/game1/bigWin_anim/6_1.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_2', needUrlPath + '/img/game1/bigWin_anim/6_2.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_3', needUrlPath + '/img/game1/bigWin_anim/6_3.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_4', needUrlPath + '/img/game1/bigWin_anim/6_4.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_5', needUrlPath + '/img/game1/bigWin_anim/6_5.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_6', needUrlPath + '/img/game1/bigWin_anim/6_6.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_7', needUrlPath + '/img/game1/bigWin_anim/6_7.png', 1024, 768);
          game.load.spritesheet('bigWin_gold_8', needUrlPath + '/img/game1/bigWin_anim/7_1.png', 368, 768);
          game.load.spritesheet('bigWin_gold_9', needUrlPath + '/img/game1/bigWin_anim/7_2.png', 368, 768);
          game.load.spritesheet('bigWin_gold_10', needUrlPath + '/img/game1/bigWin_anim/7_3.png', 368, 768);

          /* Sound */ 
          game.load.audio('stone_wheel1', needUrlPath + '/sound/game1/1_stone_wheel1.mp3');
          game.load.audio('stone_wheel2', needUrlPath + '/sound/game1/2_stone_wheel2.mp3');
          game.load.audio('stone_wheel3', needUrlPath + '/sound/game1/3_stone_wheel3.mp3');
          game.load.audio('stone_wheel4', needUrlPath + '/sound/game1/4_stone_wheel4.mp3');
          game.load.audio('stone_wheel5', needUrlPath + '/sound/game1/5_stone_wheel5.mp3');
          game.load.audio('Explosion', needUrlPath + '/sound/game1/5_Explosion2.mp3');
          game.load.audio('shake', needUrlPath + '/sound/game1/7_Boom.mp3');
          game.load.audio('WinSnd1', needUrlPath + '/sound/game1/21_WinSnd1.mp3');
          game.load.audio('WinSnd2', needUrlPath + '/sound/game1/13_WinSnd2.mp3');
          game.load.audio('WinSnd3', needUrlPath + '/sound/game1/23_WinSnd3.mp3');
          game.load.audio('attention1Sound', needUrlPath + '/sound/game1/attention1Sound.mp3');
          game.load.audio('attention2Sound', needUrlPath + '/sound/game1/attention2Sound.mp3');
          game.load.audio('attention3Sound', needUrlPath + '/sound/game1/attention3Sound.mp3');
          game.load.audio('bgSound', needUrlPath + '/sound/game1/bgSound.mp3');
          game.load.audio('bgSoundFreeSpin', needUrlPath + '/sound/game1/bgSoundFreeSpin.mp3');
          game.load.audio('btn', needUrlPath + '/sound/game1/btn.mp3');
          game.load.audio('btn_spin', needUrlPath + '/sound/game1/btn_spin.mp3');
          game.load.audio('after_spin', needUrlPath + '/sound/game1/after_spin.mp3');
          game.load.audio('shake', needUrlPath + '/sound/game1/shake.mp3');
          game.load.audio('value_sound', needUrlPath + '/sound/game1/value_sound.mp3');
          game.load.audio('enter', needUrlPath + '/sound/game1/enter.mp3');
          game.load.audio('free_spin_cell', needUrlPath + '/sound/game1/free_spin_cell.mp3');
          game.load.audio('free_spin_soldier', needUrlPath + '/sound/game1/free_spin_soldier.mp3');
          game.load.audio('moving_cell', needUrlPath + '/sound/game1/moving_cell.mp3');
          game.load.audio('idle_1sound', needUrlPath + '/img/game1/idle/sounds/1_Sound3.mp3');
          game.load.audio('idle_2sound', needUrlPath + '/img/game1/idle/sounds/2_Sound2.mp3');
          game.load.audio('idle_3sound', needUrlPath + '/img/game1/idle/sounds/3_Sound1.mp3');
          game.load.audio('freeFall_1sound', needUrlPath + '/img/game1/freeFalls/sounds/1_Sound3.mp3');
          game.load.audio('freeFall_2sound', needUrlPath + '/img/game1/freeFalls/sounds/2_Sound2.mp3');
          game.load.audio('freeFall_3sound', needUrlPath + '/img/game1/freeFalls/sounds/3_Sound1.mp3');
          game.load.audio('freeFallOut_2sound', needUrlPath + '/img/game1/freeFallsOut/sounds/2_Sound4.mp3');
          game.load.audio('freeFallOut_3sound', needUrlPath + '/img/game1/freeFallsOut/sounds/3_Sound3.mp3');
          game.load.audio('freeFallOut_4sound', needUrlPath + '/img/game1/freeFallsOut/sounds/4_Sound2.mp3');
          game.load.audio('freeFallOut_5sound', needUrlPath + '/img/game1/freeFallsOut/sounds/5_Sound1.mp3');
          game.load.audio('bigWin_1sound', needUrlPath + '/img/game1/bigWin_anim/sounds/1_Sound4.mp3');
          game.load.audio('bigWin_2sound', needUrlPath + '/img/game1/bigWin_anim/sounds/2_Sound3.mp3');
          game.load.audio('bigWin_3sound', needUrlPath + '/img/game1/bigWin_anim/sounds/3_Sound2.mp3');
          game.load.audio('bigWin_4sound', needUrlPath + '/img/game1/bigWin_anim/sounds/4_Sound1.mp3');

//game.load.audio('sound16', needUrlPath + '/soundButton/game2/16_sound15.mp3');
//game.load.spritesheet('redButtonAnim', needUrlPath + '/img/desktopButtons/redButtonAnim.png', 170, 52);

};

preload.create = function() {
	game.state.start('game1');
	document.getElementById('preloader').style.display = 'none';
};

game.state.add('preload', preload);

})();

game.state.start('preload');

