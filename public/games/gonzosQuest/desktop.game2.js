var FScount = 10;
function game2() {

    var game2 = {};

    game2.preload = function () {};

    game2.soundStatus = false;
    game2.create = function () {

        game.physics.startSystem(Phaser.Physics.ARCADE);

        checkGame = 2;
        freeSpinAnim = false;
        preFreeSpinAnim = false;
        cellTop = 2;
        countAnimHold = 0;
        xb = [];
        xo = [];
        xb_2 = [];
        xo_2 = [];
        attention = [];
        WinSnd = [];
        stone_wheel = [];
        multiplierValue = 0;
        FScount = 10;
        allWinOldTotal = 0;
        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
        // звуки
        stone_wheel[1] = game.add.audio('stone_wheel1');
        stone_wheel[2] = game.add.audio('stone_wheel2');
        stone_wheel[3] = game.add.audio('stone_wheel3');
        stone_wheel[4] = game.add.audio('stone_wheel4');
        stone_wheel[5] = game.add.audio('stone_wheel5');
        Explosion = game.add.audio('Explosion');
        shake = game.add.audio('shake');
        WinSnd[1] = game.add.audio('WinSnd1');
        WinSnd[2] = game.add.audio('WinSnd2');
        WinSnd[3] = game.add.audio('WinSnd3');
        WinSnd[4] = game.add.audio('WinSnd3');
        attention[1] = game.add.audio('attention1Sound');
        attention[2] = game.add.audio('attention2Sound');
        attention[3] = game.add.audio('attention3Sound');
        bgSoundFreeSpin = game.add.audio('bgSoundFreeSpin');
        bgSoundFreeSpin.loop = true;
        btn = game.add.audio('btn');
        btn_spin = game.add.audio('btn_spin');
        after_spin = game.add.audio('after_spin');
        shake = game.add.audio('shake');
        value_sound = game.add.audio('value_sound');
        enter = game.add.audio('enter');
        idle_1sound = game.add.audio('idle_1sound');
        idle_2sound = game.add.audio('idle_2sound');
        idle_3sound = game.add.audio('idle_3sound');        
        freeFallOut_2sound = game.add.audio('freeFallOut_2sound');
        freeFallOut_3sound = game.add.audio('freeFallOut_3sound');
        freeFallOut_4sound = game.add.audio('freeFallOut_4sound');
        freeFallOut_5sound = game.add.audio('freeFallOut_5sound');
        free_spin_cell = game.add.audio('free_spin_cell');
        free_spin_soldier = game.add.audio('free_spin_soldier');
        moving_cell = game.add.audio('moving_cell');
        bgSoundFreeSpin.play();

        // изображения
        var bg2 = game.add.sprite(0,0, 'bg2');
        BlackBG = game.add.sprite(0,0, 'BlackBG');  
        BlackBG.alpha = 1;
        var i = 0;
        (function() {
            if (i < 10) {
                BlackBG.alpha -= 0.1;
                i++;
                setTimeout(arguments.callee, 200);
            } else {
                unlockDisplay();
            }
        })();
        buttonPanelFS = game.add.sprite(0,548, 'buttonPanelFS');
        game.physics.enable(buttonPanelFS, Phaser.Physics.ARCADE);
        buttonPanelFS.enableBody = true;
        buttonPanelFS.body.immovable = true;
        buttonPanelFS.body.collideWorldBounds = true;
        border_flick = game.add.sprite(874,633, 'border_flick');
        border_flick.visible = false;
        // border_flick2 = game.add.sprite(874,579, 'border_flick');
        // border_flick2.visible = false;
        // buttonPanel.body.checkCollision.up = false;
        // buttonPanel.body.checkCollision.down = false;
        var score = game.add.sprite(0,738, 'score');
        multiplierGold = game.add.sprite(538, 3, 'multiplierGold');
        multiWin = game.add.sprite(616, -41, 'multiWin');
        multiWin.visible = false;

        xb[0]  = game.add.sprite(571-5, 44, 'x3_b');
        xb[0].visible = false;
        xb[1]  = game.add.sprite(656, 44, 'x6_b');
        xb[2]  = game.add.sprite(748, 44, 'x9_b');
        xb[3]  = game.add.sprite(838-7, 44, 'x15_b');

        xo[0]  = game.add.sprite(571-5, 44, 'x3_o');
        // xo[0].visible = false;
        xo[1]  = game.add.sprite(656, 44, 'x6_o');
        xo[1].visible = false;
        xo[2]  = game.add.sprite(748, 44, 'x9_o');
        xo[2].visible = false;
        xo[3]  = game.add.sprite(838-7, 44, 'x15_o');
        xo[3].visible = false;

        //multi anim

        multiplier_2 = game.add.sprite(538, 3, 'multiplierGold');
        multiplier_topFS = game.add.sprite(541, 0, 'multiplier_topFS');
        multiplier_bottomFS = game.add.sprite(541, 93, 'multiplier_bottomFS');

        xb_2[0]  = game.add.sprite(571-5, 96, 'x3_b');
        xb_2[1]  = game.add.sprite(656, 44, 'x6_b');
        xb_2[2]  = game.add.sprite(748, 44, 'x9_b');
        xb_2[3]  = game.add.sprite(838-7, 44, 'x15_b');

        xo_2[0]  = game.add.sprite(571-5, 44, 'x3_o');
        xo_2[1]  = game.add.sprite(656, -8, 'x6_o');
        xo_2[2]  = game.add.sprite(748, -8, 'x9_o');
        xo_2[3]  = game.add.sprite(838-7, -8, 'x15_o');

        enterSoldier = game.add.sprite(0,312, 'enterSoldier');        
        enterSoldierAnim = enterSoldier.animations.add('enterSoldier', [], 10, false);
        holdSoldier_1 = game.add.sprite(0,314, 'holdSoldier_1');
        holdSoldier_1.visible = false;
        holdSoldier_1Anim = holdSoldier_1.animations.add('holdSoldier_1', [], 10, false);
        holdSoldier_1Anim.onComplete.add(function () {
            holdSoldier_1.visible = false;
            holdSoldier_2 = game.add.sprite(0,314, 'holdSoldier_2');
            if (freeSpinAnim !== true)
                holdSoldier_2.visible = true;
            slotLayer3Group.add(holdSoldier_2);
            holdSoldier_2.animations.add('holdSoldier_2', [], 10, false).play().onComplete.add(function () {
                countAnimHold = countAnimHold + 1;
                holdSoldier_2.visible = false;
                if (freeSpinAnim !== true){

                    if (winAnim){
                        randAnimWin()
                    } else {                    
                    // Рандомайз hold
                    if (countAnimHold > 2 & FScount > 0){
                        randAnimHold();
                    } else {
                        holdSoldier_1.visible = true;
                        holdSoldier_1.animations.getAnimation('holdSoldier_1').play();
                    }
                }

                // Запуск анимации для перехода в фриспины
                // freeFalls_1.visible = true;
                // freeFalls_1Anim.play();
                // freeFall_1sound.play();                
                // setTimeout(function(){            
                //     freeFall_2sound.play()
                // }, 700);
                // Запуск анимации с колесом
                // freeFallsOut_1.visible = true;
                // freeFallsOut_1Anim.play();

            }
        });
        });
        ambient_1 = game.add.sprite(0,314, 'ambient_1');
        ambient_1.visible = false;        
        ambient_1Anim = ambient_1.animations.add('ambient_1', [], 10, false);
        ambient_1Anim.onComplete.add(function () {
            ambient_1.visible = false;
            ambient_2 = game.add.sprite(0,314, 'ambient_2');
            ambient_2.visible = true;
            slotLayer3Group.add(ambient_2);
            ambient_2.animations.add('ambient_2', [], 10, false).play().onComplete.add(function () {
                ambient_2.visible = false;
                if (freeSpinAnim === true){
                    freeFalls_1.visible = true;
                    freeFalls_1Anim.play();
                } else {
                    holdSoldier_1.visible = true;
                    holdSoldier_1Anim.play();
                    holdanim = true;
                }
            });
        });
        idle_1 = game.add.sprite(0,307, 'idle_1');
        idle_1.visible = false;
        idle_1Anim = idle_1.animations.add('idle_1', [], 10, false);
        idle_1Anim.onComplete.add(function () {
            idle_1.visible = false;
            idle_2 = game.add.sprite(0,307, 'idle_2');        
            idle_2.visible = true;
            slotLayer3Group.add(idle_2);
            setTimeout(function(){            
                idle_2sound.play();
            }, 1400);
            idle_2.animations.add('idle_2', [], 10, false).play().onComplete.add(function () {
                idle_2.visible = false;
                idle_3 = game.add.sprite(0,307, 'idle_3');
                idle_3.visible = true;
                slotLayer3Group.add(idle_3);
                idle_3.animations.add('idle_3', [], 10, false).play().onComplete.add(function () {
                    idle_3.visible = false;
                    idle_4 = game.add.sprite(0,307, 'idle_4');
                    idle_4.visible = true;
                    slotLayer3Group.add(idle_4);
                    setTimeout(function(){            
                        idle_3sound.play();
                    }, 1100);
                    idle_4.animations.add('idle_4', [], 10, false).play().onComplete.add(function () {
                        idle_4.visible = false;
                        idle_5 = game.add.sprite(0,307, 'idle_5');
                        idle_5.visible = true;
                        slotLayer3Group.add(idle_5);
                        idle_5.animations.add('idle_5', [], 10, false).play().onComplete.add(function () {
                            idle_5.visible = false;
                            if (freeSpinAnim === true){
                                freeFallsOut_1.visible = true;
                                freeFallsOut_1Anim.play();          
                            } else {
                                holdSoldier_1.visible = true;
                                holdSoldier_1Anim.play();
                                holdanim = true;
                            }
                        });
                    });
                });
            });
        });
        freeFallsOut_1 = game.add.sprite(0,320, 'freeFallsOut_1');
        freeFallsOut_1.visible = false;
        freeFallsOut_1Anim = freeFallsOut_1.animations.add('freeFallsOut_1', [], 10, false);
        freeFallsOut_1Anim.onComplete.add(function () {
            freeFallsOut_1.visible = false;
            freeFallsOut_2 = game.add.sprite(0,320, 'freeFallsOut_2');        
            freeFallsOut_2.visible = true;
            slotLayer3Group.add(freeFallsOut_2);
            freeFallsOut_2.animations.add('freeFallsOut_2', [], 10, false).play().onComplete.add(function () {
                freeFallsOut_2.visible = false;
                freeFallsOut_3 = game.add.sprite(0,320, 'freeFallsOut_3');
                freeFallsOut_3.visible = true;
                slotLayer3Group.add(freeFallsOut_3);
                freeFallsOut_3.animations.add('freeFallsOut_3', [], 10, false).play().onComplete.add(function () {
                    freeFallsOut_3.visible = false;
                    freeFallsOut_4 = game.add.sprite(0,320, 'freeFallsOut_4');
                    freeFallsOut_4.visible = true;
                    slotLayer3Group.add(freeFallsOut_4);
                    setTimeout(function(){
                        freeFallOut_3sound.play();
                    }, 300);
                    freeFallsOut_4.animations.add('freeFallsOut_4', [], 10, false).play().onComplete.add(function () {
                        freeFallsOut_4.visible = false;
                        freeFallsOut_5 = game.add.sprite(0,320, 'freeFallsOut_5');
                        freeFallsOut_5.visible = true;
                        slotLayer3Group.add(freeFallsOut_5);
                        freeFallsOutCircle_1.visible = true;
                        freeFallsOutCircle_1Anim.play();
                        freeFallOut_4sound.play();
                        freeFallOut_5sound.play();
                        game.add.tween(freeFallsOutCircle_1).to({x:-528}, 1500,Phaser.Easing.LINEAR, true);
                        game.add.tween(freeFallsOutCircle_2).to({x:-528}, 1500,Phaser.Easing.LINEAR, true);
                        game.add.tween(freeFallsOutCircle_3).to({x:-528}, 1500,Phaser.Easing.LINEAR, true);
                        freeFallsOut_5.animations.add('freeFallsOut_5', [], 10, false).play().onComplete.add(function () {
                            freeFallsOut_5.visible = false;
                        });
                    });
                });
            });
        });

        freeFallsOutCircle_1 = game.add.sprite(1024,90, 'freeFallsOutCircle_1');
        freeFallsOutCircle_2 = game.add.sprite(1024,90, 'freeFallsOutCircle_2');        
        freeFallsOutCircle_3 = game.add.sprite(1024,90, 'freeFallsOutCircle_3');
        freeFallsOutCircle_1.visible = false;
        freeFallsOutCircle_2.visible = false;
        freeFallsOutCircle_3.visible = false;
        freeFallsOutCircle_1Anim = freeFallsOutCircle_1.animations.add('freeFallsOutCircle_1', [], 10, false);
        freeFallsOutCircle_1Anim.onComplete.add(function () {
            freeFallsOutCircle_1.visible = false;
            freeFallsOutCircle_2.visible = true;
            slotLayer3Group.add(freeFallsOutCircle_2);
            freeFallsOutCircle_2.animations.add('freeFallsOutCircle_2', [], 10, false).play().onComplete.add(function () {
                freeFallsOutCircle_2.visible = false;
                freeFallsOutCircle_3.visible = true;
                slotLayer3Group.add(freeFallsOutCircle_3);
                freeFallsOutCircle_3.animations.add('freeFallsOutCircle_3', [], 10, false).play().onComplete.add(function () {
                    freeFallsOutCircle_3.visible = false;
                    BlackBG = game.add.sprite(0,0, 'BlackBG');    
                    treeLeavesGroup2.add(BlackBG);
                    BlackBG.alpha = 0;
                    var i = 0;
                    (function() {
                        if (i < 10) {
                            BlackBG.alpha += 0.1;
                            i++;
                            setTimeout(arguments.callee, 200);
                        } else {
                            afterFS = true;
                            game.state.start('game1');
                            bgSoundFreeSpin.stop();
                        }
                    })();
                });
            });
        });
        //слоты
        cellLayer1Position = [
        [176, 109],
        [176, 261],
        [176, 413],
        [328, 109],
        [328, 261],
        [328, 413],
        [480, 109],
        [480, 261],
        [480, 413],
        [632, 109],
        [632, 261],
        [632, 413],
        [784, 109],
        [784, 261],
        [784, 413]
        ];
        addCell(game, cellLayer1Position, 8);
        addCell2(game, cellLayer1Position, 8);
        
        // линии и номера
        var linePosition = [
        [190,335],
        [190,180],
        [190,485],
        [198,120],
        [193,185],
        [197,180],
        [191,317],
        [191,330],
        [193,180],
        [195,180],
        [195,330],
        [195,170],
        [195,320],
        [195,175],
        [195,335],
        [195,180],
        [195,330],
        [195,185],
        [195,185],
        [195,190]
        ];
        var numberPosition = [
        [[158,283], [158,148], [158,463], [158,103], [158,508], [0,0], [0,0], [158,373], [0,0], [158,238],[0,0], [0,0], [158,418], [0,0], [158,328], [158,193], [0,0], [0,0], [0,0], [0,0]],
        [[0,0], [0,0], [0,0], [0,0], [0,0], [915,148], [915,463], [0,0], [915,283], [0,0],[915,373], [915,238], [0,0], [915,328], [0,0], [0,0], [915,418], [915,508], [915,103], [915,193]]
        ];
        var squarePosition = [
        [177,110],
        [177,262],
        [177,414],
        [329,110],
        [329,262],
        [329,414],
        [481,110],
        [481,262],
        [481,414],
        [633,110],
        [633,262],
        [633,414],
        [785,110],
        [785,262],
        [785,414]
        ]; 
        var hoverSquarePostion = [
        [2,5,8,11,14],
        [1,4,7,10,13],
        [3,6,9,12,15],
        [1,5,9,11,13],
        [3,5,7,11,15],
        [1,4,8,10,13],
        [3,6,8,12,15],
        [2,6,9,12,14],
        [2,4,7,10,14],
        [2,4,8,10,14],
        [2,6,8,12,14],
        [1,5,7,11,13],
        [3,5,9,11,15],
        [2,5,7,11,14],
        [2,5,9,11,14],
        [1,5,8,11,13],
        [3,5,8,11,15],
        [1,5,9,12,15],
        [3,5,7,10,13],
        [1,6,7,12,13]
        ];
        addNumbers(game, numberPosition);
        addLines(game, linePosition);
        addSquares(game, squarePosition);
        addHoverForNumbers(hoverSquarePostion, numberPosition);
        hideLines();
        hideSquares();
        var treeLeavesTopLeft = game.add.sprite(0,0, 'treeLeavesTopLeft');
        var treeLeavesBottomRight2 = game.add.sprite(852,437, 'treeLeavesBottomRight2');
        var rightAncientFS = game.add.sprite(931,0, 'rightAncientFS');
        var leftAncientFS = game.add.sprite(0,0, 'leftAncientFS');

        // кнопки
        addButtonsGame2(game);

        //счет
        var betScorePosion = [[120, 638, 20], [300, 711, 15], [460, 711, 15], [625, 711, 15], [840, 711, 15], [885, 638, 20]];
        addBetScore(game, betScorePosion, bet, betline, betLines, coins, coinsValue, win);
        addFSText();
        var mainScorePosition = [[285, 745, 15], [518, 745, 15], [685, 745, 15]];
        addMainScore(game, mainScorePosition, cash, mainBet, mainWin);

        multiplierGroup = game.add.group();
        multiplierGroup.add(multiplier_2);
        multiplierGroup.add(xb_2[0]);
        multiplierGroup.add(xb_2[1]);
        multiplierGroup.add(xb_2[2]);
        multiplierGroup.add(xb_2[3]);
        multiplierGroup.add(xo_2[0]);
        multiplierGroup.add(xo_2[1]);
        multiplierGroup.add(xo_2[2]);
        multiplierGroup.add(xo_2[3]);
        multiplierGroup.add(multiplier_topFS);
        multiplierGroup.add(multiplier_bottomFS);

        slotLayer1Group = game.add.group();
        cellArray.forEach(function (item) {
            slotLayer1Group.add(item);
        });        
        cellArray2.forEach(function (item) {
            slotLayer1Group.add(item);
        });
        slotLayer1Group.enableBody = true;

        slotLayer2Group = game.add.group();
        squaresArray.forEach(function (item) {
            slotLayer2Group.add(item);
        });
        linesArray.forEach(function (item) {
            slotLayer2Group.add(item);
        });

        treeLeavesGroup = game.add.group();
        treeLeavesGroup.add(rightAncientFS);
        treeLeavesGroup.add(leftAncientFS);
        treeLeavesGroup.add(treeLeavesBottomRight2);
        var cellPosition = [
        [176+8, 109+8],
        [176+8, 261+8],
        [176+8, 413+8],
        [328+8, 109+8],
        [328+8, 261+8],
        [328+8, 413+8],
        [480+8, 109+8],
        [480+8, 261+8],
        [480+8, 413+8],
        [632+8, 109+8],
        [632+8, 261+8],
        [632+8, 413+8],
        [784+8, 109+8],
        [784+8, 261+8],
        [784+8, 413+8]
        ];
        addCell3(game, cellLayer1Position, 8);
        CellWinAnim(game, cellLayer1Position);
        CellFreeSpinAnim(game, cellPosition);
        BoomAnim(game, cellLayer1Position);
        
        slotLayer3Group = game.add.group();
        slotLayer3Group.add(multiplierGold);
        slotLayer3Group.add(xb[0]);
        slotLayer3Group.add(xb[1]);
        slotLayer3Group.add(xb[2]);
        slotLayer3Group.add(xb[3]);
        slotLayer3Group.add(xo[0]);
        slotLayer3Group.add(xo[1]);
        slotLayer3Group.add(xo[2]);
        slotLayer3Group.add(xo[3]);
        slotLayer3Group.add(multiWin);
        leftNumbers.forEach(function (item) {
            slotLayer3Group.add(item);
        });
        rightNumbers.forEach(function (item) {
            slotLayer3Group.add(item);
        });
        slotLayer3Group.add(enterSoldier);
        slotLayer3Group.add(holdSoldier_1);
        slotLayer3Group.add(ambient_1);
        slotLayer3Group.add(idle_1);
        slotLayer3Group.add(freeFallsOut_1);
        slotLayer3Group.add(freeFallsOutCircle_1);

        paytableGroup = game.add.group();
        paytableGroup.add(paytable);
        paytableGroup.add(pagePaytables[1]);
        paytableGroup.add(pagePaytables[2]);
        paytableGroup.add(pagePaytables[3]);
        paytableGroup.add(left_btn);
        paytableGroup.add(center_btn);
        paytableGroup.add(right_btn);

        panelGroup = game.add.group();
        panelGroup.add(buttonPanelFS);
        panelGroup.add(score);
        panelGroup.add(border_flick);
        // panelGroup.add(border_flick2);

        scoreGroup = game.add.group();
        scoreGroup.add(betText);
        scoreGroup.add(betLvlText);
        scoreGroup.add(betLineText);
        scoreGroup.add(coinsText);
        scoreGroup.add(coinsValueText);
        scoreGroup.add(winText);
        scoreGroup.add(winTextTotal);
        scoreGroup.add(cashText);
        scoreGroup.add(mainBetText);
        scoreGroup.add(mainWinText);
        scoreGroup.add(FStext);
        scoreGroup.add(FSwords);

        buttonGroup = game.add.group();
        // buttonGroup.add(start);
        buttonGroup.add(startFree);
        buttonGroup.add(fullButton);
        buttonGroup.add(soundButton);
        buttonGroup.add(settings);
        buttonGroup.add(autoStart);

        treeLeavesGroup2 = game.add.group();
        treeLeavesGroup2.add(treeLeavesTopLeft);
        treeLeavesGroup2.add(BlackBG);
        // platforms = game.add.group();


        // platforms.enableBody = true;
        // boom = game.add.sprite(176-124, 109-90, 'boom');
        // boom.animations.add('slot_border_win_1', [0,1,2,3,4,5], 3, true).play();


        // slot_border_win_1 = game.add.sprite(176, 109, 'slot_border_win_1');
        // slot_border_win_2 = game.add.sprite(176, 109, 'slot_border_win_2');
        // slot_border_win_2.visible = false;
        // slot_border_win_1.animations.add('slot_border_win_1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 15, false).play().onComplete.add(function(){
        //     slot_border_win_1.visible = false;
        //     slot_border_win_2.visible = true;
        //     slot_border_win_2.animations.add('slot_border_win_2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 15, false).play().onComplete.add(function(){
        //     });
        // });
        startFree.loadTexture('Start_free_d'); 
        startFree.inputEnabled = false;
        startFree.input.useHandCursor = false;
        setTimeout(function(){
            enterAnim();
            startFree.loadTexture('Start_free'); 
            startFree.inputEnabled = true;
            startFree.input.useHandCursor = true;
        }, 1500);

    };

    game2.update = function () {
        for (var i = 0; i < 15; i++) {
            if (cellTop == 1 & firstSpin == true){
                if (flyEnable[i] == true){
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 1;
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y * 1.1;
                }
                if (flyEnable2[i] == true){
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 1;
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y * 1.1;
                }
            }
            if (cellTop == 2 & firstSpin == true){
                if (flyEnable[i] == true){
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 1;
                    cellArray[i].body.velocity.y = cellArray[i].body.velocity.y * 1.1;
                }
                if (flyEnable2[i] == true){
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 1;
                    cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y * 1.1;
                }
            }
        }
        for (var i = 0; i < 15; i++) {
            if(cellTop == 2 & flyEnable2[i] == true ){ 
                if (cellArray2[i].position.y >= cellLayer1Position[i][1]){
                    flyEnable[i] = false;
                    flyEnable2[i] = false;
                    cellArray2[i].body.velocity.y = 0;
                    cellArray2[i].position.y = cellLayer1Position[i][1];                 
                    cellArray[i].body.velocity.y = 0;
                    rotateCell(cellArray2[i], i);
                    if (flyEnable2[12] == false){
                        cellTop = 1;                                     
                        console.log('Первый массив на верху'); 
                    }
                }
            } 
            if(cellTop == 1 & flyEnable[i] == true){  
                if (cellArray[i].position.y >= cellLayer1Position[i][1]){
                    flyEnable[i] = false;
                    flyEnable2[i] = false;
                    cellArray[i].body.velocity.y = 0;
                    cellArray[i].position.y = cellLayer1Position[i][1];
                    cellArray2[i].body.velocity.y = 0;
                    rotateCell(cellArray[i], i);
                    if (flyEnable[12] == false){
                        cellTop = 2; 
                        console.log('Второй массив на верху');  
                    }
                }
            }
        }   
        for (var i = 0; i < 15; i++) {
            if (flyEnable3[i] > 0 & cellTop == 2 & flyEnable[i] == true){
                cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40; 
            }
            if (flyEnable3[i] > 0 & cellTop == 1 & flyEnable2[i] == true){
                cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            }
        }
        for (var i = 0; i < 15; i++) {
            if (flyEnable[i] == true & firstSpin == false & cellTop == 1){
                cellArray[i].body.velocity.y = cellArray[i].body.velocity.y + 40; 
            }
            if (flyEnable2[i] == true & firstSpin == false & cellTop == 2){
                cellArray2[i].body.velocity.y = cellArray2[i].body.velocity.y + 40;
            }
        }
        for (var i = 0; i < 15; i++) {
            if(cellTop == 2){ 
                if(flyEnable3[i] === 1 & flyEnable[i] == true){
                    if (cellArray[i].position.y >= cellLayer1Position[i+1][1]){
                        flyEnable[i] = false;                        
                        cellArray[i].body.velocity.y = 0;
                        cellArray[i].position.y = cellLayer1Position[i+1][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                } else if(flyEnable3[i] === 2 & flyEnable[i] == true){
                    if (cellArray[i].position.y >= cellLayer1Position[i+2][1]){
                        flyEnable[i] = false;
                        cellArray[i].body.velocity.y = 0;
                        cellArray[i].position.y = cellLayer1Position[i+2][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                }
            } else {
                if(flyEnable3[i] === 1 & flyEnable2[i] == true){
                    if (cellArray2[i].position.y >= cellLayer1Position[i+1][1]){
                        flyEnable2[i] = false;
                        cellArray2[i].body.velocity.y = 0;
                        cellArray2[i].position.y = cellLayer1Position[i+1][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray2[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                } else if(flyEnable3[i] === 2 & flyEnable2[i] == true){
                    if (cellArray2[i].position.y >= cellLayer1Position[i+2][1]){
                        flyEnable2[i] = false;
                        cellArray2[i].body.velocity.y = 0;
                        cellArray2[i].position.y = cellLayer1Position[i+2][1];
                        // flyEnable3[i] = 0;
                        rotateCellNew(cellArray2[i], i);
                        // if (flyEnable2[12] == false){
                        //     cellTop = 1;                      
                        // }
                    }
                }
            }
        } 
    };
    game.state.add('game2', game2);
};
