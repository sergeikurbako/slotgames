var game = new Phaser.Game(1500, 1024, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        // звуки
        // заполняем массив звуками выигрышей
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');

        soundWinLineDurations[0] = 1000;
        soundWinLineDurations[1] = 1000;
        soundWinLineDurations[2] = 6000;
        soundWinLineDurations[3] = 1000;


        // изображения
        var border = game.add.sprite(-270,0, 'border');

        var lineNumber1left = game.add.sprite(45,436, 'number1left');
        var lineNumber2left = game.add.sprite(45,250, 'number2left');
        var lineNumber3left = game.add.sprite(45,645, 'number3left');
        var lineNumber4left = game.add.sprite(45,185, 'number4left');
        var lineNumber5left = game.add.sprite(45,710, 'number5left');
        var lineNumber1right = game.add.sprite(1385,435, 'number1right');
        var lineNumber2right = game.add.sprite(1385,250, 'number2right');
        var lineNumber3right = game.add.sprite(1385,645, 'number3right');
        var lineNumber4right = game.add.sprite(1385,185, 'number4right');
        var lineNumber5right = game.add.sprite(1385,710, 'number5right');

        //слоты
        totalWinPosion = [200,850];
        slotPosition = [[155, 142],
                        [155, 364],
                        [155, 586],
                            [399, 142],
                            [399, 364],
                            [399, 586],
                                [643, 142],
                                [643, 364],
                                [643, 586],
                                    [887, 142],
                                    [887, 364],
                                    [887, 586],
                                        [1131, 142],
                                        [1131, 364],
                                        [1131, 586]];
        addSlots(game, slotPosition, 8);

        // линии
        linePosition = [[95+23,457], [95+23,272], [95+23,668], [120,208], [120 ,230]];
        addLinesTypeBOR(game, linePosition);

        //позиции ячеек относящиеся к линиям
        cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
        slotAnimPosition = slotPosition; //позиции анимированных ячеек
        
        //создаем массив содержащий все анимации
        addAllCellAnim(game, slotPosition, 8);

        //цветные рамки для ячеек
        addSquares(game, slotPosition, 11);

        // кнопки
        addButtonsGame1TypeBORDelux(game);
        
        var groupLineNumbers = game.add.group();
        groupLineNumbers.add(lineNumber1left);
        groupLineNumbers.add(lineNumber2left);
        groupLineNumbers.add(lineNumber3left);
        groupLineNumbers.add(lineNumber4left);
        groupLineNumbers.add(lineNumber5left);
        groupLineNumbers.add(lineNumber1right);
        groupLineNumbers.add(lineNumber2right);
        groupLineNumbers.add(lineNumber3right);
        groupLineNumbers.add(lineNumber4right);
        groupLineNumbers.add(lineNumber5right);
        game.world.bringToTop(groupLineNumbers);

        //изображения для кнопок и надписей
        var status = game.add.sprite(150,830, 'status');
        var shadow_btn = game.add.sprite(150,925, 'shadow_btn');

        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[750, 962, 28], [391, 962, 28], [180, 962, 28], [580, 962, 28], [580, 962, 28]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        addTitles();

        full_and_sound();

    };

    game1.update = function () {
        
    };

    game.state.add('game1', game1);

};
