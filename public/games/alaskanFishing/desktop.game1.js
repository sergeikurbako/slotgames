//локация 1
game1 = {
	pointX : [620, 708, 868, 988, 1013, 1015, 1017, 1019, 1021, 1023, 1025 ],
	pointY : [423, 234, 154, 164, 176, 181, 186, 191, 196, 201, 206 ],
	PicksCount : 5,
	CongCount : null,
	create:function(){
		game.stage.disableVisibilityChange = true;
		checkGame = 1;
		sound = game.add.audio('sound');
		sound.allowMultiple = true;
		sound.addMarker('fishing_rod', 132.9, 4.62);
		sound.addMarker('BigWin', 106.5, 7.5);
		sound.addMarker('MedWin', 114, 7.5);
		sound.addMarker('LowWin', 121.5, 7.5);
		sound.addMarker('Final_Summary', 95.5, 5.2);
		sound.addMarker('Pick5Spots_Intro', 101, 5.5);
		sound.addMarker('Spot_Select', 129.05, 1.5);
		sound.addMarker('Transition', 138.15, 4.70);
		sound.addMarker('FreeSpin_Background', 3, 29.6);
		sound.addMarker('Fish_Transition', 138.15, 4.7);
		sound.play('FreeSpin_Background', 3, 29.6, true, true);
		sound.play('Fish_Transition');

		point = new Phaser.Point(620, 423);
		game1.bg = game.add.sprite(0,0, 'BonusBackground');
		game1.TotalWinPlaque = game.add.sprite(425,670, 'TotalWinPlaque');

		var Spot1 = game.add.sprite(50, 550, 'Spot');
		Spot1.animations.add('Spot');
		Spot1.animations.play('Spot', 13, true);
		Spot1.inputEnabled = true;
		Spot1.input.useHandCursor = true;
		Spot1.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot1.loadTexture('Spot2');
			Spot1.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot1.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot1.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left.visible = true;
					dog_idle.visible = false;
					dog_look_left.animations.play('dog_look_left', 3, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left.visible = false;
						dog_look_right.animations.play('dog_look_right', 5, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 80;
					fish1.position.y  = 500;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot2 = game.add.sprite(20, 410, 'Spot');
		Spot2.scale.setTo(0.8, 0.8);
		Spot2.animations.add('Spot');
		Spot2.animations.play('Spot', 13, true);
		Spot2.inputEnabled = true;
		Spot2.input.useHandCursor = true;
		Spot2.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot2.loadTexture('Spot2');
			Spot2.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot2.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot2.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left.visible = true;
					dog_idle.visible = false;
					dog_look_left.animations.play('dog_look_left', 3, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left.visible = false;
						dog_look_right.animations.play('dog_look_right', 5, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 35;
					fish1.position.y  = 360;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot3 = game.add.sprite(60, 300, 'Spot');
		Spot3.scale.setTo(0.6, 0.6);
		Spot3.animations.add('Spot');
		Spot3.animations.play('Spot', 13, true);
		Spot3.inputEnabled = true;
		Spot3.input.useHandCursor = true;
		Spot3.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot3.loadTexture('Spot2');
			Spot3.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot3.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot3.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left.visible = true;
					dog_idle.visible = false;
					dog_look_left.animations.play('dog_look_left', 3, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left.visible = false;
						dog_look_right.animations.play('dog_look_right', 5, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 60;
					fish1.position.y  = 250;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot4 = game.add.sprite(345, 470, 'Spot');
		Spot4.animations.add('Spot');
		Spot4.animations.play('Spot', 13, true);
		Spot4.inputEnabled = true;
		Spot4.input.useHandCursor = true;
		Spot4.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot4.loadTexture('Spot2');
			Spot4.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot4.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot4.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left_1.visible = true;
					dog_idle.visible = false;
					dog_look_left_1.animations.play('dog_look_left_1', 6, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left_1.visible = false;
						dog_look_right.animations.play('dog_look_right', 4, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 375;
					fish1.position.y  = 420;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot5 = game.add.sprite(320, 350, 'Spot');
		Spot5.scale.setTo(0.8, 0.8);
		Spot5.animations.add('Spot');
		Spot5.animations.play('Spot', 13, true);
		Spot5.inputEnabled = true;
		Spot5.input.useHandCursor = true;
		Spot5.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot5.loadTexture('Spot2');
			Spot5.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot5.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot5.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left_1.visible = true;
					dog_idle.visible = false;
					dog_look_left_1.animations.play('dog_look_left_1', 6, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left_1.visible = false;
						dog_look_right.animations.play('dog_look_right', 4, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 335;
					fish1.position.y  = 300;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot6 = game.add.sprite(360, 250, 'Spot');
		Spot6.scale.setTo(0.6, 0.6);
		Spot6.animations.add('Spot');
		Spot6.animations.play('Spot', 13, true);
		Spot6.inputEnabled = true;
		Spot6.input.useHandCursor = true;
		Spot6.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot6.loadTexture('Spot2');
			Spot6.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot6.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot6.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_look_left_1.visible = true;
					dog_idle.visible = false;
					dog_look_left_1.animations.play('dog_look_left_1', 6, false).onComplete.add(function(){
						dog_look_right.visible = true;
						dog_look_left_1.visible = false;
						dog_look_right.animations.play('dog_look_right', 4, false).onComplete.add(function(){
							dog_look_right.visible = false;
							dog_idle.visible = true;
						});
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 360;
					fish1.position.y  = 200;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot7 = game.add.sprite(640, 390, 'Spot');
		Spot7.animations.add('Spot');
		Spot7.animations.play('Spot', 13, true);
		Spot7.inputEnabled = true;
		Spot7.input.useHandCursor = true;
		Spot7.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot7.loadTexture('Spot2');
			Spot7.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot7.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot7.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_idle.visible = false;
					dog_look_right_1.visible = true;
					dog_look_right_1.animations.play('dog_look_right_1', 2, false).onComplete.add(function(){
						dog_look_right_1.visible = false;
						dog_idle.visible = true;
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 670;
					fish1.position.y  = 340;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot8 = game.add.sprite(620, 290, 'Spot');
		Spot8.scale.setTo(0.8, 0.8);
		Spot8.animations.add('Spot');
		Spot8.animations.play('Spot', 13, true);
		Spot8.inputEnabled = true;
		Spot8.input.useHandCursor = true;
		Spot8.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot8.loadTexture('Spot2');
			Spot8.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot8.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot8.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_idle.visible = false;
					dog_look_right_1.visible = true;
					dog_look_right_1.animations.play('dog_look_right_1', 2, false).onComplete.add(function(){
						dog_look_right_1.visible = false;
						dog_idle.visible = true;
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 635;
					fish1.position.y  = 240;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		var Spot9 = game.add.sprite(660, 230, 'Spot');
		Spot9.scale.setTo(0.6, 0.6);
		Spot9.animations.add('Spot');
		Spot9.animations.play('Spot', 13, true);
		Spot9.inputEnabled = true;
		Spot9.input.useHandCursor = true;
		Spot9.events.onInputUp.add(function(){
			sound.play('Spot_Select');
			moveDown();
			Spot9.loadTexture('Spot2');
			Spot9.animations.add('Spot2', [12,13,0,1,2,3,4,5,6,7,8,9,10,11]);
			Spot9.animations.play('Spot2', 13, false).onComplete.add(function(){
				Spot9.visible = false;
				if (game1.PicksCount == 0){
					hideSpots();
				}
			});
			lockDisplay();
			setTimeout(function() {
				sound.play('fishing_rod');
				fisherman1Anim.play();
				setTimeout(function() {
					dog_idle.visible = false;
					dog_look_right_1.visible = true;
					dog_look_right_1.animations.play('dog_look_right_1', 2, false).onComplete.add(function(){
						dog_look_right_1.visible = false;
						dog_idle.visible = true;
					});
					winding = true;
					fishing_rod_xy();
					fish1.visible = true;
					fish1.position.x  = 660;
					fish1.position.y  = 180;
					fish1.animations.play('fish1', 9, false);
					game.add.tween(fish1).to({y:430,x:760}, 1650, Phaser.Easing.LINEAR, true).onComplete.add(function(){
						winding = false;
						fisherman2Anim.stop();
						fisherman1_1Anim.stop();
						fisherman2.visible = false;
						fisherman1_1.visible = false;
						fisherman3.visible = true;
						fisherman3.animations.play('fisherman3', 9, false);
						fish1.animations.stop();
						fish1.visible = false;
						fish2.visible = true;
						fish2Anim.play();
					});
				}, 650);
			}, 400);
		});

		fisherman1 = game.add.sprite(623, 155, 'fisherman1');
		fisherman1Anim = fisherman1.animations.add('fisherman1',[0,1,2,3,4,5,6,7], 12, false);
		fisherman1Anim.onComplete.add(function(){
			fisherman1.visible = false;
			fisherman1_1.visible = true;
			fisherman1_1Anim.play();
		});
		fisherman1_1 = game.add.sprite(623, 155, 'fisherman1');
		fisherman1_1.visible = false;
		fisherman1_1Anim = fisherman1_1.animations.add('fisherman1_1',[8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 6, false);
		fisherman1_1Anim.onComplete.add(function(){
			fisherman1_1.visible = false;
			fisherman2.visible = true;
			fisherman2Anim.play();
		});

		fisherman2 = game.add.sprite(623, 155, 'fisherman2');
		fisherman2Anim = fisherman2.animations.add('fisherman2',[0,1], 6, true);
		fisherman2.visible = false;

		var fisherman3 = game.add.sprite(623, 155, 'fisherman3');
		fisherman3.animations.add('fisherman3');
		fisherman3.visible = false;

		fish1 = game.add.sprite(500, 300, 'fish1');
		fish1.visible = false;
		fish1.animations.add('fish1');

		var fish2 = game.add.sprite(760, 430, 'fish2');
		fish2.visible = false;
		fish2Anim = fish2.animations.add('fish2',[0,1], 9, false);
		fish2Anim.onComplete.add(function(){
			fish2.visible = false;
			setTimeout(function() {
				fisherman3.visible = false;
				fisherman1.visible = true;
				fisherman1.loadTexture('fisherman1');

				if(ropeValues[game1.PicksCount] < 601) {
					SmallFish();
				} else if(ropeValues[game1.PicksCount] > 600 && ropeValues[game1.PicksCount] < 1201) {
					MediumFish();
				} else if(ropeValues[game1.PicksCount] > 1200) {
					BigFish();
				}

			}, 150);
		});
		var dog_idle = game.add.sprite(601, 563, 'dog_idle');
		dog_idle.animations.add('dog_idle');
		dog_idle.animations.play('dog_idle', 10, true);

		var dog_look_left = game.add.sprite(601, 563, 'dog_look_left');
		dog_look_left.animations.add('dog_look_left', [5,4,3,2]);
		dog_look_left.visible = false;

		var dog_look_left_1 = game.add.sprite(601, 563, 'dog_look_left');
		dog_look_left_1.animations.add('dog_look_left_1', [3,2,1,0]);
		dog_look_left_1.visible = false;

		var dog_look_right = game.add.sprite(601, 563, 'dog_look_right');
		dog_look_right.animations.add('dog_look_right', [0,1,2,3,4,5,6]);
		dog_look_right.visible = false;

		var dog_look_right_1 = game.add.sprite(601, 563, 'dog_look_right');
		dog_look_right_1.animations.add('dog_look_right_1', [2,3,4,5,6,7]);
		dog_look_right_1.visible = false;

		line1 = new Phaser.Line(fish1.position.x, fish1.position.y, point.x, point.y);

		picks_left = game.add.sprite(840, 612, 'picks_left_5');
		picks_left.animations.add('picks_left', [0,1,2,3,4,5,6,7,8,9], 15, true).play();

		Congratulations1 = game.add.sprite(141, 0, 'Congratulations1');
		Congratulations1.visible = false;
		Congratulations1Animation = Congratulations1.animations.add('Congratulations', [0,1,2,3,4], 15, false);
		Congratulations1Animation.onComplete.add(function(){
			Congratulations1.visible = false;
			Congratulations2 = game.add.sprite(141, 0, 'Congratulations2');
			Congratulations2.visible = true;
			finalValueHide();
			finalValueShow();
			Congratulations2.animations.add('Congratulations', [0,1,2,3,4], 15, false).play().onComplete.add(function(){
				Congratulations2.visible = false;
				Congratulations3 = game.add.sprite(141, 0, 'Congratulations3');
				Congratulations3.visible = true;
				finalValueHide();
				finalValueShow();
				Congratulations3.animations.add('Congratulations', [0,1,2,3,4], 15, false).play().onComplete.add(function(){
					finalValueHide();
					Congratulations3.visible = false;
					game1.CongCount = game1.CongCount + 1;
					if (game1.CongCount < 4){
						CongratulationsShow();
					} else {
						game1.PicksCount = 5;
						game1.CongCount = null;
						unlockDisplay();
						if (freeSpinAfterBonus){
							game.state.start('game3');
						} else {
							game.state.start('game2');
						}
					}
				});
			});
		});
		lockDisplay();
		introBonus = game.add.sprite(-2176, 0, 'introBonus');
		game.add.tween(introBonus).to({x:1024}, 2000, Phaser.Easing.Quadratic.In, true).onComplete.add(function(){
			introBonus.visible = false;
			sound.play('Pick5Spots_Intro');
			bonus_Picks_5_1 = game.add.sprite(252, 0, 'bonus_Picks_5_1');
			bonus_Picks_5_1Animation = bonus_Picks_5_1.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play();
			bonus_Picks_5_1Animation.onComplete.add(function(){
				bonus_Picks_5_1.visible = false;
				bonus_Picks_5_2 = game.add.sprite(252, 0, 'bonus_Picks_5_2');
				bonus_Picks_5_2.visible = true;
				bonus_Picks_5_2.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play().onComplete.add(function(){
					bonus_Picks_5_2.visible = false;
					bonus_Picks_5_3 = game.add.sprite(238, 0, 'bonus_Picks_5_3');
					bonus_Picks_5_3.visible = true;
					bonus_Picks_5_3.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play().onComplete.add(function(){
						bonus_Picks_5_3.visible = false;
						bonus_Picks_5_4 = game.add.sprite(247, 0, 'bonus_Picks_5_4');
						bonus_Picks_5_4.visible = true;
						bonus_Picks_5_4.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play().onComplete.add(function(){
							bonus_Picks_5_4.visible = false;
							bonus_Picks_5_5 = game.add.sprite(252, 0, 'bonus_Picks_5_5');
							bonus_Picks_5_5.visible = true;
							bonus_Picks_5_5.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play().onComplete.add(function(){
								bonus_Picks_5_5.visible = false;
								bonus_Picks_5_6 = game.add.sprite(252, 0, 'bonus_Picks_5_6');
								bonus_Picks_5_6.visible = true;
								bonus_Picks_5_6.animations.add('bonus_Picks_5', [0,1,2,3,4,5,6], 10, false).play().onComplete.add(function(){
									bonus_Picks_5_6.visible = false;
									unlockDisplay();
								});
							});
						});
					});
				});
			});
		});
		function CongratulationsShow(){
			Congratulations1.visible = true;
			Congratulations1Animation.play();
			finalValueShow();
		}
		function finalValueShow() {
			finalValue = game.add.text(512, 238, totalValue, {
				font: '125px "FairplexNarrowBold"',
				fill: '#ffffff',
				stroke: '#d8671d',
				strokeThickness: 10,
			});
			finalValue.anchor.setTo(0.5, 0.5);
			finalValue.setShadow(0,5, "#000000", 10, true, false);
		}
		function finalValueHide() {
			finalValue.visible = false;
		}
		function hideSpots(){
			var i = 0;
			(function() {
				if (i < 10) {
					Spot1.alpha -= 0.1;
					Spot2.alpha -= 0.1;
					Spot3.alpha -= 0.1;
					Spot4.alpha -= 0.1;
					Spot5.alpha -= 0.1;
					Spot6.alpha -= 0.1;
					Spot7.alpha -= 0.1;
					Spot8.alpha -= 0.1;
					Spot9.alpha -= 0.1;
					i++;
					setTimeout(arguments.callee, 200);
				}
			})();
		}
		var fishValue ;
		var totalValue = null;
		var totalValueText ;
		totalValueText = game.add.text(516, 721, totalValue, {
			font: '35px "FairplexNarrowBold"',
			fill: '#cf8206',
			stroke: '#cf8206',
			strokeThickness: 2,
		});
		totalValueText.anchor.setTo(0.5, 0.5);

		function BigFish(){
			BigFishSprite1.visible = true;
			BigFishSprite1Animation.play();
			sound.play('BigWin');
		}
		BigFishSprite1 = game.add.sprite(82, 0, 'BigFish_1');
		BigFishSprite1.visible = false;
		BigFishSprite1Animation = BigFishSprite1.animations.add('BigFish', [0,1,2,3], 5, false);
		BigFishSprite1Animation.onComplete.add(function(){
			BigFishSprite1.visible = false;
			BigFishSprite2 = game.add.sprite(82, 0, 'BigFish_2');
			BigFishSprite2.visible = true;
			BigFishSprite2.animations.add('BigFish', [0,1,2], 5, false).play().onComplete.add(function(){
				BigFishSprite2.visible = false;
				BigFishSprite3 = game.add.sprite(82, 0, 'BigFish_3');
				BigFishSprite3.visible = true;
				fishValueShow();
				BigFishSprite3.animations.add('BigFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
					fishValueHide();
					BigFishSprite3.visible = false;
					BigFishSprite4 = game.add.sprite(82, 0, 'BigFish_4');
					BigFishSprite4.visible = true;
					fishValueShow();
					BigFishSprite4.animations.add('BigFish', [0,1,2], 6, false).play().onComplete.add(function(){
						fishValueHide();
						BigFishSprite4.visible = false;
						BigFishSprite3.visible = true;
						fishValueShow();
						BigFishSprite3.animations.add('BigFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
							fishValueHide();
							BigFishSprite3.visible = false;
							BigFishSprite4.visible = true;
							fishValueShow();
							BigFishSprite4.animations.add('BigFish', [0,1,2], 6, false).play().onComplete.add(function(){
								fishValueHide();
								BigFishSprite4.visible = false;
								BigFishSprite5 = game.add.sprite(82, 0, 'BigFish_5');
								BigFishSprite5.visible = true;
								UpdateTotalWin();
								BigFishSprite5.animations.add('BigFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
									BigFishSprite5.visible = false;
									BigFishSprite6 = game.add.sprite(82, 0, 'BigFish_6');
									BigFishSprite6.visible = true;
									BigFishSprite6.animations.add('BigFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
										BigFishSprite6.visible = false;
										BigFishSprite7 = game.add.sprite(82, 0, 'BigFish_7');
										BigFishSprite7.visible = true;
										BigFishSprite7.animations.add('BigFish', [0], 5, false).play().onComplete.add(function(){
											BigFishSprite7.visible = false;
											if (game1.PicksCount > 0){
												unlockDisplay();
												moveUp();
											} else {
												setTimeout(function() {
													CongratulationsShow();
													sound.play('Final_Summary');
												}, 500);
											};
										});
									});
								});
							});
						});
					});
				});
			});
		});
		function UpdateTotalWin() {
			totalValue += ropeValues[game1.PicksCount];
			totalValueText.setText(totalValue);
			var i = 0;
			var showValue = false;
			(function() {
				if (i < 6) {
					if(showValue){
						totalValueText.visible = true;
						showValue = false;
					} else {
						totalValueText.visible = false;
						showValue = true;
					}
					i++;
					setTimeout(arguments.callee, 500);
				}
			})();
		}
		function fishValueShow() {
			fishValue = game.add.text(590, 363, ropeValues[game1.PicksCount], {
				font: '55px "FairplexNarrowBold"',
				fill: '#cf8206',
				stroke: '#cf8206',
				strokeThickness: 3,
			});
			fishValue.anchor.setTo(0.5, 0.5);
		}
		function fishValueHide() {
			fishValue.visible = false;
		}
		function MediumFish(){
			MediumFishSprite1.visible = true;
			MediumFishSprite1Animation.play();
			sound.play('MedWin');
		}
		MediumFishSprite1 = game.add.sprite(82, 0, 'MediumFish_1');
		MediumFishSprite1.visible = false;
		MediumFishSprite1Animation = MediumFishSprite1.animations.add('MediumFish', [0,1,2,3], 5, false);
		MediumFishSprite1Animation.onComplete.add(function(){
			MediumFishSprite1.visible = false;
			MediumFishSprite2 = game.add.sprite(82, 0, 'MediumFish_2');
			MediumFishSprite2.visible = true;
			MediumFishSprite2.animations.add('MediumFish', [0,1,2], 5, false).play().onComplete.add(function(){
				MediumFishSprite2.visible = false;
				MediumFishSprite3 = game.add.sprite(82, 0, 'MediumFish_3');
				MediumFishSprite3.visible = true;
				fishValueShow();
				MediumFishSprite3.animations.add('MediumFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
					fishValueHide();
					MediumFishSprite3.visible = false;
					MediumFishSprite4 = game.add.sprite(82, 0, 'MediumFish_4');
					MediumFishSprite4.visible = true;
					fishValueShow();
					MediumFishSprite4.animations.add('MediumFish', [0,1,2], 6, false).play().onComplete.add(function(){
						fishValueHide();
						MediumFishSprite4.visible = false;
						MediumFishSprite3.visible = true;
						fishValueShow();
						MediumFishSprite3.animations.add('MediumFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
							fishValueHide();
							MediumFishSprite3.visible = false;
							MediumFishSprite4.visible = true;
							fishValueShow();
							MediumFishSprite4.animations.add('MediumFish', [0,1,2], 6, false).play().onComplete.add(function(){
								fishValueHide();
								MediumFishSprite4.visible = false;
								MediumFishSprite5 = game.add.sprite(82, 0, 'MediumFish_5');
								MediumFishSprite5.visible = true;
								UpdateTotalWin();
								MediumFishSprite5.animations.add('MediumFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
									MediumFishSprite5.visible = false;
									MediumFishSprite6 = game.add.sprite(82, 0, 'MediumFish_6');
									MediumFishSprite6.visible = true;
									MediumFishSprite6.animations.add('MediumFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
										MediumFishSprite6.visible = false;
										MediumFishSprite7 = game.add.sprite(82, 0, 'MediumFish_7');
										MediumFishSprite7.visible = true;
										MediumFishSprite7.animations.add('MediumFish', [0], 5, false).play().onComplete.add(function(){
											MediumFishSprite7.visible = false;
											if (game1.PicksCount > 0){
												unlockDisplay();
												moveUp();
											} else {
												setTimeout(function() {
													CongratulationsShow();
													sound.play('Final_Summary');
												}, 500);
											};
										});
									});
								});
							});
						});
					});
				});
			});
		});

		function SmallFish(){
			SmallFishSprite1.visible = true;
			SmallFishSprite1Animation.play();
			sound.play('LowWin');
		}
		SmallFishSprite1 = game.add.sprite(82, 0, 'SmallFish_1');
		SmallFishSprite1.visible = false;
		SmallFishSprite1Animation = SmallFishSprite1.animations.add('SmallFish', [0,1,2,3], 5, false);
		SmallFishSprite1Animation.onComplete.add(function(){
			SmallFishSprite1.visible = false;
			SmallFishSprite2 = game.add.sprite(82, 0, 'SmallFish_2');
			SmallFishSprite2.visible = true;
			SmallFishSprite2.animations.add('SmallFish', [0,1,2], 5, false).play().onComplete.add(function(){
				SmallFishSprite2.visible = false;
				SmallFishSprite3 = game.add.sprite(82, 0, 'SmallFish_3');
				SmallFishSprite3.visible = true;
				fishValueShow();
				SmallFishSprite3.animations.add('SmallFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
					fishValueHide();
					SmallFishSprite3.visible = false;
					SmallFishSprite4 = game.add.sprite(82, 0, 'SmallFish_4');
					SmallFishSprite4.visible = true;
					fishValueShow();
					SmallFishSprite4.animations.add('SmallFish', [0,1,2], 6, false).play().onComplete.add(function(){
						fishValueHide();
						SmallFishSprite4.visible = false;
						SmallFishSprite3.visible = true;
						fishValueShow();
						SmallFishSprite3.animations.add('SmallFish', [0,1,2,3], 6, false).play().onComplete.add(function(){
							fishValueHide();
							SmallFishSprite3.visible = false;
							SmallFishSprite4.visible = true;
							fishValueShow();
							SmallFishSprite4.animations.add('SmallFish', [0,1,2], 6, false).play().onComplete.add(function(){
								fishValueHide();
								SmallFishSprite4.visible = false;
								SmallFishSprite5 = game.add.sprite(82, 0, 'SmallFish_5');
								SmallFishSprite5.visible = true;
								UpdateTotalWin();
								SmallFishSprite5.animations.add('SmallFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
									SmallFishSprite5.visible = false;
									SmallFishSprite6 = game.add.sprite(82, 0, 'SmallFish_6');
									SmallFishSprite6.visible = true;
									SmallFishSprite6.animations.add('SmallFish', [0,1,2,3], 5, false).play().onComplete.add(function(){
										SmallFishSprite6.visible = false;
										SmallFishSprite7 = game.add.sprite(82, 0, 'SmallFish_7');
										SmallFishSprite7.visible = true;
										SmallFishSprite7.animations.add('SmallFish', [0], 5, false).play().onComplete.add(function(){
											SmallFishSprite7.visible = false;
											if (game1.PicksCount > 0){
												unlockDisplay();
												moveUp();
											} else {
												setTimeout(function() {
													CongratulationsShow();
													sound.play('Final_Summary');
												}, 500);
											};
										});
									});
								});
							});
						});
					});
				});
			});
		});
		function moveDown(){
			game1.PicksCount = game1.PicksCount - 1;
			game.add.tween(picks_left).to({y:762}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				if (game1.PicksCount > 0) {
					picks_left.loadTexture('picks_left_' + game1.PicksCount);
					picks_left.animations.add('picks_left', [0,1,2,3,4,5,6,7,8,9], 15, true).play();
				}
			});
		}
		function moveUp(){
			game.add.tween(picks_left).to({y:612}, 500, Phaser.Easing.LINEAR, true).onComplete.add(function(){

			});
		}
		function fishing_rod_xy(){
			var i = 0;
			(function() {
				if (i < 11) {
					point.x = game1.pointX[i];
					point.y = game1.pointY[i];
					i++;
					setTimeout(arguments.callee, 166.67);
				}
			})();
		}

	},
	update:function(){
		if (winding == true){
			line1.setTo(fish1.x+155, fish1.y+70, point.x, point.y)
		} else {
			line1.setTo(5000, 5000, 5000, 5000)
		}
		// if (game.scale.isFullScreen){
		// 	fullStatus = true;
		// }else{
		// 	fullStatus = false;
		// }
	},
	render:function(){
		game.debug.geom(line1, '#67683D');
	}
};
var line;
var point;
var winding = false;
game.state.add('game1', game1); 