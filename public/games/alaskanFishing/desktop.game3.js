game3 = {
	cell : [],
	bars : [],
	spinStatus : false,
	spinStatus1 : false,
	spinStatus2 : false,
	spinStatus3 : false,
	spinStatus4 : false,
	spinStatus5 : false,
	fb : null,
	create : function() {

		checkGame = 3;

		addAnimCells();

		sound = game.add.audio('sound');
		sound.allowMultiple = true;
		sound.addMarker('FreeSpin_Start', 82, 4.4);
		sound.addMarker('FreeSpin_Background', 3, 29.6);
		game3.fb = game.add.audio('sound');
		game3.fb.addMarker('FreeSpin_Background', 3, 29.6);
		sound.addMarker('FreeSpin_Complete', 34, 5);

		game3.Background = game.add.sprite(0,0, 'Background2');
		game3.ReelContainer = game.add.sprite(0,132, 'ReelContainer2');

		game3.bars[0] = game.add.tileSprite(51, 136, 174, 522, 'bar');
		game3.bars[0].tilePosition.y =  randomNumber(0,11)*204 ;
		game3.bars[1] = game.add.tileSprite(238, 136, 174, 522, 'bar');
		game3.bars[1].tilePosition.y =  randomNumber(0,11)*204;
		game3.bars[2] = game.add.tileSprite(425 , 136, 174, 522, 'bar');
		game3.bars[2].tilePosition.y =  randomNumber(0,11)*204;
		game3.bars[3] = game.add.tileSprite(612, 136, 174, 522, 'bar');
		game3.bars[3].tilePosition.y =  randomNumber(0,11)*204;
		game3.bars[4] = game.add.tileSprite(799, 136, 174, 522, 'bar');
		game3.bars[4].tilePosition.y =  randomNumber(0,11)*204;
		game3.bars[0].visible = false;
		game3.bars[1].visible = false;
		game3.bars[2].visible = false;
		game3.bars[3].visible = false;
		game3.bars[4].visible = false;

		game3.cell[1] = game.add.sprite(51,136, 'cell'+info[0]);
		game3.cell[2] = game.add.sprite(51,310, 'cell'+info[1]);
		game3.cell[3] = game.add.sprite(51,484, 'cell'+info[2]);
		game3.cell[4] = game.add.sprite(238,136, 'cell'+info[3]);
		game3.cell[5] = game.add.sprite(238,310, 'cell'+info[4]);
		game3.cell[6] = game.add.sprite(238,484, 'cell'+info[5]);
		game3.cell[7] = game.add.sprite(425,136, 'cell'+info[6]);
		game3.cell[8] = game.add.sprite(425,310, 'cell'+info[7]);
		game3.cell[9] = game.add.sprite(425,484, 'cell'+info[8]);
		game3.cell[10] = game.add.sprite(612,136, 'cell'+info[9]);
		game3.cell[11] = game.add.sprite(612,310, 'cell'+info[10]);
		game3.cell[12] = game.add.sprite(612,484, 'cell'+info[11]);
		game3.cell[13] = game.add.sprite(799,136, 'cell'+info[12]);
		game3.cell[14] = game.add.sprite(799,310, 'cell'+info[13]);
		game3.cell[15] = game.add.sprite(799,484, 'cell'+info[14]);

		game3.Background_bottom = game.add.sprite(0,658, 'BG2_bottom');
		game3.Logo = game.add.sprite(0,0, 'Logo2');

		//buttons

		game3.credits = game.add.sprite(12,668, 'credits');
		game3.freeSpins = game.add.sprite(284,670, 'freeSpins');
		game3.txtMultiplier = game.add.sprite(284,670, 'txtMultiplier');
		game3.txtMultiplier.visible = false;

		game3.win2 = game.add.sprite(753,666, 'win2');

		var freeSpinsValue = game.add.text(616+90, 708, '15', {
			font: '66px "FairplexNarrowBold"',
			fill: '#ffffff',
			stroke: '#d8671d',
			strokeThickness: 6,
		});
		freeSpinsValue.setShadow(0,5, "#000000", 10, true, false);
		freeSpinsValue.anchor.setTo(0.5, 0.5);
		balanceScore = game.add.text(160, 719, balance, {
			font: '28px "Arial"',
			fill: '#ffffff',
			stroke: '#ffffff',
			strokeThickness: 0,
		});
		balanceScore.anchor.setTo(0.5, 0.5);
		allWinScore = game.add.text(635+160+90-5, 719, allWin, {
			font: '28px "Arial"',
			fill: '#ffffff',
			stroke: '#ffffff',
			strokeThickness: 0,
		});
		allWinScore.anchor.setTo(0.5, 0.5);

		game3.FreeSpinsWinSum = game.add.sprite(0,0, 'FreeSpinsWinSumCoin');
		game3.FreeSpinsWinSum.visible = false;
		var freeSpinsSumm = game.add.text(512, 370, '100', {
			font: '120px "FairplexNarrowBold"',
			fill: '#ffffff',
			stroke: '#d8671d',
			strokeThickness: 6,
		});
		freeSpinsSumm.setShadow(0,5, "#000000", 10, true, false);
		freeSpinsSumm.anchor.setTo(0.5, 0.5);
		freeSpinsSumm.visible = false;

		if (freeSpinAfterBonus){
			var countFreeSpinValue = freeSpinAfterBonusBalance;
			allWinOld = freeSpinAfterBonusAllWinOld;
			allWinScore.setText(allWinOld);
			freeSpinsValue.setText(countFreeSpinValue);
			freeSpinAfterBonus = false;
			freeSpinAfterBonusBalance = 0;
			freeSpinAfterBonusAllWinOld = 0;
			// sound.play('FreeSpin_Background', 3, 29.6, true, true);
			game3.fb.play('FreeSpin_Background', 3, 29.6, false, true);
			setTimeout(function() {
				startFreeSpin();
			}, 500);
		} else {
			var countFreeSpinValue = 15;
			allWinOld = allWin;
			game3.FSCongrats = game.add.sprite(0,0, 'FSCongrats');
			sound.play('FreeSpin_Start');
			setTimeout(function() {
				game3.FSCongrats.visible = false;
				sound.stop('FreeSpin_Start');
				console.log(123);
				// sound.play('FreeSpin_Background', 3, 29.6, true, true);
				game3.fb.play('FreeSpin_Background', 3, 29.6, false, true);
				console.log('включение звука1')
				setTimeout(function() {
					startFreeSpin();
				}, 500);
			}, 4400);
		}



		function startFreeSpin(){

			hideAnimCells(showedAnimArray);

			requestSpin(gamename, sessionName, betline, lines);

			function requestSpin(gamename, sessionName, betline, lines) {
				$.ajax({
					type: "get",
					url: getNeedUrlPath() + 'spin/'+gamename+'?sessionName='+sessionName+'&betLine='+betline+'&linesInGame='+lines,
					dataType: 'html',
					success: function (data) {
						checkGame = 3;

						dataSpinRequest = JSON.parse(data);

						if(dataSpinRequest['state']) {
							if(dataSpinRequest['rope']) {
								checkRopeGame = true;
								parseSpinAnswer(dataSpinRequest);
							} else {
								parseSpinAnswer(dataSpinRequest);
							}
						}

						countFreeSpinValue = countFreeSpinValue -1;

						freeSpinsValue.setText(countFreeSpinValue);
						game3.spinStatus = true;
						startspin(0);
						setTimeout(function() {
							startspin(1);
						}, 100+100);
						setTimeout(function() {
							startspin(2);
						}, 200+200);
						setTimeout(function() {
							startspin(3);
						}, 300+300);
						setTimeout(function() {
							startspin(4);
						}, 400+400);

					},
					error: function (xhr, ajaxOptions, thrownError) {
						var errorText = '//ошибка 30';
						console.log(errorText);
						setTimeout("requestSpin(gamename, sessionName, betline, lines);",2000);
					}
				});
			}
		}

		function startspin(number){
			game.add.tween(game3.cell[1+number*3]).to({y:game3.cell[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game3.cell[1+number*3].visible = false;
			});
			game.add.tween(game3.cell[2+number*3]).to({y:game3.cell[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game3.cell[2+number*3].visible = false;
			});
			game.add.tween(game3.cell[3+number*3]).to({y:game3.cell[3+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
				game3.cell[3+number*3].visible = false;
				game3.bars[number].visible = true;
				if (number == 0){
					game3.spinStatus1 = true;
					setTimeout(function() {
						game3.spinStatus1 = false;
						game3.bars[0].visible = false;
						game3.cell[1+3*0].visible = true;
						game3.cell[2+3*0].visible = true;
						game3.cell[3+3*0].visible = true;

						game3.cell[1].loadTexture('cell'+info[0]);
						game3.cell[2].loadTexture('cell'+info[1]);
						game3.cell[3].loadTexture('cell'+info[2]);

						// демонстрация особых ячеек
						if(winCellInfo[0] === 'bonusCell13') {
							playBonusCellAnim(0, 13);

						}
						if(winCellInfo[1] === 'bonusCell13') {
							playBonusCellAnim(1, 13);

						}
						if(winCellInfo[2] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(2, 13);",500);

						}

						if(winCellInfo[0] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(0, 12);",500);
						}
						if(winCellInfo[1] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(1, 12);",500);

						}
						if(winCellInfo[2] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(2, 12);",500);

						}
					}, 2000);
				}
				if (number == 1){
					game3.spinStatus2 = true;
					setTimeout(function() {
						game3.spinStatus2 = false;
						game3.bars[1].visible = false;
						game3.cell[1+3*1].visible = true;
						game3.cell[2+3*1].visible = true;
						game3.cell[3+3*1].visible = true;

						game3.cell[4].loadTexture('cell'+info[3]);
						game3.cell[5].loadTexture('cell'+info[4]);
						game3.cell[6].loadTexture('cell'+info[5]);

						// демонстрация особых ячеек
						if(winCellInfo[3] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(3, 13);",500);

						}
						if(winCellInfo[4] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(4, 13);",500);

						}
						if(winCellInfo[5] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(5, 13);",500);

						}
					}, 2000);
				}
				if (number == 2){
					game3.spinStatus3 = true;
					setTimeout(function() {
						game3.spinStatus3 = false;
						game3.bars[2].visible = false;
						game3.cell[1+3*2].visible = true;
						game3.cell[2+3*2].visible = true;
						game3.cell[3+3*2].visible = true;

						game3.cell[7].loadTexture('cell'+info[6]);
						game3.cell[8].loadTexture('cell'+info[7]);
						game3.cell[9].loadTexture('cell'+info[8]);

						// демонстрация особых ячеек
						if(winCellInfo[6] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(6, 13);",500);

						}
						if(winCellInfo[7] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(7, 13);",500);

						}
						if(winCellInfo[8] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(8, 13);",500);

						}
					}, 2000);
				}
				if (number == 3){
					game3.spinStatus4 = true;
					setTimeout(function() {
						game3.spinStatus4 = false;
						game3.bars[3].visible = false;
						game3.cell[1+3*3].visible = true;
						game3.cell[2+3*3].visible = true;
						game3.cell[3+3*3].visible = true;

						game3.cell[10].loadTexture('cell'+info[9]);
						game3.cell[11].loadTexture('cell'+info[10]);
						game3.cell[12].loadTexture('cell'+info[11]);

						// демонстрация особых ячеек
						if(winCellInfo[9] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(9, 13);",500);

						}
						if(winCellInfo[10] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(10, 13);",500);

						}
						if(winCellInfo[11] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(11, 13);",500);

						}
					}, 2000);
				}
				if (number == 4){
					game3.spinStatus5 = true;
					setTimeout(function() {
						game3.spinStatus5 = false;
						game3.bars[4].visible = false;
						game3.cell[1+3*4].visible = true;
						game3.cell[2+3*4].visible = true;
						game3.cell[3+3*4].visible = true;

						game3.cell[13].loadTexture('cell'+info[12]);
						game3.cell[14].loadTexture('cell'+info[13]);
						game3.cell[15].loadTexture('cell'+info[14]);

						// демонстрация особых ячеек
						if(winCellInfo[12] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(12, 13);",500);

						}
						if(winCellInfo[13] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(13, 13);",500);

						}
						if(winCellInfo[14] === 'bonusCell13') {
							setTimeout("playBonusCellAnim(14, 13);",500);

						}

						if(winCellInfo[12] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(12, 1);",500);

						}
						if(winCellInfo[13] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(13, 1);",500);

						}
						if(winCellInfo[14] === 'bonusCell12') {
							setTimeout("playBonusCellAnim(14, 1);",500);
						}
					}, 2000);
				}


				setTimeout(function() {
					endspin(number);
				}, 2000);
			});
};
function endspin(number){
	game3.cell[1+number*3].position.y = 136+30;
	game3.cell[2+number*3].position.y = 310+30;
	game3.cell[3+number*3].position.y = 484+30;
	game.add.tween(game3.cell[1+number*3]).to({y:game3.cell[1+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
	});
	game.add.tween(game3.cell[2+number*3]).to({y:game3.cell[2+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
	});
	game.add.tween(game3.cell[3+number*3]).to({y:game3.cell[3+number*3].position.y-30}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
		if (number == 4){
			game3.spinStatus = false;
			if(ropeValues == false || ropeValues == '15freeSpin'){
				if(allWin > 0) {
					if(ropeValues !== false) {
						showAnimCells(winCellInfo);
					} else {
						showAnimCells(winCellInfo);
					}
					game3.freeSpins.visible = false;
					freeSpinsValue.visible = false;
					game3.txtMultiplier.visible = true;
				}
				updateBalance();
			} else {
				if (countFreeSpinValue > 0){
					freeSpinAfterBonus = true;
					freeSpinAfterBonusBalance = countFreeSpinValue;
					freeSpinAfterBonusAllWinOld = allWinOld
				}
				showAnimCells(winCellInfo);
				sound.play('bonusWin');
			}
		}
	});
}
function updateBalance(){
	updateStatus = true;
	var x = 0;
	var interval;
	(function() {
		if (x < allWin) {
			interval = 5000/(betlineR*linesR);
			if (allWin > 1000/interval*5*5){
				x += Math.round(allWin*5*2/(betlineR*linesR*5));
			} else {
				x += 10;
			}
			allWinScore.setText(x + allWinOld);
			setTimeout(arguments.callee, interval);
		} else {
			allWinOld += allWin;
			allWinScore.setText(allWinOld);
			balanceScore.setText(balance);
			updateStatus = false;
			if(ropeValues == '15freeSpin'){
				countFreeSpinValue = 15;
			}
			if (countFreeSpinValue == 0){
				setTimeout(function() {
					sound.stop('FreeSpin_Background');
					game3.FreeSpinsWinSum.visible = true;
					freeSpinsSumm.visible = true;
					freeSpinsSumm.setText(allWinOld);
					sound.play('FreeSpin_Complete');
					hideAnimCells(showedAnimArray);
					setTimeout(function() {
						game3.fb.stop();
						game.state.start('game2');
					}, 5000);
				}, 2000);
			} else {
				setTimeout(function() {
					startFreeSpin(countFreeSpinValue);
					game3.freeSpins.visible = true;
					freeSpinsValue.setText(countFreeSpinValue);
					freeSpinsValue.visible = true;
					game3.txtMultiplier.visible = false;
				}, 1000);
			}
		}
	})();
}
},
update:function(){

	if (game3.spinStatus1){
		game3.bars[0].tilePosition.y += 50;
	}
	if (game3.spinStatus2){
		game3.bars[1].tilePosition.y += 50;
	}
	if (game3.spinStatus3){
		game3.bars[2].tilePosition.y += 50;
	}
	if (game3.spinStatus4){
		game3.bars[3].tilePosition.y += 50;
	}
	if (game3.spinStatus5){
		game3.bars[4].tilePosition.y += 50;
	};
}
};
game.state.add('game3', game3);