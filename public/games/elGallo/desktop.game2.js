function game2() {

	var game2 = {
		cell : [],
		spinStatus : true,
		betPanel : false,
		pickBet : 2,
		bet : [],
		ticker : null,
		betAnim : false,
		spinjackpot : false
	};

	game2.preload = function () {};

	game2.create = function () {
		if (game.sound.usingWebAudio &&
			game.sound.context.state === 'suspended')
		{
			game.input.onTap.addOnce(game.sound.context.resume, game.sound.context);
		}
		if (this.game.device.android && this.game.device.chrome && this.game.device.chromeVersion >= 55) {
			this.game.sound.setTouchLock();
			this.game.sound.touchLocked = true;
			this.game.input.touch.addTouchLockCallback(function () {
				if (this.noAudio || !this.touchLocked || this._unlockSource !== null) {
					return true;
				}
				if (this.usingWebAudio) {

					var buffer = this.context.createBuffer(1, 1, 22050);
					this._unlockSource = this.context.createBufferSource();
					this._unlockSource.buffer = buffer;
					this._unlockSource.connect(this.context.destination);

					if (this._unlockSource.start === undefined) {
						this._unlockSource.noteOn(0);
					}
					else {
						this._unlockSource.start(0);
					}

					if (this._unlockSource.context.state === 'suspended') {
						this._unlockSource.context.resume();
					}
				}

				return true;

			}, this.game.sound, true);
		}
		curGame = 2;
		hideMobileBtn();
		game2.spinjackpot = true;
		game2.spinStatus = true;
		// game2.jackpotValue = 0; // major
		// game2.jackpotValue = 1; // big
		// game2.jackpotValue = 2; // minor
		// game2.jackpotValue = 3; // mini

		var game2_bg = game.add.audio('drumroll');
		var game2_win = game.add.audio('game2_win');
		game2_bg.loop = true;
		game2_bg.play();
		game.add.sprite(0,0, 'background_2');
		bg_bottom = game.add.sprite(0,685, 'bg_2_bottom');
		bg_bottom.visible = false;
		game2.bar_jackpot = game.add.tileSprite(572, 153, 166, 498, 'bar_jackpot');
		game2.bar_jackpot.tilePosition.y =  0 ;
		lattice1 = game.add.sprite(570 , 151, 'lattice');
		lattice2 = game.add.sprite(570 , 483, 'lattice');

		stop_reel = game.add.sprite(798 , 684, 'stop_reel');
		stop_reel.inputEnabled = true;
		stop_reel.input.useHandCursor = true;
		stop_reel.events.onInputOver.add(function(){
			stop_reel.loadTexture('stop_reel_h');
		});
		stop_reel.events.onInputOut.add(function(){
			stop_reel.loadTexture('stop_reel');
		});
		stop_reel.events.onInputUp.add(function(click, pointer) {
      // if (pointer.button !== 0 && pointer.button !== undefined)
      //   return;
			if (!window.navigator.onLine) return;

			game2_bg.stop();
			game2_win.play();
			stop_reel.visible = false;
			creditOldDOl.visible = true;
			function send() {
				$.ajax({
					type: "get",
					url: getNeedUrlPath() +'/get-user-jackpot?sessionName='+sessionName+'&platformId='+ platformId,
					dataType: 'html',
					success: function (data) {
						console.log(data);
						switch (jackpot) {
							case 'MAJOR':
								console.log('MAJOR');
								game2.jackpotValue = 0;
								creditOld.setText(+(jackpotArr["MAJOR"]*100).toFixed());
								creditOldDOl.setText('$'+ (jackpotArr["MAJOR"]).toFixed(2));
								balance = +(balance + jackpotArr["MAJOR"]*100).toFixed();
								break;
							case 'BIG_DADDY':
								console.log('BIG_DADDY');
								game2.jackpotValue = 1;
								creditOld.setText(+(jackpotArr["BIG_DADDY"]*100).toFixed());
								creditOldDOl.setText('$'+ (jackpotArr["BIG_DADDY"]).toFixed(2));
								balance = +(balance + jackpotArr["BIG_DADDY"]*100).toFixed();
								break;
							case 'MINOR':
								console.log('MINOR');
								game2.jackpotValue = 2;
								creditOld.setText(+(jackpotArr["MINOR"]*100).toFixed());
								creditOldDOl.setText('$'+ (jackpotArr["MINOR"]).toFixed(2));
								balance = +(balance + jackpotArr["MINOR"]*100).toFixed();
								break;
							case 'MINI':
								console.log('MINI');
								game2.jackpotValue = 3;
								creditOld.setText(+(jackpotArr["MINI"]*100).toFixed());
								creditOldDOl.setText('$'+ (jackpotArr["MINI"]).toFixed(2));
								balance = +(balance + jackpotArr["MINI"]*100).toFixed();
								break;
						}
						game2.spinStatus = false;
						bg_bottom.visible = true;
						flashWinPosition(game2.jackpotValue);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (!window.navigator.onLine) {
							send()
						} else {
							var errorText = 'ошибка 61';
							console.log(errorText);
							error_bg.visible = true;
							errorStatus = true;
						}
					}
				});
			}

			send();
		});

		// addScore(game, scorePosions, balance);

		miniScore = game.add.text(370, 215, '$'+ (jackpotArr["MINI"]).toFixed(2), {
			font: '46px "Arial Black"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		miniScore.anchor.setTo(0.5, 0.5);
		minorScore = game.add.text(370, 348, '$'+ (jackpotArr["MINOR"]).toFixed(2), {
			font: '46px "Arial Black"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		minorScore.anchor.setTo(0.5, 0.5);
		majorScore = game.add.text(370, 479, '$'+ (jackpotArr["MAJOR"]).toFixed(2), {
			font: '46px "Arial Black"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		majorScore.anchor.setTo(0.5, 0.5);
		bigDaddyScore = game.add.text(370, 606, '$'+ (jackpotArr["BIG_DADDY"]).toFixed(2), {
			font: '46px "Arial Black"',
			fill: '#000000',
			stroke: '#000000',
			strokeThickness: 0,
		});
		bigDaddyScore.anchor.setTo(0.5, 0.5);
		winScoreGame2 = game.add.text(876, 251, balance, {
			font: '39px "Arial"',
			fill: '#f2f46e',
			stroke: '#000000',
			strokeThickness: 0,
		});
		winScoreGame2.anchor.setTo(0.5, 0.5);
		winDolGame2 = game.add.text(876, 296, '$'+ (balance/100).toFixed(2), {
			font: '29px "Arial"',
			fill: '#ffffff',
			stroke: '#000000',
			strokeThickness: 0,
		});
		winDolGame2.anchor.setTo(0.5, 0.5);
		creditOld = game.add.text(876, 582, '??????', {
			font: '39px "Arial"',
			fill: '#f2f46e',
			stroke: '#000000',
			strokeThickness: 0,
		});
		creditOld.anchor.setTo(0.5, 0.5);
		creditOldDOl = game.add.text(876, 627, '', {
			font: '29px "Arial"',
			fill: '#ffffff',
			stroke: '#000000',
			strokeThickness: 0,
		});
		creditOldDOl.anchor.setTo(0.5, 0.5);
		creditOldDOl.visible = false;
		game2.ticker = game.add.tileSprite(0, 785, 1154, 31, 'ticker');
		error_bg = game.add.sprite(0, 0, 'error_bg');
		error_bg.visible = false;
		function flashWinPosition(value){
			var curText;
			switch (value) {
				case 0:
				curText = majorScore;
				break;
				case 1:
				curText = bigDaddyScore;
				break;
				case 2:
				curText = minorScore;
				break;
				case 3:
				curText = miniScore;
				break;
			}
			setTimeout(function() {
				curText.visible = false;
				setTimeout(function() {
					curText.visible = true;
					setTimeout(function() {
						curText.visible = false;
						setTimeout(function() {
							curText.visible = true;
							setTimeout(function() {
								curText.visible = false;
								setTimeout(function() {
									curText.visible = true;
									setTimeout(function() {
										curText.visible = false;
										setTimeout(function() {
											curText.visible = true;
											jackpotStatus = false;
											game.state.start('game1');
											waterfallCoin = true;
										}, 500);
									}, 500);
								}, 500);
							}, 500);
						}, 500);
					}, 500);
				}, 500);
			}, 500);
		}
	};



	game2.update = function () {
		if (game2.spinjackpot){
			game2.bar_jackpot.tilePosition.y += 20;
			if (game2.spinStatus == false) {
				if(game2.bar_jackpot.tilePosition.y >= 30+166*game2.jackpotValue & game2.bar_jackpot.tilePosition.y <= 50+166*game2.jackpotValue){
					game2.spinjackpot = false;
					console.log(game2.jackpotValue);
					game.add.tween(game2.bar_jackpot.tilePosition).to({y: 0+166*game2.jackpotValue}, 200, Phaser.Easing.LINEAR, true).onComplete.add(function(){
					});
				}
			}
		}
		game2.ticker.tilePosition.x += 0.5;
	};

	game.state.add('game2', game2);

};
