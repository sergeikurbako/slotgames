function game2() {
    var game2 = {};

    game2.preload = function () {};

    game2.create = function () {

        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        background1 = game.add.sprite(94,22, 'game.background1');
        // topBarImage2 = game.add.sprite(94,22, 'topScoreGame3');
        backgroundGame3 = game.add.sprite(94,22, 'game.backgroundGame2');
        ice = game.add.sprite(95, 406, 'ice');
        iceAnimation = ice.animations.add('ice', [0,1,2,3,4,5,6,7,8,9], 8, true).play();
        ice.visible = true; 
        background = game.add.sprite(0,0, 'game.background');

        // addTableTitle(game, 'winTitleGame2', 526,436);
        hideTableTitle();
        
        // кнопки
        addButtonsGame2(game);


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[255, 26, 20], [455, 26, 20], [640, 26, 20], [494, 86, 24]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[138,134], [250,134], [362,134], [474,134], [586,134]];
        addCards(game, cardPosition);

        openDCard(dcard);
        for(var i=1; i<=5; ++i) {
            game.add.sprite(26+112*i, 134, 'border_card');
        }
        //анимация
        game.add.sprite(291, 381, 'game2.head');
        game.add.sprite(238, 406, 'game2.hand');


        showDoubleToAndTakeOrRiskTexts(game, totalWin, 548, 449);

        full_and_sound();
    };

    game2.update = function () {
        if (game.scale.isFullScreen)
        {
          full.loadTexture('game.full');
          fullStatus = true;
      }
      else
      {
       full.loadTexture('game.non_full');
       fullStatus = false;
   }
};

game.state.add('game2', game2);

    // звуки
}