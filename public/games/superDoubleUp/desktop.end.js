(function() {
  // window.PhaserGlobal = {
  //     disableWebAudio: false,
  //     disableAudio: false,
  // };
  var preload = {};

  preload.preload = function() {

    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;

    game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.scale.pageAlignVertically = true;
    game.scale.scaleMode = 2;
    game.scale.pageAlignHorizontally = true;
    game.stage.disableVisibilityChange = true;

    var needUrlPath = '';
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
      needUrlPath = location.href.substring(0, location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
    } else if (location.href.indexOf('/game/') !== -1) {
      var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
      needUrlPath = location.href.substring(0, location.href.indexOf('/game/')) + '/games/' + gamename;
    } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1) {
      var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
      needUrlPath = location.href.substring(0, location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
    }

    if (location.href.indexOf('slotgames') !== -1) {
      var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
      needUrlPath = 'http://slotgames/games/superDoubleUp';
    }

    var part2Url = '';
    if (location.href.indexOf('ezsl.tk') !== -1) {
      // var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
      // gamename = gamename.substring(0, gamename.indexOf('/?'));
      // var part2Url = location.href.substring(location.href.indexOf('?'));
      // needUrlPath = 'http://ezsl.tk/games/' + gamename;
    }
    if (location.href.indexOf('playgames.devbet.live') !== -1) {
      var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
      gamename = gamename.substring(0, gamename.indexOf('/?'));
       part2Url = '';
      needUrlPath = 'https://playgames.devbet.live/games/' + gamename;
    }
    if (location.href.indexOf('game.play777games.com') !== -1) {
      var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
      gamename = gamename.substring(0, gamename.indexOf('/?'));
      var part2Url = location.href.substring(location.href.indexOf('?'));
      needUrlPath = 'https://game.play777games.com/games/' + gamename;
    }
    if (game.sound.usingWebAudio &&
      game.sound.context.state === 'suspended') {
      game.input.onTap.addOnce(game.sound.context.resume, game.sound.context);
    }
    if (this.game.device.android && this.game.device.chrome && this.game.device.chromeVersion >= 55) {
      this.game.sound.setTouchLock();
      this.game.sound.touchLocked = true;
      this.game.input.touch.addTouchLockCallback(function() {
        if (this.noAudio || !this.touchLocked || this._unlockSource !== null) {
          return true;
        }
        if (this.usingWebAudio) {

          var buffer = this.context.createBuffer(1, 1, 22050);
          this._unlockSource = this.context.createBufferSource();
          this._unlockSource.buffer = buffer;
          this._unlockSource.connect(this.context.destination);

          if (this._unlockSource.start === undefined) {
            this._unlockSource.noteOn(0);
          } else {
            this._unlockSource.start(0);
          }

          if (this._unlockSource.context.state === 'suspended') {
            this._unlockSource.context.resume();
          }
        }

        return true;

      }, this.game.sound, true);
    }
    game.load.image('game.background', needUrlPath + '/img/bg.png' + part2Url);
    game.load.image('black_bg', needUrlPath + '/img/black_bg.png' + part2Url);
    game.load.image('error_bg', needUrlPath + '/img/error_bg.png' + part2Url);
    game.load.image('session_bg', needUrlPath + '/img/session_bg.png' + part2Url);
    game.load.image('establishing_bg', needUrlPath + '/img/establishing_bg.png' + part2Url);
    game.load.image('btn_yes', needUrlPath + '/img/btn_yes.png' + part2Url);
    game.load.image('btn_no', needUrlPath + '/img/btn_no.png' + part2Url);

    game.load.image('bet_bottom', needUrlPath + '/img/btns/Bet_bottom.png' + part2Url);
    game.load.image('bet_top', needUrlPath + '/img/btns/Bet_top.png' + part2Url);
    game.load.image('exit', needUrlPath + '/img/btns/Exit.png' + part2Url);
    game.load.image('help', needUrlPath + '/img/btns/Help.png' + part2Url);
    game.load.image('playGame', needUrlPath + '/img/btns/Play Game.png' + part2Url);
    game.load.image('quickPick', needUrlPath + '/img/btns/Quik Pick.png' + part2Url);
    game.load.image('wipeCard', needUrlPath + '/img/btns/Wipe Card.png' + part2Url);
    game.load.image('bet_bottom_p', needUrlPath + '/img/btns/Bet_bottom_p.png' + part2Url);
    game.load.image('bet_top_p', needUrlPath + '/img/btns/Bet_top_p.png' + part2Url);
    game.load.image('exit_p', needUrlPath + '/img/btns/Exit_p.png' + part2Url);
    game.load.image('help_p', needUrlPath + '/img/btns/Help_p.png' + part2Url);
    game.load.image('playGame_p', needUrlPath + '/img/btns/Play Game_p.png' + part2Url);
    game.load.image('quickPick_p', needUrlPath + '/img/btns/Quik Pick_p.png' + part2Url);
    game.load.image('wipeCard_p', needUrlPath + '/img/btns/Wipe Card_p.png' + part2Url);
    game.load.image('raise_bet', needUrlPath + '/img/btns/RAISE_BET.png' + part2Url);
    game.load.image('raise_bet_p', needUrlPath + '/img/btns/RAISE_BET_p.png' + part2Url);
    game.load.image('keep_bet', needUrlPath + '/img/btns/KEEP_BET.png' + part2Url);
    game.load.image('keep_bet_p', needUrlPath + '/img/btns/KEEP_BET_p.png' + part2Url);
    game.load.image('AddCredit', needUrlPath + '/img/btns/Add_credit.png' + part2Url);
    game.load.image('AddCredit_p', needUrlPath + '/img/btns/Add_credit_p.png' + part2Url);

    game.load.image('block1', needUrlPath + '/img/block1.png' + part2Url);
    game.load.image('block2', needUrlPath + '/img/block2.png' + part2Url);
    game.load.image('sorry_add_credits', needUrlPath + '/img/sorry_add_credits.png' + part2Url);

    game.load.image('help1', needUrlPath + '/img/help1.png' + part2Url);
    game.load.image('help2', needUrlPath + '/img/help2.png' + part2Url);
    game.load.image('ticker', needUrlPath + '/img/ticker.png' + part2Url);

    game.load.image('bet_down', needUrlPath + '/img/bet_down.png' + part2Url);
    game.load.image('bet_down_p', needUrlPath + '/img/bet_down_p.png' + part2Url);
    game.load.image('bet_up', needUrlPath + '/img/bet_up.png' + part2Url);
    game.load.image('bet_up_p', needUrlPath + '/img/bet_up_p.png' + part2Url);
    game.load.image('call', needUrlPath + '/img/call.png' + part2Url);
    game.load.image('call_p', needUrlPath + '/img/call_p.png' + part2Url);
    game.load.image('exit_help', needUrlPath + '/img/exit_help.png' + part2Url);
    game.load.image('exit_help_p', needUrlPath + '/img/exit_help_p.png' + part2Url);
    game.load.image('next', needUrlPath + '/img/next.png' + part2Url);
    game.load.image('next_p', needUrlPath + '/img/next_p.png' + part2Url);
    game.load.image('prev', needUrlPath + '/img/prev.png' + part2Url);
    game.load.image('prev_p', needUrlPath + '/img/prev_p.png' + part2Url);
    game.load.image('red_square', needUrlPath + '/img/red_square.png' + part2Url);
    game.load.image('purple_btn', needUrlPath + '/img/purple_btn.png' + part2Url);
    game.load.image('red_btn', needUrlPath + '/img/red_btn.png' + part2Url);

    game.load.image('dol', needUrlPath + '/img/dol.png' + part2Url);
    game.load.image('dol22', needUrlPath + '/img/dol22.png' + part2Url);
    game.load.image('goodLuck', needUrlPath + '/img/goodLuck.png' + part2Url);
    game.load.image('win_center', needUrlPath + '/img/win_center.png' + part2Url);
    game.load.image('big_dol', needUrlPath + '/img/big_dol.png' + part2Url);
    game.load.image('superball_hit', needUrlPath + '/img/superball_hit.png' + part2Url);
    game.load.image('sorry', needUrlPath + '/img/sorry.png' + part2Url);
    game.load.image('picks', needUrlPath + '/img/picks.png' + part2Url);
    game.load.image('jackpot_amount', needUrlPath + '/img/jackpot_amount.png' + part2Url);
    game.load.image('right_panel', needUrlPath + '/img/right_panel.png' + part2Url);
    game.load.image('choose_double', needUrlPath + '/img/CHOOSE_DOUBLE.png' + part2Url);

    game.load.image('yellow_btn', needUrlPath + '/img/yellow_btn/btn.png' + part2Url);
    game.load.image('green_btn', needUrlPath + '/img/green_btn/btn.png' + part2Url);
    game.load.image('last_ball_bg', needUrlPath + '/img/text6.png' + part2Url);
    game.load.image('last_ball_pay', needUrlPath + '/img/text7.png' + part2Url);
    game.load.image('last_ball_super', needUrlPath + '/img/text8.png' + part2Url);

    game.load.image('red_bg_1', needUrlPath + '/img/red_bg_1.png' + part2Url);
    game.load.image('red_bg_2', needUrlPath + '/img/red_bg_2.png' + part2Url);
    game.load.image('red_bg_3', needUrlPath + '/img/red_bg_3.png' + part2Url);
    game.load.image('red_bg_4', needUrlPath + '/img/red_bg_4.png' + part2Url);
    game.load.image('red_bg_5', needUrlPath + '/img/red_bg_5.png' + part2Url);
    game.load.image('red_bg_6', needUrlPath + '/img/red_bg_6.png' + part2Url);
    game.load.image('red_bg_7', needUrlPath + '/img/red_bg_7.png' + part2Url);
    for (var i = 1; i <= 80; ++i) {
      game.load.image('yellow_' + i, needUrlPath + '/img/yellow_btn/' + i + '.png' + part2Url);
      game.load.image('green_' + i, needUrlPath + '/img/green_btn/' + i + '.png' + part2Url);
      game.load.image('red_' + i, needUrlPath + '/img/red_btn/' + i + '.png' + part2Url);
    }
    for (var i = 1; i <= 80; ++i) {
      game.load.image('ball_' + i, needUrlPath + '/img/balls/' + i + '.png' + part2Url);
    }

    game.load.atlasJSONHash('big_red_border', needUrlPath + '/img/spritesheet.png', needUrlPath + '/img/sprites.json' + part2Url);

    game.load.audio('number', needUrlPath + '/sounds/number.mp3' + part2Url);
    game.load.audio('autopick', needUrlPath + '/sounds/autopick.mp3' + part2Url);
    game.load.audio('autopick_end', needUrlPath + '/sounds/autopick_end.mp3' + part2Url);
    game.load.audio('betDown', needUrlPath + '/sounds/betDown.mp3' + part2Url);
    game.load.audio('betUp', needUrlPath + '/sounds/betUp.mp3' + part2Url);
    game.load.audio('betmax', needUrlPath + '/sounds/betmax.mp3' + part2Url);
    game.load.audio('bwin', needUrlPath + '/sounds/bwin.mp3' + part2Url);
    game.load.audio('endgame', needUrlPath + '/sounds/endgame.mp3' + part2Url);
    game.load.audio('exit', needUrlPath + '/sounds/exit.mp3' + part2Url);
    game.load.audio('help', needUrlPath + '/sounds/help.mp3' + part2Url);
    game.load.audio('nummax', needUrlPath + '/sounds/nummax.mp3' + part2Url);
    game.load.audio('playgame', needUrlPath + '/sounds/playgame.mp3' + part2Url);
    game.load.audio('spusksharov', needUrlPath + '/sounds/spusksharov.mp3' + part2Url);
    game.load.audio('superballmiss', needUrlPath + '/sounds/superballmiss.mp3' + part2Url);
    game.load.audio('superballwin', needUrlPath + '/sounds/superballwin.mp3' + part2Url);
    game.load.audio('vibraniyshar', needUrlPath + '/sounds/vibraniyshar.mp3' + part2Url);
    game.load.audio('wipecard_quickpick', needUrlPath + '/sounds/wipecard_quickpick.mp3' + part2Url);
    game.load.audio('zvyksharov', needUrlPath + '/sounds/zvyksharov.mp3' + part2Url);
    game.load.audio('win', needUrlPath + '/sounds/win.wav' + part2Url);
    game.load.audio('coins', needUrlPath + '/sounds/coins.mp3' + part2Url);
    game.load.audio('half_sound', needUrlPath + '/sounds/Z_02_Cub.mp3' + part2Url);
    for (var i = 1; i <= 10; ++i) {
      game.load.audio('ballSound' + i, needUrlPath + '/sounds/ball' + i + '.mp3' + part2Url);
    }
    game.load.spritesheet('superball_winner', needUrlPath + '/img/superball_winner.png' + part2Url, 220, 59, 5);
    game.load.spritesheet('touch_anim', needUrlPath + '/img/touch_anim.png' + part2Url, 547, 70, 5);
    game.load.spritesheet('last_ball', needUrlPath + '/img/last_ball.png' + part2Url, 536, 70, 5);
    game.load.spritesheet('last_ball_start', needUrlPath + '/img/last_ball_start.png' + part2Url, 546, 76, 5);
    game.load.spritesheet('sorry_anim', needUrlPath + '/img/sorry_anim.png' + part2Url, 465, 69, 5);
    // game.load.spritesheet('big_red_border', needUrlPath + '/img/big_red_border.png' + part2Url, 646, 671, 6);
    game.load.spritesheet('purple_block_anim', needUrlPath + '/img/purple_block_anim.png' + part2Url, 61, 65, 5);
    game.load.spritesheet('purple_border_anim', needUrlPath + '/img/purple_border_anim.png' + part2Url, 150, 150, 10);
    game.load.spritesheet('red_border_anim', needUrlPath + '/img/red_border_anim.png' + part2Url, 148, 148, 11);
    game.load.spritesheet('red_btn_anim', needUrlPath + '/img/red_btn_anim.png' + part2Url, 61, 65, 8);
    game.load.spritesheet('red_square_anim', needUrlPath + '/img/red_square_anim.png' + part2Url, 66, 63, 8);
    game.load.spritesheet('red_square_anim2', needUrlPath + '/img/red_square_anim2.png' + part2Url, 66, 63, 5);
    game.load.spritesheet('coin_anim_2', needUrlPath + '/img/coin_anim2.png' + part2Url, 135, 135, 8);
    game.load.spritesheet('btn_border_anim', needUrlPath + '/img/btn_border_anim.png' + part2Url, 103, 73, 3);
    game.load.spritesheet('Purple_stars', needUrlPath + '/img/Purple_stars.png' + part2Url, 232, 234, 6);
    game.load.spritesheet('Red_stars', needUrlPath + '/img/Red_stars.png' + part2Url, 290, 294, 6);
    game.load.spritesheet('Yellow_stars', needUrlPath + '/img/Yellow_stars.png' + part2Url, 202, 202, 6);
  };

  preload.create = function() {
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
    game.sound.mute = false;
    if (firstRequest) {
      checkBalance();
      game.scale.refresh();
      document.getElementById('preloader').style.display = 'none';
      game.state.start('game1');
      checkWidth();
    } else {
      preloaderStatus = true;
    }
  };

  game.state.add('preload', preload);

})();

game.state.start('preload');