var game = new Phaser.Game(864.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;

        // звуки
        // заполняем массив звуками выигрышей
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');

        soundWinLineDurations[0] = 1000;
        soundWinLineDurations[1] = 1000;
        soundWinLineDurations[2] = 6000;


        // изображения
        game.add.sprite(0,0, 'border');
        var slotBackground = game.add.sprite(99,23, 'slotBackground');
        slotBackground.scale.setTo(1.045, 1.045);
        game.add.sprite(110,99, 'slots');
        var slotHeader = game.add.sprite(99,23, 'slotHeader');
        var slotFooter = game.add.sprite(99,431, 'slotFooter');
        slotHeader.scale.setTo(1.045, 1.045);
        slotFooter.scale.setTo(1.045, 1.045);

        
        var lineNumber1 = game.add.sprite(99,248, 'number1');
        var lineNumber2 = game.add.sprite(99,146, 'number2');
        var lineNumber3 = game.add.sprite(99,342, 'number3');
        var lineNumber4 = game.add.sprite(99,108, 'number4');
        var lineNumber5 = game.add.sprite(99,380, 'number5');
        lineNumber1.scale.setTo(1.048, 1.048);
        lineNumber2.scale.setTo(1.048, 1.048);
        lineNumber3.scale.setTo(1.048, 1.048);
        lineNumber4.scale.setTo(1.048, 1.048);
        lineNumber5.scale.setTo(1.048, 1.048);

        //слоты
        totalWinPosion = [500,498];
        slotPosition = [[142+10, 55+57],
                        [142+10, 147+57+5],
                        [142+10, 242+57+11],
                            [254+10+2, 55+57],
                            [254+10+2, 147+57+5],
                            [254+10+2, 242+57+11],
                                [365+10+4, 55+57],
                                [365+10+4, 147+57+5],
                                [365+10+4, 242+57+11],
                                    [477+10+7, 55+57],
                                    [477+10+7, 147+57+5],
                                    [477+10+7, 242+57+11],
                                        [589+10+9, 55+57],
                                        [589+10+9, 147+57+5],
                                        [589+10+9, 242+57+11]];
        addSlots(game, slotPosition, 8);

        // линии
        linePosition = [[99+27,248+12], [99+27,146+12], [99+27,342+12], [99+27,108+12], [99+27,126+12]];
        addLinesTypeBOR(game, linePosition);
        linefull1.scale.setTo(1.048, 1.048);
        linefull2.scale.setTo(1.048, 1.048);
        linefull3.scale.setTo(1.048, 1.048);
        linefull4.scale.setTo(1.048, 1.048);
        linefull5.scale.setTo(1.048, 1.048);

        //позиции ячеек относящиеся к линиям
        cellPositionOnLines = [[1,4,7,10,13], [0,3,6,9,12], [2,5,8,11,14], [0,4,8,10,12], [2,4,6,10,14]];
        slotAnimPosition = slotPosition; //позиции анимированных ячеек
        
        //создаем массив содержащий все анимации
        addAllCellAnim(game, slotPosition, numberOfSlotValues);

        //цветные рамки для ячеек
        addSquares(game, slotPosition, 5);

        // кнопки
        addButtonsGame1TypeBOR(game);
        


        //счет
        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[690+10, 475+25, 22], [690+10, 440+27, 22], [200+10, 473+25, 22], [690+10, 420+27, 22], [690+10, 420+27, 22]];
        addScore(game, scorePosions, bet, lines, balance, betline);

        addTitles();

        // анимация
        
        full_and_sound();

    };

    game1.update = function () {
        
    };

    game.state.add('game1', game1);

};
