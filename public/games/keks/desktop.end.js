(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }

        game.load.image('startButton', needUrlPath + '/img/image1445.png');
        game.load.image('startButton_p', needUrlPath + '/img/image1447.png');
        game.load.image('startButton_d', needUrlPath + '/img/image1451.png');
        game.load.image('selectGame', needUrlPath + '/img/image1419.png');
        game.load.image('selectGame_p', needUrlPath + '/img/image1421.png');
        game.load.image('selectGame_d', needUrlPath + '/img/image1425.png');
        game.load.image('payTable', needUrlPath + '/img/image1428.png');
        game.load.image('payTable_p', needUrlPath + '/img/image1430.png');
        game.load.image('payTable_d', needUrlPath + '/img/image1433.png');
        game.load.image('automaricstart', needUrlPath + '/img/image1436.png');
        game.load.image('automaricstart_p', needUrlPath + '/img/image1438.png');
        game.load.image('automaricstart_d', needUrlPath + '/img/image1442.png');
        game.load.image('betone', needUrlPath + '/img/image1471.png');
        game.load.image('betone_p', needUrlPath + '/img/image1473.png');
        game.load.image('betone_d', needUrlPath + '/img/image1477.png');
        game.load.image('betmax', needUrlPath + '/img/image1480.png');
        game.load.image('betmax_p', needUrlPath + '/img/image1482.png');
        game.load.image('betmax_d', needUrlPath + '/img/image1485.png');
        game.load.image('buttonLine1', needUrlPath + '/img/image1505.png');
        game.load.image('buttonLine1_p', needUrlPath + '/img/image1507.png');
        game.load.image('buttonLine1_d', needUrlPath + '/img/image1511.png');
        game.load.image('buttonLine3', needUrlPath + '/img/image1496.png');
        game.load.image('buttonLine3_p', needUrlPath + '/img/image1498.png');
        game.load.image('buttonLine3_d', needUrlPath + '/img/image1502.png');
        game.load.image('buttonLine5', needUrlPath + '/img/image1488.png');
        game.load.image('buttonLine5_p', needUrlPath + '/img/image1490.png');
        game.load.image('buttonLine5_d', needUrlPath + '/img/image1493.png');
        game.load.image('buttonLine7', needUrlPath + '/img/image1462.png');
        game.load.image('buttonLine7_p', needUrlPath + '/img/image1464.png');
        game.load.image('buttonLine7_d', needUrlPath + '/img/image1468.png');
        game.load.image('buttonLine9', needUrlPath + '/img/image1454.png');
        game.load.image('buttonLine9_p', needUrlPath + '/img/image1456.png');
        game.load.image('buttonLine9_d', needUrlPath + '/img/image1459.png');

        game.load.image('game.number1', needUrlPath + '/img/1.png');
        game.load.image('game.number2', needUrlPath + '/img/2.png');
        game.load.image('game.number3', needUrlPath + '/img/3.png');
        game.load.image('game.number4', needUrlPath + '/img/4.png');
        game.load.image('game.number5', needUrlPath + '/img/5.png');
        game.load.image('game.number6', needUrlPath + '/img/6.png');
        game.load.image('game.number7', needUrlPath + '/img/7.png');
        game.load.image('game.number8', needUrlPath + '/img/8.png');
        game.load.image('game.number9', needUrlPath + '/img/9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shape107.png');

        game.load.image('card_39', needUrlPath + '/img/shape109.png');
        game.load.image('card_40', needUrlPath + '/img/shape111.png');
        game.load.image('card_41', needUrlPath + '/img/shape113.png');
        game.load.image('card_42', needUrlPath + '/img/shape115.png');
        game.load.image('card_43', needUrlPath + '/img/shape117.png');
        game.load.image('card_44', needUrlPath + '/img/shape119.png');
        game.load.image('card_45', needUrlPath + '/img/shape121.png');
        game.load.image('card_46', needUrlPath + '/img/shape123.png');
        game.load.image('card_47', needUrlPath + '/img/shape125.png');
        game.load.image('card_48', needUrlPath + '/img/shape127.png');
        game.load.image('card_49', needUrlPath + '/img/shape129.png');
        game.load.image('card_50', needUrlPath + '/img/shape131.png');
        game.load.image('card_51', needUrlPath + '/img/shape133.png');

        game.load.image('card_26', needUrlPath + '/img/shape135.png');
        game.load.image('card_27', needUrlPath + '/img/shape137.png');
        game.load.image('card_28', needUrlPath + '/img/shape139.png');
        game.load.image('card_29', needUrlPath + '/img/shape141.png');
        game.load.image('card_30', needUrlPath + '/img/shape143.png');
        game.load.image('card_31', needUrlPath + '/img/shape145.png');
        game.load.image('card_32', needUrlPath + '/img/shape147.png');
        game.load.image('card_33', needUrlPath + '/img/shape149.png');
        game.load.image('card_34', needUrlPath + '/img/shape151.png');
        game.load.image('card_35', needUrlPath + '/img/shape153.png');
        game.load.image('card_36', needUrlPath + '/img/shape155.png');
        game.load.image('card_37', needUrlPath + '/img/shape157.png');
        game.load.image('card_38', needUrlPath + '/img/shape159.png');

        game.load.image('card_0', needUrlPath + '/img/shape161.png');
        game.load.image('card_1', needUrlPath + '/img/shape163.png');
        game.load.image('card_2', needUrlPath + '/img/shape165.png');
        game.load.image('card_3', needUrlPath + '/img/shape167.png');
        game.load.image('card_4', needUrlPath + '/img/shape169.png');
        game.load.image('card_5', needUrlPath + '/img/shape171.png');
        game.load.image('card_6', needUrlPath + '/img/shape173.png');
        game.load.image('card_7', needUrlPath + '/img/shape175.png');
        game.load.image('card_8', needUrlPath + '/img/shape177.png');
        game.load.image('card_9', needUrlPath + '/img/shape179.png');
        game.load.image('card_10', needUrlPath + '/img/shape181.png');
        game.load.image('card_11', needUrlPath + '/img/shape183.png');
        game.load.image('card_12', needUrlPath + '/img/shape185.png');

        game.load.image('card_13', needUrlPath + '/img/shape187.png');
        game.load.image('card_14', needUrlPath + '/img/shape189.png');
        game.load.image('card_15', needUrlPath + '/img/shape191.png');
        game.load.image('card_16', needUrlPath + '/img/shape193.png');
        game.load.image('card_17', needUrlPath + '/img/shape195.png');
        game.load.image('card_18', needUrlPath + '/img/shape197.png');
        game.load.image('card_19', needUrlPath + '/img/shape199.png');
        game.load.image('card_20', needUrlPath + '/img/shape201.png');
        game.load.image('card_21', needUrlPath + '/img/shape203.png');
        game.load.image('card_22', needUrlPath + '/img/shape205.png');
        game.load.image('card_23', needUrlPath + '/img/shape207.png');
        game.load.image('card_24', needUrlPath + '/img/shape209.png');
        game.load.image('card_25', needUrlPath + '/img/shape211.png');

        game.load.image('card_52', needUrlPath + '/img/shape213.png');

        //game.load.image('pick', 'img/shape340.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');

        //файлы которых может и не быть
        game.load.image('takeOrRisk1', needUrlPath + '/img/takeOrRisk.png');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('bonusGame', needUrlPath + '/img/bonusGame.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('game.backgroundGame1Score1', needUrlPath + '/img/backgroundGame1Score1.png');
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/pech.png', 96, 96); //текущее кол-во кадров = 11
        game.load.image('topScoreGame2', needUrlPath + '/img/image457.png');

        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');

        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sound/sound30.mp3');

        game.load.image('game.backgroundTotal2', needUrlPath + '/img/image443.png');
        /* sources */

        game.load.image('game.background', needUrlPath + '/img/canvas-bg.svg');
        game.load.image('game.background1', needUrlPath + '/img/background1.png');
        game.load.image('game.totalBet', needUrlPath + '/img/totalBet.png');



        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');
        game.load.audio('game.babaOpenBox1', needUrlPath + '/sound/babaOpenBox1.mp3');
        game.load.audio('game.babaOpenBox2', needUrlPath + '/sound/babaOpenBox2.mp3');
        game.load.audio('game.babaOpenBox3', needUrlPath + '/sound/babaOpenBox3.mp3');
        game.load.audio('game.babaOpenBadBox', needUrlPath + '/sound/babaOpenBadBox.mp3');
        game.load.audio('game.babaLoseFromWolf', needUrlPath + '/sound/lose.mp3');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.jpg', 96, 96);
        game.load.spritesheet('barSpin', needUrlPath + '/img/barSpin.png', 96, 384);

        game.load.spritesheet('game.baba1', needUrlPath + '/img/baba1.png', 96, 176);
        game.load.spritesheet('game.baba2', needUrlPath + '/img/baba2.png', 96, 176);
        game.load.spritesheet('game.baba3', needUrlPath + '/img/baba3.png', 96, 176);
        game.load.spritesheet('game.baba4', needUrlPath + '/img/baba4.png', 96, 176);
        game.load.spritesheet('game.baba5', needUrlPath + '/img/baba5.png', 96, 176);
        game.load.spritesheet('game.baba6', needUrlPath + '/img/baba6.png', 96, 176);

        game.load.spritesheet('game.baba7', needUrlPath + '/img/baba7.png', 128, 160);
        game.load.spritesheet('game.baba8', needUrlPath + '/img/baba8.png', 128, 160);
        game.load.spritesheet('game.baba9', needUrlPath + '/img/baba9.png', 128, 160);
        game.load.spritesheet('game.baba10', needUrlPath + '/img/baba10.png', 128, 160);
        game.load.spritesheet('game.baba11', needUrlPath + '/img/baba11.png', 128, 160);
        game.load.spritesheet('game.baba12', needUrlPath + '/img/baba12.png', 112, 160);

        game.load.spritesheet('game.cat', needUrlPath + '/img/cat.png', 48, 64);
        game.load.spritesheet('game.testo', needUrlPath + '/img/testo.png', 144, 64);

        game.load.spritesheet('game.flashNamber1', needUrlPath + '/img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', needUrlPath + '/img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', needUrlPath + '/img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', needUrlPath + '/img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', needUrlPath + '/img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', needUrlPath + '/img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', needUrlPath + '/img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', needUrlPath + '/img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', needUrlPath + '/img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.blin', needUrlPath + '/img/blin.png', 96, 96);
        game.load.spritesheet('game.pech', needUrlPath + '/img/pech.png', 96, 96);

        game.load.image('game.backgroundGame3', needUrlPath + '/img/shape104.png');

        game.load.audio('game.winPech', needUrlPath + '/sound/winPech.mp3');


        game.load.image('game.backgroundGame4', needUrlPath + '/img/shape1545.png');
        game.load.image('game.babaLegs', needUrlPath + '/img/shape1191.png');
        game.load.spritesheet('game.cat2', needUrlPath + '/img/cat2.png', 80, 80);

        game.load.spritesheet('game.openPie', needUrlPath + '/img/openPie.png', 96, 112);
        game.load.spritesheet('game.openPorridge', needUrlPath + '/img/openPorridge.png', 96, 112);
        game.load.spritesheet('game.openBlin', needUrlPath + '/img/openBlin.png', 96, 112);
        game.load.spritesheet('game.openChiсken', needUrlPath + '/img/openChiсken.png', 96, 112);
        game.load.spritesheet('game.openFish', needUrlPath + '/img/openFish.png', 96, 112);
        game.load.spritesheet('game.openPig', needUrlPath + '/img/openPig.png', 96, 112);
        game.load.spritesheet('game.openSmoke', needUrlPath + '/img/openSmoke.png', 96, 144);

        game.load.spritesheet('game.blin2', needUrlPath + '/img/blin2.png', 96, 112);
        game.load.spritesheet('game.chiсken', needUrlPath + '/img/chiсken.png', 96, 112);
        game.load.spritesheet('game.fish', needUrlPath + '/img/fish.png', 96, 112);
        game.load.spritesheet('game.pie', needUrlPath + '/img/pie.png', 96, 112);
        game.load.spritesheet('game.pig', needUrlPath + '/img/pig.png', 96, 112);
        game.load.spritesheet('game.porridge', needUrlPath + '/img/porridge.png', 96, 112);
        game.load.spritesheet('game.smoke', needUrlPath + '/img/smoke.png', 96, 144);

        //game.load.image('game.babaLegsFromOpenBox', 'img/babaLegsFromOpenBox.svg');
        game.load.spritesheet('game.babaOpenBox', needUrlPath + '/img/babaOpenBox.png', 128, 300);
        game.load.spritesheet('game.babaLoseStick', needUrlPath + '/img/babaLoseStick.png', 224, 192);
        game.load.spritesheet('game.babSad', needUrlPath + '/img/babSad.png', 128, 160);
        game.load.image('game.babaSadLags', needUrlPath + '/img/babaSadLags.png');
        game.load.spritesheet('game.catSad', needUrlPath + '/img/catSad.png', 80, 80);

        game.load.image('game.backgroundForGame2', needUrlPath + '/img/shape1189.png');
        game.load.image('game.titleForGame2', needUrlPath + '/img/shape1399.png');
        game.load.spritesheet('game.arrow1', needUrlPath + '/img/arrow1.png', 32, 32);
        game.load.spritesheet('game.arrow2', needUrlPath + '/img/arrow2.png', 32, 32);
        game.load.spritesheet('game.boogiman', needUrlPath + '/img/boogiman.png', 96, 80);
        game.load.spritesheet('game.babaStrike1_wolf', needUrlPath + '/img/babaStrike1_wolf.png', 208, 224);
        game.load.spritesheet('game.babaStrike2_wolf', needUrlPath + '/img/babaStrike2_wolf.png', 208, 192);
        game.load.spritesheet('game.upWolf', needUrlPath + '/img/upWolf.png', 160, 448);
        game.load.spritesheet('game.babaAndFolf11', needUrlPath + '/img/babaAndFolf11.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf12', needUrlPath + '/img/babaAndFolf12.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf21', needUrlPath + '/img/babaAndFolf21.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf22', needUrlPath + '/img/babaAndFolf22.png', 500, 471);
        game.load.spritesheet('game.babaTakeKeks1', needUrlPath + '/img/babaTakeKeks1.png', 400, 300);
        game.load.spritesheet('game.babaTakeKeks2', needUrlPath + '/img/babaTakeKeks2.png', 400, 300);

        game.load.image('game.pechka', needUrlPath + '/img/shape1666.png');
        game.load.image('thicket', needUrlPath + '/img/thiket.png');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

