function game3() {
    (function () {

        var game3 = {};

        game3.preload = function () {};

        game3.create = function () {

            checkGame = 3;

            //сбрамываем необходимые переменные
            autostart = false;
            checkAutoStart = false;
            checkUpdateBalance = false;
            stepTotalWinR = 0;
            ropeStep = 0;
            checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса
            
            //звуки
            babaOpenBox1 = game.add.sound('game.babaOpenBox1');
            babaOpenBox2 = game.add.sound('game.babaOpenBox2');
            babaOpenBox3 = game.add.sound('game.babaOpenBox3');
            babaOpenBadBox = game.add.sound('game.babaOpenBadBox');
            
            
            //изображения
            background = game.add.sprite(0,0, 'game.background');
            game.add.sprite(93,53, 'game.background1');
            totalBet = game.add.sprite(95,22, 'game.totalBet');
            backgroundGame4 = game.add.sprite(95,54, 'game.backgroundGame4');
            showBabaLegs(350,465);

            pechka1 =  game.add.sprite(143,182,'game.pechka');
            pechka1.inputEnabled = true;
            pechka1.input.useHandCursor = true;
            pechka1.events.onInputDown.add(function(){
                pechka1.inputEnabled = false;
                buttonLine1.loadTexture('buttonLine1_d');
                buttonLine1.inputEnabled = false;
                buttonLine1.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) {
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                ropesAnim[0](ropeValues, ropeStep, checkHelm);

                setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

                ropeStep += 1;
            });

            pechka2 =  game.add.sprite(255,198,'game.pechka');
            pechka2.inputEnabled = true;
            pechka2.input.useHandCursor = true;
            pechka2.events.onInputDown.add(function(){
                pechka2.inputEnabled = false;
                buttonLine3.loadTexture('buttonLine3_d');
                buttonLine3.inputEnabled = false;
                buttonLine3.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }


                ropesAnim[1](ropeValues, ropeStep);
                setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

                ropeStep += 1;
            });

            pechka3 =  game.add.sprite(367,182,'game.pechka');
            pechka3.inputEnabled = true;
            pechka3.input.useHandCursor = true;
            pechka3.events.onInputDown.add(function(){
                pechka3.inputEnabled = false;
                buttonLine5.loadTexture('buttonLine5_d');
                buttonLine5.inputEnabled = false;
                buttonLine5.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                ropesAnim[2](ropeValues, ropeStep);
                setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

                ropeStep += 1;
            });

            pechka4 =  game.add.sprite(479,198,'game.pechka');
            pechka4.inputEnabled = true;
            pechka4.input.useHandCursor = true;
            pechka4.events.onInputDown.add(function(){
                pechka4.inputEnabled = false;
                buttonLine7.loadTexture('buttonLine7_d');
                buttonLine7.inputEnabled = false;
                buttonLine7.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                ropesAnim[3](ropeValues, ropeStep);
                setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

                ropeStep += 1;
            });

            pechka5 =  game.add.sprite(591,182,'game.pechka');
            pechka5.inputEnabled = true;
            pechka5.input.useHandCursor = true;
            pechka5.events.onInputDown.add(function(){
                pechka5.inputEnabled = false;
                buttonLine9.loadTexture('buttonLine9_d');
                buttonLine9.inputEnabled = false;
                buttonLine9.input.useHandCursor = false;

                if(ropeValues[ropeStep] != 0 && ropeValues[ropeStep] !== 6) { // изменяем значение общего выигрыша
                    stepTotalWinR += betline*lines*ropeValues[ropeStep];
                }

                ropesAnim[4](ropeValues, ropeStep);
                setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

                ropeStep += 1;
            });

            //счет
            scorePosions = [[260, 23, 20], [435, 23, 20], [610, 23, 20], [475, 68, 30]];
            addScore(game, scorePosions, bet, '', balance - totalWinR, betline);



            //кнопки и анимации событий нажатия

            ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша   
            
            winRopeNumberPosition = [[185,155],[195+103,155],[195+220,155],[195+330,155],[195+440,155]];
            timeoutForShowWinRopeNumber = 2000;
            timeoutGame3ToGame4 = 2000;
            typeWinRopeNumberAnim = 0;
            checkHelm = false;
            winRopeNumberSize = 22;


            ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
                lockDisplay();
                setTimeout('unlockDisplay();',5500);
                playAnim(0,0);
            };

            ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
                lockDisplay();
                setTimeout('unlockDisplay();',5500);
                playAnim(112,16);
            };

            ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
                lockDisplay();
                setTimeout('unlockDisplay();',5500);
                playAnim(112*2,0);
            };

            ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
                lockDisplay();
                setTimeout('unlockDisplay();',5500);
                playAnim(112*3,16);
            };

            ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
                lockDisplay();
                setTimeout('unlockDisplay();',5500);
                playAnim(112*4,0);
            };

            addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);


            var arr = [];
            var blin = ['blin2','openBlin'];
            var chiсken = ['chiсken','openChiсken'];
            var fish = ['fish','openFish'];
            var pie = ['pie','openPie'];
            var pig = ['pig','openPig'];
            var porridge = ['porridge','openPorridge'];
            arr.push(blin,chiсken,fish,pie,pig,porridge);

            //анимации

            function randomBox(ropeValue) {
                //добавление массивов изображений
                var arr = [
                    ['fish','openFish'],
                    ['porridge','openPorridge'],
                    ['chiсken','openChiсken'],
                    ['pie','openPie'],
                    ['pig','openPig']
                    /*['blin2','openBlin']*/
                ];

                var blin = ['blin2','openBlin'];
                var chiсken = ['chiсken','openChiсken'];
                var fish = ['fish','openFish'];
                var pie = ['pie','openPie'];
                var pig = ['pig','openPig'];
                var porridge = ['porridge','openPorridge'];


                arr.push(chiсken,fish,pie,pig,porridge);
                //arr.push(smoke);

                var rand = Math.floor(Math.random() * arr.length);

                return arr[rand];
            }

            function showBabaLegs(x,y){
                babaLegs = game.add.sprite(x,y, 'game.babaLegs');
            }

            function hideBaba(){
                baba7.visible = false;
                baba8.visible = false;
                baba9.visible = false;
                baba10.visible = false;
                baba11.visible = false;
                baba12.visible = false;

                baba7.animations.stop();
                baba8.animations.stop();
                baba9.animations.stop();
                baba10.animations.stop();
                baba11.animations.stop();
                baba12.animations.stop();
            }

            function showRandBaba(){
                var randBaba = randomNumber(7,10);

                switch(randBaba) {
                    case 7:
                        hideBaba();
                        baba7.visible = true;
                        baba7.animations.getAnimation('baba7').play();
                        break;
                    case 8:
                        hideBaba();
                        baba8.visible = true;
                        baba8.animations.getAnimation('baba8').play();
                        break;
                    case 9:
                        hideBaba();
                        baba9.visible = true;
                        baba9.animations.getAnimation('baba9').play();
                        break;
                    case 10:
                        hideBaba();
                        baba10.visible = true;
                        baba10.animations.getAnimation('baba10').play();
                        break;
                    case 12:
                        hideBaba();
                        baba12.visible = true;
                        baba12.animations.getAnimation('baba12').play();
                        break;
                }
            }

            function createBaba(x,y){
                baba7 = game.add.sprite(x,y, 'game.baba7');
                baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
                baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                    baba7.animations.stop();
                    showRandBaba();
                });
                baba7.visible = false;

                baba8 = game.add.sprite(x,y, 'game.baba8');
                baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
                baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                    baba8.animations.stop();
                    showRandBaba();
                });
                baba8.visible = false;

                baba9 = game.add.sprite(x,y, 'game.baba9');
                baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
                baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                    baba9.animations.stop();
                    showRandBaba();
                });
                baba9.visible = false;

                baba10 = game.add.sprite(x,y, 'game.baba10');
                baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
                baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                    baba10.animations.stop();
                    showRandBaba();
                });
                baba10.visible = false;

                baba11 = game.add.sprite(x,y, 'game.baba11');
                baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
                baba11.animations.getAnimation('baba11').play().onComplete.add(function(){
                    baba11.animations.stop();
                    showRandBaba();
                });
                baba11.visible = false;

                baba12 = game.add.sprite(x,y, 'game.baba12');
                baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                    baba12.animations.stop();
                    showRandBaba();
                });
                baba12.visible = false;
            }

            function playAnim(x,y){
                hideBaba();
                babaLegs.visible = false;

                var box = randomBox(ropeValues[ropeStep]);

                babaOpenBox1.play();
                setTimeout("babaOpenBox2.play();",1000);

                
                if(ropeValues[ropeStep] == 0) {
                    box = ['smoke','openSmoke'];
                }

                if(ropeValues[ropeStep] == 6) {
                    box = ['blin2','openBlin'];
                }

                if(box[0] == 'smoke'){
                    box1 = game.add.sprite(143+x,150+y, 'game.' + box[1]);
                    box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

                    box1.animations.getAnimation('box1').play().onComplete.add(function () {

                        babaOpenBadBox.play();

                        box1n = game.add.sprite(143+x,150+y, 'game.' + box[0]);
                        box1n.animations.add('box1n', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 5, false);
                        box1n.animations.getAnimation('box1n').play().onComplete.add(function(){
                            hidedButtonArray = [];
                            game.state.start('game1');
                        });
                    });

                    babaOpenBox = game.add.sprite(122+x,182+y, 'game.babaOpenBox');
                    babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
                    babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
                        babaOpenBox.visible = false;

                        cat2.visible = false;
                        catSad = game.add.sprite(271,70, 'game.catSad');
                        catSad.animations.add('catSad', [0,1], 5, true);
                        catSad.animations.getAnimation('catSad').play();

                        //showBabaLegs(137,452);
                        babaLoseStick = game.add.sprite(122+x,292+y, 'game.babaLoseStick');
                        babaLoseStick.animations.add('babaLoseStick', [0,1,2,3,4,5,6], 5, false);
                        babaLoseStick.animations.getAnimation('babaLoseStick').play().onComplete.add(function(){
                            babaLoseStick.visible = false;

                            babSad = game.add.sprite(122+x,293+y, 'game.babSad');
                            babSad.animations.add('babSad', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 5, false);
                            babSad.animations.getAnimation('babSad').play().onComplete.add(function(){
                                hidedButtonArray = [];
                                game.state.start('game1');
                            });
                            babaSadLags = game.add.sprite(137+x,452+y, 'game.babaSadLags');
                        });



                    });
                } else if(box[0] == 'blin2') {
                    box1 = game.add.sprite(143+x,182+y, 'game.' + box[1]);
                    box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

                    box1.animations.getAnimation('box1').play().onComplete.add(function () {
                        if(box[0] != 'blin2'){
                            setTimeout("babaOpenBox3.play();",0);
                            box1n = game.add.sprite(143+x,182+y, 'game.' + box[0]);
                            box1n.animations.add('box1n', [0,1,2], 5, true);
                            box1n.animations.getAnimation('box1n').play();
                        } else {
                            setTimeout("babaOpenBox3.play();",0);
                            box1n = game.add.sprite(143+x,182+y, 'game.' + box[0]);
                            box1n.animations.add('box1n', [0,1,2,3,4,5,6,7], 5, false);
                            box1n.animations.getAnimation('box1n').play();
                        }
                    });

                    babaOpenBox = game.add.sprite(122+x,182+y, 'game.babaOpenBox');
                    babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
                    babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
                        babaOpenBox.visible = false;

                        showBabaLegs(137+x,452+y);
                        baba12 = game.add.sprite(122+x,292+y, 'game.baba12');
                        baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        baba12.animations.getAnimation('baba12').play().onComplete.add(function(){

                            game.state.start('game4');
                            /*baba12.visible = false;

                            createBaba(122+x,292+y);
                            showRandBaba();*/
                        });



                    });
                } else {
                    box1 = game.add.sprite(143+x,182+y, 'game.' + box[1]);
                    box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

                    box1.animations.getAnimation('box1').play().onComplete.add(function () {
                        if(box[0] != 'blin2'){
                            setTimeout("babaOpenBox3.play();",0);
                            box1n = game.add.sprite(143+x,182+y, 'game.' + box[0]);
                            box1n.animations.add('box1n', [0,1,2], 5, true);
                            box1n.animations.getAnimation('box1n').play();
                        } else {
                            setTimeout("babaOpenBox3.play();",0);
                            box1n = game.add.sprite(143+x,182+y, 'game.' + box[0]);
                            box1n.animations.add('box1n', [0,1,2,3,4,5,6,7], 5, false);
                            box1n.animations.getAnimation('box1n').play();
                        }
                    });

                    babaOpenBox = game.add.sprite(122+x,182+y, 'game.babaOpenBox');
                    babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
                    babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
                        babaOpenBox.visible = false;

                        showBabaLegs(137+x,452+y);
                        baba12 = game.add.sprite(122+x,292+y, 'game.baba12');
                        baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                            baba12.visible = false;

                            createBaba(122+x,292+y);
                            showRandBaba();
                        });



                    });
                }
            }


            cat2 = game.add.sprite(271,70, 'game.cat2');
            cat2.animations.add('cat2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50], 5, true);
            cat2.animations.getAnimation('cat2').play();



            createBaba(334,306);

            showRandBaba();


            full_and_sound();


        };

        game3.update = function () {
        };

        game.state.add('game3', game3);

    })();

}