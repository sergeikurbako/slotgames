var game = new Phaser.Game(829.7, 598.95, Phaser.AUTO, 'game-area', 'ld29', null, false, false);

function game1() {
    
    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {
        
        checkGame = 1;

        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;


        // звуки
        var playSound = game.add.audio('play');


        slotPosition = [[141, 54], [141, 150], [141, 246], [253, 54], [253, 150], [253, 246], [365, 54], [365, 150], [365, 246], [476, 54], [476, 150], [476, 246], [588, 54], [588, 150], [588, 246], [141, 342], [253, 342], [365, 342], [476, 342], [588, 342]];
        addSlots(game, slotPosition);
        // изображения
        topBarImage = game.add.sprite(93,23, 'game.backgroundGame1Score1');

        game.add.sprite(0,0, 'game.background');
        game.add.sprite(93,53, 'game.background1');
        totalBet = game.add.sprite(95,21, 'game.totalBet');



        //addSelectionOfTheManyCellAnim(game, slotPosition);

        addTableTitle(game, 'play1To',270,358);




        var linePosition = [[134,199], [134,71], [134,322], [134,130], [134,95], [134,102], [134,228], [134,226], [134,120]];
        var numberPosition = [[109,183], [109,54], [109,310], [109,118], [109,246], [109,86], [109,278], [109,214], [109,150]];
        addLinesAndNumbers(game, linePosition, numberPosition);


        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);



        // кнопки
        addButtonsGame1(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230, 23, 20], [435, 23, 20], [610, 23, 20], [697, 369, 16], [697, 369, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        topGroup = game.add.group();
        var sourceArray = [
            ['game.baba1', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5],
            ['game.baba2', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba3', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5],
            ['game.baba4', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba5', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5],
            ['game.baba6', [125,326], [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5]
        ];

        playAnimQueue(sourceArray);

        cat = game.add.sprite(221,374, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557,390, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();

        full_and_sound();

    };



    game1.update = function () {
        game.world.bringToTop(topGroup);
    };

    game.state.add('game1', game1);

};
