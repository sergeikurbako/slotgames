window.game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-example');

isMobile = true;

var mobileX = 93;
var mobileY = 23;

var checkHelm = false; //проверка надела ли каска

//переменные получаемые из api
var gamename = 'luckyhaunter'; //название игры
var result;
var state;
var sid;
var user;
var min;
var id;
var balance = 10000000;
var extralife = 45;
var jackpots;
var betline = 1;
var lines = 9;
var bet = 9;
var info;
var wl;
var dcard;
var dwin;
var dcard2;
var select;

//звуки и полноэкранный режим
var fullStatus = false;
var soundStatus = true;

//game - гланый объект игры, в который все добавляется

//функция для рандома
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//включение звука и полноэкранного режимов
function full_and_sound(){
    if (!fullStatus)
        full = game.add.sprite(738,27, 'game.non_full');
    else
        full = game.add.sprite(738,27, 'game.full');
    full.inputEnabled = true;
    full.input.useHandCursor = true;
    full.events.onInputUp.add(function(){
        if (fullStatus == false){
            full.loadTexture('game.full');
            fullStatus = true;
            if(document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if(document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if(document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen();
            }
        } else {
            full.loadTexture('game.non_full');
            fullStatus = false;
            if(document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    });
    if (soundStatus)
        sound = game.add.sprite(738,53, 'sound_on');
    else
        sound = game.add.sprite(738,53, 'sound_off');
    sound.inputEnabled = true;
    sound.input.useHandCursor = true;
    sound.events.onInputUp.add(function(){
        if (soundStatus == true){
            sound.loadTexture('sound_off');
            soundStatus =false;
            game.sound.mute = true;
        } else {
            sound.loadTexture('sound_on');
            soundStatus = true;
            game.sound.mute = false;
        }
    });
}

//блокировка экрана
function lockDisplay() {
    document.getElementById('displayLock').style.display = 'block';
}
function unlockDisplay() {
    document.getElementById('displayLock').style.display = 'none';
}

//Функции связанные с линиями и их номерами

var line1; var linefull1; var number1;
var line2; var linefull2; var number2;
var line3; var linefull3; var number3;
var line4; var linefull4; var number4;
var line5; var linefull5; var number5;
var line6; var linefull6; var number6;
var line7; var linefull7; var number7;
var line8; var linefull8; var number8;
var line9; var linefull9; var number9;

//var linePosition = [[0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0], [0,0]] - координаты расположения линий
//var numberPosition = [[0,0], ...] - координаты расположения цифр
function addLinesAndNumbers(game, linePosition, numberPosition) {

    var linefullNames = ['linefull1', 'linefull2', 'linefull3', 'linefull4', 'linefull5', 'linefull6', 'linefull7', 'linefull8', 'linefull9'];
    var lineNames = ['line1', 'line2', 'line3', 'line4', 'line5', 'line6', 'line7', 'line8', 'line9'];

    //загружаем изображения в игру

    linefull1 = game.add.sprite(linePosition[0][0], linePosition[0][1], linefullNames[0]);
    linefull2 = game.add.sprite(linePosition[1][0], linePosition[1][1], linefullNames[1]);
    linefull3 = game.add.sprite(linePosition[2][0], linePosition[2][1], linefullNames[2]);
    linefull4 = game.add.sprite(linePosition[3][0], linePosition[3][1], linefullNames[3]);
    linefull5 = game.add.sprite(linePosition[4][0], linePosition[4][1], linefullNames[4]);
    linefull6 = game.add.sprite(linePosition[5][0], linePosition[5][1], linefullNames[5]);
    linefull7 = game.add.sprite(linePosition[6][0], linePosition[6][1], linefullNames[6]);
    linefull8 = game.add.sprite(linePosition[7][0], linePosition[7][1], linefullNames[7]);
    linefull9 = game.add.sprite(linePosition[8][0], linePosition[8][1], linefullNames[8]);

    linefull1.visible = false;
    linefull2.visible = false;
    linefull3.visible = false;
    linefull4.visible = false;
    linefull5.visible = false;
    linefull6.visible = false;
    linefull7.visible = false;
    linefull8.visible = false;
    linefull9.visible = false;

    line1 = game.add.sprite(linePosition[0][0], linePosition[0][1], lineNames[0]);
    line2 = game.add.sprite(linePosition[1][0], linePosition[1][1], lineNames[1]);
    line3 = game.add.sprite(linePosition[2][0], linePosition[2][1], lineNames[2]);
    line4 = game.add.sprite(linePosition[3][0], linePosition[3][1], lineNames[3]);
    line5 = game.add.sprite(linePosition[4][0], linePosition[4][1], lineNames[4]);
    line6 = game.add.sprite(linePosition[5][0], linePosition[5][1], lineNames[5]);
    line7 = game.add.sprite(linePosition[6][0], linePosition[6][1], lineNames[6]);
    line8 = game.add.sprite(linePosition[7][0], linePosition[7][1], lineNames[7]);
    line9 = game.add.sprite(linePosition[8][0], linePosition[8][1], lineNames[8]);

    line1.visible = false;
    line2.visible = false;
    line3.visible = false;
    line4.visible = false;
    line5.visible = false;
    line6.visible = false;
    line7.visible = false;
    line8.visible = false;
    line9.visible = false;

    number1 = game.add.sprite(numberPosition[0][0], numberPosition[0][1], 'game.number1');
    number2 = game.add.sprite(numberPosition[1][0], numberPosition[1][1], 'game.number2');
    number3 = game.add.sprite(numberPosition[2][0], numberPosition[2][1], 'game.number3');
    number4 = game.add.sprite(numberPosition[3][0], numberPosition[3][1], 'game.number4');
    number5 = game.add.sprite(numberPosition[4][0], numberPosition[4][1], 'game.number5');
    number6 = game.add.sprite(numberPosition[5][0], numberPosition[5][1], 'game.number6');
    number7 = game.add.sprite(numberPosition[6][0], numberPosition[6][1], 'game.number7');
    number8 = game.add.sprite(numberPosition[7][0], numberPosition[7][1], 'game.number8');
    number9 = game.add.sprite(numberPosition[8][0], numberPosition[8][1], 'game.number9');

    number1.visible = false;
    number2.visible = false;
    number3.visible = false;
    number4.visible = false;
    number5.visible = false;
    number6.visible = false;
    number7.visible = false;
    number8.visible = false;
    number9.visible = false;

}

// lineArray = [1,2,3,4, ...] - перечисляются линии которые нужно скрыть (1-9 - обычные линии, 11-19 прерывисные)
var linesArray = [];
function hideLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = false;
        linefull2.visible = false;
        linefull3.visible = false;
        linefull4.visible = false;
        linefull5.visible = false;
        linefull6.visible = false;
        linefull7.visible = false;
        linefull8.visible = false;
        linefull9.visible = false;
        line1.visible = false;
        line2.visible = false;
        line3.visible = false;
        line4.visible = false;
        line5.visible = false;
        line6.visible = false;
        line7.visible = false;
        line8.visible = false;
        line9.visible = false;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = false;
                    break;
                case 2:
                    linefull2.visible = false;
                    break;
                case 3:
                    linefull3.visible = false;
                    break;
                case 4:
                    linefull4.visible = false;
                    break;
                case 5:
                    linefull5.visible = false;
                    break;
                case 6:
                    linefull6.visible = false;
                    break;
                case 7:
                    linefull7.visible = false;
                    break;
                case 8:
                    linefull8.visible = false;
                    break;
                case 9:
                    linefull9.visible = false;
                    break;
                case 11:
                    line1.visible = false;
                    break;
                case 12:
                    line2.visible = false;
                    break;
                case 13:
                    line3.visible = false;
                    break;
                case 14:
                    line4.visible = false;
                    break;
                case 15:
                    line5.visible = false;
                    break;
                case 16:
                    line6.visible = false;
                    break;
                case 17:
                    line7.visible = false;
                    break;
                case 18:
                    line8.visible = false;
                    break;
                case 19:
                    line9.visible = false;
                    break;
            }
        });
    }
}

function showLines(linesArray) {
    if(linesArray.length == 0) {
        linefull1.visible = true;
        linefull2.visible = true;
        linefull3.visible = true;
        linefull4.visible = true;
        linefull5.visible = true;
        linefull6.visible = true;
        linefull7.visible = true;
        linefull8.visible = true;
        linefull9.visible = true;
        line1.visible = true;
        line2.visible = true;
        line3.visible = true;
        line4.visible = true;
        line5.visible = true;
        line6.visible = true;
        line7.visible = true;
        line8.visible = true;
        line9.visible = true;
    } else {
        linesArray.forEach(function (item) {
            switch (item) {
                case 1:
                    linefull1.visible = true;
                    break;
                case 2:
                    linefull2.visible = true;
                    break;
                case 3:
                    linefull3.visible = true;
                    break;
                case 4:
                    linefull4.visible = true;
                    break;
                case 5:
                    linefull5.visible = true;
                    break;
                case 6:
                    linefull6.visible = true;
                    break;
                case 7:
                    linefull7.visible = true;
                    break;
                case 8:
                    linefull8.visible = true;
                    break;
                case 9:
                    linefull9.visible = true;
                    break;
                case 11:
                    line1.visible = true;
                    break;
                case 12:
                    line2.visible = true;
                    break;
                case 13:
                    line3.visible = true;
                    break;
                case 14:
                    line4.visible = true;
                    break;
                case 15:
                    line5.visible = true;
                    break;
                case 16:
                    line6.visible = true;
                    break;
                case 17:
                    line7.visible = true;
                    break;
                case 18:
                    line8.visible = true;
                    break;
                case 19:
                    line9.visible = true;
                    break;
            }
        });
    }
}

// numberArray = [1, 2, ...] - массив объектов с изображениями цифр
var numberArray = [];
function hideNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = false;
        number2.visible = false;
        number3.visible = false;
        number4.visible = false;
        number5.visible = false;
        number6.visible = false;
        number7.visible = false;
        number8.visible = false;
        number9.visible = false;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.visible = false;
                    break;
                case 2:
                    number2.visible = false;
                    break;
                case 3:
                    number3.visible = false;
                    break;
                case 4:
                    number4.visible = false;
                    break;
                case 5:
                    number5.visible = false;
                    break;
                case 6:
                    number6.visible = false;
                    break;
                case 7:
                    number7.visible = false;
                    break;
                case 8:
                    number8.visible = false;
                    break;
                case 9:
                    number9.visible = false;
                    break;
            }
        });
    }
}

function showNumbers(numberArray) {
    if(numberArray.length == 0) {
        number1.visible = true;
        number2.visible = true;
        number3.visible = true;
        number4.visible = true;
        number5.visible = true;
        number6.visible = true;
        number7.visible = true;
        number8.visible = true;
        number9.visible = true;
    } else {
        numberArray.forEach(function (item) {
            switch (item) {
                case 1:
                    number1.visible = true;
                    break;
                case 2:
                    number2.visible = true;
                    break;
                case 3:
                    number3.visible = true;
                    break;
                case 4:
                    number4.visible = true;
                    break;
                case 5:
                    number5.visible = true;
                    break;
                case 6:
                    number6.visible = true;
                    break;
                case 7:
                    number7.visible = true;
                    break;
                case 8:
                    number8.visible = true;
                    break;
                case 9:
                    number9.visible = true;
                    break;
            }
        });
    }
}

var timerNumbersAmin;
function showNumbersAmin(numberArray) {
    hideNumbers(numberArray);

    var i = 1;
    timerNumbersAmin = setInterval(function() {
        if(i == 0) {
            hideNumbers(numberArray);

            i = 1;
        } else {
            i = 0;

            showNumbers(numberArray);
        }

    }, 500);
}

function hideNumbersAmin() {
    clearInterval(timerNumbersAmin);
    showNumbers(numberArray);
}



// Функции связанные с кнопками
var selectGame; var payTable; var betone; var betmax; var automaricstart; var startButton; var buttonLine1; var buttonLine3; var buttonLine5; var buttonLine7; var buttonLine9;

//кнопки для слотов
function addButtonsGame1(game) {

    //звуки для кнопок
    var line1Sound = game.add.audio('line1Sound');
    var line3Sound = game.add.audio('line3Sound');
    var line5Sound = game.add.audio('line5Sound');
    var line7Sound = game.add.audio('line7Sound');
    var line9Sound = game.add.audio('line9Sound');

    //var soundForBattons = []; - массив содержащий объекты звуков для кнопок
    var soundForBattons = [];
    soundForBattons.push(line1Sound, line3Sound, line5Sound, line7Sound, line9Sound);

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = true;
    selectGame.input.useHandCursor = true;
    selectGame.events.onInputOver.add(function(){
        selectGame.loadTexture('selectGame_p');
    });
    selectGame.events.onInputOut.add(function(){
        selectGame.loadTexture('selectGame');
    });
    selectGame.events.onInputDown.add(function(){});

    payTable = game.add.sprite(150,510, 'payTable');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = true;
    payTable.input.useHandCursor = true;
    payTable.events.onInputOver.add(function(){
        payTable.loadTexture('payTable_p');
    });
    payTable.events.onInputOut.add(function(){
        payTable.loadTexture('payTable');
    });


    betone = game.add.sprite(490,510, 'betone');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = true;
    betone.input.useHandCursor = true;
    betone.events.onInputOver.add(function(){
        betone.loadTexture('betone_p');
    });
    betone.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            upBetline(betlineOptions);
            updateBetinfo(game, scorePosions, lines, betline);
        }
    });
    betone.events.onInputOut.add(function(){
        betone.loadTexture('betone');
    });


    betmax = game.add.sprite(535,510, 'betmax');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = true;
    betmax.input.useHandCursor = true;
    betmax.events.onInputOver.add(function(){
        betmax.loadTexture('betmax_p');
    });
    betmax.events.onInputDown.add(function(){
        if(checkWin == 1){
            checkWin = 0;
            hideNumbersAmin();
            game.state.start('game2');
        } else {
            maxBetline();
            updateBetinfo(game, scorePosions, lines, betline);
            //betMaxSound.play();
        }
    });
    betmax.events.onInputOut.add(function(){
        betmax.loadTexture('betmax');
    });

    automaricstart = game.add.sprite(685,510, 'automaricstart');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = true;
    automaricstart.input.useHandCursor = true;
    automaricstart.events.onInputOver.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart_p');
        }
    });
    automaricstart.events.onInputOut.add(function(){
        if(automaricstart.inputEnabled == true){
            automaricstart.loadTexture('automaricstart');
        }
    });
    automaricstart.events.onInputDown.add(function(){
        //проверка есть ли выигрышь который нужно забрать и проверка включен ли авто-режим
        //главная проверка на то можно ли включить/выключить автостарт

        if(checkAutoStart == false) {

            checkAutoStart = true; // теперь автостарт нельзя отключить

            if(checkWin == 0) {
                if(autostart == false){
                    autostart = true;
                    hideLines([]);
                    requestSpin(gamename, betline, lines, bet, sid);
                } else {
                    autostart = false;
                }
            } else {
                autostart = true;
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {
            //если автостарт работает, то просто включем либо выключаем его как опцию без совершения каких либо других действий
            if(autostart == false){
                autostart = true;
            } else {
                autostart = false;
            }
        }
    });

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton_p');
        }
    });
    startButton.events.onInputOut.add(function(){
        if(startButton.inputEnabled == true) {
            startButton.loadTexture('startButton');
        }
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons();
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }

    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;
    buttonLine1.events.onInputOver.add(function(){
        buttonLine1.loadTexture('buttonLine1_p');
    });
    buttonLine1.events.onInputOut.add(function(){
        buttonLine1.loadTexture('buttonLine1');
    });
    buttonLine1.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1]);
    });
    buttonLine1.events.onInputDown.add(function(){
        soundForBattons[0].play();
        hideLines([]);
        linesArray = [11];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1];
        showNumbers(numberArray);

        lines = 1;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3]);
    });
    buttonLine3.events.onInputDown.add(function(){
        soundForBattons[1].play();

        hideLines([]);
        linesArray = [11, 12, 13];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3];
        showNumbers(numberArray);

        lines = 3;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5]);
    });
    buttonLine5.events.onInputDown.add(function(){
        soundForBattons[2].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5];
        showNumbers(numberArray);

        lines = 5;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7]);
    });
    buttonLine7.events.onInputDown.add(function(){
        soundForBattons[3].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7];
        showNumbers(numberArray);

        lines = 7;
        updateBetinfo(game, scorePosions, lines, betline);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputUp.add(function(){
        hideLines([]);
        showLines([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });
    buttonLine9.events.onInputDown.add(function(){
        soundForBattons[4].play();

        hideLines([]);
        linesArray = [11, 12, 13, 14, 15, 16, 17, 18, 19];
        showLines(linesArray);

        hideNumbers([]);
        numberArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        showNumbers(numberArray);

        lines = 9;
        updateBetinfo(game, scorePosions, lines, betline);
    });

}

//кнопки для карт
function addButtonsGame2(game) {

    var soundForBattons = [];

    // кнопки
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputOver.add(function(){
        startButton.loadTexture('startButton_p');
    });
    startButton.events.onInputOut.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;
    buttonLine3.events.onInputOver.add(function(){
        buttonLine3.loadTexture('buttonLine3_p');
    });
    buttonLine3.events.onInputOut.add(function(){
        buttonLine3.loadTexture('buttonLine3');
    });
    buttonLine3.events.onInputDown.add(function(){
        requestDouble(gamename, 1, lines, bet, sid);
    });

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;
    buttonLine5.events.onInputOver.add(function(){
        buttonLine5.loadTexture('buttonLine5_p');
    });
    buttonLine5.events.onInputOut.add(function(){
        buttonLine5.loadTexture('buttonLine5');
    });
    buttonLine5.events.onInputDown.add(function(){
        requestDouble(gamename, 2, lines, bet, sid);
    });

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;
    buttonLine7.events.onInputOver.add(function(){
        buttonLine7.loadTexture('buttonLine7_p');
    });
    buttonLine7.events.onInputOut.add(function(){
        buttonLine7.loadTexture('buttonLine7');
    });
    buttonLine7.events.onInputDown.add(function(){
        requestDouble(gamename, 3, lines, bet, sid);
    });

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;
    buttonLine9.events.onInputOver.add(function(){
        buttonLine9.loadTexture('buttonLine9_p');
    });
    buttonLine9.events.onInputOut.add(function(){
        buttonLine9.loadTexture('buttonLine9');
    });
    buttonLine9.events.onInputDown.add(function(){
        requestDouble(gamename, 4, lines, bet, sid);
    });

}

//кнопки и логика для игры с последовательным выбором
var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
function addButtonsGame3(game, ropesAnim, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm){
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = true;
    buttonLine1.input.useHandCursor = true;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = true;
    buttonLine5.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = true;
    buttonLine9.input.useHandCursor = true;

    buttonLine1.events.onInputOver.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1_p');
        }
    });
    buttonLine1.events.onInputOut.add(function(){
        if(buttonLine1.inputEnabled == true) {
            buttonLine1.loadTexture('buttonLine1');
        }
    });

    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine5.events.onInputOver.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5_p');
        }
    });
    buttonLine5.events.onInputOut.add(function(){
        if(buttonLine5.inputEnabled == true) {
            buttonLine5.loadTexture('buttonLine5');
        }
    });

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });

    buttonLine9.events.onInputOver.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9_p');
        }
    });
    buttonLine9.events.onInputOut.add(function(){
        if(buttonLine9.inputEnabled == true) {
            buttonLine9.loadTexture('buttonLine9');
        }
    });

    buttonLine1.events.onInputDown.add(function(){

        buttonLine1.loadTexture('buttonLine1_d');
        buttonLine1.inputEnabled = false;
        buttonLine1.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) {
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[0](ropeValues, ropeStep, checkHelm);

        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[0][0],winRopeNumberPosition[0][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }


        ropesAnim[1](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[1][0],winRopeNumberPosition[1][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine5.events.onInputDown.add(function(){

        buttonLine5.loadTexture('buttonLine5_d');
        buttonLine5.inputEnabled = false;
        buttonLine5.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[2](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[2][0],winRopeNumberPosition[2][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[3](ropeValues, ropeStep);
        setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[3][0],winRopeNumberPosition[3][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
    buttonLine9.events.onInputDown.add(function(){

        buttonLine9.loadTexture('buttonLine9_d');
        buttonLine9.inputEnabled = false;
        buttonLine9.input.useHandCursor = false;

        if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
            stepTotalWinR += betline*lines*ropeValues[ropeStep];
        }

        ropesAnim[4](ropeValues, ropeStep);
        setTimeout(showWinGame3, timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[4][0],winRopeNumberPosition[4][1] , ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

        ropeStep += 1;
    });
}

// для выбора одного из двух (действия при нажатии некоторых кнопок нужно задать в самой игре)
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
function addButtonsGame4(game, selectionsAmin, timeout1Game4ToGame1, timeout2Game4ToGame1) {
    selectGame = game.add.sprite(70,510, 'selectGame_d');
    selectGame.scale.setTo(0.7, 0.7);
    selectGame.inputEnabled = false;

    payTable = game.add.sprite(150,510, 'payTable_d');
    payTable.scale.setTo(0.7, 0.7);
    payTable.inputEnabled = false;

    betone = game.add.sprite(490,510, 'betone_d');
    betone.scale.setTo(0.7, 0.7);
    betone.inputEnabled = false;


    betmax = game.add.sprite(535,510, 'betmax_d');
    betmax.scale.setTo(0.7, 0.7);
    betmax.inputEnabled = false;

    automaricstart = game.add.sprite(685,510, 'automaricstart_d');
    automaricstart.scale.setTo(0.7, 0.7);
    automaricstart.inputEnabled = false;

    startButton = game.add.sprite(597, 510, 'startButton_d');
    startButton.scale.setTo(0.7,0.7);
    startButton.inputEnabled = false;

    buttonLine1 = game.add.sprite(260, 510, 'buttonLine1_d');
    buttonLine1.scale.setTo(0.7,0.7);
    buttonLine1.inputEnabled = false;

    buttonLine5 = game.add.sprite(340, 510, 'buttonLine5_d');
    buttonLine5.scale.setTo(0.7,0.7);
    buttonLine5.inputEnabled = false;

    buttonLine9 = game.add.sprite(420, 510, 'buttonLine9_d');
    buttonLine9.scale.setTo(0.7,0.7);
    buttonLine9.inputEnabled = false;

    buttonLine3 = game.add.sprite(300, 510, 'buttonLine3');
    buttonLine3.scale.setTo(0.7,0.7);
    buttonLine3.inputEnabled = true;
    buttonLine3.input.useHandCursor = true;

    buttonLine7 = game.add.sprite(380, 510, 'buttonLine7');
    buttonLine7.scale.setTo(0.7,0.7);
    buttonLine7.inputEnabled = true;
    buttonLine7.input.useHandCursor = true;

    buttonLine7.events.onInputOver.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7_p');
        }
    });
    buttonLine7.events.onInputOut.add(function(){
        if(buttonLine7.inputEnabled == true) {
            buttonLine7.loadTexture('buttonLine7');
        }
    });
    buttonLine3.events.onInputOver.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3_p');
        }
    });
    buttonLine3.events.onInputOut.add(function(){
        if(buttonLine3.inputEnabled == true) {
            buttonLine3.loadTexture('buttonLine3');
        }
    });

    buttonLine3.events.onInputDown.add(function(){

        buttonLine3.loadTexture('buttonLine3_d');
        buttonLine3.inputEnabled = false;
        buttonLine3.input.useHandCursor = false;

        selectionsAmin[0](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }

    });
    buttonLine7.events.onInputDown.add(function(){

        buttonLine7.loadTexture('buttonLine7_d');
        buttonLine7.inputEnabled = false;
        buttonLine7.input.useHandCursor = false;

        selectionsAmin[1](ropeValues, ropeStep);

        if(ropeValues[5] != 0){
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout1Game4ToGame1);
        } else {
            lockDisplay();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine7, 'buttonLine7']]);
            updateBalanceGame4(game, scorePosions, balanceR);
            setTimeout('unlockDisplay(); game.state.start("game1");',timeout2Game4ToGame1);
        }
    });
}

//var buttonsArray = [[selectGame,'selectGame'], [... , ...]]; - в массиве перечисляется название кнопок
var buttonsArray = [];
function hideButtons(buttonsArray) {
    if(!isMobile){
        if(buttonsArray.length == 0) {
            if(autostart == false){
                automaricstart.loadTexture('automaricstart_d');
                automaricstart.inputEnabled = false;
                automaricstart.input.useHandCursor = false;
            }

            selectGame.loadTexture('selectGame_d');
            selectGame.inputEnabled = false;
            selectGame.input.useHandCursor = false;

            payTable.loadTexture('payTable_d');
            payTable.inputEnabled = false;
            payTable.input.useHandCursor = false;

            betone.loadTexture('betone_d');
            betone.inputEnabled = false;
            betone.input.useHandCursor = false;

            betmax.loadTexture('betmax_d');
            betmax.inputEnabled = false;
            betmax.input.useHandCursor = false;

            startButton.loadTexture('startButton_d');
            startButton.inputEnabled = false;
            startButton.input.useHandCursor = false;

            buttonLine1.loadTexture('buttonLine1_d');
            buttonLine1.inputEnabled = false;
            buttonLine1.input.useHandCursor = false;

            buttonLine3.loadTexture('buttonLine3_d');
            buttonLine3.inputEnabled = false;
            buttonLine3.input.useHandCursor = false;

            buttonLine5.loadTexture('buttonLine5_d');
            buttonLine5.inputEnabled = false;
            buttonLine5.input.useHandCursor = false;

            buttonLine7.loadTexture('buttonLine7_d');
            buttonLine7.inputEnabled = false;
            buttonLine7.input.useHandCursor = false;

            buttonLine9.loadTexture('buttonLine9_d');
            buttonLine9.inputEnabled = false;
            buttonLine9.input.useHandCursor = false;

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]+'_d');
                item[0].inputEnabled = false;
                item[0].input.useHandCursor = false;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = false;
            double.visible = false;
            bet1.visible = false;
            dollar.visible = false;
            gear.visible = false;
            home.visible = false;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = false;
            })
        }
    }
}

function showButtons(buttonsArray) {
    if(!isMobile) {
        if(buttonsArray.length == 0) {

            if(autostart == false){
                automaricstart.loadTexture('automaricstart');
                automaricstart.inputEnabled = true;
                automaricstart.input.useHandCursor = true;

                selectGame.loadTexture('selectGame');
                selectGame.inputEnabled = true;
                selectGame.input.useHandCursor = true;

                payTable.loadTexture('payTable');
                payTable.inputEnabled = true;
                payTable.input.useHandCursor = true;

                betone.loadTexture('betone');
                betone.inputEnabled = true;
                betone.input.useHandCursor = true;

                betmax.loadTexture('betmax');
                betmax.inputEnabled = true;
                betmax.input.useHandCursor = true;

                startButton.loadTexture('startButton');
                startButton.inputEnabled = true;
                startButton.input.useHandCursor = true;

                buttonLine1.loadTexture('buttonLine1');
                buttonLine1.inputEnabled = true;
                buttonLine1.input.useHandCursor = true;

                buttonLine3.loadTexture('buttonLine3');
                buttonLine3.inputEnabled = true;
                buttonLine3.input.useHandCursor = true;

                buttonLine5.loadTexture('buttonLine5');
                buttonLine5.inputEnabled = true;
                buttonLine5.input.useHandCursor = true;

                buttonLine7.loadTexture('buttonLine7');
                buttonLine7.inputEnabled = true;
                buttonLine7.input.useHandCursor = true;

                buttonLine9.loadTexture('buttonLine9');
                buttonLine9.inputEnabled = true;
                buttonLine9.input.useHandCursor = true;
            }

        } else {
            buttonsArray.forEach(function (item) {
                item[0].loadTexture(item[1]);
                item[0].inputEnabled = true;
                item[0].input.useHandCursor = true;
            })
        }
    } else {
        if(buttonsArray.length == 0) {
            startButton.visible = true;
            double.visible = true;
            bet1.visible = true;
            dollar.visible = true;
            gear.visible = true;
            home.visible = true;
        } else {
            buttonsArray.forEach(function (item) {
                item[0].visible = true;
            })
        }
    }
}


//слоты и их вращение

//переменные содержащие все объекты слотов
var slot1; var slot2; var slot3;  var slot4; var slot5; var slot6; var slot7; var slot8; var slot9; var slot10; var slot11; var slot12; var slot13; var slot14; var slot15;
var slot1Anim; var slot2Anim; var slot3Anim;  var slot4Anim; var slot5Anim; var slot6Anim; var slot7Anim; var slot8Anim; var slot9Anim; var slot10Anim; var slot11Anim; var slot12Anim; var slot13Anim; var slot14Anim; var slot15Anim;

function addSlots(game, slotPosition) {
    //slotValueNames - названия изображений слотов; slotCellAnimName - название спрайта анимации кручения
    var slotValueNames = ['cell0', 'cell1', 'cell2', 'cell3', 'cell4', 'cell5', 'cell6', 'cell7', 'cell8'];
    var slotCellAnimName = 'cellAnim';

    slot1 = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotValueNames[0]);
    slot2 = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotValueNames[1]);
    slot3 = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotValueNames[3]);
    slot4 = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotValueNames[4]);
    slot5 = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotValueNames[7]);
    slot6 = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotValueNames[2]);
    slot7 = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotValueNames[4]);
    slot8 = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotValueNames[3]);
    slot9 = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotValueNames[8]);
    slot10 = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotValueNames[2]);
    slot11 = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotValueNames[0]);
    slot12 = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotValueNames[1]);
    slot13 = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotValueNames[5]);
    slot14 = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotValueNames[3]);
    slot15 = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotValueNames[5]);

    slot1Anim = game.add.sprite(slotPosition[0][0],slotPosition[0][1], slotCellAnimName);
    slot2Anim = game.add.sprite(slotPosition[1][0],slotPosition[1][1], slotCellAnimName);
    slot3Anim = game.add.sprite(slotPosition[2][0],slotPosition[2][1], slotCellAnimName);
    slot4Anim = game.add.sprite(slotPosition[3][0],slotPosition[3][1], slotCellAnimName);
    slot5Anim = game.add.sprite(slotPosition[4][0],slotPosition[4][1], slotCellAnimName);
    slot6Anim = game.add.sprite(slotPosition[5][0],slotPosition[5][1], slotCellAnimName);
    slot7Anim = game.add.sprite(slotPosition[6][0],slotPosition[6][1], slotCellAnimName);
    slot8Anim = game.add.sprite(slotPosition[7][0],slotPosition[7][1], slotCellAnimName);
    slot9Anim = game.add.sprite(slotPosition[8][0],slotPosition[8][1], slotCellAnimName);
    slot10Anim = game.add.sprite(slotPosition[9][0],slotPosition[0][1], slotCellAnimName);
    slot11Anim = game.add.sprite(slotPosition[10][0],slotPosition[10][1], slotCellAnimName);
    slot12Anim = game.add.sprite(slotPosition[11][0],slotPosition[11][1], slotCellAnimName);
    slot13Anim = game.add.sprite(slotPosition[12][0],slotPosition[12][1], slotCellAnimName);
    slot14Anim = game.add.sprite(slotPosition[13][0],slotPosition[13][1], slotCellAnimName);
    slot15Anim = game.add.sprite(slotPosition[14][0],slotPosition[14][1], slotCellAnimName);

    slot1Anim.animations.add('slot1Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot2Anim.animations.add('slot2Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot3Anim.animations.add('slot3Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot4Anim.animations.add('slot4Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot5Anim.animations.add('slot5Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot6Anim.animations.add('slot6Anim', [5,6,7,8,0,1,2,3,4], 20, true);
    slot7Anim.animations.add('slot7Anim', [6,7,8,0,1,2,3,4,5], 20, true);
    slot8Anim.animations.add('slot8Anim', [7,8,0,1,2,3,4,5,6], 20, true);
    slot9Anim.animations.add('slot9Anim', [8,0,1,2,3,4,5,6,7], 20, true);
    slot10Anim.animations.add('slot10Anim', [0,1,2,3,4,5,6,7,8], 20, true);
    slot11Anim.animations.add('slot11Anim', [1,2,3,4,5,6,7,8,0], 20, true);
    slot12Anim.animations.add('slot12Anim', [2,3,4,5,6,7,8,0,1], 20, true);
    slot13Anim.animations.add('slot13Anim', [3,4,5,6,7,8,0,1,2], 20, true);
    slot14Anim.animations.add('slot14Anim', [4,5,6,7,8,0,1,2,3], 20, true);
    slot15Anim.animations.add('slot15Anim', [7,8,0,1,2,3,4,5,6], 20, true);

    slot1Anim.animations.getAnimation('slot1Anim').play();
    slot2Anim.animations.getAnimation('slot2Anim').play();
    slot3Anim.animations.getAnimation('slot3Anim').play();
    slot4Anim.animations.getAnimation('slot4Anim').play();
    slot5Anim.animations.getAnimation('slot5Anim').play();
    slot6Anim.animations.getAnimation('slot6Anim').play();
    slot7Anim.animations.getAnimation('slot7Anim').play();
    slot8Anim.animations.getAnimation('slot8Anim').play();
    slot9Anim.animations.getAnimation('slot9Anim').play();
    slot10Anim.animations.getAnimation('slot10Anim').play();
    slot11Anim.animations.getAnimation('slot11Anim').play();
    slot12Anim.animations.getAnimation('slot12Anim').play();
    slot13Anim.animations.getAnimation('slot13Anim').play();
    slot14Anim.animations.getAnimation('slot14Anim').play();
    slot15Anim.animations.getAnimation('slot15Anim').play();

    slot1Anim.visible = false;
    slot2Anim.visible = false;
    slot3Anim.visible = false;
    slot4Anim.visible = false;
    slot5Anim.visible = false;
    slot6Anim.visible = false;
    slot7Anim.visible = false;
    slot8Anim.visible = false;
    slot9Anim.visible = false;
    slot10Anim.visible = false;
    slot11Anim.visible = false;
    slot12Anim.visible = false;
    slot13Anim.visible = false;
    slot14Anim.visible = false;
    slot15Anim.visible = false;
}

var finalValues; var wlValues; var balanceR; var totalWin; var totalWinR; var dcard; var linesR; var betlineR; //totalWin - общая сумма выигрыша посчитанная из разноности балансов до и полсе запроса. totalWinR - полученный из ответа (аналогично linesR и betlineR)
var checkRopeGame = 0; var checkRopeGameAnim = 0; var ropeValues; var ropeStep = 0;
var monkeyCell = []; // массив содержащий номера ячеек, в которых выпали обезьяны
var autostart = false; var checkAutoStart = false; var checkUpdateBalance = false;
function parseSpinAnswer(dataSpinRequest) {
    dataArray = dataSpinRequest.split('&');

    if (find(dataArray, 'result=ok') !== -1 && find(dataArray, 'state=0') !== -1) {
        dataArray.forEach(function (item) {
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                finalValues = cellValuesString.split('|');

                //получаем ячейки в которых содержатся обезьяны
                monkeyCell = [];
                finalValues.forEach(function (item, i) {
                    if(item == 8) {
                        monkeyCell.push(i);
                    }
                });
            }
            if (item.indexOf('wl=') + 1) {
                var wlString = item.replace('wl=|', '');
                wlValues = wlString.split('|');
            }
            if (item.indexOf('min=') + 1) {
                min = item.replace('min=', '');
            }
            if (item.indexOf('balance=') + 1) {
                totalWinR = finalValues[15];

                balanceOld = balance; // сохраняем реальный баланс для слеудующей итерации
                balanceR = item.replace('balance=', '').replace('.00','');
                balance = parseInt(balanceR) + parseInt(finalValues[15]); // получаем реальный баланс
            }
            if (item.indexOf('rope=') + 1) {
                var ropeStr = item.replace('rope=|', '');
                ropeValues = ropeStr.split('|');

                //проверка на случай если "rope=|" - пусто
                if(ropeValues.length > 5){
                    checkRopeGame = 1;
                }
            }
            if (item.indexOf('dcard=') + 1) {
                dcard = item.replace('dcard=', '');
            }
            if (item.indexOf('extralife=') + 1) {
                extralife = item.replace('extralife=', '');
            }
        });

        if(typeof finalValues != "undefined") {
            totalWinR = finalValues[15];
            linesR = finalValues[16];
            betlineR = finalValues[17];

            checkSpinResult(totalWinR);

            slotRotation(game, finalValues);
        }




    }
}

var timer;
function showSpinResult(checkWin, checkRopeGame, wlValues) {
    if(checkRopeGame == 1) {

        hideButtons([]);

        checkRopeGameAnim = 1;

        var winMonkey = game.add.audio('game.winPech');
        winMonkey.play();

        showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell);

        setTimeout("checkRopeGame = 0; checkRopeGameAnim = 0; game.state.start('game3');", 7000);
    } else if(checkWin == 1) {

        topBarImage.loadTexture('topScoreGame2'); //заменяем в topBar line play на win

        hideTableTitle();

        var soundWinLines = []; // массив для объектов звуков
        soundWinLines[0] = game.add.audio('soundWinLine1');
        soundWinLines[1] = game.add.audio('soundWinLine2');
        soundWinLines[2] = game.add.audio('soundWinLine3');
        soundWinLines[3] = game.add.audio('soundWinLine4');
        soundWinLines[4] = game.add.audio('soundWinLine5');
        soundWinLines[5] = game.add.audio('soundWinLine6');
        soundWinLines[6] = game.add.audio('soundWinLine7');
        soundWinLines[7] = game.add.audio('soundWinLine8');

        var soundWinLinesCounter = 0; // счетчик для звуков озвучивающих выигрышные линии

        var wlWinValuesArray = [];

        wlValues.forEach(function (line, i) {
            if(line > 0) {
                wlWinValuesArray.push(i+1);
            }
        });

        stepTotalWinR = 0; // число в которое сумируются значения из wl (из выигрышных линий)
        var currentIndex = -1;

        timer = setInterval(function() {
            if(++currentIndex > (wlWinValuesArray.length - 1)) {
                if(!isMobile) {
                    showButtons([[startButton, 'startButton'], [betmax, 'betmax'], [betone, 'betone'], [payTable, 'payTable']]);
                } else {
                    showButtons([[startButton], [home], [gear], [dollar], [double]]);
                }

                hideNumberWinLine();

                showNumbersAmin(wlWinValuesArray);
                changeTableTitle('takeOrRisk1');

                clearInterval(timer);

                if(autostart == true){
                    takePrize(game, scorePosions, balanceOld, balance);
                } else {
                    checkAutoStart = false;
                }
            } else {
                stepTotalWinR += parseInt(wlValues[wlWinValuesArray[currentIndex] - 1]);
                showStepTotalWinR(game, scorePosions, parseInt(stepTotalWinR));
                //TODO: не получилось обстрагировать координаты для текста выводящего номера выигрышных линий
                if(isMobile) {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 575-mobileX, 444-mobileY);
                } else {
                    showNumberWinLine(game, wlWinValuesArray[currentIndex], 575, 444);
                }
                soundWinLines[soundWinLinesCounter].play();
                soundWinLinesCounter += 1;
                showLines([wlWinValuesArray[currentIndex]]);
            }
        }, 500);

    } else {
        if(autostart == true){
            updateBalance(game, scorePosions, balanceOld, balance);
            hideLines([]);
            requestSpin(gamename, betline, lines, bet, sid);
        } else {
            changeTableTitle('play1To');
            updateBalance(game, scorePosions, balanceOld, balance);
            if(isMobile){
                showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            } else {
                showButtons();
            }
            checkAutoStart = false;
        }
    }

}


//запрос для слотов
var balanceOld;
var dataSpinRequest; // данные полученны из запроса
function requestSpin(gamename, betline, lines, bet, sid) {
    hideButtons([]);

    //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=20&extralife=45&jackpots=1822.16|4122.16|6122.16';
    data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|1|2|3|4|5|0&extralife=45&jackpots=1822.16|4122.16|6122.16';
    //alert(data);
    dataSpinRequest = data;

    parseSpinAnswer(dataSpinRequest);

    /*$.ajax({
        type: "POST",
        url: 'http://api.gmloto.ru/index.php?action=spin&min=1&game='+gamename+'&betline='+betline+'&lines='+lines+'&bet='+ bet +'&SID='+sid,
        dataType: 'html',
        success: function (data) {
            //data = 'result=ok&state=0&info=|8|6|2|2|1|6|4|2|1|8|7|6|3|7|8|20|9|4&wl=|20|0|0|0|0|0|0|0|0&balance=77750.00&dcard=&rope=|2|2|2|2|1|1&extralife=45&jackpots=1822.16|4122.16|6122.16';
            //alert(data);
            dataSpinRequest = data;

            parseSpinAnswer(dataSpinRequest);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });*/
}

//анимации связаные с выпадением бонусной игры с последовательным выбором
var manyCellAnim = [];
var slotPosition;
function addSelectionOfTheManyCellAnim(game, slotPosition) {
    for (var i = 0; i < 15; i++){
        manyCellAnim[i] = game.add.sprite(slotPosition[i][0],slotPosition[i][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[i].animations.add('selectionOfTheManyCellAnim', [0,1,2,3,4,5,6,7,8,9], 8, true);
    }
}

function showSelectionOfTheManyCellAnim(game, slotPosition, monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item] = game.add.sprite(slotPosition[item][0],slotPosition[item][1], 'selectionOfTheManyCellAnim');
        manyCellAnim[item].animations.add('selectionOfTheManyCellAnim', [0,1,2], 8, true);
        manyCellAnim[item].animations.getAnimation('selectionOfTheManyCellAnim').play();
    });
}

function hideSelectionOfTheManyCellAnim(monkeyCell) {
    monkeyCell.forEach(function (item) {
        manyCellAnim[item].visible = false;
    });
}

var checkRotaion = false;
function slotRotation(game, finalValues) {
    checkRotaion = true;

    changeTableTitle('bonusGame');

    var rotateSound = game.add.audio('rotateSound');
    rotateSound.loop = true;
    var stopSound = game.add.audio('stopSound');

    balanceScore.visible = false;
    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balanceR, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    rotateSound.play();

    slot1Anim.visible = true;
    slot2Anim.visible = true;
    slot3Anim.visible = true;
    slot4Anim.visible = true;
    slot5Anim.visible = true;
    slot6Anim.visible = true;
    slot7Anim.visible = true;
    slot8Anim.visible = true;
    slot9Anim.visible = true;
    slot10Anim.visible = true;
    slot11Anim.visible = true;
    slot12Anim.visible = true;
    slot13Anim.visible = true;
    slot14Anim.visible = true;
    slot15Anim.visible = true;

    setTimeout(function() {
        stopSound.play();

        slot1Anim.visible = false;
        slot2Anim.visible = false;
        slot3Anim.visible = false;

        slot1.loadTexture('cell'+finalValues[0]);
        slot2.loadTexture('cell'+finalValues[1]);
        slot3.loadTexture('cell'+finalValues[2]);
    }, 1000);

    setTimeout(function() {
        stopSound.play();

        slot4Anim.visible = false;
        slot5Anim.visible = false;
        slot6Anim.visible = false;

        slot4.loadTexture('cell'+finalValues[3]);
        slot5.loadTexture('cell'+finalValues[4]);
        slot6.loadTexture('cell'+finalValues[5]);
    }, 1200);

    setTimeout(function() {
        stopSound.play();

        slot7Anim.visible = false;
        slot8Anim.visible = false;
        slot9Anim.visible = false;

        slot7.loadTexture('cell'+finalValues[6]);
        slot8.loadTexture('cell'+finalValues[7]);
        slot9.loadTexture('cell'+finalValues[8]);
    }, 1400);

    setTimeout(function() {
        stopSound.play();

        slot10Anim.visible = false;
        slot11Anim.visible = false;
        slot12Anim.visible = false;

        slot10.loadTexture('cell'+finalValues[9]);
        slot11.loadTexture('cell'+finalValues[10]);
        slot12.loadTexture('cell'+finalValues[11]);
    }, 1600);

    setTimeout(function() {
        stopSound.play();

        slot13Anim.visible = false;
        slot14Anim.visible = false;
        slot15Anim.visible = false;

        slot13.loadTexture('cell'+finalValues[12]);
        slot14.loadTexture('cell'+finalValues[13]);
        slot15.loadTexture('cell'+finalValues[14]);
    }, 1800);

    // итоговые действия
    setTimeout(function() {
        rotateSound.stop();
        checkRotaion = false;
        showSpinResult(checkWin, checkRopeGame, wlValues);
    }, 1800);
}

var checkWin = 0;
function checkSpinResult(totalWinR) {
    if (totalWinR > 0) {
        checkWin = 1;
    } else {
        checkWin = 0;
    }
}

function takePrize(game, scorePosions, balanceOld, balance) {
    changeTableTitle('take');
    hideButtons([]);

    hideNumbersAmin();
    hideLines([]);

    updateBalance(game, scorePosions, balanceOld, balance);
    updateTotalWinR(game, scorePosions, totalWinR);
}

//вывод информации в табло
var tableTitle; // название изображения заданное в прелодере
function addTableTitle(game, loadTexture, x,y) {
    tableTitle = game.add.sprite(x,y, loadTexture);
}

function changeTableTitle(loadTexture) {
    tableTitle.visible = true;
    tableTitle.loadTexture(loadTexture);
}

function hideTableTitle() {
    tableTitle.visible = false;
}

var winLineText;
function showNumberWinLine(game, winLine, x,y) {
    if(typeof(winLineText) != "undefined") {
        winLineText.visible = false;
    }

    winLineText = game.add.text(x, y, 'win line: '+winLine, {
        font: '20px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideNumberWinLine() {
    winLineText.visible = false;
}


function getNeedUrlPath() {
    if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
        var number = location.pathname.indexOf('/games/');
        var needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname.substring(0,number) + '/';

        return needLocation;
    } else if (location.href.indexOf('public') !== -1 && location.href.indexOf('/game/') !== -1) {
        var number = location.pathname.indexOf('/public/');
        var needLocation = location.href.substring(0,location.href.indexOf('public')) + 'public';

        return needLocation;
    } else if (location.href.indexOf('public') === -1) {
        needLocation = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname;

        return needLocation;
    }
}

// ajax-запросы

//init-запрос
function requestInit() {
    $.ajax({
        type: "GET",
        url: getNeedUrlPath() + '/init',
        url: 'test.php',
        dataType: 'html',
        success: function (data) {

            dataString = data;

            initDataArray = dataString.split('&');

            initDataArray.forEach(function (item) {
                if(item.indexOf('SID=') + 1) {
                    sid = item.replace('SID=','');
                }
                if(item.indexOf('user=') + 1) {
                    user = item.replace('user=','');
                }
            });

            if (data.length != 0 && (find(initDataArray, 'result=ok')) != -1 && (find(initDataArray, 'state=0')) != -1) {
                requestState();
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

//state-запрос
var jackpots;
function requestState() {
    $.ajax({
        type: "GET",
        //getNeedUrlPath()
        //url: 'http://api.gmloto.ru/index.php?action=state&min=1&game='+gamename+'&SID='+sid,
        url: 'test.php',
        dataType: 'html',
        success: function (data) {

            var dataString = data;

            startDataArray = dataString.split('&');

            if (data.length !== 0 && (find(startDataArray, ' result=ok')) != -1 && (find(startDataArray, 'state=0')) != -1) {

                startDataArray = dataString.split('&');

                startDataArray.forEach(function (item) {
                    if(item.indexOf('balance=') + 1) {
                        balance = item.replace('balance=', '').replace('.00','');
                    }
                    if(item.indexOf('extralife=') + 1) {
                        extralife = item.replace('extralife=','');
                    }
                    if(item.indexOf('jackpots=') + 1) {
                        var jackpotsString = item.replace('jackpots=','');
                        jackpots =  jackpotsString.split('|');
                    }
                    if(item.indexOf('id=') + 1) {
                        id = item.replace('id=','');
                    }
                    if(item.indexOf('min=') + 1) {
                        min = item.replace('min=','');
                    }

                    /*game1();
                     game2();
                     game3();
                     game4();*/
                });
            } else {
                var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
                alert(errorText);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}


//функции отображения цифр

var betScore;
var linesScore;
var balanceScore;
var betline1Score;
var betline2Score;
var riskStep;

var checkGame = 0; // индикатор текущего экрана (текущей игры). Нужен для корректной обработки и вывода некоторых данных

//var scorePosions = [[x,y, px], [x,y, px] ...]; - массив, в котором в порядке определенном выше идут координаты цифр
// для игры с картами betline содержит номер попытки
var scorePosions;
function addScore(game, scorePosions, bet, lines, balance, betline) {
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
        font: scorePosions[2][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    if(checkGame == 1){
        betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
            font: scorePosions[4][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }

    if(checkGame == 2){
        riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
            font: scorePosions[3][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }
}

var takeWin;
var textCounter;
var topBarImage;
var ActionsAfterUpdatingBalance; //задаем таймер, который останавливаем в случае если игрок решил не дожидаться окончания анимации
function updateBalance(game, scorePosions, balanceR, balance) {
    if(checkWin == 0){
        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], balance, {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

    } else {

        if(autostart == false) {
            checkUpdateBalance = true;
            showButtons([[startButton, 'startButton']]);
        }

        balanceScore.visible = false;

        takeWin = game.add.audio('takeWin');
        //takeWin.addMarker('take', 0, 0.6);
        takeWin.loop = true;
        takeWin.play();

        if(totalWinR > 100){
            var interval = 5;
        } else {
            var interval = 50;
        }

        var timeInterval = parseInt(interval)*parseInt(totalWinR);

        var currentBalanceDifference = 0;

        textCounter = setInterval(function () {

            currentBalanceDifference += 1;

            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(+balanceR-betline*lines) + parseInt(currentBalanceDifference), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });
        }, interval);

        ActionsAfterUpdatingBalance = setTimeout(function() {
            if(checkRotaion == false) { //проверка крутятся ли слоты, чтобы не появялись в случае быстрых нажатий кнопки страт все кнопки в неположенное время
                if(isMobile) {
                    showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
                } else {
                    showButtons();
                }

                takeWin.stop();
                changeTableTitle('play1To');
                clearInterval(textCounter);

                topBarImage.loadTexture('game.insideBackground'); //убираем win из topBar

                if(autostart == true) {
                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    setTimeout(requestSpin, 1000, gamename, betline, lines, bet, sid);
                } else {
                    checkUpdateBalance = false;

                    //для исправления ошибки в единицу, которая появляется рандомно
                    balanceScore.visible = false;
                    balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                        font: scorePosions[2][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });

                    hideLines([]);
                    checkAutoStart = false;
                }
            }

        }, timeInterval);

        checkWin = 0;
    }
}

var totalWinRCounter;
function updateTotalWinR(game, scorePosions, totalWinR) {

    //обновление totalWin в cлотах при забирании выигрыша
    if(totalWinR > 100){
        var interval = 5;
    } else {
        var interval = 50;
    }

    var difference = parseInt(totalWinR);

    //значение totalWinR уменьшается
    var timeInterval = parseInt(interval*difference);
    var mark = -1;

    var currentDifference = 0;

    totalWinRCounter = setInterval(function () {

        currentDifference += 1*mark;

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(totalWinR) + parseInt(currentDifference), {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    setTimeout(function() {
        hideStepTotalWinR(game, scorePosions, lines);
        clearInterval(totalWinRCounter);
    }, timeInterval);
}

var stepTotalWinR = 0;
function showStepTotalWinR(game, scorePosions, stepTotalWinR) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function hideStepTotalWinR(game, scorePosions, lines) {
    linesScore.visible = false;
    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

function updateBetinfo(game, scorePosions, lines, betline) {
    betScore.visible = false;
    linesScore.visible = false;
    betline1Score.visible = false;
    betline2Score.visible = false;

    bet = lines*betline;
    betScore = game.add.text(scorePosions[0][0], scorePosions[0][1], bet, {
        font: scorePosions[0][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline1Score = game.add.text(scorePosions[3][0], scorePosions[3][1], betline, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    betline2Score = game.add.text(scorePosions[4][0], scorePosions[4][1], betline, {
        font: scorePosions[4][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}



// функции пересчета цифр

//пересчет ставки на линию
var betlineOptions = [1, 2, 3, 4, 5, 10, 15, 20, 25];
var betlineCounter = 0;
function upBetline() {
    if(betlineCounter < (betlineOptions.length-1)) {
        betlineCounter += 1;
        betline = betlineOptions[betlineCounter];
    } else {
        betlineCounter = 0;
        betline = betlineOptions[betlineCounter];
    }
}

function maxBetline() {
    betlineCounter = betlineOptions.length - 1;
    betline = betlineOptions[betlineOptions.length - 1];
}




//функции для игры с картами

var dataDoubleRequest; var selectedCard;
function requestDouble(gamename, selectedCard, lines, bet, sid) {
    hideButtons([[startButton, 'startButton']]);
    disableInputCards();
    lockDisplay();
    $.ajax({
        type: "POST",
        url: getNeedUrlPath() + '/double/'+gamename+'?sessionName='+sessionName+'&selectedCard='+selectedCard,
        dataType: 'html',
        success: function (data) {

            dataDoubleRequest = data.split('&');
            parseDoubleAnswer(dataDoubleRequest);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorText = '//ошибка error|Ошибка! Ваш баланс ($user_balance) недостаточен для игры. Пополните счёт. | index.php?GE=enter';
            alert(errorText);
        }
    });
}

var dwin; var dcard2; var selectedCardR;
function parseDoubleAnswer(dataDoubleRequest) {
    if (find(dataDoubleRequest, 'result=ok') != -1 && find(dataDoubleRequest, 'state=0') != -1) {

        dataDoubleRequest.forEach(function (item) {
            if (item.indexOf('dwin=') + 1) {
                dwin = item.replace('dwin=', '');
                totalWin = dwin; // изменяем для последующего использования dwin из ответа для вывода dwin
            }
            if (item.indexOf('balance=') + 1) {
                balance = item.replace('balance=', '').replace('.00','');
            }
            if (item.indexOf('dcard2=') + 1) {
                dcard2 = item.replace('dcard2=', '');
                dcard = dcard2;
            }
            if (item.indexOf('info=') + 1) {
                var cellValuesString = item.replace('info=|', '');
                valuesOfAllCards = cellValuesString.split('|');
            }
            if (item.indexOf('select=') + 1) {
                selectedCardR = item.replace('select=', '');
            }
            if(item.indexOf('jackpots=') + 1) {
                var jackpotsString = item.replace('jackpots=','');
                jackpots =  jackpotsString.split('|');
            }
        });

        showDoubleResult(dwin, selectedCardR, valuesOfAllCards);

    }
}

var step = 1;
function showDoubleResult(dwin, selectedCardR, valuesOfAllCards) {
    if (!isMobile) {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout('hideAllCards(cardArray); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step)', 2000);
            setTimeout('openDCard(dcard); showButtons([[buttonLine3, "buttonLine3"], [buttonLine5, "buttonLine5"], [buttonLine7, "buttonLine7"], [buttonLine9, "buttonLine9"], [startButton, "startButton"]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideButtons([[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], [startButton, 'startButton']]);
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            setTimeout('openAllCards(valuesOfAllCards)', 1000);
            setTimeout("tableTitle.visible = false; game.state.start('game1');", 2000);
        }
    } else {
        if(dwin > 0) {
            hideDoubleToAndTakeOrRiskTexts();
            tableTitle.visible = true;
            changeTableTitle('winTitleGame2');
            winCard.play();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); lockDisplay(); disableInputCards();', 500);
            setTimeout('hideAllCards(cardArray); openDCard(dcard); disableInputCards(); tableTitle.visible = false; step += 1; updateTotalWin(game, dwin, step);', 2000);
            setTimeout('enableInputCards(); unlockDisplay(); showButtons([[startButton]]); showDoubleToAndTakeOrRiskTexts(game, totalWin, xSave, ySave, fontsize);', 3000);
        } else {
            tableTitle.visible = true;
            changeTableTitle('loseTitleGame2');
            hideDoubleToAndTakeOrRiskTexts();
            openSelectedCard(selectedCardR, valuesOfAllCards);
            lockDisplay();
            disableInputCards();
            setTimeout('openAllCards(valuesOfAllCards); disableInputCards(); lockDisplay();', 1000);
            setTimeout("tableTitle.visible = false; unlockDisplay(); game.state.start('game1');", 3000);
        }
    }
}

var doubleToText; //создаем переменные в которых содержится текст для табло
var takeOrRiskText;
var timerTitleAmin; // объект таймера для переклучения текстов
var xSave; //сохраняем для последующего использования координаты
var ySave;
function showDoubleToAndTakeOrRiskTexts(game, totalWin, x,y, fontsize) {
    xSave = x;
    ySave = y;

    var i = 1;
    timerTitleAmin = setInterval(function() {
        if(i == 0) {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            doubleToText = game.add.text(x, y, 'DOUBLE TO '+totalWin*2+' ?', {
                font: fontsize+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 1;
        } else {
            if(typeof(doubleToText) != "undefined") {
                doubleToText.visible = false;
            }
            if(typeof(takeOrRiskText) != "undefined") {
                takeOrRiskText.visible = false;
            }

            takeOrRiskText = game.add.text(x, y, 'TAKE OR RISK', {
                font: fontsize+'px "Press Start 2P"',
                fill: '#ffffff',
                stroke: '#000000',
                strokeThickness: 3,
            });

            i = 0;
        }

    }, 500);
}

function hideDoubleToAndTakeOrRiskTexts() {
    doubleToText.visible = false;
    takeOrRiskText.visible = false;
    clearInterval(timerTitleAmin);
}

function updateTotalWin(game, dwin, step){
    //обновление totalWin в игре с картами

    linesScore.visible = false;
    riskStep.visible = false;

    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], dwin, {
        font: scorePosions[1][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });

    riskStep = game.add.text(scorePosions[3][0], scorePosions[3][1], step, {
        font: scorePosions[3][2]+'px "Press Start 2P"',
        fill: '#fcfe6e',
        stroke: '#000000',
        strokeThickness: 3,
    });
}

//переменные содержащие объекты карт
var card1; var card2; var card3; var card4; var card5; //card1 - карта диллера
var cardArray = [card1, card2, card3, card4, card5];

//var cardPosition = [[x,y], [x,y], [x,y], [x,y], [x,y]]  - нулевой элемент массива карта диллера
var openCard; //звуки
var winCard;
function addCards(game, cardPosition) {
    if(!isMobile) {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    } else {
        openCard = game.add.audio("openCard");
        winCard = game.add.audio("winCard");

        card1 = game.add.sprite(cardPosition[0][0],cardPosition[0][1], 'card_bg');
        card2 = game.add.sprite(cardPosition[1][0],cardPosition[1][1], 'card_bg');
        card3 = game.add.sprite(cardPosition[2][0],cardPosition[2][1], 'card_bg');
        card4 = game.add.sprite(cardPosition[3][0],cardPosition[3][1], 'card_bg');
        card5 = game.add.sprite(cardPosition[4][0],cardPosition[4][1], 'card_bg');

        card2.inputEnabled = true;
        card2.events.onInputDown.add(function(){
            requestDouble(gamename, 1, lines, bet, sid);
        });
        card3.inputEnabled = true;
        card3.events.onInputDown.add(function(){
            requestDouble(gamename, 2, lines, bet, sid);
        });
        card4.inputEnabled = true;
        card4.events.onInputDown.add(function(){
            requestDouble(gamename, 3, lines, bet, sid);
        });
        card5.inputEnabled = true;
        card5.events.onInputDown.add(function(){
            requestDouble(gamename, 4, lines, bet, sid);
        });

        cardArray[0] = card1;
        cardArray[1] = card2;
        cardArray[2] = card3;
        cardArray[3] = card4;
        cardArray[4] = card5;
    }
}

function openDCard(dcard) {
    lockDisplay();
    disableInputCards();
    setTimeout("card1.loadTexture('card_'+dcard); openCard.play(); enableInputCards(); unlockDisplay();", 1000);
}

var selectedCardValue; //значение карты выбранной игроком
function openSelectedCard(selectedCardR, valuesOfAllCards) {
    openCard.play();
    cardArray[selectedCardR].loadTexture("card_"+valuesOfAllCards[selectedCardR]);
}

var valuesOfAllCards; // значения остальных карт [,,,,]
function openAllCards(valuesOfAllCards) {
    openCard.play();
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_'+valuesOfAllCards[i]);
    });
}

function hideAllCards(cardArray) {
    cardArray.forEach(function (item, i) {
        item.loadTexture('card_bg');
        item.inputEnabled
    });
}

function disableInputCards() {
    card2.inputEnabled = false;
    card3.inputEnabled = false;
    card4.inputEnabled = false;
    card5.inputEnabled = false;
}

function enableInputCards() {
    card2.inputEnabled = true;
    card3.inputEnabled = true;
    card4.inputEnabled = true;
    card5.inputEnabled = true;
}





//функции для игры с последовательным выбором (веревки, ящики, бочки и т.д.)

// отображает результат выбора веревки (подлетает цифра и пересчитвается значение totalWin)
var loseTryInGame3 = false;
function showWinGame3(winRopeNumberSize, x, y, win, stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim) {

    if(typeWinRopeNumberAnim == 0) {
        if(loseTryInGame3 == false) {
            if(ropeValues[ropeStep - 1] > 0 && ropeStep == 5) {

                var text = game.add.text(x,y, win, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "Press Start 2P"',
                    fill: '#fcfe6e',
                    stroke: '#000000',
                    strokeThickness: 3,
                });

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game4')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    var text = game.add.text(x,y, win, { font: winRopeNumberSize+'px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }
        } else {
            if(ropeValues[ropeStep - 1] > 0 && ropeStep == 5) {
                var text = game.add.text(x,y, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                linesScore.visible = false;
                linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                    font: scorePosions[1][2]+'px "Press Start 2P"',
                    fill: '#fcfe6e',
                    stroke: '#000000',
                    strokeThickness: 3,
                });

                ropeStep = 0;
                loseTryInGame3 = false; //удаляем память о проигрышной попытке (использовании шлема)

                setTimeout("game.state.start('game1')", timeoutGame3ToGame4);
            } else {
                if(win == 0) {
                    updateBalanceGame3(game, scorePosions, balanceR);
                } else {
                    var text = game.add.text(x,y, win, { font: '22px \"Press Start 2P\"', fill: '#fcfe6e', stroke: '#000000', strokeThickness: 3});

                    linesScore.visible = false;
                    linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], stepTotalWinR, {
                        font: scorePosions[1][2]+'px "Press Start 2P"',
                        fill: '#fcfe6e',
                        stroke: '#000000',
                        strokeThickness: 3,
                    });
                }
            }
        }

    }

}

function updateBalanceGame3(game, scorePosions, balanceR) {
    if(!isMobile) {
        hideButtons([[buttonLine1, 'buttonLine1'],[buttonLine3, 'buttonLine3'], [buttonLine5, 'buttonLine5'], [buttonLine7, 'buttonLine7'], [buttonLine9, 'buttonLine9'], ]);
    }

    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        setTimeout("game.state.start('game1');", 1000);
    }, timeInterval);
}



//функции для игры с выбором из двух вариантов

function updateBalanceGame4(game, scorePosions, balanceR) {
    balanceScore.visible = false;

    var takeWin = game.add.audio('takeWin');
    //takeWin.addMarker('take', 0, 0.6);
    takeWin.loop = true;
    takeWin.play();

    var interval = 5;

    var ropeValuesResult = 0;
    ropeValues.forEach(function (item) {
        ropeValuesResult += item*lines*betline;
    });

    var balanceDifference = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult)) - parseInt(balanceR);

    if(balanceDifference < 0) {
        //балан уменьшился
        var timeInterval = parseInt((-1)*(interval*balanceDifference));
        var mark = -1;
    } else {
        //баланс увеличился
        var timeInterval = parseInt(interval*balanceDifference);
        var mark = 1;
    }

    var currentBalanceDifference = 0;

    var textCounter = setInterval(function () {

        currentBalanceDifference += 1*mark;

        balanceScore.visible = false;
        balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balanceR) + parseInt(currentBalanceDifference), {
            font: scorePosions[2][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });

        linesScore.visible = false;
        linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], parseInt(ropeValuesResult) - currentBalanceDifference, {
            font: scorePosions[1][2]+'px "Press Start 2P"',
            fill: '#fcfe6e',
            stroke: '#000000',
            strokeThickness: 3,
        });
    }, interval);

    balance = parseInt(parseInt(balanceR)+parseInt(ropeValuesResult));

    setTimeout(function() {
        takeWin.stop();
        clearInterval(textCounter);
        game.state.start('game1');
    }, timeInterval);
}





//функции для мобильной версии

var startButton; var double; var bet1; var dollar; var gear; var home;
function addButtonsGame1Mobile(game) {

    startButton = game.add.sprite(588, 228, 'startButton');
    startButton.bringToTop();
    startButton.anchor.setTo(0.5, 0.5);
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('startButton');
    });
    startButton.events.onInputDown.add(function(){
        if(checkUpdateBalance == false) { //проверка на то идет ли обновление баланса
            startButton.loadTexture('startButton_d');
            if(checkWin == 0) {
                hideLines([]);
                requestSpin(gamename, betline, lines, bet, sid);
            } else {
                takePrize(game, scorePosions, balanceOld, balance);
            }
        } else {

            //быстрое получение приза

            checkUpdateBalance = false;

            //сопутствующие действия
            showButtons([[startButton], [home], [gear], [dollar], [bet1]]);
            takeWin.stop();
            changeTableTitle('play1To');

            //останавливаем счетчики
            clearInterval(textCounter);
            clearInterval(totalWinRCounter);
            clearInterval(timer);
            clearTimeout(ActionsAfterUpdatingBalance);

            //отображаем конечный результат
            balanceScore.visible = false;
            balanceScore = game.add.text(scorePosions[2][0], scorePosions[2][1], parseInt(balance), {
                font: scorePosions[2][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

            linesScore.visible = false;
            linesScore = game.add.text(scorePosions[1][0], scorePosions[1][1], lines, {
                font: scorePosions[1][2]+'px "Press Start 2P"',
                fill: '#fcfe6e',
                stroke: '#000000',
                strokeThickness: 3,
            });

        }
    });

    double = game.add.sprite(549, 133, 'double');
    double.inputEnabled = true;
    double.input.useHandCursor = true;
    double.events.onInputDown.add(function(){
        checkWin = 0;
        hideNumbersAmin();
        game.state.start('game2');
    });
    double.events.onInputUp.add(function(){
        double.loadTexture('double');
    });
    double.visible = false;

    bet1 = game.add.sprite(546, 274, 'bet1');
    bet1.inputEnabled = true;
    bet1.input.useHandCursor = true;
    bet1.events.onInputDown.add(function(){
        /*lines = 9;
         betline = 25;*/

        bet1.loadTexture('bet1_p');
        document.getElementById('betMode').style.display = 'block';
        widthVisibleZone = $('.betWrapper .visibleZone').height();
        console.log(widthVisibleZone);
        $('.betCell').css('height', widthVisibleZone*0.32147 + 'px');
        $('canvas').css('display', 'none');
    });
    bet1.events.onInputUp.add(function(){
        bet1.loadTexture('bet1');
    });

    dollar = game.add.sprite(445, 30, 'dollar');
    dollar.scale.setTo(0.7,0.7);
    dollar.inputEnabled = true;
    dollar.input.useHandCursor = true;
    dollar.events.onInputDown.add(function(){
        //game.state.start('game4');
    });

    gear = game.add.sprite(519, 30, 'gear');
    gear.scale.setTo(0.7,0.7);
    gear.inputEnabled = true;
    gear.input.useHandCursor = true;
    gear.events.onInputDown.add(function(){
        //game.state.start('game3');
    });

    home = game.add.sprite(45, 30, 'home');
    home.scale.setTo(0.7,0.7);
    home.inputEnabled = true;
    home.input.useHandCursor = true;
    home.events.onInputDown.add(function(){
        home.loadTexture('home_p');
    });
    home.events.onInputUp.add(function(){
        home.loadTexture('home');
    });
}

function addButtonsGame2Mobile(game) {
    startButton = game.add.sprite(538, 300, 'collect');
    startButton.inputEnabled = true;
    startButton.input.useHandCursor = true;
    startButton.events.onInputUp.add(function(){
        startButton.loadTexture('collect');
    });
    startButton.events.onInputDown.add(function(){
        hideDoubleToAndTakeOrRiskTexts();
        game.state.start('game1');
    });
    startButton.scale.setTo(0.7,0.7);
}

var ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации
var winRopeNumberPosition = []; // координаты цифр выигрышей обозначающих множетель  [[x,y],[x,y], ...]. Идут в том порядке что и кнопки
var timeoutForShowWinRopeNumber = 0; //время до появления множетеля
var typeWinRopeNumberAnim = 0; // тип появления множенетя: 0- простой; 1- всплывание; дальше можно по аналогии в функции showWin добавлять свои версии
var timeoutGame3ToGame4 = 0; // время до перехода на четвертый экран в случае выигрыша
var checkHelm = false; //проверка есть ли каска
var winRopeNumberSize = 22;
var buttonsGame3Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок
function addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm) {
    //логика для событий нажатия на изображения
    buttonsGame3Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){

            button.inputEnabled = false;

            if(ropeValues[ropeStep] != 0) { // изменяем значение общего выигрыша
                stepTotalWinR += betline*lines*ropeValues[ropeStep];
            }

            ropesAnim[i](ropeValues,ropeStep, checkHelm);

            //setTimeout(showWin, 500, 195+440-mobileX,180-mobileY, ropeValues[ropeStep], stepTotalWinR);
            setTimeout(showWinGame3,timeoutForShowWinRopeNumber, winRopeNumberSize, winRopeNumberPosition[i][0],winRopeNumberPosition[i][1], ropeValues[ropeStep], stepTotalWinR, timeoutGame3ToGame4, typeWinRopeNumberAnim);

            ropeStep += 1;
        });
    });
}

//функции для игры с выбором из двух вариантов
var selectionsAmin = [] //в массиве по порядку содержатся функции которые выполняют анимации
var timeout1Game4ToGame1 = 0; var timeout2Game4ToGame1 = 0; //параметры задающие время перехода на первый экран в случае выигрыша и проигрыша. Если параметры не нужны, то можно их впилить из функции
var buttonsGame4Mobile = []; //массив в котором содержатся фазер-объекты изображений-кнопок
function addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile,  timeout1Game4ToGame1, timeout2Game4ToGame1) {
    buttonsGame4Mobile.forEach(function (button, i) {
        button.events.onInputDown.add(function(){
            button.inputEnabled = false;

            selectionsAmin[i](ropeValues, ropeStep);

            if(ropeValues[5] != 0){
                lockDisplay();
                setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout1Game4ToGame1);
            } else {
                lockDisplay();
                setTimeout('updateBalanceGame4(game, scorePosions, balanceR);',timeout2Game4ToGame1);
            }
        });
    });
}






//выбор множителя в меню bet
var denomination = 1;
function selectDenomination(el) {
    denomination = el.innerText;

    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;

    var selectedElement = document.getElementsByClassName('denomSize selected');
    selectedElement[0].className = 'denomSize';

    el.className = 'denomSize selected';
}

//выставление максимального значения ставки на линию
function maxBetlineForBetMenu() {
    maxBetline();
    document.getElementById('panelRealBet').innerHTML = lines*betline*denomination;
    document.getElementsByClassName('checkCssTopBetLineRange')[0].style.top = '34.5%';
    document.querySelectorAll('.checkCssTopBetLineRange')[0].querySelectorAll('.selected')[0].classList.remove('selected');
    document.getElementById('cellBetLine25').classList.add('selected');
}


//===========================================================================================================
//============== GAME 1 =====================================================================================
//===========================================================================================================

requestInit();

(function () {

    var game1 = {};

    game1.preload = function () {};

    game1.soundStatus = true;

    game1.create = function () {

        checkGame = 1;

        // звуки
        var playSound = game.add.audio('play');


        // изображения
        topBarImage = game.add.sprite(93-mobileX,23-mobileY, 'game.backgroundGame1Score1');

        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,21-mobileY, 'game.totalBet');


        slotPosition = [[144-3-mobileX, 87-30-mobileY], [144-3-mobileX, 193-40-mobileY], [144-3-mobileX, 299-50-mobileY], [254-1-mobileX, 87-30-mobileY], [254-1-mobileX, 193-40-mobileY], [254-1-mobileX, 299-50-mobileY], [365-mobileX, 87-30-mobileY], [365-mobileX, 193-40-mobileY], [365-mobileX, 299-50-mobileY], [477-1-mobileX, 87-30-mobileY], [477-1-mobileX, 193-40-mobileY], [477-1-mobileX, 299-50-mobileY], [589-1-mobileX, 87-30-mobileY], [589-1-mobileX, 193-40-mobileY], [589-1-mobileX, 299-50-mobileY]];
        addSlots(game, slotPosition);

        addTableTitle(game, 'play1To',270-mobileX,358-mobileY);




        var linePosition = [[134-mobileX,199-mobileY], [134-mobileX,71-mobileY], [134-mobileX,322-mobileY], [134-mobileX,130-mobileY], [134-mobileX,95-mobileY], [134-mobileX,102-mobileY], [134-mobileX,228-mobileY], [134-mobileX,226-mobileY], [134-mobileX,120-mobileY]];
        var numberPosition = [[109-mobileX,183-mobileY], [109-mobileX,54-mobileY], [109-mobileX,310-mobileY], [109-mobileX,118-mobileY], [109-mobileX,246-mobileY], [109-mobileX,86-mobileY], [109-mobileX,278-mobileY], [109-mobileX,214-mobileY], [109-mobileX,150-mobileY]];
        addLinesAndNumbers(game, linePosition, numberPosition);


        hideLines([]);
        hideNumbers([]);
        var lineArray = [];
        for (var i = 0; i <= lines; i++) {
            if(i != 0) {
                lineArray.push(i);
            }
        }
        showNumbers(lineArray);
        showLines(lineArray);



        // кнопки
        addButtonsGame1Mobile(game);



        //счет

        // betScore, linesScore, balanceScore, betline1Score, betline2Score
        scorePosions = [[230-mobileX, 23-mobileY, 20], [435-mobileX, 23-mobileY, 20], [610-mobileX, 23-mobileY, 20], [697-mobileX, 369-mobileY, 16], [697-mobileX, 369-mobileY, 16]];
        addScore(game, scorePosions, bet, lines, balance, betline);


        // анимация

        function hideBaba(){
            baba1.visible = false;
            baba2.visible = false;
            baba3.visible = false;
            baba4.visible = false;
            baba5.visible = false;
            baba6.visible = false;
        }

        function showRandBaba(){
            var randBaba = randomNumber(1,6);

            switch(randBaba) {
                case 1:
                    hideBaba();
                    baba1.visible = true;
                    baba1.animations.getAnimation('baba1').play().onComplete.add(function(){
                        baba1.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 2:
                    hideBaba();
                    baba2.visible = true;
                    baba2.animations.getAnimation('baba2').play().onComplete.add(function(){
                        baba2.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 3:
                    hideBaba();
                    baba3.visible = true;
                    baba3.animations.getAnimation('baba3').play().onComplete.add(function(){
                        baba3.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 4:
                    hideBaba();
                    baba4.visible = true;
                    baba4.animations.getAnimation('baba4').play().onComplete.add(function(){
                        baba4.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 5:
                    hideBaba();
                    baba5.visible = true;
                    baba5.animations.getAnimation('baba5').play().onComplete.add(function(){
                        baba5.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 6:
                    hideBaba();
                    baba6.visible = true;
                    baba6.animations.getAnimation('baba6').play().onComplete.add(function(){
                        baba6.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }

        baba1 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba1');
        baba1.animations.add('baba1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5, false);
        baba1.visible = false;

        baba2 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba2');
        baba2.animations.add('baba2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba2.visible = false;

        baba3 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba3');
        baba3.animations.add('baba3', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5, false);
        baba3.visible = false;

        baba4 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba4');
        baba4.animations.add('baba4', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba4.visible = false;

        baba5 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba5');
        baba5.animations.add('baba5', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba5.visible = false;

        baba6 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba6');
        baba6.animations.add('baba6', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5, false);
        baba6.visible = false;


        showRandBaba();

        cat = game.add.sprite(221-mobileX,374-mobileY, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557-mobileX,390-mobileY, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();

    };



    game1.update = function () {

    };

    game.state.add('game1', game1);

})();

//===========================================================================================================
//============== GAME 2 =====================================================================================
//===========================================================================================================


(function () {

    var button;

    var game2 = {};

    game2.preload = function () {

    };

    game2.create = function () {
        checkGame = 2;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;

        //изображения
        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.backgroundTotal2');
        backgroundGame3 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame3');

        addTableTitle(game, 'winTitleGame2', 235-mobileX,358-mobileY);
        hideTableTitle();


        //счет
        var step = 1;
        var inputWin = totalWinR;
        var totalWin = totalWinR;

        // inputWin, totalWin, balanceR, step
        scorePosions = [[230-mobileX, 23-mobileY, 20], [435-mobileX, 23-mobileY, 20], [610-mobileX, 23-mobileY, 20], [697-mobileX, 369-mobileY, 16], [697-mobileX, 369-mobileY, 16]];
        addScore(game, scorePosions, inputWin, totalWin, balanceR, step);

        //карты
        var cardPosition = [[127-mobileX,118-mobileY], [253-mobileX,118-mobileY], [365-mobileX,118-mobileY], [480-mobileX,118-mobileY], [592-mobileX,118-mobileY]];
        addCards(game, cardPosition);

        openDCard(dcard);

        //анимация

        function hideBaba(){
            baba1.visible = false;
            baba2.visible = false;
            baba3.visible = false;
            baba4.visible = false;
            baba5.visible = false;
            baba6.visible = false;
        }

        function showRandBaba(){
            var randBaba = randomNumber(1,6);

            switch(randBaba) {
                case 1:
                    hideBaba();
                    baba1.visible = true;
                    baba1.animations.getAnimation('baba1').play().onComplete.add(function(){
                        baba1.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 2:
                    hideBaba();
                    baba2.visible = true;
                    baba2.animations.getAnimation('baba2').play().onComplete.add(function(){
                        baba2.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 3:
                    hideBaba();
                    baba3.visible = true;
                    baba3.animations.getAnimation('baba3').play().onComplete.add(function(){
                        baba3.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 4:
                    hideBaba();
                    baba4.visible = true;
                    baba4.animations.getAnimation('baba4').play().onComplete.add(function(){
                        baba4.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 5:
                    hideBaba();
                    baba5.visible = true;
                    baba5.animations.getAnimation('baba5').play().onComplete.add(function(){
                        baba5.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 6:
                    hideBaba();
                    baba6.visible = true;
                    baba6.animations.getAnimation('baba6').play().onComplete.add(function(){
                        baba6.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }


        baba1 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba1');
        baba1.animations.add('baba1', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37], 5, false);
        baba1.visible = false;

        baba2 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba2');
        baba2.animations.add('baba2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba2.visible = false;

        baba3 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba3');
        baba3.animations.add('baba3', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36], 5, false);
        baba3.visible = false;

        baba4 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba4');
        baba4.animations.add('baba4', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba4.visible = false;

        baba5 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba5');
        baba5.animations.add('baba5', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19], 5, false);
        baba5.visible = false;

        baba6 = game.add.sprite(125-mobileX,326-mobileY, 'game.baba6');
        baba6.animations.add('baba6', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 5, false);
        baba6.visible = false;


        showRandBaba();

        cat = game.add.sprite(221-mobileX,374-mobileY, 'game.cat');
        cat.animations.add('cat', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58], 5, true);
        cat.animations.getAnimation('cat').play();

        testo = game.add.sprite(557-mobileX,390-mobileY, 'game.testo');
        testo.animations.add('testo', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17], 5, true);
        testo.animations.getAnimation('testo').play();


        fontsize = 20;
        showDoubleToAndTakeOrRiskTexts(game, totalWin, 335-mobileX,360-mobileY, fontsize);


        // кнопки
        addButtonsGame2Mobile(game);

    };

    game2.update = function () {};

    game.state.add('game2', game2);

})();

//===========================================================================================================
//============== GAME 3 =====================================================================================
//===========================================================================================================


(function () {

    var game3 = {};

    game3.preload = function () {

    };

    game3.create = function () {

        checkGame = 3;

        //сбрамываем необходимые переменные
        autostart = false;
        checkAutoStart = false;
        checkUpdateBalance = false;
        stepTotalWinR = 0;
        ropeStep = 0;
        checkWin = 0; //чтобы впоследствии на первом экране при нажатии start не происходил пересчет баланса

        //звуки
        babaOpenBox1 = game.add.sound('game.babaOpenBox1');
        babaOpenBox2 = game.add.sound('game.babaOpenBox2');
        babaOpenBox3 = game.add.sound('game.babaOpenBox3');
        babaOpenBadBox = game.add.sound('game.babaOpenBadBox');


        //изображения
        game.add.sprite(93-mobileX,53-mobileY, 'game.background1');
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.totalBet');
        backgroundGame4 = game.add.sprite(95-mobileX,54-mobileY, 'game.backgroundGame4');
        showBabaLegs(350-mobileX,465-mobileY);

        var pechka1 =  game.add.sprite(143-mobileX,182-mobileY,'game.pechka');
        var pechka2 =  game.add.sprite(255-mobileX,198-mobileY,'game.pechka');
        var pechka3 =  game.add.sprite(367-mobileX,182-mobileY,'game.pechka');
        var pechka4 =  game.add.sprite(479-mobileX,198-mobileY,'game.pechka');
        var pechka5 =  game.add.sprite(591-mobileX,182-mobileY,'game.pechka');
        pechka1.inputEnabled = true;
        pechka2.inputEnabled = true;
        pechka3.inputEnabled = true;
        pechka4.inputEnabled = true;
        pechka5.inputEnabled = true;


        //счет
        scorePosions = [[260-mobileX, 23-mobileY, 20], [435-mobileX, 23-mobileY, 20], [610-mobileX, 23-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
        addScore(game, scorePosions, bet, '', balance, betline);



        //кнопки и анимации событий нажатия

        ropesAnim = []; // в массиве по порядку содержатся функции которые выполняют анимации выигрыша

        winRopeNumberPosition = [[185-mobileX,155-mobileY],[195+103-mobileX,155-mobileY],[195+220-mobileX,155-mobileY],[195+330-mobileX,155-mobileY],[195+440-mobileX,155-mobileY]];
        timeoutForShowWinRopeNumber = 2000;
        timeoutGame3ToGame4 = 2000;
        typeWinRopeNumberAnim = 0;
        checkHelm = false;
        winRopeNumberSize = 22;


        ropesAnim[0] = function (ropeValues, ropeStep, checkHelm) {
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
            playAnim(0,0);
        };

        ropesAnim[1] = function (ropeValues, ropeStep, checkHelm) {
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
            playAnim(112,16);
        };

        ropesAnim[2] = function (ropeValues, ropeStep, checkHelm) {
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
            playAnim(112*2,0);
        };

        ropesAnim[3] = function (ropeValues, ropeStep, checkHelm) {
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
            playAnim(112*3,16);
        };

        ropesAnim[4] = function (ropeValues, ropeStep, checkHelm) {
            lockDisplay();
            setTimeout('unlockDisplay();',5500);
            playAnim(112*4,0);
        };

        //анимации

        function randomBox() {
            //добавление массивов изображений
            var arr = [];

            var blin = ['blin2','openBlin'];
            var chiсken = ['chiсken','openChiсken'];
            var fish = ['fish','openFish'];
            var pie = ['pie','openPie'];
            var pig = ['pig','openPig'];
            var porridge = ['porridge','openPorridge'];


            arr.push(blin,chiсken,fish,pie,pig,porridge);
            //arr.push(smoke);

            var rand = Math.floor(Math.random() * arr.length);

            return arr[rand];
        }

        function showBabaLegs(x,y){
            babaLegs = game.add.sprite(x,y, 'game.babaLegs');
        }

        function hideBaba(){
            baba7.visible = false;
            baba8.visible = false;
            baba9.visible = false;
            baba10.visible = false;
            baba11.visible = false;
            baba12.visible = false;

            baba7.animations.stop();
            baba8.animations.stop();
            baba9.animations.stop();
            baba10.animations.stop();
            baba11.animations.stop();
            baba12.animations.stop();
        }

        function showRandBaba(){
            var randBaba = randomNumber(7,10);

            switch(randBaba) {
                case 7:
                    hideBaba();
                    baba7.visible = true;
                    baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                        baba7.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 8:
                    hideBaba();
                    baba8.visible = true;
                    baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                        baba8.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 9:
                    hideBaba();
                    baba9.visible = true;
                    baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                        baba9.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 10:
                    hideBaba();
                    baba10.visible = true;
                    baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                        baba10.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 12:
                    hideBaba();
                    baba12.visible = true;
                    baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                        baba12.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }

        function createBaba(x,y){
            baba7 = game.add.sprite(x,y, 'game.baba7');
            baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
            baba7.visible = false;

            baba8 = game.add.sprite(x,y, 'game.baba8');
            baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
            baba8.visible = false;

            baba9 = game.add.sprite(x,y, 'game.baba9');
            baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
            baba9.visible = false;

            baba10 = game.add.sprite(x,y, 'game.baba10');
            baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
            baba10.visible = false;

            baba11 = game.add.sprite(x,y, 'game.baba11');
            baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
            baba11.visible = false;

            baba12 = game.add.sprite(x,y, 'game.baba12');
            baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            baba12.visible = false;
        }

        function playAnim(x,y){
            hideBaba();
            babaLegs.visible = false;

            var box = randomBox();

            babaOpenBox1.play();
            setTimeout("babaOpenBox2.play();",1000);

            if(ropeValues[ropeStep] == 0) {
                box = ['smoke','openSmoke'];
            }

            if(box[0] == 'smoke'){
                box1 = game.add.sprite(143+x-mobileX,150+y-mobileY, 'game.' + box[1]);
                box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

                box1.animations.getAnimation('box1').play().onComplete.add(function () {

                    babaOpenBadBox.play();

                    box1n = game.add.sprite(143+x-mobileX,150+y-mobileY, 'game.' + box[0]);
                    box1n.animations.add('box1n', [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2], 5, false);
                    box1n.animations.getAnimation('box1n').play().onComplete.add(function(){
                        hidedButtonArray = [];
                        game.state.start('game1');
                    });
                });

                babaOpenBox = game.add.sprite(122+x-mobileX,182+y-mobileY, 'game.babaOpenBox');
                babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
                babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
                    babaOpenBox.visible = false;

                    cat2.visible = false;
                    catSad = game.add.sprite(271-mobileX,70-mobileY, 'game.catSad');
                    catSad.animations.add('catSad', [0,1], 5, true);
                    catSad.animations.getAnimation('catSad').play();

                    //showBabaLegs(137,452);
                    babaLoseStick = game.add.sprite(122+x-mobileX,292+y-mobileY, 'game.babaLoseStick');
                    babaLoseStick.animations.add('babaLoseStick', [0,1,2,3,4,5,6], 5, false);
                    babaLoseStick.animations.getAnimation('babaLoseStick').play().onComplete.add(function(){
                        babaLoseStick.visible = false;

                        babSad = game.add.sprite(122+x-mobileX,293+y-mobileY, 'game.babSad');
                        babSad.animations.add('babSad', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22], 5, false);
                        babSad.animations.getAnimation('babSad').play().onComplete.add(function(){
                            hidedButtonArray = [];
                            game.state.start('game1');
                        });
                        babaSadLags = game.add.sprite(137+x-mobileX,452+y-mobileY, 'game.babaSadLags');
                    });



                });
            } else {
                box1 = game.add.sprite(143+x-mobileX,182+y-mobileY, 'game.' + box[1]);
                box1.animations.add('box1', [0,0,0,0,1,2,3,4,5,6,7], 5, false);

                box1.animations.getAnimation('box1').play().onComplete.add(function () {
                    if(box[0] != 'blin2'){
                        setTimeout("babaOpenBox3.play();",0);
                        box1n = game.add.sprite(143+x-mobileX,182+y-mobileY, 'game.' + box[0]);
                        box1n.animations.add('box1n', [0,1,2], 5, true);
                        box1n.animations.getAnimation('box1n').play();
                    } else {
                        setTimeout("babaOpenBox3.play();",0);
                        box1n = game.add.sprite(143+x-mobileX,182+y-mobileY, 'game.' + box[0]);
                        box1n.animations.add('box1n', [0,1,2,3,4,5,6,7], 5, false);
                        box1n.animations.getAnimation('box1n').play();
                    }
                });

                babaOpenBox = game.add.sprite(122+x-mobileX,182+y-mobileY, 'game.babaOpenBox');
                babaOpenBox.animations.add('babaOpenBox', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 7, false);
                babaOpenBox.animations.getAnimation('babaOpenBox').play().onComplete.add(function () {
                    babaOpenBox.visible = false;

                    showBabaLegs(137+x-mobileX,452+y-mobileY);
                    baba12 = game.add.sprite(122+x-mobileX,292+y-mobileY, 'game.baba12');
                    baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                    baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                        baba12.visible = false;

                        createBaba(122+x-mobileX,292+y-mobileY);
                        showRandBaba();
                    });



                });
            }
        }


        cat2 = game.add.sprite(271-mobileX,70-mobileY, 'game.cat2');
        cat2.animations.add('cat2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50], 5, true);
        cat2.animations.getAnimation('cat2').play();



        createBaba(334-mobileX,306-mobileY);

        showRandBaba();



        buttonsGame3Mobile[0] = pechka1;
        buttonsGame3Mobile[1] = pechka2;
        buttonsGame3Mobile[2] = pechka3;
        buttonsGame3Mobile[3] = pechka4;
        buttonsGame3Mobile[4] = pechka5;

        addButtonsGame3Mobile(game, ropesAnim, buttonsGame3Mobile, winRopeNumberSize, winRopeNumberPosition, timeoutForShowWinRopeNumber, typeWinRopeNumberAnim, timeoutGame3ToGame4, checkHelm);

    };

    game3.update = function () {};

    game.state.add('game3', game3);

})();

//===========================================================================================================
//============== GAME 4 =====================================================================================
//===========================================================================================================
ropeValues = [1,2,3,4,5,0];
(function () {

    var button;

    var game4 = {};

    game4.preload = function () {};

    game4.create = function () {
        //звуки
        winCards = game.add.audio("game.winCards");
        babaLoseFromWolf = game.add.audio('game.babaLoseFromWolf');

        //изображения
        //Добавление фона
        totalBet = game.add.sprite(95-mobileX,22-mobileY, 'game.totalBet');
        backgroundForGame2 = game.add.sprite(94-mobileX,54-mobileY, 'game.backgroundForGame2');
        titleForGame2 = game.add.sprite(222-mobileX,54-mobileY, 'game.titleForGame2');

        //счет
        scorePosions = [[260-mobileX, 23-mobileY, 20], [435-mobileX, 23-mobileY, 20], [610-mobileX, 23-mobileY, 20], [475-mobileX, 68-mobileY, 30]];
        addScore(game, scorePosions, bet, stepTotalWinR, balance, betline);


        //кнопки и анимации событий нажатия

        //для данной игры в каждую функцию вносится по две анимации (win и lose) для каждой кнопки
        selectionsAmin = [];
        timeout1Game4ToGame1 = 3000;
        timeout2Game4ToGame1 = 6000;

        // анимация для кнопки 3
        selectionsAmin[0] = function (ropeValues, ropeStep) {
            if(ropeValues[5] != 0){
                lockDisplay();

                setTimeout("winCards.play();",1000);

                hideBaba();
                babaLegs.visible = false;
                arrow1.visible = false;
                arrow2.visible = false;
                if(boogiman != undefined){
                    boogiman.animations.stop();
                    boogiman.visible = false;
                } else {
                    boogiman2.animations.stop();
                    boogiman2.visible = false;
                }

                babaTakeKeks1 = game.add.sprite(40-mobileX,215-mobileY, 'game.babaTakeKeks1');
                babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
                babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
                    babaTakeKeks1.visible = false;

                    babaTakeKeks2 = game.add.sprite(40-mobileX,215-mobileY, 'game.babaTakeKeks2');
                    babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
                    babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
                        babaTakeKeks2.visible = false;
                    });
                });

                setTimeout("unlockDisplay();",3000);
            } else {
                lockDisplay();

                hideBaba();
                babaLegs.visible = false;

                if(boogiman != undefined){
                    boogiman.animations.stop();
                    boogiman.visible = false;
                } else {
                    boogiman2.animations.stop();
                    boogiman2.visible = false;
                }


                babaStrike1_wolf = game.add.sprite(95+40-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
                babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
                babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
                    babaStrike1_wolf.visible = false;

                    upWolf = game.add.sprite(215+40-mobileX,20-15-mobileY, 'game.upWolf');
                    upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
                    upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
                        upWolf.visible = false;
                    });

                    babaLoseFromWolf.play();

                    arrow1.visible = false;
                    arrow2.visible = false;

                    babaStrike2_wolf = game.add.sprite(115+40-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
                    babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
                    babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
                        babaStrike2_wolf.visible = false;

                        babaAndFolf11 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf11');
                        babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
                        babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
                            babaAndFolf11.visible = false;

                            babaAndFolf12 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf12');
                            babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
                            babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
                                babaAndFolf12.visible = false;

                                babaAndFolf21 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf21');
                                babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
                                babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
                                    babaAndFolf21.visible = false;

                                    babaAndFolf22 = game.add.sprite(-33+40-mobileX,50-15-mobileY, 'game.babaAndFolf22');
                                    babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
                                    babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
                                        babaAndFolf22.visible = false;
                                    });
                                });

                            });
                        });
                    });
                });

                setTimeout("unlockDisplay();",6000);
            }
        };

        // анимация для кнопки 7
        selectionsAmin[1] = function (ropeValues, ropeStep) {
            if(ropeValues[5] != 0){
                lockDisplay();

                setTimeout("winCards.play();",1000);

                hideBaba();
                babaLegs.visible = false;
                arrow1.visible = false;
                arrow2.visible = false;
                if(boogiman != undefined){
                    boogiman.animations.stop();
                    boogiman.visible = false;
                } else {
                    boogiman2.animations.stop();
                    boogiman2.visible = false;
                }

                babaTakeKeks1 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks1');
                babaTakeKeks1.animations.add('babaTakeKeks1', [0,1,2,3,4,5,6,7,8,9], 7, false);
                babaTakeKeks1.animations.getAnimation('babaTakeKeks1').play().onComplete.add(function(){
                    babaTakeKeks1.visible = false;

                    babaTakeKeks2 = game.add.sprite(300-mobileX,215-mobileY, 'game.babaTakeKeks2');
                    babaTakeKeks2.animations.add('babaTakeKeks2', [0,1,2,3,4,5,6,6,6,6,6,6,6,6,6], 7, false);
                    babaTakeKeks2.animations.getAnimation('babaTakeKeks2').play().onComplete.add(function(){
                        babaTakeKeks2.visible = false;
                    });
                });

                setTimeout("unlockDisplay();",3000);
            } else {
                lockDisplay();

                hideBaba();
                babaLegs.visible = false;

                if(boogiman != undefined){
                    boogiman.animations.stop();
                    boogiman.visible = false;
                } else {
                    boogiman2.animations.stop();
                    boogiman2.visible = false;
                }


                babaStrike1_wolf = game.add.sprite(95+40+260-mobileX,252-15-mobileY, 'game.babaStrike1_wolf');
                babaStrike1_wolf.animations.add('babaStrike1_wolf', [0,1,2,3,4,5], 10, false);
                babaStrike1_wolf.animations.getAnimation('babaStrike1_wolf').play().onComplete.add(function(){
                    babaStrike1_wolf.visible = false;

                    upWolf = game.add.sprite(215+40+260-mobileX,20-15-mobileY, 'game.upWolf');
                    upWolf.animations.add('upWolf', [0,1,2,3], 7, false);
                    upWolf.animations.getAnimation('upWolf').play().onComplete.add(function(){
                        upWolf.visible = false;
                    });

                    babaLoseFromWolf.play();

                    arrow1.visible = false;
                    arrow2.visible = false;

                    babaStrike2_wolf = game.add.sprite(115+40+260-mobileX,300-15-mobileY, 'game.babaStrike2_wolf');
                    babaStrike2_wolf.animations.add('babaStrike2_wolf', [0,1,2,3,4,5], 10, false);
                    babaStrike2_wolf.animations.getAnimation('babaStrike2_wolf').play().onComplete.add(function(){
                        babaStrike2_wolf.visible = false;

                        babaAndFolf11 = game.add.sprite(-33+40+260-mobileX,50-15-mobileY, 'game.babaAndFolf11');
                        babaAndFolf11.animations.add('babaAndFolf11', [0,1,2,3,4,5,6,7], 7, false);
                        babaAndFolf11.animations.getAnimation('babaAndFolf11').play().onComplete.add(function(){
                            babaAndFolf11.visible = false;

                            babaAndFolf12 = game.add.sprite(-33+40+260-mobileX,50-15-mobileY, 'game.babaAndFolf12');
                            babaAndFolf12.animations.add('babaAndFolf12', [0,1,2,3,4,5,6,7], 7, false);
                            babaAndFolf12.animations.getAnimation('babaAndFolf12').play().onComplete.add(function(){
                                babaAndFolf12.visible = false;

                                babaAndFolf21 = game.add.sprite(-33+40+260-mobileX,50-15-mobileY, 'game.babaAndFolf21');
                                babaAndFolf21.animations.add('babaAndFolf21', [0,1,2,3,4,5,6,7], 7, false);
                                babaAndFolf21.animations.getAnimation('babaAndFolf21').play().onComplete.add(function(){
                                    babaAndFolf21.visible = false;

                                    babaAndFolf22 = game.add.sprite(-33+40+260-mobileX,50-15-mobileY, 'game.babaAndFolf22');
                                    babaAndFolf22.animations.add('babaAndFolf22', [0,1,2], 7, false);
                                    babaAndFolf22.animations.getAnimation('babaAndFolf22').play().onComplete.add(function(){
                                        babaAndFolf22.visible = false;
                                    });
                                });

                            });
                        });
                    });
                });

                setTimeout("unlockDisplay();",6000);
            }
        };




        //анимации


        function showRandBaba(){
            var randBaba = randomNumber(7,10);

            switch(randBaba) {
                case 7:
                    hideBaba();
                    baba7.visible = true;
                    baba7.animations.getAnimation('baba7').play().onComplete.add(function(){
                        baba7.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 8:
                    hideBaba();
                    baba8.visible = true;
                    baba8.animations.getAnimation('baba8').play().onComplete.add(function(){
                        baba8.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 9:
                    hideBaba();
                    baba9.visible = true;
                    baba9.animations.getAnimation('baba9').play().onComplete.add(function(){
                        baba9.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 10:
                    hideBaba();
                    baba10.visible = true;
                    baba10.animations.getAnimation('baba10').play().onComplete.add(function(){
                        baba10.animations.stop();
                        showRandBaba();
                    });
                    break;
                case 12:
                    hideBaba();
                    baba12.visible = true;
                    baba12.animations.getAnimation('baba12').play().onComplete.add(function(){
                        baba12.animations.stop();
                        showRandBaba();
                    });
                    break;

            }
        }

        function showBabaLegs(x,y){
            babaLegs = game.add.sprite(x,y, 'game.babaLegs');
        }

        function createBaba(x,y){
            baba7 = game.add.sprite(x,y, 'game.baba7');
            baba7.animations.add('baba7', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
            baba7.visible = false;

            baba8 = game.add.sprite(x,y, 'game.baba8');
            baba8.animations.add('baba8', [0,1,2,3,4,5,6,7,8,9,10,11,12,13], 5, false);
            baba8.visible = false;

            baba9 = game.add.sprite(x,y, 'game.baba9');
            baba9.animations.add('baba9', [0,1,2,3,4,5,6,7,8], 5, false);
            baba9.visible = false;

            baba10 = game.add.sprite(x,y, 'game.baba10');
            baba10.animations.add('baba10', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 5, false);
            baba10.visible = false;

            baba11 = game.add.sprite(x,y, 'game.baba11');
            baba11.animations.add('baba11', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, false);
            baba11.visible = false;

            baba12 = game.add.sprite(x,y, 'game.baba12');
            baba12.animations.add('baba12', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
            baba12.visible = false;
        }

        function showBoogiman(){
            arrayXY = [[250-mobileX,300-mobileY],[270-mobileX,320-mobileY],[[230-mobileX],[300-mobileY]],[[260-mobileX],[310-mobileY]]];

            var rand = randomNumber(1,2);

            switch (rand) {
                case 1:
                    var x = arrayXY[0][0];
                    var y = arrayXY[0][1];

                    boogiman = game.add.sprite(x,y, 'game.boogiman');
                    boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                    boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                        boogiman.visible = false;

                        boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                        boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                            boogiman2.visible = false;

                            showBoogiman();
                        });
                    });

                    break;
                case 2:
                    var x = arrayXY[1][0];
                    var y = arrayXY[1][1];

                    boogiman = game.add.sprite(x,y, 'game.boogiman');
                    boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                    boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                        boogiman.visible = false;

                        boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                        boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                            boogiman2.visible = false;

                            showBoogiman();
                        });
                    });

                    break;
                case 3:
                    var x = arrayXY[2][0];
                    var y = arrayXY[2][1];

                    boogiman = game.add.sprite(x,y, 'game.boogiman');
                    boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                    boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                        boogiman.visible = false;

                        boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                        boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                            boogiman2.visible = false;

                            showBoogiman();
                        });
                    });
                    break;
                case 4:
                    var x = arrayXY[3][0];
                    var y = arrayXY[3][1];

                    boogiman = game.add.sprite(x,y, 'game.boogiman');
                    boogiman.animations.add('boogiman', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                    boogiman.animations.getAnimation('boogiman').play().onComplete.add(function(){
                        boogiman.visible = false;

                        boogiman2 = game.add.sprite(x+310,y, 'game.boogiman');
                        boogiman2.animations.add('boogiman2', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 5, false);
                        boogiman2.animations.getAnimation('boogiman2').play().onComplete.add(function(){
                            boogiman2.visible = false;

                            showBoogiman();
                        });
                    });
                    break;
            }

        }

        function hideBaba(){
            baba7.visible = false;
            baba8.visible = false;
            baba9.visible = false;
            baba10.visible = false;
            baba11.visible = false;
            baba12.visible = false;
        }

        var arrow1 = game.add.sprite(295-mobileX,232-mobileY, 'game.arrow1');
        arrow1.inputEnabled = true;
        

        var arrow2 = game.add.sprite(595-mobileX,232-mobileY, 'game.arrow2');
        arrow2.inputEnabled = true;

        arrow1.animations.add('arrow1', [0,1], 2, true);
        arrow2.animations.add('arrow2', [0,1], 2, true);
        arrow1.animations.getAnimation('arrow1').play();
        arrow2.animations.getAnimation('arrow2').play();

        showBoogiman();
        showBabaLegs(117,452);
        createBaba(101,292);
        showRandBaba();

        buttonsGame4Mobile[0] = arrow1;
        buttonsGame4Mobile[1] = arrow2;


        addButtonsGame4Mobile(game, selectionsAmin, buttonsGame4Mobile, timeout1Game4ToGame1, timeout2Game4ToGame1)

    };

    game4.update = function () {
    };

    game.state.add('game4', game4);

})();

//===========================================================================================================
//============== PRELOAD ====================================================================================
//===========================================================================================================
(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            if(progress % 8 == 0) {
                document.getElementById('preloaderBar').style.width = progress + '%';
            }
        });

        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;

        game.load.image('startButton', needUrlPath + '/img/mobileButtons/spin.png');
        game.load.image('startButton_p', needUrlPath + '/img/mobileButtons/spin_p.png');
        game.load.image('startButton_d', needUrlPath + '/img/mobileButtons/spin_d.png');
        game.load.image('bet1', needUrlPath + '/img/mobileButtons/bet1.png');
        game.load.image('bet1_p', needUrlPath + '/img/mobileButtons/bet1_p.png');
        game.load.image('home', needUrlPath + '/img/mobileButtons/home.png');
        game.load.image('home_p', needUrlPath + '/img/mobileButtons/home_p.png');
        game.load.image('dollar', needUrlPath + '/img/mobileButtons/dollar.png');
        game.load.image('gear', needUrlPath + '/img/mobileButtons/gear.png');
        game.load.image('double', needUrlPath + '/img/mobileButtons/double.png');
        game.load.image('collect', needUrlPath + '/img/mobileButtons/collect.png');
        game.load.image('collect_p', needUrlPath + '/img/mobileButtons/collect_p.png');
        game.load.image('collect_d', needUrlPath + '/img/mobileButtons/collect_d.png');

        game.load.image('game.number1', needUrlPath + '/img/1.png');
        game.load.image('game.number2', needUrlPath + '/img/2.png');
        game.load.image('game.number3', needUrlPath + '/img/3.png');
        game.load.image('game.number4', needUrlPath + '/img/4.png');
        game.load.image('game.number5', needUrlPath + '/img/5.png');
        game.load.image('game.number6', needUrlPath + '/img/6.png');
        game.load.image('game.number7', needUrlPath + '/img/7.png');
        game.load.image('game.number8', needUrlPath + '/img/8.png');
        game.load.image('game.number9', needUrlPath + '/img/9.png');

        game.load.image('game.non_full',needUrlPath + '/img/full.png');
        game.load.image('game.full',needUrlPath + '/img/non_full.png');
        game.load.image('sound_on', needUrlPath + '/img/sound_on.png');
        game.load.image('sound_off', needUrlPath + '/img/sound_off.png');

        game.load.image('line1', needUrlPath + '/lines/select/1.png');
        game.load.image('line2', needUrlPath + '/lines/select/2.png');
        game.load.image('line3', needUrlPath + '/lines/select/3.png');
        game.load.image('line4', needUrlPath + '/lines/select/4.png');
        game.load.image('line5', needUrlPath + '/lines/select/5.png');
        game.load.image('line6', needUrlPath + '/lines/select/6.png');
        game.load.image('line7', needUrlPath + '/lines/select/7.png');
        game.load.image('line8', needUrlPath + '/lines/select/8.png');
        game.load.image('line9', needUrlPath + '/lines/select/9.png');

        game.load.image('linefull1', needUrlPath + '/lines/win/1.png');
        game.load.image('linefull2', needUrlPath + '/lines/win/2.png');
        game.load.image('linefull3', needUrlPath + '/lines/win/3.png');
        game.load.image('linefull4', needUrlPath + '/lines/win/4.png');
        game.load.image('linefull5', needUrlPath + '/lines/win/5.png');
        game.load.image('linefull6', needUrlPath + '/lines/win/6.png');
        game.load.image('linefull7', needUrlPath + '/lines/win/7.png');
        game.load.image('linefull8', needUrlPath + '/lines/win/8.png');
        game.load.image('linefull9', needUrlPath + '/lines/win/9.png');

        game.load.audio('line1Sound', needUrlPath + '/lines/sounds/line1.wav');
        game.load.audio('line3Sound', needUrlPath + '/lines/sounds/line3.wav');
        game.load.audio('line5Sound', needUrlPath + '/lines/sounds/line5.wav');
        game.load.audio('line7Sound', needUrlPath + '/lines/sounds/line7.wav');
        game.load.audio('line9Sound', needUrlPath + '/lines/sounds/line9.wav');

        game.load.audio('sound', needUrlPath + '/sound/spin.mp3');
        game.load.audio('rotateSound', needUrlPath + '/sound/rotate.wav');
        game.load.audio('stopSound', needUrlPath + '/sound/stop.wav');
        game.load.audio('tada', needUrlPath + '/sound/tada.wav');
        game.load.audio('play', needUrlPath + '/sound/play.mp3');
        game.load.audio('takeWin', needUrlPath + '/sound/takeWin.mp3');
        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');

        game.load.audio('soundWinLine8', needUrlPath + '/sound/winLines/sound12.mp3');
        game.load.audio('soundWinLine7', needUrlPath + '/sound/winLines/sound13.mp3');
        game.load.audio('soundWinLine6', needUrlPath + '/sound/winLines/sound14.mp3');
        game.load.audio('soundWinLine5', needUrlPath + '/sound/winLines/sound15.mp3');
        game.load.audio('soundWinLine4', needUrlPath + '/sound/winLines/sound16.mp3');
        game.load.audio('soundWinLine3', needUrlPath + '/sound/winLines/sound17.mp3');
        game.load.audio('soundWinLine2', needUrlPath + '/sound/winLines/sound18.mp3');
        game.load.audio('soundWinLine1', needUrlPath + '/sound/winLines/sound19.mp3');

        //карты
        game.load.image('card_bg', needUrlPath + '/img/shape107.png');

        game.load.image('card_39', needUrlPath + '/img/shape109.png');
        game.load.image('card_40', needUrlPath + '/img/shape111.png');
        game.load.image('card_41', needUrlPath + '/img/shape113.png');
        game.load.image('card_42', needUrlPath + '/img/shape115.png');
        game.load.image('card_43', needUrlPath + '/img/shape117.png');
        game.load.image('card_44', needUrlPath + '/img/shape119.png');
        game.load.image('card_45', needUrlPath + '/img/shape121.png');
        game.load.image('card_46', needUrlPath + '/img/shape123.png');
        game.load.image('card_47', needUrlPath + '/img/shape125.png');
        game.load.image('card_48', needUrlPath + '/img/shape127.png');
        game.load.image('card_49', needUrlPath + '/img/shape129.png');
        game.load.image('card_50', needUrlPath + '/img/shape131.png');
        game.load.image('card_51', needUrlPath + '/img/shape133.png');

        game.load.image('card_26', needUrlPath + '/img/shape135.png');
        game.load.image('card_27', needUrlPath + '/img/shape137.png');
        game.load.image('card_28', needUrlPath + '/img/shape139.png');
        game.load.image('card_29', needUrlPath + '/img/shape141.png');
        game.load.image('card_30', needUrlPath + '/img/shape143.png');
        game.load.image('card_31', needUrlPath + '/img/shape145.png');
        game.load.image('card_32', needUrlPath + '/img/shape147.png');
        game.load.image('card_33', needUrlPath + '/img/shape149.png');
        game.load.image('card_34', needUrlPath + '/img/shape151.png');
        game.load.image('card_35', needUrlPath + '/img/shape153.png');
        game.load.image('card_36', needUrlPath + '/img/shape155.png');
        game.load.image('card_37', needUrlPath + '/img/shape157.png');
        game.load.image('card_38', needUrlPath + '/img/shape159.png');

        game.load.image('card_0', needUrlPath + '/img/shape161.png');
        game.load.image('card_1', needUrlPath + '/img/shape163.png');
        game.load.image('card_2', needUrlPath + '/img/shape165.png');
        game.load.image('card_3', needUrlPath + '/img/shape167.png');
        game.load.image('card_4', needUrlPath + '/img/shape169.png');
        game.load.image('card_5', needUrlPath + '/img/shape171.png');
        game.load.image('card_6', needUrlPath + '/img/shape173.png');
        game.load.image('card_7', needUrlPath + '/img/shape175.png');
        game.load.image('card_8', needUrlPath + '/img/shape177.png');
        game.load.image('card_9', needUrlPath + '/img/shape179.png');
        game.load.image('card_10', needUrlPath + '/img/shape181.png');
        game.load.image('card_11', needUrlPath + '/img/shape183.png');
        game.load.image('card_12', needUrlPath + '/img/shape185.png');

        game.load.image('card_13', needUrlPath + '/img/shape187.png');
        game.load.image('card_14', needUrlPath + '/img/shape189.png');
        game.load.image('card_15', needUrlPath + '/img/shape191.png');
        game.load.image('card_16', needUrlPath + '/img/shape193.png');
        game.load.image('card_17', needUrlPath + '/img/shape195.png');
        game.load.image('card_18', needUrlPath + '/img/shape197.png');
        game.load.image('card_19', needUrlPath + '/img/shape199.png');
        game.load.image('card_20', needUrlPath + '/img/shape201.png');
        game.load.image('card_21', needUrlPath + '/img/shape203.png');
        game.load.image('card_22', needUrlPath + '/img/shape205.png');
        game.load.image('card_23', needUrlPath + '/img/shape207.png');
        game.load.image('card_24', needUrlPath + '/img/shape209.png');
        game.load.image('card_25', needUrlPath + '/img/shape211.png');

        game.load.image('card_52', needUrlPath + '/img/shape213.png');

        //game.load.image('pick', needUrlPath + '/img/shape340.png');

        game.load.image('cell0', needUrlPath + '/img/cell0.png');
        game.load.image('cell1', needUrlPath + '/img/cell1.png');
        game.load.image('cell2', needUrlPath + '/img/cell2.png');
        game.load.image('cell3', needUrlPath + '/img/cell3.png');
        game.load.image('cell4', needUrlPath + '/img/cell4.png');
        game.load.image('cell5', needUrlPath + '/img/cell5.png');
        game.load.image('cell6', needUrlPath + '/img/cell6.png');
        game.load.image('cell7', needUrlPath + '/img/cell7.png');
        game.load.image('cell8', needUrlPath + '/img/cell8.png');

        //файлы которых может и не быть
        game.load.image('takeOrRisk1', needUrlPath + '/img/takeOrRisk.png');
        game.load.image('play1To', needUrlPath + '/img/image546.png');
        game.load.image('bonusGame', needUrlPath + '/img/bonusGame.png');
        game.load.image('take', needUrlPath + '/img/image554.png');

        game.load.image('game.backgroundGame1Score1', needUrlPath + '/img/backgroundGame1Score1.png');
        game.load.spritesheet('selectionOfTheManyCellAnim', needUrlPath + '/img/pech.png', 96, 96); //текущее кол-во кадров = 11
        game.load.image('topScoreGame2', needUrlPath + '/img/image457.png');

        game.load.image('winTitleGame2', needUrlPath + '/img/image486.png');
        game.load.image('loseTitleGame2', needUrlPath + '/img/image484.png');
        game.load.image('forwadTitleGame2', needUrlPath + '/img/image504.png');

        game.load.audio('openCard', needUrlPath + '/sound/sound31.mp3');
        game.load.audio('winCard', needUrlPath + '/sound/sound30.mp3');

        game.load.image('game.backgroundTotal2', needUrlPath + '/img/image443.png');
        /* sources */

        game.load.image('game.background', needUrlPath + '/img/canvas-bg.svg');
        game.load.image('game.background1', needUrlPath + '/img/background1.png');
        game.load.image('game.totalBet', needUrlPath + '/img/totalBet.png');



        game.load.audio('game.winCards', needUrlPath + '/sound/sound30.mp3');
        game.load.audio('game.babaOpenBox1', needUrlPath + '/sound/babaOpenBox1.mp3');
        game.load.audio('game.babaOpenBox2', needUrlPath + '/sound/babaOpenBox2.mp3');
        game.load.audio('game.babaOpenBox3', needUrlPath + '/sound/babaOpenBox3.mp3');
        game.load.audio('game.babaOpenBadBox', needUrlPath + '/sound/babaOpenBadBox.mp3');
        game.load.audio('game.babaLoseFromWolf', needUrlPath + '/sound/lose.mp3');

        game.load.spritesheet('cellAnim', needUrlPath + '/img/cellAnim.jpg', 96, 96);

        game.load.spritesheet('game.baba1', needUrlPath + '/img/baba1.png', 96, 176);
        game.load.spritesheet('game.baba2', needUrlPath + '/img/baba2.png', 96, 176);
        game.load.spritesheet('game.baba3', needUrlPath + '/img/baba3.png', 96, 176);
        game.load.spritesheet('game.baba4', needUrlPath + '/img/baba4.png', 96, 176);
        game.load.spritesheet('game.baba5', needUrlPath + '/img/baba5.png', 96, 176);
        game.load.spritesheet('game.baba6', needUrlPath + '/img/baba6.png', 96, 176);

        game.load.spritesheet('game.baba7', needUrlPath + '/img/baba7.png', 128, 160);
        game.load.spritesheet('game.baba8', needUrlPath + '/img/baba8.png', 128, 160);
        game.load.spritesheet('game.baba9', needUrlPath + '/img/baba9.png', 128, 160);
        game.load.spritesheet('game.baba10', needUrlPath + '/img/baba10.png', 128, 160);
        game.load.spritesheet('game.baba11', needUrlPath + '/img/baba11.png', 128, 160);
        game.load.spritesheet('game.baba12', needUrlPath + '/img/baba12.png', 112, 160);

        game.load.spritesheet('game.cat', needUrlPath + '/img/cat.png', 48, 64);
        game.load.spritesheet('game.testo', needUrlPath + '/img/testo.png', 144, 64);

        game.load.spritesheet('game.flashNamber1', needUrlPath + '/img/flashingNumber1.png', 608, 32);
        game.load.spritesheet('game.flashNamber2', needUrlPath + '/img/flashingNumber2.png', 608, 32);
        game.load.spritesheet('game.flashNamber3', needUrlPath + '/img/flashingNumber3.png', 608, 32);
        game.load.spritesheet('game.flashNamber4', needUrlPath + '/img/flashingNumber4.png', 608, 32);
        game.load.spritesheet('game.flashNamber5', needUrlPath + '/img/flashingNumber5.png', 608, 32);
        game.load.spritesheet('game.flashNamber6', needUrlPath + '/img/flashingNumber6.png', 608, 32);
        game.load.spritesheet('game.flashNamber7', needUrlPath + '/img/flashingNumber7.png', 608, 32);
        game.load.spritesheet('game.flashNamber8', needUrlPath + '/img/flashingNumber8.png', 608, 32);
        game.load.spritesheet('game.flashNamber9', needUrlPath + '/img/flashingNumber9.png', 608, 32);

        game.load.spritesheet('game.blin', needUrlPath + '/img/blin.png', 96, 96);
        game.load.spritesheet('game.pech', needUrlPath + '/img/pech.png', 96, 96);

        game.load.image('game.backgroundGame3', needUrlPath + '/img/shape104.png');

        game.load.audio('game.winPech', needUrlPath + '/sound/winPech.mp3');


        game.load.image('game.backgroundGame4', needUrlPath + '/img/shape1545.png');
        game.load.image('game.babaLegs', needUrlPath + '/img/shape1191.png');
        game.load.spritesheet('game.cat2', needUrlPath + '/img/cat2.png', 80, 80);

        game.load.spritesheet('game.openPie', needUrlPath + '/img/openPie.png', 96, 112);
        game.load.spritesheet('game.openPorridge', needUrlPath + '/img/openPorridge.png', 96, 112);
        game.load.spritesheet('game.openBlin', needUrlPath + '/img/openBlin.png', 96, 112);
        game.load.spritesheet('game.openChiсken', needUrlPath + '/img/openChiсken.png', 96, 112);
        game.load.spritesheet('game.openFish', needUrlPath + '/img/openFish.png', 96, 112);
        game.load.spritesheet('game.openPig', needUrlPath + '/img/openPig.png', 96, 112);
        game.load.spritesheet('game.openSmoke', needUrlPath + '/img/openSmoke.png', 96, 144);

        game.load.spritesheet('game.blin2', needUrlPath + '/img/blin2.png', 96, 112);
        game.load.spritesheet('game.chiсken', needUrlPath + '/img/chiсken.png', 96, 112);
        game.load.spritesheet('game.fish', needUrlPath + '/img/fish.png', 96, 112);
        game.load.spritesheet('game.pie', needUrlPath + '/img/pie.png', 96, 112);
        game.load.spritesheet('game.pig', needUrlPath + '/img/pig.png', 96, 112);
        game.load.spritesheet('game.porridge', needUrlPath + '/img/porridge.png', 96, 112);
        game.load.spritesheet('game.smoke', needUrlPath + '/img/smoke.png', 96, 144);

        //game.load.image('game.babaLegsFromOpenBox', needUrlPath + '/img/babaLegsFromOpenBox.svg');
        game.load.spritesheet('game.babaOpenBox', needUrlPath + '/img/babaOpenBox.png', 128, 300);
        game.load.spritesheet('game.babaLoseStick', needUrlPath + '/img/babaLoseStick.png', 224, 192);
        game.load.spritesheet('game.babSad', needUrlPath + '/img/babSad.png', 128, 160);
        game.load.image('game.babaSadLags', needUrlPath + '/img/babaSadLags.png');
        game.load.spritesheet('game.catSad', needUrlPath + '/img/catSad.png', 80, 80);

        game.load.image('game.backgroundForGame2', needUrlPath + '/img/shape1189.png');
        game.load.image('game.titleForGame2', needUrlPath + '/img/shape1399.png');
        game.load.spritesheet('game.arrow1', needUrlPath + '/img/arrow1.png', 32, 32);
        game.load.spritesheet('game.arrow2', needUrlPath + '/img/arrow2.png', 32, 32);
        game.load.spritesheet('game.boogiman', needUrlPath + '/img/boogiman.png', 96, 80);
        game.load.spritesheet('game.babaStrike1_wolf', needUrlPath + '/img/babaStrike1_wolf.png', 208, 224);
        game.load.spritesheet('game.babaStrike2_wolf', needUrlPath + '/img/babaStrike2_wolf.png', 208, 192);
        game.load.spritesheet('game.upWolf', needUrlPath + '/img/upWolf.png', 160, 448);
        game.load.spritesheet('game.babaAndFolf11', needUrlPath + '/img/babaAndFolf11.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf12', needUrlPath + '/img/babaAndFolf12.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf21', needUrlPath + '/img/babaAndFolf21.png', 500, 471);
        game.load.spritesheet('game.babaAndFolf22', needUrlPath + '/img/babaAndFolf22.png', 500, 471);
        game.load.spritesheet('game.babaTakeKeks1', needUrlPath + '/img/babaTakeKeks1.png', 400, 300);
        game.load.spritesheet('game.babaTakeKeks2', needUrlPath + '/img/babaTakeKeks2.png', 400, 300);

        game.load.image('game.pechka', needUrlPath + '/img/shape1666.png');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

