<?php

header('Access-Control-Allow-Origin: *');
// header("Content-Type: application/json; charset=utf-8");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$salt = "3d19e27a-82ce-461a-8b75-4f96b0674f55";


//получаем баланс

//$my_file = 'balance.txt';
//$handle = fopen($my_file, 'r');
//$balance = fread($handle, filesize($my_file));
$balance = 0;
$ballsDouble = false;
////


// predvaritelnye statusi
$mesage = "succes";
$lastbal = 'No';

// проверка входящих данных
if (!isset($_GET['balls_selected']) or !isset($_GET['betValue'])) {
    $data = json_encode(["mesage" => "error with get data"]);
    echo $data;
    exit();
}

//>duble balls proverka
if (isset($_GET['ballsDouble'])) {
    $kenoDouble = true;
    $ballsDouble = $_GET['ballsDouble'];
} else {
    $kenoDouble = false;
}
//< end


// получаем шары которые выбрал игрок
$user_numbers = array_map('intval', explode(',', $_GET["balls_selected"]));
$sum_bet = floatval($_GET["betValue"]);
$picked = count($user_numbers);


if ($kenoDouble == true and $ballsDouble == 0) {
    $server_numbers = getNumbers($kenoDouble, $ballsDouble, 10);
} else {
    $server_numbers = getNumbers($kenoDouble, $ballsDouble, 20);
}

//>------------ uncomment for test ----------
$balance = 100000;

for ($x = 0; $x <= 100000; $x++) {
   $sum_bet = 1;
   $lastbal = 'No';
   $server_numbers = getNumbers($kenoDouble, $ballsDouble, 20);
   $ballsDouble = false;
   $firstTen = array_slice($server_numbers, 0, 10);
if (count(array_intersect($firstTen, $user_numbers)) > 1) {
	$sum_bet = 1;
	$x++;
}
//<

$balance -= $sum_bet;
$status = "lose";


// Проверка на совпадение
$result = array_intersect($server_numbers, $user_numbers);

// Генерация ответа
$response = [
    "server_numbers" => $server_numbers,
    "user_numbers" => $user_numbers,
    "result" => $result
];

$count = count($response['result']);

$win_sum = 0;

switch ($picked) {
    case 2:
        if ($count == 2) {
            $win_sum = $sum_bet * 8;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 3:
        if ($count == 3) {
            $win_sum = $sum_bet * 16;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 2;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 4:
        if ($count == 4) {
            $win_sum = $sum_bet * 36;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 4;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 1;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 5:
        if ($count == 5) {
            $win_sum = $sum_bet * 80;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 16;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 2;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 6:
        if ($count == 6) {
            $win_sum = $sum_bet * 150;
        } elseif ($count == 5) {
            $win_sum = $sum_bet * 20;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 7;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 1;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 7:
        if ($count == 7) {
            $win_sum = $sum_bet * 200;
        } elseif ($count == 6) {
            $win_sum = $sum_bet * 25;
        } elseif ($count == 5) {
            $win_sum = $sum_bet * 8;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 3;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 1;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 8:
        if ($count == 8) {
            $win_sum = $sum_bet * 500;
        } elseif ($count == 7) {
            $win_sum = $sum_bet * 80;
        } elseif ($count == 6) {
            $win_sum = $sum_bet * 22;
        } elseif ($count == 5) {
            $win_sum = $sum_bet * 8;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 2;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 9:
        if ($count == 9) {
            $win_sum = $sum_bet * 1000;
        } elseif ($count == 8) {
            $win_sum = $sum_bet * 200;
        } elseif ($count == 7) {
            $win_sum = $sum_bet * 50;
        } elseif ($count == 6) {
            $win_sum = $sum_bet * 10;
        } elseif ($count == 5) {
            $win_sum = $sum_bet * 5;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 1;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;

    case 10:
        if ($count == 10) {
            $win_sum = $sum_bet * 1000;
        } elseif ($count == 9) {
            $win_sum = $sum_bet * 500;
        } elseif ($count == 8) {
            $win_sum = $sum_bet * 50;
        } elseif ($count == 7) {
            $win_sum = $sum_bet * 11;
        } elseif ($count == 6) {
            $win_sum = $sum_bet * 6;
        } elseif ($count == 5) {
            $win_sum = $sum_bet * 3;
        } elseif ($count == 4) {
            $win_sum = $sum_bet * 1;
        } elseif ($count == 3) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 2) {
            $win_sum = $sum_bet * 0;
        } elseif ($count == 1) {
            $win_sum = $sum_bet * 0;
        }
        break;
}

if ($ballsDouble and in_array($server_numbers[19], $user_numbers)) {
    $win_sum *= 4;
    $lastbal = "yes";
}
// плюсуем выиграш
$balance += $win_sum;

//>------------ uncomment for test ----------
}
echo "count: " . $count . "\n";
echo "lastball: " . $lastbal . "\n";
echo "balance: " . $balance . "\n";
die();
//<


///баланс пока храним в файле
//$my_file = 'balance.txt';
//$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
//$data = $balance;
//fwrite($handle, $data);
////


if ($win_sum > 0) {
    $status = "win";
}

$password = hash('sha256', $salt.implode(",", $server_numbers));


//// kenoduble
if ($kenoDouble == true and $ballsDouble == 0) {
    $balance = false;
    $win_sum = false;
    $status = false;
    $count =false;
    $result=false;
    $lastbal=false;
    $picked=false;
} elseif ($kenoDouble == true and $ballsDouble != 0) {
    $keyRequest = $_GET['key'];

    $keyLocal = hash('sha256', $salt.$ballsDouble);

    if ($keyRequest != $keyLocal) {
        $balance = false;
        $win_sum = false;
        $status = false;
        $count =false;
        $result=false;
        $lastbal=false;
        $picked=false;
        $server_numbers = false;
        $user_numbers =false;
        $result=false;
        $mesage = "Error request not validate";
        $password = false;
    }
}

/// keno duble
$data = json_encode(array(
    "balance" => $balance,
    "result" => $win_sum,
    "status" => $status,
    "balls" => $server_numbers,
    "balls_selected" => $user_numbers,
    "got_balls" => $count,
    "win_balls" => $result,
    "lastbal" => $lastbal,
    "picked" => $picked,
    "mesage" => $mesage,
    "key" => $password
));

echo $data;
//END

//                                ======================== FUNCTIONS ========================

// generator
function getNumbers($kenoDouble, $ballsDouble, $count = 20)
{
    $numbers = range(1, 80);
/// !!!!!!!!!!!!  duble keno
    if ($kenoDouble == true and $ballsDouble != 0) {
        $ballsDouble = array_map('intval', explode(',', $_GET["ballsDouble"]));
        foreach ($ballsDouble as $oldNumber) {
            unset($numbers[$oldNumber - 1]);
        }
    }
    // !!!!!!!!!!!!!  duble keno // end
    shuffle($numbers);

    if ($kenoDouble == true and $ballsDouble != 0) {
        $numbers = $ballsDouble+$numbers;
    }

    return array_slice($numbers, 0, $count);
}
