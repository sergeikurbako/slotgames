(function(){
    var preload = {};

    preload.preload = function() {
        //для прогресс-бара
        game.load.onFileComplete.add(function(progress, cacheKey, success, totalLoaded, totalFiles){
            document.getElementById('percent-preload').innerHTML = progress;
        });

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignVertically = true;
        game.scale.scaleMode = 2;
        game.scale.pageAlignHorizontally = true;
        //анимация продолжает работать после фокуса на другое окно
        game.stage.disableVisibilityChange = true;
		        var needUrlPath = '';
        if (location.href.indexOf('/games/') !== -1 && location.href.indexOf('public') !== -1) {
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + location.pathname;
        } else if (location.href.indexOf('/game/') !== -1) {
            var gamename = location.href.substring(location.href.indexOf('/game/') + 6);
            needUrlPath = location.href.substring(0,location.href.indexOf('/game/')) + '/games/' + gamename;
        } else if (location.href.indexOf('public') === -1 && location.href.indexOf('/games/') !== -1 ) {
            var gamename = location.href.substring(location.href.indexOf('/games/') + 7);
            needUrlPath = location.href.substring(0,location.href.indexOf('://')) + '://' + location.hostname + '/games/' + gamename
        }
        /* bg */
        game.load.image('bg1', needUrlPath + '/img/bg/4.jpg');
        game.load.image('bg2', needUrlPath + '/img/bg/90.png');
        game.load.image('bg_top', needUrlPath + '/img/bg/215.png');
        game.load.image('bg_fs', needUrlPath + '/img/bg/1.png');

        /* Big WIn */ 
        game.load.image('bigWinBG', needUrlPath + '/img/430.png');
        game.load.image('sun', needUrlPath + '/img/437.png');

        /* panelGroup */
        game.load.image('score', needUrlPath + '/img/bottom_line/661.png');
        game.load.image('buttonPanel', needUrlPath + '/img/desktopButtons/91.png');

        /* buttonGroup */
        game.load.image('fullButton', needUrlPath + '/img/bottom_line/680.png');
        game.load.image('nonFull', needUrlPath + '/img/bottom_line/679.png');
        game.load.image('soundOn', needUrlPath + '/img/bottom_line/3.png');
        game.load.image('soundOn_h', needUrlPath + '/img/bottom_line/3_h.png');
        game.load.image('soundOff', needUrlPath + '/img/bottom_line/2.png');
        game.load.image('soundOff_h', needUrlPath + '/img/bottom_line/2_h.png');
        game.load.image('settings', needUrlPath + '/img/bottom_line/1.png');
        game.load.image('settings_h', needUrlPath + '/img/bottom_line/1_h.png');
        game.load.image('autoStart', needUrlPath + '/img/bottom_line/4.png');
        game.load.image('autoStart_h', needUrlPath + '/img/bottom_line/4_h.png');
        game.load.image('auto_play', needUrlPath + '/img/desktopButtons/auto_play.png');
        game.load.image('auto_play_d', needUrlPath + '/img/desktopButtons/auto_play_d.png');
        game.load.image('auto_play_h', needUrlPath + '/img/desktopButtons/auto_play_h.png');
        game.load.image('auto_play_p', needUrlPath + '/img/desktopButtons/auto_play_p.png');
        game.load.image('max_bet', needUrlPath + '/img/desktopButtons/max_bet.png');
        game.load.image('max_bet_d', needUrlPath + '/img/desktopButtons/max_bet_d.png');
        game.load.image('max_bet_h', needUrlPath + '/img/desktopButtons/max_bet_h.png');
        game.load.image('max_bet_p', needUrlPath + '/img/desktopButtons/max_bet_p.png');
        game.load.image('start', needUrlPath + '/img/desktopButtons/start.png');
        game.load.image('start_d', needUrlPath + '/img/desktopButtons/start_d.png');
        game.load.image('start_h', needUrlPath + '/img/desktopButtons/start_h.png');
        game.load.image('start_p', needUrlPath + '/img/desktopButtons/start_p.png');        
        game.load.image('right_btn', needUrlPath + '/img/desktopButtons/right_btn.png');
        game.load.image('right_btn_d', needUrlPath + '/img/desktopButtons/right_btn_d.png');
        game.load.image('right_btn_h', needUrlPath + '/img/desktopButtons/right_btn_h.png');
        game.load.image('right_btn_p', needUrlPath + '/img/desktopButtons/right_btn_p.png');
        game.load.image('left_btn', needUrlPath + '/img/desktopButtons/left_btn.png');
        game.load.image('left_btn_d', needUrlPath + '/img/desktopButtons/left_btn_d.png');
        game.load.image('left_btn_h', needUrlPath + '/img/desktopButtons/left_btn_h.png');
        game.load.image('left_btn_p', needUrlPath + '/img/desktopButtons/left_btn_p.png');
        game.load.image('paytable', needUrlPath + '/img/desktopButtons/paytable.png');
        game.load.image('paytable_h', needUrlPath + '/img/desktopButtons/paytable_h.png');
        game.load.image('paytable_p', needUrlPath + '/img/desktopButtons/paytable_p.png');
        // game.load.image('paytable_mask', needUrlPath + '/img/desktopButtons/paytable_mask.png');
        game.load.image('paytable_mask2', needUrlPath + '/img/desktopButtons/paytable_mask2.png');
        game.load.image('green_line_1', needUrlPath + '/img/desktopButtons/green_line_1.png');
        game.load.image('green_line_2', needUrlPath + '/img/desktopButtons/green_line_2.png');
        game.load.image('black_line_1', needUrlPath + '/img/desktopButtons/black_line_1.png');
        game.load.image('black_line_2', needUrlPath + '/img/desktopButtons/black_line_2.png');
        game.load.image('green_line_1_p', needUrlPath + '/img/desktopButtons/green_line_1_p.png');
        game.load.image('green_line_2_p', needUrlPath + '/img/desktopButtons/green_line_2_p.png');
        game.load.image('green_line_1_h', needUrlPath + '/img/desktopButtons/green_line_1_h.png');
        game.load.image('green_line_2_h', needUrlPath + '/img/desktopButtons/green_line_2_h.png');
        game.load.image('green_line_1_d', needUrlPath + '/img/desktopButtons/green_line_1_d.png');
        game.load.image('green_line_2_d', needUrlPath + '/img/desktopButtons/green_line_2_d.png');

        /* Paytable */
        game.load.image('page_1', needUrlPath + '/img/paytable/page_1.png');
        game.load.image('page_2', needUrlPath + '/img/paytable/page_2.png');        
        game.load.image('paytable_right_btn', needUrlPath + '/img/paytable/right_btn.png');
        game.load.image('paytable_right_btn_h', needUrlPath + '/img/paytable/right_btn_h.png');
        game.load.image('paytable_right_btn_p', needUrlPath + '/img/paytable/right_btn_p.png');
        game.load.image('paytable_center_btn', needUrlPath + '/img/paytable/centr_btn.png');
        game.load.image('paytable_center_btn_h', needUrlPath + '/img/paytable/centr_btn_h.png');
        game.load.image('paytable_center_btn_p', needUrlPath + '/img/paytable/centr_btn_p.png');
        game.load.image('paytable_left_btn', needUrlPath + '/img/paytable/left_btn.png');
        game.load.image('paytable_left_btn_h', needUrlPath + '/img/paytable/left_btn_h.png');
        game.load.image('paytable_left_btn_p', needUrlPath + '/img/paytable/left_btn_p.png');
        game.load.atlasJSONHash('paytableVideo', needUrlPath + '/img/paytable/paytableVideo.png', needUrlPath + '/img/paytable/paytableVideo.json');

        /* slotLayer1Group */
        game.load.image('cell0', needUrlPath + '/img/slotValues/0.png');
        game.load.image('cell1', needUrlPath + '/img/slotValues/1.png');
        game.load.image('cell2', needUrlPath + '/img/slotValues/2.png');
        game.load.image('cell3', needUrlPath + '/img/slotValues/3.png');
        game.load.image('cell4', needUrlPath + '/img/slotValues/4.png');
        game.load.image('cell5', needUrlPath + '/img/slotValues/5.png');
        game.load.image('cell6', needUrlPath + '/img/slotValues/6.png');
        game.load.image('cell7', needUrlPath + '/img/slotValues/7.png');
        game.load.image('cell0_f', needUrlPath + '/img/slotValues/0_f.png');
        game.load.image('cell1_f', needUrlPath + '/img/slotValues/1_f.png');
        game.load.image('cell2_f', needUrlPath + '/img/slotValues/2_f.png');
        game.load.image('cell3_f', needUrlPath + '/img/slotValues/3_f.png');
        game.load.image('cell4_f', needUrlPath + '/img/slotValues/4_f.png');
        game.load.image('cell5_f', needUrlPath + '/img/slotValues/5_f.png');
        game.load.image('cell6_f', needUrlPath + '/img/slotValues/6_f.png');
        game.load.image('cell7_f', needUrlPath + '/img/slotValues/7_f.png');
        game.load.image('slotBar', needUrlPath + '/img/slotValues/slotColumn.png');

        /* slotLayer2Group */
        game.load.image('line1', needUrlPath + '/img/lines/1.png');
        game.load.image('line2', needUrlPath + '/img/lines/2.png');
        game.load.image('line3', needUrlPath + '/img/lines/3.png');
        game.load.image('line4', needUrlPath + '/img/lines/4.png');
        game.load.image('line5', needUrlPath + '/img/lines/5.png');
        game.load.image('line6', needUrlPath + '/img/lines/6.png');
        game.load.image('line7', needUrlPath + '/img/lines/7.png');
        game.load.image('line8', needUrlPath + '/img/lines/8.png');
        game.load.image('line9', needUrlPath + '/img/lines/9.png');
        game.load.image('line10', needUrlPath + '/img/lines/10.png');


        /* numberGroup */
        game.load.image('number1_h', needUrlPath + '/img/lineNumbers/1_h.png');
        game.load.image('number2_h', needUrlPath + '/img/lineNumbers/2_h.png');
        game.load.image('number3_h', needUrlPath + '/img/lineNumbers/3_h.png');
        game.load.image('number4_h', needUrlPath + '/img/lineNumbers/4_h.png');
        game.load.image('number5_h', needUrlPath + '/img/lineNumbers/5_h.png');
        game.load.image('number6_h', needUrlPath + '/img/lineNumbers/6_h.png');
        game.load.image('number7_h', needUrlPath + '/img/lineNumbers/7_h.png');
        game.load.image('number8_h', needUrlPath + '/img/lineNumbers/8_h.png');
        game.load.image('number9_h', needUrlPath + '/img/lineNumbers/9_h.png');
        game.load.image('number10_h', needUrlPath + '/img/lineNumbers/10_h.png');
        game.load.image('number1', needUrlPath + '/img/lineNumbers/1.png');
        game.load.image('number2', needUrlPath + '/img/lineNumbers/2.png');
        game.load.image('number3', needUrlPath + '/img/lineNumbers/3.png');
        game.load.image('number4', needUrlPath + '/img/lineNumbers/4.png');
        game.load.image('number5', needUrlPath + '/img/lineNumbers/5.png');
        game.load.image('number6', needUrlPath + '/img/lineNumbers/6.png');
        game.load.image('number7', needUrlPath + '/img/lineNumbers/7.png');
        game.load.image('number8', needUrlPath + '/img/lineNumbers/8.png');
        game.load.image('number9', needUrlPath + '/img/lineNumbers/9.png');
        game.load.image('number10', needUrlPath + '/img/lineNumbers/10.png');
        game.load.image('circle_h', needUrlPath + '/img/lineNumbers/circle_h.png');
        game.load.image('circle', needUrlPath + '/img/lineNumbers/circle.png');

        // animWin JSON
        game.load.atlasJSONHash('WinAnim0', needUrlPath + '/img/WinAnim/WinAnim0.png', needUrlPath + '/img/WinAnim/WinAnim0.json');
        game.load.atlasJSONHash('WinAnim1', needUrlPath + '/img/WinAnim/WinAnim1.png', needUrlPath + '/img/WinAnim/WinAnim1.json');
        game.load.atlasJSONHash('WinAnim2', needUrlPath + '/img/WinAnim/WinAnim2.png', needUrlPath + '/img/WinAnim/WinAnim2.json');
        game.load.atlasJSONHash('WinAnim3', needUrlPath + '/img/WinAnim/WinAnim3.png', needUrlPath + '/img/WinAnim/WinAnim3.json');
        game.load.atlasJSONHash('WinAnim4', needUrlPath + '/img/WinAnim/WinAnim4.png', needUrlPath + '/img/WinAnim/WinAnim4.json');
        game.load.atlasJSONHash('WinAnim5', needUrlPath + '/img/WinAnim/WinAnim5.png', needUrlPath + '/img/WinAnim/WinAnim5.json');
        game.load.atlasJSONHash('WinAnim6', needUrlPath + '/img/WinAnim/WinAnim6.png', needUrlPath + '/img/WinAnim/WinAnim6.json');
        game.load.atlasJSONHash('WinAnim7', needUrlPath + '/img/WinAnim/WinAnim7.png', needUrlPath + '/img/WinAnim/WinAnim7.json');

        /* Soldier Anim*/ 
        // game.load.spritesheet('bigWin_gold_10', needUrlPath + '/img/game1/bigWin_anim/7_3.png', 368, 768);

        /* Sound */ 
        game.load.audio('bgSound', needUrlPath + '/sound/24_AmbienceBasic.mp3');
        game.load.audio('SpinButton', needUrlPath + '/sound/20_SpinButtonSnd.mp3');
        game.load.audio('PaytableButton', needUrlPath + '/sound/21_PaytableButtonSnd.mp3');
        game.load.audio('PanelButton', needUrlPath + '/sound/23_DefaultKeypadButtonSnd.mp3');
        game.load.audio('ReelBounce', needUrlPath + '/sound/13_ReelBounceSnd.mp3');
        game.load.audio('ReelSpin', needUrlPath + '/sound/14_ReelSpinSnd.mp3');
        game.load.audio('winCountLoop', needUrlPath + '/sound/16_WinCountUpLoopSnd.mp3');
        game.load.audio('winCountEnd', needUrlPath + '/sound/15_WinCountUpEndSnd.mp3');
        game.load.audio('WinSnd1', needUrlPath + '/sound/5_WinSnd1.mp3');
        game.load.audio('WinSnd2', needUrlPath + '/sound/3_WinSnd2.mp3');
        game.load.audio('WinSnd3', needUrlPath + '/sound/4_WinSnd3.mp3');
        game.load.audio('WinSymbol', needUrlPath + '/sound/1_WinSymbolMedSnd.mp3');
        game.load.audio('BigWin_1', needUrlPath + '/sound/17_BigWinCrescendoAndInitialSnd.mp3');
        game.load.audio('BigWin_2', needUrlPath + '/sound/18_BigWinAmbienceSnd.mp3');

    };

    preload.create = function() {
        game.state.start('game1');
        document.getElementById('preloader').style.display = 'none';
    };

    game.state.add('preload', preload);

})();

game.state.start('preload');

